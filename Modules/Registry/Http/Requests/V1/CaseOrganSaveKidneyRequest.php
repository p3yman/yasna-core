<?php

namespace Modules\Registry\Http\Requests\V1;

class CaseOrganSaveKidneyRequest extends CaseOrganSaveRequest
{
    /**
     * get validation rules for prohibitions
     *
     * @return array
     */
    protected function prohibitionRules()
    {
        $first  = $this->getComboValidator("kidney_prohibit_by_desc", "kidney-prohibit-desc");
        $second = $this->getComboValidator("kidney_prohibit_by_tests", "kidney-prohibit-tests");
        $third  = $this->getComboValidator("kidney_prohibit_by_biopsy", "kidney-prohibit-biopsy");

        return [
             "kidney_prohibit_by_desc"   => $first,
             "kidney_prohibit_by_tests"  => $second,
             "kidney_prohibit_by_biopsy" => $third,
        ];
    }



    /**
     * get validation rules for the section 1 of the form
     *
     * @return array
     */
    protected function section1Rules()
    {
        $required       = !boolval($this->getData('kidney_prohibit'));
        $requirement    = $required ? "required|" : "";
        $abnormality    = $this->getComboValidator("kidney_abnormal_finding_type", "kidney-abnormality");
        $hydronephrosis = $this->getComboValidator("kidney_hydronephrosis_severity", "severity");

        return [
             "kidney_sonography_report" => $requirement . "array",
             "kidney_size_left"         => $this->getComboValidator("kidney_size_left", "kidney-size", $required),
             "kidney_size_left_cm"      => $requirement . "numeric|between:5,20",
             "kidney_size_right"        => $this->getComboValidator("kidney_size_right", "kidney-size", $required),
             "kidney_size_right_cm"     => $requirement . "numeric|between:5,20",

             "kidney_abnormal_finding"          => $requirement . "boolean",
             "kidney_abnormal_finding_type"     => "required_if:kidney_abnormal_finding,1|$abnormality",
             "kidney_abnormal_finding_other"    => "required_if:kidney_abnormal_finding_type,kidney-abnormality-others",
             "kidney_abnormal_finding_position" => "required_if:kidney_abnormal_finding,1",
             "kidney_abnormal_finding_size"     => "required_if:kidney_abnormal_finding,1",
             "kidney_hydronephrosis_severity"   => "required_if:kidney_abnormal_finding_type,kidney-abnormality-hydronephrosis|$hydronephrosis",
        ];
    }



    /**
     * get validation rules for the section 2 of the form
     *
     * @return array
     */
    protected function section2Rules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";
        $type        = $this->getComboValidator("kidney_biopsy_type", "kidney-biopsy-type");
        $fibrosis    = $this->getComboValidator("kidney_biopsy_fibrosis", "kidney-biopsy-severity", $required);
        $vascular    = $this->getComboValidator("kidney_biopsy_vascular_changes", "kidney-biopsy-severity", $required);
        $glomerulus  = $this->getComboValidator("kidney_biopsy_glomerulus", "kidney-glomerulus");

        return [
             "kidney_biopsy"                  => $requirement . "boolean",
             "kidney_biopsy_type"             => "required_if:kidney_biopsy,1|$type",
             "kidney_biopsy_type_other"       => "required_if:kidney_biopsy_type,kidney-biopsy-type-others",
             "kidney_biopsy_fibrosis"         => "required_if:kidney_biopsy,1|$fibrosis",
             "kidney_biopsy_vascular_changes" => "required_if:kidney_biopsy,1|$vascular",
             "kidney_biopsy_glomerulus_count" => "required_if:kidney_biopsy,1|numeric|between:0,300",
             "kidney_biopsy_glomerulus"       => $glomerulus,
             "kidney_biopsy_report"           => $requirement . "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "kidney_size_left_cm"              => "ed",
             "kidney_size_right_cm"             => "ed",
             "kidney_abnormal_finding_size"     => "ed",
             "kidney_biopsy_glomerulus_count"   => "ed",
             "kidney_biopsy_glomerulus_percent" => "ed",
        ];
    }



    /**
     * get validation rules for the section 3 of the form
     *
     * @return array
     */
    protected function section3Rules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";

        return [
             "kidney_hla_typing"      => $requirement . "boolean",
             "kidney_hla_typing_desc" => "required_if:kidney_hla_typing,1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
        //$this->allowOneProhibition();
    }



    /**
     * set `heart_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("kidney_organ_filled", 1);
    }



    /**
     * set `kidney_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if (
             $this->getData("kidney_prohibit_by_desc")
             or $this->getData("kidney_prohibit_by_tests")
             or $this->getData("kidney_prohibit_by_biopsy")
        ) {
            $this->setData("kidney_prohibit", 1);
        }

        $this->setData("kidney_prohibit", 0);
    }



    /**
     * allow one prohibition at a time
     */
    private function allowOneProhibition()
    {
        if ($this->getData("kidney_prohibit_by_desc")) {
            $this->setData("kidney_prohibit_by_tests", null);
            $this->setData("kidney_prohibit_by_biopsy", null);
        }

        if ($this->getData("kidney_prohibit_by_tests")) {
            $this->setData("kidney_prohibit_by_desc", null);
            $this->setData("kidney_prohibit_by_biopsy", null);
        }

        if ($this->getData("kidney_prohibit_by_biopsy")) {
            $this->setData("kidney_prohibit_by_desc", null);
            $this->setData("kidney_prohibit_by_tests", null);
        }
    }

}
