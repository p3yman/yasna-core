<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UniversitySaveRequest
 *
 * @property UnityAbstract $model
 * @package Modules\Registry\Http\Requests\V1
 */
class UnityRoleRevokeRequest extends YasnaRequest
{
    protected $responder = 'white-house';

    private $allowed_roles = [];



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->canRevoke();
    }



    /**
     * General rules on revoking
     *
     * @return array
     */
    public function mainRules()
    {

        return [
             "unity" => $this->model->exists ? "" : "accepted",
             "user"  => $this->data['user']->exists ? "" : "accepted",
             "type"  => "required", //<~~ never happens!
        ];
    }



    /**
     * Roles rules for revoking
     *
     * @return array
     */
    public function rolesRules()
    {
        $allowed_roles  = $this->model->userRoles();
        $allowed_string = implode(",", $allowed_roles);
        $roles          = $this->getData('role');
        $is_subset      = count(array_intersect($allowed_roles, (array)$roles)) == count((array)$roles);

        if (is_string($roles)) {
            return [
                 "role" => "required|in:$allowed_string",
            ];
        }

        return [
             "role" => $is_subset ? "required" : "required|accepted",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "user.accepted"  => trans("registry::validation.user_not_found"),
             "role.in"        => trans("registry::validation.role_not_found"),
             "role.required"  => trans("registry::validation.role_not_found"),
             "role.accepted"  => trans("registry::validation.role_not_found"),
             "unity.accepted" => trans("registry::validation.unity_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveModelName();
        $this->resolveUnity();
        $this->resolveUser();
        $this->resolveAllowedRoles();
    }



    /**
     * Return requested roles that current user can revoke them
     *
     * @return array
     */
    public function getAllowedRoles(): array
    {
        return $this->allowed_roles;
    }



    /**
     * resolve $this->model_name, based on the given type
     *
     * @return void
     */
    private function resolveModelName()
    {
        $this->model_name = studly_case("registry-" . $this->data['type']);
    }



    /**
     * resolve the unity
     *
     * @return void
     */
    private function resolveUnity()
    {
        $this->model = model($this->model_name)->grabHashid($this->data['unity']);
    }



    /**
     * resolve user model
     *
     * @return void
     */
    private function resolveUser()
    {
        if (isset($this->data['user'])) {
            $this->data['user'] = user()->grabHashid($this->data['user']);
        } else {
            $this->data['user'] = user(-1);
        }
    }



    /**
     * Resolve allowed roles which current user can revoke
     */
    private function resolveAllowedRoles()
    {
        $roles = (array)$this->getData('role');

        foreach ($roles as $role) {
            if ($this->model->canAssign($role)) {
                $this->allowed_roles[] = $role;
            }
        }
    }



    /**
     * Indicate current user can revoke roles or not
     *
     * @return bool
     */
    private function canRevoke(): bool
    {
        return !empty($this->getAllowedRoles());
    }
}
