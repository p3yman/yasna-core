<?php

namespace Modules\Registry\Http\Requests\V1;

class CaseOrganSavePancreasRequest extends CaseOrganSaveRequest
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $insulin             = $this->getComboValidator("received_insulin_type", "insulin-type");
        $insulin_requirement = boolval($this->getData("pancreas_prohibit")) ? "" : "required|";
        $prohibit            = $this->getComboValidator("pancreas_prohibit_by_desc", "pancreas-prohibit-desc");

        return [
             "pancreas_prohibit_by_desc"   => $prohibit,
             "pancreas_hla_typing"         => "array",
             "test_hemoglobin_a1c"         => "required|numeric|between:3,20",
             "test_serum_amylase"          => "required|numeric|between:0,5000",
             "test_serum_lipase"           => "required|numeric|between:0,5000",
             "received_insulin"            => $insulin_requirement . "boolean",
             "received_insulin_type"       => "required_if:received_insulin,1|$insulin",
             "received_insulin_daily_dose" => "required_if:received_insulin,1|numeric",
             "organ"                       => "required",
             "tests_group6_done_at"        => "date",
             "tests_group7_done_at"        => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "test_hemoglobin_a1c"    => "ed",
             "test_serum_amylase"     => "ed",
             "test_serum_lipase"      => "ed",
             "tests_group6_done_at"   => "date",
             "tests_group7_done_at"   => "date",
        ];
    }



    /**
     * set `pancreas_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("pancreas_organ_filled", 1);
    }



    /**
     * set `kidney_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if ($this->getData("pancreas_prohibit_by_desc")) {
            $this->setData("pancreas_prohibit", 1);
        }

        $this->setData("pancreas_prohibit", 0);
    }

}
