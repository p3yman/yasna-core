<?php

namespace Modules\Registry\Http\Requests\V1;

class CaseOrganSaveLungRequest extends CaseOrganSaveRequest
{
    /**
     * get validation rules for prohibitions
     *
     * @return array
     */
    protected function prohibitionRules()
    {
        $desc      = $this->getComboValidator("lung_prohibit_by_desc", "lung-prohibit-desc");
        $radiology = $this->getComboValidator("lung_prohibit_by_radiology", "lung-prohibit-radiology");
        //$o2challenge  = $this->getComboValidator("lung_prohibit_by_o2challenge", "lung-prohibit-o2challenge");
        $bronchoscopy = $this->getComboValidator("lung_prohibit_by_bronchoscopy", "lung-prohibit-bronchoscopy");

        return [
             "lung_prohibit_by_desc"         => $desc,
             "lung_prohibit_by_radiology"    => $radiology,
             //"lung_prohibit_by_o2challenge"  => $o2challenge,
             "lung_prohibit_by_bronchoscopy" => $bronchoscopy,
        ];
    }



    /**
     * get validation rules regarding the ventilator fields (section 1)
     *
     * @return array
     */
    protected function ventilatorRules()
    {
        $required      = !boolval($this->getData("lung_prohibit"));
        $requirement   = $required ? "required|" : "";
        $modeValidator = $this->getComboValidator("lung_ventilator_mode", "lung-ventilator-mode", $required);

        return [
             "lung_ventilator_mode"       => $modeValidator,
             "lung_ventilator_mode_other" => "required_if:lung_ventilator_mode,lung-ventilator-mode-others",

             "lung_fio2" => $requirement . "numeric|between:0,100",
             "lung_peep" => $requirement . "numeric|between:0,40",
             "lung_tv"   => $requirement . "numeric|between:0,2000",
             "lung_rr"   => $requirement . "numeric|between:0,40",
        ];
    }



    /**
     * get validation rules regarding the o2 challenge fields (section 2)
     *
     * @return array
     */
    protected function o2challengeRules()
    {
        $required    = !boolval($this->getData("lung_prohibit"));
        $requirement = $required ? "required|" : "";

        $this->setData("lung_o2challenge_fio2", 100); // <~~ Due to the new requirements

        return [
             "lung_o2challenge_po2"  => $requirement . "numeric|between:1,700",
             "lung_o2challenge_fio2" => $requirement . "numeric|between:0,100",
        ];
    }



    /**
     * get validation rules regarding recruitment test (section 3)
     *
     * @return array
     */
    protected function recruitmentRules()
    {
        $required    = boolval(intval($this->getData("lung_o2challenge_po2")) < 300);
        $requirement = $required ? "required|" : "";
        $cause       = $this->getComboValidator("lung_o2challenge_cause", "o2challenge-cause");

        if ($this->getData("lung_recruitment_test")) {
            $this->setData("lung_o2challenge_fio2", 100); // <~~ Due to the new requirements
        }

        return [
             "lung_recruitment_fio2"  => $requirement . "numeric",
             "lung_recruitment_test"  => $requirement . "boolean",
             "lung_o2challenge_cause" => "required_if:lung_recruitment_test,0|$cause",
             "lung_o2challenge_file"  => "required_if:lung_recruitment_test,1|array",
             "lung_recruitment_po2"   => "required_if:lung_recruitment_test,1|numeric|between:1,700",
             "lung_o2challenge_fio2"  => "required_if:lung_recruitment_test,1|numeric|between:1,100",
        ];
    }



    /**
     * get validation rules for CXR and any other thing in section 4
     *
     * @return array
     */
    protected function cxrRules()
    {
        $required             = !boolval($this->getData("lung_prohibit"));
        $requirement          = $required ? "required|" : "";
        $pneumothorax_reason  = $this->getComboValidator("lung_pneumothorax_reason", "lung-pneumothorax");
        $infiltration_section = $this->getComboValidator("lung_infiltration_section", "lung-side");
        $collapse_section     = $this->getComboValidator("lung_collapse_section", "lung-side");
        $tumor_section        = $this->getComboValidator("lung_tumor_section", "lung-side");

        return [
             "heart_cxr"                 => $requirement . "boolean",
             "heart_cxr_report"          => $requirement . "array",
             "lung_pneumothorax"         => $requirement . "boolean",
             "lung_pneumothorax_reason"  => "required_if:lung_pneumothorax,1|$pneumothorax_reason",
             "lung_infiltration"         => $requirement . "boolean",
             "lung_infiltration_section" => "required_if:lung_infiltration,1|$infiltration_section",
             "lung_collapse"             => $requirement . "boolean",
             "lung_collapse_section"     => "required_if:lung_collapse,1|$collapse_section",
             "lung_tumor"                => $requirement . "boolean",
             "lung_tumor_section"        => "required_if:lung_tumor,1|$tumor_section",
        ];
    }



    /**
     * get validation rules for CT Scan (section 5)
     *
     * @return array
     */
    protected function ctScanRules()
    {
        $required         = false;
        $requirement      = $required ? "required|" : "";
        $reason_validator = $this->getComboValidator("lung_ctscan_reason", "lung-ctscan-cause");

        return [
             "lung_ctscan_need"         => $requirement . "boolean",
             "lung_ctscan_reason"       => "required_if:lung_ctscan_need,1|$reason_validator",
             "lung_ctscan_reason_other" => "required_if:lung_ctscan_reason,lung-ctscan-cause-others",
        ];
    }



    /**
     * get abnormality rules for bronchoscopy (added after section 6)
     *
     * @return array
     */
    protected function abnormalityRules()
    {
        $left_abn      = boolval($this->getData("lung_left_abnormal"));
        $left_abn_rule = $this->getComboValidator("lung_left_abnormality", "lung-abnormality", $left_abn);
        $left_sev_rule = $this->getComboValidator("lung_left_abnormality_severity", "severity", $left_abn);

        $right_abn      = boolval($this->getData("lung_right_abnormal"));
        $right_abn_rule = $this->getComboValidator("lung_right_abnormality", "lung-abnormality", $right_abn);
        $right_sev_rule = $this->getComboValidator("lung_right_abnormality_severity", "severity", $right_abn);

        return [
             "lung_left_abnormal"             => "boolean",
             "lung_left_abnormality"          => $left_abn_rule,
             "lung_left_abnormality_other"    => "required_if:lung_left_abnormality,lung-abnormality-others",
             "lung_left_abnormality_severity" => $left_sev_rule,

             "lung_right_abnormal"             => "boolean",
             "lung_right_abnormality"          => $right_abn_rule,
             "lung_right_abnormality_other"    => "required_if:lung_right_abnormality,lung-abnormality-others",
             "lung_right_abnormality_severity" => $right_sev_rule,
        ];
    }



    /**
     * get validation rules for secretion (section 7)
     *
     * @return array
     */
    protected function secretionRules()
    {
        $required    = !boolval($this->getData("lung_prohibit"));
        $type        = $this->getComboValidator("lung_pulmonary_secretion_culture_type", "pulmonary-secretion-culture");
        $cultur_rule = $this->getComboValidator("lung_pulmonary_secretion_culture", "boolean", $required);

        return [
             "lung_pulmonary_secretion_culture"             => $cultur_rule,
             "lung_pulmonary_secretion_culture_type"        => "required_if:lung_pulmonary_secretion_culture,1|$type",
             "lung_pulmonary_secretion_culture_detail"      => "required_if:lung_pulmonary_secretion_culture,1",
             "lung_pulmonary_secretion_culture_antibiotic"  => "required_if:lung_pulmonary_secretion_culture,1",
             "lung_pulmonary_secretion_culture_sensitivity" => "required_if:lung_pulmonary_secretion_culture,1",
             "lung_pulmonary_secretion_culture_resistance"  => "required_if:lung_pulmonary_secretion_culture,1",

        ];
    }



    /**
     * get validation rules for choice of lung (section 8)
     *
     * @return array
     */
    protected function lungChoiceRules()
    {
        $required    = !boolval($this->getData("lung_prohibit"));
        $requirement = $required ? "required|" : "";

        return [
             "lung_can_donate_left"  => $requirement . "boolean",
             "lung_can_donate_right" => $requirement . "boolean",
        ];
    }



    /**
     * get validation rules for bal culture
     *
     * @return array
     */
    protected function balCultureRules()
    {
        return [
             "bacteria_bal_culture"       => "required|boolean",
             "bacteria_bal_culture_type"  => "required_if:bacteria_bal_culture,1",
             "bacteria_bal_culture_other" => "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
        //$this->allowOneProhibition();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "lung_fio2"             => 'ed',
             "lung_peep"             => 'ed',
             "lung_tv"               => 'ed',
             "lung_rr"               => 'ed',
             "lung_o2challenge_fio2" => 'ed',
             "lung_o2challenge_po2"  => 'ed',
             "lung_recruitment_fio2" => 'ed',
             "lung_recruitment_po2"  => 'ed',

        ];
    }



    /**
     * set `heart_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("lung_organ_filled", 1);
    }



    /**
     * set `heart_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if (
             $this->getData("lung_prohibit_by_desc")
             or $this->getData("lung_prohibit_by_radiology")
             //or $this->getData("lung_prohibit_by_o2challenge")
             or $this->getData("lung_prohibit_by_bronchoscopy")
        ) {
            $this->setData("lung_prohibit", 1);
        }

        $this->setData("lung_prohibit", 0);
    }
}
