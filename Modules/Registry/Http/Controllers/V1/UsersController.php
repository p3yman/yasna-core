<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryUnityRole;
use App\Models\User;
use App\Models\UsersLogin;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Http\Requests\V1\AssignMasterRequest;
use Modules\Registry\Http\Requests\V1\GetUserFieldsRequest;
use Modules\Registry\Http\Requests\V1\LoginsHistoryRequest;
use Modules\Registry\Http\Requests\V1\UpdateSelfProfileRequest;
use Modules\Registry\Http\Requests\V1\UserEditRequest;
use Modules\Registry\Http\Requests\V1\UserRolesRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaFormRequest;
use Modules\Yasna\Services\YasnaApiController;

class UsersController extends YasnaApiController
{
    /**
     * get validation rules or purification rules in the absences of role
     *
     * @return array
     */
    public function validationRules()
    {
        $role = request()->role;

        if (!$role) {
            return $this->purificationRules();
        }

        return $this->success(
             (array)config("registry.roles.$role.fields"),
             [
                  "role" => $role,
             ]
        );
    }



    /**
     * get user fields, called from UserFieldsEndpoint
     *
     * @param GetUserFieldsRequest $request
     *
     * @return array
     */
    public function userFields(GetUserFieldsRequest $request)
    {
        return $this->success($request->getMergedFieldRules(), [
             "hashid" => $request->model->id,
        ]);
    }



    /**
     * get the unity roles
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function unityRoles(SimpleYasnaRequest $request)
    {
        $model_name = studly_case("registry-" . $request->type);
        if (!class_exists(MODELS_NAMESPACE . $model_name)) {
            return $this->clientError(404);
        }

        /** @var UnityAbstract $model */
        $model = model("registry-" . $request->type);

        return $this->success([
             "administrative_roles" => $model->administrativeRoles(),
             "dependant_roles"      => $model->dependantRoles(),
        ], [
             "type" => $request->type,
        ]);
    }



    /**
     * perform self-profile update
     *
     * @param UpdateSelfProfileRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateSelfProfile(UpdateSelfProfileRequest $request)
    {
        $done = user()->batchSave($request);

        return $this->modelSaveFeedback($done);
    }



    /**
     * Return login history of a user.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function lastLogins(SimpleYasnaRequest $request)
    {
        $counts = $request->input('counts', 3);
        $counts = $counts > 10 ? 10 : $counts;
        $logins = user()->lastLogins($counts);
        if ($logins) {
            $results = $logins->map(function ($login) {
                return $login->toResource();
            });

            return $this->success($results->toArray());
        }

        return $this->success([]);
    }



    /**
     * Return a combined array of current logged-in user information
     *
     * @return array
     */
    public function userChecker()
    {
        return $this->success(user()->toFront());
    }



    /**
     * get the user-roles purification rules
     *
     * @return array
     */
    private function purificationRules()
    {
        return $this->success(
             (array)config("registry.roles.purification_rules"),
             [
                  "role" => "[Purification Rules]",
             ]
        );
    }



    /**
     * Returns the roles of the specified user and the information of each role in the white-house format.
     *
     * @param UserRolesRequest $request
     *
     * @return array
     */
    public function roles(UserRolesRequest $request)
    {
        $user = $request->model->exists
             ? $request->model
             : user();

        return $this->success(
             $this->getUserRolesArray($user),
             ["hashid" => $user->hashid]
        );
    }



    /**
     * Returns the roles of the specified user and the information of each role as an array.
     *
     * @param User $user
     *
     * @return array
     */
    protected function getUserRolesArray(User $user)
    {
        $unity_roles_collection = $user->unityRoles;
        $result                 = $unity_roles_collection
             ->map(function (RegistryUnityRole $role_relation) {
                 return $role_relation->toUserRolesResource();
             })
             ->toArray()
        ;

        if ($user->isSuperadmin()) {
            $result[] = [
                 'role'       => 'superadmin',
                 'unity'      => null,
                 'unity_type' => null,
            ];
        }

        if ($user->isDeveloper()) {
            $result[] = [
                 'role'       => 'developer',
                 'unity'      => null,
                 'unity_type' => null,
            ];
        }

        return $result;
    }



    /**
     * Edits the requested user with the given data.
     *
     * @param UserEditRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(UserEditRequest $request)
    {
        $saved_model = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * assign a superadmin as master
     *
     * @param AssignMasterRequest $request
     *
     * @return array
     */
    public function assignMaster(AssignMasterRequest $request)
    {
        /** @var User $user */
        foreach (role('master')->users()->get() as $user) {
            $user->detachRole('master');
        }

        $ok = $request->model->attachRole('master');

        return $this->typicalSaveFeedback($ok, [
             "hashid" => $request->model->hashid,
        ]);
    }



    /**
     * get the login history of the online user
     *
     * @param LoginsHistoryRequest $request
     *
     * @return array
     */
    public function loginsHistory(LoginsHistoryRequest $request)
    {
        $elector = $request->only('from', 'to', 'criteria', 'user');
        $builder = model("usersLogin")->elector($elector);

        if ($request->sort != "asc") {
            $builder->orderBy("id", "desc");
        }

        $paginator = $builder->paginate($request->perPage());
        $mapped    = $paginator->map(function (UsersLogin $user) {
            return $user->toResource();
        })->toArray()
        ;

        return $this->success($mapped, $this->paginatorMetadata($paginator));
    }



    /**
     * save the selected role.
     *
     * @param SimpleYasnaFormRequest $request
     *
     * @return array
     */
    public function saveSelectedRole(SimpleYasnaFormRequest $request)
    {
        $done = user()->updateOneMeta("selected_role", (string)$request->selected_role);

        return $this->typicalSaveFeedback($done);
    }
}
