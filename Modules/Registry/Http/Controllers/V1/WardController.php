<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryUnityRole;
use Modules\Registry\Http\Controllers\V1\Abstracts\UnityBaseController;
use Modules\Registry\Http\Controllers\V1\Traits\DeathCountsTrait;
use Modules\Registry\Http\Controllers\V1\Traits\FiltersBasedOnUserRoleTrait;
use Modules\Registry\Http\Requests\V1\WardSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class WardController extends UnityBaseController
{
    //use DeathCountsTrait;
    use FiltersBasedOnUserRoleTrait;

    protected $model_name = "registry_ward";



    /**
     * Save a ward.
     *
     * @param WardSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(WardSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);
        //if ($saved_model->exists) {
        //    $this->saveDeathCounts($request, $saved_model);
        //}

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * @inheritdoc
     */
    protected function listParticularElectors(SimpleYasnaRequest $request)
    {
        return [
             "type"              => "ward",
             "hasUnities"        => ["hospital", "university"],
             "opu"               => $request->opu,
             "hospital"          => $request->hospital,
             "university"        => $request->university,
        ];
    }
}
