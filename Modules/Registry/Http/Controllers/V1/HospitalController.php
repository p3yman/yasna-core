<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryHospital;
use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;
use Modules\Registry\Entities\RegistrySharedUnity;
use Modules\Registry\Http\Controllers\V1\Abstracts\UnityBaseController;
use Modules\Registry\Http\Controllers\V1\Traits\DeathCountsTrait;
use Modules\Registry\Http\Controllers\V1\Traits\FiltersBasedOnUserRoleTrait;
use Modules\Registry\Http\Requests\V1\HospitalSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class HospitalController extends UnityBaseController
{
    use DeathCountsTrait;
    use FiltersBasedOnUserRoleTrait;

    protected $model_name = "registry_hospital";



    /**
     * Save a given hospital under a university.
     *
     * @param HospitalSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(HospitalSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request, ['opu']);
        if ($saved_model->exists) {
            $this->saveDeathCounts($request, $saved_model);
        }

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * @inheritdoc
     */
    protected function listParticularElectors(SimpleYasnaRequest $request)
    {
        return [
             "type"       => "hospital",
             "hasUnities" => "university",
             "opu"        => $request->opu,
             "university" => $request->university,
        ];
    }
}
