<?php

namespace Modules\Registry\Http\Controllers\V1\Abstracts;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Http\Requests\V1\UnityRoleAssignmentRequest;
use Modules\Registry\Http\Requests\V1\UnityRoleRevokeRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaRequest;

trait AssignmentTrait
{
    /**
     * assign a unity role to a particular user
     *
     * @param UnityRoleAssignmentRequest $request
     *
     * @return array
     */
    public function assign(UnityRoleAssignmentRequest $request)
    {
        /*-----------------------------------------------
        | Check current user can assign requested roles
        */
        if (!$this->canAssign($request)) {
            return $this->clientError("registry-403");
        }

        /*-----------------------------------------------
        | In case of specific user ...
        */
        if ($request->askedForSpecificUser()) {
            return $this->assignSpecificUser($request);
        }

        /*-----------------------------------------------
        | Otherwise ...
        */
        $user = $this->findUserByCodeMelli($request->code_melli);
        $user = $this->createOrUpdateUser($user, $request);

        if (!$user->exists) {
            return $this->saveFailureError();
        }

        $request->user = $user;
        return $this->assignSpecificUser($request);
    }



    /**
     * Indicate current user can assign at least one of the given roles or not
     *
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssign(YasnaRequest $request): bool
    {
        $roles = (array)$request->input('role', '0');
        foreach ($roles as $role) {
            if ($request->model->canAssign($role)) {
                return true;
            }
        }

        return false;
    }



    /**
     * check if the assignment can be performed.
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canPerformAssignment(User $assignee, string $role, YasnaRequest $request): bool
    {
        return true;
        // override this in your controller to define some more conditions.
    }



    /**
     * revoke a user's access to a unity role
     *
     * @param UnityRoleRevokeRequest $request
     *
     * @return array
     */
    public function revoke(UnityRoleRevokeRequest $request)
    {
        $roles   = $request->getAllowedRoles();
        $results = [];

        foreach ($roles as $role) {
            $done = $request->model->revokeUserAs($request->user->id, $role);

            $results[] = [
                 "user"  => $request->user->hashid,
                 "role"  => $request->role,
                 "unity" => $request->unity,
                 "type"  => $request->model->type,
            ];
        }

        return $this->typicalSaveFeedback(true, $results); //<~~ Blind feedback!
    }



    /**
     * Return list of assigned roles.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function assignees(SimpleYasnaRequest $request)
    {
        /** @var UnityAbstract $model */
        $model = model($this->model_name)->grabHashid($request->unity);

        if (!$model->exists) {
            return $this->clientError(404);
        }
        if (!$model->canViewAssignedRoles()) {
            return $this->clientError(403);
        }

        $role                 = $request->input('role', 0);
        $direct_method        = camel_case(str_plural($role));

        $domain_direct_method = camel_case(sprintf("%s_%s", $request->input("type", ""), $direct_method));
        if ($request->input('history', false)) {
            $builder = $model->getEagerUsersWithTrash($request->input('role', 0))->getQuery();
            $users = $builder->get()->pluck('user.id')->toArray();
            $builder = user()->whereIn("id", $users);
        } elseif (method_exists($model, $domain_direct_method)) {
            $builder = $model->{$domain_direct_method}()->getQuery();
        } elseif (method_exists($model, $direct_method)) {
            $builder = $model->$direct_method();
        } else {
            $builder = $model->getEagerUsers($request->input('role', 0))->getQuery();
        }

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * get a list of assignees for multiple roles
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function multiRoleAssignees(SimpleYasnaRequest $request)
    {
        /** @var UnityAbstract $model */
        $model = model($this->model_name)->grabHashid($request->unity);

        if (!$model->exists) {
            return $this->clientError(404);
        }
        if (!$model->canViewAssignedRoles()) {
            return $this->clientError(403);
        }

        $with_trashed = $request->input('history', false);
        if (!$with_trashed) {
            $builder = RegistryUnityRole::assigneesOf($model->id, $request->role);
        } else {
            $builder = $model->getEagerUsersWithTrash($request->input('role', 0))->getQuery();
        }

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * perform assignment process when the user hashid is available
     *
     * @param UnityRoleAssignmentRequest $request
     *
     * @return array
     */
    private function assignSpecificUser(UnityRoleAssignmentRequest $request)
    {
        $errors  = 0;
        $results = [];
        $roles   = (array)$request->input('role', '0');

        foreach ($roles as $role) {
            //if(in_array($role,[ RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR, RegistryUnityRole::ROLE_OPU_SUPERVISOR ])) {
            //    continue;
            //}
            //if ($this->userAlreadyHasSpecialRoles($request->user) or !$this->canPerformAssignment($request->user, $role, $request)) {
            //    $errors++;
            //    continue;
            //}

            $done = $request->model->assignUserAs($request->user->id, $role);

            if ($done) {
                $request->user->setDefaultPassword();

                $results[] = [
                     "user"  => $request->user->hashid,
                     "role"  => $role,
                     "unity" => $request->unity,
                     "type"  => $request->model->type,
                ];
            }
        }

        if ($errors == count($roles)) {
            return $this->clientError("registry-114");
        }

        // to save BC with previous response on single role requests
        if (1 == count($results)) {
            $results = $results[0];
        }

        return $this->typicalSaveFeedback(true, $results);
    }



    /**
     * make sure users can hold one administrative role at a time
     *
     * @param User $user
     *
     * @return bool
     */
    private function userAlreadyHasSpecialRoles(User $user): bool
    {
        $special_roles = ['transplant-expert', 'opu-manager'];

        return array_intersect($user->unityRolesArray(), $special_roles) != null;
    }



    /**
     * try to find the user by their code_melli
     *
     * @param string $code_melli
     *
     * @return User
     */
    private function findUserByCodeMelli($code_melli)
    {
        return user()->findByUsername($code_melli);
    }



    /**
     * create or update the user row
     *
     * @param User                       $user
     * @param UnityRoleAssignmentRequest $request
     *
     * @return User
     */
    private function createOrUpdateUser(User $user, UnityRoleAssignmentRequest $request)
    {
        return $user->batchSave($request, ['unity', 'type', 'role', 'user']);
    }

}
