<?php

namespace Modules\Registry\Http\Controllers\V1\Abstracts;

use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Http\Requests\V1\UnityDeactivateRequest;
use Modules\Registry\Http\Requests\V1\UnityDeleteRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;

abstract class UnityBaseController extends YasnaApiController
{

    use AssignmentTrait;



    /**
     * list the unities, supporting search etc.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function list(SimpleYasnaRequest $request)
    {
        /** @var UnityAbstract $model */
        $model         = model($this->model_name);
        $elector_array = $this->listElectors($request);
        $builder       = $model->elector($elector_array)->with($model->eagerLoadedRelationships());

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * retrieve a single unity instance, by its hashid.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        /** @var UnityAbstract $model */
        $model = model($this->model_name)->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->clientError(404);
        }
        if (!$model->canView()) {
            return $this->clientError(403);
        }

        return $this->success($model->toSingleResource(), [
             "hashid" => $request->hashid,
             "type"   => $request->type,
        ]);
    }



    /**
     * delete a single unity instance, by its hashid.
     *
     * @param UnityDeleteRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(UnityDeleteRequest $request)
    {
        $done = $request->model->delete();
        if ($done) {
            $request->model->unityAssignedRoles()->delete();
        }

        return $this->typicalSaveFeedback($done, [
             "hashid" => $request->hashid,
             "type"   => $request->type,
        ]);
    }



    /**
     * activate/deactivate a single unity instance, by its hashid.
     *
     * @param UnityDeactivateRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deactivate(UnityDeactivateRequest $request)
    {
        $model = $request->model;

        if ($request->input('restore', false) == false) {
            $this->deactivateUnity($model);
        } else {
            $this->activateUnity($model);
        }

        return $this->typicalSaveFeedback(true, [
             "hashid" => $request->hashid,
             "type"   => $request->type,
        ]);
    }



    /**
     * list by pagination
     *
     * @param Builder            $builder
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated(Builder $builder, $request)
    {
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mappedArray($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * list all together
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat($builder)
    {
        $meta = [
             "total" => $builder->count(),
        ];
        $data = $this->mappedArray($builder->get());

        return $this->success($data, $meta);
    }



    /**
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection->map(function ($unity) {
            /** @var UnityAbstract $unity */
            return $unity->toListResource();
        })->toArray()
             ;

        //->where("_can_view", true)->toArray()
        //     ;
    }



    /**
     * get the elector required for the list
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function listElectors(SimpleYasnaRequest $request): array
    {
        return array_merge($this->listGeneralElectors($request), $this->listParticularElectors($request));
    }



    /**
     * get the general electors common in all unities
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    final protected function listGeneralElectors(SimpleYasnaRequest $request)
    {
        return [
             "name"     => $request->name,
             "city"     => $request->city,
             "province" => $request->province,
        ];
    }



    /**
     * get the particular electors required for one unity
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function listParticularElectors(SimpleYasnaRequest $request)
    {
        return []; //to be overridden when required
    }



    /**
     * Deactivate a unity and its sub unities.
     *
     * @param YasnaModel $model
     */
    protected function activateUnity($model)
    {
        $subUnities            = $this->unitiesHierarchy($model->type);
        $model->deactivated_at = null;
        $model->deactivated_by = null;
        if ($model->save() and !empty($subUnities)) {
            foreach ($subUnities as $unity) {
                model(studly_case(sprintf("registry-%s", $unity)))
                     ->where(sprintf("%s_id", $model->type), $model->id)
                     ->update([
                          'deactivated_at' => null,
                          'deactivated_by' => null,
                     ])
                ;
            }
        }
    }



    /**
     * Deactivate a unity and its sub unities.
     *
     * @param YasnaModel $model
     */
    protected function deactivateUnity($model)
    {
        $subUnities            = $this->unitiesHierarchy($model->type);
        $time                  = date('c');
        $model->deactivated_at = $time;
        $model->deactivated_by = user()->id;
        if ($model->save() and !empty($subUnities)) {
            foreach ($subUnities as $unity) {
                model(studly_case(sprintf("registry-%s", $unity)))
                     ->where(sprintf("%s_id", $model->type), $model->id)
                     ->update([
                          'deactivated_at' => $time,
                          'deactivated_by' => user()->id,
                     ])
                ;
            }
        }
    }



    /**
     * Return unities under a given unity
     *
     * @param string|null $key
     * @param bool        $all
     *
     * @return array|null
     */
    protected function unitiesHierarchy(?string $key, bool $all = false): ?array
    {
        $unities = [
             "university" => ["opu", "hospital", "ward"],
             "opu"        => ["hospital", "ward"],
             "hospital"   => ["ward"],
        ];

        if ($all) {
            return $unities;
        }

        if ($key and array_key_exists($key, $unities)) {
            return $unities[$key];
        }

        return null;
    }
}
