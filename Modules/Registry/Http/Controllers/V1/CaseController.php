<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryCase;
use function array_values;
use Modules\Registry\Http\Controllers\V1\Traits\CommonCaseAction;
use Modules\Registry\Http\Controllers\V1\Traits\PatientTransferActions;
use Modules\Registry\Http\Requests\V1\CaseFetchRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSaveHeartRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSaveIntestineRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSaveKidneyRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSaveLiverRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSaveLungRequest;
use Modules\Registry\Http\Requests\V1\CaseOrganSavePancreasRequest;
use Modules\Registry\Http\Requests\V1\CaseOrgansFetchRequest;
use Modules\Registry\Http\Requests\V1\CaseSaveRequest;
use Modules\Registry\Http\Requests\V1\CaseTrashRequest;
use Modules\Registry\Http\Requests\V1\SimilarCasesRequest;
use Modules\Yasna\Services\YasnaApiController;

class CaseController extends YasnaApiController
{
    use CommonCaseAction;
    use PatientTransferActions;

    protected $model_name = "registry_case";



    /**
     * save the case
     *
     * @param CaseSaveRequest $request
     *
     * @return array
     */
    public function save(CaseSaveRequest $request)
    {
        $overflow = array_merge(["blood"], $request->transferFields());

        /** @var RegistryCase $saved */
        $saved = $request->model->batchSave($request, $overflow);

        if ($saved->exists) {
            $saved->attachRequestFiles($request->toArray());
            $this->saveTransferToOpu($saved, $request);
        }


        return $this->modelSaveFeedback($saved);
    }



    /**
     * fetch a single row of the case, without demographics data (good for showing the edit page).
     *
     * @param CaseFetchRequest $request
     *
     * @return array
     */
    public function fetch(CaseFetchRequest $request)
    {
        return $this->success($request->model->toFetchResource());
    }



    /**
     * save organs, heart tab
     *
     * @param CaseOrganSaveHeartRequest $request
     *
     * @return array
     */
    public function saveOrganHeart(CaseOrganSaveHeartRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * save organs, lung tab
     *
     * @param CaseOrganSaveLungRequest $request
     *
     * @return array
     */
    public function saveOrganLung(CaseOrganSaveLungRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * save organs, Kidney tab
     *
     * @param CaseOrganSaveKidneyRequest $request
     *
     * @return array
     */
    public function saveOrganKidney(CaseOrganSaveKidneyRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * save organs, Liver tab
     *
     * @param CaseOrganSaveLiverRequest $request
     *
     * @return array
     */
    public function saveOrganLiver(CaseOrganSaveLiverRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * save organs, Intestine tab
     *
     * @param CaseOrganSaveIntestineRequest $request
     *
     * @return array
     */
    public function saveOrganIntestine(CaseOrganSaveIntestineRequest $request)
    {
        /** @var RegistryCase $saved */
        $saved = $request->model->batchSave($request);

        if ($saved->exists) {
            $saved->attachRequestFiles($request->toArray());
        }

        return $this->modelSaveFeedback($saved);
    }



    /**
     * save organs, Pancreas tab
     *
     * @param CaseOrganSavePancreasRequest $request
     *
     * @return array
     */
    public function saveOrganPancreas(CaseOrganSavePancreasRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * fetch case organs
     *
     * @param CaseOrgansFetchRequest $request
     *
     * @return array
     */
    public function fetchOrgans(CaseOrgansFetchRequest $request)
    {
        return $this->success($request->model->fetchOrgans(), [
             "case" => $request->model->hashid,
        ]);
    }



    /**
     * get the similar cases according to the given code_melli, name_first and name_last fields
     *
     * @param SimilarCasesRequest $request
     *
     * @return array
     */
    public function similarCases(SimilarCasesRequest $request)
    {
        $array = RegistryCase::getMappedSimilarCases($request->code_melli, $request->name_first, $request->name_last);

        return $this->success($array, [
             "code_melli" => $request->code_melli,
             "name_first" => $request->name_first,
             "name_last"  => $request->name_last,
             "total"      => count($array),
        ]);
    }



    /**
     * perform soft-delete action of the case.
     *
     * @param CaseTrashRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(CaseTrashRequest $request)
    {
        $done = $request->model->delete();

        return $this->typicalSaveFeedback($done);
    }
}
