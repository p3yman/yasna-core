<?php

namespace Modules\Registry\Http\Controllers\V1;

use Illuminate\Support\Facades\Hash;
use Modules\Registry\Http\Controllers\V1\Traits\UserHelpersTrait;
use Modules\Registry\Http\Requests\V1\UserChangePasswordRequest;
use Modules\Registry\Http\Requests\V1\ChangePasswordRequest;
use Modules\Registry\Http\Requests\V1\UserDeactivateRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class ActionOnUserAccountsController extends YasnaApiController
{
    use UserHelpersTrait;



    /**
     * Change password of a user
     *
     * @param UserChangePasswordRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordByAuthorizedUser(UserChangePasswordRequest $request)
    {
        $user = $request->getUser();
        if (!$user) {
            return $this->clientError("registry-106");
        }

        $new_password = $request->input('password');
        if (!$user->checkPasswordSeverity($new_password)) {
            return $this->clientError("registry-107");
        }

        $user->password              = Hash::make($new_password);
        $user->password_force_change = true; // if someone has changed my password, I must change it in first login
        if (!$user->save()) {
            return $this->clientError("registry-108");
        }

        $token_info = $this->regenerateTokenForNewPassword($user, $new_password, false);
        if (!$token_info) {
            return $this->clientError("registry-111");
        }

        return $this->success(['done' => true]);
    }



    /**
     * Change my account's password.
     *
     * @param ChangePasswordRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function resetMyPassword(ChangePasswordRequest $request)
    {
        $user         = user();
        $new_password = $request->input('password');

        $user->password              = Hash::make($new_password);
        $user->password_force_change = false;
        if (!$user->save()) {
            return $this->clientError("registry-108");
        }

        // invalidate current token and generate a new one
        $token_info = $this->regenerateTokenForNewPassword($user, $new_password);
        if (!$token_info) {
            return $this->clientError("registry-111");
        }

        return $this->success($token_info);
    }



    /**
     * Tries to fetch a requested username.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getUser(SimpleYasnaRequest $request)
    {
        // first tries to get user by hashid
        if ($request->filled('hashid')) {
            $user = user()->grabHashid($request->input('hashid', 0));
            if ($user->exists) {
                return $this->success($user->toFront());
            }
        }

        // then tries to get user by code melli
        if ($request->filled('code_melli')) {
            $user = user()->findByUsername($request->input('code_melli', ''));
            if ($user->exists) {
                return $this->success($user->toFront());
            }
        }

        // at end returns 404
        return $this->clientError(404);
    }



    /**
     * activate/deactivate a user, by its hashid.
     *
     * @param UserDeactivateRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deactivate(UserDeactivateRequest $request)
    {
        $model = $request->model;

        if ($request->input('restore', false) == false) {
            $time                  = date('c');
            $model->deactivated_at = $time;
            $model->deactivated_by = user()->id;
        } else {
            $model->deactivated_at = null;
            $model->deactivated_by = null;
        }

        return $this->typicalSaveFeedback($model->save(), [
             "hashid" => $request->hashid,
             "type"   => $request->type,
        ]);
    }
}
