<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryOpu;
use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Registry\Http\Controllers\V1\Abstracts\UnityBaseController;
use Modules\Registry\Http\Controllers\V1\Traits\CoordinatorAssignTrait;
use Modules\Registry\Http\Controllers\V1\Traits\FiltersBasedOnUserRoleTrait;
use Modules\Registry\Http\Controllers\V1\Traits\HarvestersAssignTrait;
use Modules\Registry\Http\Controllers\V1\Traits\InspectorsAssignTrait;
use Modules\Registry\Http\Controllers\V1\Traits\OpuManagerAssignTrait;
use Modules\Registry\Http\Controllers\V1\Traits\ProtectorsAssignTrait;
use Modules\Registry\Http\Requests\V1\OpuSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class OpuController
 *
 * @property RegistryOpu $model
 */
class OpuController extends UnityBaseController
{
    use FiltersBasedOnUserRoleTrait;
    use CoordinatorAssignTrait;
    use ProtectorsAssignTrait;
    use HarvestersAssignTrait;
    use InspectorsAssignTrait;
    use OpuManagerAssignTrait;


    protected $model_name = "registry-opu";



    /**
     * Saves an OPU.
     *
     * @param OpuSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(OpuSaveRequest $request)
    {
        //return $this->success($request->toArray());
        $saved_model = $request->model->batchSave($request, ["university", "hospitals",]);

        if ($saved_model) {
            $this->syncHospitals($saved_model, $request->hospitals);
        }

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * sync the hospital list
     *
     * @param RegistryOpu $opu
     * @param array       $ids
     */
    private function syncHospitals($opu, $ids)
    {
        if (is_null($ids)) {
            return;
        }

        $opu->hospitals()->update([
             "opu_id"     => "0",
             "updated_at" => now()->toDateTimeString(),
             "updated_by" => user()->id,
        ])
        ;

        model("registry-hospital")->whereIn("id", $ids)->update([
             "opu_id"     => $opu->id,
             "updated_at" => now()->toDateTimeString(),
             "updated_by" => user()->id,
        ])
        ;

        model("registry-ward")->whereIn("hospital_id", $ids)->update([
             "opu_id" => $opu->id,
        ])
        ;
    }



    /**
     * @inheritdoc
     */
    protected function canPerformAssignment(User $assignee, string $role, YasnaRequest $request): bool
    {
        if (str_contains($role, RegistryUnityRole::ROLE_COORDINATOR)) {
            $this->saveDonorCertificates($assignee, $request);

            return $this->canAssignCoordinators($assignee, $role, $request);
        }

        if (str_contains($role, RegistryUnityRole::ROLE_PROTECTOR)) {
            return $this->canAssignProtectors($assignee, $role, $request);
        }

        if (str_contains($role, RegistryUnityRole::ROLE_HARVESTER)) {
            return $this->canAssignHarvesters($assignee, $role, $request);
        }

        if (str_contains($role, "inspector")) {
            return $this->canAssignInspectors($assignee, $role, $request);
        }

        if ($role == RegistryUnityRole::ROLE_OPU_MANAGER) {
            $this->saveDonorCertificates($assignee, $request);

            return $this->canAssignOPUManager($assignee, $role, $request);
        }

        return true;
    }



    /**
     * @inheritdoc
     */
    protected function listParticularElectors(SimpleYasnaRequest $request)
    {
        //$all_available_universities = $this->authorizedUniversitiesOfUser(
        //     [
        //          RegistryUnityRole::ROLE_DONOR_EXPERT,
        //          RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
        //     ]
        //);

        return [
             "university"        => $request->university,
             "hasUnities"        => "university",
             //"limitByUniversity" => $all_available_universities,
        ];
    }



    /**
     * Save donor certificates of an assignee
     *
     * @param User         $assignee
     * @param YasnaRequest $request
     */
    protected function saveDonorCertificates(User $assignee, YasnaRequest $request)
    {
        if ($request->filled('iran_donor_certificate')) {
            $assignee->iran_donor_certificate = $request->input('iran_donor_certificate');
            $assignee->save();
        }
        if ($request->filled('international_donor_certificate')) {
            $assignee->international_donor_certificate = $request->input('international_donor_certificate');
            $assignee->save();
        }
    }
}
