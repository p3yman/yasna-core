<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\User;
use Modules\Registry\Http\Requests\V1\SuperadminDeleteRequest;
use Modules\Registry\Http\Requests\V1\SuperadminSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class SuperadminsController extends YasnaApiController
{
    /**
     * get a list of superadmins
     *
     * @return array
     */
    public function list()
    {
        $builder    = role("superadmin")->users()->orderBy("created_at", "desc");
        $total      = $builder->count();
        $collection = $builder->get();

        $mapped = $collection->map(function (User $user) {
            return $user->toSuperadminsListView();
        })->toArray()
        ;

        return $this->success($mapped, [
             "total" => $total,
        ]);
    }



    /**
     * get the resource of a single superadmin
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single()
    {
        /** @var User $user */
        $user = model("user")->grabHashid(request()->user);
        if (!$user->exists or $user->isNot('superadmin')) {
            return $this->clientError(404);
        }

        return $this->success($user->toSuperadminsSingleView(), ["done" => "1",]);
    }



    /**
     * save the superadmin (both on create and edit modes)
     *
     * @param SuperadminSaveRequest $request
     *
     * @return array
     */
    public function save(SuperadminSaveRequest $request)
    {
        $new_user = !$request->model->exists;

        /** @var User $saved */
        $saved = $request->model->batchSave($request);

        if ($saved and $new_user) {
            $saved->attachRole("superadmin");
        }

        return $this->modelSaveFeedback($saved);
    }



    /**
     * delete the superadmin
     *
     * @param SuperadminDeleteRequest $request
     *
     * @return array
     */
    public function delete(SuperadminDeleteRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->typicalSaveFeedback($deleted);
    }
}
