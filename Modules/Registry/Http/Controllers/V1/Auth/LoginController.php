<?php

namespace Modules\Registry\Http\Controllers\V1\Auth;

use App\Models\User;
use Modules\Yasna\Events\LoginFailed;
use Modules\Registry\Http\Requests\V1\LoginRequest;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Services\YasnaApiController;

class LoginController extends YasnaApiController
{


    /**
     * handle the login request
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|array
     */
    public function login(LoginRequest $request)
    {
        $token = auth('api')->attempt([
             "code_melli" => $request->username,
             "password"   => $request->password,
        ]);

        if (!$token) {
            event( new LoginFailed());
            return $this->clientError("registry-401");
        }

        if (auth('api')->user()->canNotLogin()) {
            event(new LoginFailed());
            return $this->clientError("registry-423");
        }

        event(new UserLoggedIn(auth('api')->user()));

        return $this->respondSuccessfulLogin(auth('api')->user(), $token);
    }



    /**
     * respond in case of a successful login
     *
     * @param User   $user
     * @param string $token
     */
    protected function respondSuccessfulLogin(User $user, string $token)
    {
        $user_array = $user->toFront();
        $token_info = [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ];

        return $this->success($user_array, $token_info);
    }
}
