<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryUniversity;
use Modules\Registry\Entities\RegistryOpu;
use Modules\Yasna\Services\YasnaApiController;

class FulfilmentController extends YasnaApiController
{
    /**
     * keep the university of which the expert belongs to
     *
     * @var null|RegistryUniversity
     */
    private $user_university = null;

    /**
     * keep the opu of which the user belongs to
     *
     * @var null|RegistryOpu
     */
    private $user_opu = null;



    /**
     * check the fulfilment status of the current user
     *
     * @return array
     */
    public function check()
    {
        $checks  = $this->getChecklist();
        $results = [];
        $passed  = 0;

        foreach ($checks as $check) {
            $method = camel_case("check-$check");
            $result = false;
            if (static::hasMethod($method)) {
                $result = (bool)$this->$method();
                $passed += $result;
            }

            $results[$check] = $result;
        }

        return $this->success($results, [
             "total_checks" => count($checks),
             "total_passed" => $passed,
             "total_failed" => count($checks) - $passed,
        ]);
    }



    /**
     * check if the superadmin has defined at least one university
     *
     * @return bool
     */
    protected function checkSuperRegisterOneUniversity(): bool
    {
        return boolval(model('registry-university')->count());
    }



    /**
     * check if the superadmin has assigned at least one expert
     *
     * @return bool
     */
    protected function checkSuperRegisterOneExpert(): bool
    {
        $roles = ['expert-in-charge', 'donor-expert'];
        $count = model('registry-unity-role')->whereIn('role', $roles)->count();

        return boolval($count);
    }



    /**
     * check if the expert has set all university settings
     *
     * @return bool
     */
    protected function checkExpertUniversitySettings(): bool
    {
        $university = $this->getUserUniversity();

        return $university->isComplete();
    }



    /**
     * check if the expert has set a president for their university
     *
     * @return bool
     */
    protected function checkExpertUniversityPresident(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->presidents()->count());
    }



    /**
     * check if the expert has set a deputy for their university
     *
     * @return bool
     */
    protected function checkExpertUniversityDeputy(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->deputies()->count());
    }



    /**
     * check if the expert has set donor expert for their university
     *
     * @return bool
     */
    protected function checkExpertSubs(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->donorExperts()->count());
    }



    /**
     * check if the expert has set Neurologist seconder for their university
     *
     * @return bool
     */
    protected function checkExpertOneNeurologistSeconder(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->neurologistSeconders()->count());
    }



    /**
     * check if the expert has set Neurosurgeons seconder for their university
     *
     * @return bool
     */
    protected function checkExpertOneNeurosurgeonsSeconder(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->neurosurgeonsSeconders()->count());
    }



    /**
     * check if the expert has set internist seconder for their university
     *
     * @return bool
     */
    protected function checkExpertOneInternistSeconder(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->internistSeconders()->count());
    }



    /**
     * check if the expert has set at least one opu for their university
     *
     * @return bool
     */
    protected function checkExpertOneOpu(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->opus()->count());
    }



    /**
     * check if the expert has assigned at least one opu manager for their university
     *
     * @return bool
     */
    protected function checkExpertOneOpuManager(): bool
    {
        $university = $this->getUserUniversity();

        /** @var RegistryOpu $opu */
        foreach ($university->opus as $opu) { // <~~ Better to solve this by query unions, but these doesn't work on relationships.
            if ($opu->opuManagers()->count()) {
                return true;
            }
        }

        return false;
    }



    /**
     * check if the expert has set at least one hospital for their university
     *
     * @return bool
     */
    protected function checkExpertOneHospital(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->hospitals()->count());
    }



    /**
     * check if the expert has set at least one ward for their university
     *
     * @return bool
     */
    protected function checkExpertOneWard(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->wards()->count());
    }



    /**
     * check if the expert has set anesthesiologist seconder for their university
     *
     * @return bool
     */
    protected function checkExpertOneAnesthesiologistSeconder(): bool
    {
        $university = $this->getUserUniversity();

        return boolval($university->anesthesiologistSeconders()->count());
    }



    /**
     * check if the opu manager has set all opu settings
     *
     * @return bool
     */
    protected function checkOpuSettings(): bool
    {
        $opu = $this->getUserOpu();

        return $opu->isComplete();
    }



    /**
     * check if the opu manager has set one of `clinical_detection` or `telephone_detection`
     *
     * @return bool
     */
    protected function checkOpuType(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->clinical_detection) or boolval($opu->telephone_detection);
    }



    /**
     * check if the opu manager has assigned at least one consultant
     *
     * @return bool
     */
    protected function checkOpuOneConsultant(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->consultants()->count());
    }



    /**
     * check if the opu manager has assigned at least one legal medicine
     *
     * @return bool
     */
    protected function checkOpuOneLegalMedicine(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->legalMedicines()->count());
    }



    /**
     * check if the opu manager has assigned at least one chief legal medicine
     *
     * @return bool
     */
    protected function checkOpuOneChiefLegalMedicine(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->chiefLegalMedicines()->count());
    }



    /**
     * check if the opu manager has assigned at least one coordinator
     *
     * @return bool
     */
    protected function checkOpuOneCoordinator(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->coordinators()->count());
    }



    /**
     * check if the opu manager has assigned at least one chief coordinator
     *
     * @return bool
     */
    protected function checkOpuOneChiefCoordinator(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->chiefCoordinators()->count());
    }



    /**
     * check if the opu manager has assigned at least one inspector
     *
     * @return bool
     */
    protected function checkOpuOneInspector(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->inspectorClinicals()->count() + $opu->inspectorTels()->count());
    }



    /**
     * check if the opu manager has assigned at least one inspector
     *
     * @return bool
     */
    protected function checkProtectors(): bool
    {
        $opu = $this->getUserOpu();

        return boolval($opu->chiefProtectors()->count()) and boolval($opu->protectors()->count());
    }



    /**
     * get the university of which the expert belongs to
     *
     * @return RegistryUniversity
     */
    private function getUserUniversity()
    {
        if ($this->user_university) {
            return $this->user_university;
        }

        $role   = user()->detailedUnityRoles()->whereIn('role', ['expert-in-charge', 'donor-expert'])->first();
        $hashid = 0;
        if ($role) {
            $hashid = $role['unity'];
        }

        return model('registry-university', $hashid);
    }



    /**
     * get the opu of which the user belongs to
     *
     * @return RegistryOpu
     */
    private function getUserOpu()
    {
        if ($this->user_opu) {
            return $this->user_opu;
        }

        $role   = user()->detailedUnityRoles()->where('role', 'opu-manager')->first();
        $hashid = 0;
        if ($role) {
            $hashid = $role['unity'];
        }

        return model('registry-opu', $hashid);
    }



    /**
     * get the fulfilment checklist according to the user's role(s)
     *
     * @return array
     */
    private function getChecklist()
    {
        $array = [];
        $roles = user()->unityRolesArray();

        if (user()->isSuper()) {
            $array = array_merge($array, [
                 "super_register_one_university",
                 "super_register_one_expert",
            ]);
        }

        if (in_array("expert-in-charge", $roles)) {
            $array = array_merge($array, [
                 "expert_university_settings",
                 "expert_university_president",
                 "expert_university_deputy",
                 "expert_subs",
                 "expert_one_neurologist_seconder",
                 "expert_one_neurosurgeons_seconder",
                 "expert_one_internist_seconder",
                 "expert_one_anesthesiologist_seconder",
                 "expert_one_opu",
                 "expert_one_opu_manager",
                 "expert_one_hospital",
                 "expert_one_ward",
            ]);
        }

        if (in_array("opu-manager", $roles)) {
            $array = array_merge($array, [
                 "opu_settings",
                 "opu_one_consultant",
                 "opu_one_legal_medicine",
                 "opu_one_chief_legal_medicine",
                 "opu_one_coordinator",
                 "opu_one_chief_coordinator",
                 "opu_one_inspector",
            ]);
        }

        return $array;
    }
}
