<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\User;
use Illuminate\Support\Collection;
use Modules\Registry\Http\Requests\V1\SuperadminAssignSupervisorRequest;
use Modules\Yasna\Services\YasnaApiController;

class SupervisorsController extends YasnaApiController
{
    /**
     * Assigns the requested user as the supervisor of the requested universities.
     *
     * @param SuperadminAssignSupervisorRequest $request
     *
     * @return array
     */
    public function assignBySuperadmin(SuperadminAssignSupervisorRequest $request)
    {
        $universities_models = $this->getUniversitiesModelByIds($request->universities);


        if ($request->askedForSpecificUser()) {
            return $this->assignSpecificUser($request->user, $universities_models);
        }


        $user = $this->findUserByCodeMelli($request->code_melli);
        $user = $this->createOrUpdateUser($user, $request);

        return $this->assignSpecificUser($user, $universities_models);
    }



    /**
     * Assigns a specific user as a supervisor of the given universities models.
     *
     * @param User       $user
     * @param Collection $universities_models
     *
     * @return array
     */
    protected function assignSpecificUser(User $user, Collection $universities_models)
    {
        foreach ($universities_models as $university_model) {
            $university_model->assignUserAs($user->id, 'university-supervisor');
        }

        return $this->success([
             'done' => 1,
        ], [
             'user'         => $user->hashid,
             'universities' => $universities_models->pluck('hashid')->toArray(),
        ]);
    }



    /**
     * Tries to find the user by its code_melli.
     *
     * @param string $code_melli
     *
     * @return User
     */
    private function findUserByCodeMelli($code_melli)
    {
        return user()->findByUsername($code_melli);
    }



    /**
     * Creates or updates the user record.
     *
     * @param User                              $user
     * @param SuperadminAssignSupervisorRequest $request
     *
     * @return User
     */
    private function createOrUpdateUser(User $user, SuperadminAssignSupervisorRequest $request)
    {
        return $user->batchSave($request, ['universities']);
    }



    /**
     * Creates an array of `App\Models\RegistryUniversity` instances with their IDs and returns it.
     *
     * @param array $ids
     *
     * @return Collection
     */
    protected function getUniversitiesModelByIds(array $ids)
    {
        return collect($ids)
             ->map(function (int $id) {
                 return model('registry-university', $id);
             });
    }
}
