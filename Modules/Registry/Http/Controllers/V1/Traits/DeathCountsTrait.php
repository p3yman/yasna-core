<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;


use Illuminate\Support\Collection;
use Modules\Yasna\Services\YasnaModel;
use RuntimeException;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Trait DeathCountsTrait Add ability of save/process of death counts to controllers.
 *
 * @package Modules\Registry\Http\Controllers\V1\Traits
 */
trait DeathCountsTrait
{
    /**
     * Save death counts for a given requested model. Given death counts from a request must be in `[97 => 235]`
     * format, which the key is the year and the value is the number of deaths.
     *
     * @param YasnaRequest $request
     * @param YasnaModel   $model
     */
    private function saveDeathCounts(YasnaRequest $request, YasnaModel $model)
    {
        if (!method_exists($request, 'getDeathCounts')) {
            throw new RuntimeException("Request class must implement a `getDeathCounts` method.");
        }

        $death = new Collection($request->getDeathCounts());
        $in_db = $model->death_counts;
        if (!$in_db) {
            $in_db = [];
        }

        foreach ($in_db as $death_model) {
            if ($death->contains('year', $death_model->year)) {
                $ys = $death->firstWhere('year', $death_model->year); // year statistics
                if ($death_model->death_counts != $ys['counts'] or $death_model->icu_death_counts != $ys['icu_counts']) {
                    $death_model->death_counts     = $ys['counts'];
                    $death_model->icu_death_counts = $ys['icu_counts'];
                    $death_model->save();
                }

                $death = $death->where('year', '<>', $death_model->year);
            } else {
                $death_model->delete();
            }
        }

        $death->every(function ($stat) use ($model) {
            $death_model                   = model('RegistryUnityDeathCount');
            $death_model->unity_id         = $model->id;
            $death_model->year             = $stat['year'];
            $death_model->death_counts     = $stat['counts'];
            $death_model->icu_death_counts = $stat['icu_counts'];

            return $death_model->save();
        });
    }



    /**
     * get a ready to display array of death counts of a model.
     *
     * @param YasnaModel $model
     *
     * @return array
     */
    private function getDeathCountsArray(YasnaModel $model): array
    {
        if (is_null($model->death_counts)) {
            return [];
        }


        $death = $model->death_counts
             ->map(function ($item) {
                 return [
                      'year'   => $item->year,
                      'counts' => $item->death_counts,
                 ];
             })
             ->toArray()
        ;

        return $death;
    }
}
