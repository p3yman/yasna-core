<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;


use App\Models\RegistryCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Registry\Http\Requests\V1\GetCaseListRequest;
use Modules\Registry\Http\Requests\V1\GetCaseSingleRequest;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Trait CommonCaseAction add some common actions to controller.
 *
 * @package Modules\Registry\Http\Controllers\V1\Traits
 */
trait CommonCaseAction
{
    use SortTrait;



    /**
     * list the cases, supporting search etc.
     *
     * @param GetCaseListRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function list(GetCaseListRequest $request)
    {
        /** @var RegistryCase $model */
        $model         = model($this->model_name);
        $elector_array = $this->listElectors($request);
        $builder       = $model->elector($elector_array)->with(RegistryCase::requiredRelations());
        $this->applyRequestedSort($builder, $request);

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * retrieve a single unity instance, by its hashid.
     *
     * @param GetCaseSingleRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single(GetCaseSingleRequest $request)
    {
        return $this->success($request->model->toDemographicsArray(), [
             "hashid" => $request->hashid,
             "type"   => $request->type,
        ]);
    }



    /**
     * list by pagination
     *
     * @param Builder            $builder
     * @param GetCaseListRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated(Builder $builder, $request)
    {
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mappedArray($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * list all together
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat(Builder $builder)
    {
        $meta = [
             "total" => $builder->count(),
        ];
        $data = $this->mappedArray($builder->get());

        return $this->success($data, $meta);
    }



    /**
     * Show a representation of a given RegistryCase model
     *
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection->map(function ($case) {
            /** @var RegistryCase $case */
            return $case->toListResource();
        })->toArray()
             ;
    }



    /**
     * get the elector required for the list
     *
     * @param YasnaRequest $request
     *
     * @return array
     */
    protected function listElectors(YasnaRequest $request): array
    {
        return array_merge(
             $this->requestBasedElectors($request),
             $this->roleBasedElectors()
        );
    }



    /**
     * Electors for filter results of list based on request
     *
     * @param YasnaRequest $request
     *
     * @return array
     */
    protected function requestBasedElectors(YasnaRequest $request): array
    {
        $allowed_filter = [
             'q',
             'code_melli',
             'name_first',
             'name_last',
             'file_no',
             'gender',
             'name',
             'university',
             'opu',
             'hospital',
             'ward',
             'inspector',
             'coordinator',
             'case_situation',
             'identification_type',
             'consciousness_disorder_cause',
        ];

        return collect($request->all())
             ->filter(function ($value, $key) use ($allowed_filter) {
                 return in_array($key, $allowed_filter);
             })
             ->toArray()
             ;
    }



    /**
     * Electors which filled based on current user role
     *
     * @return array
     */
    protected function roleBasedElectors(): array
    {
        if (user()->isSuperadmin()) {
            return [];
        }

        return user()->unityRoles
             ->map(function ($role) {
                 return [
                      'id'   => $role->unity->id,
                      'type' => $role->unity->type,
                 ];
             })
             ->pluck('id', 'type')
             ->toArray()
             ;
    }
}
