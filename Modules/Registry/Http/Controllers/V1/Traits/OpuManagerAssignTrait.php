<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

trait OpuManagerAssignTrait
{
    /**
     * check if the role assignment can be performed on the opu manager role
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignOPUManager(User $assignee, string $role, YasnaRequest $request): bool
    {
        // From AC: A user can be OPU manager of one OPU at a time
        return $this->alreadyIsNotAnOPUManager($assignee);
    }



    /**
     * Indicate the requested assignee is already an opu manager or not
     *
     * @param User $assignee
     *
     * @return bool
     */
    protected function alreadyIsAnOPUManager(User $assignee): bool
    {
        return in_array(RegistryUnityRole::ROLE_OPU_MANAGER, $assignee->unityRolesArray());
    }



    /**
     * Indicate the requested assignee is not already an opu manager
     *
     * @param User $assignee
     *
     * @return bool
     */
    protected function alreadyIsNotAnOPUManager(User $assignee): bool
    {
        return !$this->alreadyIsAnOPUManager($assignee);
    }
}
