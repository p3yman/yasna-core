<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Trait SortTrait add ability of sorting on the query based on a given request.
 *
 * @package Modules\Registry\Http\Controllers\V1\Traits
 */
trait SortTrait
{
    /**
     * This method just prepare data to be applied sorts on them based on a given request.
     *
     * @param Builder      $builder
     * @param YasnaRequest $request
     */
    public function applyRequestedSort(Builder $builder, YasnaRequest $request)
    {
        $not_allowed_array = [];
        $allowed_array     = [];
        $sorts             = $request->input('sort', []);
        $maps              = [];

        if (!is_null($request->model) and method_exists($request->model, 'notSortableFields')) {
            $not_allowed_array = $request->model::notSortableFields();
        }

        if (!is_null($request->model) and method_exists($request->model, 'sortableFields')) {
            $allowed_array = $request->model->sortableFields();
        }

        if (!is_null($request->model) and method_exists($request->model, 'mappedSortFields')) {
            $maps = $request->model::mappedSortFields();
        }

        $sorts = $this->getSortableFields($sorts, $maps, $not_allowed_array, $allowed_array);

        $this->applySorts($builder, $sorts);
    }



    /**
     * Apply sort modifications on a given builder based on a given set.
     *
     * @param Builder $builder
     * @param array   $sorts
     */
    public function applySorts(Builder $builder, array $sorts)
    {
        foreach ($sorts as $column => $direction) {
            $this->applySort($builder, $column, $direction);
        }
    }



    /**
     * Apply sort on a given column.
     *
     * @param Builder $builder
     * @param string  $column
     * @param string  $type
     */
    private function applySort(Builder $builder, string $column, string $type = 'asc')
    {
        if (!$column) {
            return;
        }

        $builder->orderBy($column, $type);
    }



    /**
     * Get type of sort for a given column.
     *
     * @param string $column
     *
     * @return string
     */
    private function getTypeOfSort(string $column): string
    {
        return $column[0] == '-' ? 'desc' : 'asc';
    }



    /**
     * Remove the `-` from column name and if it's mapped convert to original DB columns.
     *
     * @param string $column
     * @param array  $maps
     *
     * @return array
     */
    private function getColumnName(string $column, array $maps): array
    {
        $column = $column[0] == '-' ? substr($column, 1) : $column;

        if (!empty($maps) and in_array($column, array_keys($maps))) {
            return explode('|', $maps[$column]);
        }

        return [$column];
    }



    /**
     * The given set (`$sorts`) contains some fields
     * name and maybe a `-` sign in front of them (for example `['name', '-salary']` which the `-` sign means descending
     * direction sort for that field. Also there can be a allowed fields which if it has been given with values, the
     * sorting process applied for a field, only when that field is on the allowed fields.
     *
     * @param array $sorts
     * @param array $maps
     * @param array $not_allowed_array
     * @param array $allowed_array
     *
     * @return array
     */
    private function getSortableFields(array $sorts, array $maps, array $not_allowed_array, array $allowed_array): array
    {
        $results = [];

        foreach ($sorts as $sort) {
            $direction = $this->getTypeOfSort($sort);
            $columns   = $this->getColumnName($sort, $maps);

            foreach ($columns as $column) {
                if ($this->columnMustBeIgnored($column, $not_allowed_array, $allowed_array)) {
                    continue;
                }

                $results[$column] = $direction;
            }
        }

        return $results;
    }



    /**
     * Checks the column must be ignored or not
     *
     * @param string $column
     * @param array  $not_allowed_array
     * @param array  $allowed_array
     *
     * @return bool
     */
    private function columnMustBeIgnored(string $column, array $not_allowed_array, array $allowed_array): bool
    {
        if (
             (!empty($not_allowed_fields) and in_array($column, $not_allowed_fields))
             or
             (!empty($allowed_fields) and not_in_array($column, $allowed_fields))
        ) {
            return true;
        }

        return false;
    }
}
