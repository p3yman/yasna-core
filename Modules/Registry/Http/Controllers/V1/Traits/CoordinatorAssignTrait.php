<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

trait CoordinatorAssignTrait
{
    /**
     * check if the role assignment can be performed on the coordinators roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignCoordinators(User $assignee, string $role, YasnaRequest $request): bool
    {
        if (str_contains($role, "chief-")) {
            return $this->canAssignChiefCoordinator($assignee, $role, $request);
        }

        if ($role == RegistryUnityRole::ROLE_COORDINATOR) {
            return $this->canAssignCoordinator($assignee, $role);
        }

        return false;
    }



    /**
     * check if assignment of a chief coordinator is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignChiefCoordinator(User $assignee, string $role, YasnaRequest $request): bool
    {
        return $assignee->hasUnityRole($request->model->id, RegistryUnityRole::ROLE_COORDINATOR);
    }



    /**
     * check if assignment of a coordinator is possible
     *
     * @param User   $assignee
     * @param string $role
     *
     * @return bool
     */
    protected function canAssignCoordinator(User $assignee, string $role): bool
    {
        return true; // now no special validation is required
    }
}
