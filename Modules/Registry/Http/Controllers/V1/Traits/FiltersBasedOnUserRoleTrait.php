<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

/**
 * Trait FiltersBasedOnUserRoleTrait add filters based on user role.
 *
 * @package Modules\Registry\Http\Controllers\V1\Traits
 */
trait FiltersBasedOnUserRoleTrait
{
    /**
     * Get a list of all universities which current user can view.
     *
     * @param string|array $roles List of roles which user must have to see the list.
     *
     * @return array|null
     */
    private function authorizedUniversitiesOfUser($roles)
    {
        $roles = (array)$roles;

        if (user()->isSuper()) {
            return null;
        }

        $universities = user()->getEagerUniversities($roles)
                              ->get()
                              ->pluck('id')
                              ->toArray()
        ;

        return $universities;
    }
}
