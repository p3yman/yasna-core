<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 11/26/18
 * Time: 12:42 PM
 */

namespace Modules\Registry\Http\Controllers\V1\Traits;


use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Trait UserHelpersTrait add ability of working with a user model.
 *
 * @package Modules\Registry\Http\Controllers\V1\Traits
 */
trait UserHelpersTrait
{
    /**
     * Fill information of a user model from a given request. If any field has
     * been changed, it returns true, otherwise it returns false.
     *
     * @param User         $user
     * @param YasnaRequest $request
     *
     * @return bool Detect the user model has been changed or not.
     */
    private function fillUserModelFromRequest(
         User $user,
         YasnaRequest $request
    ): bool {
        $changed = false;

        if ($request->filled('code_melli')) {
            $user->code_melli = $request->input('code_melli');
            $changed          = true;
        }
        if ($request->filled('first_name')) {
            $user->name_first = $request->input('first_name');
            $changed          = true;
        }
        if ($request->filled('last_name')) {
            $user->name_last = $request->input('last_name');
            $changed         = true;
        }
        if ($request->filled('mobile')) {
            $user->mobile = $request->input('mobile');
            $changed      = true;
        }
        if ($request->filled('gender')) {
            $user->gender = $request->input('gender');
            $changed      = true;
        }
        if ($request->filled('expertise')) {
            $user->expertise = $request->input('expertise');
            $changed         = true;
        }
        if ($request->filled('edu_level')) {
            $user->edu_level = $request->input('edu_level');
            $changed         = true;
        }
        if ($request->filled('job_history')) {
            $user->job_history = $request->input('job_history');
            $changed           = true;
        }
        if ($request->filled('email')) {
            $user->email = $request->input('email');
            $changed     = true;
        }
        if ($request->filled('tel')) {
            $user->tel = $request->input('tel');
            $changed   = true;
        }
        if ($request->filled('birth_date')) {
            $user->birth_date = $request->input('birth_date');
            $changed          = true;
        }
        if ($request->filled('latest_job_position')) {
            $user->latest_job_position = $request->input('latest_job_position');
            $changed                   = true;
        }
        if ($request->filled('is_retired')) {
            $user->is_retired = $request->input('is_retired');
            $changed          = true;
        }
        if ($request->filled('service_location')) {
            $user->service_location = $request->input('service_location');
            $changed                = true;
        }
        if ($request->filled('second_job')) {
            $user->second_job = $request->input('second_job');
            $changed          = true;
        }

        return $changed;
    }



    /**
     * Return representational information of the given user
     *
     * @param \App\Models\User $user
     *
     * @return array
     */
    private function getUserInfoArray($user): array
    {
        return [
             'hashid'              => $user->hashid,
             'code_melli'          => $user->code_melli,
             'name_first'          => $user->name_first,
             'name_last'           => $user->name_last,
             'mobile'              => $user->mobile,
             'gender'              => $user->gender,
             'expertise'           => $user->expiertise,
             'edu_level'           => $user->edu_level,
             'job_history'         => $user->job_history,
             'email'               => $user->email,
             'tel'                 => $user->tel,
             'birth_date'          => $this->getTimestampFromDateString($user->birth_date),
             'latest_job_position' => $user->latest_job_position,
             'is_retired'          => $user->is_retired,
             'service_location'    => $user->service_location,
             'second_job'          => $user->second_job,
        ];
    }



    /**
     * Return a unix timestamp from a given date string
     *
     * @param null|string $date
     *
     * @return int|null
     */
    private function getTimestampFromDateString(?string $date): ?int
    {
        if (is_null($date)) {
            return null;
        }

        return strtotime($date);
    }



    /**
     * Generate a new token on setting a new password.
     *
     * @param User   $user
     * @param string $password
     * @param bool   $invalidate
     *
     * @return array|null
     */
    private function regenerateTokenForNewPassword(User $user, string $password, bool $invalidate = true): ?array
    {
        if ($invalidate) {
            JWTAuth::parseToken()->invalidate(true);
        }
        $token = auth('api')->attempt([
             "code_melli" => $user->username,
             "password"   => $password,
        ]);
        if (!$token) {
            return null;
        }

        $token_info = [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ];

        return $token_info;
    }
}
