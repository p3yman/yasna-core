<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryCase;
use App\Models\RegistryPatientTransfer;
use Illuminate\Database\Eloquent\Builder;
use Modules\Registry\Http\Requests\V1\CaseSaveRequest;
use Modules\Yasna\Services\YasnaRequest;
use Modules\Registry\Http\Requests\V1\PatientTransferSaveRequest;
use Modules\Registry\Http\Requests\V1\GetPatientTransferListRequest;

trait PatientTransferActions
{
    /**
     * List transfer records of a requested case
     *
     * @param GetPatientTransferListRequest $request
     *
     * @return mixed
     */
    public function listPatientTransfers(GetPatientTransferListRequest $request)
    {
        /** @var Builder $model */
        $builder = model('RegistryPatientTransfer')->newBuilderInstance();
        $builder = $builder
             ->where("case_id", $request->model->id)
             ->with(RegistryPatientTransfer::requiredRelations());

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * save a patient transfer request
     *
     * @param PatientTransferSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function savePatientTransfer(PatientTransferSaveRequest $request)
    {
        $overflow = ['case', 'city', 'hospital'];

        /* @var RegistryPatientTransfer $transfer */
        $transfer = $request->model->batchSave($request, $overflow);
        if ($transfer->isConfirmed()) {
            $transfer = $transfer->transferToNewHospital();
        }

        return $this->modelSaveFeedback($transfer);
    }



    /**
     * save transfer to opu, which is submitted via the case editor form
     *
     * @param RegistryCase    $case
     * @param CaseSaveRequest $request
     *
     * @return bool
     */
    private function saveTransferToOpu($case, $request)
    {
        /*-----------------------------------------------
        | bypass ...
        */
        if (!$request->transfer_to_opu) {
            return false;
        }

        /*-----------------------------------------------
        | Save ...
        */
        $saved = model("RegistryPatientTransfer")->batchSave([
             "case_id"                   => $case->id,
             "city_id"                   => $request->destination_hospital->city_id,
             "province_id"               => $request->destination_hospital->province_id,
             "origin_hospital_id"        => $case->hospital_id,
             "destination_hospital_id"   => $request->destination_hospital_id,
             "origin_ward_id"            => $case->ward_id,
             "destination_ward_id"       => (int)$request->destination_ward_id,
             "transfer_reason"           => "transmission-reason-transmission-to-opu",
             "status"                    => RegistryPatientTransfer::STATUS_CONFIRMED,
             "transferred_at"            => $request->transferred_at,
             "chief_protector_name"      => $request->chief_protector_name,
             "chief_protector_degree"    => $request->chief_protector_degree,
             "escorting_cooperator_name" => $request->escorting_cooperator_name,
        ]);

        if ($saved->exists) {
            $case->batchSave([
                 "hospital_id" => $request->destination_hospital_id,
                 "ward_id"     => $request->destination_ward_id,
            ]);
        }

        return $saved->exists;
    }



    /**
     * filter transfer based on request
     *
     * @param GetPatientTransferListRequest $request
     *
     * @return array
     */
    private function patientTransferElectors(GetPatientTransferListRequest $request): array
    {
        return [
             'case' => $this->getCaseID($request),
        ];
    }



    /**
     * Convert a given hashid to case_id.
     *
     * @param YasnaRequest|string|int $hashid
     *
     * @return int|null
     */
    private function getCaseID($hashid): ?int
    {
        return $this->dehash($hashid, 'case');
    }



    /**
     * Convert a given hashid to hospital_id.
     *
     * @param YasnaRequest|string|int $hashid
     *
     * @return int|null
     */
    private function getHospitalID($hashid): ?int
    {
        return $this->dehash($hashid, 'hospital');
    }



    /**
     * Convert a given hashid to id. If the given parameter is integer it returns null.
     *
     * @param YasnaRequest|string|int $hashid
     * @param string|null             $field
     *
     * @return int|null
     */
    private function dehash($hashid, $field = null): ?int
    {
        if ($hashid instanceof YasnaRequest) {
            $hashid = $hashid->{$field};
        }

        if (!$hashid or $hashid == (string)intval($hashid)) {
            return null;
        }

        return hashid($hashid);
    }
}
