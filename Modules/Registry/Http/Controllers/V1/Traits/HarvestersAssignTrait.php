<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

trait HarvestersAssignTrait
{
    /**
     * check if the role assignment can be performed on the harvester roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignHarvesters(User $assignee, string $role, YasnaRequest $request): bool
    {
        if ($role == RegistryUnityRole::ROLE_HARVESTER) {
            return $this->canAssignHarvester($assignee, $role, $request);
        }

        return false;
    }



    /**
     * check if assignment of a harvester is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignHarvester(User $assignee, string $role, YasnaRequest $request): bool
    {
        return $assignee->hasUnityRole($request->model->university->id, $role);
    }
}
