<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

trait InspectorsAssignTrait
{
    /**
     * check if the role assignment can be performed on the inspector roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignInspectors(User $assignee, string $role, YasnaRequest $request): bool
    {
        if ($role == RegistryUnityRole::ROLE_INSPECTOR_TEL) {
            return $this->canAssignInspectorTel($assignee, $role, $request);
        }

        if ($role == RegistryUnityRole::ROLE_INSPECTOR_CLINICAL) {
            return $this->canAssignInspectorClinical($assignee, $role, $request);
        }

        return false;
    }



    /**
     * check if assignment of a telephone inspector is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignInspectorTel(User $assignee, string $role, YasnaRequest $request): bool
    {
        return not_in_array(RegistryUnityRole::ROLE_INSPECTOR_TEL, $assignee->unityRolesArray());
    }



    /**
     * check if assignment of a clinical inspector is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignInspectorClinical(User $assignee, string $role, YasnaRequest $request): bool
    {
        return not_in_array(RegistryUnityRole::ROLE_INSPECTOR_CLINICAL, $assignee->unityRolesArray());
    }
}
