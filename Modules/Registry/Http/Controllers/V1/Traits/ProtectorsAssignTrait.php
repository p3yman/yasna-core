<?php

namespace Modules\Registry\Http\Controllers\V1\Traits;

use App\Models\RegistryUnityRole;
use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

trait ProtectorsAssignTrait
{
    /**
     * check if the role assignment can be performed on the protector roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignProtectors(User $assignee, string $role, YasnaRequest $request): bool
    {
        if (str_contains($role, "chief-")) {
            return $this->canAssignChiefProtector($assignee, $role, $request);
        }

        if ($role == RegistryUnityRole::ROLE_PROTECTOR) {
            return $this->canAssignProtector($assignee, $role, $request);
        }

        return false;
    }



    /**
     * check if assignment of a head protector is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignChiefProtector(User $assignee, string $role, YasnaRequest $request): bool
    {
        // the protector only can be one of added protector
        return $assignee->hasUnityRole($request->model->id, RegistryUnityRole::ROLE_PROTECTOR);
    }



    /**
     * check if assignment of a protector is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    protected function canAssignProtector(User $assignee, string $role, YasnaRequest $request): bool
    {
        return true; // there is no special role for becoming a protector!
    }
}
