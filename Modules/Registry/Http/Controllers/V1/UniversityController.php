<?php

namespace Modules\Registry\Http\Controllers\V1;

use App\Models\RegistryUniversity;
use App\Models\User;
use Modules\Registry\Http\Controllers\V1\Abstracts\UnityBaseController;
use Modules\Registry\Http\Requests\V1\UniversitySaveRequest;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UniversityController
 *
 * @package Modules\Registry\Http\Controllers\V1
 */
class UniversityController extends UnityBaseController
{
    protected $model_name = "registry-university";



    /**
     * save the university from, requested by the superadmin
     *
     * @param UniversitySaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(UniversitySaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * @inheritdoc
     */
    protected function canPerformAssignment(User $assignee, string $role, YasnaRequest $request): bool
    {
        if (str_contains($role, "-seconder")) {
            return $this->canAssignSeconders($assignee, $role, $request);
        }

        if ($role == "chief-legal-medicine") {
            return $this->canAssignChiefLegalMedicine($assignee, $role, $request);
        }
        return true;
    }



    /**
     * check if the role assignment can be performed on the chief-legal-medicine roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    private function canAssignChiefLegalMedicine(User $assignee, string $role, $request): bool
    {
        $allowed = $request->model->getEagerUsers("legal-medicine")->pluck('id')->toArray();

        return in_array($assignee->id, $allowed);
    }



    /**
     * check if the role assignment can be performed on the seconder roles
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    private function canAssignSeconders(User $assignee, string $role, $request): bool
    {
        if (!str_contains($role, "chief-")) {
            return $this->canAssignOrdinarySeconders($assignee, $role, $request);
        }

        if (str_contains($role, "chief-") and $role != "chief-seconder") {
            return $this->canAssignChiefSeconders($assignee, $role, $request);
        }

        if ($role == "chief-seconder") {
            return $this->canAssignOverallChiefSeconder($assignee, $role, $request);
        }

        return false;
    }



    /**
     * check if assignment of an ordinary seconder is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    private function canAssignOrdinarySeconders(User $assignee, string $role, $request): bool
    {
        $current_roles = $assignee->unityRolesArray();
        foreach (RegistryUniversity::dependantSeconderRoles() as $forbidden) {
            if (in_array($forbidden, $current_roles)) {
                return false;
            }
        }

        return true;
    }



    /**
     * check if assignment of a chief seconder is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    private function canAssignChiefSeconders(User $assignee, string $role, $request): bool
    {
        $allowed = $request->model->seconders()->get()->pluck('id')->toArray();

        return in_array($assignee->id, $allowed);
    }



    /**
     * check if assignment of the overall chief seconder is possible
     *
     * @param User         $assignee
     * @param string       $role
     * @param YasnaRequest $request
     *
     * @return bool
     */
    private function canAssignOverallChiefSeconder(User $assignee, string $role, $request): bool
    {
        return $this->canAssignChiefSeconders($assignee, $role, $request);
    }
}
