<?php

namespace Modules\Registry\Http\Middleware\V1;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeactivatedCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->currentUserIsWanderer()) {
            $this->restrictUserToken();
            throw new HttpException(trans('registry::userMessages.registry-403'));
        }

        return $next($request);
    }



    /**
     * Invalidate the user token to avoid more authenticated requests.
     */
    protected function restrictUserToken()
    {
        try {
            JWTAuth::parseToken()->invalidate(true);
        } catch (Exception $e) {
            // Pis pis pis, sleep sleep! Nothing happened!
        }
    }



    /**
     * Check an authenticated user wander in our routes or not.
     *
     * @return bool
     */
    protected function currentUserIsWanderer(): bool
    {
        $user = user();
        if (
             $user->isLoggedIn() and $user->canNotLogin()
        ) {
            return true;
        }

        return false;
    }
}
