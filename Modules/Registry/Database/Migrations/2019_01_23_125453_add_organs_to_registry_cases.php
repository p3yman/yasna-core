<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RegistryCase;

class AddOrgansToRegistryCases extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            foreach (array_reverse($this->getFields()) as $key => $options) {
                $options = explode(",", $options);
                $type    = $options[0];
                $default = isset($options[1]) ? $options[1] : null;

                if (!$type) {
                    continue;
                }

                $table->$type($key)->nullable()->default($default)->after("identified_at");
            }
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            $array = [];

            foreach (array_reverse($this->getFields()) as $key => $options) {
                $options = explode(",", $options);
                $type    = $options[0];

                if ($type) {
                    $array[] = $key;
                }
            }

            $table->dropColumn($array);
        });
    }



    /**
     * get fields definitions
     *
     * @return array
     */
    private function getFields()
    {
        return array_merge(
             $this->getHeartFields(),
             $this->getLungFields(),
             $this->getLiverFields(),
             $this->getKidneyFields(),
             $this->getIntestineFields(),
             $this->getPancreasFields()
        );
    }



    /**
     * get heart fields
     *
     * @return array
     */
    private function getHeartFields()
    {
        return [
             "heart_organ_filled" => "boolean",

             "heart_prohibit"                      => "boolean",
             "heart_prohibit_by_desc"              => "string",
             "heart_prohibit_by_electrocardiogram" => "string",
             "heart_prohibit_by_angiography"       => "string",

             "heart_cardiography"        => "boolean",
             "heart_cardiography_report" => "text",
             "heart_cardiography_test"   => "text",

             "heart_echocardiography"            => "boolean",
             "heart_echocardiography_report"     => "string",
             "heart_echocardiography_consultant" => "unsignedInteger",

             "heart_tests_ef"  => "float",
             "heart_tests_lvh" => "float",
             "heart_tests_pap" => "float",
             "heart_tests_trg" => "float",

             "heart_wall_motion_abnormality" => "string",

             "heart_valvular_problem"          => "boolean",
             "heart_valvular_problem_type"     => "string",
             "heart_valvular_problem_severity" => "string",

             "heart_diastolic_dysfunction"          => "boolean",
             "heart_diastolic_dysfunction_severity" => "string",

             "heart_lv_size"     => "string",
             "heart_lv_function" => "string",
             "heart_rv_size"     => "string",
             "heart_rv_function" => "string",
             "heart_la_size"     => "string",
             "heart_la_function" => "string",
             "heart_ra_size"     => "string",
             "heart_ra_function" => "string",

             "heart_tests_consultant"        => "unsignedInteger",
             "heart_tests_consultant_report" => "text",

             "heart_cxr"         => "boolean",
             "heart_cxr_report"  => "text",
             "heart_cxr_picture" => "text",

             "heart_angiography"        => "string",
             "heart_angiography_report" => "text",
        ];
    }



    /**
     * get lung fields
     *
     * @return array
     */
    private function getLungFields()
    {
        return [
             "lung_organ_filled" => "boolean",

             "lung_prohibit"                 => "boolean",
             "lung_prohibit_by_desc"         => "string",
             "lung_prohibit_by_radiology"    => "string",
             "lung_prohibit_by_o2challenge"  => "string",
             "lung_prohibit_by_bronchoscopy" => "string",

             "lung_ventilator_mode"       => "string",
             "lung_ventilator_mode_other" => "string",

             "lung_fio2" => "float",
             "lung_peep" => "float",
             "lung_tv"   => "float",
             "lung_rr"   => "float",

             "lung_o2challenge_fio2"  => "float",
             "lung_o2challenge_po2"   => "float",
             "lung_recruitment_test"  => "boolean",
             "lung_o2challenge_cause" => "string",
             "lung_o2challenge_file"  => "text",

             "lung_cxr"         => "string",
             "lung_cxr_report"  => "text",
             "lung_cxr_picture" => "text",

             "lung_pneumothorax"         => "boolean",
             "lung_pneumothorax_reason"  => "string",
             "lung_infiltration"         => "boolean",
             "lung_infiltration_section" => "string",
             "lung_collapse"             => "boolean",
             "lung_collapse_section"     => "string",
             "lung_tumor"                => "boolean",
             "lung_tumor_section"        => "string",

             "lung_ctscan_need"         => "boolean",
             "lung_ctscan_reason"       => "string",
             "lung_ctscan_reason_other" => "string",

             "lung_bronchoscopy" => "boolean",

             "lung_purulent_tracheal_secretion"          => "boolean",
             "lung_purulent_tracheal_secretion_origin"   => "string",
             "lung_purulent_tracheal_secretion_severity" => "string",

             "lung_tutruasity" => "boolean",
             "lung_erythema"   => "boolean",
             "lung_contusion"  => "boolean",

             "lung_pulmonary_secretion_culture"      => "boolean",
             "lung_pulmonary_secretion_culture_type" => "string",

             "lung_can_donate_left"  => "boolean",
             "lung_can_donate_right" => "boolean",
        ];
    }



    /**
     * get kidney fields
     *
     * @return array
     */
    private function getKidneyFields()
    {
        return [
             "kidney_organ_filled" => "boolean",

             "kidney_prohibit"           => "boolean",
             "kidney_prohibit_by_desc"   => "string",
             "kidney_prohibit_by_tests"  => "string",
             "kidney_prohibit_by_biopsy" => "string",

             "kidney_sonography_report" => "text",
             "kidney_size_left"         => "string",
             "kidney_size_left_cm"      => "integer",
             "kidney_size_right"        => "string",
             "kidney_size_right_cm"     => "integer",

             "kidney_abnormal_finding"          => "boolean",
             "kidney_abnormal_finding_type"     => "string",
             "kidney_abnormal_finding_other"    => "string",
             "kidney_abnormal_finding_position" => "string",
             "kidney_abnormal_finding_size"     => "string",
             "kidney_hydronephrosis_severity"   => "string",

             "kidney_biopsy"                    => "boolean",
             "kidney_biopsy_type"               => "string",
             "kidney_biopsy_type_other"         => "string",
             "kidney_biopsy_fibrosis"           => "string",
             "kidney_biopsy_vascular_changes"   => "string",
             "kidney_biopsy_glomerulus_count"   => "float",
             "kidney_biopsy_glomerulus_percent" => "float",
             "kidney_biopsy_report"             => "text",

             "kidney_hla_typing"      => "boolean",
             "kidney_hla_typing_desc" => "text",
        ];
    }



    /**
     * get liver fields
     *
     * @return array
     */
    private function getLiverFields()
    {
        return [
             "liver_organ_filled" => "boolean",

             "liver_prohibit"                => "boolean",
             "liver_prohibit_by_desc"        => "string",
             "liver_prohibit_by_tests"       => "string",
             "liver_prohibit_by_sonography"  => "string",
             "liver_prohibit_by_observation" => "string",

             "liver_size"      => "string",
             "liver_ecosystem" => "string",

             "liver_mass"                => "string",
             "liver_mass_count"          => "integer",
             "liver_mass_size"           => "float",
             "liver_mass_position"       => "string",
             "liver_mass_position_other" => "string",

             "liver_gallstones"         => "boolean",
             "liver_bile_ducts_inside"  => "boolean",
             "liver_bile_ducts_outside" => "boolean",

             "liver_sonography_report" => "text",

             "liver_biopsy"            => "boolean",
             "liver_biopsy_type"       => "string",
             "liver_biopsy_type_other" => "string",
             "liver_biopsy_result"     => "text",
             "liver_biopsy_report"     => "text",

             "liver_macro_vesicular_fat"              => "float",
             "liver_micro_intermediate_vesicular_fat" => "float",

             "liver_ctscan"            => "boolean",
             "liver_ctscan_report"     => "text",
             "liver_hla_typing_report" => "text",
        ];
    }



    /**
     * get intestine fields
     *
     * @return array
     */
    private function getIntestineFields()
    {
        return [
             "intestine_organ_filled" => "boolean",

             "intestine_prohibit"         => "boolean",
             "intestine_prohibit_by_desc" => "string",

             "intestine_abdominal_sonography"        => "string",
             "intestine_abdominal_sonography_report" => "text",
             "intestine_abdominal_ctscan"            => "string",
             "intestine_abdominal_ctscan_report"     => "text",

             "test_serum_amylase"         => "",
             "test_serum_lipase"          => "",
             "test_ast"                   => "",
             "test_alt"                   => "",
             "test_alk_p"                 => "",
             "test_bili_t"                => "",
             "test_bili_d"                => "",
             "test_hemoglobin_a1c"        => "",
             "biochemistry_tests_done_at" => "",
             "biochemistry_tests_files"   => "",
        ];
    }



    /**
     * get pancreas fields
     *
     * @return array
     */
    private function getPancreasFields()
    {
        return [
             "pancreas_organ_filled" => "boolean",
             "pancreas_hla_typing"   => "string",

             "test_hemoglobin_a1c"         => "",
             "test_serum_amylase"          => "",
             "test_serum_lipase"           => "",
             "received_insulin"            => "",
             "received_insulin_type"       => "",
             "received_insulin_daily_dose" => "",
        ];
    }

}
