<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIcuDeathCountsToRegistryUnityDeathCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unity_death_counts', function (Blueprint $table) {
            $table->unsignedInteger('icu_death_counts')->after('death_counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unity_death_counts', function (Blueprint $table) {
            $table->dropColumn('icu_death_counts');
        });
    }
}
