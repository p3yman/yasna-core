<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTcdFieldToRegistryUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->boolean("tcd_tools")->after("isotope_scan")->default(0);

            $table->dropColumn("marked_icu_beds");
            $table->dropColumn("dedicated_icu_beds");
            $table->string("bed_type")->after("bed_counts")->default(0);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn("tcd_tools");

            $table->integer('marked_icu_beds')->index()->after('service_location');
            $table->integer('dedicated_icu_beds')->index()->after('service_location');
            $table->dropColumn("bed_type");
        });
    }
}
