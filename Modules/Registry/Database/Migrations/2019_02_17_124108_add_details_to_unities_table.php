<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsToUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->string("university_type")->nullable()->after("website");
            $table->string("sector_other")->nullable()->after("sector_type");
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn([
                 "sector_other",
                 "university_type",
            ]);
        });
    }
}
