<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewOrganFieldsToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            $table->float("lung_recruitment_fio2")->after("lung_recruitment_test")->nullable();
            $table->float("lung_recruitment_po2")->after("lung_recruitment_test")->nullable();
            $table->string("lung_pulmonary_secretion_culture")->nullable()->change();

            $table->string("lung_left_abnormality_severity")->after("lung_bronchoscopy")->nullable();
            $table->string("lung_left_abnormality_other")->after("lung_bronchoscopy")->nullable();
            $table->string("lung_left_abnormality")->after("lung_bronchoscopy")->nullable();
            $table->boolean("lung_left_abnormal")->after("lung_bronchoscopy")->default(0);

            $table->string("lung_right_abnormality_severity")->after("lung_bronchoscopy")->nullable();
            $table->string("lung_right_abnormality_other")->after("lung_bronchoscopy")->nullable();
            $table->string("lung_right_abnormality")->after("lung_bronchoscopy")->nullable();
            $table->boolean("lung_right_abnormal")->after("lung_bronchoscopy")->default(0);

            $after = "lung_pulmonary_secretion_culture";
            $table->text("lung_pulmonary_secretion_culture_resistance")->after($after)->nullable();
            $table->text("lung_pulmonary_secretion_culture_sensitivity")->after($after)->nullable();
            $table->text("lung_pulmonary_secretion_culture_antibiotic")->after($after)->nullable();
            $table->text("lung_pulmonary_secretion_culture_detail")->after($after)->nullable();

            $table->float("liver_size_cm")->after("liver_size")->nullable();
            $table->string("kidney_biopsy_glomerulus")->after("kidney_biopsy_glomerulus_percent")->nullable();

            $table->string("pancreas_prohibit_by_desc")->after("pancreas_organ_filled")->nullable();
            $table->boolean("pancreas_prohibit")->after("pancreas_organ_filled")->default(0);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            $table->dropColumn([
                 "lung_recruitment_fio2",
                 "lung_recruitment_po2",

                 "lung_left_abnormal",
                 "lung_left_abnormality",
                 "lung_left_abnormality_other",
                 "lung_left_abnormality_severity",
                 "lung_right_abnormal",
                 "lung_right_abnormality",
                 "lung_right_abnormality_other",
                 "lung_right_abnormality_severity",
                 "lung_pulmonary_secretion_culture_detail",
                 "lung_pulmonary_secretion_culture_antibiotic",
                 "lung_pulmonary_secretion_culture_sensitivity",
                 "lung_pulmonary_secretion_culture_resistance",
                 "liver_size_cm",

                 "kidney_biopsy_glomerulus",
                 "pancreas_prohibit",
                 "pancreas_prohibit_by_desc",

            ]);
        });
    }
}
