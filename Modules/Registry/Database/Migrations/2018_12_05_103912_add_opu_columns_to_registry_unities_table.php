<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpuColumnsToRegistryUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {

            $table->text('harvestable_organs')->after('service_location');

            $table->boolean('organ_harvesting')->after('service_location');
            $table->boolean('isotope_scan')->after('service_location');
            $table->boolean('vessels_angiography')->after('service_location');
            $table->boolean('portable_eeg')->after('service_location');
            $table->boolean('portable_ventilator')->after('service_location');
            $table->boolean('portable_monitor')->after('service_location');


            $table->integer('pmp')->index()->after('service_location');
            $table->integer('population')->index()->after('service_location');
            $table->integer('marked_icu_beds')->index()->after('service_location');
            $table->integer('dedicated_icu_beds')->index()->after('service_location');

            $table->boolean('clinical_detection')->index()->after('service_location');
            $table->boolean('telephone_detection')->index()->after('service_location');

            $table->string('opu_type')->after('service_location');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn('opu_type');
            $table->dropColumn('telephone_detection');
            $table->dropColumn('clinical_detection');
            $table->dropColumn('dedicated_icu_beds');
            $table->dropColumn('marked_icu_beds');
            $table->dropColumn('population');
            $table->dropColumn('pmp');

            $table->dropColumn('portable_monitor');
            $table->dropColumn('portable_ventilator');
            $table->dropColumn('portable_eeg');
            $table->dropColumn('vessels_angiography');
            $table->dropColumn('isotope_scan');
            $table->dropColumn('organ_harvesting');
            $table->dropColumn('harvestable_organs');
        });
    }
}
