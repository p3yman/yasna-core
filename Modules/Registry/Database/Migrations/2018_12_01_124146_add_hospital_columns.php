<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHospitalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->string('postal_code')->nullable()->after('address');
            $table->string('website')->nullable()->after('phones');
            $table->unsignedTinyInteger('sector')->after('website');
            $table->boolean('neurosurgery_services')->nullable()->after('sector');
            $table->boolean('neurological_services')->nullable()->after('neurosurgery_services');
            $table->boolean('trauma_center')->nullable()->after('neurological_services');
            $table->unsignedInteger('opu_distance')->nullable()->after('trauma_center');
            $table->unsignedInteger('bed_counts')->after('opu_distance');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn('postal_code');
            $table->dropColumn('website');
            $table->dropColumn('sector');
            $table->dropColumn('neurosurgery_services');
            $table->dropColumn('neurological_services');
            $table->dropColumn('trauma_center');
            $table->dropColumn('opu_distance');
            $table->dropColumn('bed_counts');
        });
    }
}
