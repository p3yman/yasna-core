<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryPatientTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_patient_transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('case_id')->index();

            $table->unsignedInteger('city_id')->index();
            $table->unsignedInteger('province_id');

            $table->unsignedInteger('origin_hospital_id')->index();
            $table->unsignedInteger('destination_hospital_id')->index();

            $table->tinyInteger('transfer_reason');
            $table->tinyInteger('dismiss_transfer_reason')->nullable();

            $table->string('status', 10)->defaultValue('pending')->index();

            $table->string('transfer_next_action')->nullable();
            $table->string('case_final_state')->nullable();

            $table->timestamp('transferred_at', 0)->nullable()->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index(['province_id', 'city_id']);
            $table->index(['origin_hospital_id', 'status']);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_patient_transfers');
    }
}
