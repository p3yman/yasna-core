<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_unities', function (Blueprint $table) {
            $table->increments('id');

            $table->string("name")->index();
            $table->string("type")->index();

            $table->unsignedInteger("university_id");
            $table->unsignedInteger("opu_id");
            $table->unsignedInteger("hospital_id");

            $table->unsignedInteger('province_id')->default(0)->index();
            $table->unsignedInteger('city_id')->default(0)->index();
            $table->mediumText("address")->nullable();
            $table->string("phones")->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_unities');
    }
}
