<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoordinatorCertificatesFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('medical_system_number')->after('service_location')->nullable();
            $table->string('organs')->after('medical_system_number')->nullable();
            $table->string('international_donor_certificate')->after('organs')->nullable();
            $table->string('iran_donor_certificate')->after('international_donor_certificate')->nullable();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('medical_system_number');
            $table->dropColumn('organs');
            $table->dropColumn('international_donor_certificate');
            $table->dropColumn('iran_donor_certificate');
        });
    }
}
