<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationFieldsAndTypeToUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->decimal('latitude', 10, 6)->nullable()->defaultValue(null)->after('address');
            $table->decimal('longitude', 10, 6)->nullable()->defaultValue(null)->after('latitude');

            $table->string('sector_type')->nullable()->after('sector');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');

            $table->dropColumn('sector_type');
        });
    }
}
