<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeOfYearDeathCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unity_death_counts', function (Blueprint $table) {
            $table->unsignedSmallInteger('year')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unity_death_counts', function (Blueprint $table) {
            $table->unsignedTinyInteger('year')->change();
        });
    }
}
