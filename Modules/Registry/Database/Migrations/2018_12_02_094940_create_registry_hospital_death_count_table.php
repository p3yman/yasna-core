<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryHospitalDeathCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_hospital_death_counts', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('hospital_id')->index();

            $table->unsignedTinyInteger('year');
            $table->unsignedInteger('death_counts');
            
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_hospital_death_counts');
    }
}
