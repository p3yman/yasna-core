<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaseTotalCountsToRegistryCensorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('censors', function (Blueprint $table) {
            $table->unsignedInteger('case_total_counts')->after('day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('censors', function (Blueprint $table) {
            $table->dropColumn('case_total_counts');
        });
    }
}
