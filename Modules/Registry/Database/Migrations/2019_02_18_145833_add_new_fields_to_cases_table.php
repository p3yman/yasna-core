<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            $table->boolean("unknown")->after("coordinator_id")->default(0);
            $table->text("case_unfeasible_reason_others")->after("case_unfeasible_reason")->nullable();
            $table->tinyInteger("proximate_age")->after("birth_date")->nullable();

            $table->timestamp("tests_group12_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group11_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group10_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group9_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group8_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group7_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group6_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group5_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group4_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group3_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group2_done_at")->nullable()->after("virology_tests_files");
            $table->timestamp("tests_group1_done_at")->nullable()->after("virology_tests_files");

            $table->renameColumn('virology_toxo_ab', 'virology_toxo_ab_igg');
            $table->boolean("virology_toxo_ab_igm")->after("virology_vdrl")->nullable();

            $table->string("cigars_recent",20)->after("cigars_per_day")->nullable();
            $table->string("cigars_took",20)->after("cigars_per_day")->nullable();

            $table->string("drug_took_other_recent",20)->after("drug_history")->nullable();
            $table->string("drug_took_other_type",20)->after("drug_history")->nullable();
            $table->string("drug_took_other")->after("drug_history")->nullable();
            $table->string("drug_recent",20)->after("drug_history")->nullable();
            $table->string("drug_took",20)->after("drug_history")->nullable();

            $table->string("alcohol_recent",20)->after("alcohol_amount")->nullable();
            $table->string("alcohol_took",20)->after("alcohol_amount")->nullable();

            $table->boolean("craniotomy")->after("malignancy_chemotherapy_at")->nullable();
            $table->boolean("ventriculoatrial_shunt")->after("malignancy_chemotherapy_at")->nullable();

            $table->timestamp("heart_tests_done_at")->after("heart_tests_trg")->nullable();
            $table->timestamp("pancreas_tests_done_at")->after("pancreas_hla_typing")->nullable();

            $table->string("case_unfeasible_tumor",50)->after("case_unfeasible_reason")->nullable();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_cases', function (Blueprint $table) {
            $table->dropColumn([
                 'unknown',
                 "case_unfeasible_reason_others",
                 "proximate_age",

                 'tests_group1_done_at',
                 'tests_group2_done_at',
                 'tests_group3_done_at',
                 'tests_group4_done_at',
                 'tests_group5_done_at',
                 'tests_group6_done_at',
                 'tests_group7_done_at',
                 'tests_group8_done_at',
                 'tests_group9_done_at',
                 'tests_group10_done_at',
                 'tests_group11_done_at',
                 'tests_group12_done_at',

                 "virology_toxo_ab_igm",
                 "cigars_recent",
                 "cigars_took",
                 "alcohol_recent",
                 "alcohol_took",
                 "drug_took_other_recent",
                 "drug_took_other_type",
                 "drug_took_other",
                 "drug_recent",
                 "drug_took",

                 "craniotomy",
                 "ventriculoatrial_shunt",

                 "heart_tests_done_at",
                 "pancreas_tests_done_at",
                 "case_unfeasible_tumor",
            ]);

            $table->renameColumn('virology_toxo_ab_igg', 'virology_toxo_ab');
        });
    }
}
