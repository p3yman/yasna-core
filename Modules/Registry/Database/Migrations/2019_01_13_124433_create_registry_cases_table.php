<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_cases', function (Blueprint $table) {
            $table->increments('id');

            $this->beddingInfo($table);
            $this->personalInfo($table);
            $this->generalInfo($table);
            $this->clinicalInfo($table);
            $this->reflexes($table);
            $this->stability($table);
            $this->biochemistryTests($table);
            $this->bacteriologyTests($table);
            $this->virologyTests($table);
            $this->bloodAnalysis($table);
            $this->diseases($table);
            $this->malignancy($table);
            $this->heartDiseases($table);
            $this->surgicalHistory($table);
            $this->habits($table);
            $this->recoveries($table);
            $this->medicines($table);
            $this->bloodTransfusion($table);
            $this->infectionTests($table);
            $this->brainDeath($table);
            $this->agreements($table);
            $this->legalMedicinesStuff($table);
            $this->timestamps($table);

            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_cases');
    }



    /**
     * migrate personal info
     *
     * @param Blueprint $table
     */
    private function personalInfo(Blueprint $table)
    {
        $table->string('code_melli', 20)->nullable()->index();
        $table->string('name_first')->nullable()->index();
        $table->string('name_last')->nullable()->index();
        $table->string('full_name')->nullable();
        $table->tinyInteger('gender')->default(0)->index();
        $table->date('birth_date')->nullable()->index();

        $table->unsignedInteger('nationality_id')->default(0);
        $table->unsignedInteger("country_id")->index()->default(0);
        $table->unsignedInteger('province_id')->default(0)->index();
        $table->unsignedInteger('city_id')->default(0)->index();
        $table->text('address')->nullable();
    }



    /**
     * migrate personal info
     *
     * @param Blueprint $table
     */
    private function generalInfo(Blueprint $table)
    {

        $table->float("weight")->comment("in kg")->nullable();
        $table->integer("height")->comment("in cm")->nullable();

        $table->string("blood_type",2)->nullable();
        $table->string("blood_rh",2)->nullable();
    }



    /**
     * migrate clinical info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function clinicalInfo(Blueprint $table)
    {
        $table->boolean("is_suicide")->default(0);

        $table->tinyInteger("gcs")->index()->nullable();
        $table->tinyInteger("gcs_on_arrival")->index()->nullable();
        $table->tinyInteger("gcs_on_identification")->index()->nullable();

        $table->string("consciousness_disorder_cause",60)->nullable();
        $table->text("consciousness_disorder_cause_detail")->nullable();
        $table->text("consciousness_disorder_cause_remarks")->nullable();

        $table->string("identification_type",50)->nullable();
        $table->string("clinical_situation",50)->nullable();
        $table->string("case_situation",50)->nullable()->index();
        $table->string("case_unfeasible_reason")->nullable();

        $table->text("remarks")->nullable();
    }



    /**
     * migrate bedding info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function beddingInfo(Blueprint $table)
    {
        $table->string("file_no",50)->index()->nullable();

        $table->unsignedInteger("ward_id")->default(0)->index();
        $table->unsignedInteger("hospital_id")->default(0)->index();
        $table->unsignedInteger("opu_id")->default(0)->index();
        $table->unsignedInteger("university_id")->default(0)->index();

        $table->unsignedInteger("inspector_id")->default(0)->index();
        $table->unsignedInteger("coordinator_id")->default(0)->index();
    }



    /**
     * migrate reflexes
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function reflexes(Blueprint $table)
    {
        $table->tinyInteger("reflex_breathing");
        $table->tinyInteger("reflex_breathing_rhythm")->comment("times per minute");

        $table->tinyInteger("reflex_face");
        $table->text("reflex_face_remarks")->nullable();

        $table->tinyInteger("reflex_body");
        $table->text("reflex_body_remarks")->nullable();

        $table->tinyInteger("reflex_cornea");
        $table->tinyInteger("reflex_pupil");
        $table->tinyInteger("reflex_gag");
        $table->tinyInteger("reflex_cough");
        $table->tinyInteger("reflex_doll");
    }



    /**
     * migrate stability situations
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function stability(Blueprint $table)
    {
        $table->float("stability_t")->comment("20-50")->nullable();
        $table->float("stability_pr")->comment("0-200")->nullable();
        $table->float("stability_fio2")->comment("0-100")->nullable();
        $table->float("stability_out")->comment("0-3000")->nullable();
        $table->float("stability_dbp")->comment("0-200")->nullable();
        $table->float("stability_sbp")->comment("0-300")->nullable();
        $table->float("stability_rr")->comment("0-100")->nullable();
        $table->float("stability_o2sat")->comment("0-100")->nullable();

        $table->boolean("stability_sedation")->default(0);
        $table->string("stability_sedation_type")->nullable();
        $table->string("stability_sedation_amount")->nullable();
        $table->timestamp("stability_sedation_time")->nullable();
    }



    /**
     * migrate biochemistry tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function biochemistryTests(Blueprint $table)
    {
        $table->float("test_bs")->nullable();
        $table->float("test_urea")->nullable();
        $table->float("test_bun")->nullable();
        $table->float("test_cr")->nullable();
        $table->float("test_wbc")->nullable();
        $table->float("test_hgb")->nullable();
        $table->float("test_plt")->nullable();
        $table->float("test_ast")->nullable();
        $table->float("test_alt")->nullable();
        $table->float("test_alk_p")->nullable();
        $table->float("test_bili_t")->nullable();
        $table->float("test_bili_d")->nullable();
        $table->float("test_na")->nullable();
        $table->float("test_k")->nullable();
        $table->float("test_ca")->nullable();
        $table->float("test_mg")->nullable();
        $table->float("test_pt")->nullable();
        $table->float("test_ptt")->nullable();
        $table->float("test_inr")->nullable();
        $table->float("test_troponin_i")->nullable();
        $table->float("test_hemoglobin_a1c")->nullable();
        $table->float("test_serum_amylase")->nullable();
        $table->float("test_serum_lipase")->nullable();

        $table->timestamp("biochemistry_tests_done_at")->nullable();
        $table->text("biochemistry_tests_files")->nullable();
    }



    /**
     * migrate bacteriology tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function bacteriologyTests(Blueprint $table)
    {
        $items = [
            "bacteria_ua",
            "bacteria_uc",
            "bacteria_bc",
            "bacteria_bal_culture",
            "bacteria_wound_culture",
        ];

        foreach ($items as $item) {
            $table->boolean($item)->default(0);
            $table->string($item."_type")->nullable();
            $table->string($item."_other")->nullable();
        }

        $table->float('bacteria_pro_calcitionin')->nullable();
        $table->timestamp("bacteria_tests_done_at")->nullable();
        $table->text("bacteria_tests_files")->nullable();
    }



    /**
     * migrate virology tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function virologyTests(Blueprint $table)
    {
        $tests = [
             "hbs_ag",
             "hbs_ab",
             "hbc_ab",
             "hcv_ab",
             "hcv_nat",
             "hiv_ab",
             "hiv_nat",
             "htlv_ab",
             "cmv_ab_igm",
             "cmv_ab_igg",
             "ebv_ab_igm",
             "ebv_ab_igg",
             "toxo_ab",
             "vdrl",
        ];

        foreach ($tests as $test) {
            $table->boolean("virology_{$test}")->default(0);
        }

        $table->timestamp("virology_tests_done_at")->nullable();
        $table->text("virology_tests_files")->nullable();

    }



    /**
     * migrate blood analysis
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function bloodAnalysis(Blueprint $table)
    {
        $tests = [
             "ph",
             "pco2",
             "po2",
             "be",
             "o2sat",
             "hco3",
        ];

        foreach ($tests as $test) {
            $table->float("test_{$test}")->nullable();
        }
    }



    /**
     * migrate diseases
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function diseases(Blueprint $table)
    {
        $items = ["hypertension", "diabetes"];

        foreach ($items as $item) {
            $table->boolean($item)->default(0);
            $table->integer("{$item}_time")->default(0);
            $table->boolean("{$item}_is_controlled")->default(0);
            $table->boolean("{$item}_cure_edible")->default(0);
            $table->boolean("{$item}_cure_non_edible")->default(0);
        }
    }



    /**
     * migrate malignancy
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function malignancy(Blueprint $table)
    {
        $table->boolean("malignancy")->default(0);
        $table->text("malignancy_organs")->nullable();
        $table->text("malignancy_others")->nullable();

        $table->timestamp("malignancy_detected_at")->nullable();
        $table->text("malignancy_pathology_result")->nullable();
        $table->text("malignancy_pathology_scan")->nullable();
        $table->timestamp("malignancy_chemotherapy_at")->nullable();
    }



    /**
     * migrate heart diseases
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function heartDiseases(Blueprint $table)
    {
        $table->boolean("heart_disease")->default(0);
        $table->string("heart_diseases_type",50)->nullable();
        $table->string("heart_diseases_other")->nullable();
    }



    /**
     * migrate surgical history
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function surgicalHistory(Blueprint $table)
    {
        $table->boolean("surgical_history")->default(0);
    }



    /**
     * migrate habits
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function habits(Blueprint $table)
    {
        $table->integer("cigars_per_day")->nullable();
        $table->integer("cigars_abandoned_time")->nullable()->comment("in months");

        $table->boolean("alcohol_history")->default(0);
        $table->string("alcohol_type",10)->nullable();
        $table->string("alcohol_amount",10)->nullable();

        $table->boolean("drug_history")->default(0);
        $table->text("drug_types")->nullable();
        $table->text("drug_amount")->nullable();

        $table->boolean("had_risky_sex")->nullable();
        $table->boolean("had_tattoo")->nullable();
        $table->boolean("had_prison_past")->nullable();
    }



    /**
     * migrate recoveries
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function recoveries(Blueprint $table)
    {
        $table->integer("cpr_numbers")->default(0);
        $table->timestamp("cpr_received_at")->nullable();

        $table->integer("shock_numbers")->default(0);
        $table->timestamp("shock_received_at")->nullable();
    }



    /**
     * migrate medicines
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function medicines(Blueprint $table)
    {
        $table->text("received_antibiotics")->nullable();
        $table->boolean("received_serum")->default(0);
        $table->boolean("received_thyroid_hormone")->default(0);
        $table->boolean("received_steroid")->default(0);
        $table->boolean("received_anticonvulsant")->default(0);
        $table->string("received_antihypertensives",50)->nullable();
        $table->text("received_antihypertensives_others")->nullable();
        $table->boolean("received_heparin")->default(0);
        $table->boolean("received_vasopressin")->default(0);
        $table->boolean("received_diuretic")->default(0);

        $table->boolean("received_insulin")->default(0);
        $table->string("received_insulin_type",20)->nullable();
        $table->integer("received_insulin_daily_dose")->default(0);
        $table->text("received_others")->nullable();
    }



    /**
     * migrate blood Transfusion
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function bloodTransfusion(Blueprint $table)
    {
        $table->text("received_blood")->nullable();
        $table->tinyInteger("received_blood_amount")->default(0);

        //$items = ["platelet", "packed_cell", "cryopercipitate", "ffp", "whole"];
        //
        //foreach ($items as $item) {
        //    $table->tinyInteger("received_blood_{$item}")->default(0);
        //}
    }



    /**
     * migrate infection tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function infectionTests(Blueprint $table)
    {
        $table->text("infection_organ_engagement")->nullable();
        $table->string("infection_organ_cultivation",50)->nullable();
        $table->text("infection_organ_engagement_text")->nullable();
        $table->text("infection_organ_cultivation_text")->nullable();

        $table->timestamp("infection_treatment_started_at")->nullable();
        $table->boolean("infection_treatment_successful")->nullable();
        $table->string("infection_treatment_positive_signs")->nullable();
        $table->text("infection_treatment_negative_consultant")->nullable();
        $table->text("infection_treatment_doc")->nullable();

    }



    /**
     * migrate brain death confirmations
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function brainDeath(Blueprint $table)
    {
        $table->timestamp("bd_detected_at")->nullable();
        $table->timestamp("bd_claimed_at")->nullable();

        for ($i = 1; $i <= 4; $i++) {
            $table->timestamp("bd_confirmed_at_{$i}")->nullable();
            $table->unsignedInteger("bd_confirmed_by_{$i}")->default(0);
            $table->text("bd_confirmation_{$i}")->nullable();
        }
    }



    /**
     * migrate agreements
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function agreements(Blueprint $table)
    {
        $table->boolean("has_donation_card")->nullable();
        $table->boolean("verbally_allowed_donation")->nullable();

        $table->timestamp("donation_agreement_confirmed_at")->nullable();
        $table->unsignedInteger("donation_agreement_confirmed_by")->default(0);
        $table->text("donation_agreement")->nullable();
    }


    /**
     * migrate legal medicine stuff
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function legalMedicinesStuff(Blueprint $table)
    {
        for($i=1 ; $i<=2 ; $i++) {
            $table->unsignedBigInteger("legal_medicine_{$i}")->nullable();
            $table->timestamp("legal_medicine_{$i}_confirmed_at")->nullable();
            $table->text("legal_medicine_{$i}_file")->nullable();
        }
    }



    /**
     * migrate timestamps
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function timestamps(Blueprint $table)
    {
        $table->timestamp("admitted_at")->nullable();

        $table->timestamp("identified_at")->nullable();
        $table->unsignedInteger("identified_by")->default(0);

        $table->timestamp("died_at")->nullable();
        $table->timestamp("heart_death_detected_at")->nullable();
        $table->timestamp("aortic_clamped_at")->nullable();
        $table->timestamp("donated_at")->nullable();

        $table->timestamps();
    }

}
