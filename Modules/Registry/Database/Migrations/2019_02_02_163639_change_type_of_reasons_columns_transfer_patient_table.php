<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeOfReasonsColumnsTransferPatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("registry_patient_transfers", function (Blueprint $table) {
            $table->string("transfer_reason")->nullable()->change();
            $table->string("dismiss_transfer_reason")->nullable()->change();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("registry_patient_transfers", function (Blueprint $table) {
            $table->tinyInteger("transfer_reason")->nullable()->change();
            $table->tinyInteger("dismiss_transfer_reason")->nullable()->change();
        });
    }
}
