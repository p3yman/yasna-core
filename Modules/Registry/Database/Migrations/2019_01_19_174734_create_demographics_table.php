<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemographicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_demographics', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("case_id")->index();
            $table->integer("batch")->index();
            $table->string("slug")->nullable()->index();
            $table->string("clue")->index();
            $table->string("value_string")->nullable()->index();
            $table->float("value_numeric")->nullable()->index();
            $table->timestamp("value_timestamp")->nullable()->index();
            $table->text("value_text")->nullable();

            $table->string("supporting_file")->nullable();
            $table->timestamp("effected_at")->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('case_id')->references('id')->on('registry_cases')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_demographics');
    }
}
