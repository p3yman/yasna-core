<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('expiertise')->nullable()->after('edu_field');
            $table->string('job_history')->nullable()->after('expiertise');
            $table->string('latest_job_position')->nullable()->after('job_history');
            $table->boolean('is_retired')->default(0)->after('latest_job_position');
            $table->string('service_location')->nullable()->after('is_retired');
            $table->string('second_job')->nullable()->after('service_location');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('expiertise');
            $table->dropColumn('job_history');
            $table->dropColumn('latest_job_position');
            $table->dropColumn('is_retired');
            $table->dropColumn('service_location');
            $table->dropColumn('second_job');
        });
    }
}
