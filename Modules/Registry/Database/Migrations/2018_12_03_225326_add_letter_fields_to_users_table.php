<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLetterFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp("letter_expiry")->after("second_job")->nullable()->index();
            $table->timestamp("letter_expiry_notified_at")->after("letter_expiry")->nullable();
            $table->string("letter_file")->after('second_job')->nullable();
            $table->unsignedInteger("work_unity")->after("second_job")->nullable()->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                 'letter_expiry',
                 'letter_expiry_notified_at',
                 'letter_file',
                 'work_unity',
            ]);
        });
    }
}
