<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRegistryHospitalDeathCountsToRegistryUnitiesDeathCounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('registry_hospital_death_counts', 'registry_unity_death_counts');

        Schema::table('registry_unity_death_counts', function (Blueprint $table) {
            $table->renameColumn('hospital_id', 'unity_id');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('registry_unity_death_counts', 'registry_hospital_death_counts');

        Schema::table('registry_hospital_death_counts', function (Blueprint $table) {
            $table->renameColumn('unity_id', 'hospital_id');
        });
    }
}
