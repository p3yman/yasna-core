<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnityUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_unity_roles', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("unity_id")->index();
            $table->unsignedInteger("user_id")->index();
            $table->string("role")->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_unity_roles');
    }
}
