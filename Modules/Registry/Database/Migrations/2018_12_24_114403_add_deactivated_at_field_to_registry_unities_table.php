<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeactivatedAtFieldToRegistryUnitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->timestamp('deactivated_at')->index()->nullable()->after('deleted_at');
            $table->unsignedInteger('deactivated_by')->default(0)->nullable()->after('deleted_by');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registry_unities', function (Blueprint $table) {
            $table->dropColumn('deactivated_at');
            $table->dropColumn('deactivated_by');
        });
    }
}
