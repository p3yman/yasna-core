<?php

namespace Modules\Registry\Database\Seeders;

use Illuminate\Database\Seeder;

class RegistryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CombosTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
    }
}
