<?php

namespace Modules\Registry\Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed("roles", $this->getArray());
    }



    /**
     * get the seeding array
     *
     * @return array
     */
    private function getArray()
    {
        return [
             [
                  "slug"         => "master",
                  "title"        => trans("registry::roles.master"),
                  "plural_title" => trans("registry::roles.master"),
                  "is_admin"     => 1,
             ],
        ];
    }
}
