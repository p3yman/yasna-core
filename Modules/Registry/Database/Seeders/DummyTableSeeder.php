<?php

namespace Modules\Registry\Database\Seeders;

use App\Models\Division;
use App\Models\RegistryCase;
use App\Models\RegistryUniversity;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Users\Database\Seeders\DummyTableSeeder as UsersDummyTableSeeder;
use Modules\Yasna\Services\YasnaModel;

class DummyTableSeeder extends Seeder
{
    protected $users_count = 0;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedDummyUniversities();
        $this->seedDummyOPUs();
        $this->seedHospitals();
        $this->seedWards();

        $this->seedUsers();
        $this->seedDummyRolesForAllUnities();

        $this->seedDummyPatients();
    }



    /**
     * seed dummy users, by means of the dummy generator in the users module.
     * This works safe even if the users module is turned off. But it must be present.
     */
    protected function seedUsers()
    {
        $this->call(UsersDummyTableSeeder::class);
        $this->setUselessPasswordForEverybody();

        $this->users_count = user()->count();
    }



    /**
     * seed dummy universities
     *
     * @param int $total
     *
     * @return void
     */
    protected function seedDummyUniversities($total = 10)
    {
        $data = [];
        for ($i = 1; $i <= $total; $i++) {
            $province = $this->getRandomProvince();
            $city     = $this->getRandomCity($province);

            $data[] = [
                 "name"        => trans("registry::fields.university") . SPACE . dummy()::persianWord(2),
                 "type"        => "university",
                 "province_id" => $province->id,
                 "city_id"     => $city->id,
                 "address"     => dummy()::persianWord(10),
                 "phones"      => dummy()::mobile(),
            ];

        }

        yasna()::seed("registry_unities", $data);
    }



    /**
     * seed dummy OPUs
     *
     * @param int $total
     */
    protected function seedDummyOPUs($total = 2)
    {
        $universities = model('registry-university')->all();
        $data         = [];

        foreach ($universities as $university) {
            for ($i = 1; $i <= $total; $i++) {
                $data[] = [
                     "university_id" => $university->id,
                     "name"          => trans("registry::fields.opu") . SPACE . dummy()::persianWord(2),
                     "type"          => "opu",
                     "province_id"   => $university->province_id,
                     "city_id"       => $university->city_id,
                     "address"       => dummy()::persianWord(10),
                     "phones"        => dummy()::mobile(),
                ];
            }
        }

        yasna()::seed("registry_unities", $data);
    }



    /**
     * seed dummy hospitals
     *
     * @param int $total
     */
    protected function seedHospitals($total = 5)
    {
        $units = model("registry-opu")->all();
        $data  = [];

        foreach ($units as $unit) {
            for ($i = 1; $i <= $total; $i++) {
                $data[] = [
                     "university_id" => $unit->university_id,
                     "opu_id"        => $unit->id,
                     "name"          => trans("registry::fields.hospital") . SPACE . dummy()::persianWord(2),
                     "type"          => "hospital",
                     "province_id"   => $unit->province_id,
                     "city_id"       => $unit->city_id,
                     "address"       => dummy()::persianWord(10),
                     "phones"        => dummy()::mobile(),
                ];
            }
        }

        yasna()::seed("registry_unities", $data);
    }



    /**
     * seed dummy wards
     *
     * @param int $total
     */
    protected function seedWards($total = 2)
    {
        $hospitals = model("registry-hospital")->all();
        $data      = [];

        foreach ($hospitals as $hospital) {
            for ($i = 1; $i <= $total; $i++) {
                $data[] = [
                     "university_id" => $hospital->university_id,
                     "opu_id"        => $hospital->opu_id,
                     "hospital_id"   => $hospital->id,
                     "name"          => trans("registry::fields.ward") . SPACE . dummy()::persianWord(1),
                     "type"          => "ward",
                     "province_id"   => $hospital->province_id,
                     "city_id"       => $hospital->city_id,
                     "address"       => dummy()::persianWord(10),
                     "phones"        => dummy()::mobile(),
                ];
            }
        }

        yasna()::seed("registry_unities", $data);
    }



    /**
     * Seed Cases
     *
     * @param int $total
     */
    protected function seedDummyPatients($total = 50)
    {
        $counter = 0;

        while ($counter < $total) {
            $ward     = model('registry-ward')->where('converted',1)->inRandomOrder()->first();
            $province = $this->getRandomProvince();
            $city     = $this->getRandomCity($province);

            $inspectors = array_random(['inspectorClinicals', 'inspectorTels']);

            $disorder_cause = array_random([
                 "hit-on-head-accident",
                 "hit-on-head-fall",
                 "hit-on-head-fight",
                 "hit-on-head-bullet-hit",
                 "cerebrovascular-damage-strokes",
                 "cerebrovascular-damage-aneurysm-rupture",
            ]);

            $clinical_situation = array_random([
                 "clinical-situation-gcs3-died",
                 "clinical-situation-gcs3-alive",
                 "clinical-situation-gcs4and5",
                 "clinical-situation-unfeasible",
            ]);

            $deleted = boolval(rand(1, 20) > 18);

            $identification_type = array_random(["identification-type-clinical", "identification-type-over-tel"]);

            $case_situation = array_random([
                 'case-situation-new',
                 'case-situation-follow-up',
                 'case-situation-under-action',
                 'case-situation-healed',
                 'case-situation-donated',
                 'case-situation-unfeasible',
            ]);

            $unfeasible = array_random([
                 "unfeasible_reason-brain-tumors",
                 "unfeasible_reason-unusable-other-tumors",
                 "unfeasible_reason-uncontrolled-systemic-infections",
                 "unfeasible_reason-local-acute-infections",
                 "unfeasible_reason-hiv",
                 "unfeasible_reason-hcv",
                 "unfeasible_reason-creutzfeldt-jakob",
                 "unfeasible_reason-unknown-brain-death-reasons",
                 "unfeasible_reason-unknown-identity",
                 "unfeasible_reason-legal-prohibitions",
                 "unfeasible_reason-foreigners",
                 "unfeasible_reason-old-age",
            ]);

            $bacteriology = [
                 "bacteriology-type-positive-estafilocock",
                 "bacteriology-type-positive-pesudomona",
                 "bacteriology-type-positive-calbesila",
                 "bacteriology-type-positive-penomocock",
                 "bacteriology-type-positive-masgocock",
                 "bacteriology-type-positive-ecoli",
                 "bacteriology-type-positive-other",
            ];

            $heart_diseases = array_random([
                 "known-heart-disease-vascular-stenosis",
                 "known-heart-disease-valves",
                 "known-heart-disease-congenital",
                 "known-heart-disease-others",
            ]);

            $bd = intval(!boolval(rand(0, 5)));

            $counter++;

            $data = [
                 "file_no"       => str_random(10),
                 "ward_id"       => $ward->id,
                 "hospital_id"   => $ward->hospital_id,
                 "opu_id"        => $ward->opu_id,
                 "university_id" => $ward->university_id,

                 "inspector_id"   => $inspector_id = $ward->opu->$inspectors()->inRandomOrder()->first()->id,
                 "coordinator_id" => $coordinator_id = $ward->opu->coordinators()->inRandomOrder()->first()->id,

                 'code_melli'     => rand(1111111111, min(9999999999, getrandmax())),
                 'name_first'     => dummy()::persianName(),
                 'name_last'      => dummy()::persianFamily(),
                 'gender'         => rand(1, 2),
                 'birth_date'     => now()->subWeeks(rand(40, 520))->toDateTimeString(),
                 "nationality_id" => rand(603, 685),
                 "country_id"     => "495",
                 "province_id"    => $province->id,
                 "city_id"        => $city->id,
                 "address"        => dummy()::persianText(1),

                 'weight'     => rand(50, 200),
                 'height'     => rand(50, 220),
                 'blood_type' => array_random(["O", "A", "B", "AB"]),
                 'blood_rh'   => array_random(["+", "-"]),
                 "is_suicide" => rand(0, 1),

                 "gcs"                   => rand(3, 15),
                 "gcs_on_arrival"        => rand(3, 15),
                 "gcs_on_identification" => rand(3, 15),

                 "consciousness_disorder_cause_detail"  => $disorder_cause,
                 "consciousness_disorder_cause_remarks" => dummy()::persianWord(3),
                 "identification_type"                  => $identification_type,
                 "clinical_situation"                   => $clinical_situation,
                 "case_situation"                       => $case_situation,
                 "case_unfeasible_reason"               => $unfeasible,
                 "remarks"                              => dummy()::persianWord(3),

                 'reflex_breathing'        => array_random([-1, 1]),
                 'reflex_breathing_rhythm' => rand(1, 30),
                 'reflex_face'             => array_random([-1, 1]),
                 "reflex_face_remarks"     => dummy()::persianWord(3),
                 'reflex_body'             => array_random([-1, 1]),
                 "reflex_body_remarks"     => dummy()::persianWord(3),
                 'reflex_cornea'           => array_random([-1, 1]),
                 'reflex_pupil'            => array_random([-1, 1]),
                 'reflex_gag'              => array_random([-1, 1]),
                 'reflex_cough'            => array_random([-1, 1]),
                 'reflex_doll'             => array_random([-1, 1]),

                 "stability_t"               => rand(20, 50),
                 "stability_pr"              => rand(0, 200),
                 "stability_fio2"            => rand(0, 100),
                 "stability_out"             => rand(0, 3000),
                 "stability_dbp"             => rand(0, 200),
                 "stability_sbp"             => rand(0, 300),
                 "stability_rr"              => rand(0, 100),
                 "stability_o2sat"           => rand(0, 100),
                 'stability_sedation'        => rand(0, 1),
                 'stability_sedation_type'   => dummy()::englishWord(),
                 'stability_sedation_amount' => rand(4, 42),
                 'stability_sedation_time'   => now()->subRealHours(rand(1, 42))->toDateTimeString(),

                 "test_bs"                    => rand(20, 1000),
                 'test_urea'                  => rand(1, 300),
                 'test_bun'                   => rand(1, 200),
                 'test_cr'                    => rand(1, 150) / 10,
                 'test_wbc'                   => rand(1, 200000),
                 'test_hgb'                   => rand(1, 30),
                 'test_plt'                   => rand(10, 900000),
                 'test_ast'                   => rand(1, 3000),
                 'test_alt'                   => rand(1, 3000),
                 'test_alk_p'                 => rand(10, 3000),
                 'test_bili_t'                => rand(1, 300) / 10,
                 'test_bili_d'                => rand(1, 300) / 10,
                 'test_na'                    => rand(50, 400),
                 'test_k'                     => rand(1, 200) / 10,
                 'test_ca'                    => rand(1, 200),
                 'test_mg'                    => rand(1, 20),
                 'test_pt'                    => rand(1, 60),
                 'test_ptt'                   => rand(1, 200),
                 'test_inr'                   => rand(1, 30),
                 'test_troponin_i'            => rand(0, 1000) / 100,
                 'test_hemoglobin_a1c'        => rand(3, 20),
                 'test_serum_amylase'         => rand(0, 5000),
                 'test_serum_lipase'          => rand(0, 5000),
                 'biochemistry_tests_done_at' => now()->subRealHours(rand(1, 42))->toDateTimeString(),

                 "bacteria_ua"              => array_random($bacteriology),
                 "bacteria_uc"              => array_random($bacteriology),
                 "bacteria_bc"              => array_random($bacteriology),
                 "bacteria_bal_culture"     => array_random($bacteriology),
                 "bacteria_wound_culture"   => array_random($bacteriology),
                 "bacteria_pro_calcitionin" => rand(0, 5000),
                 'bacteria_tests_done_at'   => now()->subRealHours(rand(1, 42))->toDateTimeString(),

                 "virology_hbs_ag"        => intval(!boolval(rand(0, 10))),
                 "virology_hbs_ab"        => intval(!boolval(rand(0, 10))),
                 "virology_hbc_ab"        => intval(!boolval(rand(0, 10))),
                 "virology_hcv_ab"        => intval(!boolval(rand(0, 10))),
                 "virology_hcv_nat"       => intval(!boolval(rand(0, 10))),
                 "virology_hiv_ab"        => intval(!boolval(rand(0, 10))),
                 "virology_hiv_nat"       => intval(!boolval(rand(0, 10))),
                 "virology_htlv_ab"       => intval(!boolval(rand(0, 10))),
                 "virology_cmv_ab_igm"    => intval(!boolval(rand(0, 10))),
                 "virology_cmv_ab_igg"    => intval(!boolval(rand(0, 10))),
                 "virology_ebv_ab_igm"    => intval(!boolval(rand(0, 10))),
                 "virology_ebv_ab_igg"    => intval(!boolval(rand(0, 10))),
                 "virology_toxo_ab_igg"   => intval(!boolval(rand(0, 10))),
                 "virology_vdrl"          => intval(!boolval(rand(0, 10))),
                 'virology_tests_done_at' => now()->subRealHours(rand(1, 42))->toDateTimeString(),

                 'test_ph'    => rand(4, 9),
                 'test_pco2'  => rand(1, 200),
                 'test_po2'   => rand(1, 700),
                 'test_be'    => rand(-10, 10),
                 'test_o2sat' => rand(1, 100),
                 'test_hco3'  => rand(1, 100),

                 "hypertension"                 => $hypertension = intval(!boolval(rand(0, 3))),
                 "hypertension_time"            => $hypertension ? rand(1, 10) : 0,
                 "hypertension_is_controlled"   => rand(0, 1),
                 "hypertension_cure_edible"     => rand(0, 1),
                 "hypertension_cure_non_edible" => rand(0, 1),
                 "diabetes"                     => $diabetes = intval(!boolval(rand(0, 3))),
                 "diabetes_time"                => $diabetes ? rand(1, 10) : 0,
                 "diabetes_is_controlled"       => rand(0, 1),
                 "diabetes_cure_edible"         => rand(0, 1),
                 "diabetes_cure_non_edible"     => rand(0, 1),

                 "malignancy"                  => $malignancy = intval(!boolval(rand(0, 3))),
                 "malignancy_organs"           => $malignancy ? "folan,bisar" : "",
                 "malignancy_others"           => dummy()::englishWord(2),
                 'malignancy_detected_at'      => now()->subRealHours(rand(1, 42))->toDateTimeString(),
                 "malignancy_pathology_result" => dummy()::englishWord(2),
                 'malignancy_chemotherapy_at'  => rand(0, 1) ? now()->toDateTimeString() : null,

                 "heart_disease"        => $heart = intval(!boolval(rand(0, 3))),
                 "heart_diseases_type"  => $heart_diseases,
                 "heart_diseases_other" => rand(0, 1) ? dummy()::englishWord(1) : '',

                 "surgical_history" => $heart = intval(!boolval(rand(0, 3))),

                 "cigars_per_day"        => rand(0, 10),
                 "cigars_abandoned_time" => rand(0, 10),
                 "alcohol_history"       => rand(0, 1),
                 "alcohol_amount"        => array_random(["use-course-daily", "use-course-weekly"]),
                 "had_risky_sex"         => rand(0, 1),
                 "had_tattoo"            => rand(0, 1),
                 "had_prison_past"       => rand(0, 1),
                 "cpr_numbers"           => rand(0, 20),
                 "cpr_received_at"       => now()->subRealHours(rand(1, 42))->toDateTimeString(),
                 "shock_numbers"         => rand(0, 20),
                 "shock_received_at"     => now()->subRealHours(rand(1, 42))->toDateTimeString(),

                 "received_antibiotics"              => rand(0, 1),
                 'received_serum'                    => rand(0, 1),
                 'received_thyroid_hormone'          => rand(0, 1),
                 'received_steroid'                  => rand(0, 1),
                 'received_anticonvulsant'           => rand(0, 1),
                 'received_antihypertensives'        => rand(0, 1),
                 'received_antihypertensives_others' => rand(0, 1),
                 'received_heparin'                  => rand(0, 1),
                 'received_vasopressin'              => rand(0, 1),
                 'received_diuretic'                 => rand(0, 1),
                 'received_insulin'                  => rand(0, 1),
                 'received_insulin_type'             => rand(0, 1),
                 'received_insulin_daily_dose'       => rand(0, 1),
                 'received_others'                   => rand(0, 1),
                 'received_blood'                    => '',

                 'infection_organ_engagement'              => '',
                 'infection_organ_cultivation'             => '',
                 'infection_organ_engagement_text'         => dummy()::englishWord(),
                 'infection_organ_cultivation_text'        => dummy()::englishWord(),
                 'infection_treatment_started_at'          => now()->subRealHours(rand(1, 42))->toDateTimeString(),
                 'infection_treatment_successful'          => rand(0, 1),
                 'infection_treatment_positive_signs'      => '',
                 'infection_treatment_negative_consultant' => '',
                 'infection_treatment_doc'                 => '',

                 'bd_detected_at'    => $bd ? now()->subRealHours(rand(1, 42))->toDateTimeString() : null,
                 'bd_claimed_at'     => $bd ? now()->subRealHours(rand(1, 42))->toDateTimeString() : null,
                 'bd_confirmed_at_1' => null,
                 'bd_confirmed_by_1' => 0,
                 'bd_confirmation_1' => "",
                 'bd_confirmed_at_2' => null,
                 'bd_confirmed_by_2' => 0,
                 'bd_confirmation_2' => "",
                 'bd_confirmed_at_3' => null,
                 'bd_confirmed_by_3' => 0,
                 'bd_confirmation_3' => "",
                 'bd_confirmed_at_4' => null,
                 'bd_confirmed_by_4' => 0,
                 'bd_confirmation_4' => "",

                 'has_donation_card'               => rand(0, 1),
                 'verbally_allowed_donation'       => rand(0, 1),
                 'donation_agreement_confirmed_at' => null,
                 'donation_agreement_confirmed_by' => 0,
                 'donation_agreement'              => null,

                 'admitted_at'             => now()->subRealHours(rand(1, 42))->toDateTimeString(),
                 'identified_at'           => now()->subRealHours(rand(1, 42))->toDateTimeString(),
                 'identified_by'           => array_random([$inspector_id, $coordinator_id]),
                 'died_at'                 => $bd ? now()->subRealHours(rand(1, 42))->toDateTimeString() : null,
                 'heart_death_detected_at' => null,
                 'aortic_clamped_at'       => null,
                 'donated_at'              => null,
                 "created_by"              => array_random([$inspector_id, $coordinator_id]),
            ];

            model('registry-case')->batchSave($data);
        }

    }



    /**
     * seed dummy roles for all unities by calling another method
     */
    protected function seedDummyRolesForAllUnities()
    {
        foreach (UnityAbstract::validTypes() as $type) {
            $this->seedDummyRolesForOneUnity($type);
        }
    }



    /**
     * seed dummy roles for all the records of one unity type
     *
     * @param string $type
     */
    protected function seedDummyRolesForOneUnity(string $type)
    {
        $model = model("registry-$type")->first();

        if ($model) {
            $this->seedDummyRolesForOneModel($model);
            $model->update([
                 "converted" => "1",
            ]);
        }
    }



    /**
     * seed dummy roles for one model assuming 200 dummy users are already seeded.
     *
     * @param UnityAbstract|YasnaModel $model
     */
    protected function seedDummyRolesForOneModel($model)
    {
        foreach ($model->userRoles() as $role) {
            $model->assignUserAs(rand(1, $this->users_count), $role);
            $model->assignUserAs(rand(1, $this->users_count), $role);
        }
    }



    /**
     * get a random province
     *
     * @return Division
     */
    protected function getRandomProvince()
    {
        return model('division')->where("country_id", 495)->where("province_id", 0)->inRandomOrder()->first();
    }



    /**
     * @param Division $province
     *
     * @return ?Division
     */
    protected function getRandomCity(Division $province)
    {
        return model('division')->where("province_id", $province->id)->where("city_id", 0)->inRandomOrder()->first();
    }



    /**
     * set useless password for everybody other than the developer account
     */
    protected function setUselessPasswordForEverybody()
    {
        model('user')->whereNull('password')->update([
             "password" => "1",
        ])
        ;
    }
}
