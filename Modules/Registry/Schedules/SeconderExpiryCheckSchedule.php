<?php

namespace Modules\Registry\Schedules;

use App\Models\RegistryUniversity;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Modules\Registry\Notifications\UserLetterExpiredNotification;
use Modules\Yasna\Services\YasnaSchedule;

class SeconderExpiryCheckSchedule extends YasnaSchedule
{

    /**
     * perform the action
     */
    protected function job()
    {
        $builder = $this->getBuilder();

        if ($builder->count()) {

            /** @var User $user */
            foreach ($builder->get() as $user) {
                $this->raiseNotification($user);
            }

        }
    }



    /**
     * get eloquent builder
     *
     * @return Builder
     */
    private function getBuilder(): Builder
    {
        return user()::whereNotNull("letter_expiry")
                     ->whereNull("letter_expiry_notified_at")
                     ->where("letter_expiry", "<", now())
             ;

    }



    /**
     * raise notification for the expired user
     *
     * @param User $user
     */
    private function raiseNotification(User $user)
    {
        $notifiables = $this->discoverNotifiables($user);

        /** @var User $notifiable */
        foreach ($notifiables as $notifiable) {
            $notifiable->notify(new UserLetterExpiredNotification($user));
            //$this->markAsNotified($user);
        }
    }



    /**
     * mark the user as notified
     *
     * @param User $user
     */
    private function markAsNotified(User $user)
    {
        $user->letter_expiry_notified_at = now()->toDateTimeString();
        $user->save();
    }



    /**
     * discover the notifiable user
     *
     * @param User $user
     *
     * @return array
     */
    private function discoverNotifiables(User $user): array
    {
        $university = $this->discoverUniversity($user);
        $array      = [];

        $notifiable1 = $university->expert_in_charge;
        $notifiable2 = $university->donor_expert;

        if ($notifiable1->exists) {
            $array[] = $notifiable1;
        }
        if ($notifiable2->exists) {
            $array[] = $notifiable2;
        }

        return $array;
    }



    /**
     * discover the university of the user with expired letter
     *
     * @param User $user
     *
     * @return RegistryUniversity
     */
    private function discoverUniversity(User $user): RegistryUniversity
    {
        //$ids = $user->unityRoles()->get()->pluck('unity_id')->toArray();
        $id    = 0;
        $pivot = $user->unityRoles()->first();
        if ($pivot and $pivot->exists) {
            $id = $pivot->unity_id;
        }

        return model("registry-university", $id);
    }
}
