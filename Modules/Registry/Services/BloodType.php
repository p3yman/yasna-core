<?php

namespace Modules\Registry\Services;

class BloodType
{

    /**
     * return the possible Blood Types letters
     *
     * @return array
     */
    public static function getTypes()
    {
        return ['A', 'B', 'AB', 'O'];
    }



    /**
     * return the blood RH
     *
     * @return array
     */
    public static function getRH()
    {
        return ['+', '-'];
    }



    /**
     * return all blood types
     *
     * @return array
     */
    public static function getFullList()
    {
        return [
             "A+",
             "A-",
             "B+",
             "B-",
             "AB+",
             "AB-",
             "O+",
             "O-",
        ];
    }



    /**
     * get a blood group and split it into group and rh
     *
     * @param string $group
     *
     * @return array
     */
    public static function parseTypeAndRH(string $group): array
    {
        $rh   = null;
        $type = null;

        if (str_contains($group, "+")) {
            $rh = "+";
        } elseif (str_contains($group, "-")) {
            $rh = "-";
        }

        $type = strtoupper(str_before(str_before($group, "-"), "+"));

        if(!in_array($type, static::getTypes())) {
            $type = null;
        }
        //if(!in_array($rh, static::getRH())) {
        //    $rh = null;
        //}

        return [
             "type" => $type,
             "rh"   => $rh,
        ];
    }
}
