<?php

namespace Modules\Registry\Services;

use Illuminate\Support\Arr;
use Illuminate\Validation\Validator;

class CustomValidators
{
    /**
     * `'field1 => 'inconsistent_with:field2',` rule checks just one of the `field1` or `field2` has value at a time
     *
     * @param string          $field1
     * @param string|int|null $value_field1
     * @param string          $parameters
     * @param Validator       $validator
     *
     * @return bool
     */
    public function inconsistentWith($field1, $value_field1, $parameters, $validator): bool
    {
        $value_field2 = Arr::get($validator->getData(), $parameters[0]);

        return !empty($value_field1) and !empty($value_field2) ? false : true;
    }



    /**
     * message that will be shown on `inconsistent_with` rule failed situations
     *
     * @param string $message
     * @param string $field1
     * @param string $rule
     * @param array  $parameters
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function inconsistentWithMessage($message, $field1, $rule, $parameters)
    {
        $field2 = $parameters[0];

        return trans(
             "registry::validation.inconsistent_with_error_message",
             [
                  'field1' => trans("registry::fields.$field1"),
                  'field2' => trans("registry::fields.$field2"),
             ]
        );
    }
}
