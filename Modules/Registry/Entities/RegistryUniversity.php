<?php

namespace Modules\Registry\Entities;

use App\Models\RegistrySharedUnity;
use App\Models\RegistryUnityRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Entities\Traits\UniversityPermissionsTrait;
use Modules\Registry\Entities\Traits\UniversitySecondersTrait;

class RegistryUniversity extends UnityAbstract
{
    use UniversityPermissionsTrait;
    use UniversitySecondersTrait;


    /**
     * @inheritdoc
     */
    protected $fields_must_filled = [
         'name',
         'phones',
         'postal_code',
         'address',
         'province_id',
         'city_id',
    ];



    /**
     * @inheritdoc
     */
    public function administrativeRoles(): array
    {
        return [
             RegistryUnityRole::ROLE_PRESIDENT,
             RegistryUnityRole::ROLE_DEPUTY,
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_TRANSPLANT_EXPERT,
             RegistryUnityRole::ROLE_CHIEF_NEUROLOGIST_SECONDER,
             RegistryUnityRole::ROLE_CHIEF_NEUROSURGEONS_SECONDER,
             RegistryUnityRole::ROLE_CHIEF_INTERNIST_SECONDER,
             RegistryUnityRole::ROLE_CHIEF_ANESTHESIOLOGIST_SECONDER,
             RegistryUnityRole::ROLE_CHIEF_SECONDER,
             RegistryUnityRole::ROLE_CHIEF_LEGAL_MEDICINE,
             RegistryUnityRole::ROLE_CHIEF_PROTECTOR,
        ];
    }



    /**
     * @inheritdoc
     */
    public function dependantRoles(): array
    {
        $array = [
             RegistryUnityRole::ROLE_LEGAL_MEDICINE,
             RegistryUnityRole::ROLE_TRANSPORTER,
             RegistryUnityRole::ROLE_CONSULTANT,
             RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR,
             RegistryUnityRole::ROLE_HARVESTER,
             RegistryUnityRole::ROLE_PROTECTOR,
        ];

        return array_merge($array, static::dependantSeconderRoles());
    }



    /**
     * get the list of dependant roles for seconder jobs
     *
     * @return array
     */
    public static function dependantSeconderRoles(): array
    {
        return [
             RegistryUnityRole::ROLE_NEUROLOGIST_SECONDER,
             RegistryUnityRole::ROLE_NEUROSURGEONS_SECONDER,
             RegistryUnityRole::ROLE_INTERNIST_SECONDER,
             RegistryUnityRole::ROLE_ANESTHESIOLOGIST_SECONDER,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toListResource(): array
    {
        return [
             "id"                    => $this->hashid,
             "name"                  => $this->name,
             'city'                  => $this->city_id ? $this->city->hashid : null,
             'city_title'            => $this->city_id ? $this->city->title_fa : null,
             'province'              => $this->province_id ? $this->province->hashid : null,
             'province_title'        => $this->province_id ? $this->province->title_fa : null,
             "president"             => $this->president->toFront(),
             "deputy"                => $this->deputy->toFront(),
             "opu_count"             => $this->opus()->count(),
             "hospital_count"        => $this->hospitals->count(),
             "expert-in-charge"      => $this->expert_in_charge->toFront(),
             "is_active"             => is_null($this->deactivated_at),
             "coordinator_count"     => $this->coordinators()->count(),
             "inspector_count"       => $this->inspectorClinicals()->count() + $this->inspectorTels()->count(),
             "seconder_count"        => $this->seconders()->count(),
             "consultant_count"      => $this->consultants()->count(),
             "legal_medicines_count" => $this->legalMedicines()->count(),
             "_can_view"             => $this->canView(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function toSingleResource(): array
    {
        return [
             "id"                    => $this->hashid,
             "type"                  => $this->type,
             "university_type"       => $this->university_type,
             "name"                  => $this->name,
             'city'                  => $this->city_id ? $this->city->hashid : null,
             'city_title'            => $this->city_id ? $this->city->title_fa : null,
             'province'              => $this->province_id ? $this->province->hashid : null,
             'province_title'        => $this->province_id ? $this->province->title_fa : null,
             "president"             => $this->president->toFront(),
             "deputy"                => $this->deputy->toFront(),
             "expert-in-charge"      => $this->expert_in_charge->toFront(),
             "donor-expert"          => $this->donor_expert->toFront(),
             "address"               => $this->address,
             "phones"                => $this->phones,
             "postal_code"           => $this->postal_code,
             "website"               => $this->website,
             "opu_count"             => $this->opus()->count(),
             "hospital_count"        => $this->hospitals->count(),
             "university-supervisor" => $this->supervisorsResource(),
             "coordinator_count"     => $this->coordinators()->count(),
             "inspector_count"       => $this->inspectorClinicals()->count() + $this->inspectorTels()->count(),
             "seconder_count"        => $this->seconders()->count(),
             "consultant_count"      => $this->consultants()->count(),
             "legal_medicines_count" => $this->legalMedicines()->count(),
             "created_at"            => $this->created_at,
             "is_active"             => is_null($this->deactivated_at),
        ];
    }



    /**
     * get resource list of supervisors
     *
     * @return array
     */
    public function supervisorsResource(): array
    {
        $supervisors = $this->university_supervisors;

        if (!$supervisors) {
            return [];
        }

        return $supervisors->map(function ($supervisor) {
            return $supervisor->toFront();
        })
                           ->toArray()
             ;
    }



    /**
     * get a builder of all the universities superior to the given unity ids
     *
     * @param array $unities
     *
     * @return Builder
     */
    public static function superiorsOf(array $unities)
    {
        $list1 = RegistrySharedUnity::whereIn("id", $unities)->where("type", "university")->pluck("id")->toArray();
        $list2 = RegistrySharedUnity::whereIn("id", $unities)
                                    ->where("type", "!=", "university")
                                    ->pluck("university_id")
                                    ->toArray()
        ;

        return model("registry-university")->whereIn("id", array_merge($list1, $list2));
    }
}
