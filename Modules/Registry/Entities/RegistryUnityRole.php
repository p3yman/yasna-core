<?php

namespace Modules\Registry\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Modules\Registry\Entities\Traits\UnityRoleResourceTrait;
use Modules\Registry\Events\NewUnityRoleAssigned;
use Modules\Yasna\Services\YasnaModel;
use stdClass;

/**
 * Class RegistryUnityRole
 * serves as a pivot-like table to hold one-to-one relations between users and unities,
 * keeping the old relations as archived
 * @method Builder where($field, $value)
 *
 * @package Modules\Registry\Entities
 */
class RegistryUnityRole extends YasnaModel
{
    use SoftDeletes;
    use UnityRoleResourceTrait;

    /*-----------------------------------------------
    | University Roles ...
    */
    const ROLE_PRESIDENT             = 'president';
    const ROLE_DEPUTY                = 'deputy';
    const ROLE_EXPERT_IN_CHARGE      = 'expert-in-charge';
    const ROLE_DONOR_EXPERT          = 'donor-expert';
    const ROLE_TRANSPLANT_EXPERT     = 'transplant-expert';
    const ROLE_TRANSPORTER           = "transporter";
    const ROLE_CONSULTANT            = "consultant";
    const ROLE_UNIVERSITY_SUPERVISOR = 'university-supervisor';

    const ROLE_CHIEF_SECONDER                  = "chief-seconder";
    const ROLE_CHIEF_NEUROLOGIST_SECONDER      = "chief-neurologist-seconder";
    const ROLE_CHIEF_NEUROSURGEONS_SECONDER    = "chief-neurosurgeons-seconder";
    const ROLE_CHIEF_INTERNIST_SECONDER        = "chief-internist-seconder";
    const ROLE_CHIEF_ANESTHESIOLOGIST_SECONDER = "chief-anesthesiologist-seconder";
    const ROLE_NEUROLOGIST_SECONDER            = "neurologist-seconder";
    const ROLE_NEUROSURGEONS_SECONDER          = "neurosurgeons-seconder";
    const ROLE_INTERNIST_SECONDER              = "internist-seconder";
    const ROLE_ANESTHESIOLOGIST_SECONDER       = "anesthesiologist-seconder";

    const ROLE_CHIEF_LEGAL_MEDICINE = "chief-legal-medicine";
    const ROLE_LEGAL_MEDICINE       = "legal-medicine";


    /*-----------------------------------------------
    | OPU Roles ...
    */
    const ROLE_OPU_MANAGER        = 'opu-manager';
    const ROLE_CHIEF_COORDINATOR  = 'chief-coordinator';
    const ROLE_COORDINATOR        = 'coordinator';
    const ROLE_CHIEF_PROTECTOR    = 'chief-protector';
    const ROLE_PROTECTOR          = 'protector';
    const ROLE_HARVESTER          = 'harvester';
    const ROLE_INSPECTOR_TEL      = 'inspector-tel';
    const ROLE_INSPECTOR_CLINICAL = 'inspector-clinical';
    const ROLE_OPU_SUPERVISOR     = 'opu-supervisor';


    /*-----------------------------------------------
    | Hospital Roles ...
    */
    const ROLE_HOSPITAL_PRESIDENT = 'hospital-president';
    const ROLE_HOSPITAL_DEPUTY    = 'hospital-deputy';

    /*-----------------------------------------------
    | Ward Roles ...
    */
    const ROLE_CHIEF_WARD = 'chief-ward';


    /**
     * @var stdClass|null
     */
    protected $unity_record;



    /**
     * get the related user instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * get the related university (if present)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryUniversity", "unity_id");
    }



    /**
     * get a safe instance of the user
     *
     * @return \App\Models\User
     */
    public function getSafeUser()
    {
        $user = $this->user;

        if (!$user) {
            $user = user(-1);
        }

        return $user;
    }



    /**
     * get the first
     *
     * @param int    $unity_id
     * @param string $role
     *
     * @return \App\Models\RegistryUnityRole
     */
    public static function locateByUnity(int $unity_id, string $role)
    {
        $record = static::where("unity_id", $unity_id)->where("role", $role)->first();

        if (!$record) {
            /** @var \App\Models\RegistryUnityRole $record */
            $record = model("RegistryUnityRole");
        }

        return $record;
    }



    /**
     * assign a role
     *
     * @param int    $unity_id
     * @param int    $user_id
     * @param string $role
     * @param bool   $should_archive_currents
     *
     * @return bool
     */
    public static function assign(int $unity_id, int $user_id, string $role, bool $should_archive_currents): bool
    {
        /*-----------------------------------------------
        | Bypass if already exist ...
        */
        if (static::alreadyExists($unity_id, $user_id, $role)) {
            return true;
        }

        /*-----------------------------------------------
        | Assignment ...
        */
        $data  = compact('unity_id', 'user_id', 'role');
        $model = (new static)->newInstance()->batchSave($data);

        if ($model->exists) {
            static::archiveCurrentRoles($should_archive_currents, $unity_id, $role, $user_id);
            event(new NewUnityRoleAssigned($model));
        }

        /*-----------------------------------------------
        | Feedback ...
        */
        return $model->exists;
    }



    /**
     * archive a role assignment
     *
     * @param int    $unity_id
     * @param int    $user_id
     * @param string $role
     *
     * @return bool
     */
    public static function revoke(int $unity_id, int $user_id, string $role): bool
    {
        $deleted = static::where("role", $role)
                         ->where('unity_id', $unity_id)
                         ->where("user_id", $user_id)
                         ->delete()
        ;

        return boolval($deleted);
    }



    /**
     * check if the given user already has the given role in the given unity
     *
     * @param int    $unity_id
     * @param int    $user_id
     * @param string $role
     *
     * @return bool
     */
    public static function alreadyExists(int $unity_id, int $user_id, string $role): bool
    {
        return static::where("role", $role)
                     ->where('unity_id', $unity_id)
                     ->where("user_id", $user_id)
                     ->exists()
             ;
    }



    /**
     * soft delete all current relations of the given role
     *
     * @param bool   $condition
     * @param int    $unity_id
     * @param string $role
     * @param int    $current_user
     *
     * @return int
     */
    public static function archiveCurrentRoles(bool $condition, int $unity_id, string $role, int $current_user = 0)
    {
        if (!$condition) {
            return 0;
        }

        return static::where("role", $role)
                     ->where('unity_id', $unity_id)
                     ->where("user_id", "!=", $current_user)
                     ->delete()
             ;
    }



    /**
     * soft delete a role of a given user.
     *
     * @param int    $unity_id
     * @param string $role
     * @param int    $user
     *
     * @return int
     */
    public static function archiveRoleOfUser(
         int $unity_id,
         string $role,
         int $user = 0
    ) {
        return static::where("role", $role)
                     ->where('unity_id', $unity_id)
                     ->where("user_id", $user)
                     ->whereNull("deleted_at")
                     ->delete()
             ;
    }



    /**
     * A One-to-Many Relationship with proper unity model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unity()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'RegistrySharedUnity');
    }



    /**
     * Return an instance of Registry* unity, based on the destination record type
     *
     * @return YasnaModel|null
     */
    public function getUnityAttribute()
    {
        // if the relation already loaded return it
        if ($this->relationLoaded('unity')) {
            return $this->returnTypedRelation('unity');
        }

        // otherwise query for it, then save it
        $unity = $this->unity()->first();
        $this->setRelation('unity', $unity);
        if (!$unity) { // if no result return null
            return null;
        }

        // otherwise convert the general method to typed and return it
        return $this->returnTypedRelation('unity');
    }



    /**
     * Returns the full namespace of the proper unity model's class.
     *
     * @return string
     */
    public function unityModelClass()
    {
        $type         = $this->unityType();
        $model_name   = 'Registry' . studly_case($type);
        $model_object = model($model_name);

        return get_class($model_object);
    }



    /**
     * Returns the type of the related unity.
     *
     * @return string
     */
    public function unityType()
    {
        return $this->getUnityRecord()->type;
    }



    /**
     * Returns the information of the related unity as a stdClass object.
     *
     * @return \Illuminate\Database\Query\Builder|mixed|stdClass|null
     */
    protected function getUnityRecord()
    {
        if (!$this->unity_record) {
            $this->unity_record = DB::table('registry_unities')->find($this->unity_id);
        }

        return $this->unity_record;
    }



    /**
     * Converts model to a resource array in the user's roles view.
     *
     * @return array
     */
    public function toUserRolesResource(): array
    {
        $unity = $this->unity;

        return [
             'role'       => $this->role,
             'unity'      => $unity->hashid,
             'unity_type' => $this->unityType(),
        ];
    }



    /**
     * get a builder or users assigned to the selected role(s)
     *
     * @param int          $unity_id
     * @param string|array $roles
     * @param bool         $with_trashed
     *
     * @return Builder
     */
    public static function assigneesOf(int $unity_id, $roles, $with_trashed = false)
    {
        $unities = model('registry-shared-unity', $unity_id)->allChildren(true);
        $pivots = static::whereIn("role", (array)$roles);
        $pivots = $pivots->whereIn('unity_id', $unities);
        if ($with_trashed) {
            $pivots = $pivots->withTrashed();
        }

        $users = array_unique($pivots->pluck("user_id")->toArray());

        return user()->whereIn("id", $users);
    }



    /**
     * Return a unity specified model from a given shared relation
     *
     * @param string $relation
     *
     * @return YasnaModel|null
     */
    protected function returnTypedRelation(string $relation)
    {
        /* @var \App\Models\RegistrySharedUnity $unity */
        $unity = $this->getRelation($relation);
        if (!$unity) {
            return null;
        }

        $new_unity = model(sprintf("registry-%s", $unity->type));
        $new_unity->setRawAttributes($unity->getAttributes());
        $this->setRelation($relation, $new_unity);

        return $new_unity;
    }
}
