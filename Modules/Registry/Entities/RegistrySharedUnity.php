<?php

namespace Modules\Registry\Entities;

use Modules\Yasna\Services\YasnaModel;

class RegistrySharedUnity extends YasnaModel
{
    protected $table = "registry_unities";



    /**
     * get ids of all children.
     *
     * @param bool $including_self
     *
     * @return array
     */
    public function allChildren($including_self = false)
    {
        $children = [];

        if ($this->type == "university") {
            $unity     = university($this->id);
            $opus      = $unity->opus->pluck('id')->toArray();
            $hospitals = $unity->hospitals->pluck('id')->toArray();
            $wards     = $unity->wards->pluck('id')->toArray();
            $children  = array_merge($opus, $hospitals, $wards);
        } elseif ($this->type == "opu") {
            $unity     = opu($this->id);
            $hospitals = $unity->hospitals->pluck('id')->toArray();
            $wards     = $unity->wards->pluck('id')->toArray();
            $children  = array_merge($hospitals, $wards);
        } elseif ($this->type == "hospital") {
            $unity    = opu($this->id);
            $wards    = $unity->wards->pluck('id')->toArray();
            $children = $wards;
        }

        if ($including_self) {
            $children[] = $this->id;
        }

        return $children;
    }
}
