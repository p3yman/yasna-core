<?php

namespace Modules\Registry\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Registry\Entities\Traits\CaseAttributesTrait;
use Modules\Registry\Entities\Traits\CaseCensorTrait;
use Modules\Registry\Entities\Traits\CaseDemographicTrait;
use Modules\Registry\Entities\Traits\CaseElectorTrait;
use Modules\Registry\Entities\Traits\CaseFetchTrait;
use Modules\Registry\Entities\Traits\CaseMetaFieldsTrait;
use Modules\Registry\Entities\Traits\CaseMutatorsTrait;
use Modules\Registry\Entities\Traits\CaseOrgansTrait;
use Modules\Registry\Entities\Traits\CasePermissionsTrait;
use Modules\Registry\Entities\Traits\CaseRelationsTrait;
use Modules\Registry\Entities\Traits\CaseSimilarityTrait;
use Modules\Registry\Entities\Traits\CaseSortTrait;
use Modules\Registry\Entities\Traits\CaseToResourceTrait;
use Modules\Registry\Events\NewCaseCreated;
use Modules\Uploader\Services\Entity\FilesTrait;
use Modules\Yasna\Services\YasnaModel;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class RegistryCase
 *
 * @package Modules\Registry\Entities
 * @property RegistryOpu        $opu
 * @property RegistryWard       $ward
 * @property RegistryHospital   $hospital
 * @property RegistryUniversity $university
 * @property string             $file_no
 * @property string             $remarks
 * @property string             $identification_type
 * @property string             $consciousness_disorder_cause
 * @property string             $case_situation
 * @property int                $gcs
 * @property int                $gcs_on_identification
 * @property int                $reflex_cornea
 * @property int                $reflex_breathing
 * @property int                $reflex_pupil
 * @property int                $reflex_face
 * @property int                $reflex_body
 * @property int                $reflex_doll
 * @property int                $reflex_gag
 * @property int                $reflex_cough
 * @property bool               $stability_sedation
 * @property string             $stability_sedation_type
 * @property string             $stability_sedation_amount
 */
class RegistryCase extends YasnaModel
{
    use CaseDemographicTrait;
    use CaseRelationsTrait;
    use CaseCensorTrait;
    use CaseToResourceTrait;
    use CaseAttributesTrait;
    use CaseMetaFieldsTrait;
    use CaseElectorTrait;
    use CaseSortTrait;
    use CasePermissionsTrait;
    use CaseMutatorsTrait;
    use CaseFetchTrait;
    use CaseOrgansTrait;
    use CaseSimilarityTrait;
    use FilesTrait;
    use SoftDeletes;

    protected $dates = [
         'identified_at',
         'stability_sedation_time',
         'birth_date',
    ];



    /**
     * List of relations which is needed to show a case file completely and must be eager loaded.
     *
     * @return array
     */
    public static function requiredRelations(): array
    {
        return [
             'hospital',
             'hospital.city',
             'hospital.province',
             'creator',
             'coordinator',
             'inspector',
             'opu',
             'ward',
             'university',
        ];
    }



    /**
     * override parent batchSave() to house Demographics as well.
     *
     * @param array|YasnaRequest $request
     * @param array              $overflow_parameters
     *
     * @return $this
     */
    public function batchSave($request, $overflow_parameters = [])
    {
        $data = $this->batchSavePurification($request, $overflow_parameters);

        /** @var \App\Models\RegistryCase $saved */
        $saved = parent::batchSave($data);

        if ($saved->exists) {
            $saved->saveDemographics($data);
            event(new NewCaseCreated($saved));
        }

        return $saved;
    }



    /**
     * attach the files received via the request
     *
     * @param array $request
     */
    public function attachRequestFiles(array $request)
    {
        $file_fields = [
             "biochemistry_tests_files",
             "bacteria_tests_files",
             "virology_tests_files",
        ];

        foreach ($request as $field => $files) {
            if (!in_array($field, $file_fields)) {
                continue;
            }

            $files = (array)$files;
            foreach ($files as $file) {
                uploader()->file($file)->attachTo($this, $field);
            }
        }
    }



    /**
     * get main meta fields
     *
     * @return array
     */
    public function getMainMetaFields()
    {
        return [
             'consciousness_disorder_tumor',
             "surgical_history_details",
             "drug_details",
             "received_serum_details",
             'received_antibiotics_remarks',
             "inotrop_eco",
             "inotrop_icu",
             "inotrop_surgery",
             'cpr_details',
             'shock_details',
             "emergency_phones",
             "organs",
             "donated_photo",
        ];
    }
}
