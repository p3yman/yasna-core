<?php

namespace Modules\Registry\Entities\Abstracts;

use Modules\Registry\Entities\Traits\UnityCasesTrait;
use Modules\Registry\Entities\Traits\UnityCrossRelationsTrait;
use Modules\Registry\Entities\Traits\UnityElectorTrait;
use Modules\Registry\Entities\Traits\UnityScopeTrait;
use Modules\Registry\Entities\Traits\UnityRolesTrait;
use Modules\Registry\Entities\Traits\UnityUsersTrait;
use Modules\Registry\Events\UnityCreated;
use Modules\Registry\Events\UnityUpdated;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class UnityAbstract extends YasnaModel
{
    use SoftDeletes;
    use UnityElectorTrait;
    use UnityScopeTrait;
    use UnityRolesTrait;
    use UnityUsersTrait;
    use UnityCasesTrait;
    use UnityCrossRelationsTrait;

    protected $table = "registry_unities";

    /**
     * Fields which must be filled to suppose the model as a completed record. If a role must be checked, put a `role-`
     * on front of it (e.g. `role-chief-ward`).
     *
     * @var array $must_be_filled_fields
     */
    protected $fields_must_filled = [];



    /**
     * get an array of valid types
     *
     * @return array
     */
    public static function validTypes(): array
    {
        return ['university', 'opu', 'hospital', 'ward'];
    }



    /**
     * get an array of the phone numbers
     *
     * @return array
     */
    public function phonesArray()
    {
        return explode_not_empty(",", $this->phones);
    }



    /**
     * Checks the model is completely filled
     *
     * @return bool
     */
    public function isComplete(): bool
    {
        foreach ($this->fields_must_filled as $field) {
            if (str_contains($field, 'role-')) {
                $role = str_after($field, 'role-');
                if (!$this->hasAssignedRole($role)) {
                    return false;
                }
            } elseif (is_null($this->{$field}) || trim($this->{$field}) === '') {
                return false;
            }
        }

        return true;
    }



    /**
     * Checks the model is not completely filled
     *
     * @return bool
     */
    public function isNotComplete(): bool
    {
        return !$this->isComplete();
    }



    /**
     * determine if the online user can create/edit the unity
     *
     * @return bool
     */
    public function canCreateOrEdit(): bool
    {
        if ($this->exists) {
            return $this->canEdit();
        }

        return $this->canCreate();
    }



    /**
     * determine if the online user can create a unity
     *
     * @return bool
     */
    public function canCreate(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can view a unity
     *
     * @return bool
     */
    public function canView(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can view a list of all user assigned to the unity
     *
     * @return bool
     */
    public function canViewAssignedRoles(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can edit the unity
     *
     * @return bool
     */
    public function canEdit(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can delete the unity
     *
     * @return bool
     */
    public function canDelete(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can deactivate the unity
     *
     * @return bool
     */
    public function canDeactivate(): bool
    {
        return $this->canDelete();
    }



    /**
     * determine if the online user can assign the given role to somebody
     *
     * @param string $role
     *
     * @return bool
     */
    public function canAssign(string $role): bool
    {
        if (!$this->isRoleDefined($role)) {
            return false;
        }

        $method_name = camel_case("canAssign-$role");
        if ($this->hasMethod($method_name)) {
            return $this->{$method_name}();
        }

        return user()->isSuperadmin();
    }



    /**
     * get an array of all the roles the model can handle
     *
     * @return array
     */
    public function userRoles(): array
    {
        return array_merge($this->dependantRoles(), $this->administrativeRoles());
    }



    /**
     * Returns the city which the unity is on it.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'city_id');
    }



    /**
     * Returns the province which the unity is on it.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'province_id');
    }



    /**
     * @inheritdoc
     */
    public function batchSave($request, $overflow_parameters = [])
    {
        $old_model = $this;
        $new_model = parent::batchSave($request, $overflow_parameters);

        if ($old_model->exists) {
            event(new UnityUpdated($new_model, $old_model));
        } else {
            event(new UnityCreated($new_model));
        }

        return $new_model;
    }



    /**
     * check if the given role is defined in the model
     *
     * @param string $role
     *
     * @return bool
     */
    protected function isRoleDefined(string $role): bool
    {
        return in_array($role, $this->userRoles());
    }



    /**
     * get the desired content of the type field in the children models
     *
     * @return string
     */
    protected function getDesiredType(): string
    {
        return static::getType();
    }



    /**
     * get the type of the model
     *
     * @return string
     */
    protected static function getType()
    {
        return kebab_case(str_after(static::className(), "Registry"));
    }



    /**
     * get an array of administrative roles in which one use can be assigned at a time
     *
     * @return array
     */
    abstract public function administrativeRoles(): array;



    /**
     * get an array of dependant roles in which multiple users can be assigned at a time
     *
     * @return array
     */
    abstract public function dependantRoles(): array;
}
