<?php

namespace Modules\Registry\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Registry\Entities\Traits\DemographicCluesTrait;
use Modules\Registry\Entities\Traits\DemographicDetailsTrait;
use Modules\Registry\Entities\Traits\DemographicGetTrait;
use Modules\Registry\Entities\Traits\DemographicSetTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class RegistryDemographic represents an active record model of the
 * `registry_demographics` table.
 *
 * @package Modules\Registry\Entities
 */
class RegistryDemographic extends YasnaModel
{
    use SoftDeletes;
    use DemographicCluesTrait;
    use DemographicGetTrait;
    use DemographicSetTrait;
    use DemographicDetailsTrait;

    protected $casts = [
         "value_timestamp" => "datetime",
    ];



    /**
     * get the highest batch number for the specific user
     *
     * @param int $case_id
     *
     * @return int
     */
    public static function highestBatch(int $case_id): int
    {
        $latest = static::where("case_id", $case_id)->orderBy("batch", "desc")->first();

        if (!$latest) {
            return 0;
        }

        return $latest->batch;
    }



    /**
     * get the one-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function case()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryCase");
    }



    /**
     * get main meta fields
     *
     * @return array
     */
    public function getMainMetaFields()
    {
        return [
             "details",
        ];
    }
}
