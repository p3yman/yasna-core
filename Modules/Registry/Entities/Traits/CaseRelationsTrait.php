<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait CaseRelationsTrait
{
    /**
     * get relationship instance to the university
     *
     * @return BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryUniversity");
    }



    /**
     * get university name
     *
     * @return null|string
     */
    public function getUniversityNameAttribute()
    {
        $university = $this->university;

        return $university ? $university->name : null;
    }



    /**
     * get relationship instance to the opu
     *
     * @return BelongsTo
     */
    public function opu()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryOpu");
    }



    /**
     * get opu name
     *
     * @return null|string
     */
    public function getOpuNameAttribute()
    {
        $opu = $this->opu;

        return $opu ? $opu->name : null;
    }



    /**
     * get relationship instance to the opu
     *
     * @return BelongsTo
     */
    public function hospital()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryHospital");
    }



    /**
     * get hospital name
     *
     * @return null|string
     */
    public function getHospitalNameAttribute()
    {
        $hospital = $this->hospital;

        return $hospital ? $hospital->name : null;
    }



    /**
     * get relationship instance to the opu
     *
     * @return BelongsTo
     */
    public function ward()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryWard");
    }



    /**
     * get ward name
     *
     * @return null|string
     */
    public function getWardNameAttribute()
    {
        $ward = $this->ward;

        return $ward ? $ward->name : null;
    }



    /**
     * get relationship instance to the inspector
     *
     * @return BelongsTo
     */
    public function inspector()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * get relationship instance to the coordinator
     *
     * @return BelongsTo
     */
    public function coordinator()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * get relationship instance to the country of nationality
     *
     * @return BelongsTo
     */
    public function nation()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Division", "nationality_id");
    }



    /**
     * get relationship instance to the country of residency
     *
     * @return BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Division", "country_id");
    }



    /**
     * get relationship instance to the province of residency
     *
     * @return BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Division", "province_id");
    }



    /**
     * get relationship instance to the city of residency
     *
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Division", "city_id");
    }



    /**
     * Return a one-to-one relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'User', 'created_by');
    }
}
