<?php

namespace Modules\Registry\Entities\Traits;

trait CaseMetaFieldsTrait
{
    /**
     * contact information of case emergency phones
     *
     * @return array
     */
    public function emergencyPhonesMetaFields(): array
    {
        return [
             'emergency_phones',
        ];
    }



    /**
     * souvenir information of the case (like photo, ...)
     *
     * @return array
     */
    public function caseSouvenirMetaFields(): array
    {
        return [
             'photo',
        ];
    }
}
