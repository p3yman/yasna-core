<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryDemographic;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use function is_null;

trait CaseDemographicTrait
{
    protected static $creators = [];



    /**
     * get one-to-many relationship to the demographics table
     *
     * @return HasMany
     */
    public function demographics()
    {
        return $this->hasMany(MODELS_NAMESPACE . "RegistryDemographic", "case_id");
    }



    /**
     * Return an array of demographics information batches
     *
     * @return array
     */
    public function toDemographicsArray(): array
    {
        return $this->demographics
             ->groupBy('batch')
             ->map(function ($collection) {
                 return $collection->map(function ($demography) {
                     $demography->spreadMeta();
                     $user = static::getCreator($demography->created_by);

                     $result = [
                          'case'                => hashid($demography->case_id),
                          'created_at'          => $demography->created_at->timestamp,
                          'created_by'          => is_null($user) ? null : $user->hashid,
                          '_created_by_details' => [
                               "hashid"    => is_null($user) ? null : $user->hashid,
                               "full_name" => is_null($user) ? null : $user->full_name,
                          ],
                          $demography->clue     => $demography->resource_value,
                     ];

                     if ($demography->effected_at) {
                         $result[$demography->clue . '_effective_date'] = Carbon::parse($demography->effected_at)->timestamp;
                     }

                     if ($demography->details) {
                         $result["_{$demography->clue}_details"] = $demography->details;
                     }

                     return $result;
                 })
                                   ->collapse()
                      ;
             })
             ->reverse()
             ->toArray()
             ;
    }



    /**
     * get an array of defined demographic batches
     *
     * @return array
     */
    public function demographicBatches()
    {
        return $this->demographics()->groupBy("batch")->get()->pluck("batch")->toArray();
    }



    /**
     * save demographic data of a user
     *
     * @param array $data
     *
     * @return int
     */
    public function saveDemographics(array $data): int
    {
        /** @var RegistryDemographic $model */
        $model = model("RegistryDemographic");
        $batch = $model::highestBatch($this->id) + 1;
        $saved = 0;

        foreach ($data as $clue => $value) {
            $effected_at = $this->getEffectedAtFromSubmittedData($clue, $data);
            $file_hashid = $this->getSupportingFileFromSubmittedData($clue, $data);

            $saved += $model->setValue($this->id, $batch, $clue, $value, $effected_at, $file_hashid);
        }

        return $saved;
    }



    /**
     * get `effected_at` field from the submitted data for a given clue
     *
     * @param string $clue
     * @param array  $data
     *
     * @return string|Carbon|null
     */
    protected function getEffectedAtFromSubmittedData(string $clue, array $data)
    {
        $field = $clue . "_date";

        return isset($data[$field]) ? $data[$field] : null;
    }



    /**
     * get `supporting_file` field from the submitted data for a given clue
     *
     * @param string $clue
     * @param array  $data
     *
     * @return string|Carbon|null
     */
    protected function getSupportingFileFromSubmittedData(string $clue, array $data)
    {
        $field = $clue . "_file";

        return isset($data[$field]) ? $data[$field] : null;
    }



    /**
     * Return creators of demographics collection based on $user_id
     *
     * @param int|string $user_id
     *
     * @return User|null
     */
    protected static function getCreator($user_id): ?User
    {
        if (isset(static::$creators[$user_id])) {
            return static::$creators[$user_id];
        }

        static::$creators[$user_id] = $user = model('user', $user_id);

        return $user;
    }

}
