<?php

namespace Modules\Registry\Entities\Traits;

trait DemographicCluesTrait
{
    /**
     * get the slug of all the valid clues
     *
     * @return array
     */
    public static function validClues(): array
    {
        return array_merge(
             static::booleanClues(),
             static::numericClues(),
             static::stringClues(),
             static::textClues(),
             static::timestampClues(),
             static::fileClues(),
             static::arrayClues()
        );
    }



    /**
     * check if the given clue is valid
     *
     * @param string $clue
     *
     * @return bool
     */
    protected static function isClueValid(string $clue): bool
    {
        return in_array($clue, static::validClues());
    }



    /**
     * get the field type of the given clue
     *
     * @param string $clue
     *
     * @return string
     */
    protected static function getClueDataType(string $clue): ?string
    {
        if (in_array($clue, static::numericClues())) {
            return "numeric";
        }

        if (in_array($clue, static::stringClues())) {
            return "string";
        }

        if (in_array($clue, static::textClues())) {
            return "text";
        }

        if (in_array($clue, static::timestampClues())) {
            return "timestamp";
        }

        if (in_array($clue, static::fileClues())) {
            return "file";
        }

        if (in_array($clue, static::arrayClues())) {
            return "array";
        }

        return null;
    }



    /**
     * get the field name of the given clue
     *
     * @param string $clue
     *
     * @return string
     */
    protected static function getClueFieldName(string $clue): string
    {
        $type = static::getClueDataType($clue);

        switch ($type) {
            case "array":
                return "value_string";
            case "file":
                return "value_text";

            default:
                return "value_" . $type;
        }
    }



    /**
     * get the slug of all the array-type clues (made public on purpose)
     *
     * @return array
     */
    public static function arrayClues(): array
    {
        return [
             "malignancy_organs",
             "malignancy_others",
             'received_antibiotics',
             'received_serum_details',
             'infection_organ_engagement',
             "inotrop_eco",
             "inotrop_icu",
             "inotrop_surgery",
             'cpr_details',
             'shock_details',
             "surgical_history_details",
             "emergency_phones",
             "organs",
        ];
    }



    /**
     * get the slug of all the numeric-type clues
     *
     * @return array
     */
    private static function numericClues(): array
    {
        return [
             "university_id",
             "opu_id",
             "hospital_id",
             "ward_id",
             "inspector_id",
             "coordinator_id",
             "proximate_age",
             "case_evidenced_by",
             "case_identified_by",
             "nationality_id",
             "country_id",
             "province_id",
             "city_id",
             "gender",
             "weight",
             "height",
             "is_suicide",
             "gcs",
             "gcs_on_arrival",
             "gcs_on_identification",
             "reflex_cough",
             "reflex_gag",
             "reflex_doll",
             "reflex_body",
             "reflex_face",
             "reflex_pupil",
             "reflex_cornea",
             "reflex_breathing",
             "reflex_breathing_rhythm",
             "stability_dbp",
             "stability_sbp",
             "stability_out",
             "stability_o2sat",
             "stability_fio2",
             "stability_rr",
             "stability_pr",
             "stability_t",
             "stability_sedation",
             "test_ca",
             "test_cr",
             "test_bs",
             "test_plt",
             "test_wbc",
             "test_hgb",
             "test_ast",
             "test_alt",
             "test_urea",
             "test_bun",
             "test_k",
             "test_na",
             'test_alk_p',
             'test_bili_t',
             'test_bili_d',
             'test_mg',
             'test_pt',
             'test_ptt',
             'test_inr',
             'test_troponin_i',
             'test_hemoglobin_a1c',
             "test_serum_amylase",
             "test_serum_lipase",
             'test_ph',
             'test_pco2',
             'test_po2',
             'test_be',
             'test_o2sat',
             'test_hco3',
             "hypertension_time",
             "diabetes_time",
             'received_insulin_daily_dose',
             'bd_confirmed_by_1',
             'bd_confirmed_by_2',
             'bd_confirmed_by_3',
             'bd_confirmed_by_4',
             'legal_medicine_1',
             'legal_medicine_2',
             'donation_agreement_confirmed_by',
             'identified_by',
             'created_by',
             'updated_by',
             'deleted_by',
             'had_risky_sex',

             /*-----------------------------------------------
             | Organ Numeric Fields ...
             */
             "heart_echocardiography_consultant",
             "heart_tests_ef",
             "heart_tests_lvh",
             "heart_tests_pap",
             "heart_tests_trg",
             "heart_tests_consultant",
             "lung_fio2",
             "lung_peep",
             "lung_tv",
             "lung_rr",
             "lung_o2challenge_fio2",
             "lung_o2challenge_po2",
             "lung_recruitment_fio2",
             "lung_recruitment_po2",
             "kidney_size_left_cm",
             "kidney_size_right_cm",
             "kidney_biopsy_glomerulus_count",
             "kidney_biopsy_glomerulus_percent",
             "liver_mass_count",
             "liver_mass_size",
             "liver_size_cm",
             "liver_macro_vesicular_fat",
             "liver_micro_intermediate_vesicular_fat",
        ];
    }



    /**
     * get the slug of the boolean-type clues
     *
     * @return array
     */
    private static function booleanClues(): array
    {
        return [
             "unknown",
             'virology_hbs_ag',
             'virology_hbs_ab',
             'virology_hbc_ab',
             'virology_hcv_ab',
             'virology_hcv_nat',
             'virology_hiv_ab',
             'virology_hiv_nat',
             'virology_htlv_ab',
             'virology_cmv_ab_igm',
             'virology_cmv_ab_igg',
             'virology_ebv_ab_igm',
             'virology_ebv_ab_igg',
             'virology_toxo_ab_igg',
             'virology_toxo_ab_igm',
             'virology_vdrl',
             'bacteria_ua',
             'bacteria_uc',
             'bacteria_bc',
             'bacteria_bal_culture',
             'bacteria_wound_culture',
             "hypertension",
             "hypertension_is_controlled",
             'hypertension_cure_edible',
             'hypertension_cure_non_edible',
             'diabetes',
             'diabetes_is_controlled',
             'diabetes_cure_edible',
             'diabetes_cure_non_edible',
             'malignancy',
             'heart_disease',
             'surgical_history',
             'had_tattoo',
             'had_prison_past',
             'received_serum',
             'received_thyroid_hormone',
             'received_steroid',
             'received_anticonvulsant',
             'received_heparin',
             'received_vasopressin',
             'received_diuretic',
             'received_insulin',
             'infection_treatment_successful',
             'has_donation_card',
             'verbally_allowed_donation',
             "craniotomy",
             "ventriculoatrial_shunt",

             /*-----------------------------------------------
             | Organ Boolean Fields ...
             */
             'heart_organ_filled',
             'heart_prohibit',
             "heart_cardiography",
             "heart_echocardiography",
             "heart_valvular_problem",
             "heart_diastolic_dysfunction",
             'lung_organ_filled',
             "lung_prohibit",
             "lung_recruitment_test",
             "lung_pneumothorax",
             "lung_infiltration",
             "lung_collapse",
             "lung_tumor",
             "lung_ctscan_need",
             "lung_pulmonary_secretion_culture",
             "lung_can_donate_left",
             "lung_can_donate_right",
             "lung_left_abnormal",
             "lung_right_abnormal",
             "kidney_organ_filled",
             "kidney_prohibit",
             "kidney_abnormal_finding",
             "kidney_biopsy",
             "kidney_hla_typing",
             "liver_organ_filled",
             "liver_prohibit",
             "liver_mass",
             "liver_gallstones",
             "liver_bile_ducts_inside",
             "liver_bile_ducts_outside",
             "liver_biopsy",
             "intestine_organ_filled",
             "intestine_prohibit",
             "pancreas_organ_filled",
             "pancreas_prohibit",
        ];
    }



    /**
     * get the slug of all the string-type clues
     *
     * @return array
     */
    private static function stringClues(): array
    {
        return [
             "name_first",
             "name_last",
             "full_name",
             "code_melli",
             "file_no",
             "blood_type",
             "blood_rh",
             "consciousness_disorder_cause",
             "consciousness_disorder_cause_detail",
             "case_situation",
             "case_unfeasible_reason",
             "case_unfeasible_reason_others",
             "case_unfeasible_tumor",
             "clinical_situation",
             "identification_type",
             "stability_sedation_amount",
             "stability_sedation_type",
             "address",
             'bacteria_ua_type',
             "bacteria_ua_other",
             'bacteria_uc_type',
             "bacteria_uc_other",
             'bacteria_bc_type',
             "bacteria_bc_other",
             'bacteria_bal_culture_type',
             "bacteria_bal_culture_other",
             'bacteria_wound_culture_type',
             "bacteria_wound_culture_other",
             'malignancy_pathology_result',
             'heart_diseases_type',
             'heart_diseases_other',
             'received_antihypertensives',
             'received_insulin_type',
             'infection_organ_cultivation',
             'infection_treatment_positive_signs',
             'infection_treatment_negative_consultant',
             "cigars_recent",
             "cigars_took",
             "drug_took_other_recent",
             "drug_took_other_type",
             "drug_took_other",
             "drug_recent",
             "drug_took",
             "alcohol_recent",
             "alcohol_took",

             /*-----------------------------------------------
             | Organ String Fields ...
             */
             "heart_prohibit_by_desc",
             "heart_prohibit_by_electrocardiogram",
             "heart_prohibit_by_angiography",
             "heart_wall_motion_abnormality",
             "heart_valvular_problem_type",
             "heart_valvular_problem_severity",
             "heart_diastolic_dysfunction_severity",
             "heart_lv_size",
             "heart_lv_function",
             "heart_rv_size",
             "heart_rv_function",
             "heart_la_size",
             "heart_la_function",
             "heart_cxr",
             "heart_angiography",
             "lung_prohibit_by_desc",
             "lung_prohibit_by_radiology",
             //"lung_prohibit_by_o2challenge",
             "lung_prohibit_by_bronchoscopy",
             "lung_ventilator_mode",
             "lung_ventilator_mode_other",
             "lung_o2challenge_cause",
             "lung_pneumothorax_reason",
             "lung_infiltration_section",
             "lung_collapse_section",
             "lung_tumor_section",
             "lung_ctscan_reason",
             "lung_ctscan_reason_other",
             "lung_pulmonary_secretion_culture_type",
             "lung_pulmonary_secretion_culture_detail",
             "lung_pulmonary_secretion_culture_antibiotic",
             "lung_pulmonary_secretion_culture_sensitivity",
             "lung_pulmonary_secretion_culture_resistance",
             "lung_left_abnormality",
             "lung_left_abnormality_other",
             "lung_left_abnormality_severity",
             "lung_right_abnormality",
             "lung_right_abnormality_other",
             "lung_right_abnormality_severity",
             "kidney_prohibit_by_desc",
             "kidney_prohibit_by_tests",
             "kidney_prohibit_by_biopsy",
             "kidney_size_left",
             "kidney_size_right",
             "kidney_abnormal_finding_type",
             "kidney_abnormal_finding_other",
             "kidney_abnormal_finding_position",
             "kidney_abnormal_finding_size",
             "kidney_hydronephrosis_severity",
             "kidney_biopsy_type",
             "kidney_biopsy_type_other",
             "kidney_biopsy_fibrosis",
             "kidney_biopsy_vascular_changes",
             "liver_prohibit_by_desc",
             "kidney_biopsy_glomerulus",
             "liver_prohibit_by_sonography",
             "liver_prohibit_by_observation",
             "liver_size",
             "liver_ecosystem",
             "liver_mass_position",
             "liver_mass_position_other",
             "liver_biopsy_type",
             "liver_biopsy_type_other",
             "liver_biopsy_result",
             "liver_ctscan",
             "intestine_prohibit_by_desc",
             "intestine_abdominal_sonography",
             "intestine_abdominal_ctscan",
             "pancreas_prohibit_by_desc",
        ];
    }



    /**
     * get the slug of all the text-type clues
     *
     * @return array
     */
    private static function textClues(): array
    {
        return [
             "consciousness_disorder_cause_remarks",
             "consciousness_disorder_tumor",
             "remarks",
             "reflex_face_remarks",
             "reflex_body_remarks",
             "received_antihypertensives_others",
             "received_others",
             "infection_organ_engagement_text",
             "infection_organ_cultivation_text",
             "received_antibiotics_remarks",
             "received_blood", //<~~ array, but handled differently

             /*-----------------------------------------------
             | Organ Text Fields ...
             */
             "kidney_hla_typing_desc",

        ];
    }



    /**
     * get the slug of all the timestamp-type clues (made public on purpose)
     *
     * @return array
     */
    public static function timestampClues(): array
    {
        return [
             "birth_date",
             "case_identified_at",
             "case_admitted_at",
             "stability_sedation_time",
             'malignancy_detected_at',
             "malignancy_chemotherapy_at",
             'tests_group1_done_at',
             'tests_group2_done_at',
             'tests_group3_done_at',
             'tests_group4_done_at',
             'tests_group5_done_at',
             'tests_group6_done_at',
             'tests_group7_done_at',
             'tests_group8_done_at',
             'tests_group9_done_at',
             'tests_group10_done_at',
             'tests_group11_done_at',
             'tests_group12_done_at',
             "heart_tests_done_at",
             'infection_treatment_started_at',
             'bd_detected_at',
             'bd_claimed_at',
             'bd_confirmed_at_1',
             'bd_confirmed_at_2',
             'bd_confirmed_at_3',
             'bd_confirmed_at_4',
             'legal_medicine_1_confirmed_at',
             'legal_medicine_2_confirmed_at',
             'donation_agreement_confirmed_at',
             'admitted_at',
             'identified_at',
             'died_at',
             'heart_death_detected_at',
             'aortic_clamped_at',
             'donated_at',
             'created_at',
             'updated_at',
             'deleted_at',
        ];
    }



    /**
     * get the slug of all the hashid-type clues which are strings actually.
     *
     * @return array
     */
    public static function fileClues(): array
    {
        return [
             "malignancy_pathology_scan",
             "infection_treatment_doc",
             "bd_confirmation_1",
             "bd_confirmation_2",
             "bd_confirmation_3",
             "bd_confirmation_4",
             "donation_agreement",
             'legal_medicine_1_file',
             'legal_medicine_2_file',
             "donated_photo",

             /*-----------------------------------------------
             | Organ File Fields ...
             */
             "heart_tests_consultant_report",
             "kidney_sonography_report",
             "kidney_biopsy_report",
             "liver_sonography_report",
             "liver_biopsy_report",
             "liver_ctscan_report",
             "liver_hla_typing_report",
             "intestine_abdominal_sonography_report",
             "intestine_abdominal_ctscan_report",
             "heart_echocardiography_report",
             "heart_cardiography_report",
             "heart_cardiography_test",
             "heart_cxr_report",
             "heart_cxr_picture",
             "heart_angiography_report",
             "lung_o2challenge_file",
             "pancreas_hla_typing",
        ];
    }
}
