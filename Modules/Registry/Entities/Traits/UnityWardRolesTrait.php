<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryHospital;
use App\Models\RegistryUnityRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * Class UnityAbstract
 * This trait contain the necessary methods to establish relationship between unities and users.
 * They resolve university roles, even if the unity is not a university itself.
 *
 * @property string           $type
 * @property RegistryHospital $hospital_model
 * @method HasManyThrough getEagerUsers(string $role)
 * @method User getEagerFirstUser(string $role)
 * @method Collection getRelationValue(string $relation)
 * @package Modules\Registry\Entities\Traits
 */
trait UnityWardRolesTrait
{
    /**
     * get the list of chief of the ward (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefWards()
    {
        return $this->getWardUsers(RegistryUnityRole::ROLE_CHIEF_WARD);
    }



    /**
     * get chief attribute
     *
     * @return \App\Models\User
     */
    public function getChiefWardAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_WARD);
    }



    /**
     * get university users, if the unity is not a university itself
     *
     * @param string $role
     *
     * @return HasManyThrough
     */
    private function getWardUsers(string $role)
    {
        if ($this->type == "ward") {
            return $this->getEagerUsers($role);
        }

        return $this->hospital_model->getEagerUsers($role);
    }
}
