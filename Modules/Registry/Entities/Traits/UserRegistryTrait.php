<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 *
 * @property string     $hashid
 * @property string     $name_title
 * @property string     $name_first
 * @property string     $name_last
 * @property string     $full_name
 * @property string     $code_melli
 * @property string     $password
 * @property string     $email
 * @property string     $mobile
 * @property int        $edu_level
 * @property string     $expertise
 * @property string     $job_history
 * @property string     $tel
 * @property int        $gender
 * @property string     $latest_job_position
 * @property bool       $is_retired
 * @property string     $service_location
 * @property string     $second_job
 * @property string     $birth_date
 * @property Collection $unityRoles
 * @property Carbon     $deactivated_at
 * @property int        $deactivated_by
 * @property string     $medical_system_number
 */
trait UserRegistryTrait
{
    use UserSuperadminsTrait;



    /**
     * get relationship instance of unity roles of the current user
     *
     * @param array $specified
     *
     * @return HasMany
     */
    public function unityRoles(array $specified = [])
    {
        $builder = $this->hasMany(MODELS_NAMESPACE . "RegistryUnityRole")
                        ->whereHas('unity', function ($query) {
                            $query->whereNull('deactivated_at');
                        })
        ;

        if (!$specified) {
            return $builder;
        }

        return $builder->whereIn("role", $specified);
    }



    /**
     * get an array of the ids the user is attached to
     *
     * @param array|null $specified
     *
     * @return array
     */
    public function unityIds($specified = null)
    {
        return $this->unityRoles($specified)->pluck('unity_id')->toArray();
    }



    /**
     * check if the user has the requested role in the given unity
     *
     * @param int    $unity_id
     * @param string $role
     *
     * @return bool
     */
    public function hasUnityRole(int $unity_id, string $role): bool
    {
        return RegistryUnityRole::alreadyExists($unity_id, $this->id, $role);
    }



    /**
     * get an array of role slugs of the user
     *
     * @return array
     */
    public function unityRolesArray()
    {
        $roles = $this->unityRoles()->pluck("role")->toArray();

        if ($this->isSuperadmin()) {
            $roles[] = "superadmin";
        }

        if ($this->isDeveloper()) {
            $roles[] = "developer";
        }

        if ($this->hasRole("master")) {
            $roles[] = "master";
        }

        return array_unique($roles);
    }



    /**
     * Indicate user any unity role or not
     *
     * @return bool
     */
    public function hasUnityRoles(): bool
    {
        if ($this->isSuperadmin() or $this->isDeveloper()) {
            return true;
        }

        return $this->unityRoles()->exists();
    }



    /**
     * get a collection of unity roles
     *
     * @return \Illuminate\Support\Collection
     */
    public function detailedUnityRoles()
    {
        return $this->unityRoles->map(function (RegistryUnityRole $pivot) {
            return [
                 "role"  => $pivot->role,
                 "unity" => hashid($pivot->unity_id),
                 //TODO: To be completed with more details as per required.
            ];
        });
    }



    /**
     * serve as a resource of all data the front-end of this project can handle
     *
     * @return array
     */
    public function toFront(): array
    {
        if (!$this->exists) {
            return [];
        }

        if ($this->user_id) {
            $hashid = hashid($this->user_id);
        } else {
            $hashid = $this->hashid;
        }

        $user = [
             "id"                  => $hashid,
             "name_title"          => $this->name_title,
             "name_first"          => $this->name_first,
             "name_last"           => $this->name_last,
             "full_name"           => $this->full_name,
             "code_melli"          => $this->code_melli,
             "email"               => $this->email,
             "mobile"              => $this->mobile,
             "edu_level"           => $this->edu_level,
             "expertise"           => $this->expertise,
             "job_history"         => $this->job_history,
             "tel"                 => $this->tel,
             "gender"              => $this->gender,
             "city"                => $this->city->exists ? $this->city->hashid : "",
             "city_title"          => $this->city->title_fa,
             "province"            => $this->province->exists ? $this->province->hashid : "",
             "province_title"      => $this->province->title_fa,
             "latest_job_position" => $this->latest_job_position,
             "is_retired"          => $this->is_retired,
             "organs"              => $this->organs,
             "edu_field"           => $this->edu_field,
             "service_location"    => $this->service_location,
             "job"                 => $this->job,
             "second_job"          => $this->second_job,
             "universities"        => $this->universitiesResource(),
             "tel_emergency"       => $this->tel_emergency,

             "iran_donor_certificate"               => $this->iran_donor_certificate,
             "iran_donor_certificate_data"          => $this->iran_donor_certificate_data,
             "international_donor_certificate"      => $this->international_donor_certificate,
             "international_donor_certificate_data" => $this->international_donor_certificate_data,
             "letter_file"                          => $this->letter_file,
             "letter_file_data"                     => $this->letter_file_data,
             "tests_files"                          => $this->tests_files,
             "tests_files_data"                     => $this->tests_files_data,

             "medical_system_number" => $this->medical_system_number,
             "work_unity"            => $this->working_unity->hashid,
             "work_unity_title"      => $this->working_unity->name,
             "work_unity_other"      => $this->work_unity_other,

             'must_complete_unity_profile'  => $this->mustCompleteUnityProfile(),
             'unities_for_complete_profile' => $this->unitiesForCompleteProfile(),
             'roles'                        => $this->unityRolesArray(),
             'unities'                      => $this->userUnitiesList(),

             "must_reset_password" => $this->mustResetPassword(),

             "is_active"                 => is_null($this->deactivated_at),
             "letter_issuance"           => is_null($this->letter_issuance) ? null : (new Carbon($this->letter_issuance))->timestamp,
             "letter_expiry"             => is_null($this->letter_expiry) ? null : (new Carbon($this->letter_expiry))->timestamp,
             "letter_expiry_notified_at" => is_null($this->letter_expiry_notified_at) ? null : (new Carbon($this->letter_expiry_notified_at))->timestamp,
             "_can_view"                 => true,
             "_selected_role"            => $this->getMeta('selected_role'),
        ];

        if (!is_null($this->birth_date)) {
            $user['birth_date'] = $this->birth_date->timestamp;
        } else {
            $user['birth_date'] = null;
        }

        return $user;
    }



    /**
     * Return list ready-to-use presentation of a user model.
     *
     * @return array
     */
    public function toListResource(): array
    {
        return $this->toFront();
    }



    /**
     * check the password severity
     *
     * @param string $password
     *
     * @return bool
     */
    public function checkPasswordSeverity(string $password): bool
    {
        $blacklist = [$this->code_melli, $this->email, $this->mobile];

        if (in_array($password, $blacklist, true)) {
            return false;
        }

        if (strlen($password) < 8) {
            return false;
        }

        return true;
    }



    /**
     * Check a given password is the same as the current password or not. Returns true if it is.
     *
     * @param string $password
     *
     * @return bool
     */
    public function givenPasswordIsSameOfTheCurrentPassword(string $password): bool
    {
        return Hash::check($password, $this->password);
    }



    /**
     * Checks current authenticated user must complete a unity profile or not.
     *
     * @return bool
     */
    public function mustCompleteUnityProfile(): bool
    {
        $roles      = ['expert-in-charge', 'donor-expert', 'opu-manager'];
        $user_roles = $this->unityRolesArray();
        if (($intersect = array_intersect($roles, $user_roles)) != null) {
            return $this->unityRoles
                      ->filter(function ($unityRole) {
                          $unity = $unityRole->unity;
                          if (!$unity) {
                              return null;
                          }
                          return $unityRole->unity->isNotComplete();
                      })
                      ->count() > 0;
        }

        return false;
    }



    /**
     * Get unities which user must complete them.
     *
     * @return array
     */
    public function unitiesForCompleteProfile(): array
    {
        if (!$this->mustCompleteUnityProfile()) {
            return [];
        }

        return $this->unityRoles
             ->filter(function ($unityRole) {
                 $unity = $unityRole->unity;

                 if (!$unity) {
                     return null;
                 }

                 return $unityRole->unity->isNotComplete();
             })
             ->map(function ($unityRole) {
                 return [
                      'name' => $unityRole->unity->name,
                      'id'   => $unityRole->unity->hashid,
                      'type' => $unityRole->unity->type,
                 ];
             })
             ->toArray()
             ;
    }



    /**
     * Return a list of unities which user has a role on them.
     *
     * @return array
     */
    public function userUnitiesList(): array
    {
        return $this->unityRoles
             ->map(function ($unityRole) {
                 if (!$unityRole or !$unityRole->unity) {
                     return [];
                 }

                 $result = [
                      'name'      => $unityRole->unity->name,
                      'id'        => $unityRole->unity->hashid,
                      'type'      => $unityRole->unity->type,
                      'user_role' => $unityRole->role,
                 ];

                 if ($unityRole->unity->type != "university" and !is_null($unityRole->unity->university)) {
                     $result['university']      = $unityRole->unity->university->hashid;
                     $result['university_name'] = $unityRole->unity->university->name;
                 }

                 return $result;
             })
             ->toArray()
             ;
    }



    /**
     * Indicate user must change password or not.
     *
     * @return bool
     */
    public function mustResetPassword(): bool
    {
        return boolval($this->password_force_change);
    }



    /**
     * set iran_donor_certificate attribute (array to string conversion)
     *
     * @param array $value
     */
    public function setIranDonorCertificateAttribute($value)
    {
        $this->attributes['iran_donor_certificate'] = implode(",", (array)$value);
    }



    /**
     * Set tel attribute
     *
     * @param string|null|array $value
     */
    public function setTelAttribute($value)
    {
        $this->attributes['tel'] = implode(",", (array)$value);
    }



    /**
     * get iran_donor_certificate attribute (string to array conversion)
     *
     * @param string $value
     *
     * @return array
     */
    public function getIranDonorCertificateAttribute($value)
    {
        return explode_not_empty(",", $value);
    }



    /**
     * get iran_donor_certificate attribute (string to array conversion)
     *
     * @return array
     */
    public function getIranDonorCertificateDataAttribute()
    {
        return $this->returnUploadedFile($this->iran_donor_certificate);
    }



    /**
     * set international_donor_certificate attribute (array to string conversion)
     *
     * @param array $value
     */
    public function setInternationalDonorCertificateAttribute($value)
    {
        $this->attributes['international_donor_certificate'] = implode(",", (array)$value);
    }



    /**
     * get international_donor_certificate attribute (string to array conversion)
     *
     * @param string $value
     *
     * @return array
     */
    public function getInternationalDonorCertificateAttribute($value)
    {
        return explode_not_empty(",", $value);
    }



    /**
     * get international_donor_certificate attribute (string to array conversion)
     *
     * @return array
     */
    public function getInternationalDonorCertificateDataAttribute()
    {
        return $this->returnUploadedFile($this->international_donor_certificate);
    }



    /**
     * Return uploaded letter_file
     *
     * @param string $value
     *
     * @return array
     */
    public function getLetterFileAttribute($value)
    {
        return explode_not_empty(",", $value);
    }



    /**
     * Return uploaded letter_file
     *
     * @return array
     */
    public function getLetterFileDataAttribute()
    {
        return $this->returnUploadedFile($this->letter_file);
    }



    /**
     * Return uploaded tests_files
     *
     * @param string $value
     *
     * @return array
     */
    public function getTestsFilesAttribute($value)
    {
        return explode_not_empty(",", $value);
    }



    /**
     * Return uploaded tests_files
     *
     * @return array
     */
    public function getTestsFilesDataAttribute()
    {
        return $this->returnUploadedFile($this->tests_files);
    }



    /**
     * set organs attribute (array to string conversion)
     *
     * @param array $value
     */
    public function setOrgansAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['organs'] = implode(",", $value);
        }
    }



    /**
     * get organs attribute (string to array conversion)
     *
     * @param string $original
     *
     * @return array
     */
    public function getOrgansAttribute($original)
    {
        return explode_not_empty(",", $original);
    }



    /**
     * get the list of universities the user may relationally belong to
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function universities()
    {
        $unities = hashid($this->detailedUnityRoles()->pluck('unity')->toArray());

        return RegistryUniversity::superiorsOf($unities);
    }



    /**
     * get a mapped resource of the relationally belonging universities
     *
     * @return array
     */
    public function universitiesResource()
    {
        $collection = $this->universities()->get();
        $mapped     = $collection->map(function (RegistryUniversity $university) {
            return [
                 "id"   => $university->hashid,
                 "name" => $university->name,
            ];
        });

        return $mapped->toArray();
    }



    /**
     * Indicate a user can login or not
     *
     * @return bool
     */
    public function canLogin(): bool
    {
        if (!is_null($this->password) and is_null($this->deactivated_at) and $this->hasUnityRoles()) {
            return true;
        }

        return false;
    }



    /**
     * city of user
     *
     * @param int|string $value
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function getCityAttribute($value)
    {
        $city = model('division')->whereId($value)->whereCityId(0)->where('province_id', '<>', 0)->first();

        return $city ?? model('division');
    }



    /**
     * province of user
     *
     * @param int|string $value
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function getProvinceAttribute($value)
    {
        $province = model('division')->whereId($value)->whereProvinceId(0)->where('country_id', '<>', 0)->first();

        return $province ?? model('division');
    }



    /**
     * Indicate a user can login or not
     *
     * @return bool
     */
    public function canNotLogin(): bool
    {
        return !$this->canLogin();
    }



    /**
     * Return all uploaded files of a given composite hashid string
     *
     * @param array $ids
     *
     * @return array
     */
    protected function returnUploadedFile($ids): array
    {
        if (empty($ids)) {
            return [];
        }

        return uploader()->files($ids)
                         ->map(function ($file) {
                             return $file->api_array;
                         })
                         ->toArray()
             ;
    }



    /**
     * get registry-related mete fields.
     *
     * @return array
     */
    protected function getRegistryMetaFields()
    {
        return [
             'selected_role',
        ];
    }
}
