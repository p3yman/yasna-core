<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property Collection $cases
 */
trait UnityCasesTrait
{
    /**
     * relative cases
     *
     * @return HasMany
     */
    public function cases()
    {
        return $this->hasMany(MODELS_NAMESPACE . "RegistryCase", $this->type . "_id");
    }



    /**
     * relative cases that are still running
     *
     * @return HasMany
     */
    public function currentCases()
    {
        return $this->cases();
        //TODO: Conditions to be added
    }
}
