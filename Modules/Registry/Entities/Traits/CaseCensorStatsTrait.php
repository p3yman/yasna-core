<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryCase;

/**
 * Trait CaseCensorStatsTrait
 *
 * @package Modules\Registry\Entities\Traits add case stats to Census module.
 */
trait CaseCensorStatsTrait
{
    /**
     * Gather data for case table on daily repetitions.
     *
     * @return array
     */
    public function caseDailyStats(): array
    {
        return [
             'case_total_counts' => [RegistryCase::class, 'totalCount'],
        ];
    }



    /**
     * Gather data for case table on monthly repetitions.
     *
     * @return array
     */
    public function caseMonthlyStats(): array
    {
        return [
             'case_total_counts',
        ];
    }



    /**
     * Gather data for case table on yearly repetitions.
     *
     * @return array
     */
    public function caseYearlyStats(): array
    {
        return [
             'case_total_counts',
        ];
    }
}
