<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;

trait UniversityPermissionsTrait
{
    /**
     * @inheritdoc
     */
    public function canEdit(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * @inheritdoc
     */
    public function canView(): bool
    {
        return (
             $this->isSupervisor()
             or
             $this->canEdit()
        );
    }



    /**
     * @inheritdoc
     */
    public function canAssign(string $role): bool
    {
        $method_name = camel_case("canAssign-$role");
        if ($this->hasNotMethod($method_name)) {
            return parent::canAssign($role);
        }

        return $this->$method_name();
    }



    /**
     * determine if the online user can assign the university president
     *
     * @return bool
     */
    protected function canAssignPresident(): bool
    {
        return $this->canEdit();
    }



    /**
     * determine if the online user can assign the university deputy
     *
     * @return bool
     */
    protected function canAssignDeputy(): bool
    {
        return $this->canEdit();
    }



    /**
     * determine if the online user can assign the expert in charge
     *
     * @return bool
     */
    protected function canAssignExpertInCharge(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the online user can assign the donor expert
     *
     * @return bool
     */
    protected function canAssignDonorExpert(): bool
    {
        return $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_EXPERT_IN_CHARGE);
    }



    /**
     * determine if the online user can assign the transplant expert
     *
     * @return bool
     */
    protected function canAssignTransplantExpert(): bool
    {
        return $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_EXPERT_IN_CHARGE);
    }



    /**
     * determine if the online user can assign chief seconder
     *
     * @return bool
     */
    protected function canAssignChiefSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign chief-neurologist seconder
     *
     * @return bool
     */
    protected function canAssignChiefNeurologistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign chief-neurosurgeons seconder
     *
     * @return bool
     */
    protected function canAssignChiefNeurosurgeonsSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign chief-internist seconder
     *
     * @return bool
     */
    protected function canAssignChiefInternistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign chief-anesthesiologist seconder
     *
     * @return bool
     */
    protected function canAssignChiefAnesthesiologistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign neurologist seconder
     *
     * @return bool
     */
    protected function canAssignNeurologistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign neurosurgeons seconder
     *
     * @return bool
     */
    protected function canAssignNeurosurgeonsSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign internist seconder
     *
     * @return bool
     */
    protected function canAssignInternistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign anesthesiologist seconder
     *
     * @return bool
     */
    protected function canAssignAnesthesiologistSeconder(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * determine if the online user can assign legal medicine seconder
     *
     * @return bool
     */
    protected function canAssignLegalMedicine(): bool
    {
        return $this->isOneOfOpuManagers();
    }



    /**
     * determine if the online user can assign chief legal medicine seconder
     *
     * @return bool
     */
    protected function canAssignChiefLegalMedicine(): bool
    {
        return $this->isOneOfOpuManagers();
    }



    /**
     * Checks if the current user can assign a user as the supervisor of this university.
     *
     * @return bool
     */
    protected function canAssignUniversitySupervisor()
    {
        return $this->hasAnyUnityRoles([
             "expert-in-charge",
             "donor-expert",
        ]);
    }



    /**
     * Checks if the current user can assign a user as the harvester of this university.
     *
     * @return bool
     */
    protected function canAssignHarvester()
    {
        return $this->isUniversityExpertsOrOpuManager();
    }



    /**
     * Checks if the current user can assign a user as the consultant of this university.
     *
     * @return bool
     */
    protected function canAssignConsultant()
    {
        return $this->isOneOfOpuManagers();
    }



    /**
     * Checks if the current user can assign a user as the protector of this university.
     *
     * @return bool
     */
    public function canAssignProtector()
    {
        return $this->isUniversityExpertsOrOpuManager();
    }



    /**
     * Checks if the current user can assign a user as the chief-protector of this university.
     *
     * @return bool
     */
    public function canAssignChiefProtector()
    {
        return $this->isUniversityExpertsOrOpuManager();
    }



    /**
     * Checks if the current user can assign a user as the transporter of this university.
     *
     * @return bool
     */
    protected function canAssignTransporter()
    {
        return $this->isUniversityExpertsOrOpuManager();
    }



    /**
     * Checks if current user is a supervisor of this OPU.
     *
     * @return bool
     */
    public function isSupervisor()
    {
        return $this->hasAnyUnityRoles('university-supervisor');
    }



    /**
     * @inheritdoc
     */
    public function canViewAssignedRoles(): bool
    {
        return $this->currentUserIsUniversityExperts()
             or $this->hasOpuRole()
             or $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR)
             or $this->hasHierarchyUnityRoles('opu', RegistryUnityRole::ROLE_OPU_SUPERVISOR);

    }



    /**
     * Indicate current user is expert in charge or donor expert of current university or is an OPU manager of
     * university's OPUs.
     *
     * @return bool
     */
    protected function isUniversityExpertsOrOpuManager(): bool
    {
        return $this->currentUserIsUniversityExperts() or $this->isOneOfOpuManagers();
    }



    /**
     * indicate current user is an opu manager of the university's OPUs.
     *
     * @return bool
     */
    protected function isOneOfOpuManagers()
    {
        return $this->hasHierarchyUnityRoles('opu', RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * indicate current user is an opu manager of the university's OPUs.
     *
     * @return bool
     */
    public function hasOpuRole()
    {
        return $this->hasHierarchyUnityRoles('opu', [
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
             RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_CHIEF_COORDINATOR,
        ]);
    }



    /**
     * Check user is a university expert or not
     *
     * @return bool
     */
    protected function currentUserIsUniversityExperts(): bool
    {
        return $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }
}
