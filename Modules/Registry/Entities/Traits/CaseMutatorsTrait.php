<?php

namespace Modules\Registry\Entities\Traits;

trait CaseMutatorsTrait
{
    /**
     * set `full_name` attribute by merging the first and last parts of the name
     */
    public function setFullNameAttribute()
    {
        if (!isset($this->attributes['name_first'])) {
            $this->attributes['name_first'] = null;
        }
        if (!isset($this->attributes['name_last'])) {
            $this->attributes['name_last'] = null;
        }

        $this->attributes['full_name'] = $this->attributes['name_first'] . " " . $this->attributes["name_last"];
    }



    /**
     * set `received_blood` attribute
     *
     * @param array $value
     */
    public function setReceivedBloodAttribute($value)
    {
        if (!is_array($value)) {
            $this->attributes['received_blood'] = null;
            return;
        }

        $this->attributes['received_blood'] = json_encode($value);
    }
}
