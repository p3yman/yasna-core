<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Support\Carbon;
use App\Models\RegistryHospital;
use Modules\Yasna\Services\YasnaModel;

use function intval;
use function implode;
use function explode_not_empty;

/**
 * Trait PatientTransferMutatorAccessorsTrait
 *
 * @property RegistryHospital|null $origin_hospital
 * @property RegistryHospital|null $destination_hospital
 * @property array                 $transfer_next_action
 * @property int                   $transferred_at
 */
trait PatientTransferMutatorAccessorsTrait
{
    /**
     * `origin_hospital` accessor for `originHospital` relation
     *
     * @return RegistryHospital|null
     */
    public function getOriginHospitalAttribute(): ?RegistryHospital
    {
        return $this->safeRelationLoader('originHospital');
    }



    /**
     * `destination_hospital` accessor for `destinationHospital` relation
     *
     * @return RegistryHospital|null
     */
    public function getDestinationHospitalAttribute(): ?RegistryHospital
    {
        return $this->safeRelationLoader('destinationHospital');
    }



    /**
     * `transfer_next_action` accessor
     *
     * @param string $value
     *
     * @return array
     */
    public function getTransferNextActionAttribute($value): array
    {
        return explode_not_empty(',', $value);
    }



    /**
     * `transfer_next_action` mutator
     *
     * @param array $value
     */
    public function setTransferNextActionAttribute($value)
    {
        $this->attributes['transfer_next_action'] = implode(',', $value);
    }



    /**
     * get `transferred_at` as unix timestamp
     *
     * @param int|string $value
     *
     * @return int|null
     */
    public function getTransferredAtAttribute($value)
    {
        return empty($value) ? null : Carbon::parse($value)->timestamp;
    }



    /**
     * set `transferred_at`
     *
     * @param int|string $value
     */
    public function setTransferredAtAttribute($value)
    {
        $this->attributes['transferred_at'] = (empty($value) ? null : Carbon::createFromTimestamp(intval($value)));
    }



    /**
     * Safe return a relation for using in an accessor
     *
     * @param string $relation
     *
     * @return YasnaModel|null
     */
    private function safeRelationLoader($relation)
    {
        if (!$this->relationLoaded($relation)) {
            $this->load($relation);
        }

        return $this->getRelation($relation);
    }
}
