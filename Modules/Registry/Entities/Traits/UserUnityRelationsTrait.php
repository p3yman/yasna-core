<?php

namespace Modules\Registry\Entities\Traits;


use App\Models\RegistryHospital;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

trait UserUnityRelationsTrait
{
    /**
     * get the related roles supporting eager loads
     *
     * @param string            $model
     * @param string|array|null $role
     *
     * @return HasManyThrough
     */
    protected function getEagerUnities(string $model, $role = null)
    {
        $builder = $this->hasManyThrough(
             MODELS_NAMESPACE . studly_case($model),
             MODELS_NAMESPACE . "RegistryUnityRole",
             "user_id",
             "id",
             "id",
             "unity_id"
        );

        if (is_array($role)) {
            return $builder->whereIn("role", $role);
        }

        return $builder->where("role", $role);
    }



    /**
     * Return all universities which user has a role on them.
     *
     * @param string|array|null $role
     *
     * @return HasManyThrough
     */
    public function getEagerUniversities($role)
    {
        return $this->getEagerUnities('RegistryUniversity', $role);
    }



    /**
     * Return working unity of a user
     *
     * @return BelongsTo
     */
    public function workUnity()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'RegistryHospital', 'work_unity');
    }



    /**
     * Accessor of $working_unity
     *
     * @return RegistryHospital|null
     */
    public function getWorkingUnityAttribute()
    {
        $unity = $this->workUnity()->first();

        return $unity ?? model('registry-hospital');
    }
}
