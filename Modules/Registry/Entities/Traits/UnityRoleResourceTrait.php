<?php

namespace Modules\Registry\Entities\Traits;

trait UnityRoleResourceTrait
{
    /**
     * boot UnityRoleResourceTrait
     *
     * @return void
     */
    public static function bootUnityRoleResourceTrait()
    {
        static::addDirectResources([
             "role",
        ]);
    }



    /**
     * get Unity resource.
     *
     * @return string
     */
    protected function getUnityResource()
    {
        return $this->unity->hashid;
    }



    /**
     * get UnityName resource.
     *
     * @return string
     */
    protected function getUnityNameResource()
    {
        return $this->unity->name;
    }



    /**
     * get User resource.
     *
     * @return string
     */
    protected function getUserResource()
    {
        return $this->user->hashid;
    }



    /**
     * get UserName resource.
     *
     * @return string
     */
    protected function getUserNameResource()
    {
        return $this->user->full_name;
    }



    /**
     * get ArchivedAt resource.
     *
     * @return int
     */
    protected function getArchivedAtResource()
    {
        return $this->getResourceForTimestamps($this->deleted_at);
    }
}
