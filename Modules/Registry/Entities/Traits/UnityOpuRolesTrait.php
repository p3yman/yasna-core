<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryOpu;
use App\Models\RegistryUnityRole;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * Class RegistryOpu
 *
 * @property RegistryOpu $opu_model
 */
trait UnityOpuRolesTrait
{
    /**
     * get the list of managers of the opu (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function opuManagers()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * get president attribute
     *
     * @return \App\Models\User
     */
    public function getOpuManagerAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * get the list of chief-coordinators of the opu (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefCoordinators()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_CHIEF_COORDINATOR);
    }



    /**
     * get chief coordinator attribute
     *
     * @return \App\Models\User
     */
    public function getChiefCoordinatorAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_COORDINATOR);
    }



    /**
     * get the list of chief-protectors of the opu (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefProtectors()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_CHIEF_PROTECTOR);
    }



    /**
     * get chief protector attribute
     *
     * @return \App\Models\User
     */
    public function getChiefProtectorAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_PROTECTOR);
    }



    /**
     * get the list of coordinators of the opu.
     *
     * @return HasManyThrough
     */
    public function coordinators()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_COORDINATOR);
    }



    /**
     * get the list of harvesters of the opu.
     *
     * @return HasManyThrough
     */
    public function harvesters()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_HARVESTER);
    }



    /**
     * get the list of protectors of the opu.
     *
     * @return HasManyThrough
     */
    public function protectors()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_PROTECTOR);
    }



    /**
     * get the list of tel-inspectors of the opu.
     *
     * @return HasManyThrough
     */
    public function inspectorTels()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_INSPECTOR_TEL);
    }



    /**
     * get $this->inspector_tels dynamic property
     *
     * @return Collection
     */
    public function getInspectorTelsAttribute()
    {
        return $this->getRelationValue("inspectorTels");
    }



    /**
     * get the list of clinical inspectors of the opu.
     *
     * @return HasManyThrough
     */
    public function inspectorClinicals()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_INSPECTOR_CLINICAL);
    }



    /**
     * get $this->inspector_clinicals dynamic property
     *
     * @return Collection
     */
    public function getInspectorClinicalsAttribute()
    {
        return $this->getRelationValue("inspectorClinicals");
    }



    /**
     * get the list of supervisors of the opu.
     *
     * @return HasManyThrough
     */
    public function opuSupervisors()
    {
        return $this->getOpuUsers(RegistryUnityRole::ROLE_OPU_SUPERVISOR);
    }



    /**
     * get $this->opu_supervisors dynamic property
     *
     * @return Collection
     */
    public function getOpuSupervisorsAttribute()
    {
        return $this->getRelationValue("opuSupervisors");
    }



    /**
     * get university users, if the unity is not a university itself
     *
     * @param string $role
     *
     * @return HasManyThrough
     */
    private function getOpuUsers(string $role)
    {
        if ($this->type == "opu") {
            return $this->getEagerUsers($role);
        }

        if ($this->type == "university") {
            $users = [];
            foreach ($this->opus as $opu) {
                $users = array_merge($users, $opu->getOpuUsers($role)->pluck("id")->toArray());
            }

            return user()->whereIn("id", $users);
            //TODO: This is ridiculous, but Laravel Unions are not working on relationships.)
        }


        return $this->opu_model->getEagerUsers($role);
    }

}
