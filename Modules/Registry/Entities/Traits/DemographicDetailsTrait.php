<?php

namespace Modules\Registry\Entities\Traits;

/**
 * Class RegistryDemographic
 * Trait DemographicDetailsTrait
 * Development Key: Methods named with the pattern `detailsOfFoo` will be called before saving the `foo` field. These
 * methods should return an array which will be automatically inserted in the `details` meta field of the designated
 * demographics row.
 */
trait DemographicDetailsTrait
{
    use DemographicDetailsFileTrait;
    use DemographicDetailsUserTrait;
    use DemographicDetailsUnityTrait;
    use DemographicDetailsDivisionTrait;
}
