<?php

namespace Modules\Registry\Entities\Traits;

trait UnityElectorTrait
{
    /**
     * search for similar names
     *
     * @param string $name
     */
    public function electorName($name)
    {
        $this->electorFieldLike('name', $name);
    }



    /**
     * search for exact type
     *
     * @param string $type
     */
    public function electorType($type)
    {
        $this->electorFieldValue('type', $type);
    }



    /**
     * search for the city by its id or hashid
     *
     * @param string $identifier
     */
    public function electorCity($identifier)
    {
        $this->electorFieldId($identifier, 'city_id');
    }



    /**
     * search for the province by its id or hashid
     *
     * @param string $identifier
     */
    public function electorProvince($identifier)
    {
        $this->electorFieldId($identifier, 'province_id');
    }



    /**
     * search for university by its id or hashid
     *
     * @param string|int $identifier
     */
    public function electorUniversity($identifier)
    {
        $this->electorFieldId($identifier, 'university_id');
    }



    /**
     * filter the unities by the university ids
     *
     * @param array $ids
     */
    public function electorUniversities($ids)
    {
        $this->elector()->whereIn("university_id" , $ids);
    }



    /**
     * search for OPU by its id or hashid
     *
     * @param string|int $identifier
     */
    public function electorOpu($identifier)
    {
        $this->electorFieldId($identifier, 'opu_id');
    }



    /**
     * search for hospital by its id or hashid
     *
     * @param string|int $identifier
     */
    public function electorHospital($identifier)
    {
        $this->electorFieldId($identifier, 'hospital_id');
    }



    /**
     * Check upper unities of the requested unity is not deactivated
     *
     * @param array $unities
     */
    public function electorHasUnities($unities)
    {
        $unities = (array)$unities;
        foreach ($unities as $unity) {
            $this->elector()->has($unity);
        }
    }



    /**
     * Limit the search domain by a filter on university_id
     *
     * @param int|array $university
     */
    public function electorLimitByUniversity($university)
    {
        if (user()->isSuperadmin()) {
            return;
        }

        if (!$university) {
            $this->electorFieldId(0, 'university_id');
            return;
        }

        if (is_array($university)) {
            $this->elector()->whereIn('university_id', $university);
            return;
        }

        $this->electorFieldId($university, 'university_id');
    }
}
