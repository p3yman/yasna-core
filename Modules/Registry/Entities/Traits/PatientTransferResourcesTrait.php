<?php

namespace Modules\Registry\Entities\Traits;

trait PatientTransferResourcesTrait
{
    /**
     * boot PatientTransferResourcesTrait
     *
     * @return void
     */
    public static function bootPatientTransferResourcesTrait()
    {
        static::addDirectResources([
             "status",
             "transfer_reason",
             "dismiss_transfer_reason",
             "transfer_next_action",
             "case_final_state",
             "transferred_at",
             "transfer_next_action",
        ]);
    }



    /**
     * get Case resource.
     *
     * @return string
     */
    protected function getCaseResource()
    {
        return $this->case->hashid;
    }



    /**
     * get CaseName resource.
     *
     * @return string
     */
    protected function getCaseNameResource()
    {
        return $this->case->full_name;
    }



    /**
     * get City resource.
     *
     * @return string|null
     */
    protected function getCityResource()
    {
        return is_null($this->city) ? null : $this->city->hashid;
    }



    /**
     * get CityTitle resource.
     *
     * @return string
     */
    protected function getCityTitleResource()
    {
        return is_null($this->city) ? "" : $this->city->title_fa;
    }



    /**
     * get Province resource.
     *
     * @return string|null
     */
    protected function getProvinceResource()
    {
        return is_null($this->province) ? null : $this->province->hashid;
    }



    /**
     * get ProvinceTitle resource.
     *
     * @return string
     */
    protected function getProvinceTitleResource()
    {
        return is_null($this->province) ? "" : $this->province->title_fa;
    }



    /**
     * get Hospital resource.
     *
     * @return string
     */
    protected function getHospitalResource()
    {
        return $this->destination_hospital->hashid;
    }



    /**
     * get HospitalTitle resource.
     *
     * @return string
     */
    protected function getHospitalTitleResource()
    {
        return $this->destination_hospital->name;
    }



    /**
     * get OriginalHospital resource.
     *
     * @return string
     */
    protected function getOriginHospitalResource()
    {
        return $this->origin_hospital->hashid;
    }



    /**
     * get OriginHospitalTitle resource.
     *
     * @return string
     */
    protected function getOriginHospitalTitleResource()
    {
        return $this->origin_hospital->name;
    }
}
