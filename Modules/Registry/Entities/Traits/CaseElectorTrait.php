<?php

namespace Modules\Registry\Entities\Traits;


use Carbon\Carbon;

trait CaseElectorTrait
{
    /**
     * Elector for filter 'name_first'
     *
     * @param $value
     */
    public function electorNameFirst($value)
    {
        $value = sprintf("%%%s%%", $value);
        $this->elector()->where('name_first', 'like', $value);
    }



    /**
     * Elector for filter 'name_last'
     *
     * @param $value
     */
    public function electorNameLast($value)
    {
        $value = sprintf("%%%s%%", $value);
        $this->elector()->where('name_last', 'like', $value);
    }



    /**
     * Elector for filter 'name'
     *
     * @param $value
     */
    public function electorName($value)
    {
        $value = sprintf("%%%s%%", $value);
        $this->elector()->where('name_first', 'like', $value)
             ->orWhere('name_last', 'like', $value)
        ;
    }



    /**
     * Elector for filter 'code_melli'
     *
     * @param $value
     */
    public function electorCodeMelli($value)
    {
        $this->elector()->where('code_melli', $value);
    }



    /**
     * Elector for filter 'file_no'
     *
     * @param $value
     */
    public function electorFileNo($value)
    {
        $this->elector()->where('file_no', $value);
    }



    /**
     * Elector for filter 'gender'
     *
     * @param $value
     */
    public function electorGender($value)
    {
        $this->elector()->where('gender', $value);
    }



    /**
     * Elector for filter 'university_id'
     *
     * @param $value
     */
    public function electorUniversity($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('university_id', $value);
    }



    /**
     * Elector for filter 'opu_id'
     *
     * @param $value
     */
    public function electorOpu($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('opu_id', $value);
    }



    /**
     * Elector for filter 'hospital_id'
     *
     * @param $value
     */
    public function electorHospital($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('hospital_id', $value);
    }



    /**
     * Elector for filter 'ward_id'
     *
     * @param $value
     */
    public function electorWard($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('ward_id', $value);
    }



    /**
     * Elector for filter 'inspector_id'
     *
     * @param $value
     */
    public function electorInspector($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('inspector_id', $value);
    }



    /**
     * Elector for filter 'coordinator_id'
     *
     * @param $value
     */
    public function electorCoordinator($value)
    {
        if (is_string($value) and $value != (string)intval($value)) {
            $value = hashid($value);
        }

        $this->elector()->where('coordinator_id', $value);
    }



    /**
     * Elector for filter 'case_situation'
     *
     * @param $value
     */
    public function electorCaseSituation($value)
    {
        $this->elector()->where('case_situation', $value);
    }



    /**
     * Elector for filter 'identification_type'
     *
     * @param $value
     */
    public function electorIdentificationType($value)
    {
        $this->elector()->where('identification_type', $value);
    }



    /**
     * Elector for filter `consciousness_disorder_cause`
     *
     * @param $value
     */
    public function electorConsciousnessDisorderCause($value)
    {
        $this->elector()->where('consciousness_disorder_cause_detail', $value)
             ->orWhere('consciousness_disorder_cause', $value)
        ;
    }



    /**
     * Elector for filter searching in 'name_first', 'name_last', 'code_melli' and 'file_no' (for @peyman3d to have a
     * thing like what google have :wink:).
     *
     * @param $value
     */
    public function electorQ($value)
    {
        $likeValue = sprintf("%%%s%%", $value);
        $intValue  = (int)$value;

        if (!$intValue or $intValue > 100) {
            $this->elector()->where('name_first', 'like', $likeValue)
                 ->orWhere('name_last', 'like', $likeValue)
                 ->orWhere('code_melli', $value)
                 ->orWhere('file_no', $value)
            ;
        } else {
            $start = Carbon::today()->subYear($intValue)->startOfYear();
            $end   = Carbon::today()->subYear($intValue)->endOfYear();

            $this->elector()->whereBetween('birth_date', [$start, $end]);
        }
    }
}
