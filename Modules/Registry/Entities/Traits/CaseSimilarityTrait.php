<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryCase;
use Illuminate\Database\Eloquent\Builder;

trait CaseSimilarityTrait
{

    /**
     * get a builder of the similar cases according to the given code_melli, name_first and name_last fields
     *
     * @param string $code_melli
     * @param string $name_first
     * @param string $name_last
     *
     * @return Builder
     */
    public static function getSimilarCases($code_melli, $name_first, $name_last)
    {
        /** @var Builder $builder */
        $builder = model("registry-case");

        /*-----------------------------------------------
        | Checking accurate code_melli ...
        */
        if ($code_melli) {
            return $builder->where("code_melli", $code_melli);
        }

        /*-----------------------------------------------
        | Checking similar name and family ...
        */
        if ($name_first) {
            $builder = $builder->where("name_first", "like", "%{$name_first}%");
        }
        if ($name_last) {
            $builder = $builder->where("name_last", "like", "%{$name_last}%");
        }

        return $builder;
    }



    /**
     * get a mapped array of the similar cases according to the given code_melli, name_first and name_last fields
     *
     * @param string $code_melli
     * @param string $name_first
     * @param string $name_last
     *
     * @return array
     */
    public static function getMappedSimilarCases($code_melli, $name_first, $name_last)
    {
        $collection = static::getSimilarCases($code_melli, $name_first, $name_last)->get();

        return $collection->map(function ($case) {
            /** @var RegistryCase $case */
            return [
                 "id"         => $case->hashid,
                 "file_no"    => $case->file_no,
                 "code_melli" => $case->code_melli,
                 "name_first" => $case->name_first,
                 "name_last"  => $case->name_last,
                 "province"   => $case->province->title_fa,
                 "city"       => $case->city->title_fa,
                 "university" => $case->university_name,
                 "opu"        => $case->opu_name,
                 "hospital"   => $case->hospital_name,
                 "ward"       => $case->ward_name,
            ];
        })->toArray()
             ;
    }
}
