<?php

namespace Modules\Registry\Entities\Traits;


trait CaseSituationColorTrait
{
    /**
     * color of case row on 'new' situation
     *
     * @return string
     */
    private function colorNew(): string
    {
        return "white";
    }



    /**
     * color of case row on 'follow-up' situation
     *
     * @return string
     */
    private function colorFollowUp(): string
    {
        return "blue";
    }



    /**
     * color of case row on 'bd-confirmed' situation
     *
     * @return string
     */
    private function colorBdConfirmed(): string
    {
        return "yellow";
    }



    /**
     * color of case row on 'under-consent' situation
     *
     * @return string
     */
    private function colorUnderConsent(): string
    {
        return "yellow";
    }



    /**
     * color of case row on 'move-to-opu' situation
     *
     * @return string
     */
    private function colorMoveToOpu(): string
    {
        return "yellow";
    }



    /**
     * color of case row on 'organ-checks' situation
     *
     * @return string
     */
    private function colorOrganChecks(): string
    {
        return "yellow";
    }



    /**
     * color of case row on 'surgery-ready' situation
     *
     * @return string
     */
    private function colorSurgeryReady(): string
    {
        return "yellow";
    }



    /**
     * color of case row on 'healed' situation
     *
     * @return string
     */
    private function colorHealed(): string
    {
        return "purple";
    }



    /**
     * color of case row on 'donated' situation
     *
     * @return string
     */
    private function colorDonated(): string
    {
        return "green";
    }



    /**
     * color of case row on 'unfeasible' situation
     *
     * @return string
     */
    private function colorUnfeasible(): string
    {
        return "red";
    }
}
