<?php

namespace Modules\Registry\Entities\Traits;

/**
 * Trait CaseCensorTrait add RegistryCensor usable methods.
 *
 * @package Modules\Registry\Entities\Traits
 */
trait CaseCensorTrait
{
    /**
     * Return total case counts based on a given set of date bound filters.
     *
     * @param array $interval
     *
     * @return int
     */
    public function totalCount(array $interval): int
    {
        return model('registry_case')
             ->whereBetween('updated_at', $interval)
             ->count()
             ;
    }
}
