<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * Class UnityAbstract
 * This trait contain the necessary methods to establish relationship between unities and users.
 * They resolve university roles, even if the unity is not a university itself.
 *
 * @property string             $type
 * @property RegistryUniversity $university
 * @property RegistryUniversity $university_model
 * @method HasManyThrough getEagerUsers(string $role)
 * @method User getEagerFirstUser(string $role)
 * @method Collection getRelationValue(string $relation)
 * @package Modules\Registry\Entities\Traits
 */
trait UnityUniversityRolesTrait
{
    /**
     * get the list of presidents of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function presidents()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_PRESIDENT);
    }



    /**
     * get president attribute
     *
     * @return \App\Models\User
     */
    public function getPresidentAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_PRESIDENT);
    }



    /**
     * get the list of deputies of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function deputies()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_DEPUTY);
    }



    /**
     * get deputy attribute
     *
     * @return \App\Models\User
     */
    public function getDeputyAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_DEPUTY);
    }



    /**
     * get the list of experts in charge of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function expertInCharges()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_EXPERT_IN_CHARGE);
    }



    /**
     * get expert_in_charge attribute
     *
     * @return \App\Models\User
     */
    public function getExpertInChargeAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_EXPERT_IN_CHARGE);
    }



    /**
     * get the list of donor experts of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function donorExperts()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_DONOR_EXPERT);
    }



    /**
     * get donor expert attribute
     *
     * @return \App\Models\User
     */
    public function getDonorExpertAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_DONOR_EXPERT);
    }



    /**
     * get the list of transplant experts of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function transplantExperts()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_TRANSPLANT_EXPERT);
    }



    /**
     * get transplant expert attribute
     *
     * @return \App\Models\User
     */
    public function getTransplantExpertAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_TRANSPLANT_EXPERT);
    }



    /**
     * get the list of chief-neurologist-seconders of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefNeurologistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_NEUROLOGIST_SECONDER);
    }



    /**
     * get 'chief-neurologist-seconder' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefNeurologistSeconderAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_NEUROLOGIST_SECONDER);
    }



    /**
     * get the list of chief-neurosurgeons-seconders of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefNeurosurgeonsSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_NEUROSURGEONS_SECONDER);
    }



    /**
     * get 'chief-neurosurgeons-seconder' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefNeurosurgeonsSeconderAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_NEUROSURGEONS_SECONDER);
    }



    /**
     * get the list of chief-internist-seconders of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefInternistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_INTERNIST_SECONDER);
    }



    /**
     * get 'chief-internist-seconder' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefInternistSeconderAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_INTERNIST_SECONDER);
    }



    /**
     * get the list of chief-anesthesiologist-seconders of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefAnesthesiologistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_ANESTHESIOLOGIST_SECONDER);
    }



    /**
     * get 'chief-anesthesiologist-seconder' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefAnesthesiologistSeconderAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_ANESTHESIOLOGIST_SECONDER);
    }



    /**
     * get the list of 'chief-seconder' of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_SECONDER);
    }



    /**
     * get 'chief-seconder' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefSeconderAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_SECONDER);
    }



    /**
     * get the list of 'chief-legal-medicine' of the university (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function chiefLegalMedicines()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CHIEF_LEGAL_MEDICINE);
    }



    /**
     * get 'chief-legal-medicine' attribute
     *
     * @return \App\Models\User
     */
    public function getChiefLegalMedicineAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_CHIEF_LEGAL_MEDICINE);
    }



    /**
     * get the list of 'legal-medicine' of the university.
     *
     * @return HasManyThrough
     */
    public function legalMedicines()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_LEGAL_MEDICINE);
    }



    /**
     * get the list of 'legal-medicine' of the university.
     *
     * @return Collection
     */
    public function getLegalMedicinesAttribute()
    {
        return $this->getRelationValue("legalMedicines");
    }



    /**
     * get the list of neurologist Seconders of the university.
     *
     * @return HasManyThrough
     */
    public function neurologistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_NEUROLOGIST_SECONDER);
    }



    /**
     * get the list of neurologist Seconders of the university.
     *
     * @return Collection
     */
    public function getNeurologistSeconderAttribute()
    {
        return $this->getRelationValue("neurologistSeconders");
    }



    /**
     * get the list of neurosurgeons Seconders of the university.
     *
     * @return HasManyThrough
     */
    public function neurosurgeonsSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_NEUROSURGEONS_SECONDER);
    }



    /**
     * get the list of neurosurgeons Seconders of the university.
     *
     * @return Collection
     */
    public function getNeurosurgeonsSeconderAttribute()
    {
        return $this->getRelationValue("neurosurgeonsSeconders");
    }



    /**
     * get the list of "internist-seconder" of the university.
     *
     * @return HasManyThrough
     */
    public function internistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_INTERNIST_SECONDER);
    }



    /**
     * get the list of "internist-seconder" of the university.
     *
     * @return Collection
     */
    public function getInternistSeconderAttribute()
    {
        return $this->getRelationValue("internistSeconders");
    }



    /**
     * get the list of "anesthesiologist-seconder" of the university.
     *
     * @return HasManyThrough
     */
    public function anesthesiologistSeconders()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_ANESTHESIOLOGIST_SECONDER);
    }



    /**
     * get the list of "anesthesiologist-seconder" of the university.
     *
     * @return Collection
     */
    public function getAnesthesiologistSeconderAttribute()
    {
        return $this->getRelationValue("anesthesiologistSeconders");
    }



    /**
     * get the list of 'transporter' of the university.
     *
     * @return HasManyThrough
     */
    public function transporters()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_TRANSPORTER);
    }



    /**
     * get the list of consultants of the university.
     *
     * @return HasManyThrough
     */
    public function consultants()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_CONSULTANT);
    }



    /**
     * get the list of 'university-supervisor' of the university.
     *
     * @return HasManyThrough
     */
    public function universitySupervisors()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR);
    }



    /**
     * get the list of 'harvester's of the university.
     *
     * @return HasManyThrough
     */
    public function universityHarvesters()
    {
        return $this->getUniversityUsers(RegistryUnityRole::ROLE_HARVESTER);
    }



    /**
     * get the list of 'university-supervisor' of the university.
     *
     * @return Collection
     */
    public function getUniversitySupervisorsAttribute()
    {
        return $this->getRelationValue("universitySupervisors");
    }



    /**
     * get university users, if the unity is not a university itself
     *
     * @param string $role
     *
     * @return HasManyThrough
     */
    private function getUniversityUsers(string $role)
    {
        if ($this->type == "university") {
            return $this->getEagerUsers($role);
        }

        return $this->university_model->getEagerUsers($role);
    }
}
