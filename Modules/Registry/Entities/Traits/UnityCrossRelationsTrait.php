<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryOpu;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Registry\Entities\RegistryUniversity;

trait UnityCrossRelationsTrait
{
    /**
     * get the relationship to the one university superior to this unity
     *
     * @return BelongsTo
     */
    public function university()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryUniversity", "university_id");
    }



    /**
     * get a safe instance of the related university
     *
     * @return RegistryUniversity
     */
    public function getUniversityModelAttribute()
    {
        return $this->university?? model("registry-university");
    }



    /**
     * get the relationship to the one opu superior to this unity
     *
     * @return BelongsTo
     */
    public function opu()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryOpu", "opu_id");
    }



    /**
     * get a safe instance pf the related opu
     *
     * @return RegistryOpu
     */
    public function getOpuModelAttribute()
    {
        return $this->opu?? model("registry-opu");
    }



    /**
     * get the relationship to the many opus the unity is superior to
     *
     * @return HasMany
     */
    public function opus()
    {
        return $this->hasMany(MODELS_NAMESPACE . "RegistryOpu", $this->getCalledForeignKey());
    }



    /**
     * get the relationship to the one hospital superior to this unity
     *
     * @return BelongsTo
     */
    public function hospital()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryHospital", "hospital_id");
    }



    /**
     * get a safe instance pf the related hospital
     *
     * @return RegistryOpu
     */
    public function getHospitalModelAttribute()
    {
        return $this->hospital?? model("registry-hospital");
    }



    /**
     * get the relationship to the many opus the unity is superior to
     *
     * @return HasMany
     */
    public function hospitals()
    {
        return $this->hasMany(MODELS_NAMESPACE . "RegistryHospital", $this->getCalledForeignKey());
    }



    /**
     * get the relationship to the one ward superior to this unity
     *
     * @return belongsTo
     */
    public function ward()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RegistryWard", 'ward_id');
    }



    /**
     * get the relationship to the many opus the unity is superior to
     *
     * @return HasMany
     */
    public function wards()
    {
        return $this->hasMany(MODELS_NAMESPACE . "RegistryWard", $this->getCalledForeignKey());
    }



    /**
     * return an array of relationships which must be eager-loaded on list views
     *
     * @return array
     */
    public function eagerLoadedRelationships(): array
    {
        return [
             'university',
             'opu',
             'opus',
             'hospital',
             'hospitals',
             'wards',
        ];
    }



    /**
     * Return a belongs to relation for fetching city
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hospitalCity()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'city_id');
    }



    /**
     * Return a belongs to relation for fetching province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hospitalProvince()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'province_id');
    }



    /**
     * get the foreign key
     *
     * @return string
     */
    private function getCalledForeignKey(): string
    {
        $foreign_key = sprintf("%s_id", static::getType());

        if ($this->hasnotField($foreign_key)) {
            $foreign_key = "type"; // <~~ Setting this might not be a very good idea, but makes it safe when it comes to non-existing `ward_id`
        }

        return $foreign_key;
    }
}
