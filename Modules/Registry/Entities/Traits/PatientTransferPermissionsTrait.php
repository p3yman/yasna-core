<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;

trait PatientTransferPermissionsTrait
{
    /**
     * can create a patient transfer request
     *
     * @return bool
     */
    public function canCreate(): bool
    {
        return user()->isSuperadmin()
             or $this->userHasOpuAdministrativeRole()
             or $this->userIsUniversityExpert();
    }



    /**
     * can edit the patient transfer request
     *
     * @return bool
     */
    public function canEdit(): bool
    {
        return $this->canCreate();
    }



    /**
     * can create or edit a patient transfer request
     *
     * @return bool
     */
    public function canCreateOrEdit(): bool
    {
        return $this->canCreate() or $this->canEdit();
    }



    /**
     * can view the patient transfer request
     *
     * @return bool
     */
    public function canView(): bool
    {
        return $this->canCreate();
    }



    /**
     * check if the online user has any administrative role in the origin OPU
     *
     * @return bool
     */
    protected function userHasOpuAdministrativeRole(): bool
    {
        if (!$this->origin_hospital) {
            return false;
        }

        $opu = $this->origin_hospital->opu;

        if (!$opu or !$opu->exists) {
            return false;
        }

        return $opu->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
        ]);
    }



    /**
     * check if the online user is one of the origin university experts
     *
     * @return bool
     */
    protected function userIsUniversityExpert(): bool
    {
        if (!$this->origin_hospital) {
            return false;
        }

        $university = $this->origin_hospital->university;

        if (!$university or !$university->exists) {
            return false;
        }

        return $university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);

    }
}
