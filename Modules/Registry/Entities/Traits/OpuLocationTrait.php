<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryHospital;
use App\Models\RegistryUniversity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class RegistryOpu
 *
 * @property Collection $hospitals
 * @property string     $service_location
 */
trait OpuLocationTrait
{
    /**
     * One-to-Many Relationship for the Hosting Hospital
     *
     * @return BelongsTo
     */
    public function getServiceLocationHospital()
    {
        if (!$this->service_location) {
            return $this->university();
        }

        return $this->belongsTo(RegistryHospital::class, 'service_location');
    }



    /**
     * `service_location_hospital` accessor for `getServiceLocationHospital` relation
     *
     * @return RegistryHospital|RegistryUniversity|null
     */
    public function getServiceLocationHospitalAttribute()
    {
        if (!$this->service_location) {
            return $this->university;
        }

        return $this->getRelationValue('getServiceLocationHospital');
    }



    /**
     * safe instance of `service_location_hospital`
     *
     * @return RegistryHospital
     */
    public function getServiceLocationHospitalModelAttribute()
    {
        if (!$this->service_location) {
            return $this->university_model;
        }

        $hospital = $this->service_location_hospital;

        if (!$hospital) {
            return model("registry-hospital");
        }

        return $hospital;
    }



    /**
     * Return a list of hospitals for show in resource list
     *
     * @return array
     */
    protected function getHospitalsResource(): array
    {
        return $this->hospitals
             ->map(function (RegistryHospital $hospital) {
                 return $hospital->toListResource();
             })
             ->toArray()
             ;
    }
}
