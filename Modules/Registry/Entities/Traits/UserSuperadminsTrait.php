<?php

namespace Modules\Registry\Entities\Traits;

/**
 * Class User
 * @method bool hasRole($slug)
 */
trait UserSuperadminsTrait
{
    /**
     * get a list-view resource
     *
     * @return array
     */
    public function toSuperadminsListView()
    {
        return [
             "id"         => $this->hashid,
             "name_first" => $this->name_first,
             "name_last"  => $this->name_last,
             "gender"     => $this->gender,
             "mobile"     => $this->mobile,
             "code_melli" => $this->code_melli,
             "is_master"  => $this->hasRole("master"),
        ];

    }



    /**
     * get a single-view resource of the user as an superadmin
     *
     * @return array
     */
    public function toSuperadminsSingleView()
    {
        return $this->toSuperadminsListView();
    }

}
