<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\User;

trait OpuRoleCheckTrait
{
    /**
     * Checks if the specified user is the OPU manager of this OPU.
     *
     * @param User $user
     *
     * @return bool
     */
    public function userIsOpuManager(User $user)
    {
        if ($user->not_exists) {
            return false;
        }

        return $user->hasUnityRole($this->id, 'opu-manager');
    }



    /**
     * Checks if current user is the OPU manager of this OPU.
     *
     * @return bool
     */
    public function currentUserIsOpuManager()
    {
        return $this->userIsOpuManager(user());
    }



    /**
     * Checks if current user is a supervisor of this OPU.
     *
     * @return bool
     */
    public function isSupervisor()
    {
        return (
             $this->university->isSupervisor()
             or
             $this->hasAnyUnityRoles('opu-supervisor')
        );
    }



    /**
     * Checks if this OPU is a procurement OPU.
     *
     * @return bool
     */
    public function isProcurementType()
    {
        // todo: should make decision about the `procurement` title.
        // It is taken from http://en.ehda.sbmu.ac.ir/
        return (
             $this->opu_type == 'procurement'
        );
    }



    /**
     * Checks if this OPU is not a procurement OPU.
     *
     * @return bool
     */
    public function isNotProcurementType()
    {
        return !$this->isProcurementType();
    }

}
