<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;

trait UniversitySecondersTrait
{

    /**
     * get a list of all seconders of the university
     *
     * @return HasMany
     */
    public function seconders()
    {
        return $this->getEagerUsers(static::dependantSeconderRoles());
    }





}
