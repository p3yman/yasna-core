<?php

namespace Modules\Registry\Entities\Traits;

trait UnityUsersTrait
{
    use UnityUniversityRolesTrait;
    use UnityOpuRolesTrait;
    use UnityHospitalRolesTrait;
    use UnityWardRolesTrait;
}
