<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;

/**
 * Class RegistryOpu
 *
 * @property RegistryUniversity $university_model
 */
trait OpuPermissionsTrait
{
    /**
     * @inheritdoc
     */
    public function canView(): bool
    {
        return (
             $this->isSupervisor()
             or
             $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_OPU_MANAGER)
             or
             $this->canEdit()
        );
    }



    /**
     * @inheritdoc
     */
    public function canViewAssignedRoles(): bool
    {
        return $this->canView();
    }



    /**
     * @inheritdoc
     */
    public function canDelete(): bool
    {
        return $this->canEdit();
    }



    /**
     * @inheritdoc
     */
    public function canCreate(): bool
    {
        return $this->university_model->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
        ]);
    }



    /**
     * @inheritdoc
     */
    public function canEdit(): bool
    {
        $can_edit = $this->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_OPU_MANAGER,
        ]);

        return $this->canCreate() or $can_edit;
    }



    /**
     * Checks if the logged in user can assign a user as the manager of this OPU.
     *
     * @return bool
     */
    protected function canAssignOpuManager()
    {
        return $this->university_model->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
        ]);
    }



    /**
     * Checks if the current user can assign a user as the supervisor of this OPU.
     *
     * @return bool
     */
    protected function canAssignOpuSupervisor()
    {
        return $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * Indicate current logged in user has permission for coordinator assignment or not.
     *
     * @return bool
     */
    protected function canAssignCoordinator(): bool
    {
        return $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * Indicate current logged in user has permission for chief coordinator assignment or not.
     *
     * @return bool
     */
    protected function canAssignChiefCoordinator(): bool
    {
        return $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_OPU_MANAGER);
    }



    /**
     * Indicate current logged in user has permission for protector assignment or not.
     *
     * @return bool
     */
    protected function canAssignProtector(): bool
    {
        return $this->currentUserIsUniversityExpertsOrOpuManager();
    }



    /**
     * Indicate current logged in user has permission for chief protector assignment or not.
     *
     * @return bool
     */
    protected function canAssignChiefProtector(): bool
    {
        return $this->currentUserIsUniversityExpertsOrOpuManager();
    }



    /**
     * Indicate current logged in user has permission for harvester assignment or not.
     *
     * @return bool
     */
    protected function canAssignHarvester(): bool
    {
        return $this->currentUserIsUniversityExpertsOrOpuManager();
    }



    /**
     * Indicate current logged in user has permission for telephone inspector assignment or not.
     *
     * @return bool
     */
    protected function canAssignInspectorTel(): bool
    {
        return $this->currentUserIsUniversityExpertsOrOpuManager();
    }



    /**
     * Indicate current logged in user has permission for clinical inspector assignment or not.
     *
     * @return bool
     */
    protected function canAssignInspectorClinical(): bool
    {
        return $this->currentUserIsUniversityExpertsOrOpuManager();
    }



    /**
     * Indicate the current authenticated user is a expert in charge or donor expert of the university or is the OPU
     * manager.
     *
     * @return bool
     */
    private function currentUserIsUniversityExpertsOrOpuManager(): bool
    {
        $condition1 = $this->university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);

        $condition2 = $this->hasAnyUnityRoles(RegistryUnityRole::ROLE_OPU_MANAGER);

        return $condition1 or $condition2;
    }
}
