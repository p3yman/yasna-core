<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\UploaderFile;

use function explode;
use function is_string;
use function uploader;
use function array_filter;

trait DemographicDetailsFileTrait
{
    /**
     * Return list of files information from a given hashid set
     *
     * @param string|array $hashids
     * @param bool         $result_must_be_array
     *
     * @return array
     */
    protected function commonFilesInformation($hashids, $result_must_be_array = false)
    {
        $hashids = is_string($hashids) ? explode(",", $hashids) : $hashids;
        $hashids = array_filter((array)$hashids); // remove zero-values
        if (empty($hashids)) {
            return [];
        }

        $files = uploader()::files($hashids);

        $files_collection = $files->map(function (UploaderFile $file) {
            return $file->toResource();
        })->filter()
        ;

        if ($files_collection->count() == 0) {
            return [];
        }

        if (count($hashids) == 1 && !$result_must_be_array) {
            return $files_collection->first();
        }

        return $files_collection->toArray();
    }



    /**
     * get `details` meta field to be saved for `biochemistry_tests_files`
     *
     * @param array $value
     *
     * @return array
     */
    protected function detailsOfBiochemistryTestsFiles($value): array
    {
        return $this->commonFilesInformation($value, true);
    }



    /**
     * get `details` meta field to be saved for `bacteria_tests_files`
     *
     * @param array $value
     *
     * @return array
     */
    protected function detailsOfBacteriaTestsFiles($value): array
    {
        return $this->commonFilesInformation($value, true);
    }



    /**
     * get `details` meta field to be saved for `virology_tests_files`
     *
     * @param array $value
     *
     * @return array
     */
    protected function detailsOfVirologyTestsFiles($value): array
    {
        return $this->commonFilesInformation($value, true);
    }



    /**
     * get `details` meta field to be saved for `tests_files`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfTestsFiles($value): array
    {
        return $this->commonFilesInformation($value, true);
    }



    /**
     * get `details` meta field to be saved for `malignancy_pathology_scan`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfMalignancyPathologyScan($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `infection_treatment_doc`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfInfectionTreatmentDoc($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmation_1`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmation1($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmation_2`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmation2($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmation_3`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmation3($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmation_4`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmation4($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `donation_agreement`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfDonationAgreement($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_tests_consultant_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartTestsConsultantReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `kidney_sonography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfKidneySonographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `kidney_biopsy_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfKidneyBiopsyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `liver_sonography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLiverSonographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `liver_biopsy_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLiverBiopsyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `liver_ctscan_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLiverCtscanReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `liver_hla_typing_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLiverHlaTypingReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `intestine_abdominal_sonography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfIntestineAbdominalSonographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `intestine_abdominal_ctscan_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfIntestineAbdominalCtscanReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_echocardiography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartEchocardiographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_cardiography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartCardiographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_cardiography_test`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartCardiographyTest($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_cxr_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartCxrReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_cxr_picture`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartCxrPicture($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `heart_angiography_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfHeartAngiographyReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `lung_o2challenge_file`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLungO2challengeFile($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `lung_cxr_report`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLungCxrReport($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `lung_cxr_picture`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfLungCxrPicture($value): array
    {
        return $this->commonFilesInformation($value);
    }



    /**
     * get `details` meta field to be saved for `pancreas_hla_typing`
     *
     * @param string|array $value
     *
     * @return array
     */
    protected function detailsOfPancreasHlaTyping($value): array
    {
        return $this->commonFilesInformation($value);
    }
}
