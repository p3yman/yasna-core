<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryDemographic;
use Carbon\Carbon;

/**
 * Class RegistryCase
 *
 * @property Carbon $stability_sedation_time
 * @property Carbon $case_identified_at
 * @property Carbon $updated_at
 * @property Carbon $birth_date
 */
trait CaseFetchTrait
{
    /**
     * convert the model to a fetch resource array
     *
     * @return array
     */
    public function toFetchResource(): array
    {
        $array = $this->spreadMeta()->toArray();

        $array = $this->removeNonClues($array);
        $array = $this->replaceIdValues($array);
        $array = $this->replaceTimestampValues($array);
        $array = $this->replaceBloodType($array);
        $array = $this->replaceWard($array);
        $array = $this->castArrays($array);
        $array = $this->removeUnnecessaryItems($array);
        $array = $this->addFileFields($array);
        $array = $this->removeOrganFields($array);

        return $array;
    }



    /**
     * replace the value of id fields with the equivalent hashid
     *
     * @param array $array
     *
     * @return array
     */
    private function replaceIdValues(array $array): array
    {
        foreach ($array as $key => $value) {
            if (str_contains($key, "_id") and ends_with($key, "_id")) {
                $array[$key] = $value? hashid($value) : null;
            }
            if (str_contains($key, "_by") and ends_with($key, "_by")) {
                $array[$key] = $value? hashid($value) : null;
            }
            if (str_contains($key, "_by_")) {
                $array[$key] = $value? hashid($value) : null;
            }
        }

        $array['id'] = hashid($array['id']);

        return $array;
    }



    /**
     * replace the value of the timestamp fields with the equivalent unix timestamp value
     *
     * @param array $array
     *
     * @return array
     */
    private function replaceTimestampValues(array $array): array
    {
        $keys = RegistryDemographic::timestampClues();

        foreach ($keys as $key) {
            if (!isset($array[$key])) {
                continue;
            }

            $value = $array[$key];

            if (!$value) {
                $array[$key] = null;
                continue;
            }

            $array[$key] = (new Carbon($value))->getTimestamp();

            if ($array[$key] < 0) {
                $array[$key] = null;
            }
        }

        return $array;
    }



    /**
     * replace the value of the blood types with a combined one
     *
     * @param array $array
     *
     * @return array
     */
    private function replaceBloodType(array $array): array
    {
        $array['blood'] = $this->blood;

        unset($array['blood_type']);
        unset($array['blood_rh']);

        return $array;
    }



    /**
     * replace `ward` with `ward_id`
     *
     * @param array $array
     *
     * @return array
     */
    private function replaceWard(array $array): array
    {
        $array['ward'] = $array['ward_id'];

        unset($array['ward_id']);

        return $array;
    }



    /**
     * replace comma-separated arrays with real arrays
     *
     * @param array $array
     *
     * @return array
     */
    private function castArrays(array $array): array
    {
        $items = RegistryDemographic::arrayClues();

        foreach ($items as $item) {
            if (!isset($array[$item]) or is_array($array[$item])) {
                continue;
            }

            $array[$item] = explode_not_empty(",", $array[$item]);
        }

        return $array;
    }



    /**
     * remove unnecessary items
     *
     * @param array $array
     *
     * @return array
     */
    private function removeUnnecessaryItems(array $array): array
    {
        $items = [
             "university_id",
             "opu",
             "opu_id",
             "inspector_id",
             "coordinator_id",
             "identified_by",
             "full_name",
             "consciousness_disorder_cause",
             "created_at",
             "created_by",
             "updated_at",
             "updated_by",
             "deleted_at",
             "deleted_by",
             "meta",
             "converted",

             "biochemistry_tests_done_at",
             "bacteria_tests_done_at",
             "virology_tests_done_at",
             "tests_files",
        ];

        foreach ($items as $item) {
            unset($array[$item]);
        }

        return $array;
    }



    /**
     * remove non-clue fields from the fetcher
     *
     * @param array $array
     *
     * @return array
     */
    private function removeNonClues(array $array): array
    {
        $fields     = RegistryDemographic::validClues();
        $exceptions = ['id'];
        $allowed    = array_merge($fields, $exceptions);

        foreach ($array as $key => $value) {
            if (!in_array($key, $allowed)) {
                array_forget($array, $key);
            }
        }

        return $array;
    }



    /**
     * remove organ fields from the fetcher
     *
     * @param array $array
     *
     * @return array
     */
    private function removeOrganFields(array $array): array
    {
        $fields  = static::getOrganFields();
        $allowed = [
             "heart_organ_filled",
             "lung_organ_filled",
             "kidney_organ_filled",
             "liver_organ_filled",
             "intestine_organ_filled",
             "pancreas_organ_filled",
        ];

        foreach ($fields as $field => $options) {
            if (!$options or in_array($field, $allowed)) {
                continue;
            }

            array_forget($array, $field);
        }

        return $array;
    }



    /**
     * fetch organ file fields
     *
     * @param array $array
     *
     * @return array
     */
    private function addFileFields(array $array): array
    {
        $file_fields = RegistryDemographic::fileClues();

        foreach ($file_fields as $field) {
            $value   = isset($array[$field]) ? $array[$field] : [];
            $hashids = is_array($value) ? $value : explode_not_empty(",", $value);
            $files   = uploader()->filesArray($hashids);

            $array["_file_" . $field] = $files;
            $array[$field]            = $hashids;
        }

        return $array;
    }

}
