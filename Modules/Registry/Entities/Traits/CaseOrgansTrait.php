<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryDemographic;

trait CaseOrgansTrait
{
    /**
     * get organ fields
     *
     * @return array
     */
    public static function getOrganFields()
    {
        return array_merge(
             static::getHeartFields(),
             static::getLungFields(),
             static::getKidneyFields(),
             static::getLiverFields(),
             static::getIntestineFields(),
             static::getPancreasFields()
        );
    }



    /**
     * fetch required data, enough to show an organ edition form
     *
     * @return array
     */
    public function fetchOrgans(): array
    {
        $array = [];
        $array = $this->fetchOrgansConsistentFields($array);
        $array = $this->fetchOrgansListOfFields($array);
        $array = $this->fetchOrgansNormalFields($array);
        $array = $this->replaceTimestampValues($array);
        $array = $this->fetchOrgansFileFields($array);
        $array = $this->fetchOrgansIdReplacer($array);

        return $array;
    }



    /**
     * fetch information regarding the normal ORGAN fields
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansNormalFields(array $array): array
    {

        $organ_fields  = static::getOrganFields();
        $mutual_fields = static::getOrganAndCaseMutualFields();
        $fields        = array_keys(array_merge($organ_fields, $mutual_fields));

        $this->spreadMeta();
        foreach ($fields as $item) {
            $array[$item] = $this->$item;
        }

        return $array;
    }



    /**
     * fetch organ file fields
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansFileFields(array $array): array
    {
        $file_fields = RegistryDemographic::fileClues();

        foreach ($file_fields as $field) {
            $value   = $this->getAttribute($field);
            $hashids = is_array($value) ? $value : explode_not_empty(",", $value);
            $files   = uploader()->filesArray($hashids);

            $array["_file_" . $field] = $files;
            $array[$field]            = $hashids;
        }

        return $array;
    }



    /**
     * fetch information regarding the consistent fields
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansConsistentFields(array $array): array
    {
        $array['id'] = $this->hashid;

        return $array;
    }



    /**
     * add a list of organ fields to the fetch array
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansListOfFields(array $array): array
    {
        $array['_fields'] = static::getOrganFields();

        $forbidden_fields = [
             'heart_organ_filled',
             'lung_organ_filled',
             'kidney_organ_filled',
             'liver_organ_filled',
             'intestine_organ_filled',
             'pancreas_organ_filled',
        ];

        $additional_fields = static::getOrganAndCaseMutualFields();

        $array['_fields'] = array_merge($array['_fields'], $additional_fields);
        array_forget($array['_fields'], $forbidden_fields);

        return $array;
    }



    /**
     * add a list of organ fields to the fetch array
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansBooleanFields(array $array): array
    {
        $fields = [
             "lung_right_abnormal",
             "lung_left_abnormal",
             "diabetes",
             "hypertension",
             "malignancy",
             "heart_disease",
             "surgical_history",
        ];

        foreach ($fields as $field) {
            if (!isset($array[$field]) or !$array[$field]) {
                $array[$field] = null;
            }
        }

        return $array;
    }



    /**
     * replace the necessary `id`s with `hashid`s
     *
     * @param array $array
     *
     * @return array
     */
    protected function fetchOrgansIdReplacer(array $array): array
    {
        if (isset($array['heart_tests_consultant']) and $array['heart_tests_consultant']) {
            $array['heart_tests_consultant'] = hashid_string($array['heart_tests_consultant']);
        }

        return $array;
    }



    /**
     * fetch a nested list of fields required for each organ
     *
     * @return array
     */
    protected function fetchOrgansFieldList(): array
    {
        return [
             '_fields' => [
                  "heart"     => array_keys(static::getHeartFields()),
                  "lung"      => array_keys(static::getLungFields()),
                  'kidney'    => array_keys(static::getKidneyFields()),
                  "liver"     => array_keys(static::getLiverFields()),
                  'pancreas'  => array_keys(static::getPancreasFields()),
                  "intestine" => array_keys(static::getIntestineFields()),
             ],
        ];
    }



    /**
     * get heart fields
     *
     * @return array
     */
    private static function getHeartFields()
    {
        return [
             "heart_organ_filled" => "boolean",

             "heart_prohibit"                      => "boolean",
             "heart_prohibit_by_desc"              => "string",
             "heart_prohibit_by_electrocardiogram" => "string",
             "heart_prohibit_by_angiography"       => "string",

             "heart_cardiography"        => "boolean",
             "heart_cardiography_report" => "string",
             "heart_cardiography_test"   => "string",

             "heart_echocardiography"            => "boolean",
             "heart_echocardiography_report"     => "string",
             "heart_echocardiography_consultant" => "unsignedInteger",

             "heart_tests_ef"      => "float",
             "heart_tests_lvh"     => "float",
             "heart_tests_pap"     => "float",
             "heart_tests_trg"     => "float",
             "test_troponin_i"     => "float",
             "heart_tests_done_at" => "timestamp",

             "heart_wall_motion_abnormality" => "string",

             "heart_valvular_problem"          => "boolean",
             "heart_valvular_problem_type"     => "string",
             "heart_valvular_problem_severity" => "string",

             "heart_diastolic_dysfunction"          => "boolean",
             "heart_diastolic_dysfunction_severity" => "string",

             "heart_lv_size"     => "string",
             "heart_lv_function" => "string",
             "heart_rv_size"     => "string",
             "heart_rv_function" => "string",
             "heart_la_size"     => "string",
             "heart_la_function" => "string",
             "heart_ra_size"     => "string",
             "heart_ra_function" => "string",

             "heart_tests_consultant"        => "unsignedInteger",
             "heart_tests_consultant_report" => "string",

             "heart_cxr"         => "boolean",
             "heart_cxr_report"  => "string",
             "heart_cxr_picture" => "string",

             "heart_angiography"        => "string",
             "heart_angiography_report" => "string",

             "inotrop_eco" => "array",
        ];
    }



    /**
     * get lung fields
     *
     * @return array
     */
    private static function getLungFields()
    {
        return [
             "lung_organ_filled" => "boolean",

             "lung_prohibit"                 => "boolean",
             "lung_prohibit_by_desc"         => "string",
             "lung_prohibit_by_radiology"    => "string",
             //"lung_prohibit_by_o2challenge"  => "string",
             "lung_prohibit_by_bronchoscopy" => "string",

             "lung_ventilator_mode"       => "string",
             "lung_ventilator_mode_other" => "string",

             "lung_fio2" => "float",
             "lung_peep" => "float",
             "lung_tv"   => "float",
             "lung_rr"   => "float",

             "lung_o2challenge_fio2"  => "float",
             "lung_o2challenge_po2"   => "float",
             "lung_recruitment_test"  => "boolean",
             "lung_recruitment_fio2"  => "float",
             "lung_recruitment_po2"   => "float",
             "lung_o2challenge_cause" => "string",
             "lung_o2challenge_file"  => "string",

             "heart_cxr"         => "string",
             "heart_cxr_report"  => "string",
             "heart_cxr_picture" => "string",

             "lung_pneumothorax"         => "boolean",
             "lung_pneumothorax_reason"  => "string",
             "lung_infiltration"         => "boolean",
             "lung_infiltration_section" => "string",
             "lung_collapse"             => "boolean",
             "lung_collapse_section"     => "string",
             "lung_tumor"                => "boolean",
             "lung_tumor_section"        => "string",

             "lung_ctscan_need"         => "boolean",
             "lung_ctscan_reason"       => "string",
             "lung_ctscan_reason_other" => "string",

             "lung_left_abnormal"              => "boolean",
             "lung_left_abnormality"           => "string",
             "lung_left_abnormality_other"     => "string",
             "lung_left_abnormality_severity"  => "string",
             "lung_right_abnormal"             => "boolean",
             "lung_right_abnormality"          => "string",
             "lung_right_abnormality_other"    => "string",
             "lung_right_abnormality_severity" => "string",

             "lung_pulmonary_secretion_culture_detail"      => "string",
             "lung_pulmonary_secretion_culture_antibiotic"  => "string",
             "lung_pulmonary_secretion_culture_sensitivity" => "string",
             "lung_pulmonary_secretion_culture_resistance"  => "string",

             "lung_pulmonary_secretion_culture"      => "boolean",
             "lung_pulmonary_secretion_culture_type" => "string",

             "lung_can_donate_left"  => "boolean",
             "lung_can_donate_right" => "boolean",

             "bacteria_bal_culture"       => "boolean",
             "bacteria_bal_culture_type"  => "string",
             "bacteria_bal_culture_other" => "string",
        ];
    }



    /**
     * get kidney fields
     *
     * @return array
     */
    private static function getKidneyFields()
    {
        return [
             "kidney_organ_filled" => "boolean",

             "kidney_prohibit"           => "boolean",
             "kidney_prohibit_by_desc"   => "string",
             "kidney_prohibit_by_tests"  => "string",
             "kidney_prohibit_by_biopsy" => "string",

             "kidney_sonography_report" => "string",
             "kidney_size_left"         => "string",
             "kidney_size_left_cm"      => "integer",
             "kidney_size_right"        => "string",
             "kidney_size_right_cm"     => "integer",

             "kidney_abnormal_finding"          => "boolean",
             "kidney_abnormal_finding_type"     => "string",
             "kidney_abnormal_finding_other"    => "string",
             "kidney_abnormal_finding_position" => "string",
             "kidney_abnormal_finding_size"     => "string",
             "kidney_hydronephrosis_severity"   => "string",

             "kidney_biopsy"                    => "boolean",
             "kidney_biopsy_type"               => "string",
             "kidney_biopsy_type_other"         => "string",
             "kidney_biopsy_fibrosis"           => "string",
             "kidney_biopsy_vascular_changes"   => "string",
             "kidney_biopsy_glomerulus_count"   => "float",
             "kidney_biopsy_glomerulus_percent" => "float",
             "kidney_biopsy_glomerulus"         => "string",
             "kidney_biopsy_report"             => "string",

             "kidney_hla_typing"      => "boolean",
             "kidney_hla_typing_desc" => "text",
        ];
    }



    /**
     * get liver fields
     *
     * @return array
     */
    private static function getLiverFields()
    {
        return [
             "liver_organ_filled" => "boolean",

             "liver_prohibit"                => "boolean",
             "liver_prohibit_by_desc"        => "string",
             //"liver_prohibit_by_tests"       => "string",
             "liver_prohibit_by_sonography"  => "string",
             "liver_prohibit_by_observation" => "string",

             "liver_size"      => "string",
             "liver_size_cm"   => "float",
             "liver_ecosystem" => "string",

             "liver_mass"                => "string",
             "liver_mass_count"          => "integer",
             "liver_mass_size"           => "float",
             "liver_mass_position"       => "string",
             "liver_mass_position_other" => "string",

             "liver_gallstones"         => "boolean",
             "liver_bile_ducts_inside"  => "boolean",
             "liver_bile_ducts_outside" => "boolean",

             "liver_sonography_report" => "string",

             "liver_biopsy"            => "boolean",
             "liver_biopsy_type"       => "string",
             "liver_biopsy_type_other" => "string",
             "liver_biopsy_result"     => "string",
             "liver_biopsy_report"     => "string",

             "liver_macro_vesicular_fat"              => "float",
             "liver_micro_intermediate_vesicular_fat" => "float",

             "liver_ctscan"            => "boolean",
             "liver_ctscan_report"     => "string",
             "liver_hla_typing_report" => "string",
        ];
    }



    /**
     * get intestine fields
     *
     * @return array
     */
    private static function getIntestineFields()
    {
        return [
             "intestine_organ_filled" => "boolean",

             "intestine_prohibit"         => "boolean",
             "intestine_prohibit_by_desc" => "string",

             "intestine_abdominal_sonography"        => "string",
             "intestine_abdominal_sonography_report" => "string",
             "intestine_abdominal_ctscan"            => "string",
             "intestine_abdominal_ctscan_report"     => "string",

             "test_serum_amylase"  => "",
             "test_serum_lipase"   => "",
             "test_ast"            => "",
             "test_alt"            => "",
             "test_alk_p"          => "",
             "test_bili_t"         => "",
             "test_bili_d"         => "",
             "test_hemoglobin_a1c" => "",
        ];
    }



    /**
     * get pancreas fields
     *
     * @return array
     */
    private static function getPancreasFields()
    {
        return [
             "pancreas_organ_filled"     => "boolean",
             "pancreas_prohibit"         => "boolean",
             "pancreas_prohibit_by_desc" => "string",
             "pancreas_hla_typing"       => "string",

             "test_hemoglobin_a1c"         => "float",
             "test_serum_amylase"          => "float",
             "test_serum_lipase"           => "float",
             "received_insulin"            => "",
             "received_insulin_type"       => "",
             "received_insulin_daily_dose" => "",

             "tests_group6_done_at" => "timestamp",
             "tests_group7_done_at" => "timestamp",
        ];
    }



    /**
     * get the list of fields being mutual with organs and cases
     *
     * @return array
     */
    private static function getOrganAndCaseMutualFields()
    {
        return [
             "tests_group3_done_at" => "",
        ];
    }
}
