<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\Division;
use App\Models\User;
use Carbon\Carbon;

use function is_null;
use function explode_not_empty;

/**
 * Trait CaseAttributesTrait
 *
 * @package Modules\Registry\Entities\Traits
 * @property int      $age
 * @property array    $sedation
 * @property string   $color
 * @property User     $case_creator
 * @property User     $last_inspector
 * @property User     $last_coordinator
 * @property Division $hospital_city
 * @property Division $hospital_province
 * @property string   $blood_type
 * @property string   $blood_rh
 * @property string   $blood
 * @property int      $proximate_age
 */
trait CaseAttributesTrait
{
    use CaseSituationColorTrait;



    /**
     * The user which created the case file
     *
     * @return User|null
     */
    public function getCaseCreatorAttribute()
    {
        $creator = $this->getRelation('caseCreator');

        return $creator;
    }



    /**
     * The last inspector which visited the case
     *
     * @return User|null
     */
    public function getLastInspectorAttribute()
    {
        $inspector = $this->getRelation('lastInspector');

        return $inspector;
    }



    /**
     * The last coordinator which visited the case
     *
     * @return User|null
     */
    public function getLastCoordinatorAttribute()
    {
        $lastCoordinator = $this->getRelation('lastCoordinator');

        return $lastCoordinator;
    }



    /**
     * Return stability sedation of the case
     *
     * @return array|null
     */
    public function getSedationAttribute(): ?array
    {
        if ($this->stability_sedation) {
            return [
                 "type"   => $this->stability_sedation_type,
                 "amount" => $this->stability_sedation_amount,
                 "time"   => $this->stability_sedation_time->timestamp,
            ];
        }

        return null;
    }



    /**
     * Color of file case based its file status
     *
     * @return string|null
     */
    public function getColorAttribute(): ?string
    {
        $situation = str_after($this->case_situation, 'case-situation-');
        $method    = studly_case(sprintf("color-%s", $situation));
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }

        return null;
    }



    /**
     * Calculate age of case based on birth date
     *
     * @return int
     */
    public function getAgeAttribute(): int
    {
        if (is_null($this->birth_date)) {
            return (int)$this->proximate_age;
        }

        return $this->birth_date->diff(Carbon::now())->format('%y');
    }



    /**
     * Return city which the case is located at
     *
     * @return Division|null
     */
    public function getHospitalCityAttribute(): ?Division
    {
        $hospital = $this->getRelation('hospital');

        return is_null($hospital) ? null : $hospital->city;
    }



    /**
     * Return province which the case is located at
     *
     * @return Division|null
     */
    public function getHospitalProvinceAttribute(): ?Division
    {
        $hospital = $this->getRelation('hospital');

        return is_null($hospital) ? null : $hospital->province;
    }



    /**
     * get blood attribute
     *
     * @return string
     */
    public function getBloodAttribute()
    {
        return $this->blood_type . $this->blood_rh;
    }



    /**
     * get biochemistry tests files
     *
     * @param string|null $value
     *
     * @return array
     */
    public function getBiochemistryTestsFilesAttribute(?string $value): array
    {
        return explode_not_empty(",", $value);
    }



    /**
     * get bacteria tests files
     *
     * @param string|null $value
     *
     * @return array
     */
    public function getBacteriaTestsFilesAttribute(?string $value): array
    {
        return explode_not_empty(",", $value);
    }



    /**
     * get virology tests files
     *
     * @param string|null $value
     *
     * @return array
     */
    public function getVirologyTestsFilesAttribute(?string $value): array
    {
        return explode_not_empty(",", $value);
    }



    /**
     * get `malignancy_organs` attribute
     *
     * @param string $original
     *
     * @return array
     */
    public function getMalignancyOrgansAttribute($original)
    {
        return (array)json_decode($original, 1);
    }



    /**
     * get `received_blood` attribute
     *
     * @param string $original
     *
     * @return array
     */
    public function getReceivedBloodAttribute($original)
    {
        try {
            return json_decode($original, true);
        } catch (\Exception $e) {
            return [];
        }
    }
}
