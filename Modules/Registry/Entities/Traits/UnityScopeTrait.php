<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait UnityScopeTrait
{
    /**
     * boot the trait
     */
    public static function bootUnityScopeTrait()
    {
        static::addGlobalScope("type", function(Builder $builder) {
            $builder->where("type", static::getType());
        });
    }
}
