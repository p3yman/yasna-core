<?php

namespace Modules\Registry\Entities\Traits;


use Illuminate\Database\Eloquent\Collection;
use Modules\Registry\Entities\RegistryUnityDeathCount;

/**
 * Trait DeathCountsRelationTrait add relations about the death counts to the model.
 *
 * @package Modules\Registry\Entities\Traits
 * @property Collection $death_counts Death counts of the hospital.
 */
trait DeathCountsRelationTrait
{
    /**
     * return the relation for all death counts of a hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deathCounts()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'RegistryUnityDeathCount', 'unity_id')
                    ->orderBy('year', 'desc`')
             ;
    }



    /**
     * Accessor for `deathCounts` relation.
     *
     * @return Collection
     */
    public function getDeathCountsAttribute()
    {
        return $this->deathCounts()
                    ->get()
             ;
    }



    /**
     * returns a ready-to-display array of last three death stats of a unity.
     *
     * @param int|null $limit Number of returned results
     *
     * @return array
     */
    public function getDeathCountsArray(?int $limit = null): array
    {
        $builder = $this->deathCounts();
        if ($limit) {
            $builder->limit($limit);
        }

        return $builder->get()
                       ->map(function (RegistryUnityDeathCount $death) {
                           return $death->toSingleResource();
                       })
                       ->toArray()
             ;
    }
}
