<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryHospital;
use App\Models\RegistryUnityRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * Class UnityAbstract
 * This trait contain the necessary methods to establish relationship between unities and users.
 * They resolve university roles, even if the unity is not a university itself.
 *
 * @property string           $type
 * @property RegistryHospital $hospital_model
 * @method HasManyThrough getEagerUsers(string $role)
 * @method User getEagerFirstUser(string $role)
 * @method Collection getRelationValue(string $relation)
 * @package Modules\Registry\Entities\Traits
 */
trait UnityHospitalRolesTrait
{
    /**
     * get the list of presidents of the hospital (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function hospitalPresidents()
    {
        return $this->getHospitalUsers(RegistryUnityRole::ROLE_HOSPITAL_PRESIDENT);
    }



    /**
     * get president attribute
     *
     * @return \App\Models\User
     */
    public function getHospitalPresidentAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_HOSPITAL_PRESIDENT);
    }



    /**
     * get the list of deputies of the hospital (despite it is supposed to be one at a time).
     *
     * @return HasManyThrough
     */
    public function hospitalDeputies()
    {
        return $this->getHospitalUsers(RegistryUnityRole::ROLE_HOSPITAL_DEPUTY);
    }



    /**
     * get deputy attribute
     *
     * @return \App\Models\User
     */
    public function getHospitalDeputyAttribute()
    {
        return $this->getEagerFirstUser(RegistryUnityRole::ROLE_HOSPITAL_DEPUTY);
    }



    /**
     * get university users, if the unity is not a university itself
     *
     * @param string $role
     *
     * @return HasManyThrough
     */
    private function getHospitalUsers(string $role)
    {
        if ($this->type == "hospital") {
            return $this->getEagerUsers($role);
        }

        return $this->hospital_model->getEagerUsers($role);
    }
}
