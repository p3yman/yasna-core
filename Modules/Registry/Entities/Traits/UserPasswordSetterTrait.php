<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;
use Illuminate\Support\Facades\Hash;

/**
 * Trait UserPasswordSetterTrait implement a password setter for users which will be assigned to a role and can login.
 *
 * @package Modules\Registry\Entities\Traits
 */
trait UserPasswordSetterTrait
{
    /**
     * List of roles which can be login in system and a password must be set for users which are on those positions.
     *
     * @var array
     */
    protected $loginable_roles = [
         RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
         RegistryUnityRole::ROLE_DONOR_EXPERT,
         RegistryUnityRole::ROLE_OPU_MANAGER,
         RegistryUnityRole::ROLE_INSPECTOR_TEL,
         RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
         RegistryUnityRole::ROLE_COORDINATOR,
         RegistryUnityRole::ROLE_CHIEF_COORDINATOR,
         RegistryUnityRole::ROLE_OPU_SUPERVISOR,
         RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR,
    ];

    /**
     * The field which the password must be set based on it.
     *
     * @var string
     */
    protected $password_creator_field = "mobile";



    /**
     * Indicate user has a logginable role or not.
     *
     * @return bool
     */
    public function isLoginableUser(): bool
    {
        return array_intersect($this->loginable_roles, $this->unityRolesArray()) != null;
    }



    /**
     * Set a default password for a given account.
     *
     * @return string|null
     */
    public function setDefaultPassword(): ?string
    {
        if ($this->isLoginableUser() and is_null($this->password)) {
            $password                    = $this->defaultPasswordGenerator();
            $this->password              = $password['hash'];
            $this->password_force_change = true;

            if ($this->save()) {
                return $password['raw'];
            }
        }

        return null;
    }



    /**
     * Generate a default password.
     *
     * @return array
     */
    protected function defaultPasswordGenerator(): array
    {
        $raw_password = (string)rand(1000, 9999);
        if (($password = $this->{$this->password_creator_field}) != null) {
            $raw_password = $password;
        }

        $password = $this->magicLogicOfPasswordGeneration($raw_password);

        return [
             'raw'  => $password,
             'hash' => Hash::make($password),
        ];
    }



    /**
     * The logic which generate a password from a given string.
     *
     * @param string $password
     *
     * @return string
     */
    protected function magicLogicOfPasswordGeneration(string $password): string
    {
        if ($password[0] == 0) {
            return substr($password, 1);
        }

        if (str_contains($password, "+98")) {
            return str_after($password, "+98");
        }

        return $password;
    }
}
