<?php

namespace Modules\Registry\Entities\Traits;


trait CaseSortTrait
{
    /**
     * Indicate columns which is allowed to be used for sort in a request
     *
     * @return array
     */
    public function basicSortableFields(): array
    {
        $module = module('registry');

        return [
             'name_first'                   => $module->getTrans('fields.name_first'),
             'name_last'                    => $module->getTrans('fields.name_last'),
             'gcs_on_identification'        => $module->getTrans('fields.gcs_on_identification'),
             'gcs_on_arrival'               => $module->getTrans('fields.gcs_on_arrival'),
             'gcs'                          => $module->getTrans('fields.gcs'),
             'birth_date'                   => $module->getTrans('fields.birth_date'),
             'consciousness_disorder_cause' => $module->getTrans('fields.consciousness_disorder_cause'),
             'identification_type'          => $module->getTrans('fields.identification_type'),
             'inspector_id'                 => $module->getTrans('fields.inspector'),
             'hospital_id'                  => $module->getTrans('fields.hospital'),
             'ward_id'                      => $module->getTrans('fields.ward'),
             'case_evidenced_at'            => $module->getTrans('fields.case_evidenced_at'),
             'case_identified_at'           => $module->getTrans('fields.case_identified_at'),
             'updated_at'                   => $module->getTrans('fields.updated_at'),
        ];
    }



    /**
     * Create an alias for fields on sorting
     *
     * @return array
     */
    public static function mappedSortFields(): array
    {
        return [
             'ward'      => 'ward_id',
             'hospital'  => 'hospital_id',
             'inspector' => 'inspector_id',
             'age'       => 'birth_date',
             'name'      => 'name_last|name_first',
        ];
    }
}
