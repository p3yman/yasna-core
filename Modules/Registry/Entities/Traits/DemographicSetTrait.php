<?php

namespace Modules\Registry\Entities\Traits;

use Carbon\Carbon;

trait DemographicSetTrait
{
    /**
     * set and save a demographic record
     *
     * @param int           $case_id
     * @param int           $batch
     * @param string        $clue
     * @param mixed         $value
     * @param string|Carbon $effected_at
     * @param string        $file
     *
     * @return bool
     */
    public function setValue(int $case_id, int $batch, string $clue, $value, $effected_at, $file): bool
    {
        $slug    = static::generateSlug($case_id, $batch, $clue);
        $type    = static::getClueDataType($clue);
        $field   = static::getClueFieldName($clue);
        $details = [];

        if (!$type) {
            return false;
        }

        $decoration_method = camel_case("decorate-$type-value");
        if ($this->hasMethod($decoration_method)) {
            $value = $this->$decoration_method($value);
        }

        if(is_array($value)) { //<~~ Additional Safety!
            $value = json_encode($value); 
        }

        $details_method = camel_case("details-of-$clue");
        if ($this->hasMethod($details_method)) {
            $details = $this->$details_method($value);
        }


        return $this->batchSaveBoolean([
             "case_id"         => $case_id,
             "batch"           => $batch,
             "slug"            => $slug,
             "clue"            => $clue,
             $field            => $value,
             "effected_at"     => $effected_at,
             "supporting_file" => $file,
             "details"         => $details,
        ]);
    }



    /**
     * generate and return a hopefully unique slug, out of case_id, batch and clue
     *
     * @param int    $case_id
     * @param int    $batch
     * @param string $clue
     *
     * @return string
     */
    protected static function generateSlug(int $case_id, int $batch, string $clue)
    {
        return str_slug(hashid($case_id) . "-" . hashid($batch) . "-" . $clue);
    }



    /**
     * decorate array value to be stored in a text field
     *
     * @param array $original
     *
     * @return string
     */
    protected function decorateArrayValue($original)
    {
        if (!is_array($original)) {
            return $original;
        }

        return json_encode($original);
    }
}
