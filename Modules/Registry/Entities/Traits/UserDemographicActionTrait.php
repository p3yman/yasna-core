<?php

namespace Modules\Registry\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaModel;

/**
 * Trait UserDemographicActionTrait collects all actions (like retrieving or
 * saving) which a user can do with a demographic data.
 *
 * @package Modules\Registry\Entities\Traits
 * @todo    Getting absent value from `meta` returns null.
 */
trait UserDemographicActionTrait
{
    protected $last_demographic_record;



    /**
     * Returns the collection of latest demographic records.
     *
     * @param int|null $limit Number of returned records. If no limit will be
     *                        given, not limit on number of returned data will
     *                        be considered.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function demographicsInfo(?int $limit = null): Collection
    {
        $data = $this->demographics()
                     ->whereNull('deleted_at')
                     ->orderBy('created_at', 'desc')
        ;

        if ($limit) {
            $data->limit($limit);
        }

        return $data->get();
    }



    /**
     * @see UserDemographicActionTrait::demographicsInfo()
     * @return array
     */
    public function demographicsInfoArray(?int $limit = null): array
    {
        return $this->demographicsInfo($limit)->toArray();
    }



    /**
     * Insert a new record of demographic from the given array of data. The
     * absent fields from the given $information will be automatically
     * inherited from the latest available record.
     *
     * @param array $information Given information array. The array must be on
     *                           ['attribute' => 'value'] format which
     *                           attribute is the name of a column on
     *                           demographic table and value is the one which
     *                           must be saved on that column on the DB.
     *
     * @return bool
     */
    public function addDemographics(array $information): bool
    {
        if (empty($information)) {
            return false;
        }

        $information       = $this->fillAbsentDemographicValues($information);
        $demographic_model = $this->getFilledDemographicModel($information);

        $saved_model = $this->demographics()->save($demographic_model);

        return $saved_model->exists;
    }



    /**
     * Fill a demographic model with a given set of information.
     *
     * @param array $information Given demographic information
     *
     * @return YasnaModel
     */
    protected function getFilledDemographicModel(array $information): YasnaModel
    {
        $model = model('registry-demographic');

        foreach ($information as $attribute => $value) {
            if ($value) {
                $model->{$attribute} = $value;
            }
        }

        return $model;
    }



    /**
     * Fill absent values of demographic record for the given data.
     *
     * @param array $information Given demographic information array.
     *
     * @return array
     */
    protected function fillAbsentDemographicValues(array $information): array
    {
        $absent_attributes               = $this->absentDemographicAttributes($information);
        $old_data_for_save_on_new_record = $this->getComplementDemographicInformationFromLastRecord($absent_attributes);

        return array_merge($information, $old_data_for_save_on_new_record);
    }



    /**
     * Return absent attributes of the given information array.
     *
     * @param array $information Given information array.
     *
     * @return array
     */
    protected function absentDemographicAttributes(array $information): array
    {
        $attributes = array_keys($information);
        // add some attributes on present attributes to avoid set them from the last record
        $attributes[] = 'id';
        $attributes[] = 'user_id';
        $attributes[] = 'created_at';
        $attributes[] = 'updated_at';
        $attributes[] = 'deleted_at';
        $attributes[] = 'created_by';
        $attributes[] = 'updated_by';
        $attributes[] = 'deleted_by';

        $latest_record_attributes = $this->getLatestDemographicRecordAttributes();

        return array_values(array_diff($latest_record_attributes, $attributes));
    }



    /**
     * For a given set of attributes, fetch the last record of demographics
     * data of the user and returns the attributes values of that record and
     * use them as the complement information.
     *
     * @param array $attributes Set of attributes which must be retrieved from
     *                          the last record.
     *
     * @return array
     */
    protected function getComplementDemographicInformationFromLastRecord(
         array $attributes
    ): array {
        $result        = [];
        $latest_record = $this->getLatestDemographicRecord();

        foreach ($attributes as $attribute) {
            $result[$attribute] = $latest_record->{$attribute};
        }

        return $result;
    }



    /**
     * Get filled attributes of the latest demographics record.
     *
     * @return array
     */
    protected function getLatestDemographicRecordAttributes(): array
    {
        $latest_record = $this->getLatestDemographicRecord();
        if ($latest_record) {
            return array_keys($latest_record->getAttributes());
        }

        return [];
    }



    /**
     * Return the last demographic record of the user.
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    protected function getLatestDemographicRecord()
    {
        if (!is_null($this->last_demographic_record)) {
            return $this->last_demographic_record;
        }

        $this->last_demographic_record = $this->demographics()
                                              ->whereNull('deleted_at')
                                              ->orderBy('created_at', 'desc')
                                              ->first()
        ;
        $this->last_demographic_record->suppressMeta();

        return $this->last_demographic_record;
    }
}
