<?php

namespace Modules\Registry\Entities\Traits;


trait DemographicDetailsDivisionTrait
{
    /**
     * Most required detail division information are common and can be categorized on a common fetcher
     *
     * @param int $value
     *
     * @return array
     */
    protected function commonDivisionInformation($value): array
    {
        $division = model('division', $value);

        return [
             "hashid" => $division->hashid,
             "title"  => $division->title_fa,
        ];
    }



    /**
     * get `details` meta field to be saved for `nationality_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfNationalityId($value): array
    {
        return $this->commonDivisionInformation($value);
    }



    /**
     * get `details` meta field to be saved for `country_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfCountryId($value): array
    {
        return $this->commonDivisionInformation($value);
    }



    /**
     * get `details` meta field to be saved for `province_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfProvinceId($value): array
    {
        return $this->commonDivisionInformation($value);
    }



    /**
     * get `details` meta field to be saved for `city_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfCityId($value): array
    {
        return $this->commonDivisionInformation($value);
    }
}
