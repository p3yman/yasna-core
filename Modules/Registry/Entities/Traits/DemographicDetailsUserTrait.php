<?php

namespace Modules\Registry\Entities\Traits;

trait DemographicDetailsUserTrait
{
    /**
     * Most required detail user information are common and can be categorized on a common fetcher
     *
     * @param int $value
     *
     * @return array
     */
    protected function commonUserInformation($value): array
    {
        $user = user($value);

        return [
             "hashid"    => $user->hashid,
             "full_name" => $user->full_name,
        ];
    }



    /**
     * get `details` meta field to be saved for `inspector_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfInspectorId($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `coordinator_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfCoordinatorId($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `donation_agreement_confirmed_by`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfDonationAgreementConfirmedBy($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmed_by_1`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmedBy_1($value): array
    {
        return $this->commonUserInformation($value);;
    }



    /**
     * get `details` meta field to be saved for `bd_confirmed_by_2`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmedBy_2($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmed_by_3`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmedBy_3($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `bd_confirmed_by_4`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfBdConfirmedBy_4($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `identified_by`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfIdentifiedBy($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `updated_by`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfUpdatedBy($value): array
    {
        return $this->commonUserInformation($value);
    }



    /**
     * get `details` meta field to be saved for `created_by`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfCreatedBy($value): array
    {
        return $this->commonUserInformation($value);
    }
}
