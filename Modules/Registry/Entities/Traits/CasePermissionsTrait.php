<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryOpu;
use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;

trait CasePermissionsTrait
{

    /**
     * check if the current online user can view the case info
     *
     * @return bool
     */
    public function canView(): bool
    {
        if (!$this->exists) {
            return false;
        }

        return (
             user()->isSuperadmin() or
             $this->userHasOpuAdministrativeRole() or
             $this->userIsUniversityExpert() or
             $this->userIsSupervisor()
        );
    }



    /**
     * check if the online user has any administrative role in the OPU
     *
     * @return bool
     */
    protected function userHasOpuAdministrativeRole(): bool
    {
        /** @var RegistryOpu $opu */
        $opu = $this->opu;

        if (!$opu or !$opu->exists) {
            return false;
        }

        return $opu->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
        ]);
    }



    /**
     * check if the online user is one of the university experts
     *
     * @return bool
     */
    protected function userIsUniversityExpert(): bool
    {
        /** @var RegistryUniversity $university */
        $university = $this->university;

        if (!$university or !$university->exists) {
            return false;
        }

        return $university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);

    }



    /**
     * check if the online user is one of the university/opu supervisors
     *
     * @return bool
     */
    protected function userIsSupervisor(): bool
    {
        return $this->userIsUniversitySupervisor() or $this->userIsOpuSupervisor();
    }



    /**
     * check if the online user is one of the university supervisors
     *
     * @return bool
     */
    protected function userIsUniversitySupervisor()
    {
        /** @var RegistryUniversity $university */
        $university = $this->university;
        $role       = RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR;

        if ($university and $university->exists and $university->hasAnyUnityRoles($role)) {
            return true;
        }

        return false;
    }



    /**
     * check if the online user is one of the opu supervisors
     *
     * @return bool
     */
    protected function userIsOpuSupervisor()
    {
        /** @var RegistryOpu $opu */
        $opu  = $this->opu;
        $role = RegistryUnityRole::ROLE_OPU_SUPERVISOR;

        if ($opu and $opu->exists and $opu->hasAnyUnityRoles($role)) {
            return true;
        }

        return false;
    }
}
