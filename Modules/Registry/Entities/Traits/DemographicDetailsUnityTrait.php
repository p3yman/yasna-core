<?php

namespace Modules\Registry\Entities\Traits;

trait DemographicDetailsUnityTrait
{
    /*--------------------------------------------------------------------------
    | Unities
     -------------------------------------------------------------------------*/
    /**
     * get `details` meta field to be saved for `university_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfUniversityId($value): array
    {
        $university = model('registry-university', $value);
        if (!$university->exists) {
            return [];
        }

        return [
             "id"             => $university->hashid,
             "name"           => $university->name,
             "city"           => $university->city ? $university->city->hashid : "",
             "city_title"     => $university->city ? $university->city->title_fa : "",
             "province"       => $university->province ? $university->province->hashid : "",
             "province_title" => $university->province ? $university->province->title_fa : "",
        ];
    }



    /**
     * get `details` meta field to be saved for `university_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfOpuId($value): array
    {
        $opu = model('registry-opu', $value);
        if (!$opu->exists) {
            return [];
        }

        return [
             "id"             => $opu->hashid,
             "name"           => $opu->name,
             "city"           => $opu->city ? $opu->city->hashid : "",
             "city_title"     => $opu->city ? $opu->city->title_fa : "",
             "province"       => $opu->province ? $opu->province->hashid : "",
             "province_title" => $opu->province ? $opu->province->title_fa : "",
        ];
    }



    /**
     * get `details` meta field to be saved for `ward_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfWardId($value): array
    {
        $ward = model('registry-ward', $value);
        if (!$ward->exists) {
            return [];
        }

        return [
             "id"             => $ward->hashid,
             "name"           => $ward->name,
             "city"           => $ward->hospital->city ? $ward->hospital->city->hashid : "",
             "city_title"     => $ward->hospital->city ? $ward->hospital->city->title_fa : "",
             "province"       => $ward->hospital->province ? $ward->hospital->province->hashid : "",
             "province_title" => $ward->hospital->province ? $ward->hospital->province->title_fa : "",
        ];
    }



    /**
     * get `details` meta field to be saved for `hospital_id`
     *
     * @param int $value
     *
     * @return array
     */
    protected function detailsOfHospitalId($value): array
    {
        $hospital = model('registry-hospital', $value);
        if (!$hospital->exists) {
            return [];
        }

        return [
             "id"             => $hospital->hashid,
             "name"           => $hospital->name,
             "city"           => $hospital->city ? $hospital->city->hashid : "",
             "city_title"     => $hospital->city ? $hospital->city->title_fa : "",
             "province"       => $hospital->province ? $hospital->province->hashid : "",
             "province_title" => $hospital->province ? $hospital->province->title_fa : "",
        ];
    }
}
