<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\Division;
use App\Models\RegistryCase;

/**
 * Trait PatientTransferRelationsTrait
 *
 * @property RegistryCase|null $case
 * @property Division|null     $city
 * @property Division|null     $province
 */
trait PatientTransferRelationsTrait
{
    /**
     * case related to the transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function case()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'RegistryCase', 'case_id');
    }



    /**
     * origin hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function originHospital()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'RegistryHospital', 'origin_hospital_id');
    }



    /**
     * destination hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function destinationHospital()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'RegistryHospital', 'destination_hospital_id');
    }



    /**
     * destination city
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'city_id');
    }



    /**
     * destination province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Division', 'province_id');
    }
}
