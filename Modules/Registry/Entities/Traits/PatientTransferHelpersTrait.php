<?php

namespace Modules\Registry\Entities\Traits;

trait PatientTransferHelpersTrait
{
    /**
     * Create a transfer request
     *
     * @param int    $case
     * @param string $from
     * @param string $to
     * @param int    $reason
     * @param int    $at
     *
     * @return bool
     */
    public static function successTransfer($case, $from, $to, $reason, $at)
    {
        $transfer = new static;
        $dest     = model('registry_hospital', $to);

        $transfer->case_id                 = $case;
        $transfer->origin_hospital_id      = $from;
        $transfer->city_id                 = $dest->city_id;
        $transfer->province_id             = $dest->province_id;
        $transfer->destination_hospital_id = $dest->id;
        $transfer->transfer_reason         = $reason;
        $transfer->status                  = static::STATUS_CONFIRMED;
        $transfer->transferred_at          = $at;

        return $transfer->save();
    }
}
