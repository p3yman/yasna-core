<?php

namespace Modules\Registry\Entities\Traits;


use App\Models\User;
use Modules\Registry\Entities\Abstracts\UnityAbstract;

trait CaseToResourceTrait
{
    /**
     * Return a list representation of a case model
     *
     * @return array
     */
    public function toListResource(): array
    {
        return [
             "id"                    => $this->hashid,
             "code_melli"            => $this->code_melli,
             "full_name"             => $this->full_name,
             "file_no"               => $this->file_no,
             "age"                   => $this->age,
             "gcs_on_identification" => $this->gcs_on_identification,
             "gcs"                   => $this->gcs,
             "color"                 => $this->color,

             "hospital"   => !$this->hospital ? null : $this->toUnityShortResource($this->hospital),
             "ward"       => !$this->ward ? null : $this->toUnityShortResource($this->ward),
             "opu"        => !$this->opu ? null : $this->toUnityShortResource($this->opu),
             "university" => !$this->university ? null : $this->toUnityShortResource($this->university),

             "city"     => !$this->hospital_city ? null : $this->hospital_city->toListResource(),
             "province" => !$this->hospital_province ? null : $this->hospital_province->toListResource(),

             "inspector"   => !$this->inspector_id ? null : $this->toUserShortResource($this->getPerson("inspector_id")),
             "coordinator" => !$this->coordinator_id ? null : $this->toUserShortResource($this->getPerson("coordinator_id")),

             "identification_type" => $this->identification_type,
             "cause"               => $this->consciousness_disorder_cause_detail,
             "remarks"             => $this->consciousness_disorder_cause_remarks,

             "reflex_breathing" => $this->reflex_breathing,
             "reflex_cornea"    => $this->reflex_cornea,
             "reflex_pupil"     => $this->reflex_pupil,
             "reflex_face"      => $this->reflex_face,
             "reflex_body"      => $this->reflex_body,
             "reflex_doll"      => $this->reflex_doll,
             "reflex_gag"       => $this->reflex_gag,
             "reflex_cough"     => $this->reflex_cough,
             "sedation"         => $this->sedation,

             "case_situation"     => $this->case_situation,
             "clinical_situation" => $this->clinical_situation,

             "identified_at" => !$this->identified_at ? null : $this->identified_at->timestamp,
             "updated_at"    => $this->updated_at->timestamp,
             "creator"       => !$this->created_by ? null : $this->toUserShortResource($this->creator),
        ];
    }



    /**
     * Return a single representation of a case model
     *
     * @return array
     */
    public function toSingleResource(): array
    {
        // temporarily return the list view
        // TODO: Implement single view
        return $this->toListResource();
    }



    /**
     * Short representation of a unity model
     *
     * @param UnityAbstract $model
     *
     * @return array
     */
    private function toUnityShortResource($model): array
    {
        return [
             "id"   => $model->hashid,
             'name' => $model->name,
             'type' => $model->type,
        ];
    }



    /**
     * Short representation of a user model
     *
     * @param User $model
     *
     * @return array
     */
    private function toUserShortResource(User $model): array
    {
        return [
             "id"   => $model->hashid,
             "name" => $model->full_name,
        ];
    }
}
