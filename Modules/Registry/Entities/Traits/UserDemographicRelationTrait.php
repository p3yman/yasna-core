<?php

namespace Modules\Registry\Entities\Traits;


/**
 * Trait UserDemographicRelationTrait returns all relation which can be exist
 * between an demographic data and a given user.
 *
 * @package Modules\Registry\Entities\Traits
 */
trait UserDemographicRelationTrait
{
    /**
     * Return all demographics data of the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function demographics()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'RegistryDemographic');
    }
}
