<?php

namespace Modules\Registry\Entities\Traits;

use App\Models\RegistryUnityRole;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;


trait UnityRolesTrait
{
    /**
     * attach a new user as the given role
     *
     * @param int    $user_id
     * @param string $role
     *
     * @return bool
     */
    public function assignUserAs(int $user_id, string $role)
    {
        if (in_array($role, $this->administrativeRoles())) {
            return RegistryUnityRole::assign($this->id, $user_id, $role, true);
        }

        if (in_array($role, $this->dependantRoles())) {
            return RegistryUnityRole::assign($this->id, $user_id, $role, false);
        }

        return false;
    }



    /**
     * detach a user from the given role
     *
     * @param int    $user_id
     * @param string $role
     *
     * @return bool
     */
    public function revokeUserAs(int $user_id, string $role)
    {
        return RegistryUnityRole::revoke($this->id, $user_id, $role);
    }



    /**
     * get the user instance responsible for the given role
     *
     * @param string $role_slug
     *
     * @return \App\Models\User
     */
    public function getUserAs(string $role_slug)
    {
        return RegistryUnityRole::locateByUnity($this->id, $role_slug)->getSafeUser();
    }



    /**
     * check if the online user has a certain unity role
     *
     * @param string|array $roles
     *
     * @return bool
     */
    public function hasAnyUnityRoles($roles)
    {
        $roles = (array)$roles;

        if (user()->isSuperadmin()) {
            return true;
        }

        foreach ($roles as $role) {
            if (user()->hasUnityRole($this->id, $role)) {
                return true;
            }
        }

        return false;
    }



    /**
     * Determine current authenticated user has a requested role(s) on any sub-unities of current unity.
     *
     * @param string       $unity_type
     * @param string|array $roles
     * @param null|int     $user_id
     *
     * @return bool
     */
    public function hasHierarchyUnityRoles(string $unity_type, $roles, $user_id = null): bool
    {
        $roles   = (array)$roles;
        $user_id = $user_id ?? user()->id;

        $unities = model(sprintf('registry-%s', $unity_type))
             ->select('id')
             ->where(sprintf('%s_id', $this->type), $this->id)
        ;

        $builder = model('registry-unity-role')->whereIn('unity_id', $unities)
                                               ->where('user_id', $user_id)
                                               ->whereIn('role', $roles)
        ;

        return boolval($builder->count());
    }



    /**
     * get the related users supporting eager loads
     *
     * @param string|array|null $role
     *
     * @return HasManyThrough
     */
    public function getEagerUsers($role = null)
    {
        $builder = $this->hasManyThrough(
             MODELS_NAMESPACE . "User",
             MODELS_NAMESPACE . "RegistryUnityRole",
             "unity_id",
             "id",
             "id",
             "user_id"
        );

        $builder->select("users.*");

        if (!$role) {
            return $builder;
        }

        if (is_array($role)) {
            return $builder->whereIn("role", $role);
        }

        return $builder->where("role", $role);
    }



    /**
     * get all related users (trashed + non-trashed) supporting eager loads
     *
     * @param string|null $role
     *
     * @return HasMany
     */
    public function getEagerUsersWithTrash($role = null)
    {
        $builder = $this->unityAssignedRoles()
                        ->withTrashed()
                        ->with([
                             'user' => function ($query) {
                                 $query->withTrashed();
                             },
                        ])
                        ->orderByRaw('deleted_at IS NULL DESC')
                        ->orderBy('deleted_at', 'desc')
        ;

        if (!$role) {
            return $builder;
        }

        if (is_array($role)) {
            return $builder->whereIn("role", $role);
        }

        return $builder->where("role", $role);
    }



    /**
     * get the first related user, supporting eager loading
     *
     * @param null $role
     *
     * @return \App\Models\User
     */
    protected function getEagerFirstUser($role = null)
    {
        $method = str_plural(camel_case($role));

        if ($this->hasNotMethod($method)) {
            return user(-1);
        }

        $first = $this->$method->first();

        return $first ? $first : user(-1);
    }



    /**
     * Return all assigned roles of a unity
     *
     * @return HasMany
     */
    public function unityAssignedRoles()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'RegistryUnityRole', 'unity_id');
    }



    /**
     * Checks a unity has an assigned role(s) or not.
     *
     * @param array|string $role
     *
     * @return bool
     */
    public function hasAssignedRole($role): bool
    {
        $builder = $this->unityAssignedRoles();

        if (is_array($role)) {
            $builder->whereIn('role', $role);
        } elseif (is_string($role)) {
            $builder->where('role', $role);
        } else {
            return false;
        }

        return $builder->exists();
    }



    /**
     * Checks a unity has not an assigned role(s).
     *
     * @param array|string $role
     *
     * @return bool
     */
    public function hasNotAssignedRole($role): bool
    {
        return !$this->hasAssignedRole($role);
    }
}
