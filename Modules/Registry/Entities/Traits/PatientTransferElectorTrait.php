<?php

namespace Modules\Registry\Entities\Traits;

use function is_array;

trait PatientTransferElectorTrait
{
    /**
     * filter transfer requests based on case(s) ID
     *
     * @param int $value
     */
    public function electorCase($value)
    {
        if (is_array($value)) {
            $this->elector()->whereIn('case_id', $value);
            return;
        }

        $this->electorFieldId($value, 'case_id');
    }



    /**
     * filter transfer requests based on origin hospital(s) ID
     *
     * @param int $value
     */
    public function electorHospital($value)
    {
        if (is_array($value)) {
            $this->elector()->whereIn('origin_hospital_id', $value);
            return;
        }

        $this->electorFieldId($value, 'origin_hospital_id');
    }
}
