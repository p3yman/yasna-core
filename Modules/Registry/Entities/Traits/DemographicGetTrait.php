<?php

namespace Modules\Registry\Entities\Traits;

/**
 * Class RegistryDemographic
 *
 * @property string $clue
 * @property float  $value_numeric
 * @property string $value_string
 * @property string $value_text
 * @property string $value_timestamp
 * @package Modules\Registry\Entities\Traits
 */
trait DemographicGetTrait
{
    /**
     * get value out of the correct field according to its type
     *
     * @return mixed
     */
    public function getValueAttribute()
    {
        $type   = self::getClueDataType($this->clue);
        $method = camel_case("get-$type-value");

        if (!$type or $this->hasNotMethod($method)) {
            return null;
        }

        return $this->$method();
    }



    /**
     * get value out of the correct field according to its type for showing in a resource
     *
     * @return mixed
     */
    public function getResourceValueAttribute()
    {
        $type            = self::getClueDataType($this->clue);
        $resource_method = camel_case("get-$type-resource-value");

        if ($this->hasMethod($resource_method)) {
            return $this->{$resource_method}();
        }

        return $this->value;
    }



    /**
     * get the value from $this->string_value
     *
     * @return string
     */
    protected function getStringValue()
    {
        return $this->value_string;
    }



    /**
     * get the value from $this->value_numeric
     *
     * @return string
     */
    protected function getNumericValue()
    {
        return $this->value_numeric;
    }



    /**
     * get the value from $this->value_text
     *
     * @return array
     */
    protected function getArrayValue()
    {
        return json_decode($this->value_text);
    }



    /**
     * get the value from $this->value_numeric for showing in a resource
     *
     * @return string
     */
    protected function getNumericResourceValue()
    {
        if (ends_with($this->clue, "_id")) {
            return hashid((int)$this->value_numeric);
        }

        return $this->value_numeric;
    }



    /**
     * get the value from $this->value_text
     *
     * @return string
     */
    protected function getTextValue()
    {
        return $this->value_text;
    }



    /**
     * get the value from $this->value_timestamp
     *
     * @return string
     */
    protected function getTimestampValue()
    {
        return $this->value_timestamp;
    }



    /**
     * get the value from $this->value_timestamp for showing in a resource
     *
     * @return string
     */
    protected function getTimestampResourceValue()
    {
        return is_null($this->value_timestamp) ? null : $this->value_timestamp->timestamp;
    }



    /**
     * get the value from $this->value_string
     *
     * @return string
     */
    protected function getFileValue()
    {
        return $this->value_string;
        //@TODO: resolve the file and return the url, instead of simply returning the string value
    }

}
