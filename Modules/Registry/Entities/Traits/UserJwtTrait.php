<?php

namespace Modules\Registry\Entities\Traits;

/**
 * Trait UserJwtTrait Override JWT configurations methods based on Registry project.
 *
 * @package Modules\Registry\Entities\Traits
 */
trait UserJwtTrait
{
    /**
     * Returns a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
             "full_name"                    => $this->full_name,
             "code_melli"                   => $this->code_melli,
             "roles"                        => $this->unityRolesArray(),
             "must_complete_unity_profile"  => $this->mustCompleteUnityProfile(),
             "unities_for_complete_profile" => $this->unitiesForCompleteProfile(),
             "must_reset_password"          => $this->mustResetPassword(),
             "selected_role"                => $this->getMeta("selected_role"),
        ];
    }
}
