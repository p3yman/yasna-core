<?php

namespace Modules\Registry\Entities;

use Modules\Registry\Entities\Abstracts\UnityAbstract;

class RegistryWard extends UnityAbstract
{
    /**
     * go through all models and refresh the `opu_id`s according to the available hospital.
     *
     * @return void
     */
    public static function updateAllOpuIds()
    {
        foreach (hospital()->all() as $hospital) {
            ward()->where("hospital_id", $hospital->id)->update([
                 "opu_id" => $hospital->opu_id,
            ]);
        }
    }



    /**
     * @inheritdoc
     */
    public function administrativeRoles(): array
    {
        return [RegistryUnityRole::ROLE_CHIEF_WARD];
    }



    /**
     * get the user instance of the ward's chief
     *
     * @return \App\Models\User
     */
    public function chief()
    {
        return $this->getUserAs(RegistryUnityRole::ROLE_CHIEF_WARD);
    }



    /**
     * @inheritdoc
     */
    public function dependantRoles(): array
    {
        return [];
    }



    /**
     * @inheritdoc
     */
    public function canEdit(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canCreate(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canDelete(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canView(): bool
    {
        return $this->hospital_model->canView();
    }



    /**
     * @inheritdoc
     */
    public function canViewAssignedRoles(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * Indicate the current authenticated user can assign a ward chief
     *
     * @return bool
     */
    public function canAssignChiefWard(): bool
    {
        return $this->university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * convert model to a resource array in the list view
     *
     * @return array
     */
    public function toListResource(): array
    {
        $results = [
             "id"             => $this->hashid,
             'name'           => $this->name,
             'bed_counts'     => $this->bed_counts,
             'city'           => $this->city_id ? $this->city->hashid : null,
             'city_title'     => $this->city_id ? $this->city->title_fa : null,
             'province'       => $this->province_id ? $this->province->hashid : null,
             'province_title' => $this->province_id ? $this->province->title_fa : null,
             'hospital_title' => $this->hospital->name,
             'hospital'       => $this->hospital->hashid,
             "is_active"      => is_null($this->deactivated_at),
             "_can_view"      => $this->canView(),
        ];

        //$results['death_counts'] = $this->getDeathCountsArray(3);

        return $results;
    }



    /**
     * convert model to a resource array in the single view
     *
     * @return array
     */
    public function toSingleResource(): array
    {
        $results = [
             "id"             => $this->hashid,
             "type"           => $this->type,
             'name'           => $this->name,
             'bed_counts'     => $this->bed_counts,
             'city'           => $this->city_id ? $this->city->hashid : null,
             'city_title'     => $this->city_id ? $this->city->title_fa : null,
             'province'       => $this->province_id ? $this->province->hashid : null,
             'province_title' => $this->province_id ? $this->province->title_fa : null,
             'hospital_title' => $this->hospital->name,
             'hospital'       => $this->hospital->hashid,
             'chief-ward'     => $this->getUserAs(RegistryUnityRole::ROLE_CHIEF_WARD)->toFront(),
             "is_active"      => is_null($this->deactivated_at),
        ];

        //$results['death_counts'] = $this->getDeathCountsArray();

        return $results;
    }



    /**
     * @inheritdoc
     */
    public function eagerLoadedRelationships(): array
    {
        return array_merge(parent::eagerLoadedRelationships(), [
            //"deathCounts",
        ]);
    }

}
