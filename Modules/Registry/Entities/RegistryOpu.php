<?php

namespace Modules\Registry\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Entities\Traits\OpuLocationTrait;
use Modules\Registry\Entities\Traits\OpuPermissionsTrait;
use Modules\Registry\Entities\Traits\OpuRoleCheckTrait;
use App\Models\RegistryUnityRole;

/**
 * Class RegistryOpu
 *
 * @package Modules\Registry\Entities
 * @property RegistryHospital $service_location_hospital
 * @property Collection       $hospitals All hospitals which are under the current OPU
 */
class RegistryOpu extends UnityAbstract
{
    use OpuPermissionsTrait;
    use OpuLocationTrait;
    use OpuRoleCheckTrait;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
         'harvestable_organs' => 'array',
    ];


    /**
     * @inheritdoc
     */
    protected $fields_must_filled = [
    ];



    /**
     * get an array of dependant roles
     *
     * @return array
     */
    public function dependantRoles(): array
    {
        return [
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_HARVESTER,
             RegistryUnityRole::ROLE_PROTECTOR,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
             RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
             RegistryUnityRole::ROLE_OPU_SUPERVISOR,
        ];
    }



    /**
     * get an array of administrative roles in which one use can be assigned at a time
     *
     * @return array
     */
    public function administrativeRoles(): array
    {
        return [
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_CHIEF_COORDINATOR,
             RegistryUnityRole::ROLE_CHIEF_PROTECTOR,
        ];
    }



    /**
     * Converts harvestable organs value to array
     *
     * @return array
     */
    private function organsToArray()
    {
        if ($this->harvestable_organs) {
            return explode(',', $this->harvestable_organs);
        }

        return [];
    }



    /**
     * convert model to a resource array in the list view
     *
     * @return array
     */
    public function toListResource(): array
    {
        return [
             "id"                    => $this->hashid,
             'name'                  => $this->name,
             'phones'                => $this->phones,
             'opu_type'              => $this->opu_type,
             'telephone_detection'   => $this->telephone_detection,
             'clinical_detection'    => $this->clinical_detection,
             'university_id'         => $this->university->hashid,
             'university_name'       => $this->university->name,
             'service_location'      => $this->service_location_hospital ? $this->service_location_hospital->hashid : null,
             'service_location_name' => $this->service_location_hospital ? $this->service_location_hospital->name : null,
             "is_active"             => is_null($this->deactivated_at),
             "bed_counts"            => $this->bed_counts,
             "bed_type"              => $this->bed_type,
             "_can_view"             => $this->canView(),
        ];
    }



    /**
     * convert model to a resource array in the single view
     *
     * @return array
     */
    public function toSingleResource(): array
    {
        return [
             "id"                    => $this->hashid,
             'name'                  => $this->name,
             "type"                  => $this->type,
             'phones'                => $this->phones,
             'opu_type'              => $this->opu_type,
             'telephone_detection'   => $this->telephone_detection,
             'clinical_detection'    => $this->clinical_detection,
             "opening_year"          => $this->getMeta("opening_year"),
             'yearly_pmp'            => $this->getMeta("yearly_pmp"),
             "yearly_population"     => $this->getMeta("yearly_population"),
             "yearly_donation"       => $this->getMeta("yearly_donation"),
             'portable_monitor'      => $this->portable_monitor,
             'portable_ventilator'   => $this->portable_ventilator,
             'portable_eeg'          => $this->portable_eeg,
             'vessels_angiography'   => $this->vessels_angiography,
             'isotope_scan'          => $this->isotope_scan,
             "tcd_tools"             => $this->tcd_tools,
             "bed_counts"            => $this->bed_counts,
             "bed_type"              => $this->bed_type,
             //'organ_harvesting'      => $this->organ_harvesting,
             //'harvestable_organs'    => $this->organsToArray(),
             'university_id'         => $this->university->hashid,
             'university_name'       => $this->university->name,
             'service_location'      => $this->service_location_hospital ? $this->service_location_hospital->hashid : null,
             'service_location_name' => $this->service_location_hospital ? $this->service_location_hospital->name : null,
             'hospitals'             => $this->getHospitalsResource(),
             'opu-manager'           => $this->getUserAs('opu-manager')->toFront(),
             "is_active"             => is_null($this->deactivated_at),
        ];
    }



    /**
     * @inheritdoc
     */
    public function canViewAssignedRoles(): bool
    {
        $condition1 = $this->hasAnyUnityRoles([RegistryUnityRole::ROLE_OPU_MANAGER]);
        if ($condition1) {
            return true;
        }

        return $this->university_model->canViewAssignedRoles();
    }



    /**
     * get a dynamic name attribute, regardless of what has been saved in the database
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $hospital = $this->service_location_hospital_model;

        return trans("registry::fields.opu") . SPACE . $hospital->name;
    }



    /**
     * get pmp-related meta fields
     *
     * @return array
     */
    public function pmpMetaFields()
    {
        return [
             "opening_year",
             "yearly_population",
             "yearly_donation",
             "yearly_pmp",
        ];
    }
}
