<?php

namespace Modules\Registry\Entities;

use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Entities\Traits\DeathCountsRelationTrait;

/**
 * Class RegistryHospital
 *
 * @package Modules\Registry\Entities
 * @property RegistryUniversity $university   The university which the hospital is belongs to it.
 */
class RegistryHospital extends UnityAbstract
{
    use DeathCountsRelationTrait;

    const SECTOR_UNKNOWN          = 0;
    const SECTOR_NON_GOVERNMENTAL = 1;
    const SECTOR_GOVERNMENTAL     = 2;



    /**
     * return numeric equivalent of a sector.
     *
     * @param string $sector
     *
     * @return int
     */
    public static function numericValueOfSector(string $sector): int
    {
        $sector = strtolower($sector);
        switch ($sector) {
            case "non-governmental":
                return self::SECTOR_NON_GOVERNMENTAL;
            case "governmental":
                return self::SECTOR_GOVERNMENTAL;
            default:
                return self::SECTOR_UNKNOWN;
        }
    }



    /**
     * return alphabetic equivalent of a sector.
     *
     * @param int $sector
     *
     * @return string
     */
    public static function alphabeticValueOfSector(int $sector): string
    {
        switch ($sector) {
            case self::SECTOR_NON_GOVERNMENTAL:
                return "non-governmental";
            case self::SECTOR_GOVERNMENTAL:
                return "governmental";
            default:
                return 'unknown';
        }
    }



    /**
     * Define accessor for sector
     *
     * @param int $sector
     *
     * @return string
     */
    public function getSectorAttribute($sector): string
    {
        return static::alphabeticValueOfSector($sector);
    }



    /**
     * Define mutator for sector
     *
     * @param string $sector
     */
    public function setSectorAttribute($sector)
    {
        $this->attributes['sector'] = static::numericValueOfSector($sector);
    }



    /**
     * @inheritdoc
     */
    public function canDelete(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canEdit(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canCreate(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function canView(): bool
    {
        $university = $this->university;

        if (!$university) {
            return false;
        }

        if ($university->hasOpuRole()) {
            return true;
        }

        return $university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR,
        ]);
    }



    /**
     * @inheritdoc
     */
    public function canViewAssignedRoles(): bool
    {
        return !is_null($this->university) and $this->university->hasAnyUnityRoles([
                  RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
                  RegistryUnityRole::ROLE_DONOR_EXPERT,
             ]);
    }



    /**
     * @inheritdoc
     */
    public function dependantRoles(): array
    {
        return [];
    }



    /**
     * @inheritdoc
     */
    public function administrativeRoles(): array
    {
        return [
             RegistryUnityRole::ROLE_HOSPITAL_PRESIDENT,
             RegistryUnityRole::ROLE_HOSPITAL_DEPUTY,
        ];
    }



    /**
     * convert model to a resource array in the list view
     *
     * @return array
     */
    public function toListResource(): array
    {
        $results = [
             "id"                    => $this->hashid,
             'name'                  => $this->name,
             'city'                  => $this->city_id? $this->city->hashid : null,
             'city_title'            => $this->city_id? $this->city->title_fa: null,
             'province'              => $this->province_id? $this->province->hashid: null,
             'province_title'        => $this->province_id? $this->province->title_fa: null,
             'phones'                => $this->phones,
             'sector'                => $this->sector,
             'sector_type'           => $this->sector_type,
             'neurosurgery_services' => $this->neurosurgery_services,
             'neurological_services' => $this->neurological_services,
             'trauma_center'         => $this->trauma_center,
             'bed_counts'            => $this->bed_counts,
             "is_active"             => is_null($this->deactivated_at),
             "_can_view"             => $this->canView(),
        ];

        $results['death_counts'] = $this->getDeathCountsArray(3);

        return $results;
    }



    /**
     * @inheritdoc
     */
    public function eagerLoadedRelationships(): array
    {
        return array_merge(parent::eagerLoadedRelationships(), [
             "deathCounts",
        ]);
    }



    /**
     * convert model to a resource array in the single view
     *
     * @return array
     */
    public function toSingleResource(): array
    {
        $opu = $this->opu_model;

        $results = [
             "id"                    => $this->hashid,
             'name'                  => $this->name,
             "opu"                   => $opu->exists ? $opu->hashid : null,
             "opu_name"              => $opu->name,
             "type"                  => $this->type,
             "university"            => $this->university->hashid,
             "university_title"      => $this->university->name,
             'city'                  => $this->city_id? $this->city->hashid : null,
             'city_title'            => $this->city_id? $this->city->title_fa: null,
             'province'              => $this->province_id? $this->province->hashid: null,
             'province_title'        => $this->province_id? $this->province->title_fa: null,
             'address'               => $this->address,
             'latitude'              => $this->latitude,
             'longitude'             => $this->longitude,
             'postal_code'           => $this->postal_code,
             'phones'                => $this->phones,
             'website'               => $this->website,
             'sector_type'           => $this->sector_type,
             "sector_other"          => $this->sector_other,
             'neurosurgery_services' => $this->neurosurgery_services,
             'neurological_services' => $this->neurological_services,
             'trauma_center'         => $this->trauma_center,
             'opu_distance'          => $this->opu_distance,
             'bed_counts'            => $this->bed_counts,
             'hospital-president'    => $this->hospital_president->toFront(),
             'hospital-deputy'       => $this->hospital_deputy->toFront(),
             "is_active"             => is_null($this->deactivated_at),
        ];

        $results['death_counts'] = $this->getDeathCountsArray();

        return $results;
    }



    /**
     * Indicate the current authenticated user can assign a hospital supervisor or not
     *
     * @return bool
     */
    public function canAssignHospitalPresident(): bool
    {
        return $this->university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }



    /**
     * Indicate the current authenticated user can assign a hospital deputy or not
     *
     * @return bool
     */
    public function canAssignHospitalDeputy(): bool
    {
        return $this->university->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
        ]);
    }
}
