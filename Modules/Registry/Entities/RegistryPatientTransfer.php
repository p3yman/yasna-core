<?php

namespace Modules\Registry\Entities;

use Modules\Registry\Entities\Traits\PatientTransferHelpersTrait;
use Modules\Registry\Entities\Traits\PatientTransferResourcesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Registry\Entities\Traits\PatientTransferElectorTrait;
use Modules\Registry\Entities\Traits\PatientTransferRelationsTrait;
use Modules\Registry\Entities\Traits\PatientTransferPermissionsTrait;
use Modules\Registry\Entities\Traits\PatientTransferMutatorAccessorsTrait;

use function is_null;

/**
 * Class RegistryPatientTransfer
 *
 * @property int    $id
 * @property int    $case_id
 * @property int    $city_id
 * @property int    $province_id
 * @property int    $origin_hospital_id
 * @property int    $destination_hospital_id
 * @property int    $transfer_reason
 * @property int    $dismiss_transfer_reason
 * @property string $status
 * @property string $case_final_state
 */
class RegistryPatientTransfer extends YasnaModel
{
    use SoftDeletes;
    use PatientTransferHelpersTrait;
    use PatientTransferElectorTrait;
    use PatientTransferRelationsTrait;
    use PatientTransferPermissionsTrait;
    use PatientTransferMutatorAccessorsTrait;
    use PatientTransferResourcesTrait;

    const STATUS_PENDING   = 'pending';
    const STATUS_DISMISSED = 'dismissed';
    const STATUS_CONFIRMED = 'confirmed';



    /**
     * relations required to render list or single resource of patient transfer with eager loading
     *
     * @return array
     */
    public static function requiredRelations(): array
    {
        return [
             'case',
             'city',
             'originHospital',
             'destinationHospital',
        ];
    }



    /**
     * Transfer the case to new hospital
     *
     * @return YasnaModel
     */
    public function transferToNewHospital()
    {
        $hospital = $this->destination_hospital;
        $data     = [
             'university_id' => $hospital->university_id,
             'opu_id'        => $hospital->opu_id,
             'hospital_id'   => $hospital->id,
             'ward_id'       => 0,
        ];

        return model('registry_case', $this->case_id)->batchSave($data);
    }



    /**
     * Indicate status of transfer is confirmed
     *
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->statusIs(self::STATUS_CONFIRMED);
    }



    /**
     * Indicate status of transfer is dismissed
     *
     * @return bool
     */
    public function isDismissed(): bool
    {
        return $this->statusIs(self::STATUS_DISMISSED);
    }



    /**
     * Indicate status of transfer is pending
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->statusIs(self::STATUS_PENDING);
    }



    /**
     * get main meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "chief_protector_name",
             "chief_protector_degree",
             "escorting_cooperator_name",
        ];
    }



    /**
     * Compare model status with a given status
     *
     * @param string $status
     *
     * @return bool
     */
    private function statusIs($status): bool
    {
        return $this->status === $status;
    }
}
