<?php

namespace Modules\Registry\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Morilog\Jalali\jDate;
use Morilog\Jalali\jDateTime;

class RegistryUnityDeathCount extends YasnaModel
{
    use SoftDeletes;



    /**
     * convert model to a resource array in the list view
     *
     * @return array
     */
    public function toListResource(): array
    {
        return $this->toSingleResource();
    }



    /**
     * convert model to a resource array in the single view
     *
     * @return array
     */
    public function toSingleResource(): array
    {
        $jdate  = sprintf("%d-01-01 00:00:01", $this->year);
        $carbon = jDateTime::createCarbonFromFormat('Y-m-d H:i:s', $jdate);

        return [
             'year'       => $carbon->timestamp,
             'counts'     => $this->death_counts,
             'icu_counts' => $this->icu_death_counts,
        ];
    }
}
