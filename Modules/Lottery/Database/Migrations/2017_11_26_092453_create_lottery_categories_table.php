<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotteryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lottery_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('color')->default('text-info');
            $table->integer('category')->default(0); // 0 is category - 1 is box
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lottery_categories');
    }
}
