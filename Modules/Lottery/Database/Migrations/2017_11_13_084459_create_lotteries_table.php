<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotteries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable()->index();
            $table->string('last_name')->nullable()->index();
            $table->integer('login_status')->default(0)->nullable()->index(); // when people coming login_status set to 1
            $table->timestamp('login_at')->nullable()->index();
            $table->integer('category_id')->default(0)->nullable()->index(); // donator 1, waiting list 2, organ transplant 3
            $table->integer('invitation_number')->nullable()->index();
            $table->integer('lottery_code')->nullable()->index(); // lottery code
            $table->integer('lottery_status')->default(0)->nullable()->index(); // when people win in lottery set lottery_status to 1
            $table->timestamp('lottery_win_at')->nullable()->index();
            $table->string('referred_name')->nullable()->index();
            $table->integer('attendees')->default(0)->index();
            $table->integer('box')->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotteries');
    }
}
