<?php

namespace Modules\Lottery\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class LotteryCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $array = [
          [
              'title' => 'خانواده اهدا کننده' ,
              'color' => 'text-success' ,
              'category' => '0' ,
          ] ,
          [
              'title' => 'لیست انتظار' ,
              'color' => 'text-warning' ,
              'category' => '0' ,

          ],
            [
                'title' => 'گیرنده عضو' ,
                'color' => 'text-danger' ,
                'category' => '0' ,

            ],
            [
                'title' => 'مسئولین ویژه' ,
                'color' => 'text-danger' ,
                'category' => '0' ,
            ] ,
            [
                'title' => 'روسای دانشگاه' ,
                'color' => 'text-warning' ,
                'category' => '0' ,

            ],
            [
                'title' => 'تیم های پیوندی' ,
                'color' => 'text-success' ,
                'category' => '0' ,

            ],
            [
                'title' => 'تیم های اهدا' ,
                'color' => 'text-success' ,
                'category' => '0' ,
            ] ,
            [
                'title' => 'هنرمندان' ,
                'color' => 'text-purple' ,
                'category' => '0' ,

            ],
            [
                'title' => 'ورزشکاران' ,
                'color' => 'text-purple' ,
                'category' => '0' ,

            ],
            [
                'title' => 'اشخاص حقیقی' ,
                'color' => 'text-info' ,
                'category' => '0' ,
            ] ,
            [
                'title' => 'سایر مسئولین' ,
                'color' => 'text-info' ,
                'category' => '0' ,

            ],
            [
                'title' => 'نمایش' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'سخنران' ,
                'color' => '' ,
                'category' => '1' ,
            ] ,
            [
                'title' => 'چرخه اهدا' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'موسیقی' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'حماسه پیوند قلب' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'تقدیر از فعالان' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'تقدیر از مسئولین' ,
                'color' => '' ,
                'category' => '1' ,

            ],
            [
                'title' => 'تقدیر از هنرمندان' ,
                'color' => '' ,
                'category' => '1' ,

            ]

        ];

        yasna()->seed('lottery_categories', $array, true);
    }
}
