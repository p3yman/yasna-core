@extends('lottery::frame')

@section('page-title')
	{{ "Ehda List" }}
@endsection

@section('bottom-assets')
	<!-- Custom js -->
	{!! Html::script (Module::asset('lottery:js/app.js')) !!}
@endsection



@section('content')
	<!-- Statics -->
	<div class="container-fluid  b-statics">
		<div class="row">
			<div class="b-static col-md-3">
                    <span class="b-static-title">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        تعداد افراد شرکت‌کننده:</span>
				<span class="b-static-amount static--all"></span>
			</div>
			<div class="b-static col-md-3">
                    <span class="b-static-title">
<i class="fa fa-user" aria-hidden="true"></i>
                        تعداد دعوت‌نامه‌های پذیرش‌شده:</span>
				<span class="b-static-amount static--family"></span>
			</div>
			<div class="b-static col-md-3">
                    <span class="b-static-title">
<i class="fa fa-ticket" aria-hidden="true"></i>
                        اولین کد قرعه‌کشی:</span>
				<span class="b-static-amount lottery--first"></span>
			</div>
			<div class="b-static col-md-3">
                    <span class="b-static-title">
<i class="fa fa-ticket" aria-hidden="true"></i>
                        آخرین کد قرعه‌کشی:</span>
				<span class="b-static-amount lottery--last"></span>
			</div>
		</div>
	</div>

	<!-- Content -->
	<section class="b-content">
		<div class="container-fluid">
			<!-- Search Input -->
			<div class="row">
				<div class="b-content-search">
					<form class="form-inline" action="">
						<div class="form-group">
							<label for="search">شماره دعوت‌نامه یا نام مدعو:</label>
							<input class="form-control" type="text" id="search" name="search" autocomplete="off">
							<span class="b-input-icon">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
						</div>
					</form>
					<div class="alert alert-danger fade in alert-dismissable search-alert">
						<a href="#" class="close">×</a>
						<span></span>
					</div>
				</div>

			</div>

			<!-- Table of Search targets -->
			<div class="row">
				<div class="b-target-table" style="display: none;">
					<table class="table target-table">
						<thead>
						<tr>
							<th>ردیف</th>
							<th>نام مدعو</th>
							<th>شماره دعوت‌نامه</th>
							<th>نوع دعوت</th>
							<th>تعداد مراجعه‌کننده</th>
							<th>نام مراجعه کننده</th>
							<th>شماره قرعه‌کشی</th>
							<th>عملیات</th>
						</tr>
						</thead>
						<tbody class="target-table-body">
						<script id="targettable" type="text/x-handlebars-template">
							<tr class="@{{class}}" data-id="@{{id}}" data-code="@{{inviteNum}}"
								data-type="@{{inviteType}}" data-status="@{{status}}">
								<td scope="row">@{{count}}</td>
								<td class="td-fullname">
									<span>@{{name}}</span>
									<input type="text" class="form-control table-input--firstname"
										   value="@{{firstName}}" style="display: none;">
									<input type="text" class="form-control table-input--lastname" value="@{{lastName}}"
										   style="display: none;">
								</td>
								<td>@{{inviteNumFa}}</td>
								<td>@{{inviteTypeName}}</td>
								<td class="td-attendee">
									<span style="@{{spanDisplay}}">@{{attendeeCodeFa}}</span>
									<input type="number" class="form-control table-input--attendee"
										   value="@{{attendeeCode}}" style="@{{inputDisplay}}">
								</td>
								<td class="td-name">
									<span style="@{{spanDisplay}}">@{{enteredName}}</span>
									<input type="text" class="form-control table-input--name" style="@{{inputDisplay}}">
								</td>
								<td class="td-lottery-code">
									<span style="@{{spanDisplay}}">@{{enteredLottaryCode}}</span>
									<input type="number" class="form-control table-input--code" maxlength="3"
										   style="@{{inputDisplay}}" @{{disabled}}>
								</td>
								<td class="td-submit">
									<button class="btn btn-success control-btn" name="enter" style="@{{inputDisplay}}">
										ثبت ورود
									</button>
									<button class="btn btn-warning control-btn" name="edit" style="@{{spanDisplay}}">
										ویرایش
									</button>
									<span class="secondary-btn control-btn" style="display: none;">
                                            <button class="btn btn-info" name="update">بروز رسانی</button>
                                            <button class="btn btn-danger" name="delete">حذف</button>
                                        </span>
									<i class="fa fa-refresh fa-spin" aria-hidden="true" style="display: none;"></i>
								</td>
							</tr>
						</script>
						</tbody>
					</table>
				</div>
			</div>

			<!-- Table of contents -->
			<div class="row">
				<div class="b-content-table">
					<button type="button" title="اضافه کردن دعوت‌نامه" class="btn btn-primary add-to-table"
							data-toggle="modal" data-target="#modal">+
					</button>
					<a href="{{ url('/lottery/print') }}" title="پرینت" class="btn btn-warning btn-print">
						<i class="fa fa-print" aria-hidden="true"></i>
					</a>
					<div class="last-update">
					</div>
					<table class="table preview-table">
						<thead>
						<tr>
							<th>ردیف</th>
							<th>نام مدعو</th>
							<th>شماره دعوت‌نامه</th>
							<th>نوع دعوت</th>
							<th>تعداد مراجعه‌کننده</th>
							<th>نام مراجعه کننده</th>
							<th>شماره قرعه‌کشی</th>
						</tr>
						</thead>
						<tbody class="preview-table-body">
						<!-- table row template -->
						<script id="trtemplate" type="text/x-handlebars-template">
							<tr class="@{{class}}" data-id="@{{id}}" data-code="@{{inviteNum}}"
								data-type="@{{inviteType}}" data-status="@{{status}}">
								<td scope="row">@{{count}}</td>
								<td>@{{name}}</td>
								<td>@{{inviteNumFa}}</td>
								<td>@{{inviteTypeName}}</td>
								<td class="td-attendee">
									<span>@{{attendeeCodeFa}}</span>
								</td>
								<td class="td-name">
									<span>@{{enteredName}}</span>
								</td>
								<td class="td-lottery-code">
									<span>@{{enteredLottaryCode}}</span>
								</td>
							</tr>
						</script>

						</tbody>
					</table>
				</div>
			</div>

		</div>
	</section>

	<!-- Modal -->
	<div id="modal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">اضافه کردن دعوت‌نامه</h4>
				</div>
				<div class="modal-body">
					<form id="addnew" class="modal-form">
						<div class="form-group">
							<label for="name">نام</label>
							<input name="first_name" type="text" class="form-control" id="name" required>
						</div>
						<div class="form-group">
							<label for="last-name">نام خانوادگی</label>
							<input name="last_name" type="text" class="form-control" id="last-name" required>
						</div>
						<div class="form-group">
							<label for="invitation">شماره دعوت‌نامه</label>
							<input name="invitation_number" type="number" class="form-control" id="invitation" required>
						</div>
						<div class="form-group">
							<label for="category">نوع دعوت</label>
							<select name="category" class="form-control" id="category" required>
								<option value="0">انتخاب از لیست</option>
								<option value="1">خانواده اهداکننده</option>
								<option value="2">لیست انتظار</option>
								<option value="3">گیرنده عضو</option>
							</select>
						</div>
						<div class="form-group">
							<label for="count">تعداد مراجعه‌کننده</label>
							<input name="attendees" type="number" class="form-control" id="count" required>
						</div>
						<!-- Alerts -->
						<div class="alert fade in form-alert">
							<span></span>
						</div>
						<div class="form-group">
							<button id="submit" class="btn btn-primary" type="submit" name="submit">ثبت دعوت‌نامه
							</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
@endsection