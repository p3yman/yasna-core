@php $id = "option$key" @endphp
<div class="row">
	<label for="{{ $id }}" class="col-sm-3">{{ $label . ":"}}</label>
	<div class="col-sm-6">
		<input type="text" name="options[{{ $key }}]" class="form-control" id="{{ $id }}">
	</div>
	<div class="col-sm-3">
		<div class="radio">
			<label><input type="radio" value="{{ $key }}" name="answer">گزینه صحیح</label>
		</div>
	</div>
</div>