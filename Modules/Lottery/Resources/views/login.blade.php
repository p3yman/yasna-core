<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!-- Bootstrap -->
    {!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.min.css')) !!}
    {!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.rtl.min.css')) !!}


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Font -->
    {!! Html::style(Module::asset('lottery:libs/fontawesome/css/font-awesome.min.css')) !!}
    {!! Html::style(Module::asset('lottery:css/fontiran.css')) !!}

    <!-- Custom Style -->
    {!! Html::style(Module::asset('lottery:css/login-style.min.css')) !!}

</head>
<body>

<!-- Site wrapper -->
<div class="wrapper">
    <header>
        <img src="{{ Module::asset('lottery:images/Custom-logo-white.png') }}" alt="logo" class="b-header-logo-img">
    </header>
    <form class="form-signin" method="post">
        {{ csrf_field() }}
        <h2 class="form-signin-heading">ورود به پنل</h2>
        <input type="text" class="form-control" name="username" placeholder="نام کاربری" required="" autofocus="" />
        <input type="password" class="form-control" name="password" placeholder="رمز عبور" required=""/>
        <!--       <label class="checkbox">
                   <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"> Remember me
               </label>-->
        <button class="btn btn-lg btn-primary btn-block" type="submit">ورود</button>
    </form>
</div>
<!-- !END Site wrapper -->


<!-- End-body Assets -->

<!-- jQuery -->
{!! Html::script (Module::asset('lottery:libs/jquery/jquery.min.js')) !!}

<!-- Bootstrap.js -->
{!! Html::script (Module::asset('lottery:libs/bootstrap/bootstrap.min.js')) !!}


<!-- Custom js -->


<!-- !END End-body Assets -->
</body>
</html>