<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lottery</title>

    <!-- Bootstrap -->
    {!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.min.css')) !!}
    {!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.rtl.min.css')) !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Font -->
    {!! Html::style(Module::asset('lottery:libs/fontawesome/css/font-awesome.min.css')) !!}
    {!! Html::style(Module::asset('lottery:css/fontiran.css')) !!}

    <!-- Custom Style -->
    {!! Html::style(Module::asset('lottery:css/home.css')) !!}

    <script>
        function baseUrl() {
            return "{{ url('/') }}";
        }
        let lotteryCodeMinLength = {{ config('lottery.invitation-number-minimum-length') }};
    </script>

</head>
<body>
<div class="bg">
    <div class="content">
        <div class="inner-content">
            <div class="line1">
                سیزدهمین مراسم بزرگداشت پیوند اعضا
            </div>
            <div class="line2">
                قرعه‌کشی مشهد مقدس برای خانواده‌های ایثارگر اهداکننده
            </div>

            <div class="digits row" dir="ltr" onclick="inputFocus()">
                <div id="dig3" class="col-md-3 img-circle digit">-</div>
                <div id="dig2" class="col-md-3 img-circle digit">-</div>
                <div id="dig1" class="col-md-3 img-circle digit">-</div>
            </div>

            <div class="result result-name" style="display: none">
                خانواده‌ی مرحوم فلان
            </div>

            <div class="result result-buttons" style="display: none">
                <span class="fa fa-check text-success" onclick="next('save')"></span>
                <span class="fa fa-remove text-danger" onclick="next('deny')"></span>
            </div>

            <div class="alert alert-danger alert-box" style="display: none">
                <p>اخطار اینجا نمایش داده می شود</p>
            </div>

            <form action="javascript:action()">
                <input id="txtInput" name="input" class="" autocomplete="off">
            </form>
        </div>
    </div>
</div>

<!-- End-body Assets -->
    <!-- jQuery -->
{!! Html::script (Module::asset('lottery:libs/jquery/jquery.min.js')) !!}

    <!-- Bootstrap.js -->
{!! Html::script (Module::asset('lottery:libs/bootstrap/bootstrap.min.js')) !!}

<!-- Custom js -->
{!! Html::script (Module::asset('lottery:js/home.js')) !!}

<!-- !END End-body Assets -->

</body>
</html>