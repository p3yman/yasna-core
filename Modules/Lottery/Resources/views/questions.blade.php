@extends('lottery::frame')

@section('title')
	{{ "Questions" }}
@endsection

@section('top-assets')
	{!! Html::style(Module::asset('lottery:libs/persian-date/persian-datepicker.min.css')) !!}
@endsection

@section('bottom-assets')
	{!! Html::script (Module::asset('lottery:/libs/jquery.form.min.js')) !!}
	{!! Html::script (Module::asset('lottery:/libs/persian-date/persian-date.min.js')) !!}
	{!! Html::script (Module::asset('lottery:/libs/persian-date/persian-datepicker.min.js')) !!}
	{!! Html::script (Module::asset('lottery:/js/forms.min.js')) !!}
	{!! Html::script (Module::asset('lottery:/libs/jquery-ui/jquery-ui.min.js')) !!}
	{!! Html::script(Module::asset('lottery:js/questions.js')) !!}
@append

@section('content')
	<div id="app">
		<div class="container">
			<!-- From Showing Button -->
			<button class="btn btn-info" id="addFromOpener">
				<i class="fa fa-add"></i>
				افزودن سوال
			</button>
			<!-- !END From Showing Button -->

			<!-- Form -->
			<section class="add-new-form" style="display: none;">
				<h3>
					افزودن سوال جدید
				</h3>
				<div class="panel panel-default">
					<div class="panel-body">
						<form id="question-add-from" class="js" method="post">
							{{ csrf_field() }}
							<div class="row">
								<label for="question" class="col-sm-3">سوال:</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="question" id="question"></textarea>
								</div>
							</div>
							<hr>
							@php(
								$options = [
									[
										"label" => "گزینه اول" ,
										"key" => "1" ,
									],
									[
										"label" => "گزینه دوم" ,
										"key" => "2" ,
									],
									[
										"label" => "گزینه سوم" ,
										"key" => "3" ,
									],
									[
										"label" => "گزینه چهارم" ,
										"key" => "4" ,
									],
								]
							)

							@foreach($options as $option)
								@include('lottery::question-option', $option)
							@endforeach

							<div class="row">
								<label for="" class="col-sm-3">تاریخ شروع و پایان:</label>
								<div class="col-sm-9">
									<div class="col-sm-6">
										<input class="form-control" id="range-from" placeholder="از تاریخ">
										<input class="hidden" id="range-from-alt" name="starts_at">
									</div>
									<div class="col-sm-6">
										<input class="form-control" id="range-to" placeholder="تا تاریخ">
										<input class="hidden" id="range-to-alt" name="ends_at">
									</div>
								</div>
							</div>

							<hr>
							{!! widget('feed') !!}
							<button class="btn btn-success">
								ثبت سوال
							</button>
							<button class="btn btn-default" type="button" id="addFormCloser">
								انصراف
							</button>
						</form>
					</div>
				</div>
			</section>
			<!-- !END Form -->

			<!-- Questions List -->
			<div class="questions-list">
				<hr>

				@foreach($questions as $question)
				<div class="question-item">
					<div class="question-state">
						<div class="question-title">
							{{ pd($loop->iteration). ". " . $question['question'] }}
						</div>
						<div class="question-options-status">
							@foreach($question['options_info'] as $value => $option)
							<div class="question-option" data-option="1">
								<div class="row">
									<div class="col-xs-9">
										{{ pd($loop->iteration). ". " . $option['text'] }}
									</div>
									<div class="col-xs-2">
									<span class="count">
										{{ pd($option['count']) . " رای" }}
									</span>
									</div>
									<div class="col-xs-1">
									<span class="percent">
										{{ pd($option['percent'])."٪" }}
									</span>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
					@if ($question->drawable_messages or !empty($question->winners))
						<div class="question-lottery" style="display: none;">
							<div class="text-center">
								<button id="draw-{{ $question->hashid }}" class="btn btn-warning lotteryButton"
										data-question="{{ $question->hashid }}"
										@if(!$question->drawable_messages) style="display: none" @endif>
									قرعه‌کشی
								</button>
								<div class="alert alert-danger" style="display: none">
									امکان قرعه‌کشی دیگری وجود ندارد.
								</div>
							</div>

							<div class="lottery-result">
								<ul>
									{{--@foreach($question['winners'] as $winner)--}}
									{{--<li>{{ pd($loop->iteration). "- " . pd($winner) }}</li>--}}
									{{--@endforeach--}}
								</ul>
							</div>
						</div>
						<div class="lottery-result">
							<ul>
								{{--@foreach($question['winners'] as $winner)--}}
								{{--<li>{{ pd($loop->iteration). "- " . pd($winner) }}</li>--}}
								{{--@endforeach--}}
								{{-- @TODO: Use the new structure (below) --}}
								{{--<li>--}}
									{{--<span class="iteration">۱.</span>--}}
									{{--<span class="num" style="direction: ltr; display: inline-block">--}}
										{{--۰۹۳۶۶۸۸۷۶۷۸--}}
									{{--</span>--}}
								{{--</li>--}}
							</ul>
						</div>
					@endif
				</div>
				@section('bottom-assets')
					<script>
						$(document).ready(function () {
							let winners = {!! json_encode($question->spreadMeta()->winners ?: []) !!};
							let btn = $("#draw-{{ $question->hashid }}");
							for(var i = 0; i < winners.length; i++){
								renderResult(btn, winners[i]);
							}
                        });
					</script>
				@append
				@endforeach
			</div>
			<!-- !END Questions List -->
		</div>
	</div>

@endsection
