<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		@yield('page-title')
	</title>

	<!-- Bootstrap -->
{!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.min.css')) !!}
{!! Html::style(Module::asset('lottery:libs/bootstrap/bootstrap.rtl.min.css')) !!}


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Font -->
	{!! Html::style(Module::asset('lottery:libs/fontawesome/css/font-awesome.min.css')) !!}
	{!! Html::style(Module::asset('lottery:css/fontiran.css')) !!}

<!-- Custom Style -->
	{!! Html::style(Module::asset('lottery:css/screen.min.css')) !!}
	@yield('top-assets')
	<script>
        function baseUrl() {
            return "{{ url('/') }}";
        }
        let lotteryCodeMinLength = {{ config('lottery.invitation-number-minimum-length') }};
        let drawUrl = "{{ route('lottery.draw') }}";
        let _token = "{{ csrf_token() }}";
	</script>

</head>
<body>
<form id="hasan"></form>
<!-- Site wrapper -->
<div class="wrapper">
	<!-- Header -->
	<header class="b-header">
		<div class="container">
			<div class="row">
				<div class="b-header-logo">
					<img src="{{ Module::asset('lottery:images/Custom-logo-white.png') }}" alt="logo" class="b-header-logo-img">
				</div>
				<div class="b-header-btn-group">
					<a href="{{ url('/lottery/logout') }}" class="btn btn-danger">
						<i class="fa fa-power-off" aria-hidden="true"></i>
						خروج
					</a>
				</div>
			</div>
		</div>
	</header>

	@yield('content')

</div>
<!-- !END Site wrapper -->

<!-- End-body Assets -->

<!-- jQuery -->
{!! Html::script (Module::asset('lottery:libs/jquery/jquery.min.js')) !!}

<!-- Bootstrap.js -->
{!! Html::script (Module::asset('lottery:libs/bootstrap/bootstrap.min.js')) !!}


@yield('bottom-assets')

<!-- !END End-body Assets -->
</body>
</html>