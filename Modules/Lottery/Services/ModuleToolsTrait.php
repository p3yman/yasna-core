<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/21/18
 * Time: 11:36 AM
 */

namespace Modules\Lottery\Services;

use Modules\Yasna\Services\ModuleHelper;

trait ModuleToolsTrait
{
    /**
     * @return string
     */
    protected static function moduleName()
    {
        $name  = get_class(new static);
        $array = explode("\\", $name);

        if (array_first($array) == 'Modules') {
            return $array[1];
        }

        return null;
    }



    /**
     * @return ModuleHelper
     */
    protected static function module()
    {
        return module(static::moduleName());
    }
}
