/**
 * Lottery Questions Section
 * -------------------------
 * Created Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2018-05-18
 */
// global method
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

jQuery(function($){
    let addFrom = $('.add-new-form');
    let addFromOpener = $('#addFromOpener');
    let addFormCloser = $('#addFormCloser');
    let allLotterySections = $('.question-lottery');

    censoring();

    // Form Opening
    addFromOpener.on('click',function () {
        $(this).hide();
        addFrom.slideDown();
    });

    addFormCloser.on('click',function () {
        addFrom.slideUp();
        addFromOpener.show();
        $('#question-add-from').trigger('reset');
    });

    $('.question-title').on('click',function () {
        let lotterySection = $(this).parent('.question-state')
                                    .siblings('.question-lottery');

        allLotterySections.filter(function () {
            return !($(this)[0] === lotterySection[0]);
        }).slideUp();

        lotterySection.slideToggle();
    });

    $('.lotteryButton').on('click',function () {
       let btn = $(this);

       $.ajax({
           url: drawUrl,
           beforeSend: function() {
               btn.closest('.question-lottery').addClass('loading');
           },
           data: {
               _token: _token,
               question: btn.data('question')
           },
           type: 'POST',
           success: function (number) {
                renderResult(btn, number);
                btn.closest('.question-lottery').removeClass('loading');
           },
           error: function (rs) {
               if (rs.status == 410) {
                   btn.closest('.question-lottery').removeClass('loading');
                   btn.hide();
                   btn.parent().find('.alert-danger').show();
               }
           }
       })

    });


    /*
    *-------------------------------------------------------
    * Persian Datepicker
    *-------------------------------------------------------
    */

    let to, from;
    to = $("#range-to").persianDatepicker({
        altField: '#range-to-alt',
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            }
        },
        initialValue: false,
        onSelect: function (unix) {
            to.touched = true;
            if (from && from.options && from.options.maxDate != unix) {
                let cachedValue = from.getState().selected.unixDate;
                from.options = {maxDate: unix};
                if (from.touched) {
                    from.setDate(cachedValue);
                }
            }
        }
    });
    from = $("#range-from").persianDatepicker({
        altField: '#range-from-alt',
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            }
        },
        initialValue: false,
        onSelect: function (unix) {
            from.touched = true;
            if (to && to.options && to.options.minDate != unix) {
                let cachedValue = to.getState().selected.unixDate;
                to.options = {minDate: unix};
                if (to.touched) {
                    to.setDate(cachedValue);
                }
            }
        }
    });

}); //End Of siaf!

/*
 *-------------------------------------------------------
 * Global functions
 *-------------------------------------------------------
 */

function renderResult(btn, number) {
    let parent = btn.parents('.question-lottery');
    let resultList = parent
        .find('.lottery-result')
        .find('ul');

    let iteration =  pd(resultList.find('li').length + 1) + ". ";
    let text = pd(number);

    resultList.append('<li> <span class="iteration">'+ iteration + '</span><span class="num" style="direction: ltr; display: inline-block">'+ text + '</span></li>');

    censoring();
}

function censoring() {
    let phoneNumbers = $('.questions-list .lottery-result ul li');

    phoneNumbers.each(function () {
        let el = $(this);
        if(typeof el.attr('data-original') !== "undefined"){
            return;
        }
        let number = el.find('.num').text().trim();
        let censored = number.replace(number.slice(4,7), "***");

        el.attr('data-original', number);
        el.attr('data-censored', censored);
        el.addClass('censored');


        el.find('.num').text(censored);
    })
        .off('click').on('click',function () {
            let el = $(this);

            if(el.hasClass('censored')){
                el.find('.num').text( el.attr('data-original'));
                el.removeClass('censored');
            }else{
                el.find('.num').text( el.attr('data-censored'));
                el.addClass('censored');
            }
        })

}


// Digit lang change function

function forms_pd($string) {
    // if (!$string){
    //     return;
    // }
    $string = $string.toString();

    $string = $string.replaceAll(/1/g, "۱");
    $string = $string.replaceAll(/2/g, "۲");
    $string = $string.replaceAll(/3/g, "۳");
    $string = $string.replaceAll(/4/g, "۴");
    $string = $string.replaceAll(/5/g, "۵");
    $string = $string.replaceAll(/6/g, "۶");
    $string = $string.replaceAll(/7/g, "۷");
    $string = $string.replaceAll(/8/g, "۸");
    $string = $string.replaceAll(/9/g, "۹");
    $string = $string.replaceAll(/0/g, "۰");

    return $string;
}

function ed(faDigit) {
    return forms_digit_en(faDigit);
}

function forms_digit_fa(enDigit) {
    return forms_pd(enDigit);

    var newValue = "";
    for (var i = 0; i < enDigit.length; i++) {
        var ch = enDigit.charCodeAt(i);
        if (ch >= 48 && ch <= 57) {
            var newChar = ch + 1584;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else {
            newValue = newValue + String.fromCharCode(ch);
        }
    }
    return newValue;
}

function forms_digit_en(perDigit) {
    var newValue = "";
    for (var i = 0; i < perDigit.length; i++) {
        var ch = perDigit.charCodeAt(i);
        if (ch >= 1776 && ch <= 1785) // For Persian digits.
        {
            var newChar = ch - 1728;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else if (ch >= 1632 && ch <= 1641) // For Arabic & Unix digits.
        {
            var newChar = ch - 1584;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else
            newValue = newValue + String.fromCharCode(ch);
    }
    return newValue;
}

function pd(enDigit) {
    return forms_digit_fa(enDigit);

}

