/*
*-------------------------------------------------------
* Ehda Data List
*
* Created by Yasna Team
* 2017-11-08
*-------------------------------------------------------
*/
// global method
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

/*
*-------------------------------------------------------
* Document ready functions
*-------------------------------------------------------
*/

jQuery(function($){

    var mainTemplate = $.trim( $('#trtemplate').html() ),
        mainListLocation = $('.preview-table-body'),
        targetTemplate = $.trim( $('#targettable').html() ),
        targetLocation = $('.target-table-body'),
        upperTable = targetLocation.parents('.b-target-table'),
        typeClass = "",
        alertBox = $('.search-alert'),
        alert = alertBox.find('span'),
        search = $('#search'),
        searchVal,
        searchType = $('#list-type'),
        searchTypeVal,
        timer = $('.last-update'),
        boxSelector = $('#box-type'),
        boxLists = $('select[name=box]'),
        categoryList = $('select[name=category]');

    // Update select lists on load
        //box
    getBoxOptions(boxLists);
    getBoxOptions($('#filter-box'));
        //category
    getCategory(categoryList);
    getCategory($('#filter-category'));

    // Gets data for Main Table
    getPeopleList(boxSelector.val());

    // focus on search bar
    search.focus();

    // function called after typing 3 character in search input
    search.on('keyup', function () {
        alertMessage.hide();
        searchVal = search.val();
        searchTypeVal = searchType.val();

        if(searchVal.length >= 3){
            searchRows(searchVal,searchTypeVal)
        }else{
            // empty upper table
            upperTable.hide();
            targetLocation.find('tr').remove();
        }
    })
        .on('keypress', function (e) {
            var key = e.which;
            // Enter key code
            if( key=== 13){
                e.preventDefault();
            }
        });

    // function called after changing search type
    searchType.on('change',function () {
        search.trigger('keyup');
    });

    // On Filter change
    boxSelector.one('change', function () {
        $('.box-selector i').show();
        var value = boxSelector.val();
        getPeopleList(value);
    });

    // Gets data with Ajax
    function getPeopleList(param) {

        var j = 1;
        if(j>0){
            $.ajax({
                url     : baseUrl() + '/lottery/get_special/'+param,
                cache   : false,
                dataType: "json",
                method  : "GET",
            }).done(function ($result) {
                updateList($result,mainTemplate,mainListLocation);
                updateStatics($result);
                lastUpdate($result.last_update);
                $('.box-selector i').hide();
            });
            j--;
        }
        setTimeout(function () {
            getPeopleList(boxSelector.val())
        },30000);

    }

    // Update statics
    function updateStatics(result) {
        var statics = $('.b-statics'),
            staticAll = statics.find('.static--all'),
            staticFamily = statics.find('.static--family'),
            allVal = result.stats.all,
            familyVal = result.stats.family;


        addStat(staticAll,allVal);
        addStat(staticFamily,familyVal);

    }
    // Add Statistics
    function addStat(place, val) {
        if(val>0){
            place.empty().text(pd(val)).show();
            place.siblings('.b-static-title').show();
        }else{
            if(place.hasClass('lottery--first') || place.hasClass('lottery--last')){
                place.siblings('.b-static-title').hide();
            }
            place.hide();
        }
    }

    // Gets select option
    function getBoxOptions(location) {

        var j = 1;
        if(j>0){
            $.ajax({
                url     : baseUrl() + '/lottery/get_box',
                cache   : false,
                dataType: "json",
                method  : "GET",
            }).done(function ($result) {
              location.append(makeOption($result));
            });
            j--;
        }
    }

    // Make options list
    function makeOption(options) {
        var list = "";
        $.each(options,function (index,option) {
            list += "<option value ='"+ option.id + "'>"+ option.title + "</option>"
        });
        return list;
    }

    // Gets select option
    function getCategory(location) {

        var j = 1;
        if(j>0){
            $.ajax({
                url     : baseUrl() + '/lottery/get_category',
                cache   : false,
                dataType: "json",
                method  : "GET",
            }).done(function ($result) {
                location.append(makeOption($result));
            });
            j--;
        }
    }

    // Updates list data using template
    function updateList(listContent,template,location) {
        var frag = "",
            inviteNumFa,
            referredName,
            lotteryCode,
            invitationNum,
            inputDisplay,
            spanDisplay,
            buttonDisplay,
            count = 0;
        location.find('tr').remove();

        if(template === mainTemplate){
            $.each(listContent.data,function (index, obj) {


                typeClass = obj.color;
                if(obj.login_status === 1){
                    typeClass += " entered"
                }
                //inviteNumFa = pd(obj.invitation_number);
                inviteNumFa = obj.invitation_number;
                referredName = (! obj.referred_name)? "" : obj.referred_name;
                // lotteryCode = (! obj.lottery_code)? "" : pd(obj.lottery_code);
                lotteryCode = (! obj.lottery_code)? "" : obj.lottery_code;
                invitationNum = (! obj.invitation_number)? "" : obj.invitation_number;
                //count = count + 1;

                frag +=
                    template.replace( /{{id}}/ig , obj.id )
                        .replace( /{{class}}/ig , typeClass )
                        .replace( /{{inviteType}}/ig , obj.category )
                        .replace( /{{status}}/ig , obj.login_status )
                        // .replace( /{{count}}/ig , pd(count) )
                        //.replace( /{{count}}/ig , count )
                        .replace( /{{inviteNum}}/ig , invitationNum)
                        .replace( /{{name}}/ig , obj.full_name )
                        .replace( /{{inviteNumFa}}/ig , invitationNum)
                        .replace( /{{inviteTypeName}}/ig , obj.category )
                        .replace( /{{enteredName}}/ig , referredName)
                        .replace( /{{box}}/ig , obj.box_name)
                        //.replace( /{{attendeeCodeFa}}/ig , pd(obj.attendees));
                        .replace( /{{attendeeCodeFa}}/ig , obj.attendees);

            });

            location.append(frag);

        }else{
            $.each(listContent,function (index, obj) {

                typeClass = obj.color;
                if(obj.login_status === 1){
                    typeClass += " entered"
                }
                //inviteNumFa = pd(obj.invitation_number);
                inviteNumFa = obj.invitation_number;
                referredName = (! obj.referred_name)? "" : obj.referred_name;
                // lotteryCode = (! obj.lottery_code)? "" : pd(obj.lottery_code);
                lotteryCode = (! obj.lottery_code)? "" : obj.lottery_code;
                invitationNum = (! obj.invitation_number)? "" : obj.invitation_number;
                //count = count + 1;

                if(obj.category_id<=3){
                    buttonDisplay = 'disabled';
                }else {
                    buttonDisplay = '';
                }

                if(obj.login_status === 0){
                    inputDisplay = 'display:block;';
                    spanDisplay = 'display:none;';
                }else if(obj.login_status === 1){
                    inputDisplay = 'display:none;';
                    spanDisplay = 'display:block;';
                }

                frag +=
                    template.replace( /{{id}}/ig , obj.id )
                        .replace( /{{class}}/ig , typeClass )
                        .replace( /{{inviteType}}/ig , obj.category )
                        .replace( /{{status}}/ig , obj.login_status )
                        // .replace( /{{count}}/ig , pd(count) )
                        //.replace( /{{count}}/ig , count )
                        .replace( /{{inviteNum}}/ig , invitationNum)
                        .replace( /{{name}}/ig , obj.full_name )
                        .replace( /{{firstName}}/ig , obj.first_name )
                        .replace( /{{lastName}}/ig , obj.last_name )
                        .replace( /{{inviteNumFa}}/ig , invitationNum )
                        .replace( /{{inviteTypeName}}/ig , obj.category )
                        .replace( /{{enteredName}}/ig , referredName)
                        .replace( /{{attendeeCode}}/ig , obj.attendees)
                        .replace( /{{box}}/ig , obj.box_name)
                        .replace( /{{boxId}}/ig , obj.box)
                        // .replace( /{{attendeeCodeFa}}/ig , pd(obj.attendees))
                        .replace( /{{attendeeCodeFa}}/ig , obj.attendees)
                        .replace( /{{btnDisplay}}/ig , buttonDisplay)
                        .replace( /{{inputDisplay}}/ig , inputDisplay)
                        .replace( /{{spanDisplay}}/ig , spanDisplay);


            });
            location.append(frag);
        }

    }

    // gets searched row with Ajax
    function searchRows(searchVal,searchTypeVal) {
        var j = 1,
            targetRow;

        if(j>0){
            $.ajax({
                url     : baseUrl() + '/lottery/search_special_people',
                cache   : false,
                dataType: "json",
                method  : "POST",
                data: {
                    invitation_number: searchVal,
                    type: searchTypeVal
                }
            }).done(function ($result) {
                updateList($result,targetTemplate,targetLocation);
                selectElement();
                upperTable.show();
                targetRow = targetLocation.find('tr');
                findRow(targetRow);
            });
            j--;
        }
        

    }

    // Focus on row or show error
    function findRow(targetRow) {

        if(targetRow.length){

            getBoxOptions($('.td-box select'));

            if(targetRow.length === 1){
                //if found, show.
                search.blur();
                targetRow.show();
                targetRow.first().find('.table-input--name').eq(0).focus();
            }
        }else {
            //if NOT found, alert.
            alertMessage.show("شماره دعوت مورد نظر یافت نشد.");
        }

    }

    // Update time
    function lastUpdate(time) {
        timer.empty().text(time);
    }

    // Alert Message Object
    var alertMessage = {
        show: function (message) {
            alert.text(message);
            alertBox.show();
        },
        hide: function () {
            alert.text("");
            alertBox.hide();
        }
    };

    /*
     *-------------------------------------------------------
     * Searching
     *-------------------------------------------------------
     */
    function selectElement(){

        var table = $('.target-table'),
            tbodyRows = table.find('tbody tr'),
            enterBtn = tbodyRows.find('button[name=enter]'),
            editBtn = tbodyRows.find('button[name=edit]'),
            deleteBtn = tbodyRows.find('button[name=delete]'),
            updateBtn = tbodyRows.find('button[name=update]'),
            rowBox = tbodyRows.find('.td-box span');


        //input enter action -> next
        $('.table-input--name').on('keypress', function (e) {
            var key = e.which;
            if(key===13){
                $(this).parents('tr').find('.table-input--code').focus();
            }
        });

        //input enter action -> submit
        $('#box-input').on('keypress', function (e) {
            var key = e.which,
                tr = $(this).parents('tr');
            if(key===13){

                if(tr.attr("data-status") === 0){
                    tr.find('button[name=enter]').click();
                }else {
                    tr.find('button[name=update]').click();
                }
            }
        });

        // number input scroll disable
        $('input[type=number]').on('mousewheel', function(e){
            e.preventDefault();
        });


        //closing alert box
        alertBox.children('.close').on('click',function (e) {
            e.preventDefault();
            alertMessage.hide();
        });


        // Function called on Entering data
        enterBtn.unbind('click').bind('click', getData);

        // Function called on editing data
        editBtn.unbind('click').bind('click', editData);

        // Function called when deleting data
        deleteBtn.unbind('click').bind('click', deleteData);

        // Function called on updating data
        updateBtn.unbind('click').bind('click', getData);

        rowBox.unbind('dblclick').bind('dblclick', editBox);


        // Gets data to record entrance
        function getData() {

            var btn = $(this),
                tr = btn.parents('tr'),
                id =tr.data('id'),
                status = tr.attr('data-status') ,
                type = parseInt(tr.attr('data-type')) ,
                img = triggerLoadingImg(btn),
                nameInput = tr.find('.table-input--name'),
                name = nameInput.val(),
                attendeesInputs = tr.find('.table-input--attendee'),
                attendees = attendeesInputs.val(),
                firstName = tr.find('.table-input--firstname').val(),
                lastName = tr.find('.table-input--lastname').val();


                        //disable btn
                        btn.attr("disabled","disabled");
                        // show loading
                        img.show();

                        // ajax call
                        var j = 1;
                        if(j>0){
                            $.ajax({
                                url     : baseUrl() + '/lottery/set_login',
                                cache   : false,
                                dataType: "json",
                                method  : "POST",
                                data: {
                                    id: id,
                                    first_name:firstName,
                                    last_name:lastName,
                                    referred_name: name,
                                    status: status,
                                    attendees: attendees
                                }
                            }).done(function ($result) {
                                if( $result.result >= 1){
                                    entered(btn,tr,firstName,lastName,attendees,name);
                                }else if($result.result === 0){
                                    alertMessage.show("عملیات با موفقیت انجام نشد. مجددا تلاش کنید.");
                                    searchRows(searchVal);
                                }else if($result.result === -1){
                                    //alertMessage.show("کد قرعه‌کشی تکراری می‌باشد.");
                                }
                                //disable btn
                                btn.removeAttr("disabled","disabled");
                                // show loading
                                img.hide();
                            });
                            j--;
                        }

        }

        // Records entrance
        function entered(btn,tr,fistnameVal,lastnameVal,attendeesVal,nameVal) {
            //remove alerts if exist
            alertMessage.hide();

            // show edit btn
            tr.find('.control-btn').hide();
            tr.find('[name=edit]').show();

            //edit Fullname
            tr.find('.td-fullname span').empty()
                .text(fistnameVal +" "+ lastnameVal).show()
                .siblings('input').hide();

            // replace input with entered data
            tr.find('.td-attendee span').empty()
                // .text(pd(attendeesVal)).show()
                .text(attendeesVal).show()
                .next().hide();

            // replace input with entered data
            tr.find('.td-name span').empty()
                .text(nameVal).show()
                .next().hide();

            tr.attr("data-status", 1);

            // change style of row
            tr.addClass('entered');
        }

        // Edits data
        function editData() {
            var btn = $(this),
                tr = btn.parents('tr'),
                attendeesInputs = tr.find('.td-attendee span'),
                attendees = ed(attendeesInputs.text()),
                nameInput = tr.find('.td-name span'),
                name = nameInput.text(),
                boxInput = tr.find('.td-box span'),
                boxCode = boxInput.text(),
                fullnameSpan = tr.find('.td-fullname span'),
                fullnameInputs = tr.find('.td-fullname input');


            // change btn
            tr.find('.control-btn').hide();
            tr.find('.secondary-btn').show();

            //shows inputs again
            fullnameSpan.hide();
            fullnameInputs.show();

            attendeesInputs.hide()
                .next().val(attendees).show();
            nameInput.hide()
                .next().val(name).show();

            // Change style of row to its default
            tr.removeClass('entered');
        }

        // Deletes Data
        function deleteData() {
            var btn = $(this),
                tr = btn.parents('tr'),
                id =tr.data('id'),
                status = tr.attr('data-status'),
                img = triggerLoadingImg(btn);

            // disable btn
            btn.attr("disabled","disabled");
            // show loading
            img.show();
            // ajax call
            var j = 1;
            if(j>0){
                $.ajax({
                    url     : baseUrl() + '/lottery/set_logout',
                    cache   : false,
                    dataType: "json",
                    method  : "POST",
                    data: {
                        id: id,
                        status: status
                    }
                }).done(function ($result) {
                    if( $result.result === 1){

                        // change btn
                        tr.find('.control-btn').hide();
                        tr.find('[name=enter]').show();

                        tr.find('.td-fullname span').show()
                            .siblings('input').hide();

                        //shows inputs again
                        tr.find('.td-name span')
                            .hide().empty()
                            .next().val("").show();

                        // Reset the status
                        tr.attr("data-status", 0);

                        // Change style of row to its default
                        tr.removeClass('entered');

                    }else{
                        alertMessage.show("عملیات با موفقیت انجام نشد. مجددا تلاش کنید.");
                        searchRows(searchVal);
                    }
                    //disable btn
                    btn.removeAttr("disabled","disabled");

                    // show loading
                    img.hide();
                });
                j--;
            }


        }

        // edit box type
        function editBox() {
            var boxSpan = $(this),
                tr = boxSpan.parents('tr'),
                btn = tr.find('.td-submit button'),
                trId = tr.attr('data-id'),
                boxSelect = boxSpan.siblings('select[name=box]'),
                boxSelected = tr.attr('data-box');

            // set select value
            boxSelect.val(boxSelected);

            //showing select
            boxSpan.hide();
            boxSelect.show();

            // Saving box value
            boxSelect.one('change',function () {
                var boxVal = boxSelect.val(),
                    boxTitle = $(this).find('option:selected').text();


                    //disable btn
                    btn.attr("disabled","disabled");

                    // Ajax: Box value saving
                    var j = 1;
                    if(j>0){
                        $.ajax({
                            url     : baseUrl() + '/lottery/change_box',
                            cache   : false,
                            dataType: "json",
                            method  : "POST",
                            data: {
                                id: trId,
                                box: boxVal
                            }
                        }).done(function ($result) {
                            if( $result.status >= 1){
                                // go back to default veiw
                                boxSpan.empty().text(boxTitle)
                                    .show();
                                boxSelect.hide();
                            }else if($result.status === 0){
                                alertMessage.show("عملیات با موفقیت انجام نشد. مجددا تلاش کنید.");
                            }else if($result.status === -1){
                                location.reload();
                            }
                            //disable btn
                            btn.removeAttr("disabled","disabled");
                        });
                        j--;
                    }
            });

        }

        // ajax loading image
        function triggerLoadingImg(btn) {
            return btn.parents('.td-submit').find('.fa-refresh');
        }

    }

    /*
     *-------------------------------------------------------
     * Form Submit
     *-------------------------------------------------------
     */
    var form = $('#addnew'),
        formAlertBox = $('.form-alert'),
        formAlertText = formAlertBox.find('span');

    var formAlert = {
        show: function (message,type) {
            if(type === "danger"){
                formAlertBox.addClass('alert-danger');
            }else if(type === "success"){
                formAlertBox.addClass('alert-success');
            }
            formAlertText.text(message);
            formAlertBox.show();
        },
        hide: function () {
            formAlertBox.hide();
            formAlertBox.removeClass('alert-danger alert-success');
            formAlertText.text("");
        }
    };

    $('.modal .close').on('click',function () {
        form.trigger('reset');
    });

    // submit form
    form.on('submit',function (e) {
        var formData = form.serialize();
        e.preventDefault();
        formAlert.hide();

        if(validateForm()){
            formSubmit(formData);
        }else {
            formAlert.show("خطا در ورود اطلاعات","danger")
        }
    });

    // Checks all the inputs are filled
    function validateForm() {
        var invalid = 0;
        $('.modal-form .controlling').each(function () {
            var value = $(this).val();
            if (value === '' || parseInt(value) === 0) {
                invalid++;
            }
        });

        $('.modal-form input[type=number]').each(function () {
            var value = $(this).val();

            if(isNaN(value)){
                invalid++
            }
        });


        /*if( $('#invitation').val().length < 3 ){
            invalid++
        }*/


        if(invalid > 0) {
            return false;
        } else {
            return true;
        }

    }

    // Ajax: submits the form
    function formSubmit(formData) {
        var j = 1;
        if(j>0){
            $.ajax({
                url     : baseUrl() + '/lottery/add_special',
                cache   : false,
                dataType: "json",
                method  : "POST",
                data: formData
            }).done(function ($result) {
                if($result >= 1){
                    formAlert.show('دعوت‌نامه با موفقیت به لیست اضافه شد.',"success");
                    setTimeout(function () {
                        form.trigger('reset');
                        formAlert.hide();
                    }, 2000);
                }else {
                    formAlert.show("ارسال فرم با خطا مواجه شد. دوباره تلاش کنید.","danger")
                }

            });
            j--;
        }
    }
    

}); //End Of siaf!


/*
*-------------------------------------------------------
* Global functions
*-------------------------------------------------------
*/

// Digit lang change function

function forms_pd($string) {
    // if (!$string){
    //     return;
    // }
    $string = $string.toString();

    $string = $string.replaceAll(/1/g, "۱");
    $string = $string.replaceAll(/2/g, "۲");
    $string = $string.replaceAll(/3/g, "۳");
    $string = $string.replaceAll(/4/g, "۴");
    $string = $string.replaceAll(/5/g, "۵");
    $string = $string.replaceAll(/6/g, "۶");
    $string = $string.replaceAll(/7/g, "۷");
    $string = $string.replaceAll(/8/g, "۸");
    $string = $string.replaceAll(/9/g, "۹");
    $string = $string.replaceAll(/0/g, "۰");

    return $string;
}

function ed(faDigit) {
    return forms_digit_en(faDigit);
}

function forms_digit_fa(enDigit) {
    return forms_pd(enDigit);

    var newValue = "";
    for (var i = 0; i < enDigit.length; i++) {
        var ch = enDigit.charCodeAt(i);
        if (ch >= 48 && ch <= 57) {
            var newChar = ch + 1584;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else {
            newValue = newValue + String.fromCharCode(ch);
        }
    }
    return newValue;
}

function forms_digit_en(perDigit) {
    var newValue = "";
    for (var i = 0; i < perDigit.length; i++) {
        var ch = perDigit.charCodeAt(i);
        if (ch >= 1776 && ch <= 1785) // For Persian digits.
        {
            var newChar = ch - 1728;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else if (ch >= 1632 && ch <= 1641) // For Arabic & Unix digits.
        {
            var newChar = ch - 1584;
            newValue = newValue + String.fromCharCode(newChar);
        }
        else
            newValue = newValue + String.fromCharCode(ch);
    }
    return newValue;
}

function pd(enDigit) {
    return forms_digit_fa(enDigit);

}


