<?php

namespace Modules\Lottery\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AsanakController extends Controller
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public function listen(Request $request)
    {
        model('message')->batchSave([
             'source'      => '0' . str_after($request->Source, '98'),
             'destination' => '0' . str_after($request->Destination, '98'),
             'received_at' => $this->timeIsInt($request->ReceiveTime)
                  ? Carbon::createFromTimestamp($request->ReceiveTime)
                  : Carbon::parse($request->ReceiveTime),
             'text'        => ed($request->MsgBody),
        ]);

        return 'done';
    }



    /**
     * @param $time
     *
     * @return bool
     */
    protected function timeIsInt($time)
    {
        return is_numeric($time);
        return (((string)intval($time)) == $time);
    }
}
