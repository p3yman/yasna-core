<?php

namespace Modules\Lottery\Http\Controllers;

use App\Models\Lottery;
use App\Models\LotteryCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    public $zone = 'A
A
A
A
A
A
A
E
E
E
E
E
E
E
B
B
B
B
B
B
B
C
C
C
C
C
C
C
D
D
D
D
D
D
D';
    public $row = '1
2
3
4
5
6
7
1
2
3
4
5
6
7
1
2
3
4
5
6
7
1
2
3
4
5
6
7
1
2
3
4
5
6
7';
    public $from = '1
1
1
1
1
1
1
1
1
1
1
1
1
1
34
37
38
42
45
48
51
15
17
17
19
20
21
22
1
1
1
1
1
1
1';
    public $to = '25
22
21
19
17
15
14
25
22
21
19
17
15
14
47
51
54
59
63
67
72
33
36
37
41
44
47
50
14
16
16
18
19
20
21';

    public function index()
    {
        $zone = explode("\n", $this->zone);
        $row = explode("\n", $this->row);
        $from = explode("\n", $this->from);
        $to = explode("\n", $this->to);

        echo '<table>';
        for ($i = 0; $i < count($zone); $i++) {
//            echo $zone[$i] . ' - ' . $row[$i] . ' - ' . $from[$i] . ' - ' . $to[$i] . '<br>';
            $fr = (int)$from[$i];
            $t = (int)$to[$i];
            for ($a = $fr; $a <= $t; $a++) {
//                echo $a . '<br>';
                echo '<tr>';
                echo '<td>' . $zone[$i] . '</td>';
                echo '<td>' . $row[$i] . '</td>';
                echo '<td>' . $a . '</td>';
                echo '</tr>';
            }
        }
        echo '</table>';
    }
}
