<?php

namespace Modules\Lottery\Http\Controllers;

use App\Models\Lottery;
use App\Models\LotteryCategory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Lottery\Http\Requests\QuestionNewRequest;
use Modules\Lottery\Providers\LotteryServiceProvider;
use Modules\Lottery\Services\ModuleToolsTrait;
use Modules\Yasna\Services\ModuleHelper;
use Morilog\Jalali\Facades\jDate;

class LotteryController extends Controller
{
    use ModuleToolsTrait;
    use FeedbackControllerTrait;



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!$this->isLoggedIn()) {
            return redirect(url('/lottery/login'));
        }

        return view('lottery::index');
    }

    public function get_people()
    {
        $result['stats']       = $this->get_people_count();
        $result['data']        = $this->get_people_data();
        $result['lottery']     = $this->get_lottery_code_data();
        $result['last_update'] = $this->updateTime();

        return json_encode($result);
    }

    public function get_people_data()
    {
        $people = Lottery::where('id', '>', 0)
                         ->where('category_id', '<=', 3)
            ->get();
        $list = array();
        foreach ($people as $person) {
            $me     = [
                 'id'                => $person->id,
                 'full_name'         => $person->first_name . ' ' . $person->last_name,
                 'first_name'        => $person->first_name,
                 'last_name'         => $person->last_name,
                 'invitation_number' => $person->invitation_number,
                 'category'          => $person->category_id,
                 'login_status'      => $person->login_status,
                 'lottery_code'      => $person->lottery_code,
                 'referred_name'     => $person->referred_name,
                'attendees' => $person->attendees
            ];
            $list[] = $me;
        }
        return $list;
    }

    public function get_people_count()
    {
        $stats['all'] = Lottery::where('login_status', 1)
                               ->where('category_id', '<=', 3)
            ->sum('attendees');

        $stats['family'] = Lottery::where('login_status', 1)
                                  ->where('category_id', '<=', 3)
            ->count();

        return $stats;
    }

    public function get_lottery_code_data()
    {
        $result['first'] = Lottery::whereNotNull('lottery_code')->orderBy('lottery_code', 'ASC')->first();
        if ($result['first'] and $result['first']->id) {
            $result['first'] = $result['first']->lottery_code;
        } else {
            $result['first'] = 0;
        }

        $result['last'] = Lottery::whereNotNull('lottery_code')->orderBy('lottery_code', 'DESC')->first();
        if ($result['last'] and $result['last']->id) {
            $result['last'] = $result['last']->lottery_code;
        } else {
            $result['last'] = 0;
        }

        return $result;
    }

    public function set_login_people(Request $request)
    {
        $result = array();

        $update['lottery_code']  = $request->lottery_code;
        $update['first_name']    = $request->first_name;
        $update['last_name']     = $request->last_name;
        $update['id']            = $request->id;
        $update['referred_name'] = $request->referred_name;
        $update['attendees']     = $request->attendees;
        $update['login_at']      = Carbon::now()->toDateTimeString();

        /*if ($request->box)
        {
            $update['box'] = $request->box;
        }
        else
        {
            $update['box'] = 0;
        }*/

        $person = Lottery::find($update['id']);
        if ($person and $person->id) {
            if ($person->category_id == 1 and $person->login_status != 1) {
                $update['login_status'] = 1;

                $duplicate_lottery_code = model("lottery")->grab($request->lottery_code, 'lottery_code');
                if ($duplicate_lottery_code and $duplicate_lottery_code->id) {
                    $result['result'] = -1;
                }
            } elseif ($person->category_id == 1 and $person->login_status == 1) {
                if ($person->lottery_code != $update['lottery_code']) {
                    $duplicate_lottery_code = model('Lottery')->grab($request->lottery_code, 'lottery_code');
                    if ($duplicate_lottery_code and $duplicate_lottery_code->id) {
                        $result['result'] = -1;
                    }
                }
            } else {
                $update['login_status'] = 1;
            }
        } else {
            $result['result'] = 0;
        }

        if (!isset($result['result']) or $result['result'] > 0) {
            if ($person->login_status != $request->status) {
                $result['result'] = 0;
            } else {
                if (Lottery::store($update)) {
                    $result['result'] = 1;
                } else {
                    $result['result'] = 0;
                }
            }
        }
        return json_encode($result);
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function searchInvitationCode(Request $request)
    {
        if (!($invitation_number = $request->invitation_number)) {
            abort(404);
        }

        return response()->json($this->searchInvitationCodeResult($invitation_number)->map(function ($person) {
            return $this->searchInvitationCodeMap($person);
        })->toArray());
    }



    /**
     * @param string $invitation_number
     *
     * @return Collection
     */
    protected function searchInvitationCodeResult($invitation_number)
    {
        $people = Lottery::where('category_id', '<=', 3);

        return $this
             ->searchInvitationCodeCondition(Lottery::where('category_id', '<=', 3), $invitation_number)
             ->get()
             ;
    }



    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    protected function searchInvitationCodeCondition($builder, $invitation_number)
    {
        if (is_numeric($numeric_version = ed($invitation_number)) and $numeric_version > 0) {
            $builder->where('invitation_number', $numeric_version);
        } else {
            $builder->whereRaw("CONCAT(`first_name`, ' ', `last_name`) LIKE '%$invitation_number%'");
        }

        return $builder;
    }



    /**
     * @param Lottery $person
     *
     * @return array
     */
    protected function searchInvitationCodeMap($person)
    {
        return [
             'id'                => $person->id,
             'full_name'         => $person->first_name . ' ' . $person->last_name,
             'first_name'        => $person->first_name,
             'last_name'         => $person->last_name,
             'invitation_number' => $person->invitation_number,
             'category'          => $person->category_id,
             'login_status'      => $person->login_status,
             'lottery_code'      => $person->lottery_code,
             'referred_name'     => $person->referred_name,
             'attendees'         => $person->attendees,
        ];
    }

    public function set_logout_people(Request $request)
    {
        $result = array();
        $id     = $request->id;
        $status = $request->status;

        $lottery = Lottery::find($id);
        if ($lottery and $lottery->id) {
            if ($lottery->login_status == $status) {
                $update = [
                     'login_status'   => 0,
                     'login_at'       => null,
                     'lottery_code'   => null,
                     'lottery_status' => 0,
                     'lottery_win_at' => null,
                     'referred_name'  => null,
                    'id' => $id
                ];

                Lottery::store($update);
                $result['result'] = 1;
            } else {
                $result['result'] = 0;
            }
        } else {
            $result['result'] = 0;
        }

        return json_encode($result);
    }

    public function add_person(Request $request)
    {
        $insert['first_name']        = $request->first_name;
        $insert['last_name']         = $request->last_name;
        $insert['category_id']       = $request->category;
        $insert['invitation_number'] = $request->invitation_number;
        $insert['attendees']         = $request->attendees;

        $validate = Validator::make(
             [
                  'first_name'        => $insert['first_name'],
                  'last_name'         => $insert['last_name'],
                  'category_id'       => $insert['category_id'],
                  'invitation_number' => $insert['invitation_number'],
                  'attendees'         => $insert['attendees'],
             ],
             [
                  'first_name'        => 'required|persian:60',
                  'last_name'         => 'required|persian:60',
                  'category_id'       => 'required|numeric',
                  'invitation_number' => 'required|numeric',
                  'attendees'         => 'required|numeric',
             ]
        );

        if ($validate->fails()) {
            return -1;
        }

        return Lottery::store($insert);
    }

    /**
     * Gets Lottery number.
     * @return Lottery name
     */

    public function lottery()
    {
        if (!$this->isLoggedIn()) {
            return redirect(url('/lottery/login'));
        }

        return view('lottery::lottery-page');
    }

    public function search_lottery_code(Request $request)
    {
        $lottery_code = $request->value;

        $person = model('Lottery')->grab($lottery_code, 'lottery_code');

        if ($person and $person->id) {
            if ($person->lottery_status == 1) {
                return 'کد قرعه کشی تکراری است.';
            } else {
                if (strlen($person->full_name)) {
                    return 'خانواده زنده یاد ' . $person->full_name;
                } else {
                    return 'نام زنده یاد ثبت نگردیده است.';
                }
            }
        } else {
            return 'کد قرعه کشی ثبت نشده.';
        }
    }

    public function save_winner(Request $request)
    {
        $lottery_code = $request->id;
        $person       = model("lottery")->grab($lottery_code, 'lottery_code');

        if ($person and $person->id) {
            $update = [
                 'lottery_status' => 1,
                 'lottery_win_at' => Carbon::now()->toDateTimeString(),
                'id' => $person->id
            ];
            if (Lottery::store($update)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login(Request $request)
    {
        if ($this->isTryingToLogin($request)) {
            if ($this->validCredentials($request)) {
                session(['authLogin' => 'true']);
            }
        }

        if ($this->isLoggedIn()) {
            return redirect(url('/lottery'));
        }

        return view('lottery::login');
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function isTryingToLogin(Request $request)
    {
        return boolval($request->username and $request->password);
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function validCredentials(Request $request)
    {
        return in_array([
             'username' => $request->username,
             'password' => hashid($request->password),
        ], LotteryServiceProvider::getDataFromJson('credentials'));
    }



    /**
     * @return bool
     */
    protected function isLoggedIn()
    {
        return boolval(session('authLogin'));
    }



    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        session()->forget('authLogin');
        return redirect(url('/lottery/login'));
    }


    public function printing()
    {
        // @TODO: printing process
        return "print page";
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questions()
    {
        if (!$this->isLoggedIn()) {
            return redirect(url('/lottery/login'));
        }

        return view('lottery::questions', ['questions' => $this->findQuestionsList()]);
    }



    /**
     * @return mixed
     */
    protected function findQuestionsList()
    {
        return model('question')
             ->orderByDesc('created_at')
             ->get()
             ->map(function ($question) {
                 $question->spreadMeta();
                 $options_info = [];
                 $all_messages = $question->allMessages()->get()->count();
                 foreach ($question->options as $key => $value) {
                     $options_info[$key] = [
                          'count'   => ($count = $question->messagesWithAnswer($key)->count()),
                          'percent' => round(($all_messages ? (($count / $all_messages) * 100) : 0), 2),
                          'text'    => $value,
                     ];
                 }

                 $question->options_info      = $options_info;
                 $question->drawable_messages = $question->drawableMessages()->count();
                 return $question;
             })
             ;
    }



    /**
     * @param QuestionNewRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveQuestions(QuestionNewRequest $request)
    {
        return $this->jsonAjaxSaveFeedback(
             model('question')->batchSave($request->only([
                  'question',
                  'options',
                  'answer',
                  'starts_at',
                  'ends_at',
             ]))->exists,
             [
                  'success_refresh'    => 1,
                  'success_form_reset' => 1,
             ]
        );
    }

    /*
     * special people
     */
    public function special_people()
    {
        if (!$this->isLoggedIn()) {
            return redirect(url('/lottery/login'));
        }

        return view('lottery::special-people');
    }

    public function get_special_people($box = 'all')
    {
        $result['stats']       = $this->get_special_people_count();
        $result['data']        = $this->get_special_people_data($box);
        $result['last_update'] = $this->updateTime();

        return json_encode($result);
    }

    public function get_special_people_data($box = 'all')
    {
        if ($box == 'all') {
            $people = Lottery::where('id', '>', 0)
                             ->where('category_id', '>', 3)
                             ->limit(200)
                ->get();
        } else {
            $box_data = LotteryCategory::find($box);
            if ($box_data and $box_data->id) {
                if ($box_data->category === 0) {
                    $people = Lottery::where('id', '>', 0)
                                     ->where('category_id', $box)
                        ->get();
                } else {
                    $people = Lottery::where('id', '>', 0)
                                     ->where('box', $box)
                        ->get();
                }
            } else {
                $people = null;
            }
        }


        $list = array();
        foreach ($people as $person) {
            $extra  = $person->category()->first();
            $me     = [
                 'id'                => $person->id,
                 'full_name'         => $person->first_name . ' ' . $person->last_name,
                 'first_name'        => $person->first_name,
                 'last_name'         => $person->last_name,
                 'invitation_number' => $person->invitation_number,
                 'category'          => $extra->title,
                 'color'             => $extra->color,
                 'attendees'         => $person->attendees,
                 'box'               => $person->box,
                 'box_name'          => $person->box()->boxName(),
                 'login_status'      => $person->login_status,
            ];
            $list[] = $me;
        }

        return $list;
    }

    public function get_special_people_count()
    {
        $stats['all'] = Lottery::where('login_status', 1)
                               ->where('category_id', '>', 3)
            ->sum('attendees');

        $stats['family'] = Lottery::where('login_status', 1)
                                  ->where('category_id', '>', 3)
            ->count();

        return $stats;
    }

    public function search_special_people(Request $request)
    {
        $result = array();
        $invitation_number = $request->invitation_number;

        if ($request->type) {
            if ($request->type == 'all') {
                $m = '>';
                $c = 0;
            } elseif ($request->type == 'family') {
                $m = '<=';
                $c = 3;
            } elseif ($request->type == 'guests') {
                $m = '>';
                $c = 3;
            } else {
                $m = '>';
                $c = 0;
            }
        } else {
            $m = '>';
            $c = 0;
        }

        if (is_numeric($invitation_number) and $invitation_number > 0) {
            $people = Lottery::where('invitation_number', $invitation_number)
                             ->where('category_id', $m, $c)
                ->get();
        } else {
            $people = Lottery::whereRaw("CONCAT(`first_name`, ' ', `last_name`) LIKE '%$invitation_number%'")
                             ->where('category_id', $m, $c)
                ->get();
        }


        if ($people and count($people)) {
            foreach ($people as $person) {
                $extra    = $person->category()->first();
                $me       = [
                     'id'                => $person->id,
                     'full_name'         => $person->first_name . ' ' . $person->last_name,
                     'first_name'        => $person->first_name,
                     'last_name'         => $person->last_name,
                     'invitation_number' => $person->invitation_number,
                     'referred_name'     => $person->referred_name,
                     'category_id'       => $person->category_id,
                     'category'          => $extra->title,
                     'color'             => $extra->color,
                     'attendees'         => $person->attendees,
                     'box'               => $person->box,
                     'box_name'          => $person->box()->boxName(),
                     'login_status'      => $person->login_status,
                ];
                $result[] = $me;
            }
        } else {
            $result = false;
        }

        return json_encode($result);
    }

    public function get_categories()
    {
        $categories = LotteryCategory::where('category', 0)
                                     ->where('id', '>', 3)
            ->get();
        $result = array();
        if ($categories and $categories->count()) {
            foreach ($categories as $category) {
                $me       = [
                     'title' => $category->title,
                     'id'    => $category->id,
                     'color' => $category->color,
                ];
                $result[] = $me;
            }
        }

        return json_encode($result);
    }

    public function get_box()
    {
        $categories = LotteryCategory::where('category', 1)->get();
        $result = array();
        if ($categories and $categories->count()) {
            foreach ($categories as $category) {
                $me       = [
                     'title' => $category->title,
                     'id'    => $category->id,
                     'color' => $category->color,
                ];
                $result[] = $me;
            }
        }

        return json_encode($result);
    }

    public function change_box(Request $request)
    {
        $id     = $request->id;
        $box_id = $request->box;
        $result = array();

        if ($id and is_numeric($id) and isset($box_id) and is_numeric($box_id)) {
            $update = [
                 "id"  => $id,
                "box" => $box_id
            ];
            if (Lottery::store($update)) {
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
            }
        } else {
            $result['status'] = -1;
        }
        return json_encode($result);
    }

    public function add_special_person(Request $request)
    {
        $insert['first_name']        = $request->first_name;
        $insert['last_name']         = $request->last_name;
        $insert['category_id']       = $request->category;
        $insert['invitation_number'] = $request->invitation_number;
        $insert['attendees']         = $request->attendees;
        $insert['box']               = $request->box;

        $validate = Validator::make(
             [
                  'first_name'        => $insert['first_name'],
                  'last_name'         => $insert['last_name'],
                  'category_id'       => $insert['category_id'],
                  'invitation_number' => $insert['invitation_number'],
                  'attendees'         => $insert['attendees'],
                  'box'               => $insert['box'],
             ],
             [
                  'first_name'        => 'required|persian:60',
                  'last_name'         => 'required|persian:60',
                  'category_id'       => 'required|numeric',
                  'invitation_number' => 'numeric', // @TODO: it is not required
                  'attendees'         => 'required|numeric',
                  'box'               => 'required|numeric', // @TODO: this can have 0 value. default: 0 (hichkodam)
             ]
        );

        if ($validate->fails()) {
            return -1;
        }

        return Lottery::store($insert);
    }

    // date
    public function echoDate($date, $foramt = 'default', $language = 'auto', $pd = false)
    {
        /*-----------------------------------------------
        | Process ...
        */
        if ($foramt == 'default') {
            $foramt = 'j F Y [H:m]';
        }

        if ($language == 'auto') {
            $language = getLocale();
        }

        switch ($language) {
            case 'fa':
                $date = jDate::forge($date)->format($foramt);
                break;

            case 'en':
                $date = Carbon::parse($date)->format($foramt);
                break;

            default:
                $date = Carbon::parse($date)->format($foramt);
        }

        if ($pd) {
            return pd($date);
        } else {
            return $date;
        }
    }

    public function updateTime()
    {
        return $this->echoDate(Carbon::now()->timezone('Asia/Tehran')->toDateTimeString(), '[H:i:s] Y/m/d', 'fa', true);
    }


    // insert data
    public function test()
    {
        $data = '{
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3857" } },
"features": [
{ "type": "Feature", "properties": { "Field1": 101, "Field2": "حسام", "Field3": "عسگری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 102, "Field2": "محمدعلی", "Field3": "احمدی علی آباد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 103, "Field2": "مهدی ", "Field3": "استیری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 104, "Field2": "نوید", "Field3": "زمانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 105, "Field2": "نجمه", "Field3": "عرب زاده قهیازی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 106, "Field2": "فرناز", "Field3": "آهنگران", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 107, "Field2": "صابر", "Field3": "صفری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 108, "Field2": "سیدمهدی", "Field3": "میرمحمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 109, "Field2": "لیلا سادات", "Field3": "پیغمبر زاده.", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 110, "Field2": "کیومرث", "Field3": "سیدافتتاحی ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 111, "Field2": "تارا", "Field3": "مستر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 112, "Field2": "مهرناز", "Field3": "  کلاته آقا محمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 113, "Field2": "امیرحسین", "Field3": "هداوند خانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 114, "Field2": "محمد", "Field3": "شصتی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 115, "Field2": "بهاره ", "Field3": "سالمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 116, "Field2": "محسن", "Field3": "بنده یی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 117, "Field2": "عارفه", "Field3": "باقری ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 118, "Field2": "نوید", "Field3": "علوی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 119, "Field2": "میلاد", "Field3": "کربلایی طوسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 120, "Field2": "پدرام", "Field3": "ایمان علی زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 121, "Field2": "یوسف", "Field3": "حسن زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 122, "Field2": "مهدی ", "Field3": "مردانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 123, "Field2": "شهرام", "Field3": "محب حسینی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 124, "Field2": "داود", "Field3": "درخشنده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 125, "Field2": "زینب", "Field3": "طلوع کلان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 126, "Field2": "فاطمه", "Field3": "عاجرلو", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 127, "Field2": "عباس", "Field3": "حق نیا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 128, "Field2": "حسن", "Field3": "خادمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 129, "Field2": "محمد کسرا", "Field3": "کرم یانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 130, "Field2": "پروین", "Field3": "ابراهیمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 131, "Field2": "سیدمعظم", "Field3": "محمودی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 132, "Field2": "مهدی ", "Field3": "کاظمی فر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 133, "Field2": "فرامرز", "Field3": "یزدانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 134, "Field2": "حسین", "Field3": "فشکی فراهانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 135, "Field2": "حسین", "Field3": "اکبری فرامرز", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 136, "Field2": "اشکان", "Field3": "مهربان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 137, "Field2": "کیوان", "Field3": "چزانی شراهی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 138, "Field2": "مرضیه", "Field3": "مردانه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 139, "Field2": "علی", "Field3": "دوست دارابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 140, "Field2": "هانی", "Field3": "دلشادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 141, "Field2": "شیوا", "Field3": "پور حسین", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 142, "Field2": "ملینا", "Field3": "فتحی نسب", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 143, "Field2": "صبا", "Field3": "کاربر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 144, "Field2": "مینا", "Field3": "پیرزاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 145, "Field2": "نیما", "Field3": "لطفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 146, "Field2": "مجتبی", "Field3": "جانی پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 147, "Field2": "محمد", "Field3": "غلامی نیاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 148, "Field2": "هاله", "Field3": "صابر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 149, "Field2": "محمد", "Field3": "جساس", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 150, "Field2": "حمیده", "Field3": "غلامیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 151, "Field2": "مژگان", "Field3": "کربلایی عبدالمحمد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 152, "Field2": "عرفان ", "Field3": "عطار تبریزی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 153, "Field2": "حسن ", "Field3": "بخان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 154, "Field2": "زهرا", "Field3": "منافی علیشاه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 155, "Field2": "محمدحسین", "Field3": "آبکار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 156, "Field2": "مهدی ", "Field3": "قبادزاده خانقشلاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 157, "Field2": "میلاد", "Field3": "برزآبادی فراهانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 158, "Field2": "زهرا", "Field3": "کعبه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 159, "Field2": "ابوالفضل", "Field3": "فدایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 160, "Field2": "مریم", "Field3": "اسمعیلی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 161, "Field2": "ایمان", "Field3": "کلانتری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 162, "Field2": "حسین", "Field3": "صانعی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 163, "Field2": "اکرم", "Field3": "عزیز زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 164, "Field2": "آزاده", "Field3": "بیرالوند", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 165, "Field2": "امیرحسین", "Field3": "جعفری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 166, "Field2": "هانیه", "Field3": "غریبی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 167, "Field2": "حمیدرضا", "Field3": "قربانعلی فرد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 168, "Field2": "علی ", "Field3": "اکبری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 169, "Field2": "مهدی ", "Field3": "رودکی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 170, "Field2": "اسماعیل", "Field3": "ممانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 171, "Field2": "اسماعیل", "Field3": "مرادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 172, "Field2": "فاطمه ", "Field3": "رشونده آوه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 173, "Field2": "عارف", "Field3": "سیفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 174, "Field2": "یدالله", "Field3": "شهاب", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 175, "Field2": "احمد", "Field3": "طباطبایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 176, "Field2": "امیر حسین", "Field3": "حیدری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 177, "Field2": "علی ", "Field3": "دوست دارابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 178, "Field2": "حجت الله", "Field3": "خاکویی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 179, "Field2": "داوود ", "Field3": "مبارزی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 180, "Field2": "علی ", "Field3": "خاکسار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 181, "Field2": "ابولفضل", "Field3": "قهرمانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 182, "Field2": "فاطمه", "Field3": "مبارکی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 183, "Field2": "محمد حسین", "Field3": "بکان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 184, "Field2": "حسین علی", "Field3": "شیرزاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 185, "Field2": "مصطفی", "Field3": "اسدی کرم", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 186, "Field2": "بابک ", "Field3": "میرزایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 187, "Field2": "مبینا ", "Field3": "امیریان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 188, "Field2": "جعفر ", "Field3": "سه رود نشین", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 189, "Field2": "امیرحسین", "Field3": "عباسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 190, "Field2": "فاطمه ", "Field3": "یوسفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 191, "Field2": "مهدی ", "Field3": "عسگری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 192, "Field2": "دلبر", "Field3": "صحرایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 193, "Field2": "پرهام ", "Field3": "رضائیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 194, "Field2": "علی ", "Field3": "رحیمی قره تپه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 195, "Field2": "فاطمه", "Field3": "شریفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 196, "Field2": "معین", "Field3": "ظهوری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 197, "Field2": "علی ", "Field3": "گلشن روستا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 198, "Field2": "رضا", "Field3": "وخت احمد آباد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 199, "Field2": "مهسا ", "Field3": "طاهر خانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 200, "Field2": "علی قدیر", "Field3": "لطفی بهاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 201, "Field2": "طیبه ", "Field3": "خوش روی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 202, "Field2": "خدیجه ", "Field3": "محمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 203, "Field2": "رضا", "Field3": "همدانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 204, "Field2": "امین ", "Field3": "ولی زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 205, "Field2": "قاسم ", "Field3": "صفوی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 206, "Field2": "فرحناز", "Field3": "حسین زاذه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 207, "Field2": "سینا", "Field3": "بسحاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 208, "Field2": "میلاد", "Field3": "شبستانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 209, "Field2": "مرتضی", "Field3": "رسولی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 210, "Field2": "صفورا ", "Field3": "نوری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 211, "Field2": "میثم", "Field3": "خرد روستا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 212, "Field2": "رضا", "Field3": "فلاح زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 213, "Field2": "المیرا", "Field3": "صفا خواه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 214, "Field2": "محمد ", "Field3": "بلالی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 215, "Field2": "محمدرضا", "Field3": "حسنی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 216, "Field2": "صدف ", "Field3": "فرخنده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 217, "Field2": "علیرضا", "Field3": "سیستانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 218, "Field2": "محسن", "Field3": "موسی وند", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 219, "Field2": "امیر", "Field3": "خواجه وند", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 220, "Field2": "فاطمه", "Field3": "شهبازی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 221, "Field2": "کنعان", "Field3": "برزگر صدیق", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 222, "Field2": "علی", "Field3": "درمنش", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 223, "Field2": "زینت", "Field3": "نوربخش", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 224, "Field2": "منیژه ", "Field3": "میرزایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 225, "Field2": "محمدرضا", "Field3": "عموزاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 226, "Field2": "کاظم", "Field3": "یعقوبی شنلی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 227, "Field2": "حیدر", "Field3": "رمضانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 228, "Field2": "متین", "Field3": "نظری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 229, "Field2": "مهناز", "Field3": "سلمانیا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 230, "Field2": "لیلی", "Field3": "شهبازی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 231, "Field2": "محمدرضا", "Field3": "عباسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 232, "Field2": "مهدی", "Field3": "رازگردانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 233, "Field2": "وحید", "Field3": "عباسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 234, "Field2": "امیرعلی", "Field3": "بختیاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 235, "Field2": "سعید", "Field3": "نجفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 236, "Field2": "تانیا", "Field3": "یوسفیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 237, "Field2": "امیرارسلان", "Field3": "ارنواز", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 238, "Field2": "اشکان", "Field3": "خانلاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 239, "Field2": "محمدابراهیم", "Field3": "ایرانمش", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 240, "Field2": "صدرالله", "Field3": "توکلی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 241, "Field2": "کاظم", "Field3": "ابوترابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 242, "Field2": "سیدعلی", "Field3": "موسوی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 243, "Field2": "آرمین ", "Field3": "دستجردی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 244, "Field2": "رضا", "Field3": "آذهوش قاضی محله", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 245, "Field2": "محمدهادی", "Field3": "نصیری مهر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 246, "Field2": "رضا", "Field3": "غنی آبادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 247, "Field2": "امیرحسین", "Field3": "عنبرستانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 248, "Field2": "مجتبی ", "Field3": "بخشی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 249, "Field2": "مهران", "Field3": "محرابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 250, "Field2": "محمدصادق", "Field3": "شوشتری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 251, "Field2": "حمیدرضا", "Field3": "غفاری نیک", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 252, "Field2": "فرشید", "Field3": "اصلانی کلانتری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 253, "Field2": "مبینا ", "Field3": "رضایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 254, "Field2": "سید حمید", "Field3": "صنایع", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 255, "Field2": "شیوا", "Field3": "پورحسین", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 256, "Field2": "آزاده", "Field3": "رحیمی  ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 257, "Field2": "علی", "Field3": "شب گیر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 258, "Field2": "مهدی", "Field3": "ابراهیم زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 259, "Field2": "مهدی", "Field3": "جاهد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 260, "Field2": "ابوالقاسم", "Field3": "بدری مهنو", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 261, "Field2": "رقیه", "Field3": "حسین  ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 262, "Field2": "علی", "Field3": "خاطری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 263, "Field2": "منا", "Field3": "حیدری ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 264, "Field2": "ندا", "Field3": "زرین", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 265, "Field2": "گلچین", "Field3": "شکری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 266, "Field2": "ایرج", "Field3": "دارور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 267, "Field2": "شاپور", "Field3": "نریمانی گیلانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 268, "Field2": "بتول", "Field3": "داورزنی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 269, "Field2": "علیرضا", "Field3": "اسدی نیا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 270, "Field2": "احمد ", "Field3": "اکبری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 271, "Field2": "نسرین", "Field3": "فتاح مبیبی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 272, "Field2": "حسین  ", "Field3": "پرویزی راد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 273, "Field2": "آرمان ", "Field3": "منصوری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 274, "Field2": "محمد", "Field3": "مقدم امیرشکاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 275, "Field2": "الهام", "Field3": "محمودی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 276, "Field2": "امیرسام ", "Field3": "حسین زاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 277, "Field2": "معصومه", "Field3": "رستمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 278, "Field2": "علی قدیر", "Field3": "لطفی مهیاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 279, "Field2": "رضا ", "Field3": "دلشادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 280, "Field2": "ترابعلی", "Field3": "ولی  ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 281, "Field2": "شهاب الدین ", "Field3": "جندقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 282, "Field2": "معصومه", "Field3": "هاشمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 283, "Field2": "زهرا ", "Field3": "شقاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 284, "Field2": "اسماعیل ", "Field3": "فرجی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 285, "Field2": "محمد ", "Field3": "رشیدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 286, "Field2": "صبا", "Field3": "زارعی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 287, "Field2": "گیتی", "Field3": "ظفری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 288, "Field2": "گل جمال", "Field3": "ایوبی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 289, "Field2": "حسین ", "Field3": "صانعی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 290, "Field2": "باجی ", "Field3": "صدقی سلطان آبادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 291, "Field2": "اکرم ", "Field3": "عزیز زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 292, "Field2": "اکبر", "Field3": "واعظی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 293, "Field2": "امیر علی", "Field3": "بهرامی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 294, "Field2": "محمد", "Field3": "دزبون", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 295, "Field2": "محمد حسین", "Field3": "ریخته گرانی تهرانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 296, "Field2": "علیرضا", "Field3": "مصطفی زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 297, "Field2": "شیرین ", "Field3": "زارع اصل", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 298, "Field2": "ربابه ", "Field3": "نوری سوا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 299, "Field2": "مهری", "Field3": "مردانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 300, "Field2": "زینب ", "Field3": "علیپور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 301, "Field2": "مهدی ", "Field3": "استیری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 302, "Field2": "احمدرضا", "Field3": "احمدی درمنی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 303, "Field2": "نورالله ", "Field3": "پور رئیسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 304, "Field2": "قاسم ", "Field3": "کاوک شو", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 305, "Field2": "حسن ", "Field3": "نشاط شمیرانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 306, "Field2": "ژاله ", "Field3": "احمدیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 307, "Field2": "طاهره", "Field3": "اسدیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 308, "Field2": "رضا ", "Field3": "محمدیان فرید", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 309, "Field2": "فاطمه", "Field3": "اظهری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 310, "Field2": "بهروز", "Field3": "قشلاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 311, "Field2": "سید حسین", "Field3": "حسيني فيروزكلائي", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 312, "Field2": "میثم", "Field3": "بابایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 313, "Field2": "فرهنگ", "Field3": "احمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 314, "Field2": "بتول", "Field3": "کمالی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 315, "Field2": "آرزو", "Field3": "زینل زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 316, "Field2": "نادر", "Field3": "کردی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 317, "Field2": "طیبه", "Field3": "خوش روی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 318, "Field2": "رضا", "Field3": "استیری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 319, "Field2": "نورشرف", "Field3": "جلال اقدم", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 320, "Field2": "ملک السادات", "Field3": "مهدوی رشتی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 321, "Field2": "احمد", "Field3": "معصومی گرگانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 322, "Field2": "معرفت ", "Field3": "شهبازی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 323, "Field2": "محمدمهدی", "Field3": "نظافتی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 324, "Field2": "اسماعیل", "Field3": "لطفی ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 325, "Field2": "فرهاد", "Field3": "علوی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 326, "Field2": "پروین", "Field3": "کربلائی محمد میگونی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 327, "Field2": "محمد", "Field3": "رشیدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 328, "Field2": "فاطمه", "Field3": "دادخواه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 329, "Field2": "فانوس", "Field3": "عطایی سماق", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 330, "Field2": "شهلا ", "Field3": "عبدلی جورابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 331, "Field2": "احمد", "Field3": "ایروانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 332, "Field2": "مجید ", "Field3": "نامداری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 333, "Field2": "فاطمه", "Field3": "ایوبی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 334, "Field2": "علی اکبر", "Field3": "کاشی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 335, "Field2": "محمد", "Field3": "رضایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 336, "Field2": "مصطفی ", "Field3": "اکبری دهنه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 337, "Field2": "مائده ", "Field3": "عربی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 338, "Field2": "حامد", "Field3": "ارژنگی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 339, "Field2": "اکبر ", "Field3": "ایزدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 340, "Field2": "محسن", "Field3": "حسینیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 341, "Field2": "عوضعلی", "Field3": "آقا سیپور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 342, "Field2": "منیره سادات", "Field3": "عظیمی نژاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 343, "Field2": "محمد ", "Field3": "صالحی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 344, "Field2": "فرشید", "Field3": "عابدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 345, "Field2": "محمد علی ", "Field3": "ملکی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 346, "Field2": "امیررضا ", "Field3": "سرافر", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 347, "Field2": "محسن", "Field3": "برزمینی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 348, "Field2": "ابوالفضل ", "Field3": "احمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 349, "Field2": "علیرضا", "Field3": "رضایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 350, "Field2": "یونس", "Field3": "جعفری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 351, "Field2": "مرضیه", "Field3": "تاجمیری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 352, "Field2": "زهرا", "Field3": "محمدپور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 353, "Field2": "زهرا ", "Field3": "خوش خلق", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 354, "Field2": "محمدحسن", "Field3": "گیلکی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 355, "Field2": "منصوره", "Field3": "بلباسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 356, "Field2": "علی", "Field3": "صفایی فرد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 357, "Field2": "مرادحسین", "Field3": "بیات", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 358, "Field2": "محمد", "Field3": "اصغری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 359, "Field2": "محمدعلی ", "Field3": "اکبرلو", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 360, "Field2": "منصوره", "Field3": "دانشور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 361, "Field2": "محمدعلی ", "Field3": "ربیعی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 362, "Field2": "جعفر", "Field3": "رنجبر فردوس", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 363, "Field2": "لیلا", "Field3": "علیه‌پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 364, "Field2": "مهدی ", "Field3": "محمدی نودهی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 365, "Field2": "صابر", "Field3": "پرنیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 366, "Field2": "مقصود", "Field3": "آریان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 367, "Field2": "ابوالفضل", "Field3": "بلوچی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 368, "Field2": "شهناز", "Field3": "مجیدیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 369, "Field2": "معصومه", "Field3": "ترابی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 370, "Field2": "فروغ سادات", "Field3": "میر محمد خانی سمنانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 371, "Field2": "زهرا", "Field3": "ستاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 372, "Field2": "محمد اسمعیل", "Field3": "کشاورز افشار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 373, "Field2": "علی", "Field3": "خدادادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 374, "Field2": "علی رضا", "Field3": "فخاری سالم", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 375, "Field2": "مینا", "Field3": "فراهانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 376, "Field2": "فردوس", "Field3": "بحیرایی مشترکی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 377, "Field2": "محمد ", "Field3": "شاهسوند", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 378, "Field2": "حاجعلی", "Field3": "صبحتی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 379, "Field2": "لیلا", "Field3": "صمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 380, "Field2": "محمد", "Field3": "حمیدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 381, "Field2": "اکرم", "Field3": "ناطقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 382, "Field2": "فاطمه ", "Field3": "زارع داویجانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 383, "Field2": "علی", "Field3": "ساروخانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 384, "Field2": "اشرف سادات", "Field3": "میر حسینی ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 385, "Field2": "رضا", "Field3": "سیاحی ممه کندی ", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 386, "Field2": "محمد علی ", "Field3": "افتاده قزاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 387, "Field2": "حامد", "Field3": "گل بهاری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 388, "Field2": "مینا", "Field3": "پیرزاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 389, "Field2": "یوسف", "Field3": "حسن زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 390, "Field2": "سید سعید ", "Field3": "مصیبی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 391, "Field2": "محمد", "Field3": "اصغری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 392, "Field2": "خوشقدم", "Field3": "تهم", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 393, "Field2": "فاطمه", "Field3": "خماری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 394, "Field2": "فراز", "Field3": "یزدانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 395, "Field2": "آرزو", "Field3": "رستگار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 396, "Field2": "محیا", "Field3": "شاه آبادی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 397, "Field2": "حسین ", "Field3": "مصدق", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 398, "Field2": "مدد", "Field3": "سیابی کلخوران", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 399, "Field2": "فریده ", "Field3": "نادر پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 400, "Field2": "زهرا ", "Field3": "رئیس علی محمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 401, "Field2": "کریم", "Field3": "یحیی پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 402, "Field2": "محمد", "Field3": "علیوند", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 403, "Field2": "الهام", "Field3": "تربتی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 404, "Field2": "آرزو", "Field3": "آذری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 405, "Field2": "مجید ", "Field3": "گودرزی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 406, "Field2": "مریم", "Field3": "رزاقی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 407, "Field2": "پیمان ", "Field3": "احمدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 408, "Field2": "ارشد", "Field3": "شاهی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 409, "Field2": "فاطمه", "Field3": "مقصودلو", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 410, "Field2": "ابراهیم", "Field3": "سعیدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 411, "Field2": "روح اله ", "Field3": "تاجیک", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 412, "Field2": "آیدا", "Field3": "شیبانی کارخانه", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 413, "Field2": "محمد رضا", "Field3": "آقا جان زاده", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 414, "Field2": "سحر", "Field3": "صایمی پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 415, "Field2": "اسماعیل", "Field3": "رشیدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 416, "Field2": "پروانه", "Field3": "چای پز", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 417, "Field2": "نوروز", "Field3": "سلیمان پور", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 418, "Field2": "کمال", "Field3": "امی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 419, "Field2": "سیما", "Field3": "ترابیان", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 420, "Field2": "اقبال", "Field3": "خزایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 421, "Field2": "میلاد", "Field3": "کربلایی طوسی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 422, "Field2": "سعید ", "Field3": "غفوری راد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 423, "Field2": "مجید ", "Field3": "مقدس نژاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 424, "Field2": "هورا", "Field3": "هاشمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 425, "Field2": "فاطمه", "Field3": "فلاحتکار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 426, "Field2": "مهدی ", "Field3": "صادق نژاد", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 427, "Field2": "سید تیمور", "Field3": "جلالی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 428, "Field2": "امید", "Field3": "شاهوار", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 429, "Field2": "رامین", "Field3": "مولایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 430, "Field2": "مجتبی", "Field3": "جعفری", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 431, "Field2": "عباسعلی", "Field3": "حق نیا", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 432, "Field2": "زهرا", "Field3": "صارمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 433, "Field2": "محمد", "Field3": "عابدی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 434, "Field2": "مهسا", "Field3": "خزایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 435, "Field2": "رقیه", "Field3": "صوفی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 436, "Field2": "فاطمه", "Field3": "وفایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 437, "Field2": "مجتبی", "Field3": "عطایی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 438, "Field2": "بهاره", "Field3": "سالمی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 439, "Field2": "مهران", "Field3": "امانی", "Field4": 1 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 801, "Field2": "جناب آقای", "Field3": "احمد", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 802, "Field2": "جناب آقای", "Field3": "مرتضی", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 803, "Field2": "جناب آقای", "Field3": "علی", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 804, "Field2": "جناب آقای", "Field3": "حسین", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 805, "Field2": "جناب آقای", "Field3": "امیر محمد", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 806, "Field2": "جناب آقای", "Field3": "راحل", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 807, "Field2": "جناب آقای", "Field3": "شکیبا", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 808, "Field2": "سرکار خانم", "Field3": "منظر ", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 809, "Field2": "سرکار خانم", "Field3": "شهناز", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 810, "Field2": "جناب آقای", "Field3": "حسینعلی ", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 811, "Field2": "جناب آقای", "Field3": "بهنام", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 812, "Field2": "جناب آقای", "Field3": "داود", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 813, "Field2": "جناب آقای", "Field3": "حسین", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 814, "Field2": "جناب آقای", "Field3": "عباس", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 815, "Field2": "جناب آقای", "Field3": "مجتبی", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 816, "Field2": "جناب آقای", "Field3": "سید مهدی", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 817, "Field2": "جناب آقای", "Field3": "امیر رضا", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 818, "Field2": "جناب آقای", "Field3": "محمد", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 819, "Field2": "سرکار خانم", "Field3": "خدیجه", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 820, "Field2": "سرکار خانم", "Field3": "شبنم", "Field4": 3 }, "geometry": null },
{ "type": "Feature", "properties": { "Field1": 821, "Field2": "جناب آقای", "Field3": "حسین", "Field4": 3 }, "geometry": null }
]
}
';
        $data = json_decode($data, true);

        for ($i = 0; $i < count($data['features']); $i++) {
            $insert = [
                 'first_name'        => $data['features'][$i]['properties']['Field2'],
                 'last_name'         => $data['features'][$i]['properties']['Field3'],
                 'invitation_number' => $data['features'][$i]['properties']['Field1'],
                 'category_id'       => $data['features'][$i]['properties']['Field4'],
            ];

            $id = Lottery::store($insert);

            echo $id . ' inserted <br>';
        }

        //        ss($data['features']);
    }

    public function test2()
    {
        $people = Lottery::limit(150)->get();
        if ($people) {
            foreach ($people as $person) {
                $insert = [
                     'first_name'        => $person->first_name,
                     'last_name'         => $person->last_name,
                     'invitation_number' => $person->invitation_number,
                     'category_id'       => rand(4, 11),
                ];

                $id = Lottery::store($insert);

                echo $id . ' inserted <br>';
            }
        }
    }



    /**
     * Insert data from json file.
     */
    public function insertFamily()
    {
        $data = LotteryServiceProvider::getDataFromJson('family', false);

        for ($i = 0; $i < count($data); $i++) {
            $duplicate = model('lottery')->grab($data[$i]->invitation_number, 'invitation_number');

            if ($duplicate->not_exists) {
                $insert = [
                     'first_name'        => pd($data[$i]->first_name),
                     'last_name'         => pd($data[$i]->last_name),
                     'invitation_number' => $data[$i]->invitation_number,
                     'category_id'       => $data[$i]->category_id,
                     'attendees'         => $data[$i]->attendees,
                ];

                $id = Lottery::store($insert);

                echo $id . ' inserted - invation: ' .  $data[$i]->invitation_number . ' <br>';
            }
        }
    }



    /**
     * Insert data from json file.
     */
    public function insertSpecial()
    {
        $data = LotteryServiceProvider::getDataFromJson('special', false);

        for ($i = 0; $i < count($data); $i++) {
            $insert = [
                 'first_name'    => pd($data[$i]->FIELD1),
                 'last_name'     => pd($data[$i]->FIELD2),
                 'category_id'   => $data[$i]->FIELD4,
                 'attendees'     => $data[$i]->FIELD3,
                 'referred_name' => $data[$i]->FIELD5,
            ];

            $id = Lottery::store($insert);

            echo $id . ' inserted <br>';
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function draw(Request $request)
    {
        if (
             !$this->isLoggedIn() or
             !($question = model('question')->grabHashid($request->question))->exists or
             !$question->writeMessages()->count()
        ) {
            return response('Bad request!', 403);
        }

        if (
             ($winner = $question->drawableMessages()->inRandomOrder()->first()) and
             $winner->source
        ) {
            $winners   = ($question->spreadMeta()->winners ?? []);
            $winners[] = $winner->source;
            $question->refresh();
            $question->batchSave(['winners' => $winners]);
            return $winner->source;
        }
        return response('Failed :(', 410);
    }
}
