<?php

namespace Modules\Lottery\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class QuestionNewRequest extends YasnaRequest
{
    protected $minimum_options_count = 2;



    /**
     * @return array
     */
    public function rules()
    {
        return [
             'question'  => 'required',
             'options'   => 'required|array',
             'answer'    => 'required|in:' . implode(',', array_keys($this->data['options'])),
             'starts_at' => 'required',
             'ends_at'   => 'required|after:' . $this->data['starts_at'],
        ];
    }



    /**
     * @return array
     */
    public function attributes()
    {
        return [
             'question' => 'سوال',
             'answer'   => 'گزینه‌ی صحیح',
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'options.required' => 'وارد کردن حداقل ' . ad($this->minimum_options_count) . ' گزینه الزامی است.',
             'answer.in'        => 'گزینه‌ی صحیح باید از بین گزینه‌های موجود انتخاب شود.',
        ];
    }



    /**
     * Correct Data
     */
    public function corrections()
    {
        if (array_key_exists('options', $this->data)) {
            if (is_array($this->data['options'])) {
                $this->data['options'] = array_filter($this->data['options'], 'boolval');
            } else {
                $this->data['options'] = [];
            }

            if ((count($this->data['options']) < $this->minimum_options_count)) {
                $this->data['options'] = [];
            }
        }

        if (array_key_exists('starts_at', $this->data) and $this->data['starts_at']) {
            $this->data['starts_at'] = Carbon::createFromTimestamp($this->data['starts_at'] / 1000);
        }

        if (array_key_exists('ends_at', $this->data) and $this->data['ends_at']) {
            $this->data['ends_at'] = Carbon::createFromTimestamp($this->data['ends_at'] / 1000);
        }
    }
}
