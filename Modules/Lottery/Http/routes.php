<?php

Route::get('/', function () {
    return redirect(url('/lottery'));
});

Route::group(['middleware' => 'web', 'prefix' => 'lottery', 'namespace' => 'Modules\Lottery\Http\Controllers'],
     function () {
         Route::get('/', 'LotteryController@index');

         Route::any('/login', 'LotteryController@login');

         Route::get('/logout', 'LotteryController@logout');

         Route::get('/print', 'LotteryController@printing');

         Route::get('/special', 'LotteryController@special_people');

         Route::get('/process', 'LotteryController@lottery');

         Route::group(['prefix' => 'questions'], function () {
             Route::get('/', 'LotteryController@questions');
             Route::post('/', 'LotteryController@saveQuestions');
             Route::post('draw', 'LotteryController@draw')->name('lottery.draw');
         });

         Route::get('/test', 'LotteryController@insertFamily');
         Route::get('/test2', 'LotteryController@insertSpecial');
         Route::get('/test3', 'TestController@index');
     });

Route::group(['prefix' => 'lottery', 'namespace' => 'Modules\Lottery\Http\Controllers'], function () {
    // List search
    Route::get('/get_people', 'LotteryController@get_people');
    Route::post('/set_login', 'LotteryController@set_login_people');
    Route::post('/search_invitation_code', 'LotteryController@searchInvitationCode');
    Route::post('/set_logout', 'LotteryController@set_logout_people');

    // Special List
    Route::get('/get_special/{box?}', 'LotteryController@get_special_people');
    Route::post('/search_special_people', 'LotteryController@search_special_people');
    Route::post('/change_box', 'LotteryController@change_box');

    // lists
    Route::get('/get_category', 'LotteryController@get_categories');
    Route::get('/get_box', 'LotteryController@get_box');


    // add person
    Route::post('/add_person', 'LotteryController@add_person');
    Route::post('/add_special', 'LotteryController@add_special_person');

    // Lottery process
    Route::post('/search_lottery', 'LotteryController@search_lottery_code');
    Route::post('/save_winner', 'LotteryController@save_winner');
});


Route::group(['prefix' => 'asanak', 'namespace' => 'Modules\Lottery\Http\Controllers'], function () {
    Route::get('listen', 'AsanakController@listen');
});
