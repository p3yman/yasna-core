<?php

namespace Modules\Lottery\Providers;

use Modules\Lottery\Services\ModuleToolsTrait;
use Modules\Yasna\Services\ModuleHelper;
use Modules\Yasna\Services\YasnaProvider;

class LotteryServiceProvider extends YasnaProvider
{

    /**
     * @param $file_name
     *
     * @return array
     */
    public static function getDataFromJson($file_name, $assoc = true)
    {
        return json_decode(file_get_contents(
                  implode(DIRECTORY_SEPARATOR, [
                       static::staticModule()->getPath(),
                       'Resources',
                       'data',
                       $file_name,
                  ])
                  . '.json'
             ), $assoc);
    }



    /**
     * @return ModuleHelper
     */
    public static function staticModule()
    {
        return (new static(app()))->module();
    }
}
