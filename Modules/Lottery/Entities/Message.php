<?php namespace Modules\Lottery\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];
}
