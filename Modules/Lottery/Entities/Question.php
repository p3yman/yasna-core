<?php namespace Modules\Lottery\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];

    protected $casts = [
         'winners' => 'array',
    ];



    /**
     * @return array
     */
    public function optionsMetaFields()
    {
        return ['options', 'answer'];
    }



    /**
     * @return array
     */
    public function winnersMetaFields()
    {
        return ['winners'];
    }



    /**
     * @return Builder
     */
    public function allMessages()
    {
        return model('message')
             ->where('received_at', '>=', $this->starts_at)
             ->where('received_at', '<=', $this->ends_at)
             ->groupBy(DB::raw('`source` DESC'))
             ;
    }



    /**
     * @return Builder
     */
    public function messagesWithAnswer($answer)
    {
        return model('message')
             ->whereIn('id', $this->allMessages()->pluck('id'))
             ->whereText($answer)
             ;
    }



    /**
     * @return Builder
     */
    public function writeMessages()
    {
        return $this->messagesWithAnswer($this->getMeta('answer'));
    }



    /**
     * @return Builder
     */
    public function drawableMessages()
    {
        return $this->writeMessages()
             ->whereNotIn('source', $this->spreadMeta()->winners ?? []);
    }
}
