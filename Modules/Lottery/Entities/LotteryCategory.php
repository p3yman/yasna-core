<?php

namespace Modules\Lottery\Entities;

use Modules\Yasna\Services\YasnaModel;

class LotteryCategory extends YasnaModel
{
    protected $fillable = [
        'title',
        'color',
        'category',
    ];
}
