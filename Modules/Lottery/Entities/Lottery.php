<?php

namespace Modules\Lottery\Entities;

use Modules\Yasna\Services\YasnaModel;

class Lottery extends YasnaModel
{
    protected $fillable = [
        'first_name',
        'last_name',
        'login_status',
        'login_at',
        'category_id',
        'invitation_number',
        'lottery_code',
        'lottery_status',
        'lottery_win_at',
        'referred_name',
        'attendees'
    ];

    protected $box_data = null;

    public function category()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'LotteryCategory');
    }

    public function box()
    {
        $this->box_data = \App\Models\LotteryCategory::find($this->box);

        return $this;
    }

    public function boxName()
    {
        if ($this->box_data) {
            return $this->box_data->title;
        } else {
            return 'هیچکدام';
        }
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
