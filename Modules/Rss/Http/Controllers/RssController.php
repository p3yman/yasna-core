<?php

namespace Modules\Rss\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Rss\Http\utility\RssGenerator;

class RssController extends Controller
{
    /**
     * @return Response
     * @throws \Throwable
     */
    public function index()
    {
        $builder = model('post')->limit(1000);
        $feed    = new RssGenerator($builder);
        $feed->setItemAuthorClosure(function ($post) {
            return $post->getCreatorAttribute()->full_name;
        });
        $feed->setRssLang('fa');
        return $feed->setRssTitle('test first')
                    ->setRssLink('www.example.com')
                    ->setRssLang('fa')
                    ->setRssCategories('sport')
                    ->setRssAuthor('parsa')
                    //->setItemAuthorField('title')
                    ->setItemCategoriesField('title')
                    ->setItemContentField('text')
                    ->setItemDateField('created_at')
                    ->setItemLinkField('title')
                    ->setItemSummaryField('text')
                    ->setItemTitleField('title')
                    ->giveMeRss()
             ;
    }
}
