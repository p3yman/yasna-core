<?php

Route::group(['middleware' => 'web', 'prefix' => 'rss', 'namespace' => 'Modules\Rss\Http\Controllers'], function () {
    Route::get('/test', 'RssController@index');
});
