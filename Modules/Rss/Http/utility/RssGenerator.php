<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 6/27/18
 * Time: 10:33 AM
 */

namespace Modules\Rss\Http\utility;

use Carbon\Carbon;
use Mockery\Matcher\Closure;
use Modules\Rss\FeedClasses\FeedMaker\Feed;
use Modules\Rss\FeedClasses\FeedMaker\FeedItem;

class RssGenerator
{
    private $feed_obj;
    private $builder;
    private $rss_title      = '';
    private $rss_id;
    private $rss_author     = [];
    private $rss_categories = [];
    private $rss_lang       = "en";
    private $rss_link;

    private $item_title;
    private $item_title_field;
    private $item_author     = [];
    private $item_author_field;
    private $item_categories = [];
    private $item_categories_field;
    private $item_lang;
    private $item_date;
    private $item_date_field;
    private $item_summary;
    private $item_summary_field;
    private $item_content;
    private $item_content_field;
    private $item_link;
    private $item_link_field;
    private $item_author_closure;
    private $item_category_closure;



    /**
     * RssGenerator constructor.
     *
     * @param $builder
     */
    public function __construct($builder)
    {
        $this->feed_obj  = new Feed();
        $this->builder   = $builder;
        $this->rss_title = url();
    }



    /**
     * @param $rss_title
     *
     * @return $this
     */
    public function setRssTitle($rss_title)
    {
        $this->rss_title = $rss_title;
        return $this;
    }



    /**
     * @param $rss_link
     *
     * @return $this
     */
    public function setRssLink($rss_link)
    {
        $this->rss_link = $rss_link;
        $this->rss_id   = $rss_link;
        return $this;
    }



    /**
     * @param array $rss_author
     *
     * @return $this
     */
    public function setRssAuthor($rss_author)
    {
        $this->rss_author = (array)$rss_author;
        return $this;
    }



    /**
     * @param array $rss_categories
     *
     * @return $this
     */
    public function setRssCategories($rss_categories)
    {
        $this->rss_categories = (array)$rss_categories;
        return $this;
    }



    /**
     * @param $rss_lang
     *
     * @return $this
     */
    public function setRssLang($rss_lang)
    {
        $this->rss_lang  = $rss_lang;
        $this->item_lang = $rss_lang;
        return $this;
    }



    /**
     * @param      $item_title_field
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemTitleField($item_title_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_title = $item_title_field;
            return $this;
        }
        $this->item_title_field = $item_title_field;
        return $this;
    }



    /**
     * @param      $item_author_field (if hard code is true you can pass array to this)
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemAuthorField($item_author_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_author = (array)$item_author_field;
            return $this;
        }
        $this->item_author_field = $item_author_field;
        return $this;
    }



    /**
     * @param $author_closure
     *
     * @return $this
     */
    public function setItemAuthorClosure($author_closure)
    {
        $this->item_author_closure = $author_closure;
        return $this;
    }



    /**
     * @param      $item_categories_field (if hard code is true you can pass array to this)
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemCategoriesField($item_categories_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_categories = (array)$item_categories_field;
            return $this;
        }
        $this->item_categories_field = $item_categories_field;
        return $this;
    }



    /**
     * @param $item_category_closure
     *
     * @return $this
     */
    public function setItemCategoryClosure($item_category_closure)
    {
        $this->item_category_closure = $item_category_closure;
        return $this;
    }



    /**
     * @param      $item_date_field
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemDateField($item_date_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_date = $item_date_field;
            return $this;
        }

        $this->item_date_field = $item_date_field;
        return $this;
    }



    /**
     * @param      $item_summary_field
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemSummaryField($item_summary_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_summary = $item_summary_field;
            return $this;
        }
        $this->item_summary_field = $item_summary_field;
        return $this;
    }



    /**
     * @param      $item_content_field
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemContentField($item_content_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_content = $item_content_field;
            return $this;
        }
        $this->item_content_field = $item_content_field;
        return $this;
    }



    /**
     * @param      $item_link_field
     * @param bool $hard_code
     *
     * @return $this
     */
    public function setItemLinkField($item_link_field, $hard_code = false)
    {
        if ($hard_code) {
            $this->item_link = $item_link_field;
            return $this;
        }

        $this->item_link_field = $item_link_field;
        return $this;
    }



    /**
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function giveMeRss()
    {
        $data  = $this->builder->get();
        $items = $this->makeFeedItems($data);
        return $this->feed_obj->setId($this->rss_id)
                              ->setLastUpdate(Carbon::now())
                              ->setTitle($this->rss_title)
                              ->setLang($this->rss_lang)
                              ->setLink($this->rss_link)
                              ->setCategory($this->rss_categories)
                              ->setAuthor($this->rss_author)
                              ->setEntries($items)
                              ->serve()
             ;
    }



    /**
     * @param $data
     *
     * @return array
     */
    private function makeFeedItems($data)
    {
        $items = [];
        foreach ($data as $datum) {
            $item = new FeedItem();
            $this->callClosures($item, $datum);
            $datum = $datum->toArray();
            $this->checkAndSetAuthor($item, $datum);
            $this->checkAndSetCategory($item, $datum);
            $item->setTitle(empty($this->item_title_field) ? $this->item_title : $datum[$this->item_title_field])
                 ->setLink(empty($this->item_link_field) ? $this->item_link : $datum[$this->item_link_field])
                 ->setSummary(empty($this->item_summary_field) ? $this->item_summary : $datum[$this->item_summary_field])
                 ->setContent(empty($this->item_content_field) ? $this->item_content : $datum[$this->item_content_field])
                 ->setLang($this->item_lang)
                 ->setId(empty($this->item_link_field) ? $this->item_link : $datum[$this->item_link_field])
                 ->setLastUpdate(empty($this->item_date_field) ? $this->item_date : Carbon::parse($datum[$this->item_date_field]))
            ;
            $items[] = $item;
        }
        return $items;
    }



    /**
     * @param $item
     * @param $data
     */
    private function callClosures($item, $data)
    {
        if (!empty($this->item_author_closure)) {
            $this->callAuthorClosure($item, $data);
        }

        if (!empty($this->item_category_closure)) {
            $this->callCategoryClosure($item, $data);
        }
    }



    /**
     * @param $item
     * @param $data
     */
    private function callAuthorClosure($item, $data)
    {
        $author = ($this->item_author_closure)($data);
        if (isset($author)) {
            $convert_to_array = (array)$author;
            $item->setAuthor($convert_to_array);
        }
    }



    /**
     * @param $item
     * @param $data
     */
    private function callCategoryClosure($item, $data)
    {
        $cats = ($this->item_category_closure)($data);
        if (isset($cats)) {
            $convert_to_array = (array)$cats;
            $item->setCategory($convert_to_array);
        }
    }



    /**
     * set category if you did'nt  pass closure
     *
     * @param $item
     * @param $data
     */
    private function checkAndSetAuthor($item, $data)
    {
        if (empty($this->item_author_closure)) {
            $item->setAuthor((empty($this->item_author_field)) ? $this->item_author : (array)$data[$this->item_author_field]);
        }
    }



    /**
     * set category if you did'nt  pass closure
     *
     * @param $item
     * @param $data
     */
    private function checkAndSetCategory($item, $data)
    {
        if (empty($this->item_category_closure)) {
            $item->setCategory(empty($this->item_categories_field) ? $this->item_categories : (array)$data[$this->item_categories_field]);
        }
    }
}
