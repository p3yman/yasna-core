<?php

/**
 * What is Atom?
 * Atom is the name of an XML-based Web content and metadata syndication format,
 * and an application-level protocol for publishing and editing Web resources belonging to periodically updated
 * websites.
 */

namespace Modules\Rss\FeedClasses\FeedMaker;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Collection;

interface FeedInterface
{
    /**
     * Identifies the feed using a universally unique and permanent URI. If you have a long-term, renewable lease on
     * your Internet domain name, then you can feel free to use your website's address.
     * <id>http://example.com/</id>
     *
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id);



    /**
     * Contains a human readable title for the feed. Often the same as the title of the associated website. This value
     * should not be blank.
     * <title>Example, Inc.</title>
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title);



    /**
     * Identifies a related Web page. The type of relation is defined by the rel attribute. A feed is limited to one
     * alternate per type and hreflang. A feed should contain a link back to the feed itself
     *
     * @param string $link
     *
     * @return $this
     */
    public function setLink(string $link);



    /**
     * Indicates the last time the feed was modified in a significant way.
     * <updated>2003-12-13T18:30:02Z</updated>
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function setLastUpdate(Carbon $date);



    /**
     * Names one author of the feed. A feed may have multiple author elements. A feed must contain at least one author
     * element unless #all of the entry elements contain at least one author element
     *
     * @param array $author
     *
     * @return $this
     */
    public function setAuthor(array $author);



    /**
     * Specifies a category that the feed belongs to. A feed may have multiple category elements.
     *
     * @param array $cat
     *
     * @return $this
     */
    public function setCategory(array $cat);



    /**
     * @param string $lang
     *
     * @return $this
     */
    public function setLang(string $lang);



    /**
     * @param array $feedItems
     *
     * @return $this
     */
    public function setEntries(array $feedItems);
}
