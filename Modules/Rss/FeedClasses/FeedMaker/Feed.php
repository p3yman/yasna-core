<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 6/19/18
 * Time: 3:33 PM
 */

namespace Modules\Rss\FeedClasses\FeedMaker;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\View\View;

class Feed implements FeedInterface
{
    private $meta = [];



    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id)
    {
        $this->meta['id'] = $id;
        return $this;
    }



    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->meta['title'] = $title;
        return $this;
    }



    /**
     * @param string $link
     *
     * @return $this
     */
    public function setLink(string $link)
    {
        $this->meta['link'] = $link;
        return $this;
    }



    /**
     * @param Carbon $date
     *
     * @return $this
     */
    public function setLastUpdate(Carbon $date)
    {
        $this->meta['updated'] = $date;
        return $this;
    }



    /**
     * @param array $author
     *
     * @return $this
     */
    public function setAuthor(array $author)
    {
        $this->meta['author'] = $author;
        return $this;
    }



    /**
     * @param array $cat
     *
     * @return $this
     */
    public function setCategory(array $cat)
    {
        $this->meta['category'] = $cat;
        return $this;
    }



    /**
     * @param string $lang
     *
     * @return $this
     */
    public function setLang(string $lang)
    {
        $this->meta['lang'] = $lang;
        return $this;
    }



    /**
     * @param array $feedItems
     *
     * @return $this
     */
    public function setEntries(array $feedItems)
    {
        $this->meta['items'] = $feedItems;
        return $this;
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return $this->meta;
    }






    /**
     * @return Response
     * @throws \Throwable
     *
     */
    public function serve()
    {
        $contents = \view('rss::feed.index', $this->meta);
        return response()->make($contents, '200')->header('Content-Type', 'text/xml');
    }
}
