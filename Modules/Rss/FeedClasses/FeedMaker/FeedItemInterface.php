<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 6/19/18
 * Time: 3:14 PM
 */

namespace Modules\Rss\FeedClasses\FeedMaker;

use Carbon\Carbon;

interface FeedItemInterface
{
    /**
     * it can be same uri of post
     *
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id);



    /**
     * Contains a human readable title for the entry. This value should not be blank.
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title);



    /**
     * Identifies a related Web page. The type of relation is defined by the rel attribute. An entry is limited to one
     * alternate per type and hreflang. An entry must contain an alternate link if there is no content element. More
     * info here.
     * <link rel="alternate" href="/blog/1234"/>
     *
     * @param string $link
     *
     * @return $this
     */
    public function setLink(string $link);



    /**
     * Indicates the last time the entry was modified in a significant way. This value need not change after a typo is
     * fixed, only after a substantial modification. Generally, different entries in a feed will have different updated
     * timestamps.
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function setLastUpdate(Carbon $date);



    /**
     * Names one author of the entry. An entry may have multiple authors. An entry must contain at least one author
     * element unless there is an author element in the enclosing feed, or there is an author element in the enclosed
     * source element.
     * <author>
     * <name>John Doe</name>
     * </author>
     *
     * @param array $author
     *
     * @return $this
     */
    public function setAuthor(array $author);



    /**
     * Specifies a category that the entry belongs to. A entry may have multiple category elements.
     * <category term="technology"/>
     *
     * @param array $cat
     *
     * @return $this
     */
    public function setCategory(array $cat);



    /**
     * @param string $lang
     *
     * @return $this
     */
    public function setLang(string $lang);



    /**
     * @param string $summary
     *
     * @return $this
     */
    public function setSummary(string $summary);



    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content);
}
