<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 6/24/18
 * Time: 3:20 PM
 */

namespace Modules\Rss\FeedClasses\FeedMaker;

use Carbon\Carbon;

class FeedItem implements FeedItemInterface
{
    private $tag = [];



    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->tag[$name];
    }



    /**
     * @return array
     */
    public function getTag()
    {
        return $this->tag;
    }



    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id)
    {
        $this->tag['id'] = $id;
        return $this;
    }



    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->tag['title'] = $title;
        return $this;
    }



    /**
     * @param string $link
     *
     * @return $this
     */
    public function setLink(string $link)
    {
        $this->tag['link'] = $link;
        return $this;
    }



    /**
     * @param Carbon $date
     *
     * @return $this
     */
    public function setLastUpdate(Carbon $date)
    {
        $this->tag['date'] = $date;
        return $this;
    }



    /**
     * @param array $author
     *
     * @return $this
     */
    public function setAuthor(array $author)
    {
        $this->tag['author'] = $author;
        return $this;
    }



    /**
     * @param array $cat
     *
     * @return $this
     */
    public function setCategory(array $cat)
    {
        $this->tag['category'] = $cat;
        return $this;
    }



    /**
     * @param string $lang
     *
     * @return $this
     */
    public function setLang(string $lang)
    {
        $this->tag['lang'] = $lang;
        return $this;
    }



    /**
     * @param string $summary
     *
     * @return $this
     */
    public function setSummary(string $summary)
    {
        $this->tag['summary'] = str_limit($summary, 50);
        return $this;
    }



    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->tag['content'] = $this->escapeContent($content);
        return $this;
    }



    /**
     * @param $str
     *
     * @return mixed
     */
    private function escapeContent($str)
    {
        $str = str_replace("\x0E", "", $str);
        return $str;
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return $this->tag;
    }
}
