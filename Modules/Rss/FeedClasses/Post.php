<?php

namespace Modules\Rss\FeedClasses;

use Spatie\Feed\Feedable;

class Post extends \App\Models\Post implements Feedable
{
    /**
     * @return array|\Spatie\Feed\FeedItem
     */
    public function toFeedItem()
    {
    }
}
