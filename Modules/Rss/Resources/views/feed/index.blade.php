<?=
/* Using an echo tag here so the `<? ... ?>` won't get parsed as short tags */
'<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL
?>

<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="{{$lang}}">
	<title>{{$title}}</title>
	<link href="{{$link}}"/>
	<updated>{{date('c', $updated->getTimestamp())}}</updated>
	@if(!empty($author))
		<author>
			@foreach($author as $name)
				<name> <![CDATA[{{ $name }}]]></name>
			@endforeach
		</author>
	@endif
	<id>{{$link}}</id>
	@foreach($category as $item)
		<category term="{{$item}}"/>
	@endforeach


	@foreach($items as $item)
		<entry>
			<title><![CDATA[{{ $item->title }}]]></title>

			<link rel="alternate" href="{{ $item->link }}"/>
			<id>{{ $item->id }}</id>
			@if(!empty($author))
				<author>
					@foreach($item->author as $name)
						<name> <![CDATA[{{ $name }}]]></name>
					@endforeach
				</author>
			@endif
			@foreach($item->category as $cat)
				<category term="{{$cat}}"/>
			@endforeach
			<summary type="xhtml" xml:lang="{{$lang}}">
				<![CDATA[{!! $item->summary !!}]]>
			</summary>
			<content type="xhtml" xml:lang="{{$lang}}"><![CDATA[{!! $item->content !!}]]></content>

			<updated>{{date('c', $item->date->getTimestamp()) }}</updated>
		</entry>
	@endforeach
</feed>
