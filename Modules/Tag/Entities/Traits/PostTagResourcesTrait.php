<?php

namespace Modules\Tag\Entities\Traits;

use App\Models\Tag;

trait PostTagResourcesTrait
{
    /**
     * get Tags resource.
     *
     * @return array
     */
    protected function getTagsResource()
    {
        if ($this->hasnot('tag')) {
            return null;
        }

        return $this->tags->map(function (Tag $tag) {
            return [
                 'id'   => $tag->hashid,
                 'slug' => $tag->slug,
            ];
        })->toArray()
             ;
    }

}
