<?php

namespace Modules\Tag\Entities\Traits;

trait PosttypeTagTrait
{
    /**
     * @return mixed
     */
    public function tags()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Tag");
    }



    /**
     * @param $locale
     *
     * @return mixed
     */
    public function tagsIn($locale)
    {
        return $this->tags()->where('locale', $locale)->get();
    }
}
