<?php

namespace Modules\Tag\Entities\Traits;

trait PostTagTrait
{
    /**
     * @return mixed
     */
    public function tags()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Tag', 'tag_post')->withTimestamps();
    }



    /**
     * @return string
     */
    public function getMasterTag()
    {
        $data = self::find($this->id);
        return $data->master_tag;
    }



    /**
     * @return int
     */
    public function getTagCount()
    {
        return $this->tags()->count();
    }



    /**
     * if tagName be null method return that post has any tags or no
     *
     * @param null $tagName
     *
     * @return bool
     */
    public function hasTag($tagName = null)
    {
        if (is_null($tagName)) {
            if ($this->tags()->count() == 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $tag      = model('tag')::where('slug', $tagName)->first();
            $tagModel = $this->tags()->where('tag_id', $tag->id);
            if ($tagModel->exists()) {
                return $tagModel->get();
            } else {
                return false;
            }
        }
    }



    /**
     * @return bool
     */
    public function hasAnyTag()
    {
        return $this->hasTag();
    }



    /**
     * pass tag's id to electorTags
     *
     * @param int $tag_id
     */
    public function electorTag($tag_id)
    {
        $this->electorTags($tag_id);
    }



    /**
     * if @tag_ids isn't empty , pass tag's ids to electorTagsForNormalRequests
     *
     * @param int|array|string $tag_ids
     */
    public function electorTags($tag_ids)
    {
        if ($this->electorTagsForNonAssigned($tag_ids)) {
            return;
        }

        $this->electorTagsForNormalRequests($tag_ids);
    }



    /**
     * Elector for when posts without any tags are requested, by specifying a `no`
     *
     * @param int|array|string $tag
     *
     * @return bool
     */
    private function electorTagsForNonAssigned($tag)
    {
        if (is_string($tag) and strtolower($tag) == 'no') {
            $this->elector()->has('tags', '=', 0);
            return true;
        }

        return false;
    }



    /**
     * Elector for when posts, with one/some tag(s) are requested (Normal Usage).
     *
     * @param int|array|string $tag_ids
     */
    private function electorTagsForNormalRequests($tag_ids)
    {
        $tag_ids = (array)$tag_ids;
        if (count($tag_ids)) {
            $this->elector()->whereHas('tags', function ($query) use ($tag_ids) {
                $query->whereIn('tags.id', $tag_ids)->orWhereIn('tags.slug', $tag_ids);
            })
            ;
        }
    }
}
