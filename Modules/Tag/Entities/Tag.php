<?php

namespace Modules\Tag\Entities;

use App\Models\Posttype;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Tag\Events\TagAddedEvent;
use Modules\Yasna\Services\ModelTraits\UniqueUrlTrait;
use Modules\Yasna\Services\YasnaModel;

class Tag extends YasnaModel
{
    use SoftDeletes;
    use UniqueUrlTrait;

    protected $guarded = ['id'];
    protected $table   = 'tags';
    const  TABLE_TAG_POST = 'tag_post';



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Post', 'tag_post')->withTimestamps();
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function posttype()
    {
        return $this->belongsTo(Posttype::class, 'posttype_id');
    }



    /**
     *check that tag is used for a post or no
     *
     * @param $tagId
     *
     * @return bool
     */
    public function isUsed()
    {
        if ($this->posts()->exists()) {
            return true;
        }
        return false;
    }



    /**
     * @param $tagId
     *
     * @return bool
     */
    public function isNotUse()
    {
        return !$this->isUsed();
    }



    /**
     * @param $tagId
     *
     * @return int
     */
    public function postCount()
    {
        if ($this->isNotUse()) {
            return 0;
        }
        return $this->posts->count();
    }



    /**
     * create tag and return its object
     *
     * @param $tagName
     * @param $postTypeId
     * @param $locale
     *
     * @return mixed
     */
    protected function createTag($tagName, $postTypeId, $locale)
    {
        $tag = model('tag')->withTrashed()->grabSlug($tagName);

        if (!$tag->exists) {
            $tag = model('tag')->batchSave([
                 'slug'        => $tagName,
                 'posttype_id' => $postTypeId,
                 'locale'      => $locale,
            ]);
        }
        return $tag;
    }



    /**
     * @param $tags
     * @param $postId
     */
    protected function createTagRelWithPosts($tags, $postId)
    {
        $post = model('post')::find($postId);
        $post->tags()->sync($tags);
    }



    /**
     * @param $tags (must be array)
     * @param $data
     * @param $postTypeId
     *
     * @return bool
     */
    public function saveTags($tags, $data, $postTypeId)
    {
        $postId = $data['id'];
        $locale = $data['locale'];
        $tagIDs = [];
        foreach ($tags as $item) {
            if (empty($item)) {
                continue;
            }
            $tag = $this->createTag($item, $postTypeId, $locale);

            event(new TagAddedEvent($tag));

            $tagIDs[$tag->id] = ['created_by' => user()->id];
        }
        $this->createTagRelWithPosts($tagIDs, $postId);
        return true;
    }



    /**
     * @param $tagName
     * @param $postId
     */
    protected function deleteTagFromPost($tagName, $postId)
    {
        $post = model('post')::find($postId);
        $tag  = model('tag')::where('slug', $tagName)->first();
        $post->tags()->detach($tag->id);
    }
}
