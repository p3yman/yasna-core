<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 11/13/18
 * Time: 5:23 PM
 */

namespace Modules\Tag\services;


use Modules\Tag\Events\TagDeletedEvent;

class RemoveUnusedTag
{
    /**
     * remove unavailable tags
     */
    public static function removeTags()
    {
        foreach (self::findUnavailableTags() as $tag) {
            event(new TagDeletedEvent($tag));
            $tag->hardDelete();
        }
    }



    /**
     * find unused tags
     *
     * @return Collection
     */
    private static function findUnavailableTags()
    {
        return model('tag')::doesntHave('posts')->get();
    }
}