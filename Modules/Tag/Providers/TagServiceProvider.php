<?php

namespace Modules\Tag\Providers;

use Modules\Tag\Console\TagsConverterCommand;
use Modules\Tag\Schedules\RemoveUnUsedTagsSchedule;
use Modules\Yasna\Providers\YasnaServiceProvider;

class TagServiceProvider extends YasnaServiceProvider
{


    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerTraits();
        $this->registerHandlers();
        $this->registerSchedules();
        $this->registerArtisans();
    }



    /**
     * register traits
     *
     * @return void
     */
    public function registerTraits()
    {
        module('yasna')->service('traits')
                       ->add()->trait('Tag:PostTagTrait')->to('Post')
        ;

        module('yasna')->service('traits')
                       ->add()->trait('Tag:PosttypeTagTrait')->to('Posttype')
        ;

        $this->addModelTrait('PostTagResourcesTrait', 'Post');
    }



    /**
     * register handlers
     *
     * @return void
     */
    public function registerHandlers()
    {
        $my_name = 'Tag';
        module('posts')
             ->service('editor_handlers')
             ->add($my_name)
             ->method("$my_name:HandlerController@postEditor")
        ;

        module('posts')
             ->service('after_save')
             ->add($my_name)
             ->method("$my_name:HandlerController@saveTags")
        ;
    }



    /**
     * register schedules
     */
    public function registerSchedules()
    {
        $this->addSchedule(RemoveUnUsedTagsSchedule::class);
    }



    /**
     * register artisans
     */
    public function registerArtisans()
    {
        $this->addArtisan(TagsConverterCommand::class);
    }
}
