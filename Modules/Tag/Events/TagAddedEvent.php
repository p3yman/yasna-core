<?php

namespace Modules\Tag\Events;


use Modules\Yasna\Services\YasnaEvent;

class TagAddedEvent extends YasnaEvent
{
    public $model_name = "tag";
}
