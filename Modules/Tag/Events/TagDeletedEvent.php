<?php

namespace Modules\Tag\Events;

use Modules\Yasna\Services\YasnaEvent;

class TagDeletedEvent extends YasnaEvent
{
    public $model_name = 'tag';
}
