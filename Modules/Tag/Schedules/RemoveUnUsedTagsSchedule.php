<?php namespace Modules\Tag\Schedules;

use Modules\Tag\services\RemoveUnusedTag;
use Modules\Yasna\Services\YasnaSchedule;

class RemoveUnUsedTagsSchedule extends YasnaSchedule
{


    /**
     * remove tags
     */
    protected function job()
    {
        RemoveUnusedTag::removeTags();
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return 'dailyAt:2:40';
    }
}
