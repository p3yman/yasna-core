<?php

Route::group(['middleware' => 'web', 'prefix' => 'manage/tag', 'namespace' => 'Modules\Tag\Http\Controllers'],
     function () {
         Route::get('/', 'TagController@index');
     });
