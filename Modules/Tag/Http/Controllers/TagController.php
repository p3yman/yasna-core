<?php

namespace Modules\Tag\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Posts\Entities\Post;
use Modules\Tag\Entities\Tag;

class TagController extends Controller
{
    public function index()
    {
        $data=model('post')->find(8)->tags()->count();
        dd($data);
        return view('tag::index');
    }

    public function create()
    {
        return view('tag::create');
    }


    public function store(Request $request)
    {
    }

    public function show()
    {
        return view('tag::show');
    }


    public function edit()
    {
        return view('tag::edit');
    }

    public function update(Request $request)
    {
    }


    public function destroy()
    {
    }
}
