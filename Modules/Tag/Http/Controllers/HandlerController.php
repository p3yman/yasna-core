<?php namespace Modules\Tag\Http\Controllers;

use Modules\Tag\Entities\Tag;
use Modules\Yasna\Services\YasnaController;

class HandlerController extends YasnaController
{

    /**
     * add input of tags to post area
     */
    public static function postEditor()
    {
        $my_name = "tags";
        module("posts")
             ->service("editor_main")
             ->add($my_name)
             ->blade("Tag::index")
             ->order(41)
        ;
    }



    /**
     * save tags that user set when create a post
     *
     * @param $model
     * @param $data
     */
    public static function saveTags($model, $data)
    {
        if (!$model->posttype->has('tag')) {
            return;
        }

        $tags       = explode(',', $data['_tag_input']);
        $data['id'] = $model->id;
        $post_type  = $data['type'];
        //set master tag
        if ($model->master_tag != $tags[0]) {
            model('post')::where('id', $model->id)->update(['master_tag' => $tags[0]]);
        }
        //save tags and set relations
        $postType = model('posttype')::where('slug', $post_type)->first();
        $tag      = new Tag();
        $tag->saveTags($tags, $data, $postType->id);
    }
}
