<?php

namespace Modules\Tag\Console;

use App\Models\Label;
use App\Models\Post;
use App\Models\Posttype;
use App\Models\Tag;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\Console\Input\InputArgument;

class TagsConverterCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:tag-convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'convert the all old tags to new module.';

    /**
     * keep the label folder title
     *
     * @var string
     */
    protected $folder_title;

    /**
     * keep the label folder slug
     *
     * @var string
     */
    protected $folder_slug;

    /**
     * keep the label folder id
     *
     * @var int
     */
    protected $folder_id = 0;



    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->purifier();
        $this->discoverFolderId();

        $types = $this->getPosttypes();
        $posts = $this->getEligiblePosts($types);

        $count = $this->loopThroughAllPosts($posts);

        $this->info("Total $count number(s) of tags are converted.");
    }



    /**
     * loop through tags of one post
     *
     * @param Collection $posts
     *
     * @return int
     */
    protected function loopThroughAllPosts($posts)
    {
        $count = 0;

        foreach ($posts as $post) {
            $count += $this->loopThroughTagsOfOnePost($post);
        }

        return $count;
    }



    /**
     * loop through tags of one post
     *
     * @param Post $post
     *
     * @return int
     */
    protected function loopThroughTagsOfOnePost($post)
    {
        $count = 0;

        foreach ($post->tags()->get() as $tag) {
            $count += $this->convertOneTag($tag, $post);
        }

        return $count;
    }



    /**
     * convert one tag
     *
     * @param Tag  $tag
     * @param Post $post
     *
     * @return int
     */
    protected function convertOneTag($tag, $post)
    {
        $label = $this->createLabelRecord($tag);
        $count = 0;

        if ($label->exists) {
            $count += $this->attachLabel($post, $label);
        }

        return $count;
    }



    /**
     * create new label record
     *
     * @param Tag $tag
     *
     * @return Label
     */
    protected function createLabelRecord($tag)
    {
        $label_slug  = $this->folder_slug . "-" . str_slug($tag->slug);
        $label_title = [
             $tag->locale => $tag->slug,
        ];
        $options     = [
             "parent_id" => $this->folder_id,
        ];

        return post()->newLabel($label_slug, $label_title, $options);
    }



    /**
     * attach the newly-generated label to the post
     *
     * @param Post  $post
     * @param Label $label
     *
     * @return boolean
     */
    protected function attachLabel($post, $label)
    {
        $result = $post->attachLabel($label->slug);

        if ($result) {
            $post->batchSave([
                 "converted" => "1",
            ]);
        }

        return $result;
    }



    /**
     * purify input and set default values if not given
     */
    protected function purifier()
    {
        $this->folder_slug  = $this->argument('slug');
        $this->folder_title = $this->argument('title');

        if (!$this->folder_slug) {
            $this->folder_slug = "tags";
        }
        if (!$this->folder_title) {
            $this->folder_title = trans('tag::input.tags-folder');
        }
    }



    /**
     * discover the folder id by either creating a new record or locating an existing one.
     */
    protected function discoverFolderId()
    {
        $label = label($this->folder_slug);
        if ($label->exists) {
            $this->folder_id = $label->id;
            return;
        }

        $options = [
             "custom" => [
                  "feature_active" => true,
                  "feature_color"  => false,
                  "feature_pic"    => false,
                  "feature_nested" => false,
                  "feature_desc"   => true,
                  "feature_text"   => true,
             ],
        ];

        $parent = Posttype()->newLabel($this->folder_slug, $this->folder_title, $options);

        $this->attachLabelFeature();

        if ($parent->exists) {
            $this->folder_id = $parent->id;
            return;
        }

        $this->error("Unexpected error occurred when trying to create the folder.");
        die();
    }



    /**
     * attach the label feature to posttype
     */
    protected function attachLabelFeature()
    {
        $posttypes = posttype()->having('tag')->get();
        
        /**@var Posttype $posttype */
        foreach ($posttypes as $posttype) {

            $posttype->attachFeatures('labels');
            $posttype->attachLabel($this->folder_slug);
            $posttype->cacheFeatures();
            $posttype->attachRequiredInputs();
        }
    }



    /**
     * get array of slug of Posttypes having tag feature
     *
     * @return array
     */
    protected function getPosttypes()
    {
        return posttypes()->having('tag')->pluck('slug')->toArray();
    }



    /**
     *  get array of posts of slugs
     *
     * @param array $slugs
     *
     * @return Collection
     */
    protected function getEligiblePosts(array $slugs)
    {
        return post()->whereIn('type', $slugs)
                     ->where('converted', 0)
                     ->has('tags')
                     ->get()
             ;
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['slug', InputArgument::OPTIONAL, 'The slug of the labels folder'],
             ['title', InputArgument::OPTIONAL, 'The title of the labels folder'],
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
