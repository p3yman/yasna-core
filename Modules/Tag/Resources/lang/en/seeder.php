<?php
return [
     "feature_title"          => "Adding Tag Ability",
     "input_hint"             => "Receives post tags.",
     "input_title"            => 'Tags',
     "tag-unique-url-pattern" => "Pattern of the Unique URL of Tags",
];
