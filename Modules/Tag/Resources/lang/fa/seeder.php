<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 3/4/18
 * Time: 10:57 AM
 */

return [
     "feature_title"          => "امکان تعیین تگ",
     "input_hint"             => "دریافت تگ‌های یک پست",
     "input_title"            => 'تگ‌ها',
     "tag-unique-url-pattern" => "الگوی URL اختصاصی تگ‌ها",
];
