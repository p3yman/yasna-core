@php

	$postTags=$model->tags;
	$postTagString="";
	foreach ($postTags as $item)
		$postTagString.=$item->slug.",";

	 $tags=model('tag')::where('locale',$model->locale)->get(['slug as title'])->toArray();
@endphp


{!!
widget("selectize")
	 ->name('_tag_input')
	 ->inForm()
	 ->value($postTagString)
	 ->options($tags)
	 ->create(true)
	 ->createText(trans('tag::input.add_tag_label'))
	 ->label("tr:tag::input.add_tag_title")
!!}
