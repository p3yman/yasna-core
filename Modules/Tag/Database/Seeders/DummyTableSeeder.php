<?php

namespace Modules\Tag\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->fillTagTable(100);
        $this->relTagWithPosts();
    }



    /**
     * @param int $count
     */
    public function fillTagTable($count = 20)
    {
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            $tagLocale  = rand(0, 3) ? 'fa' : 'en';
            $postType = model('posttype')->inRandomOrder()->first();
            $created_at = Carbon::now()->subDays(rand(5, 20));
            $deleted_at = (!rand(0, 9)) ? $created_at->addDays(rand(1, 30)) : null;
            $user       = \model('user')->inRandomOrder()->first();
            $data[]     = [
                 'slug'        => ($tagLocale == 'fa') ? DummyServiceProvider::persianWord(1) : DummyServiceProvider::englishWord(1),
                 'posttype_id' => $postType->id,
                 'locale'      => $tagLocale,
                 'created_at'  => $created_at,
                 'deleted_at'  => $deleted_at,
                 'deleted_by'  => (!is_null($deleted_at)) ? $user->id : 0,
                 'created_by'  => $user->id,
            ];
        }
        yasna()->seed('tags', $data);
    }



    /**
     * note : this method does not check that post has same locale with tag
     */
    public function relTagWithPosts()
    {
        $postCount = model('post')->all()->count();
        if ($postCount == 0) {
            return;
        }
        $tagCount = model('tag')->all()->count();
        $tags     = model('tag')->limit($tagCount / 2)->get();
        foreach ($tags as $tag) {
            $howManyPosts = rand(1, $postCount);
            $posts        = model('post')->inRandomOrder()->limit($howManyPosts)->get(['id']);
            $tag->posts()->sync($posts);
        }
    }
}
