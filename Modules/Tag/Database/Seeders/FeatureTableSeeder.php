<?php

namespace Modules\Tag\Database\Seeders;

use Illuminate\Database\Seeder;

class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = [
             'order'       => '100',
             'slug'        => 'tag',
             'title'       => trans('tag::seeder.feature_title'),
             'icon'        => 'tag',
             'post_fields' => 'tag_name',
        ];
        
        yasna()->seed('features', $data);
    }
}
