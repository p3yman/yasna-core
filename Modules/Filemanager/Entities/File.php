<?php

namespace Modules\Filemanager\Entities;

use App\Models\Posttype;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile as UploadedFileIlluminate;
use Modules\Filemanager\Entities\Traits\FileResourcesTrait;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Yasna\Services\YasnaModel;

class File extends YasnaModel
{
    use SoftDeletes;
    use FileResourcesTrait;

    protected $fillable = [];

    protected $guarded = ['id'];
    protected $casts   = [
         'published_at'   => 'datetime',
         'meta'           => "array",
         'relative_files' => "relative_files",
    ];

    protected static $statuses_names = [
         1 => 'temp',
         2 => 'used',
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posttype()
    {
        return $this->belongsTo(get_class(posttype()), 'posttype');
    }


    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */

    /**
     * Sets the status of file with status name
     *
     * @param string $statusName
     *
     * @return static
     */
    public function setStatus($statusName)
    {
        $this->status = static::getStatusValue($statusName);
        return $this;
    }



    /**
     * Compares the status of file with value of given status name
     *
     * @param string $statusName
     *
     * @return bool
     */
    public function hasStatus($statusName)
    {
        return ($this->status == self::getStatusValue($statusName)) ? true : false;
    }



    /**
     * @param string                $task
     * @param null|\App\Models\User $user
     *
     * @return boolean
     */
    public function can($task)
    {
        if (auth()->guest()) {
            return false;
        }

        $postType = $this->posttype_eloquent;

        $isCreator = $this->creator->id == user()->id;
        $admin     = user()->as('admin');

        switch ($task) {
            case 'preview':
                if (
                    // Current user owned this file
                     $isCreator or
                     // Current user has permission to edit files in file-manager
                     $admin->can('file-manager.edit') or
                     // Current user has permission to delete files in file-manager
                     $admin->can('file-manager.delete') or
                     (
                         // This file is uploaded for a posttype, folder or category
                          $postType and
                          $postType->exists and
                          (
                              // Current user has permission to edit in this file's posttype
                               $postType->can('edit') or
                               // Current user has permission to publish in this file's posttype
                               $postType->can('publish')
                          )
                     )
                ) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'edit':
                if (
                    // Current user owned this file
                     $isCreator or
                     // Current user has permission to edit files in file-manager
                     $admin->can('file-manager.edit') or
                     (
                         // This file is uploaded for a posttype, folder or category
                          $postType and
                          $postType->exists and
                          (
                              // Current user has permission to edit in this file's posttype
                               $postType->can('edit') or
                               // Current user has permission to publish in this file's posttype
                               $postType->can('publish')
                          )
                     )
                ) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'delete':
                if (
                    // Current user owned this file
                     $isCreator or
                     // Current user has permission to edit files in file-manager
                     $admin->can('file-manager.delete')
                ) {
                    return true;
                } else {
                    return false;
                }
                break;

            case '*':
                return ($this->can('preview') or $this->can('edit') or $this->can('delete'));
                break;
        }

        if ($postType->exists) {
            return false;
        } else {
            return true;
        }
    }



    public function sizeText($precision = 2)
    {
        return formatBytes($this->size, $precision);
    }

    /*
    |--------------------------------------------------------------------------
    | Storing
    |--------------------------------------------------------------------------
    */

    /**
     * Saves an uploaded file in DB
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @param array                         $data
     */
    public static function saveFile($file, $data = [])
    {
        if ($file instanceof UploadedFileIlluminate) {
            $clientNameParts = preg_split('/\.(?=[^\.]*$)/', $file->getClientOriginalName());
            $fileStatus      = UploadServiceProvider::isTemporaryUploadEnabled() ?
                 self::getStatusValue('temp') :
                 self::getStatusValue('used');

            $data = array_default($data, [
                //                'name'          => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
                'name'          => implode('.', array_splice($clientNameParts, 0, 1)),
                'physical_name' => UploadServiceProvider::generateFileName() .
                     '.'
                     . $file->getClientOriginalExtension(),
                'directory'     => 'uploads',
                'mime_type'     => $file->getClientMimeType(),
                'extension'     => $file->getClientOriginalExtension(),
                'type'          => UploadServiceProvider::guessTypeFromMimeType($file->getClientMimeType()),
                'size'          => $file->getSize(),
                'status'        => $fileStatus,
                'posttype'      => null,
            ]);

            $fileTypeRelatedFields = UploadServiceProvider::getFileTypeRelatedFields($file);
            $data                  = array_merge($data, $fileTypeRelatedFields);

            $relatedFiles = UploadServiceProvider::generateRelatedFiles(
                 $file,
                 $data['physical_name'],
                 $data['directory']
            );
            if ($relatedFiles) {
                $data['related_files'] = $relatedFiles;
            }

            return (new static)->batchSave($data)->id;
        }
    }



    /**
     * @return string
     */
    protected function getPathnamePrefix()
    {
        if (app()->runningInConsole()) {
            return FileManagerToolsServiceProvider::getPublicFolder() . DIRECTORY_SEPARATOR;
        } else {
            return '';
        }
    }



    /**
     * @return string
     */
    public function getPartialDirectory()
    {
        return UploadServiceProvider::rectifyDirectoryName($this->getOriginal('directory'));
    }



    /**
     * @return string
     */
    public function getPartialDirectoryAttribute()
    {
        return $this->getPartialDirectory();
    }



    /**
     * @return string
     */
    public function getProperDirectory()
    {
        return $this->getPathnamePrefix() . $this->getPartialDirectory();
    }



    /**
     * @return string
     */
    public function getProperDirectoryAttribute()
    {
        return $this->getProperDirectory();
    }



    /**
     * Return rectified version of "directory" attribute
     *
     * @return string
     */
    public function getDirectory()
    {
        return $this->getPartialDirectory();
    }



    /**
     * Return rectified version of "directory" attribute
     *
     * @return string
     */
    public function getDirectoryAttribute()
    {
        return $this->getDirectory();
    }



    /**
     * @return string
     */
    public function getPartialPathname()
    {
        return implode(DIRECTORY_SEPARATOR, [
             $this->partial_directory,
             $this->physical_name,
        ]);
    }



    /**
     * @return string
     */
    public function getPartialPathnameAttribute()
    {
        return $this->getPartialPathname();
    }



    /**
     * @param string $pathname
     *
     * @return string
     */
    public function getProperPathname()
    {
        $pathname = implode(DIRECTORY_SEPARATOR, [
             $this->proper_directory,
             $this->physical_name,
        ]);
        $pathname = preg_replace("/\/|\\\\/", '/', $pathname);

        return $pathname;
    }



    /**
     * @return string
     */
    public function getProperPathnameAttribute()
    {
        return $this->getProperPathname();
    }



    /**
     * @return string
     */
    public function getPathname()
    {
        return $this->getProperPathname();
    }



    /**
     * Returns Pathname (Concatenation of "directory" and "physical_name")
     *
     * @return string
     */
    public function getPathnameAttribute()
    {
        return $this->getPathname();
    }



    /**
     * Returns names of related files
     *
     * @return array|null
     */
    public function getRelatedFilesAttribute()
    {
        return $this->getMeta('related_files') ?: [];
    }



    /**
     * Returns file name with extension
     *
     * @return string
     */
    public function getFileNameAttribute()
    {
        return $this->name . '.' . $this->extension;
    }



    /**
     * @return bool
     */
    public function getIsInTempFolderAttribute()
    {
        return starts_with($this->directory, implode(DIRECTORY_SEPARATOR, [
             UploadServiceProvider::getRootUploadDir(),
             UploadServiceProvider::getTemporaryFolderName(),
        ]));
    }



    /**
     * @return bool
     */
    public function getIsInTemporaryFolderAttribute()
    {
        return $this->is_in_temp_folder;
    }



    /**
     * @return Posttype|null
     */
    public function getPosttypeEloquentAttribute()
    {
        return $this->posttype()->first();
    }

    /*
    |--------------------------------------------------------------------------
    | Static Data
    |--------------------------------------------------------------------------
    */

    /**
     * Returns numeric value of specified $statusName
     *
     * @param string $statusName
     *
     * @return integer|string
     */
    public static function getStatusValue($statusName)
    {
        return array_search($statusName, static::$statuses_names);
    }



    /*
    |--------------------------------------------------------------------------
    | Meta Fields
    |--------------------------------------------------------------------------
    */


    /**
     * @return array
     */
    public function imageMetaFields()
    {
        return [
             'image_width',
             'image_height',
             'resolution', // for images and videos
        ];
    }



    /**
     * @return array
     */
    public function generalMetaFields()
    {
        return [
             'title',
             'alternative',
             'description',
             'related_files',
        ];
    }



    /**
     * Returns an array of the file's versions. (Including the Original Version)
     *
     * @return array
     */
    public function getAllVersions()
    {
        $related_files = $this->related_files;
        $basic_files   = [
             'original' => $this->physical_name,
        ];

        return array_merge($basic_files, $related_files);
    }
}
