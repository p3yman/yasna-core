<?php

namespace Modules\Filemanager\Entities\Traits;

trait SettingFileManagerTrait
{
    /**
     * get FileResource resource.
     *
     * @return array|null
     */
    protected function getFileResourceResource()
    {
        $file_data_types = ['photo', 'file'];
        if (!in_array($this->data_type, $file_data_types)) {
            return null;
        }

        $file_hashid = $this->gain();
        $file_model  = model('file', $file_hashid);

        return $file_model->exists ? $file_model->toResource() : null;
    }
}
