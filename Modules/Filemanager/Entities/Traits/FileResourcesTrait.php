<?php

namespace Modules\Filemanager\Entities\Traits;

trait FileResourcesTrait
{
    /**
     * get Versions Resource.
     *
     * @return array
     */
    protected function getVersionsResource()
    {
        $files = $this->getAllVersions();

        return collect($files)
             ->map(function ($file_name) {
                 return url($this->directory . DIRECTORY_SEPARATOR . $file_name);
             })
             ->toArray()
             ;
    }
}
