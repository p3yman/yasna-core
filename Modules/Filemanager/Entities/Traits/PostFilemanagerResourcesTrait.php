<?php

namespace Modules\Filemanager\Entities\Traits;

use App\Models\File;

/**
 * @property string $featured_image
 */
trait PostFilemanagerResourcesTrait
{
    /**
     * get FeaturedImage resource. (refactored from Emitis work)
     *
     * @return array|null
     */
    protected function getFeaturedImageResource()
    {
        if ($this->hasnot('featured_image') or is_null($this->featured_image)) {
            return null;
        }

        /** @var File $featured_image_file */
        $featured_image_file = model('file')->grabHashid($this->featured_image);

        return $featured_image_file->exists ? $featured_image_file->toResource() : null;
    }



    /**
     * get Attachments resource.
     *
     * @return array|null
     */
    protected function getAttachmentsResource()
    {
        if ($this->hasnot('attachments')) {
            return null;
        }

        $map = collect($this->files)
             ->values()
             ->map(function (array $file_info) {
                 /** @var File $file */
                 $file = model('file')->grabHashid($file_info['src']);

                 return [
                      'label' => $file_info['label'],
                      'link'  => $file_info['link'],
                      'file'  => $file->toResource(),
                 ];
             })
        ;

        return $map->toArray();
    }
}
