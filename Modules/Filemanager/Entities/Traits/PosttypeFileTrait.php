<?php

namespace Modules\Filemanager\Entities\Traits;

use App\Models\Input;
use League\Flysystem\Config;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use App\Models\Setting;

trait PosttypeFileTrait
{
    /**
     * Checks if files could be uploaded for this posttype
     *
     * @param string $config_slug Group of File (ex: attachments, featured_image ...)
     * @param string $file_types
     *
     * @return bool
     */
    public function hasUploadPrerequisite($config_slug = '*', $file_types = '*')
    {
        if ((!is_string($config_slug) and $config_slug) or (!is_string($file_types) and $file_types)) {
            return null;
        }
        $this->spreadMeta();

        /**
         * Find a usable value for $file_types
         */
        if (!is_array($file_types)) {
            if ($file_types == '*') {
                $file_types = FileManagerToolsServiceProvider::getAcceptableFileTypes();
            } else {
                $file_types = explode_not_empty(',', $file_types);
            }
        }

        $uploading_metas = $this->uploadingMetas($config_slug);

        foreach ($uploading_metas as $meta_slug) {
            /**
             * Find value of current meta of $this posttype
             */
            $input_val = $this->getUploadConfigs($meta_slug);

            if ($input_val) {
                /**
                 * Checks if $this posttype with current input can accept any of file types specified in $fileTypes
                 */
                $intersect = array_intersect(array_keys($input_val['fileTypes']), $file_types);
                if (count($intersect) > 0) {
                    return true;
                }
                // else go to check the next input of $this posttype
            }
        }

        /**
         * If
         *      this posttype doesn't have any related input
         *      or it has related inputs but any of them don't have value
         *      or some or all of them have value but don't accept any of file types specified in $fileTypes
         *  we say that this posttype can't accept files in this request.
         */
        return false;
    }



    /**
     * Returns an array of meta fields which could contain an upload config
     * based on the given config slug.
     *
     * @param string $config_slug
     *
     * @return array
     */
    protected function uploadingMetas($config_slug = '*')
    {
        $selected_inputs = FileManagerToolsServiceProvider::getUploadingInputs();
        $postfix         = ConfigsServiceProvider::getUploadRelatedInputsPostfix();

        if (!$config_slug) {
            $selected_inputs = $selected_inputs->where('slug', $postfix);
        } elseif ($config_slug != '*') {
            $selected_inputs = $selected_inputs->where('slug', $config_slug . '_' . $postfix);
        }

        return $selected_inputs->pluck('slug')->toArray();
    }



    /**
     * Returns the upload config of the posttype with given slug.
     *
     * @param string $config_name
     *
     * @return array|null
     * @throws \Exception
     */
    public function getUploadConfigs($config_name)
    {
        $this->spreadMeta();
        $input_slug     = ConfigsServiceProvider::configToInput($config_name);
        $meta           = $this->$input_slug ?:
             FileManagerToolsServiceProvider::findInputBySlug($input_slug)->default_value;
        $upload_configs = ConfigsServiceProvider::getConfigData($meta);
        if ($upload_configs) {
            $safeFileTypes = [];
            $configs_array       = config('filemanager.upload.default.fileTypes');
            if (is_array($configs_array)) {
                foreach ($configs_array as $type => $configs) {
                    if (isset($upload_configs['fileTypes'][$type]['status'])) {
                        if ($upload_configs['fileTypes'][$type]['status']) {
                            $safeFileTypes[$type] = $upload_configs['fileTypes'][$type];
                        } else {
                            $safeFileTypes[$type]['status'] = false;
                        }
                    }
                }
            }
            $upload_configs['fileTypes'] = $safeFileTypes;

            if (count($upload_configs['fileTypes']) > 0) {
                return $upload_configs;
            }
        }
        return null;
    }



    public function getAllUploadConfigs()
    {
        $inputs = $this->uploading_inputs;
        if ($inputs) {
            $configs = [];
            foreach ($inputs as $input) {
                $thisConfigs = $this->getUploadConfigs($input->slug);
                if ($thisConfigs) {
                    $configs[] = $thisConfigs;
                }
            }

            return UploadServiceProvider::mergeConfigs(...$configs);
        }

        return null;
    }



    public function getUploadingInputsAttribute()
    {
        return $this->inputs()
                    ->where('slug', 'like', '%' . ConfigsServiceProvider::getUploadRelatedInputsPostfix())
                    ->get()
             ;
    }
}
