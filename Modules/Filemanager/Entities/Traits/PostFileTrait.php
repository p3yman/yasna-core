<?php

namespace Modules\Filemanager\Entities\Traits;

use League\Flysystem\Config;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use App\Models\Setting;

trait PostFileTrait
{
    public function getFilesAttribute()
    {
        if (
             ($files = $this->spreadMeta()->post_files) and
             (
                  is_array($array = $files) or
                  is_array($array = json_decode($files, true))
             )
        ) {
            return $array;
        } else {
            return [];
        }
    }



    public function filesMetaFields()
    {
        return [
             'post_files',
        ];
    }
}
