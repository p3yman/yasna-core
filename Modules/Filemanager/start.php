<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

use Modules\Filemanager\Services\Tools\Doc;

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("formatBytes")) {
    /**
     * Convert bytes to other units
     *
     * @param int $bytes
     * @param int $precision
     *
     * @return string
     */
    function formatBytes($bytes, $precision = 2)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow   = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow   = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return ad(round($bytes, $precision)) . ' ' . trans('filemanager::base.byte-units.' . $units[$pow]);
    }
}

if (!function_exists("getJsTrans")) {

    function getJsTrans()
    {
        $parts = [
             'message' => trans('filemanager::base.message'),
             'error'   => trans('filemanager::base.error'),
        ];

        return $parts;
    }
}

if (!function_exists("clearFileVersionTitle")) {

    function clearFileVersionTitle($fileName)
    {
        return preg_replace("/[^\w0-9]+/", "", $fileName);
    }
}

if (!function_exists("doc")) {

    function doc($hashid = null)
    {
        return fileManager()->file($hashid);
    }
}

if (!function_exists("fileManager")) {

    function fileManager()
    {
        return new \Modules\Filemanager\Services\FileManagerHandler;
    }
}
