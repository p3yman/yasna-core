<?php

namespace Modules\Filemanager\Traits;

trait AjaxControllerTrait
{
    public function ajaxJsonResponse($data = [], $configs = [])
    {
        $configs = array_normalize($configs, [
            'status' => 200,
        ]);
        $data = array_default($data, [
            'success' => true,
        ]);

        return response()->json($data, $configs['status']);
    }
}
