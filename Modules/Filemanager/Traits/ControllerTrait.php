<?php

namespace Modules\Filemanager\Traits;

use Modules\Manage\Traits\ManageControllerTrait;

trait ControllerTrait
{
    use ManageControllerTrait {
        abort as protected manageAbort;
    }

    /**
     * @param       $errorCode
     * @param bool  $minimal      if true minimal view will be loading
     * @param bool  $affectHeader if true header or response will be changed into $errorCode
     * @param mixed $data
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     *
     */
    public function abort($errorCode, $minimal = false, $affectHeader = true, $data = null)
    {
        return $this->manageAbort($errorCode, $minimal, $affectHeader, $data);
    }
}
