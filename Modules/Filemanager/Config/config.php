<?php

$result = [
     'name' => 'Filemanager',


     'upload' => [
          'absolute' => [
               'uploadDir' => implode(DIRECTORY_SEPARATOR, ['default']),
               'fileTypes' => [
                   /**
                    * "status"             => if true => current file type of file will be uploading
                    * "acceptedExtensions" => array of accepted extensions for current file type of file
                    * "maxFileSize"        => max size of file that can be accepted (in MB)
                    * "maxFiles"           => max number of files that could be uploaded in current file type
                    * "icon"               => icon to show
                    */

                   'image' => [
                        'status'             => true,
                        'acceptedExtensions' => ['jpg', 'jpeg', 'png', 'gif'],
                        'acceptedFiles'      => [
                             'image/pjpeg',
                             'image/jpeg',
                             'image/png',
                             'image/gif',
                        ],
                        'maxFileSize'        => 5,
                        'maxFiles'           => 5,
                        'icon'               => 'picture-o',
                        'extraFiles'         => [
                             'thumb' => [
                                  'width'  => 300,
                             ],
                        ],
                   ],

                   'video' => [
                        'status'             => true,
                        'acceptedExtensions' => ['mp4', 'avi', 'mkv'],
                        'acceptedFiles'      => ['video/mp4', 'video/avi', 'video/x-matroska'],
                        'maxFileSize'        => 100,
                        'maxFiles'           => 5,
                        'icon'               => 'file-video-o',
                   ],

                   'audio' => [
                        'status'             => true,
                        'acceptedExtensions' => ['mp3', 'wave', 'wma', 'mpga'],
                        'acceptedFiles'      => [
                             'audio/mpeg',
                             'audio/x-mpeg',
                             'audio/mp3',
                             'audio/x-mp3',
                             //                'audio/mpeg3',
                             //                'audio/x-mpeg3',
                             'audio/wav',
                             'audio/x-wav',
                             //                'audio/wave',
                             'audio/x-ms-wma',
                        ],
                        'maxFileSize'        => 10,
                        'maxFiles'           => 5,
                        'icon'               => 'file-audio-o',
                   ],

                   'text' => [
                        'status'             => true,
                        'acceptedExtensions' => ['pdf', 'doc', 'docs'],
                        'acceptedFiles'      => [
                             'application/pdf',
                             'application/msword',
                             'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        ],
                        'maxFileSize'        => 100,
                        'maxFiles'           => 5,
                        'icon'               => 'file-text-o',
                   ],

                   'compressed' => [
                        'status'             => true,
                        'acceptedExtensions' => ['zip', 'rar'],
                        'acceptedFiles'      => [
                             'application/zip',
                             'application/x-rar',
                        ],
                        'maxFileSize'        => 100,
                        'maxFiles'           => 5,
                        'icon'               => 'file-archive-o',
                   ],

                   'icon' => [
                        'status'             => true,
                        'acceptedExtensions' => ['ico'],
                        'acceptedFiles'      => ['image/x-icon'],
                        'maxFileSize'        => 5,
                        'maxFiles'           => 5,
                        'icon'               => 'picture-o',
                   ],
               ],
          ],
     ],

     'file-types' => [
          'mimes' => [
               'ai'    => 'application/postscript',
               'aif'   => 'audio/x-aiff',
               'aiff'  => 'audio/x-aiff',
               'asf'   => 'video/x-ms-asf',
               'asx'   => 'video/x-ms-asx',
               'avi'   => 'video/avi',
               'bin'   => 'application/octet-stream',
               'bmp'   => 'image/bmp',
               'bz'    => 'application/x-bzip',
               'bz2'   => 'application/x-bzip2',
               'crt'   => 'application/x-x509-ca-cert',
               'css'   => 'text/css',
               'csv'   => 'text/plain',
               'doc'   => 'application/msword',
               'docx'  => 'application/msword',
               'dot'   => 'application/msword',
               'dxf'   => 'application/dxf',
               'eps'   => 'application/postscript',
               'gif'   => 'image/gif',
               'gz'    => 'application/x-gzip',
               'gzip'  => 'application/x-gzip',
               'htm'   => 'text/html',
               'html'  => 'text/html',
               'ico'   => 'image/x-icon',
               'jpg'   => 'image/jpeg',
               'jpe'   => 'image/jpeg',
               'jpeg'  => 'image/jpeg',
               'js'    => 'text/javascript',
               'm4a'   => 'audio/mp4',
               'mov'   => 'video/quicktime',
               'mp3'   => 'audio/mpeg',
               'mp4'   => 'video/mp4',
               'mpeg'  => 'video/mpeg',
               'mpg'   => 'video/mpeg',
               'pdf'   => 'application/pdf',
               'php'   => 'text/plain',
               'phps'  => 'text/plain',
               'png'   => 'image/png',
               'pot'   => 'application/vnd.ms-powerpoint',
               'ppa'   => 'application/vnd.ms-powerpoint',
               'pps'   => 'application/vnd.ms-powerpoint',
               'ppt'   => 'application/vnd.ms-powerpoint',
               'ps'    => 'application/postscript',
               'qt'    => 'video/quicktime',
               'ra'    => 'audio/x-pn-realaudio',
               'ram'   => 'audio/x-pn-realaudio',
               'rtf'   => 'application/rtf',
               'shtml' => 'text/html',
               'sit'   => 'application/x-stuffit',
               'swf'   => 'application/x-shockwave-flash',
               'sql'   => 'text/plain',
               'tar'   => 'application/x-tar',
               'tgz'   => 'application/x-compressed',
               'tif'   => 'image/tiff',
               'tiff'  => 'image/tiff',
               'txt'   => 'text/plain',
               'wav'   => 'audio/wav',
               'wma'   => 'audio/x-ms-wma',
               'wmf'   => 'windows/metafile',
               'wmv'   => 'video/x-ms-wmv',
               'xls'   => 'application/vnd.ms-excel',
               'xlsx'  => 'application/vnd.ms-excel',
               'xlt'   => 'application/vnd.ms-excel',
               'z'     => 'application/x-compressed',
               'zip'   => 'application/zip',
          ],

          'groups-extension' => [
               'image'      => [
                    'ai',
                    'bmp',
                    'dxf',
                    'eps',
                    'gif',
                    'ico',
                    'jpg',
                    'jpe',
                    'jpeg',
                    'png',
                    'ps',
                    'swf',
                    'tif',
                    'tiff',
                    'wmf',
               ],
               'video'      => [
                    'asf',
                    'asx',
                    'avi',
                    'mov',
                    'mpg',
                    'mpeg',
                    'mp4',
                    'qt',
                    'ra',
                    'ram',
                    'swf',
                    'wmv',
               ],
               'audio'      => [
                    'mp3',
                    'm4a',
                    'ra',
                    'ram',
                    'wav',
                    'wma',
               ],
               'text'       => [
                    'doc',
                    'docx',
                    'pdf',
                    'txt',
                    'dot',
                    'rtf',
                    'txt',
               ],
               'compressed' => [
                    'bin',
                    'bz',
                    'bz2',
                    'gz',
                    'sit',
                    'tar',
                    'tgz',
                    'z',
                    'zip',
               ],
          ],
          'groups-mimes'     => [],
     ],

     'uploaders' => [
          'dropzone',
     ],

     'default-uploader' => 'dropzone',
];

foreach ($result['file-types']['groups-extension'] as $group => $extensions) {
    $result['file-types']['groups-mimes'][$group] = [];
    foreach ($extensions as $extension) {
        array_push($result['file-types']['groups-mimes'][$group], $result['file-types']['mimes'][$extension]);
    }
}

return $result;
