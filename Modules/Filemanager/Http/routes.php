<?php

/*
|--------------------------------------------------------------------------
| Manage
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'manage/file-manager',
          'namespace'  => 'Modules\Filemanager\Http\Controllers',
     ],
     function () {
         /*
         |--------------------------------------------------------------------------
         | Upstream
         |--------------------------------------------------------------------------
         |
         */
         Route::group(['prefix' => 'upstream', 'middleware' => 'is:developer'], function () {
             /*
             |--------------------------------------------------------------------------
             | Configs
             |--------------------------------------------------------------------------
             |
             */
             Route::group(['prefix' => 'config', 'namespace' => 'Upstream'], function () {
                 Route::get('{action}/{hash_id?}', 'ConfigsController@action')
                      ->name('fileManager.upstream.configs.action')
                 ;

                 Route::group(['prefix' => 'save'], function () {
                     Route::post('/', 'ConfigsController@save')
                          ->name('fileManager.upstream.configs.save')
                     ;
                     Route::post('activeness', 'ConfigsController@activeness')
                          ->name('fileManager.upstream.configs.activeness')
                     ;
                     Route::post('destroy', 'ConfigsController@destroy')
                          ->name('fileManager.upstream.configs.destroy')
                     ;
                 });
             });
         });

         /*
         |--------------------------------------------------------------------------
         | Posts
         |--------------------------------------------------------------------------
         |
         */
         Route::group(['prefix' => 'posts', 'namespace' => 'Posts'], function () {
             Route::get('editor-attachments-item-new/{hashid}', 'PostsController@editorAttachmentsNewItem')
                  ->name('fileManager.manage.posts.editor.attachments.item.new')
             ;
             Route::get('editor-attachments-item-remove/{hashid}', 'PostsController@editorAttachmentsRemoveItem')
                  ->name('fileManager.manage.posts.editor.attachments.item.remove')
             ;
         });
     }
);

Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'filemanager',
          'namespace'  => 'Modules\Filemanager\Http\Controllers',
     ], function () {
         Route::get('/', 'FilemanagerController@index')
         ->name('fileManager.index')
    ;
         Route::post('get-list', 'FilemanagerController@getList')
         ->name('fileManager.getList')
    ;
         Route::post('preview', 'FilemanagerController@getPreview')
         ->name('fileManager.preview')
    ;

         Route::post('remove-file', 'UploadController@removeFile')->name('uploadFile.removeFile');

         // File
         Route::group(['prefix' => 'file'], function () {
             Route::post('delete', 'FileController@deleteFile')
             ->name('fileManager.file.deleteFile')
        ;
             Route::post('restore', 'FileController@restoreFile')
             ->name('fileManager.file.restoreFile')
        ;
             Route::get('details/{fileKey?}', 'FileController@getFileDetails')
             ->name('fileManager.file.getFileDetails')
        ;
             Route::post('details', 'FileController@setFileDetails')
             ->name('fileManager.file.setFileDetails')
        ;
             //        Route::get('disposable/{hashString}/{hashid}/{fileName?}', 'FilemanagerController@disposableDownload')
        //            ->name('file.download.disposable');
         });


         Route::group(['prefix' => 'test'], function () {
             Route::get('/', 'TestController@index');
             Route::get('file-manager-modal', 'TestController@fileManagerModal');
         });
     });


Route::group(
     [
          'middleware' => 'web',
          'prefix'     => 'filemanager',
          'namespace'  => 'Modules\Filemanager\Http\Controllers',
     ], function () {
         Route::post('upload-file', 'UploadController@uploadFile')->name('uploadFile.uploadFile');

         // File
         Route::group(['prefix' => 'file'], function () {
             Route::get('download/{hashid}/{version?}/{fileName?}', 'FileController@download')
             ->name('fileManager.file.download')
        ;
         });
     });
