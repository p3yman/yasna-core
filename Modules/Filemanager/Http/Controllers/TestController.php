<?php

namespace Modules\Filemanager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Entities\File;
use Modules\Filemanager\Providers\FilemanagerServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Traits\ControllerTrait;
use Modules\Posts\Entities\Posttype;

class TestController extends Controller
{
    use ControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $n = posttype('news');
        $f = File::all()->last();
        $doc = doc($f->hashid)
            ->version('700*ss')
            ->posttype($n)
            ->config('featured_image')
//            ->elementStyle([
//                'border' => "1px solid black"
//            ])
            ->elementStyle("border: 1px solid black; border-radius: 5px;")
            ->elementWidth('200')
            ->elementHeight('200')
            ->elementExtra('readonly')
            ->elementOtherAttributes([
                'onclick' => <<<JS
alert('yes')
JS
            ])
            ->show();
        die($doc);
        dd($doc, __FILE__ . " - " . __LINE__);
        dd($f, __FILE__ . " - " . __LINE__);
        $version = url(FilemanagerServiceProvider::getFilePosttypeVersion($n, $f->hashid, '700500'));
        dd($version, __FILE__ . " - " . __LINE__);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('filemanager::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('filemanager::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('filemanager::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    public function fileManagerModal()
    {
        dd('here', __FILE__ . " - " . __LINE__);
    }
}
