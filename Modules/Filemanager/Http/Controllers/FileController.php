<?php

namespace Modules\Filemanager\Http\Controllers;

use App\Models\File;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Http\Requests\File\FileActionRequest;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Traits\AjaxControllerTrait;
use Modules\Filemanager\Traits\ControllerTrait;

class FileController extends Controller
{
    use ControllerTrait;
    use AjaxControllerTrait;



    /**
     * @param string $fileKey
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getFileDetails($fileKey = '')
    {
        $file = model('file')->grabHashid($fileKey);
        if (!$file->exists) {
            return $this->abort(404);
        }
        if (!$file->can('preview')) {
            return $this->abort(403);
        }

        $doc = fileManager()
             ->file($fileKey)
             ->version('thumb')
             ->resolve()
        ;

        if ($doc->getUrl()) {
            $fileExistsOnStorage = true;
        } else {
            $fileExistsOnStorage = false;
        }

        $bread_crumb = [];
        if ($file->posttype) {
            $posttype = posttype()->grabId($file->posttype);
            if ($posttype->exists) {
                $bread_crumb[] = [
                     'label'    => $posttype->title,
                     'instance' => 'posttype',
                     'key'      => $posttype->hashid,
                ];
            }
        }

        return view(
             'filemanager::main.media-frame-content-gallery-file-details',
             compact('file', 'doc', 'fileExistsOnStorage', 'bread_crumb')
        );
    }



    /**
     * @param FileActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function setFileDetails(FileActionRequest $request)
    {
        $file = model("file")->grabHashid($request->fileKey);
        if (!$file->exists) {
            return $this->abort(404);
        }
        if (!$file->can('edit')) {
            return $this->abort(403);
        }

        $saveData = array_merge(['id' => $file->id], $request->all());

        $fileId      = File::store($saveData, ['fileKey']);
        $updatedFile = model("file",$fileId);

        if ($updatedFile and $updatedFile->exists and ($updatedFile->hashid == $request->fileKey)) {
            return $this->ajaxJsonResponse(['success' => true]);
        } else {
            return $this->ajaxJsonResponse(['success' => false], ['status' => 500]);
        }
    }



    /**
     * @param        $hashid
     * @param string $version
     * @param null   $fileName
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function download($hashid, $version = 'original', $fileName = null)
    {
        $file = File::findByHashid($hashid);
        if (!$file->exists or !FileServiceProvider::getFileObject($file->pathname)) {
            return $this->abort('404');
        }


        if ($fileName) {
            if (!ends_with($fileName, '.' . $file->extension)) {
                $fileName = $fileName . '.' . $file->extension;
            }
        } else {
            $fileName = $file->file_name;
        }

        $headers = [
             'Content-Type: ' . $file->mime_type,
        ];

        $relatedFiles = $file->related_files;
        if (array_key_exists($version, $relatedFiles)) {
            $pathname = $file->directory . DIRECTORY_SEPARATOR . $relatedFiles[$version];
        } else {
            $pathname = $file->pathname;
        }

        return response()->download($pathname, $fileName, $headers);
    }



    /**
     * @param FileActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteFile(FileActionRequest $request)
    {
        $file = File::findByHashid($request->fileKey);
        if (!$file->exists) {
            return $this->abort(404);
        }
        if (!$file->can('delete')) {
            return $this->abort(403);
        }

        $file->delete();
    }



    /**
     * @param FileActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restoreFile(FileActionRequest $request)
    {
        $file = File::findByHashid($request->fileKey, ['with_trashed' => true]);
        if (!$file->exists) {
            return $this->abort(404);
        }
        if (!$file->can('delete')) {
            return $this->abort(403);
        }

        $file->restore();
    }
}
