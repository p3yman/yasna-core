<?php

namespace Modules\Filemanager\Http\Controllers;

use App\Models\File;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Services\Tools\Uploader\DropzoneUploader;
use Modules\Filemanager\Traits\ControllerTrait;
use Symfony\Component\Debug\Exception\UndefinedMethodException;

class FilemanagerController extends Controller
{
    use ControllerTrait;

    protected $variables = [];



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $postTypes = FileManagerToolsServiceProvider::availablePosttypes();

        $this->indexHandleConfigName($request);

        // If current user hasn't permission to any posttype
        if (!$postTypes->count()) {
            $view = view('filemanager::errors.nothing-to-show', [
                 'errorCode' => 403,
            ]);
            return $this->abort('403', false, true, $view);
        }

        $uploader = $this->indexGenerateUploader();

        if ($fileType = $request->fileType) {
            try {
                $uploader->$fileType();
            } catch (UndefinedMethodException $exception) {
                return $this->abort(403);
            }
        }

        return view('filemanager::index', compact('postTypes', 'uploader'));
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function indexHandleConfigName(Request $request)
    {
        if ($request->input('configName')) {
            UploadServiceProvider::setCurrentConfigSlug(request()->input('configName'));
        }

        return $this;
    }



    /**
     * @return DropzoneUploader
     */
    protected function indexGenerateUploader()
    {
        return fileManager()
             ->uploader()
             ->directUpload()
             ->onEachUploadComplete('eachUploadCompleted')
             ->onQueueComplete('allUploadsCompleted')
             ->forceConfigSlug('__posttype__')
             ->withoutPosttype()
             ->progressesUnder()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getList(Request $request)
    {
        $this->variables['builder'] = model('file')->select();
        $this->variables['request'] = $request;
        $apply_folder_result        = $this->getListApplyFolder();

        if (!$apply_folder_result) {
            return '';
        }

        $this->getListApplyFileType()
             ->getListApplySearch()
             ->getListApplySort()
             ->getListTerminateQuery()
        ;

        return view('filemanager::main.media-frame-content-gallery-images-list', $this->variables);
    }



    /**
     * @return $this|bool
     */
    protected function getListApplyFolder()
    {
        switch ($this->variables['request']->instance) {
            case 'posttype':
                $this->variables['postType'] = posttype()->grabHashid($this->variables['request']->key);
                if (!$this->variables['postType']->exists) {
                    return false;
                }
                $this->variables['builder']->where(['posttype' => $this->variables['postType']->id]);
                break;
            default:
                return false;
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function getListApplyFileType()
    {
        if ($this->variables['request']->fileType) {
            $this->variables['builder']->where('type', $this->variables['request']->fileType);
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function getListApplySearch()
    {
        if ($this->variables['request']->search) {
            $this->variables['builder']->where('name', 'like', '%' . $this->variables['request']->search . '%');
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function getListApplySort()
    {
        if ($this->variables['request']->sort) {
            $sortOrders = ['asc', 'desc'];
            $sortParts  = explode('.', $this->variables['request']->sort);
            if ((count($sortParts) == 2) and (in_array($sortParts[1], $sortOrders) !== false)) {
                switch ($sortParts[0]) {
                    case 'time':
                        $this->variables['builder']->orderBy('created_at', $sortParts[1]);
                        break;

                    case 'name':
                        $this->variables['builder']->orderBy('name', $sortParts[1]);
                        break;

                    case 'size':
                        $this->variables['builder']->orderBy('size', $sortParts[1]);
                        break;

                    default:
                        $this->variables['builder']->orderBy('created_at', 'DESC');
                        break;
                }
            }
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function getListTerminateQuery()
    {
        $this->variables['files'] = $this->variables['builder']->get();
        return $this;
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function getPreview(Request $request)
    {
        $files  = explode_not_empty('-', $request->file);
        $result = '';

        foreach ($files as $file) {
            $result .= FileServiceProvider::getFileView($file, 'thumbnail', [
                 'style' => [
                      'max-width' => '100%',
                 ],
            ]);
        }
        return $result;
    }
}
