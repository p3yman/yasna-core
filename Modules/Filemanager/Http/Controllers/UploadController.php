<?php

namespace Modules\Filemanager\Http\Controllers;

use App\Models\File;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Http\Requests\RemoveFileRequest;
use Modules\Filemanager\Http\Requests\Upload\UploadFileRequest;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Traits\ControllerTrait;

class UploadController extends Controller
{
    use ControllerTrait;



    /**
     * Action to upload a single file
     *
     * @param UploadFileRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     */
    public function uploadFile(UploadFileRequest $request)
    {
        $externalFields = json_decode($request->_externalFields, true) ?: [];

        /******************** Set data related to 'posttype' ***************** START */
        if (
             isset($externalFields['posttype']) and
             $externalFields['posttype'] and
             ($posttype = Posttype::findByHashid($externalFields['posttype'])) and
             $posttype->exists
        ) {
            if ($posttype->exists) {
                $externalFields['posttype'] = $posttype->id;

                $postTypeConfigPointer = UploadServiceProvider::getPostTypeConfigPrefix() . $posttype->slug;
                $modifiedIdentifiers   = str_replace(
                     '__posttype__',
                     $postTypeConfigPointer,
                     $request->_uploadIdentifier
                );
                $request->merge(['_uploadIdentifier' => encrypt($modifiedIdentifiers)]);

                if ($request->_configName) {
                    $inputName = ConfigsServiceProvider::configToInput($request->_configName);
                    if ($posttype->hasInput($inputName)) {
                        UploadServiceProvider::setCurrentConfigSlug($request->_configName);
                    }
                }
            }
        } else {
            return $this->abort(422, false, true, [
                 'file' => [
                      trans('filemanager::validation.unable_to_upload'),
                 ],
            ]);
        }
        /******************** Set data related to 'posttype' ***************** END */


        $uploadIdentifiers = explode_not_empty(',', $request->_uploadIdentifier);

        if (count($uploadIdentifiers) == 0) {
            return $this->abort('403', true);
        }

        $file          = $request->file;
        $fileExtension = $file->guessExtension();

        if (count($uploadIdentifiers) > 1) {
            if (($validationResponse = UploadServiceProvider::validateFile($request)) !== true) {
                return response()->json($validationResponse->toArray(), 422);
            }
            foreach ($uploadIdentifiers as $uploadIdentifier) {
                $acceptedExtensions = UploadServiceProvider::getTypeRule($uploadIdentifier, 'acceptedExtensions');
                if (in_array($fileExtension, $acceptedExtensions) !== false) {
                    $typeString = $uploadIdentifier;
                    break;
                }
            }

            $request->merge(['_uploadIdentifier' => encrypt($typeString)]);
        } else {
            $typeString = $request->_uploadIdentifier;
        }

        $sessionName = $request->_groupName;

        if (($validationResponse = UploadServiceProvider::validateFile($request)) === true) {
            $typeStringParts = explode('.', $typeString);
            $sectionName     = implode('.', array_slice($typeStringParts, 0, count($typeStringParts) - 1));
            $folderName      = array_last($typeStringParts);
            UploadServiceProvider::setConfigsSection($typeStringParts[0]);

            $itemIndex = str_random(4);
            if (session()->has($sessionName)) {
                $currentUploaded = session()->get($sessionName);

                // check if this item exists in the session and change it if needed
                while (array_key_exists($itemIndex, $currentUploaded) != false) {
                    $itemIndex = str_random(4);
                }

                $currentUploaded[$itemIndex] = [
                     'name'   => $file->getClientOriginalName(),
                     'number' => (count($currentUploaded) + 1),
                     'done'   => false,
                ];
                session()->put($sessionName, $currentUploaded);
            } else {
                session()->put($sessionName, [
                     $itemIndex => [
                          'name'   => $file->getClientOriginalName(),
                          'number' => 1,
                          'done'   => false,
                     ],
                ]);
            }
            session()->save();

            $uploadDir = implode(DIRECTORY_SEPARATOR, [
                 UploadServiceProvider::getSectionRule($sectionName, 'uploadDir'),
                 $folderName,
            ]);

            if ($request->_directUpload) {
                UploadServiceProvider::disableTemporaryUpload();
            }

            $uploadResult = UploadServiceProvider::uploadFile($file, $uploadDir, $externalFields);
            $dbRow        = model("file")->grabId($uploadResult);

            if ($dbRow->exists()) {
                /**
                 * This condition is for synchronous uploading.
                 * If files number has reached the limit while uploading this file, this should be deleted.
                 */
                if (UploadServiceProvider::validateFileNumbers($sectionName, $typeString)) {
                    $currentUploaded[$itemIndex]['done'] = true;
                    $currentUploaded[$dbRow->hash_id]    = $currentUploaded[$itemIndex];
                    unset($currentUploaded[$itemIndex]);

                    session()->put($sessionName, $currentUploaded);
                    session()->save();

                    return response()->json([
                         'success' => true,
                         'file'    => $dbRow->hash_id,
                    ]);
                } else {
                    UploadServiceProvider::removeFile($dbRow);
                }
            }
        } else {
            return response()->json($validationResponse->toArray(), 422);
        }

        return response()->make('', 400);
    }



    /**
     * Action to remove a single file from uploader
     *
     * @param RemoveFileRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function removeFile(RemoveFileRequest $request)
    {
        $sessionName = $request->_groupName;
        $file        = $request->file;

        if ($file) {
            if (session()->has($sessionName)) {
                $currentUploaded = session()->get($sessionName);
                if (array_key_exists($fileHashid = hashid($request->file), $currentUploaded)) {
                    unset($currentUploaded[$fileHashid]);
                    session()->put($sessionName, $currentUploaded);
                }
            }
            session()->save();

            if (FileServiceProvider::removeFile($file, ['checkExactId' => true])) {
                return response(trans('filemanager::general.message-file-deleted'));
            }
        }

        return response(trans('filemanager::general.error-file-delete'), 410);
    }
}
