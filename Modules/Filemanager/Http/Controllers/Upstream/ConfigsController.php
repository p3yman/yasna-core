<?php

namespace Modules\Filemanager\Http\Controllers\Upstream;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Filemanager\Http\Requests\Configs\ConfigActivenessRequest;
use Modules\Filemanager\Http\Requests\Configs\ConfigSaveRequest;
use Modules\Filemanager\Http\Requests\Configs\ConfigDestroyRequest;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Traits\ControllerTrait;

class ConfigsController extends Controller
{
    use ControllerTrait;

    public static function index()
    {
        /*-----------------------------------------------
        | View File
        */
        $result['view_file'] = "filemanager::upstream.config.index";

        /*-----------------------------------------------
        | Models
        */
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();
        $result['models'] = Setting::withTrashed()
            ->where('slug', 'like', '%\_' . $postfix)
            ->orderBy('created_at', 'desc');

        /*-----------------------------------------------
        | Search
        */
        if (Input::has('searched') and Input::get('searched') and Input::has('keyword')) {
            $keyword = Input::get('keyword');
            $result['models']->where(function ($query) use ($keyword) {
                $query->where('title', 'like', '%' . $keyword . '%')
                    ->orWhere('slug', 'like', '%' . $keyword . '%');
            });
            $result['keyword'] = $keyword;
        }

        /*-----------------------------------------------
        | Paginate
        */
        $result['models'] = $result['models']->paginate(40);


        return $result;
    }

    public function action($action, $hashid = false)
    {
        /*-----------------------------------------------
        | Action Check ...
        */
        if (!in_array($action, ['edit', 'create', 'row', 'activeness', 'destroy'])) {
            return $this->abort(404, true, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        if (in_array($action, ['create'])) {
            $model = model('setting');
        } else {
            $model = model('setting')->withTrashed()->grabHashid($hashid);

            if (!$model or !$model->id) {
                return $this->abort(410, true);
            }
        }

        /*-----------------------------------------------
        | View ...
        */
        if ($action == 'create') {
            $view_file = "filemanager::upstream.config.edit";
        } else {
            $view_file = "filemanager::upstream.config.$action";
        }

        return view($view_file, compact('model'));
    }



    /**
     * @param ConfigSaveRequest $request
     *
     * @return string
     */
    public function save(ConfigSaveRequest $request)
    {
        $unsetThings = ['id'];
        /*-----------------------------------------------
        | Model Detection ...
        */
        if ($existedConfig = boolval($request->id)) {
            $model = Setting::withTrashed()->find($request->id);
            if (!$model) {
                return $this->jsonFeedback();
            }

            if (ConfigsServiceProvider::isRequired($model)) {
                $unsetThings[] = 'slug';
            }
        } else {
            $model = model('setting');
        }

        /*-----------------------------------------------
        | Save ...
        */

        Model::unguard();

        $savedModel = $model->batchSave($request->all(), $unsetThings);

        /*-----------------------------------------------
        | Feedback ...
        */

        return $this->jsonAjaxSaveFeedback($savedModel->exists, [
             'success_callback' => $existedConfig ? "rowUpdate('tblConfigs','$savedModel->hashid')" : '',
             'success_refresh'  => (!$existedConfig) ? 1 : 0,
        ]);
    }

    public function activeness(ConfigActivenessRequest $request)
    {
        switch ($request->toArray()['_submit']) {
            case 'delete':
                $model = Setting::find($request->id);
                if (!$model or ConfigsServiceProvider::isRequired($model)) {
                    return $this->jsonFeedback(trans('manage::forms.general.sorry'));
                }
                $ok = Setting::where('id', $request->id)->delete();
                break;

            case 'restore':
                $model = Setting::withTrashed()->find($request->id);
                if (!$model or ConfigsServiceProvider::isRequired($model)) {
                    return $this->jsonFeedback(trans('manage::forms.general.sorry'));
                }
                $ok = Setting::withTrashed()->where('id', $request->id)->restore();
                break;

            default:
                $ok = false;
        }

        $hashid = hashid($request->id);

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblConfigs', '$hashid')",
        ]);
    }

    public function destroy(ConfigDestroyRequest $request)
    {
        $model = Setting::withTrashed()->find($request->id);
        if (!$model or ConfigsServiceProvider::isRequired($model)) {
            return $this->jsonFeedback(trans('manage::forms.general.sorry'));
        }

        $hashid = $model->hashid;

        return $this->jsonAjaxSaveFeedback($model->forceDelete(), [
            'success_callback' => "rowHide('tblConfigs' , '$hashid')",
            'success_refresh'  => false,
        ]);
    }
}
