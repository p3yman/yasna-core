<?php

namespace Modules\Filemanager\Http\Controllers\Posts;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Traits\ControllerTrait;

class PostsController extends Controller
{
    use ControllerTrait;

    public function editorAttachmentsNewItem($option)
    {
        return view('filemanager::presenters.post-editor-attachments-uploader-new-mess', compact('option'));
    }

    public function editorAttachmentsRemoveItem($option)
    {
        FileServiceProvider::removeFile($option);
        return view('filemanager::presenters.post-editor-attachments-uploader-new-mess');
    }

    public static function saveGate($model, $data)
    {
        $filesKey = '_files';
        if (array_key_exists($filesKey, $data)) {
            if (!is_array($data[$filesKey])) {
                $decodedFiles = json_decode($data[$filesKey], true);
                if ($decodedFiles and is_array($decodedFiles)) {
                    $data[$filesKey] = $decodedFiles;
                }
            }
            foreach ($data[$filesKey] as $file) {
                FileServiceProvider::moveFromTemp($file['src']);
            }

            $data['post_files'] = $data[$filesKey];
        }

        return $data;
    }
}
