<?php

namespace Modules\Filemanager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filemanager\Http\Controllers\Upstream\ConfigsController;

class UpstreamController extends Controller
{
    public static function configs()
    {
        return ConfigsController::index();
    }
}
