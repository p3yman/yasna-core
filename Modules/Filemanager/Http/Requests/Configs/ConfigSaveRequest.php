<?php

namespace Modules\Filemanager\Http\Requests\Configs;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ConfigSaveRequest extends YasnaRequest
{
    protected $config_errors = [];
    protected $test          = 0;



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $id    = $input['id'];

        return [
             'title'         => [
                  'required',
                  Rule::unique('settings', 'title')
                      ->where('id', '<>', $id),
             ],
             'slug'          => [
                  'required',
                  'alpha_dash',
                  Rule::unique('settings', 'slug')
                      ->where('id', '<>', $id),
             ],
             'default_value' => 'required|json',
        ];
    }



    /**
     * @param array|null $keys
     *
     * @return array
     */
    public function all($keys = null)
    {
        $value    = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
             'id'    => 'dehash',
             'slug'  => 'lower',
             'order' => "ed",
        ]);

        // Append postfix
        if (isset($purified['slug'])) {
            $purified['slug'] .= '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix();
        }

        return $purified;
    }



    /**
     * @return void
     */
    public function corrections()
    {
        $this->checkConfigDir();
        $this->correctFileTypesData();
    }



    /**
     * @return void
     */
    protected function correctFileTypesData()
    {
        if (!$this->get('default_value')) {
            $file_types = $this->get('fileTypes') ?: [];
            foreach ($file_types as $type => $info) {
                $this->correctStatus($type, $info);
                $this->correctExtensions($type, $info);
                $this->correctMimeTypes($type, $info);
                $this->correctExtraFiles($type, $info);
                $this->correctTypeMaxFileSize($type, $info);
                $this->correctTypeMaxFiles($type, $info);
                $this->unsetTypeProperty($type, 'default');
            }

            $this->convertToJson();
        }
    }



    /**
     * @param $type
     * @param $property
     * @param $value
     *
     * @return $this
     */
    protected function setTypeProperty($type, $property, $value)
    {
        $this->data['fileTypes'][$type][$property] = $value;

        return $this;
    }



    /**
     * @param $type
     * @param $property
     *
     * @return $this
     */
    protected function unsetTypeProperty($type, $property)
    {
        if (isset($this->data['fileTypes'][$type][$property])) {
            unset($this->data['fileTypes'][$type][$property]);
        }

        return $this;
    }



    /**
     * @param      $type
     * @param      $property
     * @param      $value
     * @param bool $condition
     *
     * @return ConfigSaveRequest
     */
    protected function changeTypeProperty($type, $property, $value, $condition = true)
    {
        if ($condition) {
            return $this->setTypeProperty($type, $property, $this->calculateVariable($value));
        } else {
            return $this->unsetTypeProperty($type, $property);
        }
    }



    /**
     * @param $variable
     *
     * @return mixed
     */
    protected function calculateVariable($variable)
    {
        if (is_closure($variable)) {
            return $variable();
        } else {
            return $variable;
        }
    }



    /**
     * @param $type
     * @param $info
     *
     * @return ConfigSaveRequest
     */
    protected function correctStatus($type, $info)
    {
        $status = (boolval($info['status'] ?? false));
        return $this->setTypeProperty($type, 'status', $status);
    }



    /**
     * @param $type
     *
     * @return bool
     */
    protected function typeIsActivated($type)
    {
        return $this->data['fileTypes'][$type]['status'];
    }



    /**
     * @param $type
     *
     * @return bool
     */
    protected function typeIsDeactivated($type)
    {
        return !$this->typeIsActivated($type);
    }



    /**
     * purify array of numbers to make sure that all items are in english digits.
     *
     * @param array $array
     *
     * @return array
     */
    protected function purifyNumbersArray(array $array)
    {
        $keys     = array_keys($array);
        $purifier = array_fill_keys($keys, 'ed');

        return ValidationServiceProvider::purifier($array, $purifier);
    }



    /**
     * @param $type
     * @param $info
     *
     * @return ConfigSaveRequest
     */
    protected function correctExtensions($type, $info)
    {
        $default     = boolval($info['default']['acceptedExtensions'] ?? null);
        $should_fill = ($this->typeIsActivated($type) and !$default);

        return $this->changeTypeProperty(
             $type,
             'acceptedExtensions',
             function () use ($type, $info) {
                 return $this->correctExtensionsCalc($type, $info);
             },
             $should_fill
        );
    }



    /**
     * @param $type
     * @param $info
     *
     * @return array
     */
    protected function correctExtensionsCalc($type, $info)
    {
        $inputs     = $info['acceptedExtensions'] ?? [];
        $extensions = [];

        if (is_array($inputs)) {
            foreach ($inputs as $extension => $value) {
                if ($value) {
                    $extensions[] = $extension;
                }
            }
        }

        if (!count($extensions) and $this->typeIsActivated($type)) {
            $this->addConfigError(
                 "$type-extensions",
                 trans('filemanager::config.extension.required-message', [
                      'type' => trans("filemanager::base.file-types.$type.title"),
                 ])
            );
        }

        return $extensions;
    }



    /**
     * @param $type
     * @param $info
     *
     * @return ConfigSaveRequest
     */
    protected function correctMimeTypes($type, $info)
    {
        $default     = boolval($info['default']['acceptedFiles'] ?? null);
        $should_fill = ($this->typeIsActivated($type) and !$default);

        return $this->changeTypeProperty(
             $type,
             'acceptedFiles',
             function () use ($type, $info) {
                 return $this->correctMimeTypesCalc($type, $info);
             },
             $should_fill
        );
    }



    /**
     * @param $type
     * @param $info
     *
     * @return array
     */
    protected function correctMimeTypesCalc($type, $info)
    {
        $inputs     = $info['acceptedFiles'] ?? [];
        $mime_types = [];

        if (is_array($inputs)) {
            foreach ($inputs as $extension => $value) {
                if ($value) {
                    $mime_types[] = $extension;
                }
            }
        }

        if (!count($mime_types) and $this->typeIsActivated($type)) {
            $this->addConfigError(
                 "$type-mime-types",
                 trans('filemanager::config.mime_type.required-message', [
                      'type' => trans("filemanager::base.file-types.$type.title"),
                 ])
            );
        }

        return $mime_types;
    }



    /**
     * @param $type
     * @param $info
     *
     * @return ConfigSaveRequest
     */
    protected function correctExtraFiles($type, $info)
    {
        $inputs      = $info['extraFiles'] ?? [];
        $extra_files = [];
        foreach ($inputs as $version) {
            $version_title = ($version['title'] ?? null);
            if ($version_title) {
                $version_info = $version;
                unset($version_info['title']);
                if ($this->checkTypeVersion($type, $version_info)) {
                    $extra_files[$version_title] = $this->purifyNumbersArray($version_info);
                } else {
                    $this->addConfigError(
                         "$type-version",
                         trans('filemanager::config.version.width-or-height-required-message', [
                              'type' => trans("filemanager::base.file-types.$type.title"),
                         ]));
                }
            } else {
                $this->addConfigError("$type-version", trans('filemanager::config.version.title-required-message', [
                     'type' => trans("filemanager::base.file-types.$type.title"),
                ]));
            }
        }

        return $this->changeTypeProperty($type, 'extraFiles', $extra_files, count($extra_files));
    }



    /**
     * @param       $type
     * @param array $version
     *
     * @return bool
     */
    protected function checkTypeVersion($type, $version)
    {
        $checking_method_name = 'check' . studly_case($type) . 'ExtraFileInfo';

        if (method_exists($this, $checking_method_name)) {
            return $this->$checking_method_name($version);
        } else {
            return true;
        }
    }



    /**
     * @param array $version
     *
     * @return bool
     */
    protected function checkImageExtraFileInfo($version)
    {
        $width  = ($version['width'] ?? null);
        $height = ($version['height'] ?? null);

        return boolval($width or $height);
    }



    /**
     * @param $type
     * @param $info
     *
     * @return $this
     */
    protected function correctTypeMaxFileSize($type, $info)
    {
        $default     = boolval($info['default']['maxFileSize'] ?? null);
        $should_fill = ($this->typeIsActivated($type) and !$default);


        return $this->changeTypeProperty(
             $type,
             'maxFileSize',
             function () use ($type, $info) {
                 return $this->correctTypeMaxFileSizeCalc($type, $info);
             },
             $should_fill
        );
    }



    /**
     * @param $type
     * @param $info
     *
     * @return string|null
     */
    protected function correctTypeMaxFileSizeCalc($type, $info)
    {
        $max_file_size = ed($info['maxFileSize'] ?? null);


        if (!$max_file_size) {
            $this->addConfigError("$type-max-file-size", trans('filemanager::config.max_file_size.required-message', [
                 'type' => trans("filemanager::base.file-types.$type.title"),
            ]));
            return null;
        } elseif (!preg_match('/^[1-9]+[0-9]*$/', $max_file_size)) {
            $this->addConfigError("$type-max-file-size", trans('filemanager::config.max_file_size.invalid-message', [
                 'type' => trans("filemanager::base.file-types.$type.title"),
            ]));
            return null;
        } else {
            return $max_file_size;
        }
    }



    /**
     * @param $type
     * @param $info
     *
     * @return $this
     */
    protected function correctTypeMaxFiles($type, $info)
    {
        $default     = boolval($info['default']['maxFiles'] ?? null);
        $should_fill = ($this->typeIsActivated($type) and !$default);


        return $this->changeTypeProperty(
             $type,
             'maxFiles',
             function () use ($type, $info) {
                 return $this->correctTypeMaxFilesCalc($type, $info);
             },
             $should_fill
        );
    }



    /**
     * @param $type
     * @param $info
     *
     * @return string|null
     */
    protected function correctTypeMaxFilesCalc($type, $info)
    {
        $max_file_size = ed($info['maxFiles'] ?? null);


        if (!$max_file_size) {
            $this->addConfigError("$type-max-files", trans('filemanager::config.max_file_numbers.required-message', [
                 'type' => trans("filemanager::base.file-types.$type.title"),
            ]));
            return null;
        } elseif (!preg_match('/^[1-9]+[0-9]*$/', $max_file_size) and ($max_file_size != -1)) {
            $this->addConfigError("$type-max-files", trans('filemanager::config.max_file_numbers.invalid-message', [
                 'type' => trans("filemanager::base.file-types.$type.title"),
            ]));
            return null;
        } else {
            return $max_file_size;
        }
    }



    /**
     * @return $this
     */
    protected function checkConfigDir()
    {
        $upload_dir = ($this->data['uploadDir'] ?? null);

        if (!$upload_dir) {
            $this->addConfigError('upload-dir', trans('filemanager::config.directory.required-message'));
        } elseif (!preg_match('/^[a-zA-Z0-9\_\-\.]+[a-zA-Z0-9\_\-\.\\' . DIRECTORY_SEPARATOR . ']*$/', $upload_dir)) {
            $this->addConfigError('upload-dir', trans('filemanager::config.directory.invalid-message'));
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function convertToJson()
    {
        $this->checkConfigDir();
        if (empty($this->config_errors)) {
            $upload_dir = $this->data['uploadDir'];
            $file_types = ($this->data['fileTypes'] ?? []);

            if ($upload_dir and count($file_types)) {
                $array                       = [
                     'uploadDir' => $upload_dir,
                     'fileTypes' => $file_types,
                ];
                $this->data['default_value'] = json_encode($array);
            }
        } else {
            $this->data['default_value'] = '';
        }
        unset($this->data['uploadDir']);
        unset($this->data['fileTypes']);

        return $this;
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'default_value.required' => trans('filemanager::config.validation.general'),
        ];
    }



    /**
     * @param $error
     *
     * @return void
     */
    protected function addConfigError($key, $error)
    {
        $this->config_errors[$key][] = $error;
    }



    /**
     * @param Validator $validator
     *
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors   = array_merge($validator->getMessageBag()->toArray(), $this->config_errors);
        $response = new JsonResponse(['errors' => $errors], 422);

        throw (new ValidationException($validator, $response));
    }
}
