<?php

namespace Modules\Filemanager\Http\Requests\Configs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Yasna\Providers\ValidationServiceProvider;

class ConfigDestroyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                Rule::exists('settings', 'id')->where(function ($query) {
                    $query->whereNotNull('deleted_at');
                })
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
            'id' => 'dehash',
        ]);

        return $purified;
    }
}
