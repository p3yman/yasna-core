<?php

namespace Modules\Filemanager\Http\Requests;

use App\Models\File;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class RemoveFileRequest extends YasnaRequest
{
    public function rules()
    {
        return [
             'file'              => [
                  'required',
                  Rule::exists('files', 'id')
                      ->where('status', File::getStatusValue('temp')),
             ],
             '_uploadIdentifier' => 'required',
             '_groupName'        => 'required',
        ];
    }



    public function purifier()
    {
        return [
             'file' => 'dehash',
        ];
    }



    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response(trans('filemanager::validation.unreliable_inputs'), 403));
    }
}
