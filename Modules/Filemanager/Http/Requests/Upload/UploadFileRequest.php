<?php

namespace Modules\Filemanager\Http\Requests\Upload;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;

class UploadFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '_uploadIdentifier' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value = parent::all(func_get_args());

        $purified = ValidationServiceProvider::purifier($value, [
            '_uploadIdentifier' => 'decrypt',
        ]);

        if (isset($value['file'])) {
            $purified['file'] = $value['file'];
        }

        return $purified;
    }
}
