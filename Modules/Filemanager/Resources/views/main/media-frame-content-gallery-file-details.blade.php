@php $file->spreadMeta() @endphp
<h2>{{ trans('filemanager::base.title.file-details') }}</h2>

<div class="col-xs-12 mt10">
    <div class="row">
        @foreach($bread_crumb as $key => $step)
            @if($key != 0)  <span class="text-blue"> » </span>  @endif
            <a href="#" class="btn-open-folder" data-instance="{{ $step['instance'] }}" data-key="{{ $step['key'] }}">
                {{ $step['label'] }}
            </a>
        @endforeach
    </div>
</div>
<div class="attachment-info full-width pull-start mt15">
    <div class="thumbnail-image">
        {!! $doc->show() !!}
    </div>
    <div class="details text-start">
        <div class="filename ltr">{{ $doc->getFileName() }}</div>
        @if($fileExistsOnStorage)
            <div class="upload-date">{{ ad($doc->getCreationDateTimeString('j F Y')) }}</div>
            <div class="upload-size">{{ $doc->getSizeText() }}</div>
            @if(($image_width = $doc->getImageWidth()) and ($image_height = $doc->getImageHeight()))
                <div class="dimension ltr">
                    {{ ad($image_width) }} x {{ ad($image_width) }}
                </div>
            @endif
            <a class="btn btn-success btn-xs mb5 mt5" href="{{ $doc->getDownloadUrl() }}">
                <i class="fa fa-download"></i>
                {{ trans('filemanager::base.menu.download') }}
            </a>
            <br/>
        @endif
        {{--@if($file->can('edit'))--}}
        {{--<a href="#" class="edit-attachment">ویرایش تصویر</a>--}}
        {{--@endif--}}
        {{--<br />--}}
        @if($file->can('delete'))
            <span class="deletion-message panel-lightRed pl5 pr5 rounded-corners-5" style="display: none"></span>
            <button type="button" class="delete-btn btn-link delete-file-btn">
                <span class="delete-file-btn-text-delete">
                    <i class="fa fa-trash"></i>
                    {{ trans('filemanager::base.menu.delete') }}
                </span>
                <span class="delete-file-btn-text-restore">
                    <i class="fa fa-undo"></i>
                    {{ trans('filemanager::base.menu.restore') }}
                </span>
            </button>
        @endif
    </div>
</div>
@if($fileExistsOnStorage)
    <label class="setting">
        <span class="name">{{ trans('validation.attributes.name') }}</span>
        <input type="text" name="name" value="{{ $doc->getTitle() }}"/>
    </label>
    <label class="setting">
        <span class="name">{{ trans('validation.attributes.title') }}</span>
        <input type="text" name="title" value="{{ $doc->getTitleWithoutExtension() }}"/>
    </label>
    <label class="setting">
        <span class="name">{{ trans('filemanager::validation.attributes.alternative_text') }}</span>
        <input type="text" name="alternative" value="{{ $doc->getAlternative() }}"/>
    </label>
    <label class="setting">
        <span class="name">{{ trans('validation.attributes.description') }}</span>
        <textarea name="description">{{ $doc->getDescription() }}</textarea>
    </label>
@endif