<div class="media-sidebar">
    <div id="sidebar-loading" class="sidebar-loading" style="display: none;">
        <img src="{{ Module::asset('filemanager:image/preloader-circle-small.gif') }}">
    </div>

    <div class="close-sidebar">
        <span class="fa fa-chevron-left"></span>
    </div>
    <div class="media-uploader-status" style="display: none">
        <h2>{{ trans('filemanager::base.message.loading') }}...</h2>
        <button type="button" class="fa fa-times-circle upload-dismiss"></button>
        <div class="media-progress">
            <div></div>
        </div>
        <div class="upload-detail">
                <span class="upload-count">
                    <span class="upload-index">1</span>
                    /
                    <span class="upload-total">1</span>
                </span>
            <span class="upload-detail-separator">_</span>
            <span class="upload-filename">blabla.jpg</span>
        </div>
        <span class="upload-errors"></span>
    </div>
    <div class="file-details pt10"></div>
</div>