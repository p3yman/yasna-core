@php
    $folders = collect();
    $acceptedFilesCategories = [];
    $postTypes->each(function ($postType) use (&$folders, &$acceptedFilesCategories) {
        $pointData = FileManagerTools::getPointData($postType);
        $folders->push($pointData);

    });
@endphp

<div class="media-folder-menu">
    <div class="folder-container">
        <div class="folder-menu">
            <div class="close-sidebar right-align close-menu">
                <span class="fa fa-chevron-right"></span>
            </div>
            <h2 class="title">{{ trans('filemanager::base.title.folders') }}</h2>
            @include('filemanager::main.folder-menu-list', ['items' => $folders])
        </div>
    </div>
</div>

@section('html_footer')
    <script>
        var acceptedFilesCategories = {
            @yield('acceptedFilesCategories')
        };
    </script>
@append