@php
    $acceptedFileTypes = FileManagerTools::getAcceptableFileTypes();
    $currentFileTypes = (request()->input('fileType')) ?
        explode_not_empty(',', request()->input('fileType')) :
        $acceptedFileTypes;
@endphp
<div id="filters" class="media-toolbar">
    <div class="toolbar-right col-xs-6">
        <div class="row">
            <div class="col-xs-6 pull-start">
                <select id="filter-file-type" class="attachment-filters full-width" name="fileType">
                    @if(count($currentFileTypes) > 1)
                        <option value="">{{ trans('filemanager::base.file-types.file.filter-title') }}</option>
                    @endif

                    @foreach($currentFileTypes as $type)
                        @if(in_array($type, $acceptedFileTypes))
                            <option value="{{ $type }}">
                                {{ trans("filemanager::base.file-types.$type.filter-title") }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 pull-start">
                <select class="attachment-filters full-width" name="sort">
                    <option value="time.desc" selected>
                        {{ trans('filemanager::base.sort.by.time') }}
                        -
                        {{ trans('filemanager::base.sort.order.descending') }}
                    </option>
                    <option value="time.asc">
                        {{ trans('filemanager::base.sort.by.time') }}
                        -
                        {{ trans('filemanager::base.sort.order.ascending') }}
                    </option>
                    <option value="name.asc">
                        {{ trans('filemanager::base.sort.by.name') }}
                        -
                        {{ trans('filemanager::base.sort.order.ascending') }}
                    </option>
                    <option value="name.desc">
                        {{ trans('filemanager::base.sort.by.name') }}
                        -
                        {{ trans('filemanager::base.sort.order.descending') }}
                    </option>
                    <option value="size.asc">
                        {{ trans('filemanager::base.sort.by.size') }}
                        -
                        {{ trans('filemanager::base.sort.order.ascending') }}
                    </option>
                    <option value="size.desc">
                        {{ trans('filemanager::base.sort.by.size') }}
                        -
                        {{ trans('filemanager::base.sort.order.descending') }}
                    </option>
                </select>
            </div>
        </div>
    </div>
    <div class="toolbar-left col-xs-6">
        <div class="row">
            <div class="col-xs-9 pull-start">
                <input typle="search" placeholder="{{ trans('filemanager::base.title.multimedia-search') }}"
                       class="search full-width" name="search"/>
            </div>
            <div class="col-xs-3 pull-start">
                <button type="button" class="btn btn-primary refresh btn-sm" onclick="refreshGallery()"><i class="fa fa-refresh"></i>
                </button>
            </div>
        </div>
    </div>
</div>