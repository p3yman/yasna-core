<div class="page uploader" id="upload" style="display: none;">
	<div class="col-xs-12">
		{!! $uploader->render() !!}

		@section('html_header')
			{!! $uploader->renderCss() !!}
		@append

		@section('html_footer')
			{!! $uploader->renderJs() !!}
		@append
	</div>
</div>