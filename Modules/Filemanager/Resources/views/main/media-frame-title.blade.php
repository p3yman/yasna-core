<div class="media-frame-title">
    <h1>
        <span class="fa fa-bars menu-icon"></span>
        {{ trans('filemanager::base.title.main') }}
    </h1>
</div>