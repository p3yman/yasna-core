<div class="file-list-view">
    @include('filemanager::layouts.widgets.loading-dialog')
    <div class="thumbnail-container">
        <ul class="" id="thumbnail">
            @include('filemanager::main.media-frame-content-gallery-images-list')
        </ul>
    </div>
</div>