<div class="media-frame-router">
    <div class="media-router">
        <a href="#" class="media-menu-item" data-page="upload" >
            {{ trans('filemanager::base.title.upload') }}
        </a>
        <a href="#" class="media-menu-item active" data-page="gallery" >
            {{ trans('filemanager::base.title.multimedia-library') }}
        </a>
    </div>
</div>