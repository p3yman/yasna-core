<div class="page attachments-browser" id="gallery">
    @include('filemanager::main.media-frame-content-gallery-toolbar')
    @include('filemanager::main.media-frame-content-gallery-list')
    @include('filemanager::main.media-frame-content-gallery-sidebar')
</div>