<li class="folder" data-instance="{{ $item['instance'] }}" data-key="{{ $item['key'] }}">
    <a href="#" class="link-darkGray">
        <span class="folder-icon fa fa-folder ml5"></span>
        {{ $item['title'] }}

    </a>
    @isset($item['children'])
        @include('filemanager::main.folder-menu-list', ['items' => $item['children']])
    @endisset
</li>
@section('acceptedFilesCategories')
    {{ $item['instance'] }}__{{ $item['key'] }}: {!! json_encode($item['acceptedFilesCategories']) !!},
@append
