@if(isset($files))
    @foreach($files as $file)
        @if($file->can('preview'))
            <li class=" col-xs-6 col-sm-6 col-md-4 col-lg-3 ">
                <div class="thumbnail attachment" data-url="{{ url($file->pathname) }}"
                     data-file="{{ $file->hashid }}"
                     data-pathname="{{ $file->pathname }}"
                     data-file-name="{{ $file->file_name }}">
                    {{--<img src="img/insurance-gambling-300x144.jpg" alt="">--}}
                    {!! fileManager()->fileToolsProvider()->getFileView($file, 'thumb') !!}
                </div>
            </li>
        @endif
    @endforeach
@endif
