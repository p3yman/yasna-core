<hr>
@php
	$configs = UploadTools::getAbsoluteUploadConfigs();
	$file_types = $configs['fileTypes'];

	$current_array = json_decode($model->default_value, true);
	$current_config = is_array($current_array) ? $current_array : $configs;
	$current_file_types = $current_config['fileTypes'];
@endphp

{!!
	widget('input')
		->name('uploadDir')
		->label(trans('filemanager::config.directory.title.singular'))
		->value($current_config['uploadDir'])
		->required()
		->inForm()
!!}

@foreach($file_types as $type => $info)
	@if (!($info['status'] ?? false))
		@continue
	@endif
	@php
		$panel_id = "$type-configs";
		$current_info = $current_file_types[$type] ?? [];
	@endphp

	<div class="col-xs-12" style="margin-bottom: 40px">
		<div class="row">
			<div class="col-xs-12 file-type-switch">
				@php $is_active = ($current_info['status'] ?? false) @endphp
				{!!
					widget('toggle')
						->label(trans("filemanager::base.file-types.$type.title"))
						->name("fileTypes[$type][status]")
						->value(1)
						->setExtra('data-type', $type)
						->setExtra('data-panel', "#$panel_id")
						->value($is_active)
				 !!}
			</div>


			<div class="col-xs-12">
				<div class="panel panel-default mv15" id="{{ $panel_id }}"
					 @if(!$is_active) style="display: none" @endif>
					<div class="panel-body">

						<div class="row">

							<div class="col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">
										{{ trans('filemanager::config.max_file_size.title') }}
									</label>
									@php
										$current_max_file_size = ($current_info['maxFileSize'] ?? '');
										$current_max_file_size_not_set = empty($current_max_file_size);
									@endphp
									<div class="col-sm-2 default-handler"
										 data-target="#{{ $type }}-max-file-size-options">
										{!!
											widget('checkbox')
												->label(trans('filemanager::config.default'))
												->class('default-toggle')
												->value($current_max_file_size_not_set)
												->name("fileTypes[$type][default][maxFileSize]")
										!!}
									</div>
									<div class="col-sm-8" id="{{ $type }}-max-file-size-options"
										 @if($current_max_file_size_not_set) style="display: none" @endif>
										{!!
											widget('input')
												->name("fileTypes[$type][maxFileSize]")
												->value($current_max_file_size)
												->addon(trans('filemanager::base.byte-units.MB'))
										!!}
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">
										{{ trans('filemanager::config.max_file_numbers.title') }}
									</label>
									@php
										$current_max_file_number = ($current_info['maxFiles'] ?? '');
										$current_max_file_number_not_set = empty($current_max_file_number);
									@endphp
									<div class="col-sm-2 default-handler" data-target="#{{ $type }}-max-files-options">
										{!!
											widget('checkbox')
												->label(trans('filemanager::config.default'))
												->class('default-toggle')
												->name("fileTypes[$type][maxFiles][default]")
												->value($current_max_file_number_not_set)
												->name("fileTypes[$type][default][maxFiles]")
										!!}
									</div>
									<div class="col-sm-8" id="{{ $type }}-max-files-options"
										 @if($current_max_file_number_not_set) style="display: none;" @endif>
										{!!
											widget('input')
												->name("fileTypes[$type][maxFiles]")
												->value($current_max_file_number)
												->addon(trans('filemanager::config.max_file_numbers.unit'))
										!!}
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">
										{{ trans('filemanager::config.extension.title.plural') }}
									</label>
									@php
										$current_extensions = ($current_info['acceptedExtensions'] ?? []);
										$current_extensions_not_set = empty($current_extensions);
									@endphp
									<div class="col-sm-2 default-handler" data-target="#{{ $type }}-extensions-options">
										{!!
											widget('checkbox')
												->label(trans('filemanager::config.default'))
												->class('default-toggle')
												->value($current_extensions_not_set)
												->name("fileTypes[$type][default][acceptedExtensions]")
										!!}
									</div>
									<div class="col-sm-8" id="{{ $type }}-extensions-options"
										 @if($current_extensions_not_set) style="display: none" @endif>
										@foreach($info['acceptedExtensions'] as $extension)
											{!!
												widget('checkbox')
													->label($extension)
													->name("fileTypes[$type][acceptedExtensions][$extension]")
													->value(in_array($extension, $current_extensions))
											!!}
										@endforeach
									</div>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">
										{{ trans('filemanager::config.mime_type.title.plural') }}
									</label>
									@php
										$current_mime_types = ($current_info['acceptedFiles'] ?? []);
										$current_mime_types_not_set = empty($current_mime_types);
									@endphp
									<div class="col-sm-2 default-handler" data-target="#{{ $type }}-mime-types">
										{!!
											widget('checkbox')
												->label(trans('filemanager::config.default'))
												->class('default-toggle')
												->value($current_mime_types_not_set)
												->name("fileTypes[$type][default][acceptedFiles]")
										!!}
									</div>
									<div class="col-sm-8" id="{{ $type }}-mime-types"
										 @if($current_mime_types_not_set) style="display: none" @endif>
										@foreach($info['acceptedFiles'] as $mime_type)
											{!!
												widget('checkbox')
													->label($mime_type)
													->name("fileTypes[$type][acceptedFiles][$mime_type]")
													->value(in_array($mime_type, $current_mime_types))
											!!}
										@endforeach
									</div>
								</div>
							</div>


							@if (UploadTools::isVersionable($type))
								@php $version_template_id = "$type-version-template" @endphp
								@php $current_extra_files = ($current_info['extraFiles'] ?? []) @endphp
								<div class="col-xs-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">
											{{ trans('filemanager::config.version.title.plural') }}
										</label>
										<div class="col-sm-10">
											@include("filemanager::upstream.config.edit-$type-version-template")
											@if (count($current_extra_files))
												@foreach($current_extra_files as $title => $info)
													@include("filemanager::upstream.config.edit-$type-version-template-inner", [
														'version_title' => $title,
														'version_info' => $info,
														'version_index' => array_search($title, array_keys($current_extra_files)),
													])
												@endforeach
											@else
												@include("filemanager::upstream.config.edit-$type-version-template-inner")
											@endif
											<div class="row mv10 versions-control">
												<div class="col-xs-12">
													<button class="btn btn-primary btn-xs add-version"
															data-template="#{{ $version_template_id }}" type="button">
														<i class="fa fa-plus" style="margin-left: 0"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endforeach