@php $version_template_id = ($version_template_id ?? "image-version-template") @endphp
<script id="{{ $version_template_id }}" type="text/template">
	@include('filemanager::upstream.config.edit-image-version-template-inner')
</script>
