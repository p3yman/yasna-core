{!! widget('hidden')->name('id')->value($model->spreadMeta()->hashid) !!}
{!!
	widget('input')
		->id('txtSlug')
		->name('slug')
		->class('ltr form-default')
		->required()
		->value(str_before($model->slug, '_' . ConfigTools::getUploadRelatedInputsPostfix()))
		->help(trans('validation.hint.unique').' | '.trans('validation.hint.english-only'))
		->setExtra((ConfigTools::isRequired($model) ? 'readonly' : '') , '""')
		->inForm()
!!}

{!!
	widget('input')
		->name('title')
		->value($model->title)
		->required()
		->help(trans('validation.hint.unique').' | '.trans('validation.hint.persian-only'))
		->inForm()
!!}

{!! widget('textarea')->name('hint')->value($model->hint)->inForm() !!}