@php $index = ($version_index ?? '__INDEX__' ) @endphp
<div class="row mv10 version-item" data-index="{{ $index }}">
	<div class="col-sm-5">
		{!!
			widget('input')
				->placeholder(trans('filemanager::config.version.option.title'))
				->name("fileTypes[$type][extraFiles][$index][title]")
				->value($version_title ?? '')
		!!}
	</div>
	<div class="col-sm-3">
		{!!
			widget('input')
				->placeholder(trans('filemanager::config.version.option.width'))
				->name("fileTypes[$type][extraFiles][$index][width]")
				->value($version_info['width'] ?? '')
		!!}
	</div>
	<div class="col-sm-3">
		{!!
			widget('input')
				->placeholder(trans('filemanager::config.version.option.height'))
				->name("fileTypes[$type][extraFiles][$index][height]")
				->value($version_info['height'] ?? '')
		!!}
	</div>
	<div class="col-sm-1">
		<div class="row">
			<button class="btn btn-danger btn-xs remove-version" type="button" style="margin-top: 5px">
				<i class="fa fa-times" style="margin-left: 0"></i>
			</button>
		</div>
	</div>
</div>