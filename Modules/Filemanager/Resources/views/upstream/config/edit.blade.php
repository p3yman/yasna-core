@include("manage::layouts.modal-start" , [
	'form_url' => route('fileManager.upstream.configs.save'),
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">
    @include('filemanager::upstream.config.edit-settings')

    @include('filemanager::upstream.config.edit-data')

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" )
</div>

@include("manage::layouts.modal-end")
