@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => route('fileManager.upstream.configs.action', ['action' => 'row', 'hash_id' => $model->hashid]),
])

@php $isRequired = ConfigTools::isRequired($model) @endphp

{{--
|--------------------------------------------------------------------------
| Title
|--------------------------------------------------------------------------
|
--}}

<td>
    @include("manage::widgets.grid-text" , [
        'text' => $model->title,
        'size' => "16" ,
        'class' => "font-yekan text-bold" ,
        'link' =>  "modal:" . route('fileManager.upstream.configs.action', ['action' => 'edit', 'hash_id' => '-hashid-'], false),
    ])
</td>

{{--
|--------------------------------------------------------------------------
| Slug
|--------------------------------------------------------------------------
|
--}}

<td>
    @include("manage::widgets.grid-text" , [
        'text' => str_before($model->slug, '_' . ConfigTools::getUploadRelatedInputsPostfix()),
        'size' => "12" ,
    ])
</td>

{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}
@include("manage::widgets.grid-actionCol" , [
	"actions" => [
        [
            'pencil',
            trans('manage::forms.button.edit'),
            "modal:" . route('fileManager.upstream.configs.action', ['action' => 'edit', 'hash_id' => '-hashid-'], false)
        ], [
		    'trash-o',
		    trans('manage::forms.button.soft_delete'),
		    "modal:" . route('fileManager.upstream.configs.action', ['action' => 'activeness', 'hash_id' => '-hashid-'], false),
		    !$isRequired and !$model->trashed()
        ],
		[
		    'recycle',
		    trans('manage::forms.button.undelete'),
		    "modal:" . route('fileManager.upstream.configs.action', ['action' => 'activeness', 'hash_id' => '-hashid-'], false),
		    !$isRequired and $model->trashed()
        ],
		[
		    'times',
		    trans('manage::forms.button.hard_delete'),
		    "modal:" . route('fileManager.upstream.configs.action', ['action' => 'destroy', 'hash_id' => '-hashid-'], false),
		    !$isRequired and $model->trashed()
        ],
	]
])
