@extends('manage::layouts.template')

@section('content')
	@include("manage::upstream.tabs")

	@include("manage::widgets.toolbar" , [
		'buttons' => [
			[
				'target' => 'modal:' . route('fileManager.upstream.configs.action', ['action' => 'create'], false),
				'type' => "success",
				'caption' => trans('manage::forms.button.add'),
				'icon' => "plus-circle",
			],
		],
		'search' => [
			'target' => request()->url() ,
			'label' => trans('manage::forms.button.search') ,
			'value' => isset($keyword) ? $keyword : '' ,
		],
	])

	@include('filemanager::upstream.config.grid')
@endsection

@section('html_footer')
	{!! Html::script(Module::asset('filemanager:js/file-manager-config.min.js')) !!}
@append