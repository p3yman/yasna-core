@include("manage::widgets.grid" , [
    'table_id' => "tblConfigs",
    'row_view' => "filemanager::upstream.config.row",
    'handle' => "counter",
    'headings' => [
        trans('validation.attributes.title'),
        trans('validation.attributes.slug'),
        trans('manage::forms.button.action'),
    ],
])