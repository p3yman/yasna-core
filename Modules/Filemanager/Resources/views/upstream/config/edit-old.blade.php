@include("manage::layouts.modal-start" , [
	'form_url' => route('fileManager.upstream.configs.save'),
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">
    @include('manage::forms.hidden' , [
        'name' => 'id' ,
        'value' => $model->spreadMeta()->hashid,
    ])

    @include('manage::forms.input' , [
        'id' => "txtSlug" ,
        'name' =>	'slug',
        'class' =>	'form-required ltr form-default',
        'value' =>	str_before($model->slug, '_' . ConfigTools::getUploadRelatedInputsPostfix()),
        'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.english-only'),
        'extra' => ConfigTools::isRequired($model) ? 'readonly' : '',
    ])

    @include('manage::forms.input' , [
        'name' =>	'title',
        'value' =>	$model,
        'class' => 'form-required' ,
        'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.persian-only'),
    ])

    @include('manage::forms.textarea' , [
        'name' => 'default_value',
        'label' => trans("validation.attributes.value"),
        'value' =>	$model->default_value,
        'class' => 'form-required' ,
    ])

    @include('manage::forms.textarea' , [
        'name' => 'hint',
        'value' =>	$model->hint,
    ])

    {{--
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    --}}

    @include("manage::forms.buttons-for-modal" )

</div>

@include("manage::layouts.modal-end")