<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>File Maneger</title>
    {!! Html::style(Module::asset('filemanager:css/fontiran.css')) !!}
    {!! Html::style(Module::asset('filemanager:css/file-manager.min.css')) !!}
    {!! Html::style(Module::asset('filemanager:css/whirl.css')) !!}
    {!! Html::script(Module::asset('filemanager:js/dropzone.min.js')) !!}
    @yield('html_header')
</head>
