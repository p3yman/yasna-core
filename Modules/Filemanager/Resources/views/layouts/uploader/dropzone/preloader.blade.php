@php
    $uploaderClass = UploadTools::getUploaderProvider();
    $freshConfigs = $uploaderClass::getDefaultJsConfigs();
@endphp

@section('html_header')
    {!! Html::style(Module::asset('filemanager:css/dropzone.min.css')) !!}

    @if($additionalStyles)
        @if(is_string($additionalStyles))
            @php $additionalStyles = [$additionalStyles]; @endphp
        @endif

        @if(is_array($additionalStyles) and count($additionalStyles))
            @foreach($additionalStyles as $additionalStyle)
                {!! Html::style(Module::asset("filemanager:css/$additionalStyle.min.css")) !!}
            @endforeach
        @endif
    @endif

    <style>
        .dz-hidden-input { /* this is in body */
            direction: ltr !important;
        }
    </style>
@append

@section('html_footer')
    {!! Html::script(Module::asset('filemanager:js/dropzone.min.js')) !!}
    <script>
        // Setting Dynamic Variables
        var csrfToken = "{{ csrf_token() }}";
        var dropzoneRoutes = {
            images: "{{ url('assets/images') }}",
            upload: "{{ route('uploadFile.uploadFile') }}",
            remove: "{{ route('uploadFile.removeFile') }}",
        };
        var messages = {
            errors: {
                size: "{{ trans('filemanager::upload.error.size') }}",
                type: "{{ trans('filemanager::upload.error.type') }}",
                server: "{{ trans('filemanager::upload.error.server') }}",
                limit: "{{ trans('filemanager::upload.error.limit') }}",
            },
            statuses: {
                uploading: "{{ trans('filemanager::upload.status.uploading') }}",
                failed: "{{ trans('filemanager::upload.status.failed') }}",
                success: "{{ trans('filemanager::upload.status.success') }}",
            }
        };
        let fileInfoElTmp = $('<div class="media-uploader-status"> <div class="col-sm-9 col-xs-8"> <div class="media-uploader-status-info"> <h2 class="media-uploader-status-text"> </h2> <button type="button" class="fa fa-times-circle delete-upload-info"></button> <div class="media-progress"> <div class="media-progress-value"></div> </div> <div class="upload-detail"> <span class="upload-detail-separator"></span> <span class="upload-filename"></span> </div> </div> </div> <div class="col-sm-3 col-xs-4"> <div class="attachment media-uploader-status-image"> <img> </div> </div></div>');
    </script>
    {!! Html::script(Module::asset('filemanager:js/dropzone-additives.min.js')) !!}
    <script>
        Dropzone.prototype.defaultOptions.init = function () {
            var dropzoneObj = this;
            $.each(dropzoneOptions.init, function (key, value) {
                dropzoneObj.on(key, value);
            });

            @if(array_key_exists('events', $freshConfigs))
            @foreach($freshConfigs['events'] as $eventName => $eventValue)
            dropzoneObj.on("{{ $eventName }}", {{ $eventValue }});
            @endforeach

            @unset($freshConfigs['events'])
            @endif
        };

        @foreach($freshConfigs as $configKey => $configValue)
            Dropzone.prototype.defaultOptions.{{ $configKey }} = "{{ $configValue }}";
        @endforeach
    </script>
@append