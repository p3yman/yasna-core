@if ($withHtml ?? false)
	<div class="dropzone mb15 optional-input uploader-container {{ $class }}" id="{{ $id }}"
		 data-var-name="{{ $varName }}"
		 @if(isset($dataAttributes) and is_array($dataAttributes) and count($dataAttributes))
		 @foreach($dataAttributes as $fieldTitle => $filedValue)
		 data-{{ $fieldTitle }}="{{ $filedValue }}"
		 @endforeach
		 @endif
		 @if(isset($resultSelector) and $resultSelector)
		 data-target="{{ $resultSelector }}"
			@endif
	>
		<div class="dz-message" data-dz-message>
			<i class="fa fa-cloud-upload upload-icon f70 text-white"></i>
			@isset($top_title)
				<br>
				<span>{{ $top_title }}</span>
			@endisset
			<br/>
			@if(isset($uploadConfig['icon']) and ($boxIconName = $uploadConfig['icon']))
				<i class="fa fa-{{ $boxIconName }}"></i> &nbsp;
			@endif
			<span>{{ trans("filemanager::base.file-types.$fileType.uploader-text") }}</span>
			@isset($bottom_title)
				<br>
				<span>{{ $bottom_title }}</span>
			@endisset
		</div>
		@include('filemanager::layouts.forms.hidden', [
			'id' => '_uploadIdentifier',
			'name' => '_uploadIdentifier',
			'value' => encrypt(implode(',', $uploadIdentifiers)),
		])
		@if($configName == '__current__')
			@php $configName = UploadTools::getCurrentConfigSlug() @endphp
		@endif
		@include('filemanager::layouts.forms.hidden', [
			'id' => '_configName',
			'name' => '_configName',
			'value' => $configName,
		])
		@include('filemanager::layouts.forms.hidden', [
			'id' => '_groupName',
			'name' => '_groupName',
			'value' => time() . str_random(8),
		])
		@include('filemanager::layouts.forms.hidden', [
			'id' => '_directUpload',
			'name' => '_directUpload',
			'value' => $directUpload,
		])
		@if(!isset($externalFields) or !$externalFields or !is_array($externalFields))
			@php $externalFields = [] @endphp
		@endif
		@include('filemanager::layouts.forms.hidden', [
			'id' => '_externalFields',
			'name' => '_externalFields',
			'value' => json_encode($externalFields),
		])
	</div>

	@if ($progressesUnder)
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
				<div class="row files-uploading-status" data-var-name="{{ $varName }}">
				</div>
			</div>
		</div>
	@endif
@endif