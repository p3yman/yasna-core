@if ($generalCss ?? false)
	{!! Html::style(Module::asset('filemanager:css/dropzone.min.css')) !!}

	<style>
		.dz-hidden-input { /* this is in body */
			direction: ltr !important;
		}
	</style>
@endif
