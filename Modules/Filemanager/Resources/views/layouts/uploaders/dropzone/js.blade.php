@if ($withJs ?? false)
	<script>
        var {{ $varName }};
        $(document).ready(function () {
			{{ $varName }} = new Dropzone("#{{ $id }}", {!! json_encode($js_configs ?? []) !!});

			@if($progressesUnder)
				{{ $varName }}.on('sending', function (file, xhr, formData) {
                	var dropzoneSelf = this;

                	// Hide element in dropzone
		            $(file.previewElement).hide();
		            $(file.previewElement).closest('.uploader-container').find('.dz-message').show();

					// Show File Progress Info
					var fileInfoEl = fileInfoElTmp.clone();

					fileInfoEl.find('.media-uploader-status-text').html(messages.statuses.uploading);
					fileInfoEl.find('.upload-filename').html(file.name);
					fileInfoEl.find('.delete-upload-info').click(function () {
						dropzoneSelf.removeFile(file);
					});
					var fileInfoImg = fileInfoEl.find('.media-uploader-status-image').children('img');
					fileInfoImg.attr('src', dropzoneRoutes.images + '/template/file-o.svg');

					var image = new Image();
					image.onload = function (e) {
						fileInfoImg.attr('src', image.src)
					};
					image.src = URL.createObjectURL(file);

					$('.files-uploading-status').append(fileInfoEl);
					file.uploadResultElement = fileInfoEl;
				});

				{{ $varName }}.on('uploadprogress', function (file, progress, bytesSent) {
					if (progress < 100) {
						file.uploadResultElement.find('.media-progress-value').width(progress + '%');
					}
				});

				{{ $varName }}.on('success', function (file) {
                	file.uploadResultElement.find('.media-uploader-status-text').html(messages.statuses.success);
				});

				{{ $varName }}.on('complete', function (file) {
                	file.uploadResultElement.find('.media-progress-value').width('100%');
				});

				{{ $varName }}.on('error', function (file, response, xhr) {
					$(file.previewElement).find('.dz-error-message').remove();
					let uploadResultElement = file.uploadResultElement.find('.media-uploader-status-info');

					if (xhr.status == 422) {
						let errorsContainer = $('<div class="upload-errors"></div>');
						let ul = $('<ul></ul>');
						if (response.file) {
							$.each(response.file, function (index, error) {
								ul.append($('<li></li>').html(error));
							});
						}
						errorsContainer.append(ul);
						uploadResultElement.append(errorsContainer);

					}

					uploadResultElement.addClass('error');
					uploadResultElement.find('.media-uploader-status-text').html(messages.statuses.failed);
				});
			@elseif($progressesInside)
				{{ $varName }}.on('error', function (file, response, xhr) {

					if (xhr.status == 422) {
						let errorContainer = $(file.previewElement).find('.dz-error-message');
						let ul = $('<ul></ul>');
						if (response.file) {
							$.each(response.file, function (index, error) {
								ul.append($('<li></li>').html(error));
							});
						}
                        errorContainer.html(ul);
					}
				});
			@endif

			@if(isset($callbackOnEachUploadComplete) and $callbackOnEachUploadComplete)
				{{ $varName }}.on("complete", function (file) {
					{{ $callbackOnEachUploadComplete }}(file, this);
				});
			@endif

			@if(isset($callbackOnEachUploadSuccess) and $callbackOnEachUploadSuccess)
				{{ $varName }}.on("success", function (file) {
					{{ $callbackOnEachUploadSuccess }}(file, this);
				});
			@endif

			@if(isset($callbackOnEachUploadError) and $callbackOnEachUploadError)
				{{ $varName }}.on("error", function (file) {
					{{ $callbackOnEachUploadError }}(file, this);
				});
			@endif

			@if(isset($callbackOnQueueComplete) and $callbackOnQueueComplete)
				{{ $varName }}.on("queuecomplete", function () {
					{{ $callbackOnQueueComplete }}(this.getAcceptedFiles(), this.files, this);
				});
			@endif


			@if($resultSelector)
				if($('{{ $resultSelector }}').length) {
					{{ $varName }}.on("success", function (file) {
						updateTarget(this, "{{ $resultSelector }}");
					});
					{{ $varName }}.on("removedfile", function (file) {
						updateTarget(this, "{{ $resultSelector }}");
					});
				}
			@endif

			@if($ajaxRemove)
				{{ $varName }}.on("removedfile", function (file) {
                	removeFromServer(file, $(this.element));
				});
			@endif


			@if(isset($events) and $events and is_array($events))
				@foreach($events as $eventName => $eventValue)
					{{ $varName }}.on("{{ $eventName }}", {!! $eventValue !!});
				@endforeach
			@endif

            // Clear predefined data in hidden inputs after refresh
            $.each(Dropzone.instances, function (index, obj) {
                updateTarget(this, $(this.element).attr('data-target'));
            });
        });
	</script>
@endif
