{{-- start -- uploader form --}}
@php
	$formData = [
			'method'=> 'post',
			'class' => "dropzone mb15 optional-input text-blue",
		];

	if (isset($progressesPosition)) {
		$progressesUnder = $uploaderClass::progressesPositionIsEqualToConstant($progressesPosition, 'PROGRESSES_UNDER');
		$progressesInside = $uploaderClass::progressesPositionIsEqualToConstant($progressesPosition, 'PROGRESSES_INSIDE');
	}
@endphp

@include('filemanager::layouts.uploaders.dropzone.general-css')
@include('filemanager::layouts.uploaders.dropzone.box')
@include('filemanager::layouts.uploaders.dropzone.general-js')
@include('filemanager::layouts.uploaders.dropzone.js')