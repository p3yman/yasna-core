<div id="loading-dialog" class="loading-dialog text-center text-blue pt30" style="display: none">
    <p>
        {{ trans('filemanager::base.message.loading') }}
    </p>
    <p class="whirl text-center mt30"></p>
    {{--<img src="{{ Module::asset('filemanager:image/preloader-circle-small.gif') }}">--}}
</div>