@php
    /**
    |--------------------------------------------------------------------------
    | Style
    | (switches: style, width and height)
    |--------------------------------------------------------------------------
    |
    **/

    if(is_string($style)) {
        $styleParts = explode_not_empty(';', $style);
        $styleArr = [];
        foreach ($styleParts as $key => $styleItem) {
            $itemParts = explode_not_empty(':', $styleItem);
            if(
                (count($itemParts)== 2)  and
                ($itemParts[0] = trim($itemParts[0])) and
                ($itemParts[1] = trim($itemParts[1]))
            ) {
                $styleArr[$itemParts[0]] = $itemParts[1];
            }
        }
        $style = $styleArr;
    }

    if(!is_null($width)) {
        $style['width'] = (is_numeric($width)) ? ($width . 'px') : $width;
    }

    if(!is_null($height)) {
        $style['height'] = (is_numeric($height)) ? ($height . 'px') : $height;
    }

    $styleString = '';
    foreach ($style as $attributeName => $attributeValue) {
        $styleString .= $attributeName .': ' . $attributeValue . ';';
    }
    $style = $styleString;

    /**
    |--------------------------------------------------------------------------
    | Class
    | (switch: class)
    |--------------------------------------------------------------------------
    |
    **/
    if(is_array($class)) {
        $class = implode(' ', $class);
    }

    if(!$fileExisted) {
        $class .= ' not-found';
    }
@endphp

<img src="{{ $imgUrl }}" style="{{ $style }}"
    @if($class) class="{{ $class }}" @endif
    @if(isset($dataAttributes) and is_array($dataAttributes))
         @foreach($dataAttributes as $attributeName => $attributeValue)
         data-{{ $attributeName }}="{!! $attributeValue !!}"
    @endforeach
    @endif
    @if(isset($otherAttributes) and is_array($otherAttributes))
        @foreach($otherAttributes as $attributeName => $attributeValue)
            {{ $attributeName }}="{!! $attributeValue !!}"
        @endforeach
    @endif
    @if($extra) {{ $extra }} @endif
/>