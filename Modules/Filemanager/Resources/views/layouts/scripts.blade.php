<script>
    var lang = {!! json_encode(getJsTrans()) !!};
    var routes = {
        main: "{{ route('fileManager.index') }}",
        preview: "{{ route('fileManager.preview') }}",
        getList: '{{ route('fileManager.getList') }}',
        getFileDetails: '{{ route('fileManager.file.getFileDetails') }}',
        setFileDetails: '{{ route('fileManager.file.setFileDetails') }}',
        deleteFile: '{{ route('fileManager.file.deleteFile') }}',
        restoreFile: '{{ route('fileManager.file.restoreFile') }}',
    };
    window.fileManagerModalOptions = {!! json_encode(request()->input()) !!}
</script>

{!! Html::script('https://use.fontawesome.com/f4fcfb493d.js') !!}
{!! Html::script(Module::asset('filemanager:js/jquery-3.2.1.min.js')) !!}
{!! Html::script(Module::asset('filemanager:js/jquery-ui.min.js')) !!}
{!! Html::script(Module::asset('filemanager:js/bootstrap.min.js')) !!}
{!! Html::script(Module::asset('filemanager:js/tools.min.js')) !!}
{!! Html::script(Module::asset('filemanager:js/timer.min.js')) !!}
{!! Html::script(Module::asset('filemanager:js/file-manager.min.js')) !!}