<div class="modal fade file-manager-modal" id="file-manager-modal" role="dialog">

    {{--
    |--------------------------------------------------------------------------
    | File Manager Modal
    |--------------------------------------------------------------------------
    |
    --}}

    <div class="modal-dialog">
        <div class="modal-content">
            <button class="btn-close" data-dismiss="modal">
                <span class="fa fa-times"></span>
            </button>
            <div class="modal-body">
                <iframe class="file-manager-iframe" frameborder="0"
                        data-url="{{ route('fileManager.index') }}"></iframe>
            </div>
        </div>

    </div>
</div>