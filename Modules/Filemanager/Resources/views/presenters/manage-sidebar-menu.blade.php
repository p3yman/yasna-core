@if ($file_manager_is_available = FileManagerTools::fileManagerIsAvailable())
	@include('manage::layouts.sidebar-link' , [
		'icon'       => "folder",
		'caption' => trans('filemanager::base.title.main') ,
		'link' => "#" ,
		'id' => 'file-manager-sidebar-menu',
		'testVar' => 'ss',
	])
@endif

@section('html_footer')
	@if ($file_manager_is_available)
		<script>
            $(document).ready(function () {
                let fileManagerSideBar = $('#file-manager-sidebar-menu');
                if (fileManagerSideBar.length) {
                    let fileManagerSideBarLink = fileManagerSideBar.children('a');
                    if (fileManagerSideBarLink.length) {
                        fileManagerSideBarLink.attr('href', '#');
                        fileManagerSideBar.click(function (e) {
                            e.preventDefault();
                        });
                        fileManagerSideBarLink.fileManagerModal();
                    }
                }
            });
		</script>
	@endif
@append
