@if($model->posttype->hasFeature('attachments') and !is_null($model->posttype->getUploadConfigs('attachments')))
	<div class="panel panel-violet mv20">
		{{--
		|--------------------------------------------------------------------------
		| Heading
		|--------------------------------------------------------------------------
		|
		--}}
		<div class="panel-heading">
			<i class="fa fa-address-book-o mh5"></i>
			{{ trans('filemanager::other-modules.posts.attachment-files') }}
			&nbsp;(<span id="spnFileCount">{{ pd(count($model->files)) }}</span>)
		</div>

		<div class="panel-body">
			{{--
			|--------------------------------------------------------------------------
			| Already uploaded photos
			|--------------------------------------------------------------------------
			|
			--}}
			<div id="divCurrentFiles">
				@foreach($model->files as $key => $file)
					@include('filemanager::presenters.post-editor-attachments-uploader-one-file' , [
						'key' => $key ,
						'src' => $file['src'] ,
						'label' => $file['label'] ,
						'link' => isset($file['link'])? $file['link'] : '',
					])
				@endforeach
			</div>
			<input type="hidden" id="txtLastKey" value="{{$key or 0}}">


			{{--
			|--------------------------------------------------------------------------
			| New Files
			|--------------------------------------------------------------------------
			|
			--}}
			<div id="divNewFiles" class="m10 text-center"
				 data-src="{{ route("fileManager.manage.posts.editor.attachments.item.new", ['hashid' => ''], false) }}/"
				 data-type="append">
				@include("filemanager::presenters.post-editor-attachments-uploader-new-mess")
			</div>

			{{--
			|--------------------------------------------------------------------------
			| Uploader
			|--------------------------------------------------------------------------
			|
			--}}

			@php $generalCss = fileManager()->uploader()->renderGeneralCss() @endphp
			@section('html_header')
				{!! Html::style(Module::asset("filemanager:css/manage-file-manager-uploader.min.css")) !!}
				{!! $generalCss !!}
			@append

			@php
				$uploader = fileManager()
						->uploader($model->posttype)
						->jsVariableName('dropzone_object')
						->posttypeConfig('attachments')
						->onQueueComplete('filemanagerUploadFinish')
						->progressesUnder()
						->withoutAjaxRemove()
						;
			@endphp
			<div class="row">
				{!! $uploader->render()!!}
			</div>

			@section('html_header')
				{!! $uploader->renderCss() !!}
			@append

			@php $generalJs = fileManager()->uploader()->renderGeneralJs() @endphp
			@section('html_footer')
				{!! $uploader->renderJs() !!}
				{!! $generalJs !!}
			@append
		</div>
	</div>

@section('html_footer')
	<script>
        function filemanagerUploadFinish(uploaded_files) {
            let $last_key_input = $("#txtLastKey");

            while (uploaded_files.length) {
                let counter = 0;
                let result  = $last_key_input.val() + "-";
                let current = uploaded_files.splice(0, 50);
                $.each(current, function (index, item) {
                    switch (item.xhr.status) {
                        case 200:
                            result += $.parseJSON(item.xhr.response).file + "-";
                            break;

                        case 422:
                            let rs = $.parseJSON(item.xhr.response);
                            if (rs.file) {
                                let errorMessage = $('<ul></ul>');
                                $.each(rs.file, function (key, error) {
                                    errorMessage.append($('<li></li>').html(error));
                                });
                                $.notify(errorMessage.html(), {status: 'danger', pos: 'bottom-left'});
                            }
                            break;
                    }
                    counter++;
                });

                $last_key_input.val(parseInt($last_key_input.val()) + counter);

                divReload('divNewFiles', result);
            }

            refreshDropzone(dropzone_object);
        }

        function postFileCounterUpdate(update) {
            let $selector     = $('#spnFileCount');
            let current_value = parseInt(forms_digit_en($selector.html()));

            forms_log(current_value);

            if (update == '-') {
                $selector.html(forms_digit_fa(Math.max(0, current_value - 1).toString()));
            }
            else if (update == '+') {
                $selector.html(forms_digit_fa((current_value + 1).toString()));
            }
            else {
                $selector.html(forms_digit_fa(update));
            }
        }

        function postPhotoRemoved(key, hashid) {
            let div_id = "divPhoto-" + key;

            divReload(div_id);

            postFileCounterUpdate('-')
        }
	</script>
@append
@endif