@if($file = model('file' , $src) and $file->id)
    <div id="divPhoto-{{$key}}" class="row w100 m10 p10 {{$class or ''}}" -style="height: 100px"
         data-src="{{ route('fileManager.manage.posts.editor.attachments.item.remove', ['hashid' => $file->hashid], false) }}">


        <div class="col-md-3 text-center">
            {{--<img src="{{ url($file->pathname) }}" style="margin-top:15px;max-height:100px;max-width: 100px">--}}
            {!!  fileManager()->fileToolsProvider()->getFileView($file , 'thumbnail' , [
                'style' => "max-width:100%;max-height:150px" ,
            ]) !!}
        </div>

        <div class="col-md-8 text-center">
            <label class="ltr f10 pull-left text-info" style="margin-top: 5px">{{ $file->file_name }}</label>
            <input name="_files[{{$key}}][label]" value="{{$label or ''}}" class="-label form-control text-center"
                   placeholder="{{trans('filemanager::validation.attributes_placeholder.file_title')}}"
                   style="margin-top: 5px">
            <input name="_files[{{$key}}][link]" value="{{$link or ''}}" class="-label form-control text-center ltr"
                   placeholder="{{trans('filemanager::validation.attributes_placeholder.source_link')}}"
                   style="margin-top:5px">
            <input type="hidden" name="_files[{{$key}}][src]" value="{{ $file->hash_id }}" class="-src form-control">
            <button type="button" class="btn btn-link" onclick="postPhotoRemoved('{{$key}}')">
				<span class="text-danger">
					<i class="fa fa-remove"></i>
                    {{ trans('filemanager::base.menu.delete') }}
				</span>
            </button>
        </div>
        <div class="col-md-1 text-center">
        </div>
    </div>
@endif
