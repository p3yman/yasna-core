@php
    $defaultClass = 'ltr clickable text-grey italic';

    if (isset($class)) {
        if (str_contains($class, 'form-required')) {
            $required = true;
        }
        $class .= ' ' . $defaultClass;
    } else {
        $class = $defaultClass;
    }

    if (!isset($disabled))
        $disabled = false;

    if ($disabled) {
        $required = false;
    }

    if (isset($id) and $id) {
        $input_id = $id;
    } else {
        $input_id = "file-$name";
    }

    $btn_id = 'btn-' . $input_id;

@endphp

@php $post_model_class = get_class(post()) @endphp
@php $posttype_model_class = get_class(posttype()) @endphp
@if(!isset($posttype) or !($posttype instanceof $posttype_model_class))
    @if (
		isset($model) and
		($model instanceof $post_model_class) and
		($posttype_tmp = $model->posttype)
	)
        @php $posttype = $posttype_tmp @endphp
    @else
        @php $posttype = posttype() @endphp
    @endif
@endif

@if(!isset($condition) or $condition)
    <div class="form-group {{ (isset($container['class']) ? $container['class'] : '') }}"
    {{ isset($container['id']) ? "id=$container[id]" : '' }}
    @if(isset($container['other']))
        @foreach($container['other'] as $attrName => $attrValue)
            {{ $attrName }}="{{ $attrValue }}"
        @endforeach
    @endif
    >

    @if(!isset($label))
        @php $label = Lang::has("validation.attributes.$name") ? trans("validation.attributes.$name") : $name @endphp
    @endif

    @if($label)
        <label
                for="{{$name}}"
                class="col-sm-2 control-label {{$label_class or ''}}"
        >
            {{ $label }}
            @if(isset($required) and $required)
                <span class="fa fa-star required-sign " title="{{trans('manage::forms.logic.required')}}"></span>
            @endif
        </label>

        <div class="col-sm-10">
            @else
                <div class="col-sm-12">
                    @endif

                    <div class="row">
                        @if(!$disabled)
                            <div class="col-md-2">
                                @php $callbackValue = "downstreamPhotoSelected('#$input_id'); " @endphp
                                @php $callbackValue .= "$('#$input_id-prev').val('__fileName__');" @endphp
                                <button id="{{ $btn_id }}" type="button" data-file-manager-input="{{ $input_id }}"
                                        data-file-manager-callback="{{ $callbackValue }}"
                                        data-file-manager-output-type="hashid"
                                        data-file-manager-config-name="{{ $config_name or '' }}"
                                        @if ($posttype->exists)
                                            data-file-manager-default-folder="posttype___{{ $posttype->hashid }}"
                                        @endif
                                        class="btn btn-default btn-sm btn-block">
                                    {{ trans('filemanager::other-modules.posts.browse-file') }}
                                </button>
                            </div>
                        @endif
                        <div class="col-md-{{$disabled?'12':'10'}}">
                            @php $currentFile = fileManager()->fileToolsProvider()->smartFindFile($value) @endphp
                            {{--<input id="{{ $input_id }}" type="text" name="{{ $name }}" value="{{ $value or ''  }}"--}}
                            {{--readonly--}}
                            {{--class="form-control ltr clickable text-grey italic"--}}
                            {{--onclick="downstreamPhotoPreview('#{{ $input_id }}')">--}}
                            @include('manage::forms.input-self', [
                                'id' => $input_id . '-prev',
                                'name' => '',
                                'value' => ($currentFile->exists) ? $currentFile->file_name : '',
                                'extra' => 'readonly'
                            ])
                            @include('manage::forms.hidden', [
                                'id' => $input_id,
                            ])
                            @if(!$disabled)
                                <i class="fa fa-times text-grey clickable"
                                   style="position: relative;top:-25px;left:-10px"
                                   onclick="$('#{{$input_id}}').val(''); $('#{{$input_id}}-prev').val('')"></i>
                            @endif
                        </div>
                    </div>
                </div>
        </div>

        <script>
            $('#{{ $btn_id }}').fileManagerModal('Files', {
                prefix: "{{ route('fileManager.index') }}",
            });
        </script>
    @endif
