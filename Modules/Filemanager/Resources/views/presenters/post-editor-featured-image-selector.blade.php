@if($model->posttype->hasFeature('featured_image') and !is_null($model->posttype->getUploadConfigs('featured_image')))
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('filemanager::validation.attributes.featured_image') }}</div>
        <div class="panel-body text-center">
            <button type="button" id="btn-featured-image" data-file-manager-input="txt-featured-image"
                    data-file-manager-preview="div-featured-image-inside"
                    data-file-manager-callback="featuredImage('inserted')"
                    data-file-manager-file-type="image"
                    data-file-manager-output-type="hashid"
                    data-file-manager-default-folder="posttype___{{ $model->posttype->hashid }}"
                    data-file-manager-config-name="featured_image"
                    class="btn btn-{{ $model->featured_image? 'default' : 'primary' }}">
                {{ trans('filemanager::other-modules.posts.browse-image') }}
            </button>

            <input id="txt-featured-image" type="hidden" name="featured_image"
                   value="{{ $model->featured_image ? $model->featured_image : '' }}">
            <div id="div-featured-image" class="{{ $model->featured_image? '' : 'noDisplay' }}">
                <div id="div-featured-image-inside" class="w90 m10" style="border-radius: 10px">
                    @php
                        $currentFeaturedImage = fileManager()->fileToolsProvider()->smartFindFile($model->featured_image);
                    @endphp
                    <img id="img-featured-image"
                         src=" {{ $currentFeaturedImage->exists ? url($currentFeaturedImage->pathname) : '' }}"
                         style="max-width: 100%">
                </div>

                <button type="button" id="btnDeleteFeaturedImage" class="btn btn-link btn-xs"
                        onclick="featuredImage('deleted')">
				<span class="text-danger clickable">
					{{ trans('filemanager::other-modules.posts.flush-image') }}
				</span>
                </button>
            </div>
        </div>
    </div>

    @section('html_footer')
        <script>
            function featuredImage(event) {
                switch (event) {
                    case 'inserted' :
                        $('#div-featured-image').slideDown();
                        $('#btn-featured-image').addClass('btn-default').removeClass('btn-primary');
                        break;

                    case 'deleted' :
                        $('#div-featured-image').slideUp('fast');
                        $('#txt-featured-image').val('');
                        $('#img-featured-image').attr('src', '');
                        $('#btn-featured-image').addClass('btn-primary').removeClass('btn-default');
                        break;
                }
            }

            $(document).ready(function () {
                $("#btn-featured-image").fileManagerModal();
            });
        </script>
    @append
@endif
