@extends('filemanager::layouts.plane')

@section('body')
    <div class="media-content">
        @include('filemanager::main.folder-menu')
        @include('filemanager::main.media-frame')
    </div>
@stop
