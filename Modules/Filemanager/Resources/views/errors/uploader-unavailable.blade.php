<div class="{{ $class }} alert alert-danger">
	{{ trans('filemanager::validation.uploader_unavailable') }}
</div>