@extends('yasna::errors.full_template')
@section('error_code')
    {{ $errorCode }}
@endsection
@section('message')
    <div style="direction: rtl">
        {{ trans('filemanager::base.error.nothing-to-show') }}
    </div>
@endsection