<?php
return [
     'message' => [
     ],

     'error' => [
          'size'   => 'File is too large.',
          'type'   => 'File type is unacceptable.',
          'server' => 'Uploading process failed with {{statusCode}} error.',
          // <== TODO: Refactor the argument passing method.
          'limit'  => 'You can\'t upload more files.',
     ],

     'status' => [
          'uploading' => 'Uploading',
          'failed'    => 'Failed',
          'success'   => 'Success',
     ],
];
