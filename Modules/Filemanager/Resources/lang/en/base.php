<?php
return [
     'file-types' => [
          'file'       => [
               'title'         => 'File',
               'uploader-text' => 'Drop your file here or click on this area and select it.',
               'filter-title'  => 'All Files',
          ],
          'image'      => [
               'title'         => 'Image',
               'uploader-text' => 'Drop the image file here or click on this area and select it.',
               'filter-title'  => 'Images Files',
          ],
          'text'       => [
               'title'         => 'Text',
               'uploader-text' => 'Drop the text file here or click on this area and select it.',
               'filter-title'  => 'Text Files',
          ],
          'video'      => [
               'title'         => 'Video',
               'uploader-text' => 'Drop the video file here or click on this area and select it.',
               'filter-title'  => 'Video Files',
          ],
          'audio'      => [
               'title'         => 'Audio',
               'uploader-text' => 'Drop the audio file here or click on this area and select it.',
               'filter-title'  => 'Audio Files',
          ],
          'compressed' => [
               'title'         => 'Compressed',
               'uploader-text' => 'Drop the compressed file here or click on this area and select it.',
               'filter-title'  => 'Compressed Files',
          ],
          'icon'       => [
               'title'         => 'Icon',
               'uploader-text' => 'Drop the icon file here or click on this area and select it.',
               'filter-title'  => 'Icons',
          ],
     ],

     'message' => [
          'succeeded'           => 'Your request has succeeded.',
          'failed'              => 'Your request has failed.',
          'wait'                => 'Wait.',
          'loading'             => 'Loading',
          'empty'               => 'Folder is empty.',
          'file-deleted'        => 'File has been removed.',
          'choose'              => 'Choose File',
          'delete'              => 'Are you sure to remove this item.',
          'name'                => 'Folder Name:',
          'rename_to'           => 'Rename to:',
          'rename_to_something' => 'Rename to: :something',
          'extension_not_found' => '(translation wanted)',
     ],

     'error' => [
          'rename'          => 'This name has been used!',
          'file-empty'      => 'You should chose a file.',
          'file-exist'      => 'Another file with this name has been created.',
          'file-size'       => 'File size exceeds server limit! (maximum size: :max)!',
          'delete-folder'   => 'Can not delete because of the desired folder!',
          'folder-name'     => 'Folder name can not be empty!',
          'folder-exist'    => 'There is another folder with this name!',
          'folder-alnum'    => 'Only alphanumeric folder names are allowed!',
          'nothing-to-show' => 'Nothing to Show.',
          'mime'            => 'Unacceptable Extension: ',
          'size'            => 'Extra Size:',
          'instance'        => 'File should be instance of UploadedFile.',
          'invalid'         => 'Invalid upload request.',
          'file-not-found'  => 'File not found.',
     ],

     'byte-units' => [
          'B'  => 'Byte',
          'KB' => 'Kilobyte',
          'MB' => 'Megabyte',
          'GB' => 'Gigabyte',
          'TB' => 'Terabyte',
     ],
];
