<?php
return [
     'alpha_underscore'     => ':attribute should only contain english alphabets and underscore.',
     'unable_to_upload'     => 'Unable to upload.',
     'uploader_unavailable' => 'Uploader is unavailable.',
     'unreliable_inputs'    => 'Unreliable Inputs',


     'attributes' => [
          'alternative_text' => 'Alternative Text',
          'featured_image'   => 'Featured Image',
          'file'             => 'File',
     ],

     'attributes_placeholder' => [
          'file_title'  => 'File Title',
          'source_link' => 'Source Link',
     ],
];
