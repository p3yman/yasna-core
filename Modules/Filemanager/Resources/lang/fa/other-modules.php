<?php
return [
    'posts' => [
        'browse-image'     => 'انتخاب عکس',
        'browse-file'      => 'انتخاب فایل',
        'flush-image'      => 'تخلیه‌ی عکس',
        'attachment-files' => 'فایل‌های ضمیمه',
    ],
];
