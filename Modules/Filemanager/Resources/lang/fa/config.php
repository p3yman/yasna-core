<?php
return [
     "default" => "پیش‌فرض",

     "max_file_size" => [
          "title"            => "بیشینه‌ی حجم",
          "required-message" => "بیشینه‌ی حجم فایل‌های :type الزامی است.",
          "invalid-message"  => "بیشینه‌ی حجم فایل‌های :type به درستی وارد نشده است.",
     ],

     "max_file_numbers" => [
          "title"            => "بیشینه‌ی تعداد",
          "unit"             => "عدد",
          "required-message" => "بیشینه‌ی تعداد فایل‌های :type الزامی است.",
          "invalid-message"  => "بیشینه‌ی تعداد فایل‌های :type به درستی وارد نشده است.",
     ],

     "extension" => [
          "title"            => [
               "plural"   => "پسوند‌ها",
               "singular" => "پسوند‌",
          ],
          "required-message" => "حداقل یکی از پسوندها را برای فایل‌های :type مشخص و یا گزینه‌ی پیش‌فرض را انتخاب کنید.",
     ],

     "mime_type" => [
          "title"            => [
               "plural"   => "انواع فایل",
               "singular" => "نوع فایل",
          ],
          "required-message" => "حداقل یکی از انواع فایل را برای فایل‌های :type مشخص و یا گزینه‌ی پیش‌فرض را انتخاب کنید.",
     ],

     "version" => [
          "title"                            => [
               "plural"   => "ورژن‌ها",
               "singular" => "ورژن",
          ],
          "option"                           => [
               "title"  => "عنوان ورژن",
               "width"  => "طول",
               "height" => "عرض",
          ],
          "title-required-message"           => "برای ورژن‌های فایل‌های :type عنوان الزامی است.",
          "width-or-height-required-message" => "برای ورژن‌های فایل‌های :type یکی از مقادیر طول و عرض الزامی است.",
     ],

     "directory" => [
          "title"            => [
               "plural"   => "پوشه‌های آپلود",
               "singular" => "پوشه‌ی آپلود",
          ],
          "required-message" => "وارد کردن پوشه‌ی آپلود الزامی است.",
          "invalid-message"  => "پوشه‌ی آپلود به درستی وارد نشده‌است.",
     ],


     "validation" => [
          "general" => "تنظیمات آپلود صحیح نیستند.",
     ],
];
