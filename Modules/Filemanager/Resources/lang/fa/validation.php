<?php
return [
     'alpha_underscore'     => ':attribute فقط می‌تواند شامل حروف الفبای انگلیسی و زیرین خط (_) باشد.',
     'unable_to_upload'     => 'امکان آپلود وجود ندارد.',
     'uploader_unavailable' => 'آپلودر در دسترس نیست',
     'unreliable_inputs'    => 'ورودی‌ها نامطمئن هستند.',


     'attributes' => [
          'alternative_text' => 'متن جایگزین',
          'featured_image'   => 'تصویر شاخص',
          'file'             => 'فایل',
     ],

     'attributes_placeholder' => [
          'file_title'  => 'عنوان فایل',
          'source_link' => 'لینک مرجع',
     ],
];
