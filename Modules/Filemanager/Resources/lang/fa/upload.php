<?php
return [
     'message' => [
     ],

     'error' => [
          'size'   => 'حجم فایل بسیار زیاد است.',
          'type'   => 'نوع فایل قابل قبول نیست.',
          'server' => 'فرآیند آپلود فایل در سرور با خطای {{statusCode}} مواجه شد.',
          // <== TODO: Refactor the argument passing method.
          'limit'  => 'نمی‌توانید فایل دیگری آپلود کنید.',
     ],

     'status' => [
          'uploading' => 'در حال بارگذاری',
          'failed'    => 'ناموفق',
          'success'   => 'موفق',
     ],
];
