$(document).ready(function () {
    $(document).on({
        'change': function () {
            let that  = $(this);
            let panel = $(that.data('panel'));
            if (that.is(':checked')) {
                panel.slideDown();
            } else {
                panel.slideUp();
            }
        }
    }, '.file-type-switch input[type=checkbox]');


    $(document).on({
        change: function () {
            let that = $(this);
            let handler = that.closest('.default-handler');
            let target = $(handler.data('target'));
            if (that.is(':checked')){
                target.slideUp();
            } else {
                target.slideDown();
            }
        }
    }, '.default-toggle');


    $(document).on({
        click: function () {
            let that          = $(this);
            let template      = that.data('template');
            let control       = that.closest('.versions-control');
            let row           = $($(template).html());
            let current_index = row.attr('data-index');
            let new_index     = (parseInt(control.siblings('.version-item').last().attr('data-index')) + 1);
            row.attr('data-index', new_index);
            row.find('input').each(function () {
                let that_input     = $(this);
                let new_input_name = that_input.attr('name').replace('[' + current_index + ']', '[' + new_index + ']');
                that_input.attr('name', new_input_name);
            });

            that.closest('.versions-control').before(row);
        }
    }, '.add-version');


    $(document).on({
        click: function () {
            $(this).closest('.version-item').remove();
        }
    }, '.remove-version');
});