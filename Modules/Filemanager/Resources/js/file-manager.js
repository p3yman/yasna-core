var getListXhr, getFileDetailsXhr;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// FileManager Class

function FileManager(fileManagerBody) {
    let fileManagerObj       = this;
    let tabs                 = fileManagerBody.find('.media-router .media-menu-item');
    let galleryListContainer = fileManagerBody.find('#thumbnail');
    let galleryClearListBtn  = fileManagerBody.find('#clear-list');
    let detailSidebar        = fileManagerBody.find('.media-sidebar');
    let getListXhr;
    let succeededQueueFiles  = [];

    // Properties
    fileManagerObj.multiSelect = false;

    // Methods
    fileManagerObj.changeTab = function (newTab) {
        tabs.removeClass('active');
        $('.page').hide();
        tabs.filter('[data-page=' + newTab + ']').addClass('active');
        $('#' + newTab).show();
        fileManagerObj.__trigger('tabChanged', newTab);
    };

    fileManagerObj.selectFiles = function (fileHashids, clearSelectionFirst) {
        let itemsToSelect = galleryListContainer.children('li').filter(function (index, item) {
            return ($.inArray($(item).find('.thumbnail').data('file'), fileHashids) > -1);
        });

        fileManagerObj.selectGalleryItems(itemsToSelect, clearSelectionFirst);
    };

    fileManagerObj.selectFile = function (fileHahid, clearSelectionFirst) {
        fileManagerObj.selectFiles([fileHahid], clearSelectionFirst);
    };


    fileManagerObj.selectGalleryItems = function (items, clearSelectionFirst) {
        if ((typeof clearSelectionFirst !== 'undefined') && (clearSelectionFirst == true)) {
            fileManagerObj.clearSelection();
        }
        items.addClass('ui-selected');
        fileManagerObj.__finalizeSelection();
    };


    fileManagerObj.deselectGalleryItems = function (items) {
        let itemsToDeselect = galleryListContainer.find('li.ui-selected');
        if ((typeof items !== 'undefined') && items) {
            itemsToDeselect = itemsToDeselect.not(items);
        }
        itemsToDeselect.removeClass('active ui-selected');

        fileManagerObj.__finalizeSelection();
    };

    fileManagerObj.clearSelection = function () {
        fileManagerObj.deselectGalleryItems();
    };


    fileManagerObj.forceRefreshGallery = function (callback) {
        let folder              = fileManagerBody.find(".breadcrumb-folders li.current");
        let listRequest         = fileManagerBody.find('#filters').find(':input').serializeObject();
        listRequest['instance'] = folder.attr('data-instance');
        listRequest['key']      = folder.attr('data-key');

        fileManagerObj.getListXhr = $.ajax({
            url       : routes.getList,
            type      : 'POST',
            data      : listRequest,
            beforeSend: function () {
                if (fileManagerObj.getListXhr && fileManagerObj.getListXhr.readyState != 4) {
                    fileManagerObj.getListXhr.abort();
                }
                loadingDialog();
            },
            success   : function (response) {
                $('#thumbnail').html($(response));
                galleryListContainer = fileManagerBody.find('#thumbnail');

                // $('#thumbnail').find('li').each(function () {
                //     let li = $(this);
                //     let img = li.find('img')
                //     if (img.length && img.hasClass('not-found'))
                //         li.addClass('unselectable');
                // });
                loadingDialog('hide');
                refreshSelection();
                callClosure(callback);
            }
        });
    };


    fileManagerObj.refreshGallery = function (callback, force) {
        force = !(typeof force !== 'undefined') && (force == false);
        if (!force && fileManagerObj.getListXhr && fileManagerObj.getListXhr.readyState != 4) {
            fileManagerObj.getListXhr.then(function () {
                callClosure(callback);
            });
        } else {
            fileManagerObj.forceRefreshGallery(callback);
        }
    };

    fileManagerObj.canMultiSelect = function () {
        return !!(fileManagerObj.multiSelect);
    };

    fileManagerObj.canNotMultiSelect = function () {
        return !fileManagerObj.canMultiSelect();
    };

    fileManagerObj.addToSucceededQueue = function (fileHashid) {
        succeededQueueFiles.push(fileHashid);
    };

    fileManagerObj.getSucceededQueue = function (fileHashid) {
        return succeededQueueFiles;
    };


    fileManagerObj.clearSucceededQueue = function () {
        succeededQueueFiles = [];
    };


    fileManagerObj.selectSucceededQueue = function (clearSelectionFirst, resetQueue) {
        fileManagerObj.selectFiles(fileManagerObj.getSucceededQueue(), clearSelectionFirst);
        if ((typeof resetQueue !== 'undefined') && (resetQueue == true)) {
            fileManagerObj.clearSucceededQueue();
        }
    };

    // Event Tools
    fileManagerObj.on = function (eventName, listener) {
        fileManagerBody.on(specificEventToGenralEvent(eventName), listener);
    };

    fileManagerObj.__trigger = function (eventName) {
        fileManagerBody.trigger(specificEventToGenralEvent(eventName), Array.from(arguments).slice(1));
    };


    fileManagerObj.__finalizeSelection = function () {
        galleryListContainer.selectable("option", "stop")();
    };

    fileManagerObj.resetFilters = function () {
        fileManagerBody.find('#filters').find(':input').each(function () {
            let that = $(this);
            if (that.is('select')) {
                let firstVisibleOpt = that.find('option:visible').first();
                that.val(firstVisibleOpt.val());
            } else {
                that.val('');
            }
        });

        fileManagerObj.refreshGallery();
    };


    fileManagerObj.selectFolder = function (folder) {
        //Finds Activated Folders And Changes Icons
        let parents = getFolderParents(folder);

        let externalFields = {};
        parents.each(function () {
            let that = $(this);

            externalFields[that.attr('data-instance')] = that.attr('data-key');
        });
        fileManagerBody.find('.uploader-container')
            .find('#_externalFields')
            .val(JSON.stringify(externalFields));
        fileManagerBody.find(".breadcrumb-folders .folder").removeClass("active");
        fileManagerBody.find(".folder .folder-icon").removeClass("fa-folder-open").addClass('fa-folder');
        fileManagerBody.find(".breadcrumb-folders li").removeClass('current');

        folder.addClass('current');
        parents.addClass('active');

        fileManagerBody.find('.breadcrumb-folders .active').each(function (i) {
            $(this).find('span.folder-icon').first().removeClass('fa-folder').addClass("fa-folder-open");
        });

        let fileTypes = acceptedFilesCategories[folder.data('instance') + '__' + folder.data('key')];
        fileManagerBody.find('#filter-file-type option').each(function () {
            let opt = $(this);
            if (opt.val() && ($.inArray(opt.val(), fileTypes) == -1)) {
                opt.hide();
            } else {
                opt.show();
            }
        });

        fileManagerObj.resetFilters();
    };


    fileManagerObj.getDefaultFolder = function () {
        // Select default folder
        let defaultFolder = fileManagerBody.find(".breadcrumb-folders .folder").first();
        if (parent.window.fileManagerModalOptions && parent.window.fileManagerModalOptions.defaultFolder) {
            let defaultFolderParts = parent.window.fileManagerModalOptions.defaultFolder.split('___');
            if ($.isArray(defaultFolderParts) && (defaultFolderParts.length == 2)) {
                let tmpDefaultFolder =
                        $('.breadcrumb-folders .folder[data-instance="' + defaultFolderParts[0] + '"][data-key="' + defaultFolderParts[1] + '"]');
                if (tmpDefaultFolder.length) {
                    defaultFolder = tmpDefaultFolder;
                }
            }
        }

        return defaultFolder;
    };


    fileManagerObj.selectDefaultFolder = function () {
        fileManagerObj.selectFolder(fileManagerObj.getDefaultFolder());
    };


    // Events
    fileManagerObj.onTabChanged = function (listener) {
        fileManagerObj.on('tabChanged', listener);
    };

    // Init
    $(document).ready(function () {
        fileManagerObj.selectDefaultFolder();

        if (window.fileManagerModalOptions.multi) {
            fileManagerObj.multiSelect = true;
        }

        /*----Tabs Behaviour------*/
        tabs.on('click', function (e) {
            e.preventDefault();
            let tabId = $(this).data('page');
            fileManagerObj.changeTab(tabId);
        });
        /*----End Tabs Behaviour------*/


        /*----Selecting Thumbnails------*/
        galleryListContainer.selectable({
            filter   : "li:not(.unselectable)",
            selecting: function (event, ui) {
                //ul Containing The Whole Thumbnails
                var ul        = $(this),
                    //Current li Selected
                    currentEl = $(ui.selecting);

                // Deselect other selected items if multi selection is disabled
                if (fileManagerObj.canNotMultiSelect()) {
                    ul.find('.ui-selected').removeClass('ui-selected');
                }
            },
            stop     : function (event, ui) {
                //All Selected "li"
                var selected = $('li.ui-selected');

                if (fileManager.canMultiSelect()) {
                    showFileDetails(selected.first());
                } else {
                    selected.not(':first').removeClass('ui-selected');
                    selected = selected.first();
                    showFileDetails(selected);
                }

                refreshSelection(selected);
            },
        });

        galleryListContainer.find('.ui-selectee').each(function () {
            if (!$(this).is('li')) {
                $(this).removeClass('.ui-selectee');
            }
        });
        /*----End Selecting Thumbnails----*/


        /*-----Clear List And Reseting -----*/
        galleryClearListBtn.on('click', function () {
            fileManagerObj.deselectGalleryItems();

            detailSidebar.find('.file-details').hide();
        });
        /*-----End Clear List And Reseting -----*/


        fileManagerBody.find('#add-btn').click(function () {
            let selected = $('li.ui-selected .thumbnail');

            if (selected.length) {
                if (fileManagerObj.canMultiSelect()) {
                    useFiles(selected);
                } else {
                    useFile(selected.first());
                }
            } else {
                alert(lang['error']['file-empty']);
            }
        });
    });


    // Private Methods
    function getFolderParents(folder) {
        var link = folder.children('a');
        return link.parents('.folder');
    }

    function specificEventToGenralEvent(eventName) {
        return 'fileManager' + eventName.ucfirst();
    }
}

let fileManager = new FileManager($('.media-content'));


var multiSelect = false;
var caching     = {};
// On Load Variables
var $window,
    //Details Shown Inside Sidebar (Hidden On Page Load)
    detailSidebar,
    footer,
    tabs;

var timers = {
    fileDetails: {}
};

jQuery(function ($) {
    if (window.fileManagerModalOptions.multi) {
        multiSelect = true;
    }

    $window       = $(window);
    //Details Shown Inside Sidebar (Hidden On Page Load)
    detailSidebar = $('.media-sidebar');
    footer        = $('.media-footer');
    tabs          = $('.media-router .media-menu-item');


    /*---Breadcrumb Function----*/
    $(".breadcrumb-folders li a").click(function (e) {
        e.preventDefault();
        var folder = $(this).closest('.folder');
        fileManager.selectFolder(folder)
    });
    /*---- End Breadcrumb Function----*/


    /*---- Handeling Sidebar And Folder Browser-----*/
    $window.on('resize load', function () {

        var windowWidth = $window.width();

        /*---- Folder Browser Opening Function-----*/
        if (windowWidth <= 900) {
            //Closing Browser If Open
            $('.media-folder-menu').hide();

            //Setting Browser Opening Icon Function
            $('.media-frame-title .menu-icon').on('click', function () {
                $('.media-folder-menu').show();
            });

            //Setting Browser Closing Icon Function
            $('.media-folder-menu .close-sidebar').on('click', function () {
                $('.media-folder-menu').hide();
            });
        } else {

            //Always Show Browser On Bigger Screens
            $('.media-folder-menu').show();
        }
        /*---- End Folder Browser Opening Function-----*/

        /*---- Siderbar Opening Function -----*/
        if (windowWidth <= 768) {

            //Closing Sidebar If Open
            $('.media-sidebar').hide();

            //Setting Sidebar Closing Icon Function
            $('.media-sidebar .close-sidebar').on('click', function () {
                $('.media-sidebar').hide();
            });
        } else {

            //Always Show Sidebar On Bigger Screens
            $('.media-sidebar').show();
        }
        /*---- Siderbar Opening Function -----*/

    });

    $(document).on({
        click: function () {
            let that = $(this);
            let file = that.closest('.file-details').attr('data-file');
            if (file) {
                let data = {fileKey: file};
                $.ajax({
                    url       : routes.deleteFile,
                    type      : 'POST',
                    data      : data,
                    beforeSend: function () {
                        that.addClass('in-process');
                        loadingDialog('show', '#sidebar-loading');
                    },
                    success   : function () {
                        fileManager.resetFilters();
                        that.removeClass('delete-file-btn');
                        that.addClass('restore-file-btn');
                        $('.deletion-message').html(lang['message']['file-deleted']).show();
                    },
                    complete  : function () {
                        that.removeClass('in-process');
                        loadingDialog('hide', '#sidebar-loading');
                    }
                })
            }
        }
    }, '.delete-file-btn:not(.in-process)');

    $(document).on({
        click: function () {
            let that = $(this);
            let file = that.closest('.file-details').attr('data-file');
            if (file) {
                let data = {fileKey: file};
                $.ajax({
                    url       : routes.restoreFile,
                    type      : 'POST',
                    data      : data,
                    beforeSend: function () {
                        that.addClass('in-process');
                        loadingDialog('show', '#sidebar-loading');
                    },
                    success   : function () {
                        fileManager.resetFilters();
                        that.removeClass('restore-file-btn');
                        that.addClass('delete-file-btn');
                        $('.deletion-message').hide().html();
                    },
                    error     : function (rs) {
                        if (rs.status == 404) {
                            $('.deletion-message').html(lang['error']['file-not-found']).show();
                        }
                    },
                    complete  : function () {
                        that.removeClass('in-process');
                        loadingDialog('hide', '#sidebar-loading');
                    }
                })
            }
        }
    }, '.restore-file-btn:not(.in-process)');

    $(document).on({
        keyup : function () {
            updateFileDetail($(this))
        },
        change: function () {
            updateFileDetail($(this))
        }
    }, '.setting :input');

    $(document).on({
        click: function (event) {
            event.preventDefault();
            let that     = $(this);
            let instance = that.data('instance');
            let key      = that.data('key');

            if (instance && key) {
                let folder = $('.folder[data-instance="' + instance + '"][data-key="' + key + '"]');
                if (folder.length == 1) {
                    fileManager.selectFolder(folder);
                }
            }
        }
    }, '.btn-open-folder');

    $(document).on({
        click: function (event) {
        }
    }, '.delete-upload-info');

    $('.file-list-view').scroll(function () {
        let scrolled = $(this).scrollTop();
        $('#loading-dialog').css('top', scrolled + 'px')
    });

    $('.attachments-preview').on({
        click: function () {
            let that    = $(this);
            let fileKey = that.data('file');

            if (fileKey) {
                showFileDetails($(this).closest('li'))
            }
        }
    }, '.thumbnail');

    $('#filters').find(':input').change(function () {
        fileManager.refreshGallery();
    });

}); //End Of Ready!

function forms_pd($string) {
    if (!$string) {
        $string = "0";
    }
    $string = $string.toString();

    $string = $string.replaceAll(/1/g, "۱");
    $string = $string.replaceAll(/2/g, "۲");
    $string = $string.replaceAll(/3/g, "۳");
    $string = $string.replaceAll(/4/g, "۴");
    $string = $string.replaceAll(/5/g, "۵");
    $string = $string.replaceAll(/6/g, "۶");
    $string = $string.replaceAll(/7/g, "۷");
    $string = $string.replaceAll(/8/g, "۸");
    $string = $string.replaceAll(/9/g, "۹");
    $string = $string.replaceAll(/0/g, "۰");

    return $string;
}

function forms_digit_fa(enDigit) {
    return forms_pd(enDigit);

    var newValue = "";
    for (var i = 0; i < enDigit.length; i++) {
        var ch = enDigit.charCodeAt(i);
        if (ch >= 48 && ch <= 57) {
            var newChar = ch + 1584;
            newValue    = newValue + String.fromCharCode(newChar);
        }
        else {
            newValue = newValue + String.fromCharCode(ch);
        }
    }
    return newValue;
}

function pd(enDigit) {
    return forms_digit_fa(enDigit);
}

function refreshSelection(selected) {
    if (typeof seleted == 'undefined') {
        selected = $('li.ui-selected');
    }

    selected = selected.filter(function (key, obj) {
        let item = $(obj);
        if (!item.length) {
            return false;
        }

        let img = item.find('img');
        if (!img.length) {
            return false;
        }

        if (img.hasClass('not-found')) {
            item.removeClass('ui-selected');
            return false;
        }

        return true;
    });

    if (selected.length) {
        $('#add-btn').removeAttr('disabled');
    } else {
        $('#add-btn').attr('disabled', 'disabled');
    }

    let selectedClone = selected.clone().removeAttr('class'),
        selectedCount = selectedClone.length,
        PersianCount  = pd(selectedCount);

    //Adding Selected Items Into Footer Preview
    footer.find('.attachments-preview').empty().append(selectedClone);
    footer.find('.count').empty().text("گزینش شده: " + PersianCount);

    //Setting Selected Elements As Button Value
    $('#add-btn').val(selectedClone);
}

function switchToGallery() {
    fileManager.changeTab('gallery');
}

function getFolderParents(folder) {
    var link = folder.children('a');
    return link.parents('.folder');
}

function eachUploadCompleted(file, dropzoneEl) {
    if ((file.status == 'success') && (responseArray = JSON.parse(file.xhr.response)) && responseArray.success) {
        if (fileManager.canMultiSelect()) {
            fileManager.addToSucceededQueue(responseArray.file);
        } else {
            setTimeout(function () {
                fileManager.refreshGallery(function () {
                    fileManager.selectFile(responseArray.file);
                }, false);
            }, 2000);
        }
        setTimeout(function () {
            dropzoneEl.removeFile(file);
        }, 2000);
    } else {
        fileManager.refreshGallery();
    }

}

function allUploadsCompleted(acceptedFiles, files, dropzoneEl) {
    if (fileManager.canMultiSelect()) {
        fileManager.refreshGallery(function () {
            fileManager.selectSucceededQueue(true, true);
        });
    }
//    let uploadedFilesHashids = files.map(function (file) {
//        if ((file.status == 'success') && (responseArray = JSON.parse(file.xhr.response)) && responseArray.success) {
//            return responseArray.file;
//        } else {
//            return null;
//        }
//    }).filter(function (hashid) {
//        return !!hashid;
//    });
//
//    fileManager.refreshGallery(function () {
//        fileManager.selectFiles(uploadedFilesHashids);
//    }, false);

    if ($.inArray('error', files.getColumn('status')) == -1) {
        setTimeout(switchToGallery, 1000);
    }
}

function getFileUrl(file) {
    return $("[data-file=\"" + file + "\"]").data('url');
}

function getFilePathname(file) {
    return $("[data-file=\"" + file + "\"]").data('pathname');
}

function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
    var match   = window.location.search.match(reParam);
    return (match && match.length > 1) ? match[1] : null;
}

function showFileDetails(li) {
    if (!li.is('li')) {
        console.log('!li.is(\'li\')')
        return false;
    }

    if (!li.length) {
        console.log('!li.length')
        return false;
    }

    let thumb = li.find('.thumbnail');
    if (!thumb.length) {
        console.log('!thumb.length')
        return false;
    }

    let hashid = thumb.data('file');
    if (!hashid) {
        return false;
    }


    let thumbnail = $('ul.ui-selectable li .thumbnail[data-file=' + hashid + ']');
    if (!thumbnail.length) {
        console.log('!li.length')
        return false;
    }
    let ul = thumbnail.closest('ul');

    //Showing Sidebar If Hidden
    if (!detailSidebar.is(':visible')) {
        detailSidebar.show();
    }

    //Showing Details Inside Sidebar
    detailSidebar.find('.file-details').show();

    // Getting file details
    getFileDetailsXhr = $.ajax({
        url       : routes.getFileDetails + '/' + hashid,
        beforeSend: function () {
            if (getFileDetailsXhr && getFileDetailsXhr.readyState != 4) {
                getFileDetailsXhr.abort();
            }
        },
        success   : function (response) {

            $('.file-details').attr('data-file', hashid);
            $('.file-details').html($(response));

            $('.setting :input').each(function () {
                let timerName                 = $(this).attr('name') + '-' + $.now();
                timers.fileDetails[timerName] = new Timer();
                $(this).attr('data-timer', timerName);
            });
        }
    });

    // Resetting Active Class To Currently Selected Element
    ul.find('.active').removeClass('active');
    thumbnail.closest('li').addClass('active');
}

function updateFileDetail(input) {
    let file       = input.closest('.file-details').attr('data-file');
    let inputName  = input.attr('name');
    let inputValue = input.val();
    if (file && (typeof inputName !== 'undefined') && (typeof inputValue !== 'undefined')) {
        let data        = {fileKey: file};
        let timerName   = input.attr('data-timer');
        let timer       = timers.fileDetails[timerName];
        inputValue      = $.trim(inputValue);
        data[inputName] = inputValue;

        timer.delay(function () {
            $.ajax({
                url       : routes.setFileDetails,
                type      : 'POST',
                data      : data,
                beforeSend: function () {
                    loadingDialog('show', '#sidebar-loading');
                },
                complete  : function () {
                    loadingDialog('hide', '#sidebar-loading');
                }
            })
        }, 1);
    }
}

function useFile(thumbEl) {

    function useTinymce3(url) {
        var win                                                               = tinyMCEPopup.getWindowArg("window");
        win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = url;
        if (typeof(win.ImageDialog) != "undefined") {
            // Update image dimensions
            if (win.ImageDialog.getImageData) {
                win.ImageDialog.getImageData();
            }

            // Preview if necessary
            if (win.ImageDialog.showPreviewImage) {
                win.ImageDialog.showPreviewImage(url);
            }
        }
        tinyMCEPopup.close();
    }

    function useTinymce4AndColorbox(url, field_name) {
        parent.document.getElementById(field_name).value = url;

        if (typeof parent.tinyMCE !== "undefined") {
            parent.tinyMCE.activeEditor.windowManager.close();
        }
        if (typeof parent.$.fn.colorbox !== "undefined") {
            parent.$.fn.colorbox.close();
        }
    }

    function useCkeditor3(url) {
        if (window.opener) {
            // Popup
            window.opener.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
        } else {
            // Modal (in iframe)
            parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
            parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorCleanUpFuncNum'));
        }
    }

    function useFckeditor2(url) {
        var p = url;
        var w = data['Properties']['Width'];
        var h = data['Properties']['Height'];
        window.opener.SetUrl(p, w, h);
    }

    function showFile(file) {
        var preview = window.fileManagerModalOptions.preview;
        if (preview) {
            $.ajax({
                url    : routes.preview,
                type   : 'post',
                data   : {
                    file: file,
                },
                success: function (response) {
                    parent.document.getElementById(preview).innerHTML = response;
                }
            });
        }
    }

    function useModal(result) {
        let parentWindow = $(parent.document);

        // Set value in target element
        let targetInput = parentWindow.find('#' + window.fileManagerModalOptions.input);
        if (targetInput.length) {
            targetInput.val(result);
        }

        // Show file
        showFile(hashid);

        // Run callback
        let callBackValue = window.fileManagerModalOptions.callback;
        if (callBackValue) {
            parent.window.eval(callBackValue);
        }

        window
            .parent
            .$('#' + window.fileManagerModalOptions.input)
            .trigger('change')

        // Close modal
        parent.window.closeFileManagerModal()
    }

    var hashid      = thumbEl.data('file');
    var fileName    = thumbEl.data('fileName');
    var url         = getFileUrl(hashid);
    var pathname    = getFilePathname(hashid);
    var field_name  = getUrlParam('field_name');
    var is_ckeditor = getUrlParam('CKEditor');
    var is_modal    = window.fileManagerModalOptions.modal;
    var is_fcke     = typeof data != 'undefined' && data['Properties']['Width'] != '';
    var file_path   = url.replace(routes.main, '');
    var modal       = url.replace(routes.main, '');


    if (
        window.opener ||
        window.tinyMCEPopup ||
        field_name ||
        getUrlParam('CKEditorCleanUpFuncNum') ||
        is_ckeditor ||
        is_modal
    ) {
        if (is_modal) {
            // Modify Callback
            if (window.fileManagerModalOptions.callback) {
                window.fileManagerModalOptions.callback = window.fileManagerModalOptions.callback.replace('__pathname__', pathname);
                window.fileManagerModalOptions.callback = window.fileManagerModalOptions.callback.replace('__url__', url);
                window.fileManagerModalOptions.callback = window.fileManagerModalOptions.callback.replace('__hashid__', hashid);
                window.fileManagerModalOptions.callback = window.fileManagerModalOptions.callback.replace('__fileName__', fileName);
            }
            switch (window.fileManagerModalOptions.outputType) {
                case 'hashid':
                    useModal(hashid, field_name);
                    break;
                case 'url':
                    useModal(url, field_name);
                    break;
                case 'name':
                    useModal(fileName, field_name);
                    break;
                default:
                    useModal(pathname, field_name);
                    break
            }
        } else if (window.tinyMCEPopup) { // use TinyMCE > 3.0 integration method
            useTinymce3(url);
        } else if (field_name) {   // tinymce 4 and colorbox
            useTinymce4AndColorbox(url, field_name);
        } else if (is_ckeditor) {   // use CKEditor 3.0 + integration method
            useCkeditor3(url);
        } else if (is_fcke) {      // use FCKEditor 2.0 integration method
            useFckeditor2(url);
        } else {                   // standalone button or other situations
            window.opener.SetUrl(url, file_path);
        }

        if (window.opener) {
            window.close();
        }
    } else {
        // No WYSIWYG editor found, use custom method.
        window.opener.SetUrl(url, file_path);
    }
}

//end useFile
function useFiles(filesEls) {
    function showFiles(hashids) {
        var preview = window.fileManagerModalOptions.preview;
        if (preview) {
            $.ajax({
                url    : routes.preview,
                type   : 'post',
                data   : {
                    file: hashids.join('-'),
                },
                success: function (response) {
                    parent.document.getElementById(preview).innerHTML = response;
                }
            });
        }
    }

    function useModal(result) {
        let parentWindow = $(parent.document);

        // Set value in target element
        let targetInput = parentWindow.find('#' + window.fileManagerModalOptions.input);
        if (targetInput.length) {
            targetInput.val(JSON.stringify(result));
        }

        // Show file

        showFiles(hashids);

        // Run callback
        let callBackValue = window.fileManagerModalOptions.callback;
        if (callBackValue) {
            parent.window.eval(callBackValue);
        }

        window
            .parent
            .$('#' + window.fileManagerModalOptions.input)
            .trigger('change')


        // Close modal
        parent.window.closeFileManagerModal()
    }

    var hashids  = [];
    var is_modal = window.fileManagerModalOptions.modal;

    filesEls.each(function () {
        hashids.push($(this).data('file'));
    });

    if (is_modal) {
        let result = [];
        switch (window.fileManagerModalOptions.outputType) {
            case 'hashid':
                result = hashids;
                break;
            case 'url':
                filesEls.each(function () {
                    result.push($(this).data('url'));
                });
                break;
            default: // Ex: pathname
                filesEls.each(function () {
                    result.push($(this).data('pathname'));
                });
                break
        }

        useModal(result);
    }
}

function loadingDialog(parameter, dialog) {
    if (isDefined(dialog)) {
        if (!(dialog instanceof HTMLCollection) && (typeof dialog == 'string')) {
            dialog = $(dialog);
        }
    } else {
        dialog = $('#loading-dialog');
    }

    if (dialog.length) {
        if ($.inArray(parameter, ['hide', false, 0]) > -1) {
            dialog.hide();
        } else {
            dialog.show();
        }
    }
}

function isClosure(variable) {
    return (typeof variable === 'function');
}

function callClosure(closure) {
    if (isClosure(closure)) {
        return closure.apply(this, Array.from(arguments).slice(1))
    }

    return null;
}