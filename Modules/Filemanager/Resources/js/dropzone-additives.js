/**
 * Created by EmiTis on 12/08/2017.
 */

// if we miss this command, every elements with "dropzone" class will be automatically change to dropzone
Dropzone.autoDiscover = false;

// setting default options for all dropzone uploaders in this page
Dropzone.prototype.defaultOptions.url = dropzoneRoutes.upload;
Dropzone.prototype.defaultOptions.addRemoveLinks = true;
Dropzone.prototype.defaultOptions.dictRemoveFile = "";
Dropzone.prototype.defaultOptions.dictCancelUpload = "";
Dropzone.prototype.defaultOptions.dictFileTooBig = messages.errors.size;
Dropzone.prototype.defaultOptions.dictInvalidFileType = messages.errors.type;
Dropzone.prototype.defaultOptions.dictResponseError = messages.errors.server;
Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = messages.errors.limit;

// dropzone Options
var dropzoneOptions = {
    init: {
        sending: function (file, xhr, formData) {
            var dropzoneSelf = this;

            // Append csrf token
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

            // Append elements inside of dorpzone element self
            var inElementData = $(this.element).find(':input').serializeArray();
            $.each(inElementData, function (index, node) {
                formData.append(node.name, node.value);
            });

            // Append external fields (if needed)
            let currentExternalFields = $(dropzoneSelf.element).find('#_externalFields');
            let externalFields = currentExternalFields.length ? $.parseJSON(currentExternalFields.val()) : {};
            formData.append('_externalFields', JSON.stringify(externalFields));
        },

        removedfile: function (file) {
            removeFile(file, $(this.element));
        }
    }
};

function updateTarget(dropzoneInstance, target) {
    if (dropzoneInstance.getUploadingFiles().length === 0 && dropzoneInstance.getQueuedFiles().length === 0) {
        var accepted = dropzoneInstance.getAcceptedFiles();
        var dataArr = [];
        var targetEl = $(target);
        $.each(accepted, function (index, file) {
            if (file.status == "success") {
                var rsJson = $.parseJSON(file.xhr.response);
                dataArr.push(rsJson.file);
            }
        });
        if (dataArr.length) {
            targetEl.val(JSON.stringify(dataArr));
        } else {
            targetEl.val('');
        }
    }
}

function removeFile(file, dropzoneElement) {
    var uploadResultElement = file.uploadResultElement;
    if (uploadResultElement && uploadResultElement.length) {
        uploadResultElement.remove();
    }
}

function removeFromServer(file, dropzoneElement) {
    if (file.xhr) {
        var rs = $.parseJSON(file.xhr.response);
        var data = rs;
        data._token = csrfToken;

        var additionalData = dropzoneElement.find(':input').serializeArray();
        $.each(additionalData, function (index, item) {
            data[item.name] = item.value;
        });
        $.ajax({
            url: dropzoneRoutes.remove,
            type: 'POST',
            data: data,
        })
    }
}

function refreshDropzone(dropzoneObj) {
    dropzoneObj.removeAllFiles();
}

$(document).ready(function () {
    $(document).on({
        click: function () {
            let row = $(this).closest('.media-uploader-status');
            if (row.length) {
                row.remove();
            }
        }
    }, '.delete-upload-info')
});
