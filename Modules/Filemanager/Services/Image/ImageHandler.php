<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/20/18
 * Time: 12:17 PM
 */

namespace Modules\Filemanager\Services\Image;

use Intervention\Image\Facades\Image as ImageFacade;
use Intervention\Image\Image;

class ImageHandler
{
    protected $image;



    /**
     * ImageHandler constructor.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return $this|false
     */
    public function safeResize($width, $height)
    {
        if ($width or $height) {
            if ($this->cropNeeded($width, $height)) {
                $this->crop($width, $height);
            }
            $this->resize($width, $height);
            return $this;
        } else {
            return false;
        }
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return bool
     */
    protected function cropNeeded($width, $height)
    {
        return ($width and $height);
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return $this
     */
    protected function crop($width, $height)
    {
        list($crop_width, $crop_height) = array_values($this->safeCropDimensions($width, $height));
        $this->image->crop($crop_width, $crop_height);
        return $this;
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return array
     */
    protected function safeCropDimensions($width, $height)
    {
        $original_width  = $this->image->width();
        $original_height = $this->image->height();

        if ($this->canCropWidthBased($width, $height)) {
            $dimensions['width']  = $original_width;
            $dimensions['height'] = floor(($dimensions['width'] * $height) / $width);
        } else {
            $dimensions['height'] = $original_height;
            $dimensions['width']  = floor(($dimensions['height'] * $width) / $height);
        }

        // Sort $dimensions
        return array_replace(array_flip(['width', 'height']), $dimensions);
    }



    /**
     * Determine whether this image looks fit enough if it would be cropped based on its width into the given width and
     * height.
     *
     * @param int|null $width
     * @param int|null $height
     *
     * @return bool
     */
    protected function canCropWidthBased($width, $height)
    {
        $original_width  = $this->image->width();
        $original_height = $this->image->height();
        $ratioHeight     = $height * $original_width / $width;

        if ($ratioHeight <= $original_height) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return $this
     */
    public function resize($width, $height)
    {
        $resize_width  = $this->resizeProperWidth($width, $height);
        $resize_height = $this->resizeProperHeight($width, $height);

        $this->image->resize($resize_width, $resize_height);
        return $this;
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return integer
     */
    protected function resizeProperWidth($width, $height)
    {
        if ($width) {
            return $width;
        }

        return ((integer)floor(($height * $this->image->width()) / $this->image->height()));
    }



    /**
     * @param int|null $width
     * @param int|null $height
     *
     * @return integer
     */
    protected function resizeProperHeight($width, $height)
    {
        if ($height) {
            return $height;
        }

        return ((integer)floor(($width * $this->image->height()) / $this->image->width()));
    }



    /**
     * Saves encoded image in filesystem
     *
     * @param  string $path
     *
     * @return Image
     */
    public function save($path)
    {
        return $this->image->save($path);
    }



    /**
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }



    /**
     * @param mixed $pathname
     *
     * @return static
     */
    public static function make($pathname)
    {
        return new static(ImageFacade::make($pathname));
    }
}
