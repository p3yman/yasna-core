<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/15/18
 * Time: 11:46 AM
 */

namespace Modules\Filemanager\Services\Tools\VirtualUploader;


use App\Models\Posttype;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Modules\Filemanager\Providers\UploadServiceProvider;

class Uploader
{
    const DEFAULT_SIZE_LIMIT = 1;
    /**
     * The Uploading File
     *
     * @var UploadedFile
     */
    protected $file;
    /**
     * The Main Config
     *
     * @var array|null
     */
    protected $config;
    /**
     * The Posttype for the File
     *
     * @var Posttype|null
     */
    protected $posttype;



    /**
     * Uploader constructor.
     *
     * @param UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }



    /**
     * Sets the config of the posttype as the uploading config.
     *
     * @param Posttype $posttype
     * @param string   $config_slug
     *
     * @return Uploader
     */
    public function setPosttypeConfig(Posttype $posttype, string $config_slug)
    {
        $this->posttype = $posttype;

        UploadServiceProvider::setConfigsSection('posttype__' . $posttype->slug);
        UploadServiceProvider::setCurrentConfigSlug($config_slug);

        return $this->setConfig($posttype->getUploadConfigs($config_slug));
    }



    /**
     * Sets the uploading config.
     *
     * @param array|null $config
     *
     * @return $this
     */
    protected function setConfig(?array $config)
    {
        if ($config) {
            $this->config = $config;
        }

        return $this;
    }



    /**
     * Returns the uploading config.
     *
     * @return array|\Illuminate\Config\Repository|null
     */
    protected function getConfig()
    {
        return ($this->config ?? UploadServiceProvider::getDefaultConfigs());
    }



    /**
     * Weather the file is valid to bo uploaded.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->getValidator()->passes();
    }



    /**
     * Weather the file is not valid to bo uploaded.
     *
     * @return bool
     */
    public function isNotValid()
    {
        return !$this->isValid();
    }



    /**
     * Returns the errors of uploading the file.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->getValidator()->errors()->get('file');
    }



    /**
     * Uploads the file if it is valid.
     *
     * @return bool|string
     */
    public function upload()
    {
        if ($this->isNotValid()) {
            return false;
        }

        return hashid($this->doUpload());
    }



    /**
     * Returns the validator of the file.
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidator()
    {
        return Validator::make(['file' => $this->file], [
             'file' => [
                  'mimes:' . implode(',', $this->getAcceptedExtensions()),
                  'max:' . ($this->getMaxFileSize() * 1024),
             ],
        ])->setAttributeNames(['file' => trans('filemanager::validation.attributes.file')])
             ;
    }



    /**
     * Returns the configs of all file types.
     *
     * @return array
     */
    protected function getAllTypeConfigs()
    {
        return ($this->config['fileTypes'] ?? []);
    }



    /**
     * Returns the configs of all active file types.
     *
     * @return array
     */
    protected function getActiveTypesConfigs()
    {
        return array_filter($this->getAllTypeConfigs(), function ($type_config) {
            return ($type_config['status'] ?? false);
        });
    }



    /**
     * Guesses the file type and returns it.
     *
     * @return int|string
     */
    protected function guessFileType()
    {
        $file_extension = $this->file->guessExtension();
        $type_configs   = $this->getActiveTypesConfigs();

        foreach ($type_configs as $type => $config) {
            $extensions = ($config['acceptedExtensions'] ?? []);

            if (in_array($file_extension, $extensions)) {
                return $type;
            }
        }

        return '';
    }



    /**
     * Returns the config based on the file type.
     *
     * @return array
     */
    protected function getTypeConfigs()
    {
        $type          = $this->guessFileType();
        $types_configs = $this->getActiveTypesConfigs();

        return ($types_configs[$type] ?? []);
    }



    /**
     * Returns all the acceptable extensions.
     *
     * @return array
     */
    protected function getAcceptedExtensions()
    {
        $configs = $this->getActiveTypesConfigs();

        $types_extenstions = collect($configs)
             ->pluck('acceptedExtensions')
             ->filter()
             ->values()
             ->toArray()
        ;

        if (empty($types_extenstions)) {
            return [];
        }

        return array_merge(...$types_extenstions);
    }



    /**
     * Returns the max file size based on the file type.
     *
     * @return int
     */
    protected function getMaxFileSize()
    {
        $config = $this->getTypeConfigs();

        $max_file_size = ($config['maxFileSize'] ?? static::DEFAULT_SIZE_LIMIT);

        return $max_file_size;
    }



    /**
     * Does the upload process and returns the uploaded file's id.
     *
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    protected function doUpload()
    {
        return UploadServiceProvider::uploadFile($this->file, $this->uploadDirectory(), $this->getExternalFields());
    }



    /**
     * Returns the uploading directory.
     *
     * @return string
     */
    protected function uploadDirectory()
    {
        return ($this->config['uploadDir'] . DIRECTORY_SEPARATOR . $this->guessFileType());
    }



    /**
     * Returns the extension of the file.
     *
     * @return array
     */
    protected function getExternalFields()
    {
        $fields = [];

        if ($this->posttype) {
            $fields['posttype'] = $this->posttype->id;
        }

        return $fields;
    }
}
