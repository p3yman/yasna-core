<?php

namespace Modules\Filemanager\Services\Tools\Uploader;

use App\Models\Posttype;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Services\General\Traits\MagicMethodsTrait;

/**
 * Client simulates a browser and makes requests to a Kernel object.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @method $this image()
 * @method $this video()
 * @method $this audio()
 * @method $this text()
 * @method $this compressed()
 * @method $this icon()
 * @method \Illuminate\Contracts\View\Factory|\Illuminate\View\View renderGeneralJs
 * @method \Illuminate\Contracts\View\Factory|\Illuminate\View\View renderGeneralCss
 */
abstract class UploaderAbstract implements UploaderInterface
{
    use MagicMethodsTrait;

    protected $posttype;
    protected $config;
    protected $configSlug;
    protected $posttypeConfigSlug = '__current__';
    protected $with_css           = false;
    protected $with_general_css   = true;
    protected $with_js            = false;
    protected $with_general_js    = true;
    protected $with_html          = true;
    protected $class;
    protected $id;
    protected $types              = [];
    protected $direct_upload      = false;
    protected $js_variable_name;
    protected $ajax_remove        = true;
    protected $result_input_selector;
    protected $without_posttype   = false;
    protected $files_progresses_position;

    protected $top_title;
    protected $bottom_title;

    protected $on_each_upload_complete;
    protected $on_each_upload_success;
    protected $on_each_upload_error;
    protected $on_queue_complete;

    protected static $general_css_shown = false;
    protected static $general_js_shown  = false;

    const PROGRESSES_INSIDE = 'INSIDE';
    const PROGRESSES_UNDER  = 'UNDER';



    /**
     * UploaderAbstract constructor.
     *
     * @param Posttype|string|integer|null $posttype
     */
    public function __construct($posttype = null)
    {
        $this->posttype = ($posttype instanceof Posttype) ? $posttype : model('posttype', $posttype);
        $this->progressesInside();
    }



    /**
     * @param string $method
     * @param array  $parameters
     *
     * @return $this|null
     */
    public function __callFileType($method, $parameters)
    {
        if (FileManagerToolsServiceProvider::isAcceptableFileType($method)) {
            $this->popMagicMethod();
            return $this->addType($method);
        }

        return null;
    }



    /**
     * @return $this
     */
    public function allFileTypes()
    {
        $this->types = FileManagerToolsServiceProvider::getAcceptableFileTypes();

        return $this;
    }



    /**
     * @param string $method
     * @param array  $parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function __callGeneralAssets($method, $parameters)
    {
        if (
             starts_with($method, 'renderGeneral') and
             method_exists(static::class, $method)
        ) {
            $this->popMagicMethod();
            return static::$method(...$parameters);
        }

        return null;
    }



    /**
     * @param string $type
     *
     * @return $this
     */
    protected function addType($type)
    {
        $this->types[] = $type;
        return $this;
    }



    /**
     * Return file types that have been specified
     *
     * @return array
     */
    protected function getFileTypes()
    {
        return count($this->types) ? $this->types : FileManagerToolsServiceProvider::getAcceptableFileTypes();
    }



    /**
     * Return file types that have been specified and accepted by config
     *
     * @return array
     */
    protected function getSafeFileTypes()
    {
        return array_intersect(
             $this->getFileTypes(),
             isset($this->config['fileTypes']) ? array_keys($this->config['fileTypes']) : []
        );
    }



    /**
     * Get config paths of file types
     *
     * @return array
     */
    protected function getConfigPaths()
    {
        return array_map(function ($type) {
            return $this->configSlug . '.' . $type;
        }, $this->getSafeFileTypes());
    }



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function config($configSlug)
    {
        if ($this->setConfig($configSlug)->config) {
            $this->configSlug = $configSlug;
        }

        return $this;
    }



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    protected function setConfig($configSlug)
    {
        $this->config = ConfigsServiceProvider::getConfigData($configSlug);
        return $this;
    }



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function posttypeConfig($configSlug)
    {
        if ($this->config = $this->posttype->getUploadConfigs($configSlug)) {
            $this->posttypeConfigSlug = $configSlug;
            $this->configSlug         = UploadServiceProvider::getPostTypeConfigPrefix() . $this->posttype->slug;
        }

        return $this;
    }



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function forceConfigSlug($configSLug)
    {
        $this->configSlug = $configSLug;
        if (is_null($this->config)) {
            $this->setConfig('default');
        }

        return $this;
    }



    /**
     * @return $this
     */
    public function withoutPosttype()
    {
        $this->without_posttype = true;
        return $this;
    }



    /**
     * @return $this
     */
    public function withCss()
    {
        $this->with_css = true;
        return $this;
    }



    /**
     * @return $this
     */
    public function withJs()
    {
        $this->with_js = true;
        return $this;
    }



    /**
     * @return $this
     */
    public function withAssets()
    {
        return $this->withCss()
                    ->withJs()
             ;
    }



    /**
     * @return $this
     */
    public function withoutGeneralCss()
    {
        $this->with_general_css = false;

        return $this;
    }



    /**
     * @return $this
     */
    public function withoutGeneralJs()
    {
        $this->with_general_js = false;

        return $this;
    }



    /**
     * @return $this
     */
    public function withoutGeneralAssets()
    {
        return $this->withoutGeneralCss()
                    ->withoutGeneralJs()
             ;
    }



    /**
     * @param string $value
     *
     * @return $this
     */
    public function class($value)
    {
        $this->class = $value;

        return $this;
    }



    /**
     * @param string $value
     *
     * @return $this
     */
    public function id($value)
    {
        $this->id = $value;
        return $this;
    }



    /**
     * @return $this
     */
    public function directUpload()
    {
        $this->direct_upload = true;
        return $this;
    }



    /**
     * @param string $value
     *
     * @return $this
     */
    public function jsVariableName($value)
    {
        $this->js_variable_name = $value;
        return $this;
    }



    /**
     * @return $this
     */
    public function withoutAjaxRemove()
    {
        $this->ajax_remove = false;
        return $this;
    }



    /**
     * @param string $value
     *
     * @return $this
     */
    public function resultInputSelector($value)
    {
        $this->result_input_selector = $value;
        return $this;
    }



    /**
     * @return $this
     */
    public function progressesInside()
    {
        $this->files_progresses_position = static::PROGRESSES_INSIDE;

        return $this;
    }



    /**
     * @return $this
     */
    public function progressesPositionInside()
    {
        return $this->progressesInside();
    }



    /**
     * @return $this
     */
    public function progressesUnder()
    {
        $this->files_progresses_position = static::PROGRESSES_UNDER;

        return $this;
    }



    /**
     * @return $this
     */
    public function progressesPositionUnder()
    {
        return $this->progressesUnder();
    }



    /**
     * @param string $value
     *
     * @return $this
     */
    protected function setProgressesPositions($value)
    {
        $this->files_progresses_position = $value;

        return $this;
    }



    /**
     * @param string $constantName
     *
     * @return bool
     */
    public function progressesPositionIs($constantName)
    {
        return static::progressesPositionIsEqualToConstant($this->files_progresses_position, $constantName);
    }



    /**
     * @param string $value
     * @param string $constantName
     *
     * @return bool
     */
    public static function progressesPositionIsEqualToConstant($value, $constantName)
    {
        return ((defined($constantPath = "static::$constantName") and (constant($constantPath) == $value)));
    }



    /**
     * @param string $title
     *
     * @return $this
     */
    public function topTitle(string $title)
    {
        $this->top_title = $title;
        return $this;
    }



    /**
     * @param string $title
     *
     * @return $this
     */
    public function bottomTitle(string $title)
    {
        $this->bottom_title = $title;
        return $this;
    }



    /**
     * @param string $title
     *
     * @return $this
     */
    public function title(...$parameters)
    {
        return $this->bottomTitle(...$parameters);
    }



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadComplete($value)
    {
        $this->on_each_upload_complete = $value;
        return $this;
    }



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadSuccess($value)
    {
        $this->on_each_upload_success = $value;
        return $this;
    }



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadError($value)
    {
        $this->on_each_upload_error = $value;
        return $this;
    }



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onQueueComplete($value)
    {
        $this->on_queue_complete = $value;
        return $this;
    }



    /**
     * Return an always existed id
     *
     * @return string
     */
    protected function getSafeId()
    {
        if (!$this->id) {
            $this->id = 'uploader-' . str_random(8);
        }

        return $this->id;
    }



    /**
     * Return an always existed variable name
     *
     * @return string
     */
    protected function getSafeJsVariableName()
    {
        if (!$this->js_variable_name) {
            $this->js_variable_name = studly_case($this->getSafeId() . '-variable');
        }

        return $this->js_variable_name;
    }



    /**
     * Check if this uploader has presents enough conditions to be rendered
     *
     * @return bool
     */
    protected function isCreatable()
    {
        return (($this->without_posttype or $this->posttype->exists) and $this->config);
    }



    /**
     * @return bool
     */
    protected static function generalCssShowing()
    {
        if (static::generalCssIsNotShown()) {
            static::$general_css_shown = true;
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return bool
     */
    protected static function generalCssIsShown()
    {
        return static::$general_css_shown;
    }



    /**
     * @return bool
     */
    protected static function generalCssIsNotShown()
    {
        return !static::generalCssIsShown();
    }



    /**
     * @return bool
     */
    protected static function generalJsShowing()
    {
        if (static::generalJsIsNotShown()) {
            static::$general_js_shown = true;
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return bool
     */
    protected static function generalJsIsShown()
    {
        return static::$general_js_shown;
    }



    /**
     * @return bool
     */
    protected static function generalJsIsNotShown()
    {
        return !static::generalJsIsShown();
    }
}
