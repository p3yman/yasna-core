<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/17/18
 * Time: 10:29 PM
 */

namespace Modules\Filemanager\Services\Tools\Uploader;

interface UploaderInterface
{
    public function __construct($posttype);



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function config($configSlug);



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function posttypeConfig($configSlug);



    /**
     * @param string $configSlug
     *
     * @return $this
     */
    public function forceConfigSlug($configSLug);



    /**
     * @return $this
     */
    public function withoutPosttype();



    /**
     * @return $this
     */
    public function withCss();



    /**
     * @return $this
     */
    public function withJs();



    /**
     * @return $this
     */
    public function withAssets();



    /**
     * @return $this
     */
    public function withoutGeneralCss();



    /**
     * @return $this
     */
    public function withoutGeneralJs();



    /**
     * @return $this
     */
    public function withoutGeneralAssets();



    /**
     * @param string $value
     *
     * @return $this
     */
    public function class($value);



    /**
     * @param string $value
     *
     * @return $this
     */
    public function id($value);



    /**
     * @return $this
     */
    public function directUpload();



    /**
     * @param string $value
     *
     * @return $this
     */
    public function jsVariableName($value);



    /**
     * @param string $value
     *
     * @return $this
     */
    public function resultInputSelector($value);



    /**
     * @return $this
     */
    public function progressesInside();



    /**
     * @return $this
     */
    public function progressesPositionInside();



    /**
     * @return $this
     */
    public function progressesUnder();



    /**
     * @return $this
     */
    public function progressesPositionUnder();



    /**
     * @param string $constantName
     *
     * @return bool
     */
    public function progressesPositionIs($constantName);



    /**
     * @return $this
     */
    public function allFileTypes();



    /**
     * @param string $value
     * @param string $constantName
     *
     * @return bool
     */
    public static function progressesPositionIsEqualToConstant($value, $constantName);



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadComplete($value);



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadSuccess($value);



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onEachUploadError($value);



    /**
     * @param string $value Name of a js function
     *
     * @return $this
     */
    public function onQueueComplete($value);



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render();



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function renderJs();



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function renderCss();



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderAssets();
}
