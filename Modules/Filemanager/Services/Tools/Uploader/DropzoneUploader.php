<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/17/18
 * Time: 3:20 PM
 */

namespace Modules\Filemanager\Services\Tools\Uploader;

use Modules\Filemanager\Providers\Uploaders\DropzoneUploaderServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;

class DropzoneUploader extends UploaderAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        if ($this->isCreatable()) {
            return static::renderView($this->renderData());
        } else {
            return view('filemanager::errors.uploader-unavailable', [
                 'class' => $this->class,
                 'id'    => $this->id,
            ]);
        }
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderJs()
    {
        if ($this->isCreatable()) {
            return static::renderView($this->renderAssetsModifyingFields('js'));
        }

        return null;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderCss()
    {
        if ($this->isCreatable()) {
            return static::renderView($this->renderAssetsModifyingFields('css'));
        }

        return null;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderAssets()
    {
        if ($this->isCreatable()) {
            return static::renderView(array_merge($this->renderData(),
                 $this->renderAssetsModifyingFields(['js', 'css'])));
        }

        return null;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected static function renderGeneralCss()
    {
        return static::renderView(array_merge(
             static::renderGeneralAssetsModifyingFields('css'),
             static::staticRenderingData()
        ));
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected static function renderGeneralJs()
    {
        return static::renderView(array_merge(
             static::renderGeneralAssetsModifyingFields('js'),
             static::staticRenderingData()
        ));
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected static function renderGeneralAssets()
    {
        return static::renderView(array_merge(
             static::renderGeneralAssetsModifyingFields('js'),
             static::staticRenderingData()
        ));
    }



    /**
     * @param array|string $types
     *
     * @return array
     */
    protected static function renderGeneralAssetsModifyingFields($types = [])
    {
        $types = array_map(function ($type) {
            return studly_case($type);
        }, (array)$types);

        $fields = [
             'generalCss' => in_array('Css', $types) ? static::generalCssShowing() : false,
             'generalJs'  => in_array('Js', $types) ? static::generalJsShowing() : false,
        ];
        foreach ($fields as $key => $value) {
            $fields[$key] = ends_with($key, $types) ? $value : false;
        }

        return $fields;
    }



    /**
     * @param array|string $types
     *
     * @return array
     */
    protected function renderAssetsModifyingFields($types = [])
    {
        $types = array_map(function ($type) {
            return studly_case($type);
        }, (array)$types);


        $data = $this->renderData($types);


        foreach ($types as $type) {
            if (!$data[$key = 'with' . $type]) {
                $data[$key] = true;
            }
            $checkShownMethod        = 'general' . $type . 'Showing';
            $assetNeededProperty     = 'with_general_' . strtolower($type);
            $data['general' . $type] = ($this->$assetNeededProperty and static::$checkShownMethod());
        }

        return $data;
    }



    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected static function renderView(array $data)
    {
        return view('filemanager::layouts.uploaders.dropzone.main', $data);
    }



    /**
     * @param array $types
     *
     * @return array
     */
    protected function renderData($types = ['css', 'js', 'html'])
    {
        $types = array_map(function ($type) {
            return studly_case($type);
        }, (array)$types);

        $methods = $this->filterMethods(function ($method) {
            return (
                 starts_with($method, 'render') and
                 ends_with($method, 'Data') and
                 ($method != 'renderData')
            );
        });

        $data = [];
        foreach ($methods as $method) {
            $data = array_merge_recursive($data, $this->$method($types));
        }

        return $data;
    }



    /**
     * @return array
     */
    protected function renderPosttypeData()
    {
        return [
             'externalFields' => [
                  'posttype' => $this->posttype->hashid,
             ],
        ];
    }



    /**
     * @return array
     */
    protected function renderConfigNameData()
    {
        return ['configName' => $this->posttypeConfigSlug];
    }



    /**
     * @return array
     */
    protected function renderHtmlData()
    {
        return [
             'class'              => $this->class,
             'id'                 => $this->getSafeId(),
             'varName'            => $this->getSafeJsVariableName(),
             'ajaxRemove'         => $this->ajax_remove,
             'progressesPosition' => $this->files_progresses_position,
             'resultSelector'     => $this->result_input_selector,
             'top_title'          => $this->top_title,
             'bottom_title'       => $this->bottom_title,
        ];
    }



    /**
     * @return array
     */
    protected function renderFileTypeData()
    {
        foreach (($fileTypes = $this->getConfigPaths()) as $fileTypeString) {
            if (UploadServiceProvider::isActive($fileTypeString)) {
                $fileTypeStringParts = UploadServiceProvider::translateFileTypeString($fileTypeString);

                if (isset($fileType)) {
                    if ($fileType != last($fileTypeStringParts)) {
                        $fileType = 'file';
                    }
                } else {
                    $fileType = last($fileTypeStringParts);
                }
                $uploadIdentifiers[] = implode('.', $fileTypeStringParts);
            }
        }

        $uploadConfig = UploadServiceProvider::getCompleteRules($fileTypes, true);


        return compact('uploadIdentifiers', 'fileType', 'uploadConfig');
    }



    /**
     * @return array
     */
    protected function renderDirectUploadData()
    {
        return ['directUpload' => $this->direct_upload];
    }



    /**
     * @return array
     */
    protected function renderAssetsData($types)
    {
        return [
             'withCss'  => (in_array('Css', $types) and $this->with_css),
             'withJs'   => (in_array('Js', $types) and $this->with_js),
             'withHtml' => (in_array('Html', $types) and $this->with_html),
        ];
    }



    /**
     * @return array
     */
    protected function renderGeneralAssetsData($types)
    {
        return [
             'generalCss' => (
                  $this->with_general_css and
                  in_array('Html', $types) and $this->with_css and
                  static::generalCssShowing()
             ),

             'generalJs' => (
                  $this->with_general_js and
                  in_array('Html', $types) and
                  $this->with_js and static::generalJsShowing()
             ),
        ];
    }



    /**
     * @return array
     */
    protected function renderCallbacksData()
    {
        return [
             'callbackOnEachUploadComplete' => $this->on_each_upload_complete,
             'callbackOnEachUploadSuccess'  => $this->on_each_upload_success,
             'callbackOnEachUploadError'    => $this->on_each_upload_error,
             'callbackOnQueueComplete'      => $this->on_queue_complete,
        ];
    }



    /**
     * @return array
     */
    protected function renderStaticData()
    {
        return static::staticRenderingData();
    }



    /**
     * @return array
     */
    public static function staticRenderingData()
    {
        return [
             'uploaderClass' => static::class,
        ];
    }



    /**
     * Returns applicable file types.
     *
     * @param array $config
     *
     * @return array
     */
    protected function getApplicableFileTypesFromConfig()
    {
        $config = $this->config;
        if (!is_array($config) or empty($config)) {
            return [];
        }

        $file_types = $config['fileTypes'];
        $types      = $this->types;
        if (empty($types)) {
            $applicable_types = array_keys(array_filter($file_types, function ($type_info) {
                return ($type_info['status'] ?? false);
            }));
        } else {
            $applicable_types = $types;
        }

        if (empty($applicable_types)) {
            return [];
        }

        return array_only($file_types, $applicable_types);
    }



    /**
     * Returns the applicable files limit.
     *
     * @return int|null
     */
    protected function getDynamicJsMaxFilesConfig()
    {
        $types           = $this->getApplicableFileTypesFromConfig();
        $types_max_files = array_map(function ($type_info) {
            $max_files = ($type_info['maxFiles'] ?? -1);
            return (!$max_files or ($max_files == -1)) ? INF : $max_files;
        }, $types);
        $min             = min($types_max_files);

        return ($min < INF) ? intval($min) : null;
    }



    /**
     * Returns the dynamic js configs.
     *
     * @return array
     */
    protected function getDynamicJsConfigs()
    {
        return [
             'maxFiles' => $this->getDynamicJsMaxFilesConfig(),
        ];
    }



    /**
     * Renders the js configs data.
     *
     * @return array
     */
    public function renderJsConfigsData()
    {
        $static = static::staticJsConfigs();

        return [
             'js_configs' => array_merge_recursive($static, $this->getDynamicJsConfigs()),
        ];
    }



    /*
     * Returns the static js configs.
     *
     * @return array
     */
    public static function staticJsConfigs()
    {
        return UploadServiceProvider::getDefaultJsConfigs();
    }
}
