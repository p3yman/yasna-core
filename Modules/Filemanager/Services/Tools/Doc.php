<?php

namespace Modules\Filemanager\Services\Tools;

use App\Models\File;
use App\Models\User;
use Carbon\Carbon;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Posts\Entities\Posttype;

class Doc
{
    protected $hashid;
    protected $version;
    protected $posttype;
    protected $config;
    protected $fileName;
    protected $elementClass;
    protected $elementStyle;
    protected $elementOtherAttributes;
    protected $elementDataAttributes;
    protected $elementWidth;
    protected $elementHeight;
    protected $elementExtra;

    protected $__isResolved = false;
    protected $__resolvedFile;
    protected $__resolvedPosttype;
    protected $__resolvedVersion;

    protected static $originalVersionTitle = 'original';



    public function __construct($hashid = null)
    {
        if (!is_null($hashid)) {
            $this->hashid($hashid);
        }
    }



    /**
     * Set hashid of the file that will be served
     *
     * @param string $hashid
     *
     * @return $this
     */
    public function hashid($hashid)
    {
        $this->hashid = $hashid;

        return $this;
    }



    /**
     * Set requesting version
     *
     * @param string $version
     *
     * @return $this
     */
    public function version($version)
    {
        $this->version = clearFileVersionTitle($version);

        return $this;
    }



    /**
     * Set posttype that should be applied on file version
     *
     * @param Posttype|integer|string $posttype
     *
     * @return $this
     */
    public function posttype($posttype)
    {
        $this->posttype = $posttype;

        return $this;
    }



    /**
     * Set the slug of the input of the posttype that contains uploading configs
     *
     * @param string $config
     *
     * @return $this
     */
    public function config($config)
    {
        $this->config = $config;

        return $this;
    }



    /**
     * Set name of file that will be served in download link
     *
     * @param string $fileName
     *
     * @return $this
     */
    public function fileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }



    /**
     * Set class attribute of the <pre><img /></pre> that will be return in show() method
     *
     * @param array|string $class
     *
     * @return $this
     */
    public function elementClass($class)
    {
        $this->elementClass = $class;

        return $this;
    }



    /**
     * Set style attribute of the <pre><img /></pre> that will be return in show() method
     *
     * @param array|string $style
     *
     * @return $this
     */
    public function elementStyle($style)
    {
        $this->elementStyle = $style;

        return $this;
    }



    /**
     * Set some attributes of the <pre><img /></pre> that will be return in show() method
     *
     * @param array $otherAttributes
     *
     * @return $this
     */
    public function elementOtherAttributes($otherAttributes)
    {
        $this->elementOtherAttributes = $otherAttributes;

        return $this;
    }



    /**
     * Set data-attributes of the <pre><img /></pre> that will be return in show() method
     *
     * @param array $dataAttributes
     *
     * @return $this
     */
    public function elementDataAttributes($dataAttributes)
    {
        $this->elementDataAttributes = $dataAttributes;

        return $this;
    }



    /**
     * Set width in style attribute of the <pre><img /></pre> that will be return in show() method
     *
     * @param integer|string $width
     *
     * @return $this
     */
    public function elementWidth($width)
    {
        $this->elementWidth = $width;

        return $this;
    }



    /**
     * Set height in style attribute of the <pre><img /></pre> that will be return in show() method
     *
     * @param integer|string $height
     *
     * @return $this
     */
    public function elementHeight($height)
    {
        $this->elementHeight = $height;

        return $this;
    }



    /**
     * Set some string that will be directly showed in <pre><img /></pre> that will be return in show() method
     *
     * @param string $extra
     *
     * @return $this
     */
    public function elementExtra($extra)
    {
        $this->elementExtra = $extra;

        return $this;
    }



    /**
     * Read file from db. (Use $this->hashid)
     *
     * @return $this
     */
    protected function __resolveFile()
    {
        $file = File::findByHashid($this->hashid);

        if ($file and $file->exists) {
            $this->__resolvedFile = $file;
        }

        return $this;
    }



    /**
     * Resolve posttype. (Use $this->posttype)
     *
     * @return $this
     */
    protected function __resolvePosttype()
    {
        if ($this->posttype) {
            $posttype = FileManagerToolsServiceProvider::smartFindPosttype($this->posttype);
            if ($posttype and $posttype->exists) {
                $this->__resolvedPosttype = $posttype;
            }
        }

        return $this;
    }



    /**
     * Resolve version of file. (Use $this->version, $this->posttype and $this->config)
     *
     * @return $this
     */
    protected function __resolveVersion()
    {
        if ($this->__resolvedFile) {
            $file = $this->__resolvedFile;

            if ($this->version) {
                if ($this->__resolvedPosttype) {
                    $version = UploadServiceProvider::generatePosttypeVersionTitle(
                         $this->__resolvedPosttype,
                         $this->version
                    );
                } else {
                    $version = $this->version;
                }

                $newName  = UploadServiceProvider::changeFileNameVersion($file->physical_name, $version);
                $pathname = $file->proper_directory . DIRECTORY_SEPARATOR . $newName;

                if (FileServiceProvider::getFileObject($pathname)) {
                    $this->__resolvedVersion = $version;
                    return $this;
                } else {
                    $fileObj = FileServiceProvider::getFileObject($file->pathname);
                    if ($fileObj) {
                        $related = $file->related_files;
                        if ($this->__resolvedPosttype and $this->config) {
                            $uploadConfigs          = $this->__resolvedPosttype->getUploadConfigs($this->config);
                            $fileType               = $file->type;
                            $generatingFunctionName = 'createRelated' . ucfirst($fileType);
                            if (method_exists(UploadServiceProvider::class, $generatingFunctionName)) {
                                if (
                                     $uploadConfigs and
                                     array_key_exists($fileType, $uploadConfigs['fileTypes']) and
                                     array_key_exists('extraFiles', $uploadConfigs['fileTypes'][$fileType]) and
                                     ($extraFiles = $uploadConfigs['fileTypes'][$fileType]['extraFiles']) and
                                     is_array($extraFiles)
                                ) {
                                    $clearedKeys = array_map(function ($item) {
                                        return clearFileVersionTitle($item);
                                    }, array_keys($extraFiles));

                                    if (in_array($this->version, $clearedKeys)) {
                                        $extraFileInfo = array_values($extraFiles)[array_search($this->version,
                                             $clearedKeys)];
                                        if (is_array($extraFileInfo) and
                                             array_key_exists('width', $extraFileInfo) and
                                             is_numeric($extraFileInfo['width']) and
                                             array_key_exists('width', $extraFileInfo) and
                                             is_numeric($extraFileInfo['width'])
                                        ) {
                                            $newVersionTitle = UploadServiceProvider::generatePosttypeVersionTitle(
                                                 $this->__resolvedPosttype,
                                                 $this->version
                                            );
                                            $newFileName     = UploadServiceProvider::changeFileNameVersion(
                                                 $file->physical_name,
                                                 $newVersionTitle
                                            );
                                            $newFilePath     = $file->directory . DIRECTORY_SEPARATOR . $newFileName;
                                            UploadServiceProvider::$generatingFunctionName(
                                                 $fileObj,
                                                 $newFilePath,
                                                 ($extraFileInfo['width'] ?? null),
                                                 ($extraFileInfo['height'] ?? null)
                                            );

                                            $related[$newVersionTitle] = $newFileName;
                                            File::store(['id' => $file->id, 'related_files' => $related]);
                                            $this->__resolvedVersion = $newVersionTitle;
                                            return $this;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (FileServiceProvider::getFileObject($file->pathname)) { // Original version exists
                $this->__resolvedVersion = self::$originalVersionTitle;
            }
        }

        return $this;
    }



    /**
     * Resolve all needed things.
     */
    protected function __resolve()
    {
        if ($this->shouldResolve()) {
            $this->__resolveFile();
            $this->__resolvePosttype();
            $this->__resolveVersion();
        }
    }



    /**
     * Check if object need to resolved or not
     *
     * @return bool
     */
    protected function shouldResolve()
    {
        return !$this->__isResolved;
    }



    /**
     * Resolve object's resolvable data and mark it as resolved
     *
     * @return $this
     */
    public function resolve()
    {
        $this->__resolve();
        $this->__isResolved = true;

        return $this;
    }



    /**
     * Reset object's resolvable data and mark it as unresolved
     *
     * @return $this
     */
    public function dissolve()
    {
        $this->__resolvedFile     = null;
        $this->__resolvedPosttype = null;
        $this->__resolvedVersion  = null;
        $this->__isResolved       = false;

        return $this;
    }



    /**
     * Return title of file
     *
     * @return string|null
     */
    public function getTitle()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->name;
        }

        return null;
    }



    /**
     * Return title of file
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getTitle();
    }



    /**
     * Return name of file
     *
     * @return string|null
     */
    public function getFileName()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->file_name;
        }

        return null;
    }



    /**
     * Return title of file without its extension
     *
     * @return string|null
     */
    public function getTitleWithoutExtension()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->title;
        }

        return null;
    }



    /**
     * Return title of file without its extension
     *
     * @return string|null
     */
    public function getNameWithoutExtension()
    {
        return $this->getTitleWithoutExtension();
    }



    /**
     * @return string|null
     */
    public function getAlternative()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->alternative;
        }

        return null;
    }



    /**
     * @return string|null
     */
    public function getDescription()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->description;
        }

        return null;
    }



    /**
     * Return pathname of file
     *
     * @return null|string
     */
    public function getPathName()
    {
        $this->__resolve();

        if ($this->__resolvedFile and $this->__resolvedVersion) {
            $pathname = UploadServiceProvider::changeFileUrlVersion(
                 $this->__resolvedFile->pathname,
                 $this->__resolvedVersion);
            return $pathname;
        }

        return null;
    }



    /**
     * Return partial pathname of file
     *
     * @return string
     */
    public function getPartialPathname()
    {
        $pathname = $this->getPathName();
        $public_folder = FileManagerToolsServiceProvider::getPublicFolder() . DIRECTORY_SEPARATOR;

        if ($pathname) {
            if (starts_with($pathname, $public_folder)) {
                return str_after($pathname, $public_folder);
            } else {
                return $pathname;
            }
        } else {
            return null;
        }
    }



    /**
     * Return url of file
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public function getUrl()
    {
        $pathname = $this->getPartialPathname();
        return $pathname ? url($pathname) : null;
    }



    /**
     * @return double|null
     */
    public function getSize()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->size;
        }

        return null;
    }



    /**
     * @return string|null
     */
    public function getSizeText()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->sizeText();
        }

        return null;
    }



    /**
     * Return file type
     *
     * @return string|null
     */
    public function getType()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->type;
        }

        return null;
    }



    /**
     * Return file extension
     *
     * @return string|null
     */
    public function getExtension()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->extension;
        }

        return null;
    }



    /**
     * @return string|null
     */
    public function getMimeType()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->mime_type;
        }

        return null;
    }



    /**
     * @return float|bool|null
     */
    public function getImageWidth()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->image_width;
        }

        return null;
    }



    /**
     * @return float|bool|null
     */
    public function getImageHeight()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->image_height;
        }

        return null;
    }



    /**
     * @return Carbon|null
     */
    public function getCreationDateTime()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->created_at;
        }

        return null;
    }



    /**
     * @param null $format
     *
     * @return string|null
     */
    public function getCreationDateTimeString(...$parameters)
    {
        if ($carbon = $this->getCreationDateTime()) {
            return echoDate($carbon, ...$parameters);
        }

        return $carbon;
    }



    /**
     * @return User|null
     */
    public function getCreator()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->creator;
        }

        return null;
    }



    /**
     * @return User|null
     */
    public function getUpdater()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            return $this->__resolvedFile->getPerson('updated_by');
        }

        return null;
    }



    /**
     * Return download link of file
     *
     * @return string|null
     */
    public function getDownloadUrl()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            $routeData = ['hashid' => $this->__resolvedFile->hashid];

            if ($this->__resolvedVersion != self::$originalVersionTitle) {
                $routeData['version'] = $this->__resolvedVersion;
            }

            if ($this->fileName) {
                $routeData['fileName'] = $this->fileName;
            }

            return route('fileManager.file.download', $routeData);
        }

        return null;
    }



    /**
     * Return a view file containing an image element showing requested file view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function show()
    {
        $this->__resolve();

        if ($this->__resolvedFile) {
            $switches = [
                 'style'           => $this->elementStyle,
                 'width'           => $this->elementWidth,
                 'height'          => $this->elementHeight,
                 'class'           => $this->elementClass,
                 'otherAttributes' => $this->elementOtherAttributes,
                 'dataAttributes'  => $this->elementDataAttributes,
                 'extra'           => $this->elementExtra,
            ];
            $switches = array_filter($switches);

            return FileServiceProvider::getFileView($this->__resolvedFile, $this->__resolvedVersion, $switches);
        }

        return null;
    }



    /**
     * Move file and all its related files from temp folder and change its status into "used"
     *
     * @return bool|null
     */
    public function permanentSave()
    {
        $this->__resolve();

        if ($file = $this->__resolvedFile) {
            if (FileServiceProvider::moveFromTemp($file)) {
                $this->__resolvedFile->refresh();
                return true;
            } else {
                return false;
            }
        }

        return null;
    }



    /**
     * @return bool|null
     */
    public function delete()
    {
        $this->__resolve();

        if ($file = $this->__resolvedFile) {
            $delete_result = FileServiceProvider::removeFile($this->__resolvedFile, ['onlyTemp' => false]);
            $this->dissolve()->resolve();
            return $delete_result;
        }

        return null;
    }



    /**
     * @return bool|null
     */
    public function remove()
    {
        return $this->delete();
    }



    /**
     * @param $task
     *
     * @return bool|null
     */
    public function can($task)
    {
        $this->__resolve();

        if ($file = $this->__resolvedFile) {
            return $this->__resolvedFile->can($task);
        }

        return null;
    }



    /**
     * @param $task
     *
     * @return bool
     */
    public function canNot($task)
    {
        return $this->permissionOpposite($this->can($task));
    }



    /**
     * @return bool|null
     */
    public function canPreview()
    {
        return $this->can('preview');
    }



    /**
     * @return bool|null
     */
    public function canNotPreview()
    {
        return $this->permissionOpposite($this->canPreview());
    }



    /**
     * @return bool|null
     */
    public function canView()
    {
        return $this->canPreview();
    }



    /**
     * @return bool|null
     */
    public function canNotView()
    {
        return $this->canNotPreview();
    }



    /**
     * @return bool|null
     */
    public function canEdit()
    {
        return $this->can('edit');
    }



    /**
     * @return bool|null
     */
    public function canNotEdit()
    {
        return $this->permissionOpposite($this->canDelete());
    }



    /**
     * @return bool|null
     */
    public function canDelete()
    {
        return $this->can('delete');
    }



    /**
     * @return bool|null
     */
    public function canNotDelete()
    {
        return $this->permissionOpposite($this->canDelete());
    }



    /**
     * @return bool|null
     */
    public function canAny()
    {
        return $this->can('*');
    }



    /**
     * @return bool|null
     */
    public function canNotAny()
    {
        return $this->permissionOpposite($this->canAny());
    }



    /**
     * @param bool|null $permission_result
     *
     * @return bool|null
     */
    protected function permissionOpposite($permission_result)
    {
        return is_null($permission_result) ? $permission_result : (!$permission_result);
    }
}
