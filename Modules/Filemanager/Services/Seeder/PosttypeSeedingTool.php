<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/14/18
 * Time: 4:57 PM
 */

namespace Modules\Filemanager\Services\Seeder;

use App\Models\Posttype;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Posts\Services\PosttypeSeedingTool as OriginalPosttypeSeedingTool;

class PosttypeSeedingTool extends OriginalPosttypeSeedingTool
{
    /**
     * Seeds upload configs.
     *
     * @param array $data
     */
    public function seedUploadConfigs(array $data)
    {
        yasna()->seed('settings', $this->uploadConfigsOptimizedData($data));
    }



    /**
     * Optimizes the upload configs data.
     *
     * @param array $data
     *
     * @return array
     */
    protected function uploadConfigsOptimizedData(array $data)
    {
        foreach ($data as $key => $datum) {
            $data[$key] = $this->optimizeSingleUploadConfig($datum);
        }

        return $data;
    }



    /**
     * Optimizes a single upload config.
     *
     * @param array $config
     *
     * @return array
     */
    protected function optimizeSingleUploadConfig(array $config)
    {
        $config['slug']          = $this->optimizeConfigSlug($config['slug']);
        $config['default_value'] = $this->optimizeConfigValue($config['default_value']);

        return $config;
    }



    /**
     * Optimizes an upload config's slug.
     *
     * @param string $slug
     *
     * @return string
     */
    protected function optimizeConfigSlug(string $slug)
    {
        $postfix = '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix();

        if (!ends_with($slug, $postfix)) {
            $slug .= $postfix;
        }

        return snake_case(camel_case($slug));
    }



    /**
     * Optimizes upload config's value.
     *
     * @param string $slug
     *
     * @return false|string
     */
    protected function optimizeConfigValue(string $slug)
    {
        if (is_array(json_decode($slug, true))) {
            return $slug;
        }

        return $this->readJsonFromFile($slug);
    }



    /**
     * Reads upload config from a JSON file
     *
     * @param string $file_path
     *
     * @return false|string
     */
    protected function readJsonFromFile(string $file_path)
    {
        $file_content = file_get_contents($file_path);
        $json_string  = json_encode(json_decode($file_content, true));

        return $json_string;
    }



    /**
     * Seeds the information of a single posttype.
     *
     * @param Posttype $posttype
     * @param array    $posttype_information
     */
    protected function seedSinglePosttypeInformation(Posttype $posttype, array $posttype_information)
    {
        $upload_configs = ($posttype_information['upload-configs'] ?? []);

        parent::seedSinglePosttypeInformation($posttype, $posttype_information);
        $this->seedPosttypeUploadConfigs($posttype, $upload_configs);
    }



    /**
     * Seeds the upload configs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $upload_configs
     */
    protected function seedPosttypeUploadConfigs(Posttype $posttype, $upload_configs = [])
    {
        $inputs_slugs = array_keys($upload_configs);

        $this->seedPosttypeInputs($posttype, $inputs_slugs);

        $save_data = [];
        foreach ($upload_configs as $input_key => $setting_slug) {
            $setting_slug = $this->optimizeConfigSlug($setting_slug);
            if ($posttype->$input_key) {
                continue;
            }

            $save_data[$input_key] = setting($setting_slug)->id;
        }

        $posttype->fresh()->batchSave($save_data);
    }
}
