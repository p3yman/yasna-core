<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/17/18
 * Time: 1:45 PM
 */

namespace Modules\Filemanager\Services\General\Traits;

use BadMethodCallException;
use SplStack;

trait MagicMethodsTrait
{
    protected $magic_methods_stack;



    public function __call($method, $parameters)
    {
        $this->pushMagicMethod($method);

        $callMethods = $this->filterMethods(function ($methodName) {
            return preg_match("/^__call[[:alnum:]]+/i", $methodName);
        });

        foreach ($callMethods as $magicMethod) {
            $methodResult = $this->$magicMethod($method, $parameters);
            if ($this->magicMethodIsNotTop($method)) { // This method has been popped from the stack
                return $methodResult;
            }
        }

        throw new BadMethodCallException(
             sprintf('Call to undefined method %s::%s()', get_class($this), $method)
        );
    }



    protected function pushMagicMethod($method)
    {
        ($stack = &$this->getMagicMethodsStackPointer())->push($method);
    }



    protected function popMagicMethod()
    {
        return ($stack = &$this->getMagicMethodsStackPointer())->pop();
    }



    protected function magicMethodIsTop($method)
    {
        return ($this->getMagicMethodsstack()->count() and ($this->getMagicMethodsstack()->top() == $method));
    }



    protected function magicMethodIsNotTop($method)
    {
        return !$this->magicMethodIsTop($method);
    }



    protected function getMagicMethodsStack()
    {
        return $this->magic_methods_stack ?: ($this->magic_methods_stack = new SplStack);
    }



    protected function &getMagicMethodsStackPointer()
    {
        $this->getMagicMethodsStack();
        return $this->magic_methods_stack;
    }



    /**********************************************************
     * Filtering Methods
     **********************************************************/

    protected function filterMethods($condition)
    {
        return array_where($this->getAllMethods(), $condition);
    }



    protected function getAllMethods()
    {
        return get_class_methods($this);
    }



    protected function getMethods()
    {
        return $this->getAllMethods();
    }



    protected function methods()
    {
        return $this->getAllMethods();
    }



    protected function allMethods()
    {
        return $this->getAllMethods();
    }
}
