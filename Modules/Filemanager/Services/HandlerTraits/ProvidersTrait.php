<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/24/19
 * Time: 12:12 PM
 */

namespace Modules\Filemanager\Services\HandlerTraits;


use Illuminate\Support\ServiceProvider;
use Modules\Filemanager\Providers\FilemanagerServiceProvider;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;

trait ProvidersTrait
{
    /**
     * Returns a new instance of the `Modules\Filemanager\Providers\FileManagerToolsServiceProvider` class
     * <br>
     * _This method may be used instead of the `FileManagerTools` alias._
     *
     * @return FileManagerToolsServiceProvider
     */
    public function toolsProvider()
    {
        return $this->getProvider('FileManagerToolsServiceProvider');
    }



    /**
     * Returns a new instance of the `Modules\Filemanager\Providers\UploadServiceProvider` class
     * <br>
     * _This method may be used instead of the `UploadTools` alias._
     *
     * @return UploadServiceProvider
     */
    public function uploadToolsProvider()
    {
        return $this->getProvider('UploadServiceProvider');
    }



    /**
     * Returns a new instance of the `Modules\Filemanager\Providers\FileServiceProvider` class.
     * <br>
     * _This method may be used instead of the `FileTools` alias._
     *
     * @return FileServiceProvider
     */
    public function fileToolsProvider()
    {
        return $this->getProvider('FileServiceProvider');
    }



    /**
     * Returns a new instance of the `Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider` class.
     * <br>
     * _This method may be used instead of the `ConfigTools` alias._
     *
     * @return ConfigsServiceProvider
     */
    public function configToolsServiceProvider()
    {
        return $this->getProvider('Upstream\\ConfigsServiceProvider');
    }



    /**
     * Returns an instance of the `Modules\Filemanager\Providers\FilemanagerServiceProvider` class.
     * <br>
     * _This method may be used instead of the `FileManager` alias._
     *
     * @return FilemanagerServiceProvider
     */
    public function mainProvider()
    {
        return $this->getProvider();
    }



    /**
     * Returns a new instance of the specified provider.
     *
     * @param string|null $provider
     *
     * @return ServiceProvider
     */
    public function getProvider(?string $provider = null)
    {
        return $this->runningModule()->callProviderMethod('getProviderInstance', $provider);
    }
}
