<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/26/18
 * Time: 10:53 AM
 */

namespace Modules\Filemanager\Services\HandlerTraits;

use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;

trait BasicsTrait
{
    /**
     * @return array
     */
    public function availableFileTypes()
    {
        return FileManagerToolsServiceProvider::getAcceptableFileTypes();
    }



    /**
     * @return array
     */
    public function allFileTypes()
    {
        return $this->availableFileTypes();
    }
}
