<?php

namespace Modules\Filemanager\Services;

use BadMethodCallException;
use Illuminate\Http\UploadedFile;
use Modules\Filemanager\Services\General\Traits\MagicMethodsTrait;
use Modules\Filemanager\Services\HandlerTraits\BasicsTrait;
use Modules\Filemanager\Services\HandlerTraits\ProvidersTrait;
use Modules\Filemanager\Services\Tools\Doc;
use Modules\Filemanager\Services\Tools\Uploader\UploaderAbstract;
use Modules\Filemanager\Services\Tools\Uploader\UploaderInterface;
use Modules\Filemanager\Services\Tools\VirtualUploader\Uploader as VirtualUploader;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class FileManagerHandler
{
    use ModuleRecognitionsTrait;
    use MagicMethodsTrait;
    use BasicsTrait;
    use ProvidersTrait;


    protected static $uploader_magic_methods_postfix = 'Uploader';
    protected static $uploader_prefix                = '\Modules\Filemanager\Services\Tools\Uploader\\';



    /**
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    protected function __callUploader($method, $parameters)
    {
        if (
             (ends_with($method, self::$uploader_magic_methods_postfix)) and
             ($uploaderName = str_before($method, self::$uploader_magic_methods_postfix)) and
             (class_exists($uploaderClass = $this->findUploaderClass($uploaderName)))
        ) {
            $this->popMagicMethod();
            return new $uploaderClass(...$parameters);
        }

        return null;
    }



    /**
     * @param $posttype
     *
     * @return UploaderAbstract
     */
    public function uploader(...$parameters)
    {
        $uploaderMethod = config('filemanager.default-uploader') . self::$uploader_magic_methods_postfix;
        return $this->$uploaderMethod(...$parameters);
    }



    /**
     * @param string $hashid
     *
     * @return Doc
     */
    public function file($hashid = null)
    {
        return (new Doc($hashid));
    }



    /**
     * @param $uploader
     *
     * @return string
     */
    public function findUploaderClass($uploader)
    {
        return self::$uploader_prefix . studly_case($uploader) . self::$uploader_magic_methods_postfix;
    }



    /**
     * Returns a new instance of the `Modules\Filemanager\Services\Tools\VirtualUploader\Uploader` class
     * to be used for virtual uploading an uploaded file.
     *
     * @param UploadedFile $file
     *
     * @return VirtualUploader
     */
    public function virtualUploader(UploadedFile $file)
    {
        return new VirtualUploader($file);
    }
}
