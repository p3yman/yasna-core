<?php

namespace Modules\Filemanager\Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Yasna\Providers\YasnaServiceProvider;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->mainData());
        $this->relationships();
    }

    protected function mainData()
    {
        $data = [
            [
                'order'       => "42",
                'slug'        => "featured_image",
                'title'       => "عکس سربرگ",
                'icon'        => "file-image-o",
                'post_fields' => "featured_image",
            ],
            [
                'order'     => "43",
                'slug'      => "attachments",
                'title'     => "فایل‌های ضمیمه",
                'icon'      => "files-o",
                'post_meta' => "post_files:auto",
                'meta-hint' => "این ویژگی قابلیت ضمیمه‌ی یک مجموعه فایل به پست را اضافه می‌کند.",
            ],
        ];

        return $data;
    }

    protected function relationships()
    {
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();
        $data = [
            'featured_image' => "featured_image_$postfix,default_featured_image",
            'attachments'    => "attachments_$postfix",
        ];

        foreach ($data as $key => $slugs) {
            $model = model("feature")->grabSlug($key);
            if (!$model or !$model->id) {
                continue;
            }
            $model->attachInputs(explode(',', $slugs));
        }
    }
}
