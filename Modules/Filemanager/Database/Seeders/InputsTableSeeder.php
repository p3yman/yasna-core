<?php

namespace Modules\Filemanager\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Yasna\Providers\YasnaServiceProvider;

class InputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultConfigs = ConfigsServiceProvider::smartFindConfig('default');
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();

        $data = [
            [
                'slug'              => "featured_image_" . $postfix,
                'title'             => "تنظیمات آپلود عکس سربرگ",
                'type'              => "upstream",
                'order'             => "22",
                'data_type'         => "combo",
                'meta-options'      => 'filemanager::getConfigsCombo',
                'css_class'         => "form-required",
                'validation_rules'  => "sometimes|required|numeric",
                'purifier_rules'    => "ed",
                'default_value'     => $defaultConfigs->id,
                'default_value_set' => "1",
                'hint'              => "با توجه به قالب انتخاب کنید.",
            ],
            [
                'slug'             => "default_featured_image",
                'title'            => "عکس سربرگ پیش‌فرض",
                'type'             => "downstream",
                'order'            => "23",
                'data_type'        => "photo",
                //'css_class'        => "form-required",
                'validation_rules' => "" , //"sometimes|required",
                'hint'             => "برای نمایش، هنگامی که عکس سربرگ یک نوشته وجود نداشته باشد.",
            ],
            [
                'slug'              => "attachments_" . $postfix,
                'validation_rules'  => "sometimes|required|numeric",
                'title'             => "تنظیمات آپلود فایل‌های ضمیمه",
                'type'              => "upstream",
                'order'             => "24",
                'data_type'         => "combo",
                'meta-options'      => 'filemanager::getConfigsCombo',
                'purifier_rules'    => "ed",
                'default_value'     => $defaultConfigs->id,
                'default_value_set' => "1",
                'hint'              => "با توجه به قالب انتخاب کنید.",
            ],
        ];


        yasna()->seed('inputs', $data);
    }
}
