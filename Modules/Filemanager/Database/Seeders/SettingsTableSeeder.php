<?php

namespace Modules\Filemanager\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Yasna\Providers\YasnaServiceProvider;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();

        $data = [
            [
                'slug'          => "default_" . $postfix,
                'title'         => 'تنظیمات پیش‌فرض آپلود',
                'order'         => '1',
                'category'      => 'invisible',
                'data_type'     => 'textarea',
                'default_value' => json_encode(config('filemanager.upload.absolute')),
                'hint'          => 'تنظیمات پیش‌فرض آپلود برای همه‌ی قسمت‌ها',
            ]
        ];


        yasna()->seed('settings', $data);
    }
}
