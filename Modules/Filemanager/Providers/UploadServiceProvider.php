<?php

namespace Modules\Filemanager\Providers;

use App\Models\Posttype;
use App\Models\Setting;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Services\Image\ImageHandler;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Support\Facades\File as FilesFacades;
use App\Models\File as FileModel;
use Symfony\Component\HttpFoundation\Request;

class UploadServiceProvider extends ServiceProvider
{
    protected static $defaults = [
         'uploader'           => 'dropzone',
         'configsSection'     => 'default',
         'jsConfigs'          => [],
         'posttypeConfigSlug' => 'default',
         'followUp'           => true,
    ];
    protected static $currents = [
         'uploader'            => 'dropzone',
         'configsSection'      => 'default',
         'temporaryUpload'     => true,
         'temporaryFolderName' => 'temp',
         'posttypeConfigSlug'  => '',
         'followUp'            => true,
    ];
    protected static $statics  = [
         'postTypeConfigPrefix'            => 'posttype__',
         'postTypeExtraFilesVersionPrefix' => 'posttype',
         'rootUploadDir'                   => 'uploads', // Root Directory
         'randomNameLength'                => 30, // Length of Random Name to Be Generated for Uploading Files
         'fileNameSeparator'               => '_',
         'fileVersionTitleSeparator'       => '-',
    ];

    protected static $versionsPostfixes = [
         'original' => 'original',
    ]; // Postfixes that should be added at the end of names of any version of any file

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }



    /**
     * Applies default configs and settings
     */
    public static function setDefaults()
    {
        self::setDefaultConfigs();
    }



    /**
     * Returns name of the current uploader provider class
     *
     * @return string
     */
    public static function getCurrentUploaderProvider()
    {
        return class_exists($currentUploader = self::getUploaderProvider(self::$currents['uploader']))
             ? $currentUploader
             : class_exists($currentUploader = self::getUploaderProvider(self::$defaults['uploader']));
    }



    /**
     * Returns name of the uploader provider class
     *
     * @return string
     */
    public static function getUploaderProvider($uploader)
    {
        return $className = __NAMESPACE__ . '\Uploaders\\' . studly_case($uploader) . 'UploaderServiceProvider';
    }



    /**
     * Returns blade for uploader
     *
     * @param array ...$params
     *
     * @return mixed
     */
    public static function uploader(... $params)
    {
        $uploaderCLass = self::getCurrentUploaderProvider();
        return $uploaderCLass::uploader(... $params);
    }



    /**
     * Returns blade of uploader for specified posttype
     *
     * @param string|integer|Posttype $posttype
     * @param array                   $switches
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public static function posttypeUploader($posttype, $switches = [])
    {
        $fileTypes      = ['video', 'image', 'audio', 'text', 'compressed'];
        $posttypePrefix = self::getPostTypeConfigPrefix();

        $posttype = FileManagerToolsServiceProvider::smartFindPosttype($posttype);
        if ($posttype->exists and $posttype->hasUploadPrerequisite()) {
            $posttypeSlug                           = $posttype->slug;
            $switches['externalFields']['posttype'] = $posttype->hashid;

            foreach ($fileTypes as $key => $fileType) {
                $fileTypes[$key] = $posttypePrefix . $posttypeSlug . '.' . $fileType;
            }

            if (count($fileTypes)) {
                return self::uploader($fileTypes, $switches);
            }
        }

        return null;
    }



    /**
     * Checks if uploading of a file type is activated or not
     *
     * @param        $fileType
     * @param string $userType
     *
     * @return mixed
     */
    public static function isActive($fileType, $followUp = true)
    {
        if ($followUp) {
            self::$currents['followUp'] = true;
        } else {
            self::$currents['followUp'] = false;
        }

        $result = self::followUpConfig(self::generateConfigPath($fileType) . ".status");

        self::$currents['followUp'] = self::$defaults['followUp'];
        return $result;
    }



    /**
     * Get a Config Value For a File Type from Upload Configs
     *
     * @param string $fileType
     * @param string $configName
     *
     * @return mixed
     */
    public static function getTypeRule($fileType, $configName)
    {
        return self::followUpConfig(self::generateConfigPath($fileType) . ".$configName");
    }



    /**
     * Get a Config Value For a Section from Upload Configs
     *
     * @param string $section
     * @param string $configName
     *
     * @return mixed
     */
    public static function getSectionRule($section, $configName)
    {
        $result = self::followUpConfig(self::generateConfigPath($section, true) . ".$configName");

        /**
         * Modify result if needed
         */
        switch ($configName) {
            case 'uploadDir':
                $result = self::rectifyDirectoryName($result);
                break;
        }

        return $result;
    }



    /**
     * Convert $fileTypeString to a meaning full array
     *
     * @param string $fileTypeString
     *
     * @return array
     */
    public static function translateFileTypeString($fileTypeString)
    {
        $fileTypeParts = array_reverse(explode_not_empty('.', $fileTypeString));
        if (!isset($fileTypeParts[1])) {
            $fileTypeParts[1] = self::$currents['configsSection'];
        }

        $fileTypeParts = array_reverse($fileTypeParts);

        return $fileTypeParts;
    }



    /**
     * Convert $sectionString to a meaning full array
     *
     * @param string $sectionString
     *
     * @return array
     */
    public static function translateSectionString($sectionString)
    {
        $sectionParts = array_reverse(explode_not_empty('.', $sectionString));
        if (!isset($sectionParts[0])) {
            $sectionParts[0] = self::$currents['configsSection'];
        }

        $sectionParts = array_reverse($sectionParts);

        return $sectionParts;
    }



    /**
     * Set Some Configs to Be Set to All Uploader Elements
     *
     * @param array|string $first  If this is string it will be assumed as config key
     * @param null|mixed   $second If $first is string this will be assumed as config value
     */
    public static function setDefaultJsConfigs($first, $second = null)
    {
        if (is_array($first)) {
            self::$defaults['jsConfigs'] = array_merge(self::$defaults['jsConfigs'], $first);
        } else {
            self::$defaults['jsConfigs'] = array_merge(self::$defaults['jsConfigs'], [$first => $second]);
        }
    }



    /**
     * Get Current Default JS Configs
     *
     * @param string $key Key Of Requested Config (If empty all JS Configs will be returned)
     *
     * @return array|mixed
     */
    public static function getDefaultJsConfigs($key = '')
    {
        if ($key and array_key_exists($key, self::$defaults['jsConfigs'])) {
            return self::$defaults['jsConfigs'][$key];
        }

        return self::$defaults['jsConfigs'];
    }



    /**
     * Returns $postTypeConfigPrefix (static variable of this class)
     *
     * @return string
     */
    public static function getPostTypeConfigPrefix()
    {
        return self::$statics['postTypeConfigPrefix'];
    }



    /**
     * Validates an UploadedFile with other info in $request argument
     *
     * @param Request $request
     *
     * @return bool|MessageBag
     */
    public static function validateFile($request, $returnErrors = true)
    {
        $file        = $request->file;
        $typeString  = $request->_uploadIdentifier;
        $sessionName = $request->_groupName;

        $validationRules    = self::getCompleteRules($request->_uploadIdentifier);
        $acceptedExtensions = $validationRules['acceptedExtensions'];

        if (!$acceptedExtensions or !is_array($acceptedExtensions) or !count($acceptedExtensions)) {
            $acceptedExtensions = [];
        }

        // @todo: There is a problem with file type validation in some file encodings (image, compressed and audio)
        $validator = Validator::make($request->all(), [
             'file' => [
                  'mimes:' . implode(',', $acceptedExtensions),
                  'max:' . ($validationRules['maxFileSize'] * 1024),
             ],
        ])->setAttributeNames(['file' => trans('filemanager::validation.attributes.file')])
        ;

        if (!$validator->fails()) {
            if (self::validateFileNumbers($sessionName, $typeString)) {
                return true;
            } else {
                return new MessageBag([
                     'file' => [
                          trans('filemanager::upload.error.limit'),
                     ],
                ]);
            }
        }

        if ($returnErrors) {
            return $validator->errors();
        }

        return false;
    }



    /**
     * Checks if files number for an uploader reached the limit
     *
     * @param string|array $sessionName
     * @param string       $typeString
     */
    public static function validateFileNumbers($sessionName, $fileType)
    {
        $max_files = self::getCompleteRules($fileType)['maxFiles'];
        $max_files = is_numeric($max_files) ? intval($max_files) : null;

        if (!session()->has($sessionName) or
             ($max_files === null) or
             ($max_files == -1) or
             (count(array_filter(session()->get($sessionName), function ($item) {
                      return $item['done'];
                  })) < $max_files)
        ) {
            return true;
        }
        return false;
    }



    /**
     * Returns upload rules for list of identifiers
     *
     * @param string|array $identifiers If string could be comma (,) separated
     *
     * @return array
     */
    public static function getCompleteRules($identifiers, $followUp = true)
    {
        if (is_string($identifiers)) {
            $identifiers = explode_not_empty(',', $identifiers);
        }

        $rules = [];
        foreach ($identifiers as $identifier) {
            if (!self::isActive($identifier, $followUp)) {
                continue;
            }
            $thisRules = self::followUpConfig(self::generateConfigPath($identifier));
            if (!$thisRules or !is_array($thisRules)) {
                continue;
            }

            $rules = self::mergeTypeConfigs($rules, $thisRules);
        }

        return $rules;
    }



    /**
     * Set Section to Read Upload Settings
     *
     * @param string $section
     *
     * @return void
     */
    public static function setConfigsSection($section)
    {
        self::$currents['configsSection'] = $section;
    }



    /**
     * Returns the section to Read upload settings.
     *
     * @return string
     */
    public static function getConfigSection()
    {
        return static::$currents['configsSection'];
    }



    /**
     * Rectifies directory name string relating on operating system
     *
     * @param string $directory
     *
     * @return mixed
     */
    public static function rectifyDirectoryName($directory)
    {
        return ($directory and is_string($directory)) ?
             preg_replace("/\/|\\\\/", DIRECTORY_SEPARATOR, $directory)
             : '';
    }



    /**
     * Sets $temporaryFolderName
     *
     * @param string $folder
     */
    public static function setTemporaryFolderName($folder)
    {
        self::$currents['temporaryFolderName'] = $folder;
    }



    /**
     * Returns Temporary Folder Name
     *
     * @return string
     */
    public static function getTemporaryFolderName()
    {
        return self::$currents['temporaryFolderName'];
    }



    /**
     * Returns Root Upload Folder Name
     *
     * @return string
     */
    public static function getRootUploadDir()
    {
        return self::$statics['rootUploadDir'];
    }



    /**
     * Disables Temporary Upload
     */
    public static function disableTemporaryUpload()
    {
        self::$currents['temporaryUpload'] = false;
    }



    /**
     * Enables Temporary Upload
     */
    public static function enableTemporaryUpload()
    {
        self::$currents['temporaryUpload'] = true;
    }



    /**
     * Checks if file will be uploaded in temporary folder or not
     *
     * @return boolean
     */
    public static function isTemporaryUploadEnabled()
    {
        return self::$currents['temporaryUpload'];
    }



    /**
     * Upload File to Specified Directory
     *
     * @param UploadedFile $file
     * @param string       $uploadDir Directory for Destination File
     *
     * @return int;
     */
    public static function uploadFile($file, $uploadDir, $externalFields = [])
    {
        $newName             = self::generateFileName() . '.' . $file->getClientOriginalExtension();
        $finalUploadDirParts = [self::$statics['rootUploadDir']];
        if (self::$currents['temporaryUpload']) {
            $finalUploadDirParts[] = self::$currents['temporaryFolderName'];
        }
        $finalUploadDirParts[] = $uploadDir;
        $finalUploadDir        = implode(DIRECTORY_SEPARATOR, $finalUploadDirParts);

        // Save uploaded file to db
        $saveData = array_merge([
             'physical_name' => $newName,
             'directory'     => $finalUploadDir,
        ], $externalFields);
        FilesFacades::makeDirectory($finalUploadDir, 0775, true, true);
        $id = FileModel::saveFile($file, $saveData);

        // Move uploaded file to target directory
        $file->move($finalUploadDir, $newName);

        return $id;
    }



    /**
     * Returns specified $fileName in requested version
     *
     * @param string $fileName Source File Name
     * @param string $newVersion
     *
     * @return null|string
     */
    public static function changeFileNameVersion($fileName, $newVersion)
    {
        // remove extension
        $ext      = FilesFacades::extension($fileName);
        $fileName = substr($fileName, 0, -(strlen($ext) + 1));

        $fileNameParts = explode(self::$statics['fileNameSeparator'], $fileName);
        if (count($fileNameParts) == 3) {
            $fileNameParts[2] = $newVersion;
            return implode(self::$statics['fileNameSeparator'], $fileNameParts) . '.' . $ext;
        }

        return null;
    }



    /**
     * Returns specified $url in requested version
     *
     * @param string $url Source File UrL
     * @param string $version
     *
     * @return mixed
     */
    public static function changeFileUrlVersion($url, $version)
    {
        $fileName   = pathinfo($url)['basename'];
        $newVersion = self::changeFileNameVersion($fileName, $version);

        if ($newVersion) {
            $newUrl  = str_replace($fileName, $newVersion, $url);
            $newPath = str_replace(url('/') . '/', '', $newUrl);

            if (FileServiceProvider::getFileObject($newPath)) {
                return $newUrl;
            } else {
                $posttype = self::findPosttypeFromVersionTitle($version);
                if ($posttype) {
                    //                    $configs = $posttype->
                }
            }
        }

        return $url;
    }



    /**
     * Generates a random name
     *
     * @param string $version  The Postfix tha Will Be Added at the End of the File Name
     * @param string $baseName Basic Part of the File Name (If empty basic part will be generated randomly)
     *
     * @return string
     */
    public static function generateFileName($version = 'original', $baseName = '')
    {
        if (array_key_exists($version, self::$versionsPostfixes)) {
            $version = self::$versionsPostfixes[$version];
        }

        if ($baseName and is_string($baseName)) {
            $basementPart = [$baseName];
        } else {
            $basementPart = [time(), str_random(self::$statics['randomNameLength'])];
        }

        return implode(self::$statics['fileNameSeparator'], array_merge($basementPart, [$version]));
    }



    /**
     * Guesses file type form its mime type
     *
     * @param string $mime_type
     *
     * @return string|null
     */
    public static function guessTypeFromMimeType($mime_type)
    {
        $specific_types = ['image/x-icon' => 'icon'];

        if (array_key_exists($mime_type, $specific_types)) {
            return $specific_types[$mime_type];
        }

        $groups = config('filemanager.file-types.groups-mimes');
        foreach ($groups as $group => $mimes) {
            if (in_array($mime_type, $mimes) !== false) {
                return $group;
            }
        }

        return null;
    }



    /**
     * @param UploadedFile $file
     * @param string       $fileName
     * @param string       $directory
     *
     * @return array
     */
    public static function generateRelatedFiles($file, $fileName, $directory)
    {
        $mimeType = $file->getMimeType();
        $fileType = substr($mimeType, 0, strpos($mimeType, '/'));

        $result = [];
        switch ($fileType) {
            case "image":
                {

                    // Create default thumbnail and extra files
                    $result = array_merge($result, self::generateSectionRelatedImages(
                         $file,
                         $fileName,
                         $directory,
                         static::getConfigSection())
                    );

                    // Create posttype thumbnail and extra files
                    if (starts_with(self::$currents['configsSection'], self::getPostTypeConfigPrefix())) {
                        $postTypeSlug   = str_after(self::$currents['configsSection'], self::getPostTypeConfigPrefix());
                        $versionsPrefix = self::generatePosttypeVersionPrefix($postTypeSlug);
                        if ($versionsPrefix) {
                            $result = array_merge($result, self::generateSectionRelatedImages(
                                 $file,
                                 $fileName,
                                 $directory,
                                 '',
                                 $versionsPrefix)
                            );
                        }
                    }

                    return $result;
                }
        }

        return $result;
    }



    /**
     * Returns fields that should be stored only for this file type
     *
     * @param UploadedFile $file
     *
     * @return array
     */
    public static function getFileTypeRelatedFields($file)
    {
        $mimeType = $file->getMimeType();
        $fileType = substr($mimeType, 0, strpos($mimeType, '/'));
        $result   = [];

        switch ($fileType) {
            case "image":
                {
                    list($result['image_width'], $result['image_height']) = getimagesize($file->getPathname());

                    return $result;
                }
        }

        return $result;
    }



    public static function getCurrentConfigSlug()
    {
        return self::$currents['posttypeConfigSlug'] ?: self::$defaults['posttypeConfigSlug'];
    }



    public static function setCurrentConfigSlug($configSlug)
    {
        self::$currents['posttypeConfigSlug'] = $configSlug;
    }



    public static function enableFollowingUp()
    {
        self::$currents['followUp'] = true;
    }



    public static function disableFollowingUp()
    {
        self::$currents['followUp'] = false;
    }



    public static function generatePosttypeVersionTitle($posttypeIdentifier, $version)
    {
        $prefix = self::generatePosttypeVersionPrefix($posttypeIdentifier);

        if ($prefix) {
            return $prefix . self::$statics['fileVersionTitleSeparator'] . $version;
        }

        return null;
    }



    public static function mergeTypeConfigs(...$arguments)
    {
        switch (count($arguments)) {
            case 0:
                return null;
                break;

            case 1:
                return $arguments[0];
                break;

            case 2:
                $arg1 = $arguments[0];
                $arg2 = $arguments[1];

                // Merge "acceptedExtensions"
                if (array_key_exists('acceptedExtensions', $arg2) and is_array($arg2['acceptedExtensions'])) {
                    if (isset($arg1['acceptedExtensions'])) {
                        $arg1['acceptedExtensions'] = array_merge($arg1['acceptedExtensions'],
                             $arg2['acceptedExtensions']);
                    } else {
                        $arg1['acceptedExtensions'] = $arg2['acceptedExtensions'];
                    }
                }

                // Merge "acceptedFiles"
                if (array_key_exists('acceptedFiles', $arg2) and is_array($arg2['acceptedFiles'])) {
                    if (isset($arg1['acceptedFiles']) and is_array($arg1['acceptedFiles'])) {
                        $arg1['acceptedFiles'] = array_merge($arg1['acceptedFiles'], $arg2['acceptedFiles']);
                    } else {
                        $arg1['acceptedFiles'] = $arg2['acceptedFiles'];
                    }
                }

                // Merge "maxFileSize"
                if (array_key_exists('maxFileSize', $arg2)) {
                    if (isset($arg1['maxFileSize'])) {
                        $arg1['maxFileSize'] = max($arg1['maxFileSize'], $arg2['maxFileSize']);
                    } else {
                        $arg1['maxFileSize'] = $arg2['maxFileSize'];
                    }
                }

                // Merge "maxFiles"
                if (array_key_exists('maxFiles', $arg2)) {
                    if (isset($arg1['maxFiles'])) {
                        $arg1['maxFiles'] = max($arg1['maxFiles'], $arg2['maxFiles']);
                    } else {
                        $arg1['maxFiles'] = $arg2['maxFiles'];
                    }
                }

                // Merge "icon"
                if (array_key_exists('icon', $arg2)) {
                    if (isset($arg1['icon'])) {
                        if ($arg1['icon'] != $arg2['icon']) {
                            $arg1['icon'] = 'file';
                        }
                    } else {
                        $arg1['icon'] = $arg2['icon'];
                    }
                }

                return $arg1;
                break;

            default:
                $rules = [];
                foreach ($arguments as $argument) {
                    $rules = self::mergeTypeConfigs($rules, $argument);
                }

                return $rules;
                break;
        }
    }



    public static function mergeConfigs(...$arguments)
    {
        switch (count($arguments)) {
            case 0:
                return null;
                break;

            case 1:
                return $arguments[0];
                break;

            case 2:
                $arg1 = $arguments[0];
                $arg2 = $arguments[1];


                //            dd($arg1,$arg2, __FILE__ . " - " . __LINE__);

                // Merge "uploadDir"
                if (array_key_exists('uploadDir', $arg2)) {
                    if (isset($arg1['uploadDir'])) {
                        if ($arg1['uploadDir'] != $arg2['uploadDir']) {
                            $arg1['uploadDir'] = self::getSectionRule('default', 'uploadDir');
                        }
                    } else {
                        $arg1['uploadDir'] = $arg2['uploadDir'];
                    }
                }

                // Merge "fileTypes"
                if (array_key_exists('fileTypes', $arg2) and is_array($arg2['fileTypes'])) {
                    if (isset($arg1['fileTypes']) and is_array($arg1['fileTypes'])) {
                        $resultFileTypes = [];
                        $allFileTypes    = array_unique(
                             array_merge(array_keys($arg1['fileTypes']), array_keys($arg2['fileTypes']))
                        );

                        foreach ($allFileTypes as $fileType) {
                            if (
                                 array_key_exists($fileType, $arg1['fileTypes']) and
                                 array_key_exists($fileType, $arg2['fileTypes'])
                            ) {
                                $arg1['fileTypes'][$fileType] = self::mergeTypeConfigs(
                                     $arg1['fileTypes'][$fileType],
                                     $arg2['fileTypes'][$fileType]
                                );
                            } elseif (array_key_exists($fileType, $arg2['fileTypes'])) {
                                $arg1['fileTypes'][$fileType] = $arg2['fileTypes'][$fileType];
                            }
                        }
                    } else {
                        $arg1['fileTypes'] = $arg2['fileTypes'];
                    }
                }


                //                dd($arg1, $arg2, __FILE__ . " - " . __LINE__);

                return $arg1;
                break;

            default:
                $rules = [];
                foreach ($arguments as $argument) {
                    $rules = self::mergeTypeConfigs($rules, $argument);
                }

                return $rules;
                break;
        }
    }



    /**
     * Generates a new image from $sourceFile, in size of $width*$height, at $pathname
     *
     * @param \Symfony\Component\HttpFoundation\File\File $sourceFile File Object to Make Clone From It
     * @param string                                      $pathname   Pathname to save new image at it
     * @param integer                                     $width      Width of Result Image
     * @param integer                                     $height     Height of Result Image
     *
     * @return  \Intervention\Image\Image|false
     */
    public static function createRelatedImage($sourceFile, $pathname, $width, $height)
    {
        try {
            $image_handler = ImageHandler::make($sourceFile->getPathname())->safeResize($width, $height);

            if ($image_handler) {
                return $image_handler->save($pathname);
            } else {
                return false;
            }
        } catch (NotReadableException $exception) {
            return false;
        }
    }



    /**
     * @param string $file_type
     *
     * @return bool
     */
    public static function isVersionable(string $file_type)
    {
        $generating_method_name = static::versionGeneratorMethodName($file_type);;

        return method_exists(static::class, $generating_method_name);
    }



    /**
     * @param string $file_type
     *
     * @return bool
     */
    public static function isNotVersionable(string $file_type)
    {
        return !static::isVersionable($file_type);
    }



    /**
     * @param string $file_type
     *
     * @return string
     */
    public static function versionGeneratorMethodName(string $file_type)
    {
        return 'createRelated' . studly_case($file_type);
    }



    /**
     * Follow up a config and search specified or default available value
     *
     * @param string $configPath Config Path (Dot Separated)
     * @param int    $checked    Number of Checked Levels
     *
     * @return mixed Found Config
     * @throws null if config not found
     */
    protected static function followUpConfig($configPath, $checked = 0)
    {
        $existedPath = self::findBestPath($configPath);
        if ($existedPath) {
            return Config::get($existedPath);
        }
    }



    /**
     * Searches for closest existed path
     *
     * @param string $configPath
     * @param int    $step Searching Step (Starts from 0)
     *
     * @return string
     */
    protected static function findBestPath($configPath, $step = 0)
    {
        if (starts_with($configPath, 'filemanager.upload.')) {
            $usableConfigPath = str_after($configPath, 'filemanager.upload.');
        } elseif (starts_with($configPath, 'upload.')) {
            $usableConfigPath = str_after($configPath, 'upload.');
        } else {
            $usableConfigPath = $configPath;
        }

        $parts           = explode_not_empty('.', $usableConfigPath);
        $foundConfigPath = self::configPartsToPath($parts);

        if (Config::has($foundConfigPath)) {
            return $foundConfigPath;
        } else {
            if (!self::$currents['followUp']) {
                config([$foundConfigPath => false]);
                return $foundConfigPath;
            }
            $configsSlug    = $parts[0];
            $configsRowSlug = $configPath . '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix();
            $configsRow     = model("setting")->grabSlug($configsRowSlug);

            if (
                 $configsRow and
                 $configsRow->exists and
                 isJson($configsRow->default_value) and
                 ($configs = json_decode($configsRow->default_value, true)) and
                 is_array($configs)
            ) {
                config([$foundConfigPath => $configs]);
                return $foundConfigPath;
            } else {
                $parts[0] = self::$defaults['configsSection'];
                return self::configPartsToPath($parts);
            }
        }
    }



    /**
     * Returns keys that should be replaced by default value in every step
     *
     * @param int $stepNumber
     * @param int $replacementRange Number of Fields That Can Be Replaced by the Default Value
     *
     * @return array
     */
    protected static function getReplacementKeys($stepNumber, $replacementRange)
    {
        $binary            = str_pad(decbin($stepNumber), $replacementRange, 0, STR_PAD_LEFT);
        $binaryArr         = str_split($binary);
        $filteredBinaryArr = array_filter($binaryArr, function ($var) {
            return $var;
        });
        $replacementKeys   = array_keys($filteredBinaryArr);

        return $replacementKeys;
    }



    /**
     * Generate Config Path String
     *
     * @param string $configPath
     * @param bool   $section <ul><li>TRUE: Path is For a Section</li><li>FALSE: Path is For a File Type</li></ul>
     *
     * @return string This function will return a dot separated string to use in accessing a config
     */
    protected static function generateConfigPath($configPath, $section = false)
    {
        if ($section) {
            $parts = self::translateSectionString($configPath);
        } else {
            $parts = self::translateFileTypeString($configPath);
            array_splice($parts, 1, 0, 'fileTypes');
        }

        self::fetchConfigFromDb($parts);

        return self::configPartsToPath($parts);
    }



    /**
     * If a config doesn't exists and it is possible to be read from db, it will be fetched from db.
     *
     * @param array $parts
     *
     * @return void
     */
    protected static function fetchConfigFromDb($parts)
    {
        if (!Config::has(self::configPartsToPath($parts))) {
            if (starts_with($parts[0], self::getPostTypeConfigPrefix())) {
                $postTypeSlug = str_after($parts[0], self::getPostTypeConfigPrefix());
                $postType     = posttype($postTypeSlug);
                $postType->spreadMeta();
                $configs = $postType->getUploadConfigs(self::getCurrentConfigSlug());
                if ($configs) {
                    $configPath = self::configPartsToPath(array_slice($parts, 0, 1));
                    config([$configPath => $configs]);
                }
            } else {
                if ($fetchedConfigs = ConfigsServiceProvider::getConfigData($parts[0])) {
                    $configPath = self::configPartsToPath(array_slice($parts, 0, 1));
                    config([$configPath => $fetchedConfigs]);
                }
            }
        }
    }



    /**
     * Generates config path from parts array
     *
     * @param array $parts
     *
     * @return string
     */
    protected static function configPartsToPath($parts)
    {
        return 'filemanager.upload.' . implode('.', $parts);
    }



    /**
     * Sets default configs
     */
    protected static function setDefaultConfigs()
    {
        config(['filemanager.upload.default' => static::findTempDefaultConfig()]);
    }



    /**
     * Finds and returns the temp default config to set it as the default config.
     *
     * @return bool|array
     */
    protected static function findTempDefaultConfig()
    {
        if (static::configsModelIsNotReady()) {
            return static::getAbsoluteUploadConfigs();
        }

        $db_config = static::findTempDefaultConfigFromDb();

        if ($db_config) {
            return $db_config;
        }

        return static::getAbsoluteUploadConfigs();
    }



    /**
     * Finds the default temp config from DB.
     *
     * @return bool|array
     */
    protected static function findTempDefaultConfigFromDb()
    {
        $config_row = static::getDefaultConfigsRow();

        if ($config_row->not_exists or !isJson($config_row->default_value)) {
            return false;
        }

        $config_value = json_decode($config_row->default_value, true);

        if (is_array($config_value)) {
            return $config_value;
        }

        return false;
    }



    /**
     * Returns the row of the default configs from database.
     *
     * @return \Modules\Yasna\Entities\Setting
     */
    protected static function getDefaultConfigsRow()
    {
        return setting(static::getDefaultConfigsSlug());
    }



    /**
     * Returns the slug of the default config in the DB.
     *
     * @return string
     */
    protected static function getDefaultConfigsSlug()
    {
        return ('default' . '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix());
    }



    /**
     * Weather the Setting model and its table are ready.
     *
     * @return bool
     */
    protected static function configsModelIsReady()
    {
        $class = MODELS_NAMESPACE . 'Setting';
        return (class_exists($class) and Schema::hasTable($class::tableName()));
    }



    /**
     * Weather the Setting model and its table are not ready.
     *
     * @return bool
     */
    protected static function configsModelIsNotReady()
    {
        return !static::configsModelIsReady();
    }



    /**
     * @return \Illuminate\Config\Repository|array
     */
    public static function getDefaultConfigs()
    {
        return config('filemanager.upload.default');
    }



    /**
     * Return the absolute upload configs right from the config file.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function getAbsoluteUploadConfigs()
    {
        return config('filemanager.upload.absolute');
    }



    protected static function generateSectionRelatedImages($file, $fileName, $directory, $section, $versionsPrefix = '')
    {
        $result = [];

        $thumbVersionTitle   = $versionsPrefix ?
             ($versionsPrefix . self::$statics['fileVersionTitleSeparator'] . 'thumb') :
             'thumb';
        $thumbName           = self::changeFileNameVersion($fileName, $thumbVersionTitle);
        $thumbPath           = $directory . DIRECTORY_SEPARATOR . $thumbName;
        $posttypeThumbConfig = self::generateConfigPath($section . '.image') . '.thumbnail';
        if (self::generateRelatedImageFromConfig($file, $thumbPath, $posttypeThumbConfig)) {
            $result['thumbnail'] = $thumbName;
        }

        $extraFiles = self::getTypeRule($section . '.image', 'extraFiles');
        if ($extraFiles and is_array($extraFiles) and count($extraFiles)) {
            foreach ($extraFiles as $version => $extraFile) {
                $clearedTitle        = clearFileVersionTitle($version);
                $versionTitle        = $versionsPrefix ?
                     ($versionsPrefix . self::$statics['fileVersionTitleSeparator'] . $clearedTitle) :
                     $clearedTitle;
                $extraFileName       = self::changeFileNameVersion($fileName, $versionTitle);
                $extraFilePath       = $directory . DIRECTORY_SEPARATOR . $extraFileName;
                $extraFileConfigPath = self::generateConfigPath($section . '.image') . '.extraFiles.' . $version;
                if (self::generateRelatedImageFromConfig($file, $extraFilePath, $extraFileConfigPath)) {
                    $result[$versionTitle] = $extraFileName;
                }
            }
        }

        return $result;
    }



    protected static function generateRelatedImageFromConfig($file, $filePath, $config)
    {
        $fileWidth  = self::followUpConfig($config . '.width');
        $fileHeight = self::followUpConfig($config . '.height');
        if ($fileWidth or $fileHeight) {
            self::createRelatedImage($file, $filePath, $fileWidth, $fileHeight);
            return true;
        }

        return false;
    }



    protected static function generatePosttypeVersionPrefix($posttypeIdentifier)
    {
        $posttype = FileManagerToolsServiceProvider::smartFindPosttype($posttypeIdentifier);

        if ($posttype and $posttype->exists) {
            return self::$statics['postTypeExtraFilesVersionPrefix'] .
                 self::$statics['fileVersionTitleSeparator'] .
                 $posttype->hashid;
        }

        return null;
    }



    protected static function findPosttypeFromVersionTitle($version)
    {
        if (starts_with($version, self::$statics['postTypeExtraFilesVersionPrefix'])) {
            $versionParts = explode(self::$statics['fileVersionTitleSeparator'], $version);
            if ($versionParts and is_array($versionParts) and (count($versionParts == 3))) {
                $posttype = Posttype::findByHashid($versionParts[1]);
                if ($posttype and $posttype->exists) {
                    return $posttype;
                }
            }
        }

        return null;
    }



    public static function setUploader($upload)
    {
        self::$defaults['uploader'] = $upload;
    }
}
