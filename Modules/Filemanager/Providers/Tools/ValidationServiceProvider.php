<?php

namespace Modules\Filemanager\Providers\Tools;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerRules();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Registers custom validation rules
     */
    protected function registerRules()
    {
        /**
         * Validation Rule : alpha_underscore
         */
        Validator::extend('alpha_underscore', function ($attribute, $value, $parameters, $validator) {
            // Set custom error
            $validator->customMessages = array_merge($validator->customMessages, [
                'alpha_underscore' => trans('filemanager::validation.alpha_underscore'),
            ]);

            return preg_match('/^[\w_]*$/', $value);
        });
    }
}
