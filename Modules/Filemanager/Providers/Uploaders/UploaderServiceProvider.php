<?php
/**
 * Created by PhpStorm.
 * User: Yasna-PC1
 * Date: 10/10/2017
 * Time: 06:12 PM
 */

namespace Modules\Filemanager\Providers\Uploaders;

use Illuminate\Support\ServiceProvider;

abstract class UploaderServiceProvider extends ServiceProvider
{
    protected static $defaultSwitches = [
        'id'                           => null, // The "id" attribute of uploader box element
        'varName'                      => null, // Identifier of Dropzone object element
        'dataAttributes'               => [], // Data attributes of uploader box element
        'target'                       => null, // The "id" of target element which uploaded files will be stored in
        'directUpload'                 => false, // If true, files will not be uploaded in temporary folder
        'callbackOnEachUploadComplete' => '', // Name of function that will be called after uploading of each file complete (get "file" as parameter)
        'callbackOnEachUploadSuccess'  => '', // Name of function that will be called after uploading of each file success (get "file" as parameter)
        'callbackOnEachUploadError'    => '', // Name of function that will be called after uploading of each file failed (get "file" as parameter)
        'callbackOnQueueComplete'      => '', // Name of function that will be called after uploading of all files in a queue complete
        'externalFields'               => [], // Extra Fields to be posted while uploading file
        'additionalStyles'             => [], // If true, uploader styles will be included
        'configName'                   => '__current__', // Slug of Config to be Validated with (if __current__ it will be gotten from UploadTools::getCurrentConfigSlug()
    ];
}
