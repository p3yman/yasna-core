<?php

namespace Modules\Filemanager\Providers\Uploaders;

interface UploaderInterface
{
    public static function uploader($fileTypes, $switches = []);
}
