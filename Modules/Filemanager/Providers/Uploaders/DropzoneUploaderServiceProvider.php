<?php

namespace Modules\Filemanager\Providers\Uploaders;

use Modules\Filemanager\Providers\UploadServiceProvider;

class DropzoneUploaderServiceProvider extends UploaderServiceProvider implements UploaderInterface
{
    private static $preloaderShown = false;


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }


    /**
     * Return a View Containing DropZone Uploader Element and Related JavaScript Codes
     *
     * @param string|array $fileTypeString
     * @param array        $switches
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function uploader($fileTypes, $switches = [])
    {
        $switches = array_normalize($switches, self::$defaultSwitches);

        if (is_string($fileTypes)) {
            $fileTypes = [$fileTypes];
        }


        $uploadConfig = UploadServiceProvider::getCompleteRules($fileTypes, true);


        // preloader view will be added to view only in generating first uploader
        if (!self::$preloaderShown) {
            $preloaderView = view('filemanager::layouts.uploader.dropzone.preloader', $switches);
            self::$preloaderShown = true;
        }

        foreach ($fileTypes as $fileTypeString) {
            if (UploadServiceProvider::isActive($fileTypeString)) {
                $fileTypeStringParts = UploadServiceProvider::translateFileTypeString($fileTypeString);

                if (isset($fileType)) {
                    if ($fileType != last($fileTypeStringParts)) {
                        $fileType = 'file';
                    }
                } else {
                    $fileType = last($fileTypeStringParts);
                }
                $uploadIdentifiers[] = implode('.', $fileTypeStringParts);
            }
        }

        return view('filemanager::layouts.uploader.dropzone.box', compact(
                'fileType',
                'preloaderView',
                'uploadIdentifiers',
                'uploadConfig'
            ) + $switches);
    }

    /**
     * Get Current Default JS Configs
     *
     * @param string $key Key Of Requested Config (If empty all JS Configs will be returned)
     *
     * @return array|mixed
     */
    public static function getDefaultJsConfigs(...$params)
    {
        return UploadServiceProvider::getDefaultJsConfigs(...$params);
    }
}
