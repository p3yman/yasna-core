<?php

namespace Modules\Filemanager\Providers;

use App\Models\Input;
use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\ServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;

class FileManagerToolsServiceProvider extends ServiceProvider
{
    /**
     * A List of Inputs which can contain an upload config
     *
     * @var Collection|null
     */
    protected static $uploading_inputs;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    protected static $available_posttypes;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }



    /**
     * @param Posttype $input
     *
     * @return boolean
     */
    public static function getPointData($point)
    {
        $output = [];
        if ($point instanceof Posttype) {
            $output = self::getPointSelfInfo($point, 'posttype');
        }

        return $output;
    }



    /**
     * @return array
     */
    public static function getAcceptableFileTypes()
    {
        return array_keys(config('filemanager.upload.absolute.fileTypes'));
    }



    /**
     * @param string $type
     *
     * @return bool
     */
    public static function isAcceptableFileType($type)
    {
        return in_array($type, self::getAcceptableFileTypes());
    }



    /**
     * @param $point
     * @param $instance
     *
     * @return array
     */
    private static function getPointSelfInfo($point, $instance)
    {
        $output                            = [];
        $output['instance']                = $instance;
        $output['key']                     = $point->hashid;
        $output['acceptedFilesCategories'] = self::getPointAcceptedFilesCategories($point);

        switch ($instance) {
            case 'posttype':
                $output['title'] = ($point->titleIn(getLocale()) ?? $point->title);
                break;
            case 'folder':
                $output['title'] = $point->title ?: trans('front.unnamed', [], $point->locale);
                break;
            case 'category':
                $output['title'] = $point->title;
                break;
        }
        return $output;
    }



    /**
     * @param $point
     *
     * @return array
     */
    private static function getPointAcceptedFilesCategories($point)
    {
        if ($point instanceof Posttype) {
            $configSlug     = UploadServiceProvider::getPostTypeConfigPrefix() . $point->slug;
            $fileTypesArray = UploadServiceProvider::getSectionRule($configSlug, 'fileTypes');
            if ($fileTypesArray and is_array($fileTypesArray)) {
                return array_keys($fileTypesArray);
            } else {
                return [];
            }
        }
    }



    /**
     * Find posttype with multiple types of identifiers
     *
     * @param         $identifier
     * @param boolean $checkExactId If false, posttype will not be searched with id of db table
     *
     * @return \App\Models\Posttype
     */
    public static function smartFindPosttype($identifier, $checkExactId = false)
    {
        if ($identifier instanceof Posttype) {
            $posttype = $identifier;
        } elseif (is_numeric($identifier) and $checkExactId) {
            $posttype = Posttype::find($identifier);
        } elseif (count($dehashed = hashid_decrypt($identifier, 'ids')) and
             is_numeric($id = $dehashed[0])
        ) {
            $posttype = Posttype::find($id);
        } else {
            $posttype = posttype($identifier);
        }

        if ($posttype and $posttype->exists) {
            return $posttype;
        }

        return new Posttype();
    }



    /**
     * Returns a list of inputs which can contain an upload config.
     *
     * @return Collection
     */
    public static function getUploadingInputs()
    {
        if (!static::$uploading_inputs) {
            $postfix                  = ConfigsServiceProvider::getUploadRelatedInputsPostfix();
            static::$uploading_inputs = model('input')->where('slug', 'like', '%' . $postfix)->get();
        }

        return static::$uploading_inputs;
    }



    /**
     * Returns a list of slugs of inputs which can contain an upload config.
     *
     * @return array
     */
    public static function getUploadingInputsSlugs()
    {
        return static::getUploadingInputs()->pluck('slug')->toArray();
    }



    /**
     * Returns a list of available file manager post types for current client
     *
     * @return Collection
     */
    protected static function findAvailablePosttypes()
    {
        $post_types = posttype()
             ->whereHas('inputs', function ($query) {
                 $query->whereIn('slug', static::getUploadingInputsSlugs());
             })
             ->orderBy('title')
             ->get()
        ;

        $post_types->each(function ($postType, $key) use ($post_types) {
            $postType->spreadMeta();
            // If current user can do any of "create", "edit" or "publish"
            // and postType has "upload_configs
            // we will keep posttype,
            // else we will forget it.
            if (
                 (!$postType->hasUploadPrerequisite(
                      request()->input('configName') ?: '*',
                      request()->input('fileType') ?: '*'
                 )) or
                 (
                      !user()->as('admin')->can('file-manager.*') and
                      !$postType->can('create') and
                      !$postType->can('edit') and
                      !$postType->can('publish')
                 )
            ) {
                $post_types->forget($key);
            }
        });

        return $post_types;
    }



    /**
     * @return Collection
     */
    public static function getAvailablePosttypes()
    {
        if (!static::$available_posttypes) {
            static::$available_posttypes = static::findAvailablePosttypes();
        }

        return static::$available_posttypes;
    }



    /**
     * @return Collection
     */
    public static function availablePosttypes()
    {
        return static::getAvailablePosttypes();
    }



    /**
     * @return bool
     */
    public static function fileManagerIsAvailable()
    {
        return boolval(static::getAvailablePosttypes()->count());
    }



    /**
     * @return string
     */
    public static function getPublicFolder()
    {
        $public_path = rtrim(public_path(), DIRECTORY_SEPARATOR);
        $base_path   = rtrim(base_path(), DIRECTORY_SEPARATOR);

        return trim(str_after($public_path, $base_path), DIRECTORY_SEPARATOR);
    }



    /**
     * Finds an input with its slug from cache or DB
     *
     * @param string $input_slug
     *
     * @return Input
     * @throws \Exception
     */
    public static function findInputBySlug($input_slug)
    {
        return cache()->remember($input_slug, 10, function () use ($input_slug){
            return model('input', $input_slug);
        });
    }
}
