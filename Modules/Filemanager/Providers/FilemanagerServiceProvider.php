<?php

namespace Modules\Filemanager\Providers;

use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageServiceProvider;
use Modules\Filemanager\Providers\Tools\ValidationServiceProvider;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Schedules\RemoveTemporaryFilesSchedule;
use Modules\Yasna\Services\YasnaProvider;

class FilemanagerServiceProvider extends YasnaProvider
{
    protected static $moduleName = "filemanager";




    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerUpstreamServices();
        $this->registerManagePublicServices();
        $this->registerPostEditorHandlers();
        $this->setDefaults();
        $this->registerSideBar();
        $this->registerManageInputBlades();
        $this->registerProviders();
        $this->registerAliases();
        $this->registerRoleSampleModules();
        $this->registerSchedules();
        $this->registerManageAssets();
    }



    /**
     * register providers
     *
     * @return void
     */
    protected function registerProviders()
    {
        $this->addProvider(FileManagerToolsServiceProvider::class);
        $this->addProvider(UploadServiceProvider::class);
        $this->addProvider(ValidationServiceProvider::class);
        $this->addProvider(ImageServiceProvider::class);
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('file-manager')
             ->value('create ,  edit ,  delete ,')
        ;
    }



    /**
     * register schedules
     *
     * @return void
     */
    protected function registerSchedules()
    {
        $schedules = [
             RemoveTemporaryFilesSchedule::class,
        ];
        foreach ($schedules as $schedule) {
            $this->addSchedule($schedule);
        }
    }



    /**
     * register aliases
     *
     * @return void
     */
    protected function registerAliases()
    {
        $this->addAlias('FileManagerTools', FileManagerToolsServiceProvider::class);
        $this->addAlias('UploadTools', UploadServiceProvider::class);
        $this->addAlias('FileTools', FileServiceProvider::class);
        $this->addAlias('ConfigTools', ConfigsServiceProvider::class);
        $this->addAlias('FileManager', FilemanagerServiceProvider::class);
    }



    /**
     * register model traits
     *
     * @return void
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait("Filemanager:PosttypeFileTrait")->to("Posttype")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("Filemanager:PostFileTrait")->to("Post")
        ;


        $this->addModelTrait('PostFilemanagerResourcesTrait', 'Post');
        $this->addModelTrait('SettingFileManagerTrait', 'Setting');
    }



    /**
     * register upstream services
     *
     * @return void
     */
    protected function registerUpstreamServices()
    {
        service("manage:upstream_tabs")
             ->add("fileManager")
             ->link("file-manager")
             ->trans(self::$moduleName . "::general.title-page")
             ->method(self::$moduleName . ":UpstreamController@configs")
             ->order(47)
        ;
    }



    /**
     * register manage public services
     *
     * @return void
     */
    protected function registerManagePublicServices()
    {
        // Js Files
        module("manage")
             ->service("template_assets")
             ->add("toolsJs")
             ->link(self::$moduleName . ":js/tools.min.js")
             ->order(52)
        ; // @todo: to be moved to "template_bottom_assets" service


        // File Manager Modal
        module("manage")
             ->service("template_assets")
             ->add("fileManagerModalCss")
             ->link(self::$moduleName . ":css/manage-file-manager-modal.min.css")
             ->order(45)
        ;
        module("manage")
             ->service("template_assets")
             ->add("fileManagerModalJs")
             ->link(self::$moduleName . ":js/file-manager-modal.min.js")
             ->order(51)
        ; // @todo: to be moved to "template_bottom_assets" service
        module("manage")
             ->service("modals")
             ->add("fileManagerModalBlade")
             ->blade(self::$moduleName . "::presenters.file-manager-modal")
             ->order(1)
        ;

        // Uploader
        module("manage")
             ->service("template_assets")
             ->add("uploaderCss")
             ->link(self::$moduleName . ":css/manage-file-manager-uploader.min.css")
             ->order(45)
        ;
    }



    /**
     * register post editor handlers
     *
     * @return void
     */
    protected function registerPostEditorHandlers()
    {
        module("posts")
             ->service("editor_handlers")
             ->add(self::$moduleName)
             ->method(self::$moduleName . ":handleEditorPanel")
        ;

        service('posts:save_gates')
             ->add()
             ->method('filemanager:Posts\PostsController@saveGate')
        ;
    }



    /**
     * set default upload settings
     *
     * @return void
     */
    protected function setDefaults()
    {
        UploadServiceProvider::setDefaults();
    }



    /**
     * register sidebar
     *
     * @return void
     */
    protected function registerSideBar()
    {
        service("manage:sidebar")
             ->add("fileManagerSideBarMenu")
             ->blade(self::$moduleName . "::presenters.manage-sidebar-menu")
             ->order(34)
        ;
    }



    /**
     * register input blades
     *
     * @return void
     */
    protected function registerManageInputBlades()
    {
        module("manage")
             ->service("file_manager")
             ->add("single_photo")
             ->blade(self::$moduleName . "::presenters.input-photo")
        ;
        module("manage")
             ->service("file_manager")
             ->add("single_file")
             ->blade(self::$moduleName . "::presenters.input-file")
        ;
    }



    /**
     * Registers manage assets
     */
    protected function registerManageAssets()
    {
        module('manage')
             ->service('template_assets')
             ->add('dropzone-js')
             ->link($this->module()->getAssetPath('js/dropzone.min.js'))
             ->order(43)
        ;
    }



    /**
     * handle editor panel
     *
     * @return void
     */
    public static function handleEditorPanel()
    {
        // Featured Image Blade in the Side Bar
        module("posts")
             ->service("editor_side")
             ->add("featuredImageSelector")
             ->blade(self::$moduleName . "::presenters.post-editor-featured-image-selector")
             ->order(32)
        ;

        // Attachments Blade
        module("posts")
             ->service("editor_main")
             ->add("attachmentsUploadingArea")
             ->blade(self::$moduleName . "::presenters.post-editor-attachments-uploader")
             ->order(32)
        ;
    }



    /**
     * get config combo
     *
     * @return array
     */
    public static function getConfigsCombo()
    {
        $blankItem = ['id' => '', 'title' => ''];
        $data      = ConfigsServiceProvider::getConfigsCombo();

        return array_merge([$blankItem], $data);
    }



    /**
     * Returns a new instance of the specified provider.
     *
     * @param string|null $provider
     *
     * @return ServiceProvider
     */
    public function getProviderInstance(?string $provider = null)
    {
        if (!$provider) {
            return $this;
        }

        $class = __NAMESPACE__ . '\\' . $provider;

        return new $class($this->app);
    }
}
