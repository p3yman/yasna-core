<?php

namespace Modules\Filemanager\Providers\Upstream;

use App\Models\Setting;
use Exception;
use Illuminate\Support\ServiceProvider;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;

class ConfigsServiceProvider extends ServiceProvider
{
    // The string that should be added to "inputs" that are related to file upload
    protected static $uploadRelatedInputsPostfix = 'upload_configs';
    // List of slugs of configs that should't be deleted
    protected static $requiredConfigsSlugsShort = ['default'];
    // List of slugs of inputs that should't be deleted
    protected static $requiredConfigsSlugsFull = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }



    /**
     * Find config with multiple types of identifiers
     *
     * @param Setting|string|integer $identifier
     * @param boolean                $checkExactId If false, setting will not be searched with id of db table
     *
     * @return Setting
     */
    public static function smartFindConfig($identifier, $switch = [])
    {
        $switch = array_default($switch, [
             'checkExactId' => false,
        ]);

        $postfix = '_' . self::getUploadRelatedInputsPostfix();

        if ($identifier instanceof Setting) {
            $found = $identifier;
        } elseif (is_numeric($identifier) and $switch['checkExactId']) {
            $found = static::findConfigById($identifier);
        } elseif ($dehashed = hashid($identifier) and
             is_numeric($id = $dehashed)
        ) {
            $found = static::findConfigById($id);
        } else {
            if (ends_with($identifier, $postfix)) {
                $found = setting($identifier);
            } else {
                $found = setting($identifier . $postfix);
            }
        }

        if ($found and
             $found->exists and
             ends_with($found->slug, $postfix)
        ) {
            return $found;
        }

        return new Setting();
    }



    /**
     * Returns a setting model.
     * (Read it from cache or find it from the DB)
     *
     * @param string|int $id
     *
     * @return mixed
     * @throws Exception
     */
    public static function findConfigById($id)
    {
        return cache()->remember("setting-$id", 10, function () use ($id) {
            return model('setting')->grabId($id);
        });
    }



    /**
     * Returns array of required configs's slugs
     * Note: Required configs can't be deleted and their slug can't be changed
     */
    public static function requiredConfigs()
    {
        if (!count(self::$requiredConfigsSlugsFull)) {
            self::$requiredConfigsSlugsFull = array_map(function ($item) {
                return $item . '_' . self::getUploadRelatedInputsPostfix();
            }, self::$requiredConfigsSlugsShort);
        }
        return self::$requiredConfigsSlugsFull;
    }



    /**
     * Checks if a config is required or not
     *
     * @param $config
     */
    public static function isRequired($config)
    {
        $config   = self::smartFindConfig($config);
        $required = self::requiredConfigs();

        if (in_array($config->slug, $required)) {
            return true;
        }

        return false;
    }



    /**
     * Returns the postfix of the inputs which are related to upload.
     *
     * @return string
     */
    public static function getUploadRelatedInputsPostfix()
    {
        return self::$uploadRelatedInputsPostfix;
    }



    /**
     * Returns slug of a config to search in settings table
     *
     * @param string $configSlug
     *
     * @return string
     */
    public static function configToInput($configSlug)
    {
        if (ends_with($configSlug, self::getUploadRelatedInputsPostfix())) {
            return $configSlug;
        }

        return $configSlug . '_' . self::getUploadRelatedInputsPostfix();
    }



    /**
     * Returns the array to be used in the configs combo.
     *
     * @return array
     */
    public static function getConfigsCombo()
    {
        /*-----------------------------------------------
        | Models
        */
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();
        $configs = Setting::select('id', 'title')
                          ->where('slug', 'like', '%\_' . $postfix)
                          ->orderBy('title', 'asc')
                          ->get()
                          ->toArray()
        ;

        return $configs;
    }



    /**
     * Returns the value of the specified config.
     *
     * @param Setting|string|integer $identifier
     *
     * @return array|null
     */
    public static function getConfigData($identifier)
    {
        $config = self::smartFindConfig($identifier, ['checkExactId' => true]);

        if ($config->not_exists) {
            return null;
        }

        $decoded_value = json_decode($config->default_value, true);

        if (is_array($decoded_value)) {
            return $decoded_value;
        }

        return null;
    }
}
