<?php

namespace Modules\Filemanager\Providers;

use App\Models\File as FileModel;
use Illuminate\Support\Facades\File as FilesFacades;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\HttpFoundation\File\File;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }



    /**
     * Remove File Physically
     *
     * @param string|FileModel $file
     * @param boolean          $onlyTemp If true, file will be removed only in temp status
     *
     * @return bool
     */
    public static function removeFile($file, $switch = [])
    {
        $switch = array_default($switch, [
             'onlyTemp' => true,
        ]);

        $file = FileServiceProvider::smartFindFile($file, $switch);

        if ($file->exists and (!$switch['onlyTemp'] or $file->hasStatus('temp'))
        ) {
            $pathname = $file->pathname;

            $relatedFiles = $file->related_files_pathname;
            if ($relatedFiles and is_array($relatedFiles)) {
                foreach ($relatedFiles as $relatedFilePath) {
                    if (FilesFacades::exists($relatedFilePath)) {
                        FilesFacades::delete($relatedFilePath);
                    }
                }
            }

            $file->forceDelete();

            if (FilesFacades::exists($file->pathname)) {
                FilesFacades::delete($pathname);
            }

            return true;
        } else {
            return false;
        }
    }



    /**
     * Returns an <img /> element containing proper image file
     *
     * @param string|FileModel $file     File Identifier
     * @param string           $version  Version of file to be shown
     * @param array            $switches Switches to be user in showing file
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function getFileView($file, $version = 'original', $switches = [])
    {
        $switches = array_normalize($switches, [
             'style'           => [],
             'width'           => null,
             'height'          => null,
             'class'           => [],
             'otherAttributes' => [],
             'dataAttributes'  => [],
             'extra'           => '',
        ]);

        $file = self::smartFindFile($file, ['checkExactId' => true]);
        if ($file->exists) {
            $file->spreadMeta();
            $fileObj = self::getFileObject($file->pathname);
            if ($fileObj) {
                if (self::isViewable($fileObj)) {
                    $relatedFiles = $file->related_files;

                    if (
                        // version exists in db
                         array_key_exists($version, $relatedFiles) and
                         // version exists in storage
                         self::getFileObject(
                              $versionPathname = $file->directory . DIRECTORY_SEPARATOR . $relatedFiles[$version])
                    ) {
                        $pathname = $versionPathname;
                    } else {
                        $pathname = $file->pathname;
                    }

                    $imgUrl = url($pathname);
                } else {
                    $fileType = substr($file->mime_type, 0, strpos($file->mime_type, '/'));
                    switch ($fileType) {
                        case "video":
                            $imageName = 'file-video-o.svg';
                            break;
                        case "audio":
                            $imageName = 'file-audio-o.svg';
                            break;
                        case "text":
                        case "application":
                        case "docs":
                            $imageName = 'file-text-o.svg';
                            break;
                        default:
                            $imageName = 'file-o.svg';
                            break;
                    }


                    $imgUrl = Module::asset('filemanager:image/' . $imageName);
                }

                $switches['otherAttributes']['alt']   = ($switches['otherAttributes']['alt'] ?? $file->alternative);
                $switches['otherAttributes']['title'] = ($switches['otherAttributes']['title'] ?? $file->title);
            }
        }

        if (isset($imgUrl)) {
            $fileExisted = true;
        } else {
            $fileExisted = false;
            $imgUrl      = Module::asset('filemanager:image/chain-broken.svg');
        }

        return view(
             'filemanager::layouts.widgets.img-element',
             array_merge(compact('imgUrl', 'fileExisted'), $switches));
    }



    /**
     * Checks if specified file is an object
     *
     * @param \Illuminate\Http\File|File $file File Identifier
     *
     * @return bool
     */
    public static function isImage($file)
    {
        $validator = Validator::make([
             'file' => $file,
        ], [
             'file' => 'image',
        ]);

        return $validator->passes();
    }



    /**
     * Checks if specified file is an icon
     *
     * @param \Illuminate\Http\File|File $file File Identifier
     *
     * @return bool
     */
    public static function isIcon($file)
    {
        $validator = Validator::make([
             'file' => $file,
        ], [
             'mimes' => 'ico',
        ]);

        return $validator->passes();
    }



    /**
     * Checks if specified file is viewable
     *
     * @param \Illuminate\Http\File|File $file File Identifier
     *
     * @return bool
     */
    public static function isViewable($file)
    {
        return (static::isImage($file) or static::isIcon($file));
    }



    /**
     * Find file with multiple types of identifiers
     *
     * @param FileModel|integer|string $identifier
     * @param boolean                  $checkExactId If false, file will not be searched with id of db table
     *
     * @return FileModel
     */
    public static function smartFindFile($identifier, $switch = [])
    {
        $switch = array_default($switch, [
             'checkExactId' => false,
        ]);

        if ($identifier instanceof FileModel) {
            $file = $identifier;
        } elseif (is_numeric($identifier) and $switch['checkExactId']) {
            $file = FileModel::find($identifier);
        } elseif (count($dehashed = hashid_decrypt($identifier, 'ids')) and
             is_numeric($id = $dehashed[0])
        ) {
            $file = FileModel::findByHashid($identifier, true, $switch) ?: new FileModel();
        } else {
            $originalUrl = UploadServiceProvider::changeFileUrlVersion($identifier, 'original');
            $file        = self::findFileByUrl($originalUrl);

            if (!$file or !$file->exists) {
                $file = new FileModel();
            }
        }

        return $file;
    }



    /**
     * Returns file url
     * If the file doesn't exist on db, it will be return "null"
     *
     * @param string|FileModel $file File Identifier
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public static function getFileUrl($file)
    {
        $file = self::smartFindFile($file, ['checkExactId' => true]);
        if ($file->exists) {
            return url($file->pathname);
        }
        return null;
    }



    /**
     * Returns \Symfony\Component\HttpFoundation\File\File which exists on $pathname
     * If there isn't any file in specified pathname, it will be return "null"
     *
     * @param string $pathname Pathname of file to create file object
     *
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    public static function getFileObject($pathname)
    {
        if (FilesFacades::exists($pathname)) {
            return new File($pathname);
        }

        return null;
    }



    /**
     * Shows file size in suitable format
     *
     * @param FileModel|integer|string $file
     *
     * @return string
     */
    public static function echoFileSize($file)
    {
        $file = self::smartFindFile($file);

        return formatBytes($file->size);
    }



    /**
     * Returns file eloquent that matched with $pathname
     *
     * @param string $pathname Pathname (starting after public folder)
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function findFileByPathname($pathname)
    {
        if (starts_with($pathname, '/')) {
            $pathname = str_after($pathname, '/');
        }

        $conditions['directory']     = pathinfo($pathname, PATHINFO_DIRNAME);
        $conditions['physical_name'] = pathinfo($pathname, PATHINFO_BASENAME);
        $file                        = FileModel::where($conditions)->first();

        if ($file and $file->exists) {
            return $file;
        }

        return null;
    }



    /**
     * Find file by its url
     *
     * @param string $url
     *
     * @return \Illuminate\Database\Eloquent\Model|FileServiceProvider|null
     */
    public static function findFileByUrl($url)
    {
        $pathname = str_after($url, url('/'));
        return self::findFileByPathname($pathname);
    }



    /**
     * Permanently save a file
     *
     * @param string $file
     *
     * @return bool
     */
    public static function moveFromTemp($file)
    {
        $file = self::smartFindFile($file, ['checkExactId' => true]);

        if ($file->exists) {
            if (FilesFacades::exists($file->pathname) and $file->is_in_temporary_folder) {
                $newDir = str_replace(
                     UploadServiceProvider::getTemporaryFolderName() . DIRECTORY_SEPARATOR,
                     '',
                     $file->directory
                );

                $newFile = new File($file->pathname);
                if ($file->related_files) {
                    foreach ($file->related_files as $key => $relatedFileName) {
                        $relatedFilePathname = implode(DIRECTORY_SEPARATOR, [
                             $file->directory,
                             $relatedFileName,
                        ]);
                        $relatedFile         = new File($relatedFilePathname);
                        $relatedFile->move($newDir);
                    }
                }

                $newFile->move($newDir);
                $file->directory = $newDir;
            }

            if ($file->hasStatus('temp')) {
                $file->setStatus('used');
            }

            $file->save();
        }

        return $file->hasStatus('used');
    }
}
