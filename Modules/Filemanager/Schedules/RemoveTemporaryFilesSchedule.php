<?php namespace Modules\Filemanager\Schedules;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Yasna\Services\YasnaSchedule;

class RemoveTemporaryFilesSchedule extends YasnaSchedule
{
    /**
     * Custom Handle for Additional Options
     */
    protected function customHandle()
    {
        $this->callbackEvent()->cron('0 */6 * * *');
    }



    /**
     * Do the job!
     */
    protected function job()
    {
        foreach ($this->findTemporaryFiles() as $file) {
            $this->handleFile($file);
        }
    }



    /**
     * @return Collection
     */
    protected function findTemporaryFiles()
    {
        return model('file')
             ->where(function ($query) {
                 $query->whereStatus(model('file')::getStatusValue('temp'))
                       ->orWhere('directory', 'like', 'uploads/temp%')
                 ;
             })
             ->where('created_at', '<', Carbon::now()->subHours(6))
             ->limit(50)
             ->get()
             ;
    }



    /**
     * Handle a single temporary file
     *
     * @param File $file
     */
    protected function handleFile(File $file)
    {
        if ($this->fileIsUsedInPosts($file)) {
            fileManager()->file($file->hashid)->permanentSave();
        } else {
            FileServiceProvider::removeFile($file, ['onlyTemp' => false]);
        }
    }



    /**
     * @param File $file
     *
     * @return bool
     */
    protected function fileIsUsedInPosts(File $file)
    {
        return boolval(model('post')
             ->where('featured_image', ($fileHashid = $file->hashid))
             ->orWhere('meta', 'like', '%"' . $fileHashid . '"%')
             ->count());
    }



    /**
     * @return string
     */
    protected function frequency()
    {
        return 'everyMinute';
    }



    protected function withoutOverlapping()
    {
        return true;
    }
}
