<?php
return [
     "site_title"     => "Site Name",
     "contact_button" => [
          "tooltip" => "Inquire about depoint.com",
          "text"    => "Contact Us for Details",
     ],
     "call_to_action" => "Check Availability",
     "credit"         => "© Yasnateam",
     "services"       => [
          "title"      => "Choose Domain Only, Web Packages, or Other Services",
          "learn_more" => "Learn More",
     ],
     "footer_nav"     => [
          "terms"  => "Terms of Use",
          "policy" => "Privacy Policy",
     ],

];
