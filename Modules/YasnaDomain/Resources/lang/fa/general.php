<?php
return [
     "site_title"     => "عنوان سایت",
     "contact_button" => [
          "tooltip" => "درباره این دومین بپرسید.",
          "text"    => "برای اطلاعات بیشتر تماس بگیرید",
     ],
     "call_to_action" => "بررسی موجودی",
     "credit"         => "© یسناتیم",
     "services"       => [
          "title"      => "انتخاب دومین، پکیج‌ها یا خدمات",
          "learn_more" => "اطلاعات بیشتر",
     ],
     "footer_nav"     => [
          "terms"  => "شرایط ما",
          "policy" => "سیاست حریم خصوصی",
     ],

];
