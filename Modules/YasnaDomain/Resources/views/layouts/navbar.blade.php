@php
	$logo_hashid = get_setting('site_logo');
	$site_logo = fileManager()
					->file($logo_hashid)
					->getUrl();
@endphp

<div class="navbar navbar-inverse" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<h1>
				<a href="{{ url("/") }}">
					<img class="logo" src="{{ $site_logo or "" }}" alt="logo">
				</a>
			</h1>
		</div>
		<a class="btn btn-primary inquire navbar-right"
		   href="https://yasna.team"
		   title="{{ trans('yasnadomain::general.contact_button.tooltip') }}" role="button">
			<span class="glyphicon glyphicon-envelope"></span>
			{{ trans('yasnadomain::general.contact_button.text') }}
		</a>
	</div>
</div>
