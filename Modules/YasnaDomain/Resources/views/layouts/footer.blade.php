@php
	$hashid = get_setting('site_logo');
	$logo_src = fileManager()->file($hashid)->getUrl();
@endphp

<div class="footer">
	<div class="container">
		<div class="pull-right">
			<a href="{{ url('/') }}" title="eWeb Development">
				<img class="logo" src="{{ $logo_src }}">
			</a>
		</div>

		<p>
			<a href="https://yasna.team">
				{{ trans('yasnadomain::general.credit') }}
			</a>
			{{--<span class="links">--}}
				{{--| <a href="#">{{ trans('yasnaDomain::general.footer_nav.terms') }}</a>--}}
				{{--| <a href="#">{{ trans('yasnaDomain::general.footer_nav.policy') }}</a>--}}
			{{--</span>--}}
		</p>
	</div>

	{{--{!! Html::script(Module::asset('yasnaDomain:js/us.js')) !!}--}}
	{{--<img src="{{ Module::asset('yasnaDomain:images/pv.gif') }}">--}}


</div>
