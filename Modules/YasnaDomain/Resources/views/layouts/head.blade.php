<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Module YasnaDomain</title>

	<!-- Styles -->
	{!! Html::style(Module::asset('yasnadomain:libs/bootstrap/bootstrap.css')) !!}
	@if(isLangRtl())
		{!! Html::style(Module::asset('yasnadomain:libs/bootstrap/bootstrap.rtl.min.css')) !!}
		{!! Html::style(Module::asset('yasnadomain:css/style-fa.min.css')) !!}
	@else
		{!! Html::style(Module::asset('yasnadomain:css/style-en.min.css')) !!}
	@endif
	<!-- !END Styles -->

	<!-- Scripts -->
	{!! Html::script(Module::asset('yasnadomain:libs/jquery/jquery.min.js')) !!}
	{!! Html::script(Module::asset('yasnadomain:js/eweb.js')) !!}
	<!-- !END Scripts -->

	@yield('top-assets')
</head>