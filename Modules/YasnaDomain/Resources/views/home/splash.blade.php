@php
	$main_post = $posts['main']
@endphp
<div class="splash">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 col-md-6 section" style="height: 450px;">
				<h2>
					{{ parse_url(request()->url())['host'] }}<br>
					<span style="font-size: 70%">
						{{ $main_post->title2 }}
					</span>
				</h2>
				<div class="splash-text">
					{!! $main_post->text !!}
				</div>
				@if(isset($main_post->link ) and $main_post->link )
				<p>
					<a class="inquire" href="{{ $main_post->link }}" role="button">
						<span class="glyphicon glyphicon-play"></span>
						{{ trans('yasnadomain::general.call_to_action') }}
					</a>
				</p>
				@endif
				<br>
			</div>
			<div class="col-sm-5 col-md-6  hidden-xs image section" style="height: 450px;">
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $('.section').equalizeHeights();
</script>