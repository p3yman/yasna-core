@php
	$services = $posts['services']
@endphp
<div class="services">
	<div class="container">
		<div class="intro">
			<h3>{{ trans('yasnadomain::general.services.title') }}</h3>
		</div>

		<div class="row blurbs">
			@foreach($services as $service)
				@php
					$image = fileManager()->file($service->featured_image)->getUrl();
				@endphp
			<div class="col-sm-6 col-md-4">
				<div class="blurb">
					<img class="img-responsive" src="{{ $image }}">
					<h4>{{ $service->title }}</h4>
					<div>
						{!! $service->text !!}
					</div>
					@if(isset($service->link) and $service->link)
					<a class="btn btn-success btn-sm" href="{{ $service->link }}"
					   role="button">
						{{ trans('yasnadomain::general.services.learn_more') }}
					</a>
					@endif
					<br>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	</div>