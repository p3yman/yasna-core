@extends('yasnadomain::layouts.plane')

@section('content')
	@include('yasnaDomain::layouts.navbar')
	@include('yasnaDomain::home.splash')
	@include('yasnaDomain::home.services')
	@include('yasnaDomain::layouts.footer')
@stop
