<?php

namespace Modules\YasnaDomain\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class YasnaDomainController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $posts = [];

        $posts['main'] = post('main')->spreadMeta()->in(getLocale());

        $posts['services'][0] = post('domain-service')->spreadMeta()->in(getLocale());
        $posts['services'][1] = post('package-service')->spreadMeta()->in(getLocale());
        $posts['services'][2] = post('others-service')->spreadMeta()->in(getLocale());


        return view('yasnaDomain::home.index', compact('posts'));
    }
}
