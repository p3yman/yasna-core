<?php

Route::group([
     'middleware' => 'web',
     'namespace' => 'Modules\YasnaDomain\Http\Controllers'
], function () {
    Route::get('/', 'YasnaDomainController@index');

    Route::group([
         'middleware' => 'locale',
         'prefix' => '{lang?}'
    ], function () {
        Route::get('/', 'YasnaDomainController@index');
    });
});
