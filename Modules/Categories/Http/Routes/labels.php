<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:super'],
     'namespace'  => module('Categories')->getControllersNamespace(),
     'prefix'     => 'categories/downstream/groups',
], function () {
    Route::get('editor/{hashid}', 'LabelsController@editor')->name('categories.labels.editor');
    Route::post('save', 'LabelsController@save')->name('categories.labels.save');
    Route::get('row/{hashid}', 'LabelsController@row')->name('categories.labels.row');
    Route::get('categories/{hashid}', 'LabelsController@categoriesForm')->name('categories.labels.categories');
    Route::post('categories/{hashid}', 'LabelsController@categories');
    Route::get('delete/{hashid}/{action}', 'LabelsController@deleteForm')->name('categories.labels.delete');
    Route::post('delete/{hashid}/{action}', 'LabelsController@delete');
});
