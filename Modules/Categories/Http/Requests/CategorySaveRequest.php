<?php

namespace Modules\Categories\Http\Requests;

use App\Models\Category;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class CategorySaveRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->all()['_submit'] = 'delete') {
            return [];
        }

        return [
             'slug'      => 'alpha_dash|not_in:' . Category::$reserved_slugs,
             '_groups'   => 'array',
             '_groups.*' => Rule::exists('labels', 'slug')
                                ->where('model_name', 'Category'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'slug' => "slug",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctGroups();
    }



    /**
     * Corrects the value of the `_groups` field.
     */
    protected function correctGroups()
    {
        $string = ($this->data['_groups'] ?? '');

        if (!is_string($string)) {
            return;
        }

        $array = explode_not_empty(',', $string);

        $this->data['_groups'] = $array;
    }
}
