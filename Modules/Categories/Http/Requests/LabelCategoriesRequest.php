<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/17/18
 * Time: 11:44 AM
 */

namespace Modules\Categories\Http\Requests;


class LabelCategoriesRequest extends LabelSingleRequest
{
    /**
     * Whether a trashed model is acceptable or not.
     *
     * @var bool
     */
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
