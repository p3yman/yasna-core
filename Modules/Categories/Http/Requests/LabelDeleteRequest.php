<?php

namespace Modules\Categories\Http\Requests;


class LabelDeleteRequest extends LabelSingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'action' => 'in:soft_delete,restore,hard_delete',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        // If the action is soft delete, the requested model should not be a trashed one
        // Otherwise, it must be trashed before.

        if ($this->action == 'soft_delete') {
            return !$model->trashed();
        } else {
            return $model->trashed();
        }
    }
}
