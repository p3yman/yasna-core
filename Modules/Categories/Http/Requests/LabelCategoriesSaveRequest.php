<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/17/18
 * Time: 12:11 PM
 */

namespace Modules\Categories\Http\Requests;


use Illuminate\Validation\Rule;

class LabelCategoriesSaveRequest extends LabelCategoriesRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'categories'   => 'required|array',
             'categories.*' => Rule::exists('categories', 'id'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctCategories();
    }



    /**
     * Corrects the value of the `_groups` field.
     */
    protected function correctCategories()
    {
        $string = ($this->data['categories'] ?? '');

        if (!is_string($string)) {
            return;
        }

        $array = explode_not_empty(',', $string);

        $this->data['categories'] = array_map('hashid', $array);
    }
}
