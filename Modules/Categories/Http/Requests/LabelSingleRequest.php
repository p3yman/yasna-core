<?php

namespace Modules\Categories\Http\Requests;

use App\Models\Category;
use App\Models\Label;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class LabelSingleRequest
 * <br>
 * Request to be used for any single action on categories' labels.
 *
 * @property Label $model
 */
class LabelSingleRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "label";

    /**
     * Whether a trashed model is acceptable or not.
     *
     * @var bool
     */
    protected $model_with_trashed = true;



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $this->data['id']    = $this->getRequestedModelId($id_or_hashid);
        $this->data['model'] = $this->model = $this->findModel($this->data['id']);
        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }



    /**
     * Finds the desired label in a custom way.
     *
     * @param string $id
     *
     * @return Label
     */
    protected function findModel(string $id)
    {
        $builder = $this->categoryModel()->definedLabels();

        if ($this->model_with_trashed) {
            $builder->withTrashed();
        }

        return $builder->grabId($id);
    }



    /**
     * Returns a new instance of the Category model.
     *
     * @return Category
     */
    public function categoryModel()
    {
        return model('category');
    }
}
