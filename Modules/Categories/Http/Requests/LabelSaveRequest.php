<?php

namespace Modules\Categories\Http\Requests;


class LabelSaveRequest extends LabelSingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug'                 => 'required|alpha-dash',
             $this->titleFieldKey() => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             $this->titleFieldKey() => trans_safe('validation.attributes.title'),
        ];
    }



    /**
     * Returns the key of the title which should be validated.
     *
     * @return string
     */
    protected function titleFieldKey()
    {
        return 'titles.' . getLocale();
    }
}
