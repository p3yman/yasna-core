<?php

/*
|--------------------------------------------------------------------------
| Manage Side
|--------------------------------------------------------------------------
|
*/

Route::group(
    [
        'middleware' => ['web', 'auth'],
        'prefix'     => 'manage/categories',
        'namespace'  => module('categories')->controller(),
    ],
    function () {
        Route::get('selector/refresh/{model_hashid}/{type?}/{locale?}', 'CategoriesController@selectorRefresh');
        Route::get('/post/{hashid}', 'CategoriesController@modalEditor');
        Route::post('save/set', 'CategoriesController@saveCategory')->name('category-set');
    }
);


/*
|--------------------------------------------------------------------------
| Downstream
|--------------------------------------------------------------------------
|
*/
Route::group(
    [
        'middleware' => ['web', 'auth'],
        'prefix'     => 'manage/categories/downstream',
        'namespace'  => module('categories')->controller(),
    ],
    function () {
        Route::get('demo/nested-view', 'DemoController@index');

        Route::get('list/{hash_id}/{warn?}', 'DownstreamController@list');
        Route::get('create/folder/{hash_id}', 'DownstreamController@createFolder');
        Route::get('create/category/{hash_id}', 'DownstreamController@createCategory');
        Route::get('create/no-folder/{hash_id}', 'DownstreamController@createCategoryWithoutFolder');
        Route::get('edit/{hash_id}', 'DownstreamController@edit');
        Route::get('{action}/{hash_id?}', 'DownstreamController@action');


        Route::group(['prefix' => 'save'], function () {
            Route::post('/', 'DownstreamController@save')->name('category-downstream-save');
        });
    }
);
