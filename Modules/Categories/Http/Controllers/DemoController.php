<?php

namespace Modules\Categories\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class DemoController extends YasnaController
{
    protected $base_model  = ""; // @TODO <~~
    protected $view_folder = "categories::downstream.nested"; // @TODO <~~ (Remove if same as parent)



    /**
     * Index of nested view demo
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $model = $this->dummyData();

        return $this->view('index', compact('model'));
    }



    /**
     * Returns dummy data to be used in demo
     *
     * @return array
     */
    protected function dummyData()
    {
        return [
             "folders" => [
                  [
                       "title"      => "وبلاگ",
                       "slug"       => "blog",
                       "id"         => "blog",
                       "type"       => "nested",
                       "categories" => [
                            [
                                 "title"     => "ورزشی",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "children"  => [],
                                           "active"    => false,

                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],


                            ],
                            [
                                 "title"     => "سیاسی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "active"    => true,

                                           "children" => [
                                                [
                                                     "title"     => "پست چهارم",
                                                     "slug"      => "post4",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                                [
                                                     "title"     => "پست پنچم",
                                                     "slug"      => "post5",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                           ],
                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],


                            ],
                       ],
                  ],
                  [
                       "title"      => "اخبار",
                       "slug"       => "news",
                       "id"         => "news",
                       "type"       => "nested",
                       "categories" => [
                            [
                                 "title"     => "ورزشی",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => false,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "children"  => [
                                                [
                                                     "title"     => "پست سوم",
                                                     "slug"      => "post3",
                                                     "parent_id" => "post1",
                                                     "id"        => "post3",
                                                     "children"  => [],
                                                     "active"    => false,

                                                ],
                                           ],
                                           "active"    => false,

                                      ],
                                 ],


                            ],
                            [
                                 "title"     => "سیاسی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "active"    => true,

                                           "children" => [
                                                [
                                                     "title"     => "پست چهارم",
                                                     "slug"      => "post4",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                                [
                                                     "title"     => "پست پنچم",
                                                     "slug"      => "post5",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                           ],
                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],


                            ],
                       ],
                  ],
                  [
                       "title"      => "محصولات",
                       "slug"       => "products",
                       "id"         => "products",
                       "type"       => "label",
                       "categories" => [
                            [
                                 "title"     => "ورزشی",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => false,
                            ],
                            [
                                 "title"     => "سیاسی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,
                            ],
                            [
                                 "title"     => "ورزشی",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => true,
                            ],
                            [
                                 "title"     => "اقتصادی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,
                            ],
                            [
                                 "title"     => "فرهنگی هنری",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => true,
                            ],
                            [
                                 "title"     => "آموزشی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,
                            ],
                       ],
                  ],
             ],


             "categories" => [
                  [
                       "title"     => "ورزشی",
                       "slug"      => "sport",
                       "parent_id" => "blog",
                       "id"        => "sport",
                       "active"    => true,

                       "children" => [
                            [
                                 "title"     => "پست اول",
                                 "slug"      => "post1",
                                 "parent_id" => "political",
                                 "id"        => "post1",
                                 "children"  => [],
                                 "active"    => false,

                            ],
                            [
                                 "title"     => "پست دوم",
                                 "slug"      => "post2",
                                 "parent_id" => "political",
                                 "id"        => "post2",
                                 "children"  => [],
                                 "active"    => true,

                            ],
                            [
                                 "title"     => "پست سوم",
                                 "slug"      => "post3",
                                 "parent_id" => "political",
                                 "id"        => "post3",
                                 "children"  => [],
                                 "active"    => true,

                            ],
                       ],


                  ],
                  [
                       "title"     => "سیاسی",
                       "slug"      => "political",
                       "parent_id" => "blog",
                       "id"        => "political",
                       "active"    => true,

                       "children" => [
                            [
                                 "title"     => "پست اول",
                                 "slug"      => "post1",
                                 "parent_id" => "political",
                                 "id"        => "post1",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست چهارم",
                                           "slug"      => "post4",
                                           "parent_id" => "post1",
                                           "id"        => "post5",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست پنچم",
                                           "slug"      => "post5",
                                           "parent_id" => "post1",
                                           "id"        => "post5",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],
                            ],
                            [
                                 "title"     => "پست دوم",
                                 "slug"      => "post2",
                                 "parent_id" => "political",
                                 "id"        => "post2",
                                 "children"  => [],
                                 "active"    => true,

                            ],
                            [
                                 "title"     => "پست سوم",
                                 "slug"      => "post3",
                                 "parent_id" => "political",
                                 "id"        => "post3",
                                 "children"  => [],
                                 "active"    => true,

                            ],
                       ],


                  ],
             ],


        ];
    }

}
