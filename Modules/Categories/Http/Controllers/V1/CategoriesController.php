<?php

namespace Modules\Categories\Http\Controllers\V1;

use App\Models\Category;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class CategoriesController extends YasnaApiController
{
    /**
     * The Request in Use
     *
     * @var SimpleYasnaRequest
     */
    protected $request;



    /**
     * Returns the list of categories of the requested posttype.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function list(SimpleYasnaRequest $request)
    {
        $this->request = $request;

        $builder = $this->getCategoriesBuilder();

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder);
    }



    /**
     * Returns a builder to get the categories of the specified posttype.
     *
     * @return HasMany
     */
    protected function getCategoriesBuilder()
    {
        return model('category')
             ->elector($this->getElectorData());
    }



    /**
     * Returns the data to be used in the elector.
     *
     * @return array
     */
    protected function getElectorData()
    {
        return array_merge(
             ...
             $this
                  ->getElectorMethods()
                  ->map(function (string $method) {
                      return $this->$method();
                  })->toArray()
        );
    }



    /**
     * Returns the methods which should be called to generate the elector data.
     *
     * @return BaseCollection
     */
    protected function getElectorMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return starts_with($method, 'elector');
             })
             ->values()
             ;
    }



    /**
     * Returns the elector data based on the posttype field.
     *
     * @return array
     */
    protected function electorPosttype()
    {
        $posttype = $this->request->posttype;

        if (!$posttype or is_numeric($posttype)) {
            return [];
        }

        return [
             'posttype' => $posttype,
        ];
    }



    /**
     * Returns the elector data based on the parent field.
     *
     * @return array
     */
    protected function electorParent()
    {
        $parent = ($this->request->parent ?? 0);

        return [
             'parent' => $parent,
        ];
    }



    /**
     * Returns the elector data based on the label field.
     *
     * @return array
     */
    protected function electorLabel()
    {
        $label = $this->request->group;

        if (!$label) {
            return [];
        }

        return [
             'label' => $label,
        ];
    }



    /**
     * Returns the elector data based on the is_folder field.
     *
     * @return array
     */
    protected function electorIsFolder()
    {
        $is_folder = $this->request->is_folder;

        if (is_null($is_folder)) {
            return [];
        }

        return [
             'is_folder' => boolval($is_folder),
        ];
    }



    /**
     * Categories All Together
     *
     * @param HasMany $builder
     *
     * @return array
     */
    protected function listFlat($builder)
    {
        $meta = [
             "total" => $builder->count(),
        ];

        $data = $this->mappedArray($builder->get());

        return $this->success($data, $meta);
    }



    /**
     * Categories by Pagination
     *
     * @param HasMany            $builder
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function listPaginated($builder, $request)
    {
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mappedArray($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * Returns the mapped array of the result to be returned as the response.
     *
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection
             ->map(function (Category $category) {
                 return $category->toListResource();
             })
             ->toArray()
             ;
    }
}
