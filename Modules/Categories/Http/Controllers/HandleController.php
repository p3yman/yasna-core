<?php

namespace Modules\Categories\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HandleController extends Controller
{
    public static function postEditor()
    {
        $my_name = "categories";


        module("posts")
            ->service("editor_side")
            ->add($my_name)
            ->blade("categories::post-selector")
            ->order(31)
        ;

        module('posts')
            ->service('after_save')
            ->add($my_name)
            ->method("categories:CategoriesController@saveCategories")
        ;
    }

    public static function postBrowserHeadings($arguments)
    {
        $posttype = $arguments['posttype'];
        $my_name = "categories" ;

        module('posts')
            ->service('browse_headings')
            ->add($my_name)
            ->trans("$my_name::general.singular")
            ->blade("$my_name::post-browser")
            ->order(21)
            ->condition($posttype->has('category'))
        ;
    }
}
