<?php

namespace Modules\Categories\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;

class CategoriesController extends Controller
{
    use ManageControllerTrait;

    public function selectorRefresh($hashid, $type_slug = false, $locale = false)
    {
        /*-----------------------------------------------
        | Model ...
        */
        if ($hashid != hashid(0)) {
            $model = model('post', $hashid);
            if (!$model or !$model->exists) {
                return $this->abort(410);
            }
        } else {
            $model         = new Post();
            $model->type   = $type_slug;
            $model->locale = $locale;
        }

        /*-----------------------------------------------
        | View ...
        */

        return view("categories::post-selector-inside", compact("model"));
    }

    public static function saveCategories($model, $data)
    {
        if ($model->has('category')) {
            $model->saveCategories($data);
        }

        return $data;
    }

    public function modalEditor($hashid)
    {
        /*-----------------------------------------------
        | Model Retrivement  ...
        */
        $model = model('post', $hashid);
        if (!$model or !$model->exists or $model->hasnot('category')) {
            return $this->abort(410, 1);
        }
        if (!$model->canEdit()) {
            return $this->abort(403, 1);
        }

        /*-----------------------------------------------
        | View ...
        */

        return module('categories')
            ->view("post-modal", compact('model'));
    }

    public function saveCategory(Request $request)
    {
        /*-----------------------------------------------
        | Model Retrivement  ...
        */
        $model = model('post', $request->hashid);
        if (!$model or !$model->exists or $model->hasnot('category')) {
            return $this->abort(410, 1) ;
        }
        if (!$model->canEdit()) {
            return $this->abort(403, 1);
        }

        /*-----------------------------------------------
        | Save ...
        */
        $ok = $model->saveCategories($request->toArray()) ;


        /*-----------------------------------------------
        | Feedback ...
        */
        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblPosts' , '$request->hashid')",
        ]);
    }
}
