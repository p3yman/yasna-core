<?php

namespace Modules\Categories\Http\Controllers;

use App\Models\Category;
use App\Models\Label;
use Modules\Categories\Http\Requests\LabelCategoriesRequest;
use Modules\Categories\Http\Requests\LabelCategoriesSaveRequest;
use Modules\Categories\Http\Requests\LabelDeleteRequest;
use Modules\Categories\Http\Requests\LabelSaveRequest;
use Modules\Categories\Http\Requests\LabelSingleRequest;
use Modules\Yasna\Services\YasnaController;

class LabelsController extends YasnaController
{
    protected $view_folder = "categories::downstream.labels";



    /**
     * Renders the modal to create a new category label or edit an existing one.
     *
     * @param LabelSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editor(LabelSingleRequest $request)
    {
        $model = $request->model;

        return $this->view('editor', compact('model'));
    }



    /**
     * Saves a new category label or update an existing one.
     *
     * @param LabelSaveRequest $request
     *
     * @return string
     */
    public function save(LabelSaveRequest $request)
    {
        $ok = $request->model->batchSaveBoolean([
             'slug'       => $request->slug,
             'titles'     => json_encode($request->titles),
             'model_name' => $request->categoryModel()->getClassName(),
        ]);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('auto', '$request->model_hashid')",
        ]);
    }



    /**
     * Renders the browse row of the requested category label.
     *
     * @param LabelSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function row(LabelSingleRequest $request)
    {
        $model = $request->model;

        return $this->view('row', compact('model'));
    }



    /**
     * Renders a form to edit the categories of a label.
     *
     * @param LabelCategoriesRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function categoriesForm(LabelCategoriesRequest $request)
    {
        /** @var Label $model */
        $model              = $request->model;
        $categories_options = $this->categoriesCombo();
        $categories_value   = $model->models->pluck('hashid')->implode(',');

        return $this->view(
             'categories',
             compact('model', 'categories_options', 'categories_value')
        );
    }



    /**
     * Returns an array to be used in the combo of categories.
     *
     * @return array
     */
    protected function categoriesCombo()
    {
        return model('category')
             ->orderBy('posttype_id')
             ->get()
             ->map(function (Category $category) {
                 $posttype = $category->posttype;

                 if (!$posttype) {
                     return null;
                 }

                 $title = $posttype->titleIn(getLocale()) . ' / ' . $category->titleIn(getLocale());
                 return [
                      'id'    => $category->hashid,
                      'title' => $title,
                 ];
             })
             ->filter()
             ->values()
             ->toArray()
             ;

    }



    /**
     * Syncs the categories in relation with the specified label.
     *
     * @param LabelCategoriesSaveRequest $request
     *
     * @return string
     */
    protected function categories(LabelCategoriesSaveRequest $request)
    {
        /** @var Label $model */
        $model = $request->model;
        $model->models()->sync($request->categories);

        return $this->jsonAjaxSaveFeedback(1, [
             'success_callback' => "rowUpdate('auto', '$request->model_hashid')",
        ]);
    }



    /**
     * Renders a form based on the specified action on the requested model.
     * <br>
     * The action can be one of:
     * - `soft_delete`
     * - `restore`
     * - `hard_delete`
     *
     * @param LabelDeleteRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteForm(LabelDeleteRequest $request)
    {
        $action = $request->action;
        $model  = $request->model;

        return $this->view('delete', compact('model', 'action'));
    }



    /**
     * Makes the specified action on the requested model.
     * <br>
     * The action can be one of:
     * - `soft_delete`
     * - `restore`
     * - `hard_delete`
     *
     * @param LabelDeleteRequest $request
     *
     * @return string
     */
    public function delete(LabelDeleteRequest $request)
    {
        $action         = $request->action;
        $model          = $request->model;
        $method         = camel_case($action);
        $is_hard_delete = ($action == 'hard_delete');

        $ok = $this->$method($model);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => $is_hard_delete ? "" : "rowUpdate('auto', '$request->model_hashid')",
             'success_refresh'  => $is_hard_delete,
        ]);
    }



    /**
     * Deletes the specified label softly.
     *
     * @param Label $label
     *
     * @return bool
     */
    protected function softDelete(Label $label)
    {
        return $label->delete();
    }



    /**
     * Restores the specified trashed label.
     *
     * @param Label $label
     *
     * @return bool|null
     */
    protected function undelete(Label $label)
    {
        return $label->restore();
    }



    /**
     * Deletes the specified trashed label for ever.
     *
     * @param Label $label
     *
     * @return bool
     */
    protected function hardDelete(Label $label)
    {
        return $label->hardDelete();
    }
}
