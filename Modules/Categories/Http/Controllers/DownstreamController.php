<?php

namespace Modules\Categories\Http\Controllers;

use App\Models\Category;
use App\Models\Posttype;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Routing\Controller;
use Modules\Categories\Http\Requests\CategorySaveRequest;
use Modules\Manage\Traits\ManageControllerTrait;

class DownstreamController extends Controller
{
    use ManageControllerTrait;

    public function __construct()
    {
        $this->middleware('is:super');
    }

    /*
    |--------------------------------------------------------------------------
    | Posttypes
    |--------------------------------------------------------------------------
    |
    */

    public static function index()
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "categories::downstream.posttypes";

        /*-----------------------------------------------
        | Model ...
        */
        $selector         = [
            'features' => "category",
        ];
        $result['models'] = Posttype::selector($selector)->orderBy('order')->orderBy('title')->get();

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }

    public function list($hashid, $warn = false)
    {
        /*-----------------------------------------------
        | Model ...
        */
        $model = model('posttype', $hashid) ;
        if (!$model or !$model->id) {
            return $this->abort(410, true);
        }
        if ($model->cannot('setting')) {
            return $this->abort(403, true);
        }
        $model->spreadMeta();


        /*-----------------------------------------------
        | View ...
        */

        return view("categories::downstream.list", compact('model', 'warn'));
    }

    public function createCategory($hashid)
    {
        /*-----------------------------------------------
        | Folder ...
        */
        $folder = Category::findByHashid($hashid);
        if (!$folder or !$folder->exists) {
            return $this->abort(410, true);
        }
        if ($folder->cannot()) {
            return $this->abort(403, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        $model              = new Category();
        $model->posttype_id = $folder->posttype_id;
        $model->parent_id   = $folder->id;
        $model->is_folder   = false;


        /*-----------------------------------------------
        | Modal Title ...
        */
        $modal_title = trans("categories::general.new_category") . SPACE . trans("manage::forms.general.in") . SPACE . $folder->posttype->title;

        /*-----------------------------------------------
        | View ...
        */

        return view("categories::downstream.editor", compact('model', 'modal_title'));
    }

    public function createCategoryWithoutFolder($hashid)
    {
        /*-----------------------------------------------
        | Posttype ...
        */
        $type = Posttype::findByHashid($hashid);
        if (!$type or !$type->id) {
            return $this->abort(410, true);
        }
        if ($type->cannot('setting')) {
            return $this->abort(403, true);
        }
        $type->spreadMeta();

        /*-----------------------------------------------
        | Model ...
        */
        $model              = new Category();
        $model->posttype_id = $type->id;
        $model->parent_id   = 0;
        $model->is_folder   = 0;

        /*-----------------------------------------------
        | Modal Title ...
        */
        $modal_title = trans("categories::general.new_category") . SPACE . trans("manage::forms.general.in") . SPACE . $type->title;

        /*-----------------------------------------------
        | View ...
        */

        return view("categories::downstream.editor", compact('model', 'modal_title'));
    }

    public function createFolder($hashid)
    {

        /*-----------------------------------------------
        | Posttype ...
        */
        $type = Posttype::findByHashid($hashid);
        if (!$type or !$type->id) {
            return $this->abort(410, true);
        }
        if ($type->cannot('setting')) {
            return $this->abort(403, true);
        }
        $type->spreadMeta();

        /*-----------------------------------------------
        | Model ...
        */
        $model              = new Category();
        $model->posttype_id = $type->id;
        $model->parent_id   = 0;
        $model->is_folder   = 1;

        /*-----------------------------------------------
        | Modal Title ...
        */
        $modal_title = trans("categories::general.new_folder") . SPACE . trans("manage::forms.general.in") . SPACE . $type->title;


        /*-----------------------------------------------
        | View ...
        */

        return view("categories::downstream.editor", compact('model', 'modal_title'));
    }

    public function edit($hashid)
    {
        /*-----------------------------------------------
        | Model ...
        */
        $model = Category::findByHashid($hashid);
        if (!$model or !$model->id) {
            return $this->abort(410, true);
        }
        if ($model->cannot()) {
            return $this->abort(403, true);
        }
        $model->spreadMeta();

        /*-----------------------------------------------
        | Modal Title ...
        */
        $modal_title = trans($model->is_folder ? "categories::general.edit_folder" : "categories::general.edit_category");

        /*-----------------------------------------------
        | View ...
        */

        return view("categories::downstream.editor", compact('model', 'modal_title'));
    }

    public function save(CategorySaveRequest $request)
    {
        $data = $request->toArray();

        /*-----------------------------------------------
        | Posttype ...
        */
        $posttype = posttype($request->_posttype);
        if (!$posttype or !$posttype->exists or $posttype->hasnot('category')) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }
        if ($posttype->cannot('setting')) {
            return $this->jsonFeedback(trans('validation.http.Error403'));
        }
        $data['posttype_id'] = $posttype->id;

        /*-----------------------------------------------
        | Parent ...
        */
        $parent_id = $request->_parent;
        if ($parent_id) {
            $parent = Category::find($parent_id);
            if (!$parent or !$parent->exists or $parent->posttype_id != $posttype->id or !$parent->is_folder) {
                return $this->jsonFeedback(trans('validation.http.Error410'));
            } else {
                $data['parent_id'] = $parent->id;
            }
        } else {
            $data['parent_id'] = 0;
        }


        /*-----------------------------------------------
        | Model ...
        */
        $id = hashid($request->hashid);
        if ($id) {
            $model = Category::find($id);
            if (!$model or !$model->exists or $model->posttype_id != $posttype->id or $model->is_folder != $request->is_folder) {
                return $this->jsonFeedback(trans('validation.http.Error410'));
            }
        }

        /*-----------------------------------------------
        | Delete ...
        */
        if (isset($model) and $request->_submit == 'delete') {
            $done = $model->safeDestroy();

            return $this->jsonAjaxSaveFeedback($done, [
                'success_callback'   => "masterModal(url('manage/categories/downstream/list/$posttype->hashid'))",
                'success_modalClose' => "0",
            ]);
        }


        /*-----------------------------------------------
        | Titles and Images ...
        */
        $first_title = null;
        foreach ($posttype->locales_array as $locale) {
            if ($data["_title_in_$locale"]) {
                $data['titles'][ $locale ] = $data["_title_in_$locale"];
                if (!$first_title) {
                    $first_title = $data["_title_in_$locale"];
                }
            }
            if ($data["_text_in_$locale"]) {
                $data['texts'][ $locale ] = $data["_text_in_$locale"];
            }
            if ($posttype->has('category_image')) {
                $data['images'][ $locale ] = $data["_image_in_$locale"];
            }
        }
        if (!isset($data['titles'])) {
            return $this->jsonFeedback(trans("categories::general.feel_at_least_one_title"));
        }
        $data['locales'] = "|" . implode("|", array_keys($data['titles'])) . "|";

        /*-----------------------------------------------
        | Slug Existence ...
        */
        if (!$data['slug']) {
            $data['slug'] = str_slug($first_title . "-" . str_random(10));
        }


        /*-----------------------------------------------
        | Slug Validation ...
        */
        $duplicate_slug = Category::where('id', '!=', $id)
                                  ->where('posttype_id', $posttype->id)
                                  ->where('slug', $request->slug)
                                  ->first()
        ;

        if ($duplicate_slug) {
            return $this->jsonFeedback(trans('validation.unique', [
                'attribute' => trans('validation.attributes.slug'),
            ]));
        }


        /*-----------------------------------------------
        | Save ...
        */
        $saved = Category::store($data);

        // Save Labels
        $this->saveLabels($saved, $data['_groups']);


        /*-----------------------------------------------
        | Feedback ...
        */

        return $this->jsonAjaxSaveFeedback($saved, [
            'success_callback'   => "masterModal(url('manage/categories/downstream/list/$posttype->hashid'));divReload('divCategoriesInside')",
            'success_modalClose' => "0",
        ]);
    }



    /**
     * Syncs the labels of a category with the specified ID with the specified labels.
     *
     * @param int   $id
     * @param array $labels
     */
    protected function saveLabels(int $id, array $labels)
    {
        if (!$id) {
            return;
        }

        /** @var Category $model */
        $model          = model('category', $id);
        $current_labels = $model->labels->pluck('slug')->toArray();

        $attaching      = array_values(array_diff($labels, $current_labels));

        if (!empty($attaching)) {
            $model->attachLabels(...$attaching);

        }

        $detaching      = array_values(array_diff($current_labels, $labels));
        if (!empty($detaching)) {
            $model->detachLabels(...$detaching);
        }
    }



    /**
     * Returns an array of the information to render the list of categories in the downstream area.
     *
     * @return array
     */
    public static function groups(): array
    {
        $result['view_file'] = "categories::downstream.labels.list";
        $result['models']    = static::findGroups();

        return $result;
    }



    /**
     * Returns the models to be shown in the list of categories' groups.
     *
     * @return Paginator
     */
    protected static function findGroups()
    {
        return model('category')
             ->definedLabels()
             ->orderBy('created_at', 'desc')
             ->withTrashed()
             ->simplePaginate(20)
             ;
    }
}
