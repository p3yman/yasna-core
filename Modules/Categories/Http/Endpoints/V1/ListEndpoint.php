<?php

namespace Modules\Categories\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/categories-list
 *                    List
 * @apiDescription    Categories with Filters
 * @apiVersion        1.0.2
 * @apiName           Categories
 * @apiGroup          Categories
 * @apiPermission     None
 * @apiParam {String} [posttype] The hashid or the slug of the desired posttype
 * @apiParam {String} [parent] The hashid or the slug of the parent category. If missed, categories without parent will
 *           be returned.
 * @apiParam {String} [group] The slug of the group which the result categories must have relation with it
 * @apiParam {Boolean} [is_folder] If the categories in the result must if folder or not
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer}     [page=1]      Specifies the page number of results (available only with `paginated=1`).
 * @apiParam {Integer}   [per_page=15] Specifies the number of results on each page (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "total": "2"
 *      },
 *      "results": [
 *           {
 *                "title": "Category 1",
 *                "slug": "category1",
 *                "is_folder": 0,
 *                "text": false,
 *                "image_hashid": null,
 *                "children": []
 *           },
 *           {
 *                "title": "Folder 1",
 *                "slug": "folder1",
 *                "is_folder": 1,
 *                "text": false,
 *                "image_hashid": null,
 *                "children": [
 *                     {
 *                          "title": "category2",
 *                          "slug": "category2",
 *                          "is_folder": 0,
 *                          "text": "Text of Category 2",
 *                          "image_hashid": null,
 *                          "children": []
 *                     }
 *                ]
 *           }
 *      ]
 * }
 * @apiErrorExample   Not-Found:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Not Found!",
 *      "userMessage": "The requested resource could not be found but may be available in the future.",
 *      "errorCode": 404,
 *      "moreInfo": "categories.moreInfo.404",
 *      "errors": []
 * }
 * @method  \Modules\Categories\Http\Controllers\V1\CategoriesController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Categories";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Categories\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CategoriesController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             0 =>
                  [
                       'title'        => 'Category 1',
                       'slug'         => 'category1',
                       'is_folder'    => 0,
                       'text'         => false,
                       'image_hashid' => null,
                       'children'     => [],
                  ],
             1 =>
                  [
                       'title'        => 'Folder 1',
                       'slug'         => 'folder1',
                       'is_folder'    => 1,
                       'text'         => false,
                       'image_hashid' => null,
                       'children'     =>
                            [
                                 [
                                      'title'        => 'category2',
                                      'slug'         => 'category2',
                                      'is_folder'    => 0,
                                      'text'         => 'Text of Category 2',
                                      'image_hashid' => null,
                                      'children'     => [],
                                 ],
                            ],
                  ],
        ], [
             "total" => "2",
        ]);
    }
}
