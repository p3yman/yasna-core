<?php

namespace Modules\Categories\Providers;

use Modules\Categories\Console\CategoryConverter;
use Modules\Categories\Http\Endpoints\V1\ListEndpoint;
use Modules\Yasna\Services\YasnaProvider;

class CategoriesServiceProvider extends YasnaProvider
{



    /**
     * Provider Index
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerDownstream();
        $this->registerPostHandlers();
        $this->registerEndpoints();
        $this->registerAssets();
        $this->registerArtisans();
    }



    /**
     * Model Traits
     */
    public function registerModelTraits()
    {
        module('yasna')
             ->service('traits')
             ->add('category-in-posttype')->trait("Categories:PosttypeCategoryTrait")->to("Posttype")
             ->add('category-in-post')->trait("Categories:PostCategoryTrait")->to("Post")
        ;

        $this->addModelTrait('PostCategoryResourcesTrait', 'Post');
    }



    /**
     * Downstream Settings
     */
    public function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->add('categories')
             ->link('categories')
             ->trans('categories::general.plural')
             ->method('categories:DownstreamController@index')
             ->order(21)
        ;

        module('manage')
             ->service('downstream')
             ->add('categories-group')
             ->link('categories-groups')
             ->trans('categories::groups.plural')
             ->method('categories:DownstreamController@groups')
             ->condition(function () {
                 return user()->isSuper();
             })
             ->order(22)
        ;
    }



    /**
     * Post Handlers
     */
    public function registerPostHandlers()
    {
        $my_name = "categories";

        module('posts')
             ->service('editor_handlers')
             ->add($my_name)
             ->method("$my_name:HandleController@postEditor")
        ;

        module('posts')
             ->service('browse_headings_handlers')
             ->add($my_name)
             ->method("$my_name:HandleController@postBrowserHeadings")
        ;
    }



    /**
     * Registers the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
    }



    /**
     * Register module assets
     */
    protected function registerAssets()
    {
        module('manage')
             ->service('template_assets')
             ->add('categories-custom-css')
             ->link("categories:css/categories.min.css")
             ->order(44)
        ;
    }



    /**
     * register artisans
     */
    public function registerArtisans()
    {
        $this->addArtisan(CategoryConverter::class);
    }
}
