<?php
return [
     "plural"                => "گروه‌های دسته‌بندی‌ها",
     "plural-simple"         => "گروه‌ها",
     "create"                => "افزودن گروه",
     "edit-something"        => "وبرایش گروه «:title»",
     "categories-management" => "مدیریت دسته‌بندی‌ها",
];
