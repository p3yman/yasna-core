<?php
return [
    'plural'                   => "Categories",
    'singular'                 => "Category",
    'of'                       => "Categories of",
    'selection'                => "Select the Category",
    'folder'                   => "Folder",
    'edit_folder'              => "Edit Folder",
    'edit_category'            => "Edit Category",
    'folders'                  => "Folders",
    'new_folder'               => "Add New Folders",
    'new_category'             => "Add New Category",
    'folder_delete_notice'     => "Subcategories will be left without folder.",
    'no_folder'                => "Without Folder",
    'no_category'              => "Without Category",
    'category_enabled_content' => "Categorizable Content",
    'category_image'           => "Category Image",
    'and_n_more_categories'    => "and :n other categories",
    'feel_at_least_one_title'  => "Title is required.",
    'settings'                 => "Categories Settings",
    'settings_button'          => "Settings",
    'no_settings'              => "No category is set.",
    'default'                  => "Default",
    'set_default'              => "Set as Default",
    'edit_warning'             => "Changing categories setting will reset your choices.",
    'categories-folder'        => "Categories Folder"
];
