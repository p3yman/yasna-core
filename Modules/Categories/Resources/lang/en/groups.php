<?php
return [
     "plural"                => "Groups of Categories",
     "plural-simple"         => "Groups",
     "create"                => "Create Group",
     "edit-something"        => "Edit Group «:title»",
     "categories-management" => "Categories Management",
];
