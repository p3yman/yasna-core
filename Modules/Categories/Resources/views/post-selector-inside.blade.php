@php
	$printed = 0;
@endphp
{{--
|--------------------------------------------------------------------------
| Default Category
|--------------------------------------------------------------------------
|
--}}
@include('manage::forms.hidden' , [
	'name' => "default_category",
	'id' => "txtDefaultCategory" ,
	'value' => hashid(intval($model->default_category)) ,
]      )


{{--
|--------------------------------------------------------------------------
| With Folders
|--------------------------------------------------------------------------
|
--}}
@php
	$key = 0;
@endphp

@foreach($model->posttype->foldersIn($model->locale)->get() as $key => $folder)
	@include("categories::post-selector-inside-more" , [
		'categories' => $folder->childrenIn($model->locale)->get(),
		'folder_title' => $folder->titleIn($model->locale) ,
		'counter' => $printed+= $folder->childrenIn($model->locale)->get()->count(),
	]     )

@endforeach


{{--
|--------------------------------------------------------------------------
| Without Folders
|--------------------------------------------------------------------------
|
--}}
@include("categories::post-selector-inside-more" , [
	'categories' => $model->posttype->orphansIn($model->locale)->get(),
	'counter' => $printed+= $model->posttype->orphans()->get()->count(),
]     )


{{--
|--------------------------------------------------------------------------
| Nothing to Display
|--------------------------------------------------------------------------
|
--}}
@if(!$printed)
	<div class="m10 noContent text-center">
		{{ trans("categories::general.no_settings") }}
	</div>
@endif
