@if($model->has('category'))
	@php
		$can_manage_categories = $model->posttype->can('settings');
	@endphp

	<div id="divCategories" class="panel panel-default">
		{{--
		|--------------------------------------------------------------------------
		| Header
		|--------------------------------------------------------------------------
		|
		--}}


		<div class="panel-heading">
			{{ trans('validation.attributes.category') }}
			<a href="{{v0()}}" onclick="divReload('divCategoriesInside')" class="pull-right">
				<i class="fa fa-refresh text-gray"></i>
			</a>
		</div>


		{{--
		|--------------------------------------------------------------------------
		| Inside
		|--------------------------------------------------------------------------
		|
		--}}


		<div id="divCategoriesInside" class="panel-body"
			 data-src="manage/categories/selector/refresh/{{$model->hashid}}/{{$model->type}}/{{$model->locale}}"
			 style="max-height: 200px;overflow-y: auto;overflow-x: hidden">
			@include('categories::post-selector-inside')
		</div>


		{{--
		|--------------------------------------------------------------------------
		| Management Link
		|--------------------------------------------------------------------------
		|
		--}}
		@if($can_manage_categories)
			<div class="w100 text-center">
				@include("manage::widgets.grid-text" , [
					'text' => trans("categories::general.settings"),
					'size' => "10" ,
					'color' => "gray" ,
					'link' => "modal:manage/categories/downstream/list/" . $model->posttype->hashid . "/1",
					'icon' => "cog" ,
					'class' => "btn btn-default w90" ,
				]     )
			</div>
		@endif

	</div>
@endif
