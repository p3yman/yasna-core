@php
    $count = $model->categories_count;
    $edit_link = $model->canEdit()? "modal:manage/categories/post/-hashid-:md" : ''
@endphp

{{--
|--------------------------------------------------------------------------
| No Category
|--------------------------------------------------------------------------
|
--}}
@if(!$count)
	@include("manage::widgets.grid-badge" , [
		'text' => trans("categories::general.no_category"),
		'color' => "danger" ,
		'icon' => "times" ,
		'link' => $edit_link ,
	]     )
@endif

{{--
|--------------------------------------------------------------------------
| At Least One Category
|--------------------------------------------------------------------------
|
--}}
@if($count)
	@include("manage::widgets.grid-text" , [
		'text' => $model->default_category_model->titleIn($model->locale),
		'link' => $edit_link ,
		'div_class' => "mv5" ,
	])
@endif


{{--
|--------------------------------------------------------------------------
| More than one Category
|--------------------------------------------------------------------------
|
--}}
@if($count > 1)
	@include("manage::widgets.grid-text" , [
		'text' => trans("categories::general.and_n_more_categories" , [
			'n' => pd( $count-1 ) ,
		]),
		'color' => "gray" ,
		'size' => "10" ,
		'link' => $edit_link ,
	])

@endif
