@include('manage::layouts.modal-start' , [
'form_url' => route('category-downstream-save'),
'modal_title' => $modal_title ,
])
<div class="modal-body">

	{{--
	|--------------------------------------------------------------------------
	| Hidden Fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
		['_parent_hashid' , hashid($model->parent_id)] ,
		['_posttype_hashid' , $model->posttype->hashid] ,
		['is_folder' , $model->is_folder]
	]])


	{{--
	|--------------------------------------------------------------------------
	| Combo
	|--------------------------------------------------------------------------
	|
	--}}
	@if(!$model->is_folder)

		@include("manage::forms.select" , [
			'name' => "_parent_hashid",
			'value' => hashid($model->parent_id),
			'blank_value' => hashid(0) ,
			'value_field' => "hashid" ,
			'label' => trans("categories::general.folder") ,
			'options' => $model->foldersCombo(),
		])

	@endif


	{{--
	|--------------------------------------------------------------------------
	| Slug
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.input' , [
		'name' => "slug",
		'value' => $model->slug ,
		'class' => "ltr form-default" ,
	]      )


	{!!
		widget('selectize')
			->inForm()
			->name('_groups')
			->label($__module->getTrans('groups.plural-simple'))
			->options($model->definedLabelsCombo())
			->valueField('id')
			->captionField('title')
			->value($model->labels->pluck('slug')->implode(','))
	!!}


	{{--
	|--------------------------------------------------------------------------
	| Title & Image (per locale)
	|--------------------------------------------------------------------------
	|
	--}}

	@foreach($model->posttype->locales_array as $locale)
		{!!
		widget("separator")
			->label("tr:manage::forms.lang.$locale")
		!!}

		@include("manage::widgets.input-photo" , [
			'name' => "_image_in_$locale",
			'label' => trans("validation.attributes.image")   ,
			'value' => $model->imageIn($locale) ,
			'condition' => $model->hasImage(),
		])

		{!!
		widget("input")
			->name("_title_in_$locale")
			->value($model->titleIn($locale))
			->ltrIf( $is_ltr = not_in_array($locale , ['fa' , 'ar']) )
			->label("tr:validation.attributes.title")
			->inForm()
		!!}

		{!!
		widget("textarea")
			->name("_text_in_$locale")
			->value($model->textIn($locale))
			->ltrIf( $is_ltr )
			->label("tr:validation.attributes.description")
			->autoSize()
			->rows(2)
			->inForm()
			->class('tinyMini')
		!!}

	@endforeach


	{{--
	|--------------------------------------------------------------------------
	| Buttons & Feedback
	|--------------------------------------------------------------------------
	|
	--}}


	@include("manage::forms.buttons-for-modal" , [
		'extra_label' => $model->id? trans("manage::forms.button.delete") : null  ,
		'extra_shape' => 'warning' ,
		'extra_type' => "button" ,
		'extra_link' => "$('#divDeleteWarning').slideToggle('fast')" ,

		'fake' => $type_hashid = $model->posttype->hashid ,
		'cancel_link' => "masterModal(url('manage/categories/downstream/list/$type_hashid'))" ,
	])


</div>

{{--
|--------------------------------------------------------------------------
| Delete Warning
|--------------------------------------------------------------------------
|
--}}

@if($model->id)
	<div id="divDeleteWarning" class="modal-footer alert alert-danger bg-danger text-center noDisplay">
		@include('manage::forms.button' , [
			'name' => "_submit",
			'value' => "delete" ,
			'shape' => "danger" ,
			'class' => "w50" ,
			'type' => "submit" ,
			'label' => trans("manage::forms.button.sure_hard_delete") ,
		]      )
	</div>
@endif

@include('manage::layouts.modal-end')
