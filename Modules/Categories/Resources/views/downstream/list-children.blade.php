	@foreach($children as $child)
		<button class="btn btn-success btn-sm"
				onclick="masterModal('{{ url("manage/categories/downstream/edit/$child->hashid") }}')">
			<i class="fa fa-tag"></i>
			{{ $child->title }}
		</button>
	@endforeach

	@if($has_parent)
		<button class="btn btn-info btn-sm"
				onclick="masterModal('{{ url("manage/categories/downstream/create/category/$folder->hashid") }}')">
			<i class="fa fa-plus-circle"></i>
			{{ trans("categories::general.new_category") }}
		</button>
	@endif