@include('manage::layouts.modal-start' , [
	'form_url' => null,
	'modal_title' => trans("categories::general.of") . SPACE . $model->title,
])
<div class='modal-body'>

	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
	]])


	{{--
	|--------------------------------------------------------------------------
	| Warning
	|--------------------------------------------------------------------------
	|
	--}}
	@if($warn)
		@include('manage::forms.note' , [
			'shape' => "danger",
			'text' => trans("categories::general.edit_warning") ,
		]      )

	@endif

	{{--
	|--------------------------------------------------------------------------
	| Current Folders
	|--------------------------------------------------------------------------
	| 
	--}}

	@foreach($model->folders as $folder)
		<div class="mv20">
			<a class="" href="{{v0()}}" onclick="masterModal('{{ url("manage/categories/downstream/edit/$folder->hashid") }}')">
				<i class="fa fa-folder f18 mv10 mh10"></i>
				<span class="f16">{{ $folder->title }}...</span>
			</a>
			<div class="panel panel-primary mh20 p20">
				@include("categories::downstream.list-children" , [
					'children' => $folder->children ,
					'has_parent' => true ,
				])
			</div>
		</div>
	@endforeach


	{{--
	|--------------------------------------------------------------------------
	| Without Folders
	|--------------------------------------------------------------------------
	|
	--}}
	@if($model->orphans()->count())
		<div class="mv20">
			<span class="text-gray" href="{{v0()}}" >
				<i class="fa fa-folder-o f14 mv10 mh10"></i>
				<span class="f12">{{ trans("categories::general.no_folder") }}...</span>
			</span>
			<div class="panel panel-default mh20 p20">
				@include("categories::downstream.list-children" , [
					'children' => $model->orphans ,
					'has_parent' => false ,
				])
			</div>
		</div>
	@endif

	{{--
	|--------------------------------------------------------------------------
	| New Things
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="row m20">
		<div class="col-sm-6 text-center">
			<button type="button" class="btn btn-primary btn-lg w100" onclick="masterModal('{{ url("manage/categories/downstream/create/folder/$model->hashid") }}')">
				<i class="fa fa-folder f14"></i>
				<span class="f12">{{ trans("categories::general.new_folder") }}</span>
			</button>
		</div>
		<div class="col-sm-6 text-center">
			<button type="button" class="btn btn-info btn-lg w100" onclick="masterModal('{{ url("manage/categories/downstream/create/no-folder/$model->hashid") }}')">
				<i class="fa fa-tag f14"></i>
				<span class="f12">{{ trans("categories::general.new_category") }}</span>
			</button>
		</div>
	</div>

</div>

@include('manage::layouts.modal-end')