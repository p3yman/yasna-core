@include('manage::widgets.grid-row-handle' , [
    "handle" => "counter",
    "refresh_url" => route('categories.labels.row', ['hashid' => $model->hashid]),
])

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'class' => "font-yekan" ,
		'size' => "14",
		'link' => 'modal:' . route('categories.labels.editor', ['hashid' => $model->hashid], false)
	])

	@include("manage::widgets.grid-text" , [
		'text' => $model->slug,
		'size' => "13",
		'color' => 'gray',
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'size' => "10" ,
		'condition' => user()->isDeveloper(),
		'color' => 'gray',
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])

</td>

<td>
	@php $categories = $model->models @endphp

	@foreach ($categories as $category)

		@include("manage::widgets.grid-badge" , [
			'text' => $category->getFullTitleIn(getLocale()),
			'size' => "10",
			'color' => 'info',
			'icon' => 'circle',
		])

	@endforeach
</td>


@include("manage::widgets.grid-actionCol", [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			'modal:' . route('categories.labels.editor', ['hashid' => $model->hashid], false),
		],
		[
			'list',
			$__module->getTrans('groups.categories-management'),
			'modal:' . route('categories.labels.categories', ['hashid' => $model->hashid], false),
			!$model->trashed(),
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			'modal:' . route('categories.labels.delete', ['hashid' => $model->hashid, 'action' => 'soft_delete'], false),
			!$model->trashed(),
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			'modal:' . route('categories.labels.delete', ['hashid' => $model->hashid, 'action' => 'undelete'], false),
			$model->trashed(),
		],
		[
			'times',
			trans('manage::forms.button.hard_delete'),
			'modal:' . route('categories.labels.delete', ['hashid' => $model->hashid, 'action' => 'hard_delete'], false),
			$model->isTrashed(),
		],
	]
])
