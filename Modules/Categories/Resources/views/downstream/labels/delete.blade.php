@include("manage::layouts.modal-delete" , [
	'form_url' => route('categories.labels.delete', ['hashid' => $model->hashid, 'action' => $action]),
	'title_value' => $model->title,
	'save_label' => module('manage')->getTrans("forms.button.$action"),
	'save_shape' => ($action == 'undelete') ? 'primary' : 'danger' ,
])
