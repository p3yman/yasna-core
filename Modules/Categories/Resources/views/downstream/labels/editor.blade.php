{!!
	widget('modal')
		->target(route('categories.labels.save'))
		->labelIf($model->not_exists, $__module->getTrans('groups.create'))
		->labelIf($model->exists, $__module->getTrans('groups.edit-something', ['title' => $model->title]))
!!}

<div class='modal-body'>
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	{!!
		widget('input')
			->inForm()
			->name('slug')
			->label(trans_safe('validation.attributes.slug'))
			->value($model->slug)
	!!}

	{!!
		widget('input')
			->inForm()
			->name('titles[' . getLocale() . ']')
			->label(trans_safe('validation.attributes.title'))
			->value($model->exists ? $model->title : null)
	!!}
</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
