{!!
	widget('modal')
		->target(request()->url())
		->label($__module->getTrans('groups.categories-management'))
!!}


<div class='modal-body' style="padding-bottom: 50px">

	{!!
		widget('selectize')
			->inForm()
			->name('categories')
			->label($__module->getTrans('general.plural'))
			->options($categories_options)
			->valueField('id')
			->captionField('title')
			->value($categories_value)
	!!}

</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
