@extends('manage::layouts.template')

@section('content')
	@include("manage::downstream.tabs")
	@include("manage::widgets.toolbar", [
		'buttons' => [
			[
				'caption' => trans_safe('manage::forms.button.add'),
				'icon' => 'plus',
				'type' => 'success',
				'target' => 'modal:' . route('categories.labels.editor', ['hashid' => hashid(0)], false),
			]
		]
	])


	@include("manage::widgets.grid" , [
		'table_id' => "tbl-groups",
		'table_class' => 'tbl-groups',
		'row_view' => $__module->getBladePath('downstream.labels.row'),
		'handle' => "counter",
		'models' => $models,
		'operation_heading' => true,
		'headings' => [
			trans('validation.attributes.title'),
			$__module->getTrans('general.plural'),
		],
	])

@endsection
