@extends('manage::layouts.template')

@section('content')
	@include("manage::downstream.tabs")
	@include("manage::widgets.toolbar")

	<div class="container container-md">
		<div class="row-masonry row-masonry-xl-2">
			@php
				$printed = false;
			@endphp
			@foreach($models as $model)

				@if($model->can('setting'))
					@include('manage::widgets.panel-link-icon',[
						'title'=> $model->title,
						'icon' => $model->icon ,
						'link' =>  "modal:manage/categories/downstream/list/-hashid-/" ,
						'fake' => $printed = true ,
						'color' => ""
					])
				@endif

			@endforeach
		</div>
	</div>

	@if(!$printed)
		<div class="null">
			{{ trans('manage::forms.feed.nothing') }}
		</div>
	@endif

@endsection
