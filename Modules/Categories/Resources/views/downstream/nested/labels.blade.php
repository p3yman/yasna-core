@php
	$colors = ['#f9af3a', '#f9853a', '#f9613a', '#f14e3e']
@endphp

@if(isset($categories) and $categories and count($categories))
    @foreach($categories as $category)
        <div class="category_label {{ $category['active']? '' : 'deactivated' }}" style="background-color: {{ $colors[$loop->iteration % count($colors)] }};">
            <span class="category_title">
                {{ $category['title'] }}
            </span>

            <div class="category_buttons">
                <span class="category_button">
                    <i class="fa fa-edit"></i>
                </span>
                    <span class="category_button">
                    <i class="fa fa-trash"></i>
                </span>
            </div>

        </div>
    @endforeach
@endif

