@extends('manage::layouts.template')

@section('content')
    <div class="panel">
        <div class="panel-heading">
            <h4 class="mv-sm">
                {{ $__module->getTrans('general.plural') }}
            </h4>
        </div>

        <div class="panel-body text-center">
            {!!
        	    widget('button')
                ->label('Open Category Modal')
                ->shape('primary')
                ->class('btn-lg btn-taha')
                ->onClick("openCategoriesModal()")
            !!}
        </div>


        @include($__module->getBladePath('downstream.nested.main-modal'), [
            "model" => $model ,
        ])
    </div>
@endsection
