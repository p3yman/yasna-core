@php
    $options = [
        [
           "title" => "وبلاگ",
           "slug"  => "blog",
           "id"    => "blog",
        ],
        [
           "title" => "اخبار",
           "slug"  => "news",
           "id"    => "news",

        ],
        [
           "title" => "محصولات",
           "slug"  => "products",
           "id"    => "products",

        ],
    ];
@endphp

{!!
    widget('form-open')
    ->target('')
!!}

    {{--
    |--------------------------------------------------------------------------
    | Hidden Fields
    |--------------------------------------------------------------------------
    |
    --}}

    {!!
    	widget('hidden')
    	->name('folder_slug')
    	->value($folder_slug)
    !!}



    {{--
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    --}}

    {!!
        widget('input')
        ->label("tr:validation.attributes.title")
        ->name('title')
        ->inForm()
    !!}



    {{--
    |--------------------------------------------------------------------------
    | Slug
    |--------------------------------------------------------------------------
    |
    --}}

    {!!
        widget('input')
        ->label("tr:validation.attributes.slug")
        ->name('slug')
        ->valueField('slug')
        ->labelField('title')
        ->inForm()
    !!}


    {{--
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------
    |
    --}}

    {!!
        widget('combo')
        ->label("tr:validation.attributes.category")
        ->name('parent')
        ->options($options)
        ->inForm()
    !!}

    <div class="clearfix mt-xl">
        {!!
            widget('button')
            ->label('tr:manage::forms.button.save')
            ->shape('success')
            ->type('submit')
            ->class('pull-right btn-taha')
        !!}
    </div>




{!! widget('form-close') !!}
