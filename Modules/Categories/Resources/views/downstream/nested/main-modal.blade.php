@section('html_footer')

    <div class="modal fade categories_modal" tabindex="-1" id="categories_modal">
        <div class="modal-dialog m0">
            <div class="modal-content">
                {!! widget('modal')->label($__module->getTrans('general.plural')) !!}

                <div class="modal-body p0">
                    @include($__module->getBladePath('downstream.nested.browse'),[
                        "model" => $model ,
                    ])
                </div>
            </div>
        </div>
    </div>

    <script>

        /**
         * Opens Categories Modal
         */
        function openCategoriesModal() {
            $('#categories_modal').modal('show');
        }

        /**
         * Closes Categories Modal
         */
        function closeCategoriesModal() {
            $('#categories_modal').modal('hide');
        }
    </script>
@append
