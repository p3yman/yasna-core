{{--
|--------------------------------------------------------------------------
| Recursive blade
| !! ATTENTION !! This blade is being called recursively. Be careful about the
|            codes being written in this blade.
|--------------------------------------------------------------------------
|
--}}

<ul class="categories_list">
    @if(isset($categories) and $categories and count($categories))
        @foreach($categories as $category)
            <li class="categories_item">
                <div class="category_box {{ $category['active']? '' : 'deactivated' }}">
                    {{ $category['title'] }}

                    <div class="category_buttons">
                        <span class="category_button text-inverse">
                            <i class="fa fa-edit m0"></i>
                        </span>
                        <span class="category_button text-red">
                            <i class="fa fa-trash m0"></i>
                        </span>
                    </div>
                </div>

                @if(
                isset($category['children']) and
                $category['children']  and
                count($category['children'])
                )
                      @include($__module->getBladePath('downstream.nested.nested'),[
                            "categories" => $category['children'] ,
                      ])
                @endif
            </li>
        @endforeach
    @endif
</ul>
