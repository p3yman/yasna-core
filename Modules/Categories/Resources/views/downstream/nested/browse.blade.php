<div class="categories_browse bt0">
    <div class="categories_sidebar">
        <ul class="categories_folders">
            @foreach($model['folders'] as $folder)
                <li class="categories_folder">
                    <a href="#{{ $folder['slug'] }}">
                        {{ $folder['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>

        <div class="categories_footer">
            {!!
                widget('button')
                ->label('tr:categories::general.new_folder')
                ->shape('primary')
            !!}
        </div>
    </div>

    <div class="categories_content">
        @foreach($model['folders'] as $folder)
            <div class="categories_inner_body" id="{{ $folder['slug'] }}" style="display: none;">
                <div class="categories_create">
                    @include($__module->getBladePath('downstream.nested.create'), [
                        "folder_slug" => $folder['slug'] ,
                    ])
                </div>

                @if($folder['type'] === "label")
                    <div class="categories_labels">
                        @include($__module->getBladePath('downstream.nested.labels'),[
                            "categories" => isset($folder['categories']) ? $folder['categories'] : [] ,
                        ])
                    </div>
                @elseif($folder['type'] === "nested")
                    <div class="categories_nested">
                        @include($__module->getBladePath('downstream.nested.nested'),[
                            "categories" => isset($folder['categories']) ? $folder['categories'] : [] ,
                        ])
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>

@section('html_footer')
    <script>
        $(document).ready(function(){

            let folders = $('.categories_folder a');

            $(document).on('click', '.categories_folder a' ,function (e) {
                e.preventDefault();
                let targetEl = $(this);
                let target = targetEl.attr('href').slice(1);

                folders.removeClass('active');
                targetEl.addClass('active');

                $('.categories_inner_body').hide();
                $('#'+ target).show();
            });
            folders.first().click();
        });


    </script>
@append

