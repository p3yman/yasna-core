<div class="panel categories_folder">
	<div class="panel-heading">
		<div class="panel-title">
			<a data-toggle="collapse" data-parent="#{{ $parent_id }}" href="#{{ $id }}">
				{{ $title }}
			</a>
		</div>
	</div>

	<div id="{{ $id }}" class="panel-collapse collapse {{ $is_open ? 'in' : '' }}">
		<div class="panel-body ">
			@if($type === 'nested')
				<div class="categories_nested">
					@include($__module->getBladePath('post-widget.nested'), [
						"categories" => $categories ,
					])
				</div>
			@elseif($type === 'label')
				<div class="categories_label">
					@include($__module->getBladePath('post-widget.label'),[
						  "categories" => $categories ,
					])
				</div>
			@endif
		</div>
	</div>
</div>


