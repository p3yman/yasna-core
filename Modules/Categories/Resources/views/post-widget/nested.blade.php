<ul class="category_list">
	@foreach($categories as $category)
		<li class="category_item">
			{!!
				widget('checkbox')
				->label($category['title'])
				->name($category['slug'])
				->id($category['slug'])
			!!}

			@if(
                isset($category['children']) and
                $category['children']  and
                count($category['children'])
                )
				@include($__module->getBladePath('post-widget.nested'),[
					  "categories" => $category['children'] ,
				])
			@endif
		</li>
	@endforeach
</ul>
