{!!
	widget('selectize')
	->label('tr:categories::general.plural')
	->name('label_categories') // TODO: Change the name to what is desired.
	->class('category_selectize')
	->options($categories)
	->valueField('slug')
	->labelField('title')
!!}
