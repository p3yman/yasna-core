<div class="panel-group categories_accordion" id="categories_accordion">
    @foreach($folders as $folder)
        @include($__module->getBladePath('post-widget.folder'),[
            "parent_id" => "categories_accordion" ,
            //"is_open" => ($loop->iteration === 1) ,
            "is_open" => false ,
            "id" => $folder['slug'] ,
            "title" => $folder['title'] ,
            "type" => $folder['type'] ,
            "categories" => isset($folder['categories']) ? $folder['categories'] : [] ,
        ])
    @endforeach
</div>

@include($__module->getBladePath('post-widget.scripts'))
