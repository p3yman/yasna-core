<script>
    $(document).ready(function () {

       /*
       *-------------------------------------------------------
       * Nested Categories
       *-------------------------------------------------------
       */

        let checkboxes = $('#categories_accordion .categories_nested .category_list .category_item');

        checkboxes.find('input[type="checkbox"]').on('change', function (e) {
            let isChecked = this.checked;

            if(isChecked){
                // Check parent if child is checked
                $(this).parents('.category_item').each(function () {
                    $(this).find('input[type="checkbox"]').first().prop('checked', isChecked);
                });
            }else {
                // Un-check children if parent is un-checked
                $(this).parentsUntil('.category_item')
                    .siblings('.category_list')
                    .find('.category_item input[type="checkbox"]').prop('checked', isChecked);
            }

            e.stopPropagation();

        });

        /*
        *-------------------------------------------------------
        * Tag Categories
        *-------------------------------------------------------
        */

        // let selectize = $('.categories_label input.category_selectize');
    })
</script>
