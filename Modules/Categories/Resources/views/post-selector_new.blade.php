@if($model->has('category'))
	@php
		$can_manage_categories = $model->posttype->can('settings');
	@endphp

	<div id="divCategories" class="panel panel-default categories_widget">
		{{--
		|--------------------------------------------------------------------------
		| Header
		|--------------------------------------------------------------------------
		|
		--}}


		<div class="panel-heading categories_widget_heading">
			{{ trans('validation.attributes.category') }}

			{{--
			|--------------------------------------------------------------------------
			| Management Link
			|--------------------------------------------------------------------------
			|
			--}}

			<div class="control_buttons">
				@if($can_manage_categories)
					{{--@TODO: Button to open categories main modal--}}
					<button class="btn btn-primary btn-xs mh" @click="">
						<i class="fa fa-cog m0"></i>
						{{ $__module->getTrans('general.settings_button') }}
					</button>
				@endif
				<button class="btn btn-default btn-xs" onclick="divReload('divCategoriesInside')">
					<i class="fa fa-refresh m0"></i>
				</button>
			</div>

		</div>


		{{--
		|--------------------------------------------------------------------------
		| Inside
		|--------------------------------------------------------------------------
		|
		--}}

		{{-- TODO: data-src route should be changed to a route that returns 'post-widget.body' blade.--}}
		{{-- TODO: Real data should be passed to folders. --}}
		<div id="divCategoriesInside"
			 {{--data-src="manage/categories/selector/refresh/{{$model->hashid}}/{{$model->type}}/{{$model->locale}}"--}}
		>
			@include($__module->getBladePath('post-widget.body'), [
				"folders" => [
                  [
                       "title"      => "وبلاگ",
                       "slug"       => "blog",
                       "id"         => "blog",
                       "type"       => "nested",
                       "categories" => [
                            [
                                 "title"     => "ورزشی",
                                 "slug"      => "sport",
                                 "parent_id" => "blog",
                                 "id"        => "sport",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "children"  => [],
                                           "active"    => false,

                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "children"  => [],
                                           "active"    => false,

                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],


                            ],
                            [
                                 "title"     => "سیاسی",
                                 "slug"      => "political",
                                 "parent_id" => "blog",
                                 "id"        => "political",
                                 "active"    => true,

                                 "children" => [
                                      [
                                           "title"     => "پست اول",
                                           "slug"      => "post1",
                                           "parent_id" => "political",
                                           "id"        => "post1",
                                           "active"    => true,

                                           "children" => [
                                                [
                                                     "title"     => "پست چهارم",
                                                     "slug"      => "post4",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                                [
                                                     "title"     => "پست پنچم",
                                                     "slug"      => "post5",
                                                     "parent_id" => "post1",
                                                     "id"        => "post5",
                                                     "children"  => [],
                                                     "active"    => true,

                                                ],
                                           ],
                                      ],
                                      [
                                           "title"     => "پست دوم",
                                           "slug"      => "post2",
                                           "parent_id" => "political",
                                           "id"        => "post2",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                      [
                                           "title"     => "پست سوم",
                                           "slug"      => "post3",
                                           "parent_id" => "political",
                                           "id"        => "post3",
                                           "children"  => [],
                                           "active"    => true,

                                      ],
                                 ],


                            ],
                       ],
                  ],
                  [
                       "title"      => "محصولات",
                       "slug"       => "products",
                       "id"         => "products",
                       "type"       => "label",
                       "categories" => [
                       [
							   "title"     => "پست اول",
							   "slug"      => "post1",
							   "parent_id" => "news",
							   "id"        => "post1",
							   "children"  => [],
							   "active"    => true,

						  ],
						  [
							   "title"     => "پست دوم",
							   "slug"      => "post2",
							   "parent_id" => "news",
							   "id"        => "post2",
							   "children"  => [],
							   "active"    => true,

						  ],
						  [
							   "title"     => "پست سوم",
							   "slug"      => "post3",
							   "parent_id" => "news",
							   "id"        => "post3",
							   "children"  => [],
							   "active"    => true,

						  ]
                       ],
                  ],
                  [
                       "title"      => "اخبار",
                       "slug"       => "news",
                       "id"         => "news",
                       "type"       => "nested",
                       "categories" => [
						  [
							   "title"     => "پست اول",
							   "slug"      => "post1",
							   "parent_id" => "news",
							   "id"        => "post1",
							   "children"  => [],
							   "active"    => false,

						  ],
						  [
							   "title"     => "پست دوم",
							   "slug"      => "post2",
							   "parent_id" => "news",
							   "id"        => "post2",
							   "children"  => [],
							   "active"    => true,

						  ],
						  [
							   "title"     => "پست سوم",
							   "slug"      => "post3",
							   "parent_id" => "news",
							   "id"        => "post3",
							   "children"  => [],
							   "active"    => true,

						  ],
					 ],
                  ],

             ]
			])
		</div>




	</div>
@endif
