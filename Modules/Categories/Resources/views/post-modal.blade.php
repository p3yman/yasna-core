@include("manage::layouts.modal-start" , [
	'form_url' => route('category-set') ,
	'modal_title' => trans("categories::general.selection"),
])

<div class="modal-body">


	{{--
	|--------------------------------------------------------------------------
	| id and Inputs
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	@include('manage::forms.input' , [
		'name' => '',
		'label' => trans('validation.attributes.title'),
		'value' => $model->title ,
		'extra' => 'disabled' ,
	])


	{{--
	|--------------------------------------------------------------------------
	| Categories
	|--------------------------------------------------------------------------
	|
	--}}
	@include('manage::forms.group-start')

		@include('categories::post-selector-inside')

	@include('manage::forms.group-end')



	{{--
	|--------------------------------------------------------------------------
	| Button
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::forms.buttons-for-modal" )

</div>

@include("manage::layouts.modal-end")