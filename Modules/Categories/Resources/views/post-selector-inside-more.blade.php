@if($categories->count())

	<div class="text-bold f11 {{ isset($key) and $key>0? "mv20" : "" }}">
		{{--
		|--------------------------------------------------------------------------
		| Folder Title
		|--------------------------------------------------------------------------
		|
		--}}
		@if(isset($folder_title))
			{{ $folder_title }}
		@else
			<i class="fa fa-ellipsis-h"></i>
		@endif

		{{--
		|--------------------------------------------------------------------------
		| Content
		|--------------------------------------------------------------------------
		|
		--}}

		<div class="w90 mv5 mh20 text-normal">
			@foreach($categories as $category)

				<div class="row">
					<div class="col-sm-7 catSelect" data-content="{{ $category->hashid }}"
						 onchange="inlineCats('{{ $category->hashid }}')">
						@include("manage::forms.check" , [
						'id' => "chkCategory-$category->hashid" ,
						'name' => "_category-$category->hashid",
						'title' => $category->titleIn($model->locale),
						'label' => $category->titleIn($model->locale),
						'value' => $model->isUnder($category)? true : false ,
					])
					</div>
					<div class="col-sm-5 text-left pv5">

						{{-- Original Stetement -------------------------------}}

						<span
								id="spnDefaultAlready-{{$category->hashid}}"
								class="text-bold text-success f10 {{ ($model->isUnder($category) and $model->default_category == $category->id) ? '' : 'noDisplay' }}"
						>
							<i class="fa fa-check"></i>
							{{ trans("categories::general.default") }}
						</span>


						{{-- Make It Originial -------------------------------}}


						<span
								id="spnDefaultSet-{{$category->hashid}}"
								class="f10 clickable text-gray {{ ($model->isUnder($category) and $model->default_category != $category->id) ? '' : 'noDisplay' }}"
								onclick="inlineCats('{{$category->hashid}}' , 'handle')"
						>
							{{ trans("categories::general.set_default") }}
						</span>
					</div>
				</div>

			@endforeach
		</div>
	</div>

@endif


<script>
	function inlineCats(clicked_hashid, event = 'cat') {
		let $default = $("#txtDefaultCategory");
		let $checkbox = $("#chkCategory-" + clicked_hashid);
		let $indicator = $("#spnDefaultAlready-" + clicked_hashid);
		let $handle = $("#spnDefaultSet-" + clicked_hashid);
		let checkbox_value = $checkbox.is(":checked");

		switch (event) {
			case 'cat' :

				if (checkbox_value) {
					if ($default.val() == hashid0()) {
						$default.val(clicked_hashid);
						$indicator.show();
						$handle.hide();
					}
					else {
						$handle.show();
					}
				}
				else {
					$indicator.hide();
					$handle.hide();
					if ($default.val() == clicked_hashid) {
						$default.val(hashid0());
						inlineCatsGuess();
					}
				}
				break;

			case 'handle' :
				inlineCatsRefresh();
				$default.val(clicked_hashid);
				$indicator.show();
				$handle.hide();
				break;

			case 'reset' :
				$indicator.hide();
				if (checkbox_value) {
					$handle.show();
				}
				else {
					$handle.hide();
				}
				break;

		}
	}


	function inlineCatsRefresh() {
		$(".catSelect").each(function () {
			inlineCats($(this).attr('data-content'), 'reset')
		})
	}

	function inlineCatsGuess() {
		inlineCatsRefresh();
		$(".catSelect").each(function () {
			inlineCats($(this).attr('data-content'), 'cat')
		})

	}
</script>
