<?php

namespace Modules\Categories\Entities\Traits;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Category
 *
 * @property int  $parent_id
 * @property bool $is_folder
 * @method Category grabId(int $id)
 * @method Builder where($col, $value)
 */
trait CategoryRelationsTrait
{
    /**
     * get the folder instance of the current record
     *
     * @return Category|$this
     */
    public function folder()
    {
        if (!$this->parent_id) {
            return $this;
        }

        return static::grabId($this->parent_id);
    }



    /**
     * get the folder instance of the current record
     *
     * @return $this|Category
     */
    public function getFolderAttribute()
    {
        return $this->folder();
    }



    /**
     * get a Builder of the children models
     *
     * @return Builder
     */
    public function children()
    {
        return static::where("parent_id", $this->id);
    }



    /**
     * get a Collection instance of the children models
     *
     * @return Collection
     */
    public function getChildrenAttribute()
    {
        return $this->children()->orderBy('order')->get();
    }



    /**
     * get the children of the current model in the given language
     *
     * @param string $locale
     *
     * @return Builder
     */
    public function childrenIn($locale)
    {
        return $this->children()->where('locales', 'like', "%$locale%");
    }



    /**
     * get a relationship instance of the related posts
     *
     * @return BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Post')->withTimestamps();
    }



    /**
     * get a relationship instance of the related posttype
     *
     * @return BelongsTo
     */
    public function posttype()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Posttype');
    }



    /**
     * safely destroy the model, altering all the children
     *
     * @return bool
     */
    public function safeDestroy()
    {
        if ($this->is_folder) {
            $this->children()->update([
                 'parent_id' => $this->parent_id,
            ])
            ;
        }

        return $this->hardDelete();
    }
}
