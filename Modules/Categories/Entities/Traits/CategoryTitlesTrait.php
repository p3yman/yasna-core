<?php

namespace Modules\Categories\Entities\Traits;

/**
 * Class Category
 *
 * @property array $titles
 */
trait CategoryTitlesTrait
{
    /**
     * get titles array
     *
     * @return array
     */
    public function getTitlesAttribute()
    {
        return (array)$this->getMeta("titles");
    }



    /**
     * get the title in the given language
     *
     * @param string $locale
     *
     * @return string|bool
     */
    public function titleIn($locale)
    {
        if ($locale == 'first') {
            return array_first(array_values($this->titles));
        }

        if (isset($this->titles[$locale])) {
            return $this->titles[$locale];
        }

        return false;
    }



    /**
     * Returns the title of this category containing the title of the related posttype.
     *
     * @param string $locale
     *
     * @return string
     */
    public function getFullTitleIn(string $locale): string
    {
        return $this->posttype->titleIn($locale) . ' / ' . $this->titleIn($locale);
    }



    /**
     * check if the category is defined in the given language
     *
     * @param string $locale
     *
     * @return bool
     */
    public function isIn($locale)
    {
        return boolval($this->titleIn($locale));
    }



    /**
     * get the title in the first best language
     *
     * @return bool|string
     */
    public function getTitleAttribute()
    {
        $site_locale = getLocale();

        if ($this->isIn($site_locale)) {
            return $this->titleIn($site_locale);
        } else {
            return $this->titleIn('first');
        }
    }

}
