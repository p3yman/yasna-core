<?php

namespace Modules\Categories\Entities\Traits;

use App\Models\Posttype;

/**
 * Class Category
 *
 * @property array    $texts
 * @property Posttype $posttype
 */
trait CategoryImageTrait
{
    /**
     * determine if the category has image feature
     *
     * @return bool
     */
    public function hasImage()
    {
        return $this->posttype->has('category_image');
    }



    /**
     * get the image in the given language
     *
     * @param string $locale
     *
     * @return string|bool
     */
    public function imageIn($locale)
    {
        if ($this->hasImage()) {
            return ($this->getMeta("images")[$locale] ?? null);
        }

        return false;
    }
}
