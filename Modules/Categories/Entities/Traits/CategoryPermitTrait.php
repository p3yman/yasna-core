<?php

namespace Modules\Categories\Entities\Traits;

use App\Models\Posttype;

/**
 * Class Category
 *
 * @property Posttype $posttype
 */
trait CategoryPermitTrait
{
    /**
     * determine if the category can be touched by the current user
     *
     * @return bool
     */
    public function can()
    {
        return $this->posttype->can('setting');
    }



    /**
     * determine if the category cannot be touched by the current user
     *
     * @return bool
     */
    public function cannot()
    {
        return !$this->can();
    }
}
