<?php

namespace Modules\Categories\Entities\Traits;

/**
 * Class Category
 *
 * @property array $texts
 */
trait CategoryTextsTrait
{
    /**
     * get titles array
     *
     * @return array
     */
    public function getTextsAttribute()
    {
        return (array)$this->getMeta("texts");
    }



    /**
     * get the text in the given language
     *
     * @param string $locale
     *
     * @return string|bool
     */
    public function textIn($locale)
    {
        if ($locale == 'first') {
            return $this->texts[0];
        }

        if (isset($this->texts[$locale])) {
            return $this->texts[$locale];
        }

        return false;
    }
}
