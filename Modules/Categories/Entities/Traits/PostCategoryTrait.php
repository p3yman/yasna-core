<?php
namespace Modules\Categories\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Categories\Entities\Category;

/**
 * Class PostCategoryTrait
 *
 * @package Modules\Categories\Entities\Traits
 * @property $id
 * @property $categories
 * @property $default_category
 * @property $category_ids
 * @property $categories_count
 * @method Builder elector()
 */
trait PostCategoryTrait
{
    /**
     * Conventional Relationship Method
     *
     * @return mixed
     */
    public function categories()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Category')->withTimestamps();
    }



    /**
     * Generates $post->category_ids attribute.
     *
     * @return array
     */
    public function getCategoryIdsAttribute()
    {
        $categories = $this->categories;
        $list       = [];
        foreach ($categories as $category) {
            array_push($list, $category->id);
        }

        return $list;
    }



    /**
     * Generates $post->default_category_model attribute, to return an instance of the default category.
     *
     * @return Category
     */
    public function defaultCategoryModel()
    {
        $model = model('category', $this->default_category);
        if (!$model->exists) {
            return $this->guessDefaultCategory();
        }

        return $model;
    }



    /**
     * Generates $post->default_category_model attribute, to return an instance of the default category.
     *
     * @return Category
     */
    public function getDefaultCategoryModelAttribute()
    {
        return $this->defaultCategoryModel();
    }



    /**
     * Updates category in-record cache, on demand.
     *
     * @param $default_category
     *
     * @return bool
     */
    public function updateCategoryCache($default_category)
    {
        $count = $this->categories()->count();
        if (!$count) {
            $default_category = 0;
        }

        return $this->update([
             'categories_count' => $count,
             'default_category' => $default_category,
        ]);
    }



    /**
     * @param Category|int $default_category
     *
     * @return bool
     */
    public function updateDefaultCategory($default_category)
    {
        if (is_int($default_category)) {
            $default_category = model('category', $default_category);
        }

        $update_array = [
             "default_category" => $default_category->id,
        ];

        return $this->refresh()->update($update_array);
    }



    /**
     * @param int $category_id
     */
    public function electorCategory($category_id)
    {
        $this->electorCategories($category_id);
    }



    /**
     * @param int|array|string $category_ids
     */
    public function electorCategories($category_ids)
    {
        if ($this->electorCategoriesForNonAssigned($category_ids)) {
            return;
        }

        $this->electorCategoriesForNormalRequests($category_ids);
    }



    /**
     * Elector for when posts without any categories are requested, by specifying a `no`
     *
     * @param $category
     *
     * @return bool
     */
    private function electorCategoriesForNonAssigned($category)
    {
        if (is_string($category) and strtolower($category) == 'no') {
            $this->elector()->has('categories', '=', 0);
            return true;
        }

        return false;
    }



    /**
     * Elector for when posts, with one/some category(es) are requested (Normal Usage).
     *
     * @param $category_ids
     */
    private function electorCategoriesForNormalRequests($category_ids)
    {
        $category_ids = (array)$category_ids;
        if (count($category_ids)) {
            $this->elector()->whereHas('categories', function ($query) use ($category_ids) {
                $query->whereIn('categories.id', $category_ids)->orWhereIn('categories.slug', $category_ids);
            })
            ;
        }
    }



    /**
     * Checks if the post is under a certain category.
     *
     * @param Category $category
     *
     * @return bool
     */
    public function isUnder($category)
    {
        return (in_array($category->id, $this->category_ids));
    }



    /**
     * @param Category $category
     *
     * @return bool
     */
    public function isNotUnder($category)
    {
        return !$this->isUnder($category);
    }



    /**
     * @param $data
     *
     * @return bool
     */
    public function saveCategories($data)
    {
        // Categories ...
        $selected_categories = [];
        foreach ($data as $key => $value) {
            if (str_contains($key, '_category-') and $value) {
                $category = model('_category', str_replace('_category-', null, $key));
                if ($category) {
                    $selected_categories[] = $category->id;
                }
            }
        }

        return $this->attachCategories($selected_categories, hashid($data['default_category']));
    }



    /**
     * @return Category
     */
    public function guessDefaultCategory()
    {
        if (!$this->categories_count) {
            return model("Category");
        }

        $first = $this->categories()->first();
        if ($first) {
            $this->updateDefaultCategory($first);
            return $first;
        }

        return model("Category");
    }



    /**
     * attach categories to a selected post
     *
     * @param int|array $categories
     * @param int       $default_category
     *
     * @return bool
     */
    public function attachCategories($categories, $default_category = 0)
    {
        $categories = (array)$categories;

        $this->categories()->sync($categories);
        $this->updateCategoryCache($default_category);

        return true; //<~~ TODO: Blind Feedback!
    }
}
