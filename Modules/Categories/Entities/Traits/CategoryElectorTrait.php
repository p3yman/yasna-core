<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/17/18
 * Time: 12:44 PM
 */

namespace Modules\Categories\Entities\Traits;


trait CategoryElectorTrait
{
    /**
     * Modifies elector based on the specified posttypes.
     *
     * @param array $posttypes_indentifiers
     */
    protected function electorPosttypes(array $posttypes_indentifiers)
    {
        $posttypes_ids = collect($posttypes_indentifiers)
             ->map(function ($identifier) {
                 return posttype($identifier);
             })
             ->pluck('id')
             ->filter()
        ;


        $this->elector()->whereIn('posttype_id', $posttypes_ids);
    }



    /**
     * Modifies elector based on the specified posttype.
     *
     * @param string|int $posttype_identifier
     */
    public function electorPosttype($posttype_identifier)
    {
        $this->electorPosttypes((array)$posttype_identifier);
    }



    /**
     * Modifies elector based on the specified parents.
     *
     * @param array $parents_identifiers
     */
    public function electorParents(array $parents_identifiers)
    {
        $parents_ids = collect($parents_identifiers)
             ->map(function ($identifier) {
                 return model('category')->grab($identifier);
             })
             ->pluck('id')
             ->filter(function ($id) {
                 return !is_null($id);
             })
        ;


        $this->elector()->whereIn('parent_id', $parents_ids);
    }



    /**
     * Modifies elector based on the specified parent.
     *
     * @param string|int $parent_identifier
     */
    public function electorParent($parent_identifier)
    {
        $this->electorParents((array)$parent_identifier);
    }



    /**
     * Modifies elector based on the specified is folder condition.
     *
     * @param bool $value
     */
    public function electorIsFolder(bool $value)
    {
        $this->elector()->whereIsFolder(intval($value));
    }
}
