<?php

namespace Modules\Categories\Entities\Traits;

trait PostCategoryResourcesTrait
{
    /**
     * get Categories resource.
     *
     * @return array
     */
    protected function getCategoriesResource()
    {
        if ($this->hasnot('category')) {
            return [];
        }

        return $this->getResourceFromCollection($this->categories);
    }
}
