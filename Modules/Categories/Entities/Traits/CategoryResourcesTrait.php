<?php

namespace Modules\Categories\Entities\Traits;

use App\Models\Category;

trait CategoryResourcesTrait
{
    /**
     * boot CategoryResourcesTrait
     *
     * @return void
     */
    public static function bootCategoryResourcesTrait()
    {
        static::addDirectResources([
             "slug",
             "is_folder",
             "parent_id",
        ]);
    }



    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * get Posttype resource.
     *
     * @return string
     */
    protected function getPosttypeResource()
    {
        return $this->posttype->slug;
    }



    /**
     * get Text resource.
     *
     * @return string
     */
    protected function getTextResource()
    {
        return $this->textIn(getLocale());
    }



    /**
     * get ImageHashid resource.
     *
     * @return string
     */
    protected function getImageHashidResource()
    {
        return $this->imageIn(getLocale()) ?: null;
    }



    /**
     * get Children resource.
     *
     * @return array
     */
    protected function getChildrenResource()
    {
        return $this->getResourceFromCollection($this->children()->get());
    }
}
