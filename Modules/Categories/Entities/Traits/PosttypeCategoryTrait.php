<?php
namespace Modules\Categories\Entities\Traits;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PosttypeCategoryTrait
{

    /**
     * get related categories
     *
     * @return HasMany
     */
    public function categories()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Category');
    }



    /**
     * get a Builder of all the category folders
     *
     * @return Builder
     */
    public function folders()
    {
        return $this->categories()->where('is_folder', true)->where('parent_id', 0);
    }



    /**
     * get a Collection of all the category folders
     *
     * @return Collection
     */
    public function getFoldersAttribute()
    {
        return $this->folders()->orderBy('order')->get();
    }



    /**
     * get a Builder of all the categories without folders
     *
     * @return Builder
     */
    public function orphans()
    {
        return $this->categories()->where('is_folder', false)->where('parent_id', 0);
    }



    /**
     * get a Collection of all the categories without folders
     *
     * @return Collection
     */
    public function getOrphansAttribute()
    {
        return $this->orphans()->orderBy('order')->get();
    }



    /**
     * get a Builder of the category folders in the given language
     *
     * @param string $locale
     *
     * @return Builder
     */
    public function foldersIn($locale)
    {
        return $this->folders()->where('locales', 'like', "%$locale%");
    }



    /**
     * get a Builder of unfolded categories in the given language
     *
     * @param string $locale
     *
     * @return Builder
     */
    public function orphansIn($locale)
    {
        return $this->orphans()->where('locales', 'like', "%$locale%");
    }
}
