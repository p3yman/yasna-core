<?php

namespace Modules\Categories\Entities;

use App\Models\Label;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Categories\Entities\Traits\CategoryApiTrait;
use Modules\Categories\Entities\Traits\CategoryImageTrait;
use Modules\Categories\Entities\Traits\CategoryPermitTrait;
use Modules\Categories\Entities\Traits\CategoryElectorTrait;
use Modules\Categories\Entities\Traits\CategoryRelationsTrait;
use Modules\Categories\Entities\Traits\CategoryResourcesTrait;
use Modules\Categories\Entities\Traits\CategoryTextsTrait;
use Modules\Categories\Entities\Traits\CategoryTitlesTrait;
use Modules\Label\Entities\Traits\LabelsTrait;
use Modules\Yasna\Services\YasnaModel;

class Category extends YasnaModel
{
    use SoftDeletes;
    use CategoryRelationsTrait;
    use CategoryTitlesTrait;
    use CategoryTextsTrait;
    use CategoryImageTrait;
    use CategoryPermitTrait;
    use CategoryElectorTrait;
    use CategoryResourcesTrait;
    use LabelsTrait;

    public static $reserved_slugs = 'root,admin';



    /**
     * get main meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return ['images', 'titles', 'texts'];
    }



    /**
     * @return Collection
     */
    public function foldersCombo()
    {
        return $this->posttype->folders;
    }



    /**
     * Returns an array of the defined labels for this model.
     *
     * @return array
     */
    public static function definedLabelsCombo()
    {
        return static::instance()
                     ->definedLabels()
                     ->orderByDesc('created_at')
                     ->get()
                     ->map(function (Label $label) {
                         return [
                              'id'    => $label->slug,
                              'title' => $label->title,
                         ];
                     })->toArray()
             ;
    }
}
