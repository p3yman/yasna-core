<?php

namespace Modules\Categories\Database\Seeders;

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->mainData());
    }



    /**
     * get the seeder data
     *
     * @return array
     */
    protected function mainData()
    {
        $data = [];

        $feature = model("feature", "category", true);
        if (!$feature->exists) {
            $data[] = [
                 'order' => "41",
                 'slug'  => "category",
                 'title' => trans("categories::general.singular"),
                 'icon'  => "folder-o",
            ];
        }

        $feature = model("feature", "category_image", true);
        if (!$feature->exists) {
            $data[] = [
                 'order' => "41",
                 'slug'  => "category_image",
                 'title' => trans("categories::general.category_image"),
                 'icon'  => "folder-o",
            ];
        }

        return $data;
    }
}
