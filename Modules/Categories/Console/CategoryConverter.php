<?php

namespace Modules\Categories\Console;

use App\Models\Category;
use App\Models\Label;
use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CategoryConverter extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:category-convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert the all old category to new module.';

    /**
     * keep the label folder title
     *
     * @var string
     */
    protected $folder_title;

    /**
     * keep the label folder slug
     *
     * @var string
     */
    protected $folder_slug;

    /**
     * keep the label folder id
     *
     * @var int
     */
    protected $folder_id = 0;



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->purifier();
        $this->discoverFolderId();
        $count = $this->loopThroughParentCategories();

        $this->info("Total $count number(s) of categories are converted.");
    }



    /**
     * purify input and set default values if not given
     */
    protected function purifier()
    {
        $this->folder_slug  = $this->argument('slug');
        $this->folder_title = $this->argument('title');

        if (!$this->folder_slug) {
            $this->folder_slug = 'category';
        }

        if (!$this->folder_title) {
            $this->folder_title = trans('categories::general.categories-folder');
        }
    }



    /**
     * discover the folder id by either creating a new record or locating an existing one.
     */
    protected function discoverFolderId()
    {
        $label = label($this->folder_slug);
        if ($label->exists) {
            $this->folder_id = $label->id;
            return;
        }

        $options = [
             "custom" => [
                  "feature_active" => true,
                  "feature_color"  => false,
                  "feature_pic"    => false,
                  "feature_nested" => true,
                  "feature_desc"   => true,
                  "feature_text"   => true,
             ],
        ];

        $parent = Posttype()->newLabel($this->folder_slug, $this->folder_title, $options);

        $this->attachLabelFeature();

        if ($parent->exists) {
            $this->folder_id = $parent->id;
            return;
        }

        $this->error("Unexpected error occurred when trying to create the folder.");
        die();
    }



    /**
     * loop through categories parent
     *
     * @return int
     */
    protected function loopThroughParentCategories()
    {
        $count = 0;

        foreach (Category::where('is_folder', 1)->get() as $parent_category) {

            $label = $this->convertOneCategory($parent_category);

            $parent_id = $label->id;

            $count += $this->loopThroughChildCategories($parent_category, $parent_id);

            if ($label->exists) {
                $count++;
            }

        }
        return $count;
    }



    /**
     * loop through categories parent
     *
     * @param Category $parent_category
     * @param int      $parent_id
     *
     * @return int
     */
    protected function loopThroughChildCategories($parent_category, $parent_id)
    {
        $count = 0;

        foreach (Category::where('parent_id', $parent_category->id)->get() as $child_category) {

            $label = $this->convertOneCategory($child_category, $parent_id);

            $this->loopThroughPosts($label, $child_category);

            if ($label->exists) {
                $count++;
            }
        }

        return $count;
    }



    /**
     * get posts with pivot category table
     *
     * @param label    $label
     * @param Category $category
     */
    protected function loopThroughPosts($label, $category)
    {

        foreach ($category->posts as $post) {
            $this->attachLabel($post, $label, $category);
        }

    }



    /**
     * convert one category
     *
     * @param Category $category
     * @param int|null $parent_id
     *
     * @return Label
     */
    protected function convertOneCategory($category, $parent_id = null)
    {

        $label = $this->createLabelRecord($category, $parent_id);

        return $label;
    }



    /**
     * create a new label record
     *
     * @param Category $category
     * @param int|null $parent_id
     *
     * @return Label
     */
    protected function createLabelRecord($category, $parent_id = null)
    {
        $label_slug = $this->folder_slug . "-" . str_slug($category->slug);

        $label_title = $category->titles;

        $category_text = $category->texts;

        $options = [
             "parent_id" => $parent_id ? $parent_id : $this->folder_id,
             "custom"    => ["text"=>$category_text],
        ];

        return post()->newLabel($label_slug, $label_title, $options);

    }



    /**
     * attach the label feature to posttype
     */
    protected function attachLabelFeature()
    {
        $posttypes = posttype()->having('category')->get();

        /** @var Posttype $posttype */
        foreach ($posttypes as $posttype) {

            $posttype->attachFeatures('labels');
            $posttype->attachLabel($this->folder_slug);
            $posttype->cacheFeatures();
            $posttype->attachRequiredInputs();
        }
    }



    /**
     * attach the newly-generated label to the post
     *
     * @param Post     $post
     * @param Label    $label
     * @param Category $category
     *
     * @return boolean
     */
    protected function attachLabel($post, $label, $category)
    {

        $result = $post->attachLabel($label->slug);

        if ($result) {
            $category->batchSave([
                 "converted" => "1",
            ]);
        }

        return $result;
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['slug', InputArgument::OPTIONAL, 'The slug of the labels folder'],
             ['title', InputArgument::OPTIONAL, 'The title of the labels folder'],
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
