<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use App\Models\ContentType;
use App\Models\Label;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class FolderTrashRequest extends YasnaFormRequest
{

    /**
     * @var Label
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "Label";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $contenttype = new ContentType();

        if ($this->model->isFolder()) {

            return $contenttype->canConfigureLabelFolders();
        }

        if ($this->model->exists) {
            return $contenttype->canConfigureLabels();
        }

        return $contenttype->canConfigureLabels() or $contenttype->canCreateLabels();
    }

}
