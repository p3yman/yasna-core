<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use App\Models\ContentType;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class GetPostLabelsByLabelRequest
 *
 * @property ContentType $model
 */
class GetPostLabelsRequest extends YasnaFormRequest
{
    protected $responder = 'white-house';

    /**
     * @inheritdoc
     */
    protected $model_name = "ContentType";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /** @var array */
    protected $validators = [];

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    protected function loadModel($id_or_hashid = false)
    {
        $value = $this->getData('contenttype');

        if (is_numeric($value)) {
            $this->failedAuthorization();
        }

        $this->model = model('ContentType', $value);

        if (!$this->model->exists) {
            $this->failedAuthorization();
        }
    }


}
