<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use App\Models\Label;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


/**
 * Class GetPostLabelsByLabelRequest
 *
 * @property Label $model
 */
class GetPostLabelsByLabelRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Label";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function modelRequested(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    protected function getModelInstance()
    {
        $label = label()->grabHashid($this->getData("label"));

        if (!in_array($label->model_name, ["ContentType", "ContentPost"])) {
            return label();
        }

        return label()->grabHashid($this->getData("label"));
    }
}
