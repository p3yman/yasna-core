<?php

namespace Modules\ContentLabel\Http\Requests\V1;


use App\Models\ContentType;
use App\Models\Label;

/**
 * Class FolderSaveRequest
 *
 * @property Label $model
 */
class FolderSaveRequest extends DownstreamRequestAbstract
{

    /** @var array */
    public $features = [];

    /** @var array */
    protected $validators = [];

    /**
     * @inheritdoc
     */
    protected $model_name = "Label";
    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $contenttype = new ContentType();

        return $contenttype->canConfigureLabelFolders();
    }



    /**
     * @inheritdoc
     */
    public function normalRules()
    {
        return [
             "titles"         => "required|array|filled|min:1",
             "types"          => "required|array|filled|min:1|" . $this->typeAllowedValidation(),
             "feature_active" => "required|integer|digits_between: 0,1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function slugRules()
    {
        return [
             "slug" => $this->slugValidation(),
        ];
    }



    /**
     * validation slug
     *
     * @return string
     */
    public function slugValidation()
    {
        if ($this->model->slug != $this->getData('slug')) {
            $array_param = ['slug' => $this->getData('slug'), 'model_name' => 'ContentType'];
            $model       = $this->model->where($array_param);
            return $model->exists() ? 'invalid' : '';
        }
        return "";
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->filterTitlesArray();
        $this->convertContenttypes();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if (!$this->model->exists) {
            $this->checkSlug();
        }

        $this->configureCustomOptions();
        $this->convertTypes();
    }



    /**
     * configure custom options
     */
    private function configureCustomOptions()
    {
        $features = ContentType::availableLabelFeatures();
        $custom   = [];

        foreach ($features as $feature) {
            $custom[$feature] = (bool)$this->getData($feature);
        }

        $this->setData("custom", $custom);
    }



    /**
     * convert contenttypes to array
     */
    private function convertContenttypes()
    {
        $data = (Array)$this->getData("types");

        $array = array_filter($data);

        $this->setData("types", $array);
    }



    /**
     * If not exist slug, it would create a unique slug
     */
    private function checkSlug()
    {
        if (!$this->slug) {
            $this->createSlug();
        }
    }



    /**
     * check for The folder will only be attached to the content type with the labels feature
     *
     * @return string
     */
    private function typeAllowedValidation()
    {
        $types = $this->getData('types');

        foreach ($types as $type) {

            if (!is_numeric(hashid($type))) {
                return 'accepted';
            }

            $model = model('ContentType', hashid($type));
            if ($model->exists && $this->hasFeature($model)) {
                return '';
            }

        }
        return 'accepted';
    }



    /**
     * Checks the existence of  labels  feature for the content type
     *
     * @param ContentType $model
     *
     * @return bool
     */
    private function hasFeature($model)
    {
        return $model->has('labels');
    }



    /**
     * convert hashid for attach model
     */
    public function convertTypes()
    {
        $array = [];
        foreach ($this->getData('types') as $type) {
            $array[] = hashid($type);
        }

        $this->setData('types', $array);
    }

}
