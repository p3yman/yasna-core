<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

class GetPostsListRequest extends YasnaFormRequest
{
    protected $responder = 'white-house';

    protected $model_name = "Label";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function loadModel($id_or_hashid = false)
    {

        $label = $this->getData('label');

        if (is_numeric($label)) {
            $this->failedAuthorization();
        }

        $this->model = label($label);

        $model_name = $this->model->model_name;

        if ($model_name != "ContentPost") {
            $this->failedAuthorization();
        }

        if (!$this->model->exists) {
            $this->failedAuthorization();
        }
    }
}
