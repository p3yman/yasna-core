<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class DownstreamRequestAbstract extends YasnaFormRequest
{
    protected $model_name = "Label";

    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "titles.required"               => trans("content-label::downstream.titles_required"),
             "types.required"                => trans("content-label::downstream.types_required"),
             "folder_hashid.accepted"        => trans("content-label::downstream.folder_hashid_accepted"),
             "slug.invalid"                  => trans("content-label::downstream.folder_slug_unique"),
             "feature_active.integer"        => trans("content-label::downstream.feature_active_integer"),
             "feature_active.required"       => trans("content-label::downstream.feature_active_required"),
             "feature_active.digits_between" => trans("content-label::downstream.feature_active_digits_between"),
             "activeness.integer"            => trans("content-label::downstream.feature_active_integer"),
             "activeness.required"           => trans("content-label::downstream.feature_active_required"),
             "activeness.digits_between"     => trans("content-label::downstream.feature_active_digits_between"),
             "types.accepted"                => trans("content-label::downstream.types_accepted"),

        ];
    }



    /**
     * filter empty entries in the titles array
     */
    protected function filterTitlesArray()
    {
        $data = (Array)$this->getData("titles");

        $this->setData("titles", array_filter($data));
    }



    /**
     * create slug, according to the given titles
     */
    protected function createSlug()
    {
        $given = $this->getData("slug");
        $guess = $given ? $given : $this->guessSlug();

        do {
            $guess .= "-" . str_random(3);
        } while (label()->isDefined("contentType", $guess));

        $this->setData("slug", $guess);
    }



    /**
     * guess a slug, according to the given titles
     *
     * @return string
     */
    private function guessSlug(): string
    {
        $titles = (array)$this->getData('titles');
        $title  = isset($titles['en']) ? $titles['en'] : array_first($titles);

        return str_slug($title);
    }

}
