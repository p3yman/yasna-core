<?php

namespace Modules\ContentLabel\Http\Requests\V1;

use App\Models\ContentType;
use App\Models\Label;


class LabelSaveRequest extends DownstreamRequestAbstract
{

    /**
     * @var Label
     */
    protected $folder;

    /**
     * @var bool
     */
    protected $parent_accepted;


    /** @var array */
    public $features = [];

    /** @var array */
    protected $validators = [];

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $contenttype = new ContentType();

        if ($this->model->exists) {
            return $contenttype->canConfigureLabels();
        }

        return $contenttype->canConfigureLabels() or $contenttype->canCreateLabels();
    }



    /**
     * @inheritdoc
     */
    public function normalRules()
    {
        return [
             "titles"        => "required|array|filled|min:1",
             "folder_hashid" => $this->folderValidation(),
             "parent_id"     => $this->parent_accepted ? "" : "accepted",
             "activeness"    => "required|integer|digits_between: 0,1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function slugRules()
    {
        return [
             "slug" => $this->slugValidation(),
        ];
    }



    /**
     * validation slug
     *
     * @return string
     */
    public function slugValidation()
    {
        if ($this->model->slug != $this->getData('slug')) {
            $array_param = ['slug' => $this->getData('slug'), 'model_name' => 'ContentPost'];
            $model       = $this->model->where($array_param);

            return $model->exists() ? 'invalid' : '';
        }

        return "";
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->filterTitlesArray();
        $this->resolveFolder();
        $this->resolveParent();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if (!$this->model->exists) {
            $this->checkSlug();
        }

        $this->configureFeaturedOptions();
    }



    /**
     * resolve folder
     *
     * @return void|string
     */
    private function resolveFolder()
    {
        $this->folder = label($this->getData("folder_hashid"));
    }



    /**
     * resolve parent
     *
     * @return void
     */
    private function resolveParent()
    {
        /*-----------------------------------------------
        | Bypass in the absence of the nested feature or when no parent is chosen ...
        */
        if (!$this->getData("parent_id") or $this->folder->hasnotFeature("nested")) {
            $this->setData("parent_id", $this->folder->id);
            $this->parent_accepted = true;
            return;
        }


        /*-----------------------------------------------
        | Normal Mode ...
        */
        $parent = label($this->getData("parent_id"));
        if ($parent->folder()->id == $this->folder->id) {
            $this->setData("parent_id", $parent->id);
            $this->parent_accepted = true;
            return;
        }

        /*-----------------------------------------------
        | In case of invalid parent ...
        */
        $this->setData("parent_id", 0);
        $this->parent_accepted = false;
    }



    /**
     * configure options made available by features
     *
     * @return void
     */
    private function configureFeaturedOptions()
    {
        $custom = [
             "color" => $this->getFeatureValue("color"),
             "pic"   => $this->getFeatureValue("pic"),
             "desc"  => $this->getFeatureValue("desc"),
             "text"  => $this->getFeatureValue("text"),
        ];

        $this->setData("custom", $custom);
    }



    /**
     * get feature value
     *
     * @param string $feature
     *
     * @return mixed
     */
    private function getFeatureValue(string $feature)
    {
        if ($this->folder->hasnotFeature($feature)) {
            return null;
        }

        return $this->getData($feature);
    }



    /**
     * If not exist slug, it would create a unique slug
     */
    private function checkSlug()
    {
        if (!$this->slug) {
            $this->createSlug();
        }
    }



    /**
     * Only allow hashid
     *
     * @return string
     */
    private function folderValidation()
    {
        $this->folder = $this->folder ?? $this->model;

        if (!is_numeric(hashid($this->getData('folder_hashid'))) or !$this->folder->exists) {
            return 'accepted';
        }

        return '';
    }


}
