<?php

namespace Modules\ContentLabel\Http\Endpoints\V1;

use Modules\ContentLabel\Http\Controllers\V1\LabelController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/content-label-Label-trash
 *                    Label Trash
 * @apiDescription    delete a label
 * @apiVersion        1.0.0
 * @apiName           Folder Trash
 * @apiGroup          ContentLabel
 * @apiPermission     Admin
 * @apiParam {String} hashid The hashid of the label
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method LabelController controller() //
 */
class LabelTrashEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Label Trash";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\ContentLabel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LabelController@trash';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata"           => hashid(1),
             "authenticated_user" => hashid(1),
        ]);
    }
}
