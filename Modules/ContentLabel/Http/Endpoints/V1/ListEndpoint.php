<?php

namespace Modules\ContentLabel\Http\Endpoints\V1;

use Modules\ContentLabel\Http\Controllers\V1\ContentPostLabelsController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-label-list
 *                    List
 * @apiDescription    if exist label get label children ,if exist contenttype
 * @apiVersion        1.0.0
 * @apiName           List
 * @apiGroup          ContentLabel
 * @apiPermission     none
 * @apiParam {string} [contenttype]   Hashid or slug of a contenttype ,There should be one of the label or type
 *           parameters
 * @apiParam {string} [label]  Hashid or slug of a label ,There should be one of the label or type parameters
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *              {
 *                  "id": "wAemA",
 *                  "parent_id": "qKXVA",
 *                  "model_name": "ContentPost",
 *                  "slug": "listenerjjjdfecv-slG",
 *                  "titles": {
 *                  "fa": "edit title",
 *                  "en": "listener,j,jjdfecv"
 *                  },
 *                  "locales": {
 *                  "fa",
 *                  "en"
 *                  },
 *                  "created_at": 1556609778,
 *                  "updated_at": 1556609778,
 *                  "deleted_at": 1556704806,
 *                  "created_by": "qKXVA",
 *                  "updated_by": null,
 *                  "deleted_by": null,
 *                  "converted": 0,
 *                  "color": null,
 *                  "custom": {
 *                  "color": null,
 *                  "pic": null,
 *                  "desc": null,
 *                  "text": null
 *              }
 *      }
 *      "results": {
 *                   "id": "qKXVA",
 *                   "parent_id": null,
 *                   "title": "edit title",
 *                   "slug": "slug editiii",
 *                   "picture": [],
 *                   "text": null,
 *                   "description": null,
 *                   "color": null
 *                }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *       "status": 400,
 *       "developerMessage": "post-labels::developerMessages.403",
 *       "userMessage": "post-labels::userMessages.403",
 *       "errorCode": 403,
 *       "moreInfo": "post-labels.moreInfo.403",
 *       "errors": []
 * }
 * @method  ContentPostLabelsController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all labels of a specific post type";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\ContentLabel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        if (request()->has("contenttype")) {
            return 'ContentPostLabelsController@getLabelsByContenttype';
        }

        if (request()->has("label")) {
            return 'ContentPostLabelsController@getLabelsByLabel';
        }

        return "";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        if (request()->has('contenttype')) {
            return api()->successRespond([

                 "id"          => "qKXVA",
                 "parent_id"   => null,
                 "title"       => "title",
                 "slug"        => "slug-adb",
                 "picture"     => [],
                 "text"        => null,
                 "description" => null,
                 "color"       => null
                 ,
            ], [
                 "authenticated_user" => user()->hashid,
            ]);
        }

        if (request()->has('label')) {
            {
                return api()->successRespond([

                     "id"         => "nAyBx",
                     "parent_id"  => "qKXVA",
                     "model_name" => "ContentPost",
                     "slug"       => "slug-el",
                     "titles"     => [
                          "en"  => "title",
                          "fa" => "persian title",
                     ],
                     "locales"    => [
                          "fa",
                          "1",
                     ],
                     "created_at" => 1556544158,
                     "updated_at" => 1556544158,
                     "deleted_at" => 1556707904,
                     "created_by" => "qKXVA",
                     "updated_by" => null,
                     "deleted_by" => null,
                     "converted"  => 0,
                     "color"      => null,
                     "custom"     => [
                          "color" => null,
                          "pic"   => null,
                          "desc"  => null,
                          "text"  => null,

                     ],
                     [
                          "authenticated_user" => user()->hashid,
                     ],
                ]);
            }
        }

    }
}
