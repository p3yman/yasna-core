<?php

namespace Modules\ContentLabel\Http\Endpoints\V1;

use Modules\ContentLabel\Http\Controllers\V1\LabelController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/content-label-label-save
 *                    Label Save
 * @apiDescription    save a label
 * @apiVersion        1.0.0
 * @apiName           Label Save
 * @apiGroup          ContentLabel
 * @apiPermission     Admin
 * @apiParam {String} [slug] The slug of the folder
 * @apiParam {string} [hashid] Required only for updating a label.
 * @apiParam {String} folder_hashid The hashid of the folder
 * @apiParam {array}  parent_id The hashid parent_id of the label
 * @apiParam {array}  titles The titles of the label
 * @apiParam {Integer=0,1}  activeness Specifies that the label must be active (`1`) or not (`0`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "PNOLA",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method LabelController controller() //
 */
class LabelSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Label Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\ContentLabel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LabelController@labelSave';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(1),
        ]);
    }
}
