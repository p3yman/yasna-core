<?php

namespace Modules\ContentLabel\Http\Endpoints\V1;

use Modules\ContentLabel\Http\Controllers\V1\ContentPostLabelsController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-label-posts-list
 *                    Posts List
 * @apiDescription    Getting Posts That Have This Label
 * @apiVersion        1.0.0
 * @apiName           Posts List
 * @apiGroup          ContentLabel
 * @apiPermission     None
 * @apiParam {string} label  Hashid or Slug of label
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "PKEnN"
 *          "label": {
 *              "id"        : "AEBMn",
 *              "created_at": 1558355758,
 *              "updated_at": 1558355758,
 *              "deleted_at": 1559331360,
 *              "created_by": null,
 *              "updated_by": null,
 *              "deleted_by": null,
 *              "parent_id" : null,
 *              "model_name": "Posttype",
 *              "slug"      : "faq",
 *              "titles"    : [
 *                  "fa": "Title"
 *              ],
 *              "locales": [
 *                  "fa"
 *              ],
 *              "converted"  : 0,
 *              "color"      : null,
 *              "custom"     : []
 *          }
 *      },
 *      "results": {
 *           "id": "qKXVA",
 *            "sisterhood": "EbdYxejEJ83zfm0mv",
 *            "locale": "en",
 *            "is_draft": 1,
 *            "type_id": "qKXVA",
 *            "master_id": 0,
 *            "user_id": "qKXVA",
 *            "organization_id": 0,
 *            "replacement_id": 0,
 *            "title": " Leonardo taste.",
 *            "created_at": 1556524057,
 *            "updated_at": 1556524057,
 *            "published_at": null,
 *            "published_by": 0,
 *            "moderated_at": null,
 *            "moderated_by": 0,
 *            "deleted_at": null,
 *            "created_by": 0,
 *            "updated_by": 0,
 *            "deleted_by": 0,
 *            "slug": "leonardo-taste",
 *            "title2": "",
 *            "text": "\r\nThere are many variations of passages of Lorem Ipsum available",
 *            "labels":
 *            {
 *              "id": "qKXVA",
 *              "parent_id": "qKXVA",
 *              "title": "edit title",
 *              "slug": "slug editiii",
 *              "picture": [],
 *              "text": null,
 *              "description": null,
 *              "color": null
 *            },
 *            {
 *              "id": "qKXVA",
 *              "parent_id": "qKXVA",
 *              "title": "edit title",
 *              "slug": "slug editiii",
 *              "picture": [],
 *              "text": null,
 *              "description": null,
 *              "color": null
 *            }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *       "status": 400,
 *       "developerMessage": "post-labels::developerMessages.403",
 *       "userMessage": "post-labels::userMessages.403",
 *       "errorCode": 403,
 *       "moreInfo": "post-labels.moreInfo.403",
 *       "errors": []
 * }
 * @method  ContentPostLabelsController controller()
 */
class PostsListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all posts of a specific label";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\ContentLabel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ContentPostLabelsController@postsList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"              => "qKXVA",
             "sisterhood"      => "EbdYxejEJ83zfm0mv",
             "locale"          => "en",
             "is_draft"        => 1,
             "type_id"         => "qKXVA",
             "master_id"       => 0,
             "user_id"         => "qKXVA",
             "organization_id" => 0,
             "replacement_id"  => 0,
             "title"           => " Leonardo taste.",
             "created_at"      => 1556524057,
             "updated_at"      => 1556524057,
             "published_at"    => null,
             "published_by"    => 0,
             "moderated_at"    => null,
             "moderated_by"    => 0,
             "deleted_at"      => null,
             "created_by"      => 0,
             "updated_by"      => 0,
             "deleted_by"      => 0,
             "slug"            => "leonardo-taste",
             "title2"          => "",
             "text"            => "\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have ",
             "labels"          => [
                  [
                       "id"          => "qKXVA",
                       "parent_id"   => "qKXVA",
                       "title"       => "edit title",
                       "slug"        => "slug editiii",
                       "picture"     => [],
                       "text"        => null,
                       "description" => null,
                       "color"       => null,
                  ],
                  [
                       "id"          => "qKXVA",
                       "parent_id"   => "qKXVA",
                       "title"       => "edit title",
                       "slug"        => "slug editiii",
                       "picture"     => [],
                       "text"        => null,
                       "description" => null,
                       "color"       => null,
                  ],
             ],
             [
                  "authenticated_user" => user()->hashid,
                  "label"              => [
                       "id"         => "AEBMn",
                       "created_at" => 1558355758,
                       "updated_at" => 1558355758,
                       "deleted_at" => 1559331360,
                       "created_by" => null,
                       "updated_by" => null,
                       "deleted_by" => null,
                       "parent_id"  => null,
                       "model_name" => "Posttype",
                       "slug"       => "faq",
                       "titles"     => [
                            "fa" => dummy()::persianTitle(),
                       ],
                       "locales"    => [
                            "fa",
                       ],
                       "converted"  => 0,
                       "color"      => null,
                       "custom"     => [],
                  ],
             ],
        ]);
    }
}
