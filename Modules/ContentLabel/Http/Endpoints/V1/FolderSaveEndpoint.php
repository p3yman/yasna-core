<?php

namespace Modules\ContentLabel\Http\Endpoints\V1;

use Modules\ContentLabel\Http\Controllers\V1\LabelController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/content-label-folder-save
 *                    Folder Save
 * @apiDescription    Save a folder
 * @apiVersion        1.0.0
 * @apiName           Folder save
 * @apiGroup          ContentLabel
 * @apiPermission     Admin
 * @apiParam {string} [hashid] Required only for updating a label.
 * @apiParam {String} [slug] The slug of the label
 * @apiParam {array}  titles The titles of the label
 * @apiParam {array}  types The hashid contenttypes
 * @apiParam {Integer=0,1}  feature_active Specifies that the label must be active (`1`) or not (`0`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => "kxPdx",
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method LabelController controller() //
 */
class FolderSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Type Label Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\ContentLabel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LabelController@folderSave';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
