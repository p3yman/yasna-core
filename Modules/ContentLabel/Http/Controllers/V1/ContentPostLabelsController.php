<?php

namespace Modules\ContentLabel\Http\Controllers\V1;

use App\Models\ContentPost;
use Illuminate\Database\Eloquent\Collection;
use Modules\ContentLabel\Http\Requests\V1\GetPostLabelsByLabelRequest;
use Modules\ContentLabel\Http\Requests\V1\GetPostLabelsRequest;
use Modules\ContentLabel\Http\Requests\V1\GetPostsListRequest;
use Modules\Yasna\Services\YasnaApiController;


class ContentPostLabelsController extends YasnaApiController
{


    /**
     * get all label of content type
     *
     * @param GetPostLabelsRequest $request
     *
     * @return array
     */
    public function getLabelsByContentType(GetPostLabelsRequest $request)
    {
        $collections = $request->model->getAllLabelsRecursively()->get();
        $result      = (content()->post())::getMappedFromCollection($collections);

        return $this->success($result, [
             "ContentType" => $request->model->hashid,
        ]);
    }



    /**
     * get all labels nested under a particular label
     *
     * @param GetPostLabelsByLabelRequest $request
     *
     * @return array
     */
    public function getLabelsByLabel(GetPostLabelsByLabelRequest $request)
    {
        $builder = $request->model->allChildren();

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get all posts of the specified label
     *
     * @param GetPostsListRequest $request
     *
     * @return array
     */
    public function postsList(GetPostsListRequest $request)
    {
        /** @var Collection $collection */
        $collection = $request->model->models()->get();
        $result     = $collection->map(function (ContentPost $item) {
            return $item->toListResource();
        });

        return $this->success($result->toArray(), [
             "label" => $request->model->toSingleResource(),
        ]);

    }
}
