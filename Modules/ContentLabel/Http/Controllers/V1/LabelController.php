<?php

namespace Modules\ContentLabel\Http\Controllers\V1;

use App\Models\ContentType;
use Modules\ContentLabel\Http\Requests\V1\FolderSaveRequest;
use Modules\ContentLabel\Http\Requests\V1\FolderTrashRequest;
use Modules\ContentLabel\Http\Requests\V1\LabelDestroyRequest;
use Modules\ContentLabel\Http\Requests\V1\LabelRestoreRequest;
use Modules\ContentLabel\Http\Requests\V1\LabelSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class LabelController extends YasnaApiController
{


    /**
     * save a label
     *
     * @param LabelSaveRequest $request
     *
     * @return string
     */
    public function labelSave(LabelSaveRequest $request)
    {
        $model = $request->model;

        $saved = content()->post()::saveLabel($model, $request->toArray());

        return $this->modelSaveFeedback($saved);

    }



    /**
     * save a folder
     *
     * @param FolderSaveRequest $request
     *
     * @return string
     */
    public function folderSave(FolderSaveRequest $request)
    {
        $saved = ContentType::saveLabel($request->model, $request->toArray());

        return $this->modelSaveFeedback($saved);
    }



    /**
     * delete a label
     *
     * @param FolderTrashRequest $request
     *
     * @return array
     */
    public function trash(FolderTrashRequest $request)
    {
        $model = $request->model->delete();

        return $this->typicalSaveFeedback($model);

    }



    /**
     * hard delete a label
     *
     * @param LabelDestroyRequest $request
     *
     * @return array
     */
    public function destroy(LabelDestroyRequest $request)
    {
        $model = $request->model->hardDelete();

        return $this->typicalSaveFeedback($model);
    }



    /**
     * perform undelete action of the label
     *
     * @param LabelRestoreRequest $request
     *
     * @return array
     */
    public function restore(LabelRestoreRequest $request)
    {
        $saved = $request->model->undelete();

        return $this->typicalSaveFeedback($saved);
    }

}
