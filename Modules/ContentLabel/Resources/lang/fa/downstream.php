<?php
return [
     "folder_slug_unique"            => "نامک تکراری است",
     "titles_required"               => "وارد کردن یک عنوان لازم است.",
     "types_required"                => "هیچ مطلبی تحت پوشش این پوشه قرار نگرفته است.",
     "folder_hashid_accepted"        => "پوشه معتبر نیست. ممکن است توسط شخص دیگری پاک شده باشد.",
     "feature_active_integer"        => "فعال بودن باید از نوع عددی باشد",
     "feature_active_required"       => "فعال بودن برچسب باید مشخص باشد",
     "feature_active_digits_between" => "عدد می تواند صفر یا یک باشد",
     "types_accepted"                => "گونه‌ی مطلب مجاز نمی باشد",
];
