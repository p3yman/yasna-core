<?php

namespace Modules\ContentLabel\Providers;

use Modules\ContentLabel\Components\ContentLabelsComponent;
use Modules\ContentLabel\Http\Endpoints\V1\LabelDestroyEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\FolderSaveEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\LabelRestoreEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\LabelTrashEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\LabelSaveEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\ListEndpoint;
use Modules\ContentLabel\Http\Endpoints\V1\PostsListEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class ContentLabelServiceProvider
 *
 * @package Modules\ContentLabel\Providers
 */
class ContentLabelServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerEndpoints();
        $this->registerComponents();
    }



    /**
     * register model traits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("ContenttypeLabelsTrait", "ContentType");
        $this->addModelTrait("ContentLabelsTrait", "ContentPost");
        $this->addModelTrait("LabelPostsTrait", "Label");
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }
        endpoint()->register(FolderSaveEndpoint::class);
        endpoint()->register(LabelSaveEndpoint::class);
        endpoint()->register(LabelTrashEndpoint::class);
        endpoint()->register(LabelDestroyEndpoint::class);
        endpoint()->register(LabelRestoreEndpoint::class);
        endpoint()->register(ListEndpoint::class);
        endpoint()->register(PostsListEndpoint::class);
    }



    /**
     * register form components
     *
     * @return void
     */
    private function registerComponents()
    {

        if ($this->cannotUseModule("Forms")) {
            return;
        }

        component()::register("content-labels", ContentLabelsComponent::class);
    }
}
