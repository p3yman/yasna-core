<?php

namespace Modules\ContentLabel\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class ContentLabelsComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "has_setting_button" => false,
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [];
    }
}
