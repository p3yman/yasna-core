<?php

namespace Modules\ContentLabel\Entities\Traits;

use App\Models\Label;

/**
 * Trait LabelPostsTrait
 *
 * @property bool        $is_nested
 * @property bool        $is_not_nested
 * @property string      $type
 * @property string|null $color
 */
trait LabelPostsTrait
{
    /**
     * get the instance of the folder (as defined in the ContentLabel module)
     *
     * @return Label|$this
     */
    public function folder()
    {
        if ($this->isFolder()) {
            return $this;
        }


        /** @var Label $candidate */
        $candidate = $this;
        do {
            $candidate = $candidate->parent();
        } while ($candidate->isNotFolder());

        return $candidate;
    }



    /**
     * determine if the label is a folder (as defined in the ContentLabel module)
     *
     * @return bool
     */
    public function isFolder(): bool
    {
        return !boolval($this->parent_id);
    }



    /**
     * determine if the label is NOT a folder (as defined in the ContentLabel module)
     *
     * @return bool
     */
    public function isNotFolder(): bool
    {
        return !$this->isFolder();
    }



    /**
     * determine if the label has a certain feature (as defined in the ContentLabel module)
     *
     * @param string $feature
     *
     * @return bool
     */
    public function hasFeature(string $feature): bool
    {
        $slug = "feature_" . $feature;

        return boolval($this->folder()->getCustomMeta($slug));
    }



    /**
     * determine if the label has NOT a certain feature (as defined in the ContentLabel module)
     *
     * @param string $feature
     *
     * @return bool
     */
    public function hasnotFeature(string $feature): bool
    {
        return !$this->hasFeature($feature);
    }



    /**
     * get data from the custom meta attribute of the label
     *
     * @param string $slug
     *
     * @return mixed
     */
    public function getCustomMeta(string $slug)
    {
        $custom = (array)$this->getMeta("custom");

        if (!isset($custom[$slug])) {
            return null;
        }

        return $custom[$slug];
    }



    /**
     * If the label is nested.
     *
     * @return bool
     */
    public function isNested()
    {
        return $this->hasFeature('nested');
    }



    /**
     * Accessor for the `isNested()` Method.
     *
     * @return bool
     */
    public function getIsNestedAttribute()
    {
        return $this->isNested();
    }



    /**
     * If the label is not nested.
     *
     * @return bool
     */
    public function isNotNested()
    {
        return !$this->isNested();
    }



    /**
     * Accessor for the `isNotNested()` Method.
     *
     * @return bool
     */
    public function getIsNotNestedAttribute()
    {
        return $this->isNotNested();
    }



    /**
     * Returns the type of the label.
     *
     * @return string
     */
    public function getType()
    {
        return ($this->isNested() ? "nested" : "flat");
    }



    /**
     * Accessor for the `getType()` Method
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        return $this->getType();
    }



    /**
     * Returns the color of the label.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->getCustomMeta('color');
    }



    /**
     * Accessor for the `getColor()` Method
     *
     * @return string|null
     */
    public function getColorAttribute()
    {
        return $this->getColor();
    }
}
