<?php

namespace Modules\ContentLabel\Entities\Traits;

use App\Models\ContentType;
use App\Models\Label;

trait ContenttypeLabelsSaveTrait
{
    /**
     * save label according to the submitted data from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return Label
     */
    public static function saveLabel(Label $label, array $data)
    {
        if ($label->exists) {
            $label = static::saveExistingLabel($label, $data);
        } else {
            $label = static::saveNewLabel($data);
        }

        static::saveLabelActivityStatus($label, $data);
        static::saveLabelContenttypes($label, $data);

        return $label;
    }



    /**
     * create a new label according to the submitted data from the downstream form
     *
     * @param array $data
     *
     * @return Label
     */
    private static function saveNewLabel(array $data)
    {
        $options = ["custom" => $data['custom']];

        $model = model('ContentType');

        /** @var ContentType $model */
        return $model->newLabel($data['slug'], $data['titles'], $options);
    }



    /**
     * update an existing label according to the submitted data from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return Label
     */
    private static function saveExistingLabel(Label $label, array $data)
    {
        $array = [
             "titles"  => json_encode($data['titles']),
             "locales" => label()->getLocalesFromTitlesArray($data['titles']),
             "custom"  => $data['custom'],
        ];

        if (dev()) {
            $array['slug'] = $data['slug'];
        }

        $saved = $label->batchSave($array);
        if ($saved) {
            static::saveLabelFlatChildren($saved);
        }

        return $saved;
    }



    /**
     * delete/undelete the label instance, according to what received from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return void
     */
    private static function saveLabelActivityStatus(Label $label, array $data)
    {
        if ($data['feature_active'] and $label->isTrashed()) {
            $label->undelete();
        } elseif (!$data['feature_active'] and $label->isNotTrashed()) {
            $label->delete();
        }
    }



    /**
     * sync the label's attached contenttypes, according to what received from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return void
     */
    private static function saveLabelContenttypes(Label $label, array $data)
    {
        $locales = $data['titles'];
        $label->models()->detach();

        foreach ($locales as $locale) {

            $label->models()->attach($data['types'], [
                 "model_name" => "ContentType",
            ])
            ;
        }
    }



    /**
     * flat all labels when they are not supposed to be nested.
     *
     * @param Label $folder
     */
    private static function saveLabelFlatChildren(Label $folder)
    {
        /*-----------------------------------------------
        | bypass if not nested ...
        */
        if ($folder->hasFeature("nested")) {
            return;
        }

        /*-----------------------------------------------
        | action ...
        */
        $labels = static::getFolderLabels($folder->id, 0, true);

        $labels->update([
             "parent_id" => $folder->id,
        ]);
    }
}
