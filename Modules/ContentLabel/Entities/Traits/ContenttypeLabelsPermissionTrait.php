<?php

namespace Modules\ContentLabel\Entities\Traits;

/**
 * Class ContentType (attached via ContentTypeLabelsTrait)
 * This is to define access levels of each particular labels set of actions.
 *
 * @TODO: To be revised and made compatible with the contenttype requirements.
 */
trait ContenttypeLabelsPermissionTrait
{
    /**
     * check if the current user can configure labels
     *
     * @return bool
     */
    public function canConfigureLabels(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the current user can configure label folders
     *
     * @return bool
     */
    public function canConfigureLabelFolders(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the current user can create new labels
     *
     * @return bool
     */
    public function canCreateLabels(): bool
    {
        return user()->isSuperadmin();
    }
}
