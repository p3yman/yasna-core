<?php 

namespace Modules\ContentLabel\Entities\Traits;

use App\Models\Label;

trait ContentLabelsSaveTrait
{
    /**
     * save label according to the submitted data from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return Label
     */
    public static function saveLabel(Label $label, array $data)
    {
        if ($label->exists) {
            $label = static::saveExistingLabel($label, $data);
        } else {
            $label = static::saveNewLabel($data);
        }

        static::saveLabelActivityStatus($label, $data);

        return $label;
    }



    /**
     * create a new label according to the submitted data from the downstream form
     *
     * @param array $data
     *
     * @return Label
     */
    private static function saveNewLabel(array $data)
    {
        $options = [
             "custom"    => $data['custom'],
             "parent_id" => $data['parent_id'],
             "color"     => $data['custom']['color'],
        ];

        return content()->post()->newLabel($data['slug'], $data['titles'], $options);
    }



    /**
     * update an existing label according to the submitted data from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return Label
     */
    private static function saveExistingLabel(Label $label, array $data)
    {
        $array = [
             "titles"    => json_encode($data['titles']),
             "locales"   => label()->getLocalesFromTitlesArray($data['titles']),
             "parent_id" => $data['parent_id'],
             "custom"    => $data['custom'],
        ];

        if (dev()) {
            $array['slug'] = $data['slug'];
        }

        return $label->batchSave($array);
    }



    /**
     * delete/undelete the label instance, according to what received from the downstream form
     *
     * @param Label $label
     * @param array $data
     *
     * @return void
     */
    private static function saveLabelActivityStatus(Label $label, array $data)
    {
        if ($data['activeness'] and $label->isTrashed()) {
            $label->undelete();
        } elseif (!$data['activeness'] and $label->isNotTrashed()) {
            $label->delete();
        }
    }
}
