<?php

namespace Modules\Label\Entities;

use Modules\Label\Entities\Traits\MainModelDefinitionTrait;
use Modules\Label\Entities\Traits\MainModelInsertionTrait;
use Modules\Label\Entities\Traits\MainModelRelationsTrait;
use Modules\Label\Entities\Traits\MainModelResourceTrait;
use Modules\Label\Entities\Traits\MainModelSlugTrait;
use Modules\Label\Entities\Traits\MainModelTitlesTrait;
use Modules\Yasna\Services\ModelTraits\UniqueUrlTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Label extends YasnaModel
{
    use SoftDeletes;

    use MainModelTitlesTrait;
    use MainModelSlugTrait;
    use MainModelRelationsTrait;
    use MainModelDefinitionTrait;
    use MainModelInsertionTrait;
    use MainModelResourceTrait;
    use UniqueUrlTrait;

    /**
     * get the array of main meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             'color',
             'custom',
        ];
    }



    /**
     * try to locate an entry. Returns a safe new instance if not found.
     *
     * @param string $model_name
     * @param string $slug
     * @param bool   $with_trashed
     *
     * @return \App\Models\Label
     */
    public function locate(string $model_name, string $slug, bool $with_trashed = false)
    {
        $builder = $this->where('model_name', $model_name)->where('slug', $slug);
        if ($with_trashed) {
            $builder->withTrashed();
        }

        /** @var \App\Models\Label $label */
        $label = $builder->first();

        if (!$label) {
            $label = $this->newInstance([
                 "model_name" => $model_name,
            ]);
        }

        return $label->spreadOptions();
    }



    /**
     * get color attribute from the meta system
     *
     * @return string
     */
    public function getColorAttribute()
    {
        return $this->getMeta('color');
    }



    /**
     * spread label's meta options
     *
     * @return $this
     */
    public function spreadOptions()
    {
        $this->spreadMeta();
        $options = (array)$this->custom;

        foreach ($options as $option => $value) {
            $this->$option = $value;
        }

        return $this;
    }
}
