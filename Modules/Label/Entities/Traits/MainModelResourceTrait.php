<?php

namespace Modules\Label\Entities\Traits;

trait MainModelResourceTrait
{
    /**
     * boot MainModelResourceTrait
     *
     * @return void
     */
    public static function bootMainModelResourceTrait()
    {
        static::addDirectResources([
             "parent_id",
             "slug",
        ]);
    }



    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        return $this->firstTitle();
    }



    /**
     * get Titles resource.
     *
     * @return array
     */
    protected function getTitlesResource()
    {
        return $this->titlesArray();
    }



    /**
     * get Locales resource.
     *
     * @return array
     */
    protected function getLocalesResource()
    {
        return $this->localesArray();
    }
}
