<?php

namespace Modules\Label\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Label
 *
 * @property int $parent_id
 */
trait MainModelRelationsTrait
{
    /**
     * prepare a standard Laravel many-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function models()
    {
        return $this
             ->belongsToMany(MODELS_NAMESPACE . $this->model_name, 'labels_pivot', 'label_id', 'model_id')
             ->withPivot('model_name')
             ->withTimestamps()
             ;
    }



    /**
     * Return a builder of all defined labels of a model. Specific language is taken into account if provided.
     *
     * @param string      $model_name
     * @param string|null $locale
     *
     * @return Builder
     */
    public function definedFor(string $model_name, string $locale = null)
    {
        if ($locale) {
            return label()->where('model_name', $model_name)->where('locales', 'like', "%$locale%");
        }

        return label()->where('model_name', $model_name);
    }



    /**
     * Return a builder of all top-level master labels of a model.
     *
     * @param string $model_name
     *
     * @return Builder
     */
    public function mastersOf(string $model_name)
    {
        return $this->definedFor($model_name)->where('parent_id', 0);
    }



    /**
     * Gets the parent model.
     *
     * @return \App\Models\Label
     */
    public function parent()
    {
        if ($this->parent_id) {
            return label($this->parent_id);
        }

        return label();
    }



    /**
     * Gets a builder of all children records.
     *
     * @return Builder
     */
    public function children()
    {
        return label()->where('parent_id', $this->id);
    }



    /**
     * Gets a builder of all children records, recursively.
     *
     * @return Builder
     */
    public function allChildren()
    {
        $all = label()->where("parent_id", ">", 0)->select("id", "parent_id")->get();
        $ids = [$this->id];

        for ($i = 1; $i <= $all->count(); $i++) {
            $children = $all->whereIn("parent_id", $ids)->whereNotIn("id", $ids)->pluck('id')->toArray();
            $i += count($children);
            $ids = array_merge($ids, $children);
        }

        return label()->whereIn('id', $ids)->where("id", "!=", $this->id);
    }



    /**
     * identify if the model has children labels.
     *
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children()->exists();
    }



    /**
     * change the parent
     *
     * @param string|array $id_or_slug
     *
     * @return bool
     */
    public function changeParent($id_or_slug)
    {
        return $this->batchSaveBoolean([
             "parent_id" => $this->getSafeParentId($this->model_name, $id_or_slug),
        ]);
    }



    /**
     * Get id or slug, locates the record and compares it to the model name, then returns its id if everything is fine.
     *
     * @param string     $model_name
     * @param string|int $id_or_slug
     *
     * @return int
     */
    public function getSafeParentId(string $model_name, $id_or_slug)
    {
        /*-----------------------------------------------
        | Normalize id ...
        */
        if ($id_or_slug == 0) {
            return 0;
        }
        if (is_numeric($id_or_slug)) {
            $parent = label($id_or_slug);
        } else {
            $parent = $this->locate($model_name, $id_or_slug);
        }

        /*-----------------------------------------------
        | Ensure Parent Id ...
        */
        $model           = model($model_name);
        $allowed_parents = [$model_name];
        if (method_exists($model, "allowedParentModels")) {
            $allowed_parents = array_merge($allowed_parents, $model::allowedParentModels());
        }

        if ($parent->exists and in_array($parent->model_name, $allowed_parents)) {
            return $parent->id;
        }

        return 0;
    }

}
