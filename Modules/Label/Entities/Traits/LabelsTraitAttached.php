<?php

namespace Modules\Label\Entities\Traits;

trait LabelsTraitAttached
{
    /**
     * Standard Laravel many-to-many relationship
     *
     * @param bool $with_trashed
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels(bool $with_trashed = false)
    {
        $builder = $this
             ->belongsToMany(MODELS_NAMESPACE . "Label", 'labels_pivot', 'model_id')
             ->withPivot('model_name', 'deleted_at')
             ->withTimestamps()
             ->where('labels.model_name', $this->getName())
        ;

        if (!$with_trashed) {
            $builder->whereNull('labels_pivot.deleted_at');
        }

        return $builder;
    }



    /**
     * Gets an array of attached label slugs.
     *
     * @param bool $with_trashed
     *
     * @return array
     */
    public function labelsArray(bool $with_trashed = false): array
    {
        return $this->labels($with_trashed)->get()->pluck('slug')->toArray();
    }



    /**
     * Counts the labels attached to the current model
     *
     * @param bool $with_trashed
     *
     * @return int
     */
    public function labelsCount(bool $with_trashed = false)
    {
        return $this->labels($with_trashed)->count();
    }



    /**
     * Determines if a particular label has been attached
     *
     * @param string $slug
     *
     * @return bool
     */
    public function hasLabel(string $slug)
    {
        return $this->getLabel($slug)->isset();
    }



    /**
     * A pretty shortcut for the contrary of hasLabel()
     *
     * @param string $slug
     *
     * @return bool
     */
    public function hasnotLabel(string $slug)
    {
        return !$this->hasLabel($slug);
    }
}
