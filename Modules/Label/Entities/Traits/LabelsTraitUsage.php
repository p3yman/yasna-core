<?php

namespace Modules\Label\Entities\Traits;

trait LabelsTraitUsage
{
    /**
     * Generates the required data for the selectize label selector tool.
     *
     * @return array
     */
    public function selectizeList()
    {
        $array  = [];
        $labels = $this->definedChildLabels()->get();

        foreach ($labels as $label) {
            $array[] = [
                 "slug"  => $label->slug,
                 "title" => $label->short_title,
            ];
        }

        return $array;
    }

    /**
     * Gets a string of imploded attached label slugs.
     *
     * @return string
     */
    public function selectizeValue()
    {
        return implode(',', $this->labelsArray());
    }
}
