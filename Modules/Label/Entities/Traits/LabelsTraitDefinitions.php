<?php

namespace Modules\Label\Entities\Traits;

use App\Models\Label;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class LabelsTraitDefinitions
 * @method string getName()
 *
 * @package Modules\Label\Entities\Traits
 */
trait LabelsTraitDefinitions
{
    /**
     * Inserts a new record in labels table
     *
     * @param string       $slug
     * @param array|string $titles
     * @param int|string   $parent_id_or_slug
     *
     * @deprecated
     * @return bool
     */
    public function defineLabel(string $slug, $titles, $parent_id_or_slug = 0)
    {
        return label()->define($this->getName(), $slug, $titles, $parent_id_or_slug);
    }



    /**
     * insert a new record in labels table
     *
     * @param string       $slug
     * @param string|array $titles
     * @param array        $options
     *
     * @return Label
     */
    public function newLabel(string $slug, $titles, $options = []): Label
    {
        return label()->new($this->getName(), $slug, $titles, $options);
    }



    /**
     * Tries to locate an entry. Returns a safe new instance if not found.
     * This is regardless of its attachment status.
     *
     * @param string $slug
     * @param bool   $with_trashed
     *
     * @return \App\Models\Label
     */
    public function findLabel($slug, $with_trashed = false)
    {
        return label()->locate($this->getName(), $slug, $with_trashed);
    }



    /**
     * Returns a builder of all defined labels.
     *
     * @param string|null $locale
     *
     * @return Builder
     */
    public function definedLabels()
    {
        return label()->definedFor($this->getName());
    }



    /**
     * Returns a builder of all defined labels, in a specific language
     *
     * @param string $locale
     *
     * @return Builder
     */
    public function definedLabelsIn($locale)
    {
        return label()->definedFor($this->getName(), $locale);
    }



    /**
     * Returns a builder of all defined parent labels.
     *
     * @param string|null $locale
     *
     * @return Builder
     */
    public function definedParentLabels()
    {
        return $this->definedLabels()->where('parent_id', 0);
    }



    /**
     * Returns a builder of all defined parent labels of a specific language.
     *
     * @param string|null $locale
     *
     * @return Builder
     */
    public function definedParentLabelsIn($locale)
    {
        return $this->definedLabelsIn($locale)->where('parent_id', 0);
    }



    /**
     * Returns a builder of all defined child labels.
     * Specific parent is taken into account if provided.
     *
     * @param string $specific_parent_slug
     *
     * @return Builder
     */
    public function definedChildLabels($specific_parent_slug = null)
    {
        $builder = $this->definedLabels()->where('parent_id', '>', 0);

        if ($specific_parent_slug) {
            $parent  = $this->getLabel($specific_parent_slug);
            $builder = $builder->where('parent_id', $parent->id);
        }

        return $builder;
    }



    /**
     * Returns a builder of all defined child labels of a Specific language,
     * Specific parent is taken into account if provided.
     *
     * @param string $locale
     * @param string $specific_parent_slug
     *
     * @return Builder
     */
    public function definedChildLabelsIn($locale, $specific_parent_slug = null)
    {
        $builder = $this->definedLabelsIn($locale)->where('parent_id', '>', 0);

        if ($specific_parent_slug) {
            $parent  = $this->getLabel($specific_parent_slug);
            $builder = $builder->where('parent_id', $parent->id);
        }

        return $builder;
    }



    /**
     * Returns an array of the slugs of all defined labels.
     *
     * @param string|null $locale
     *
     * @return array
     */
    public function definedLabelsArray(): array
    {
        return $this->definedLabels()->pluck('slug')->toArray();
    }



    /**
     * Returns an array of the slugs of all defined labels in the provided language.
     *
     * @param string $locale
     *
     * @return array
     */
    public function definedLabelsArrayIn($locale): array
    {
        return $this->definedLabelsIn($locale)->pluck('slug')->toArray();
    }
}
