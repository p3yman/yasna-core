<?php

namespace Modules\Label\Entities\Traits;

/**
 * Class Label
 *
 * @property string $slug
 * @property string $model_name
 */
trait MainModelSlugTrait
{
    /**
     * Changes the slug of the label
     *
     * @param string $new_slug
     *
     * @return bool
     */
    public function changeSlug(string $new_slug): bool
    {
        if (!$this->validateNewSlug($new_slug)) {
            return false;
        }

        return $this->batchSaveBoolean([
             "slug" => $this->validateNewSlug($new_slug) ? $new_slug : $this->slug,
        ]);
    }



    /**
     * Changes the titles and slug of the label. Current site locale is considered if $changing_titles is a string.
     *
     * @param string|array $changing_titles
     * @param string       $new_slug
     *
     * @return bool
     */
    public function changeTitlesAndSlug($changing_titles, string $new_slug): bool
    {
        $current_titles  = $this->titlesArray();
        $changing_titles = $this->getSafeTitlesArray($changing_titles);
        $new_titles      = array_default($changing_titles, $current_titles);

        return $this->batchSaveBoolean([
             "titles"  => json_encode($new_titles),
             "locales" => $this->getLocalesFromTitlesArray($new_titles),
             "slug"    => $this->validateNewSlug($new_slug) ? $new_slug : $this->slug,
        ]);
    }



    /**
     * Checks to see if the given slug is usable.
     *
     * @param string $slug
     *
     * @return bool
     */
    private function validateNewSlug(string $slug): bool
    {
        if ($this->slug == $slug) {
            return true;
        }

        return $this->isNotDefined($this->model_name, $slug);
    }

}
