<?php

namespace Modules\Label\Entities\Traits;

/**
 * Class Label
 *
 * @property string $titles
 */
trait MainModelTitlesTrait
{
    /**
     * get a safe array of the titles of the label
     *
     * @return array
     */
    public function titlesArray(): array
    {
        return (array)json_decode($this->titles, true);
    }



    /**
     * get the title of the label in specific language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function titleIn(string $locale)
    {
        $array = $this->titlesArray();
        if (isset($array[$locale])) {
            return $array[$locale];
        }

        return null;
    }



    /**
     * get the first title of the label, with higher priority for Persian language.
     *
     * @return string
     */
    public function firstTitle()
    {
        $site_locale = getLocale();

        if ($this->titleIn($site_locale)) {
            return $this->titleIn($site_locale);
        }

        if (count($this->titlesArray())) {
            return array_first($this->titlesArray());
        }

        return '-';
    }



    /**
     * get the first title
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->firstTitle();
    }



    /**
     * get the short form of the first title, suitable to show in UI.
     *
     * @return string
     */
    public function getShortTitleAttribute()
    {
        return str_limit($this->firstTitle(), 20);
    }



    /**
     * Changes the titles of the label. Current site locale is considered if $changing_titles is a string.
     *
     * @param string|array $changing_titles
     *
     * @return bool
     */
    public function changeTitles($changing_titles): bool
    {
        $current_titles  = $this->titlesArray();
        $changing_titles = $this->getSafeTitlesArray($changing_titles);
        $new_titles      = array_default($changing_titles, $current_titles);

        return $this->batchSaveBoolean([
             "titles"  => json_encode($new_titles),
             "locales" => $this->getLocalesFromTitlesArray($new_titles),
        ]);
    }



    /**
     * Gets a variable of $titles, in array or string form, and returns a safe localed array.
     *
     * @param string|array $titles
     *
     * @return array
     */
    private function getSafeTitlesArray($titles)
    {
        if (!is_array($titles)) {
            $titles = [
                 getLocale() => $titles,
            ];
        }

        return $titles;
    }



    /**
     * Gets a safe array of the locales of the label
     *
     * @return array
     */
    public function localesArray(): array
    {
        return (array)explode_not_empty(',', $this->locales);
    }



    /**
     * Gets a localed array of titles and returns an string of comma-separated locales.
     *
     * @param array $titles
     *
     * @return string
     */
    public function getLocalesFromTitlesArray(array $titles)
    {
        return implode(',', array_keys($titles));
    }

}
