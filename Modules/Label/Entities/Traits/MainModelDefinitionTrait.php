<?php

namespace Modules\Label\Entities\Traits;

/**
 * Class Label
 */
trait MainModelDefinitionTrait
{
    /**
     * Insert a new record
     *
     * @param string       $model_name
     * @param string       $slug
     * @param string|array $titles
     * @param string|array $parent_id_or_slug
     *
     * @deprecated
     * @return bool
     */
    public function define(string $model_name, string $slug, $titles, $parent_id_or_slug = 0)
    {
        if ($this->isDefined($model_name, $slug)) {
            return false;
        }
        $titles = $this->getSafeTitlesArray($titles);

        return $this->batchSaveBoolean([
             "model_name" => $model_name,
             "slug"       => $slug,
             "titles"     => json_encode($titles),
             "locales"    => $this->getLocalesFromTitlesArray($titles),
             "parent_id"  => $this->getSafeParentId($model_name, $parent_id_or_slug),
        ]);
    }



    /**
     * Check if a certain entry has been defined.
     *
     * @param string $model_name
     * @param string $slug
     *
     * @return bool
     */
    public function isDefined(string $model_name, string $slug)
    {
        return $this->locate($model_name, $slug)->isset();
    }



    /**
     * get the contrary of $this->isDefined()
     *
     * @param string $model_name
     * @param string $slug
     *
     * @return bool
     */
    public function isNotDefined(string $model_name, string $slug)
    {
        return !$this->isDefined($model_name, $slug);
    }

}
