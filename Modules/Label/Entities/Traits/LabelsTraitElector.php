<?php

namespace Modules\Label\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class [Morph]
 * @method Builder elector()
 */
trait LabelsTraitElector
{
    /**
     * Puts elector based on slugs
     *
     * @param string|array $label_slugs
     */
    protected function electorLabel($label_slugs)
    {
        $label_slugs = (array)$label_slugs;

        $this
             ->elector()
             ->whereHas('labels', function ($query) use ($label_slugs) {
                 $query
                      ->where('labels.model_name', $this->getName())
                      ->whereIn('labels.slug', $label_slugs)
                 ;

                 return $query;
             })
        ;
    }



    /**
     * puts elector based on id(s)
     *
     * @param  int|array $label_ids
     */
    protected function electorLabelId($label_ids)
    {
        $label_ids = (array)$label_ids;
        $array = [];

        foreach ($label_ids as $label_id) {
            $array[] = hashid_number($label_id);
        }

        $this->elector()->whereHas("labels", function ($query) use ($array) {
            return $query->whereIn("labels.id", $array);
        })
        ;
    }
}
