<?php

namespace Modules\Label\Entities\Traits;

trait LabelsTraitInfo
{
    /**
     * returns the label model corresponding to the provided $slug. A new instance is returned, if not found.
     *
     * @param string $slug
     *
     * @return \App\Models\Label
     */
    public function getLabel(string $slug)
    {
        $label = $this->definedLabels()->whereSlug($slug)->first();

        if (!$label) {
            $label             = label();
            $label->model_name = $this->getName();
        }

        return $label;
    }



    /**
     * Returns a new instance of a label mode corresponding to the current model name.
     *
     * @return \App\Models\Label
     */
    public function getNewLabel()
    {
        return $this->getLabel('');
    }



    /**
     * Gets safe array of the titles of a specific label
     *
     * @param string $slug
     *
     * @return array
     */
    public function getLabelTitles(string $slug)
    {
        return $this->getLabel($slug)->titlesArray();
    }



    /**
     * Gets the title of a specific label in a specific language
     *
     * @param string $slug
     * @param string $locale
     *
     * @return null|string
     */
    public function getLabelTitleIn(string $slug, string $locale)
    {
        return $this->getLabel($slug)->titleIn($locale);
    }
}
