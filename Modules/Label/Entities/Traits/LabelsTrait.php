<?php

namespace Modules\Label\Entities\Traits;

/**
 * Add this trait to any model to add labels functionality
 *
 * @package Modules\Label\Entities\Traits
 */
trait LabelsTrait
{
    use LabelsTraitDefinitions;
    use LabelsTraitAttachment;
    use LabelsTraitAttached;
    use LabelsTraitInfo;
    use LabelsTraitElector;
    use LabelsTraitUsage;
}
