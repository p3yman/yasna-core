<?php

namespace Modules\Label\Entities\Traits;

trait LabelsTraitAttachment
{
    /**
     * Attaches a single label by her slug.
     *
     * @param string $slug
     *
     * @return bool
     */
    public function attachLabel(string $slug)
    {
        $label = $this->getLabel($slug);
        if ($label->isNotSet()) {
            return false;
        }

        $this->labels()->detach($label->id);
        $this->labels()->attach($label->id, [
             "model_name" => $this->getName(),
        ])
        ;

        return true;
    }



    /**
     * Attaches a number of labels by their slugs.
     *
     * @param array ...$slugs
     *
     * @return int
     */
    public function attachLabels(... $slugs)
    {
        if (is_array($slugs[0])) {
            $slugs = $slugs[0];
        }

        $done = 0;
        foreach ($slugs as $slug) {
            $done += $this->attachLabel($slug);
        }

        return $done;
    }



    /**
     * Detaches a single label by her slug.
     *
     * @param string $slug
     *
     * @return bool
     */
    public function detachLabel(string $slug)
    {
        $label = $this->getLabel($slug);
        if ($label->isNotSet()) {
            return false;
        }

        return boolval($this->labels()->detach($label->id));
    }



    /**
     * Attaches a number of labels by their slugs.
     *
     * @param array ...$slugs
     *
     * @return int
     */
    public function detachLabels(... $slugs)
    {
        if (is_array($slugs[0])) {
            $slugs = $slugs[0];
        }

        $done = 0;
        foreach ($slugs as $slug) {
            $done += $this->detachLabel($slug);
        }

        return $done;
    }



    /**
     * Detaches all labels from the row
     *
     * @return int
     */
    public function detachAllLabels()
    {
        $ids = $this->labels()->get()->pluck('id')->toArray();
        return $this->labels()->detach($ids);
    }
}
