<?php

namespace Modules\Label\Entities\Traits;

use App\Models\Label;

trait MainModelInsertionTrait
{
    /**
     * insert a new record
     *
     * @param string       $model_name
     * @param string       $slug
     * @param string|array $titles
     * @param array        $options
     *
     * @return Label
     */
    public function new(string $model_name, string $slug, $titles, array $options = []): Label
    {
        if ($this->isDefined($model_name, $slug)) {
            return label();
        }

        $options = $this->normalizeInsertOptions($options);
        $data    = $this->getInsertionData($model_name, $slug, $titles);
        $data    = array_merge($data, $options);

        $data['parent_id'] = $this->getSafeParentId($model_name, $data['parent_id']);

        return label()->batchSave($data);
    }



    /**
     * gather arguments into a ready-to-merge array
     *
     * @param string       $model_name
     * @param string       $slug
     * @param string|array $titles
     *
     * @return array
     */
    protected function getInsertionData(string $model_name, string $slug, $titles): array
    {
        $titles = $this->getSafeTitlesArray($titles);

        return [
             "slug"       => $slug,
             "model_name" => $model_name,
             "titles"     => json_encode($titles),
             "locales"    => $this->getLocalesFromTitlesArray($titles),
        ];
    }



    /**
     * get a normalized version of insert options
     *
     * @param array $array
     *
     * @return array
     */
    protected function normalizeInsertOptions(array $array): array
    {
        return array_normalize($array, [
             "parent_id" => "0",
             "color"     => "#ffffff",
             "custom"    => [],
        ]);
    }
}
