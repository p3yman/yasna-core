<?php

namespace Modules\Label\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class LabelServiceProvider
 *
 * @package Modules\Label\Providers
 */
class LabelServiceProvider extends YasnaProvider
{


    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        // Call your methods here!
    }
}
