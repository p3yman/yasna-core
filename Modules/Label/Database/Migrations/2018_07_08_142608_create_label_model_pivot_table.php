<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelModelPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labels_pivot', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('label_id')->index();
            $table->unsignedInteger('model_id')->index();
            $table->string('model_name')->index();

            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labels_pivot');
    }
}
