<?php

namespace Modules\Label\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'          => model('label')->getUniqueUrlPatternSlug(),
                  'title'         => $this->runningModule()->getTrans('seeder.label-unique-url-pattern'),
                  'category'      => 'upstream',
                  'order'         => '20',
                  'data_type'     => 'text',
                  'default_value' => '',
                  'hint'          => module('yasna')->getTrans('seeders.unique-url-pattern-hint'),
                  'css_class'     => 'ltr',
                  'is_localized'  => '1',
             ],
        ];
    }
}
