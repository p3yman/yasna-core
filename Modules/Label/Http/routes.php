<?php

Route::group(['middleware' => 'web', 'prefix' => 'label', 'namespace' => 'Modules\Label\Http\Controllers'], function () {
    Route::get('/', 'LabelController@index');
});
