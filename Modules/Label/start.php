<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("label")) {
    /**
     * @param string|int $id_or_slug
     * @param bool       $with_trashed
     *
     * @return \App\Models\Label
     */
    function label($id_or_slug = 0, bool $with_trashed = false)
    {
        return model('label', $id_or_slug, $with_trashed);
    }
}