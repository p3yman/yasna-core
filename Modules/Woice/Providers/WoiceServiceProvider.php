<?php

namespace Modules\Woice\Providers;

use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Woice\Http\Middleware\ApiKeyMiddleware;
use Modules\Woice\Http\Middleware\DetectLanguageMiddleware;
use Modules\Woice\Http\Middleware\TokenMiddleware;
use Modules\Yasna\Services\YasnaProvider;

class WoiceServiceProvider extends YasnaProvider
{


    /**
     *
     */
    public function index()
    {
        $this->addProviders();
        $this->addAliases();
        $this->addMiddlewares();
        $this->registerModelTraits();
        $this->defineConfigs();
        $this->registerEventListeners();
    }



    /**
     *
     */
    protected function addProviders()
    {
        $this->addProvider(TokenServiceProvider::class);
    }



    /**
     *
     */
    protected function addAliases()
    {
        $this->addAlias("Woice", self::class);
    }



    protected function addMiddlewares()
    {
        $this->addMiddleware("apiToken", TokenMiddleware::class);
        $this->addMiddleware("apiKey", ApiKeyMiddleware::class);
        $this->addMiddleware("apiLang", DetectLanguageMiddleware::class);
    }



    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait("Woice:UserWoiceTrait")->to("User")
        ;

        module("yasna")
             ->service("traits")
             ->add()->trait("Woice:PostWoiceTrait")->to("Post")
        ;
    }



    protected function defineConfigs()
    {
        //foreach($this->cardHashidConnections as $key => $config) {
        //    config(['hashids.connections.' . $key => $config]);
        //}
    }



    protected function registerEventListeners()
    {
        //$this->listen(CardRegistered::class, SendVerificationsOnCardRegistered::class);
        //$this->listen(VolunteerRegistered::class, SendVerificationsOnVolunteerRegistered::class);
        //$this->listen(
        //     PasswordChangedByUser::class,
        //     TurnOffPasswordForceChangeOnPasswordChangedByUser::class
        //);
    }



    public static function isIndex()
    {
        return strpos(request()->route()->getActionName(), 'EhdaFrontController@index') !== false;
    }



    public static function isNotIndex()
    {
        return !self::isIndex();
    }



    /**
     * Array to be used in a combo to select from file types
     *
     * @return array
     */
    public static function getFileTypesCombo()
    {
        $result = [];
        foreach (FileManagerToolsServiceProvider::getAcceptableFileTypes() as $type) {
            $result[] = ['id' => $type, 'title' => trans("filemanager::base.file-types.$type.title")];
        }

        return $result;
    }
}
