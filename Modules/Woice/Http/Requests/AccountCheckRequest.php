<?php

namespace Modules\Woice\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AccountCheckRequest extends YasnaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'fieldType'          => 'ed',
             'value'          => 'ed',
        ];
    }
}
