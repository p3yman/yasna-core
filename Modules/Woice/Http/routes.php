<?php

Route::group(['namespace' => 'Modules\Woice\Http\Controllers'], function () {
    Route::any('/', function () {
        abort(404);
    });

    /*
     * API
     */
    Route::group(['prefix' => 'api', 'namespace' => 'Api', 'middleware' => 'apiLang'], function () {

        /*
         * Api Routes
         */
        Route::group(['prefix' => 'v1', 'middleware' => ['apiKey']], function () {


            /*
             * general methods
             */
            Route::group(['prefix' => 'general'], function () {
                Route::get('/timestamp', 'GeneralController@timestamp');
            });


            /*
             * account check available
             */
            Route::get('account/check', 'AccountController@check');

            /*
             * account login
             */
            Route::post('account/login', 'AccountController@login');

            /*
             * account register
             */
            Route::post('account/register', 'AccountController@register');

            Route::group(['middleware' => 'apiToken'], function () {
                Route::group(['prefix' => 'account'], function () {
                    Route::get('/', 'AccountController@info');
                    Route::delete('/', 'AccountController@delete');
                    Route::patch('/', 'AccountController@update');
                });

                Route::group(['prefix' => 'users'], function () {
                    Route::get('/', 'UsersController@list');

                    Route::group(['prefix' => 'posts'], function () {
                        Route::get('/', 'UsersController@posts');
                        Route::get('{hashid}', 'UsersController@postInfo');
                        Route::delete('{hashid}', 'UsersController@postDelete');
                        Route::post('new', 'UsersController@newPost');
                    });
                    Route::get('{hashid}/posts', 'UsersController@posts');

                    Route::get('{user}/followers', 'UsersController@followers');
                    Route::get('followers', 'UsersController@followers');
                    Route::get('{user}/followings', 'UsersController@followings');
                    Route::get('followings', 'UsersController@followings');

                    Route::post('follow/{hashid}', 'UsersController@follow');
                    Route::post('unfollow/{hashid}', 'UsersController@unfollow');

                    Route::get('{hashid}', 'UsersController@info');
                });
            });
        });
    });
});
