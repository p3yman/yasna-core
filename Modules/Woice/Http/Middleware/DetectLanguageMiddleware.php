<?php

namespace Modules\Woice\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DetectLanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = $request->header('X-API-LOCALE');
        $availableLocales = $this->availableLocales();

        if ($lang) {
            if (in_array($lang, $availableLocales)) { // Specified `lang` is one of site available locales
                app()->setLocale($lang);
            } else { // Specified `lang` is't one of site available locales
                if (preg_match('/^_(.)*$/', $lang)) {
                    // If $lang starts with underscore do nothing (Ex:_debugger)
                    return $next($request);
                } else {
                    app()->setLocale('fa');
                }
            }
        } else {
            app()->setLocale('fa');
        }

        return $next($request);
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function availableLocales()
    {
        $setting = setting()->ask('site_locales')->gain();
        if ($setting and is_array($setting) and count($setting)) {
            return $setting;
        }

        return ['fa'];
    }
}
