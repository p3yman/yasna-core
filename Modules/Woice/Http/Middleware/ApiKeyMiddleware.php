<?php

namespace Modules\Woice\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Woice\Services\Api\Response\ApiResponse;

class ApiKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $api_key = get_setting('api-key');
        $request_api_key = $request->header('X-API-TOKEN');
        if (!$api_key) {
            // internal api key not found
            return ApiResponse::create(10000)->response();
        } else {
            if ($api_key == $request_api_key) {
                return $next($request);
            } else {
                // api not access to send request
                return ApiResponse::create(10001)->response();
            }
        }
    }
}
