<?php

namespace Modules\Woice\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Woice\Providers\TokenServiceProvider;
use Modules\Woice\Services\Api\Response\ApiResponse;
use Tymon\JWTAuth\Facades\JWTAuth;

class TokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                // user not found
                return ApiResponse::create(10003)->response();
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            // token expired
            return ApiResponse::create(10004)->response();
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            // invalid token
            return ApiResponse::create(10005)->response();
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            // token not send
            return ApiResponse::create(10006)->response();
        }

        // the token is valid and we have found the user via the sub claim
        return $next($request);
    }
}
