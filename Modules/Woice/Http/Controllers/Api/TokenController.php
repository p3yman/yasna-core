<?php

namespace Modules\Woice\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Yasna\Services\YasnaController;
use Tymon\JWTAuth\Facades\JWTAuth;

class TokenController extends YasnaController
{
    public function index(Request $request)
    {
        $user = model('user', 1);

        $token['token'] = JWTAuth::fromUser($user);
        $token['status'] = 1;
        $token['request'] = json_encode($request->header('X-Api-Key'));
        //$token['salam'] = JWTAuth::parseToken()->authenticate();
        //$token['token'] = JWTAuth::setToken('foo.bar.baz');

        return response()->json($token, 200);
    }
}
