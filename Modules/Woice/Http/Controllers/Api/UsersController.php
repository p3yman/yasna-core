<?php

namespace Modules\Woice\Http\Controllers\Api;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Woice\Services\Api\Response\ApiResponse;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return ApiResponse::create(40000)
                          ->append(
                               'users',
                               model('user')
                                    ->all()
                                    ->map(function ($user) {
                                        return $user->info_array;
                                    })
                          )->response()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function info(Request $request)
    {
        if (($user = model('user')->grabHashid($request->hashid))->exists) {
            return ApiResponse::create(40002)
                              ->append('user', $user->full_info_array)
                              ->response()
                 ;
        } else {
            return ApiResponse::create(40001)
                              ->response()
                 ;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function posts(Request $request)
    {
        $builder = model('post')
             ->elector(['criteria' => 'published'])
             ->orderByDesc('published_at')
        ;

        if ($userHashid = $request->hashid) {
            if (($user = model('user')->grabHashid($request->hashid))->exists) {
                $builder->wherePublishedBy($user->id);
            } else {
                return ApiResponse::create(40006)
                                  ->response()
                     ;
            }
        } else {
            $builder->whereIn('published_by', user()->followingIds());
        }

        if (($posts = $builder->get())->count()) {
            return ApiResponse::create($userHashid ? 40007 : 40004)
                              ->append(
                                   'posts',
                                   $posts->pluck('information_array')
                              )
                              ->response()
                 ;
        } else {
            return ApiResponse::create($userHashid ? 40005 : 40003)
                              ->response()
                 ;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postInfo(Request $request)
    {
        if (
             ($id = hashid($request->hashid)) and
             (
             $post = model('post')
                  ->elector([
                       'criteria' => 'published',
                       'id'       => $id,
                  ])
                  ->first()
             )) {
            return ApiResponse::create(40009)
                              ->append('post', $post->information_array)
                              ->response()
                 ;
        } else {
            return ApiResponse::create(40008)
                              ->response()
                 ;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDelete(Request $request)
    {
        if (
             ($id = hashid($request->hashid)) and
             (
             $post = model('post')
                  ->elector([
                       'criteria' => 'published',
                       'id'       => $id,
                  ])
                  ->first()
             )
        ) {
            if ($post->delete()) {
                return ApiResponse::create(40011)
                                  ->append('post', $post->information_array)
                                  ->response()
                     ;
            } else {
                return ApiResponse::create(40010)
                                  ->response()
                     ;
            }
        } else {
            return ApiResponse::create(40008)
                              ->response()
                 ;
        }
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function followers(Request $request)
    {
        if (!($user = $this->findUserToUse($request))->exists) {
            return ApiResponse::create(40001)
                              ->response()
                 ;
        }
        return ApiResponse::create(40012)
                          ->append('followers', $user->followers->pluck('information_array'))
                          ->response()
             ;
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function followings(Request $request)
    {
        if (!($user = $this->findUserToUse($request))->exists) {
            return ApiResponse::create(40001)
                              ->response()
                 ;
        }
        return ApiResponse::create(40013)
                          ->append('followings', $user->followings->pluck('information_array'))
                          ->response()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Modules\Yasna\Entities\User
     */
    protected function findUserToUse(Request $request)
    {
        if ($hashid = $request->user) {
            return user()->grabHashid($hashid);
        } else {
            return user();
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function follow(Request $request)
    {
        if (($user = model('user')->grabHashid($request->hashid))->exists) {
            if (($client = user())->isFollowing($user->id)) {
                return ApiResponse::create(40015)
                                  ->append(
                                       'followedAt',
                                       apiDate(userFollowingPivot()
                                            ->whereFollowerId($client->id)
                                            ->whereFollowingId($user->id)
                                            ->first()
                                            ->updated_at)
                                  )
                                  ->response()
                     ;
            } elseif (($client = user())->follow($user->id)) {
                return ApiResponse::create(40016)
                                  ->append(
                                       'followedAt',
                                       apiDate(userFollowingPivot()
                                            ->whereFollowerId($client->id)
                                            ->whereFollowingId($user->id)
                                            ->first()
                                            ->updated_at)
                                  )
                                  ->response()
                     ;
            } else {
                return ApiResponse::create(40014)
                                  ->response()
                     ;
            }
        } else {
            return ApiResponse::create(40001)
                              ->response()
                 ;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function unfollow(Request $request)
    {
        if (($user = model('user')->grabHashid($request->hashid))->exists) {
            if (($client = user())->isNotFollowing($user->id)) {
                return ApiResponse::create(40018)
                                  ->response()
                     ;
            } elseif (($client = user())->unfollow($user->id)) {
                return ApiResponse::create(40019)
                                  ->append(
                                       'unfollowdAt',
                                       apiDate(userFollowingPivot()
                                            ->whereFollowerId($client->id)
                                            ->whereFollowingId($user->id)
                                            ->withTrashed()
                                            ->first()
                                            ->deleted_at)
                                  )
                                  ->response()
                     ;
            } else {
                return ApiResponse::create(40017)
                                  ->response()
                     ;
            }
        } else {
            return ApiResponse::create(40001)
                              ->response()
                 ;
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function newPost(Request $request)
    {
        if ($validationErrors = $this->newPostValidationErrors($request)) {
            return ApiResponse::create(40020)
                              ->append('errors', $validationErrors)
                              ->response()
                 ;
        }

        $updatingData = ['title' => $request->title];
        try {
            $voice = $request->file('voice');
            $voice->move(
                 $voiceDirectory = $this->voiceDirectory(),
                 $voiceName = $this->generateVoiceName() . '.' . $voice->getClientOriginalExtension()
            );
        } catch (Exception $exception) {
            return ApiResponse::create(40021)
                              ->response()
                 ;
        }

        $updatingData['file'] = $voiceDirectory . DIRECTORY_SEPARATOR . $voiceName;

        $post = post()->batchSave($updatingData);

        if (
             $post->exists and $post->update([
                  'published_at' => ($now = Carbon::now()),
                  'published_by' => ($client = user())->id,
                  'moderated_by' => $client->id,
                  'moderated_at' => $now,
             ])
        ) {
            return ApiResponse::create(40023)
                              ->append('post', $post->information_array)
                              ->response()
                 ;
        } else {
            return ApiResponse::create(40022)
                              ->response()
                 ;
        }
    }



    /**
     * @return array
     */
    protected function newPostValidationRules()
    {
        return [
             'title' => 'required',
             'voice' => 'required|file|mimes:mpga,mp3,mp4,aac,wav',
        ];
    }



    /**
     * @return array
     */
    protected function newPostValidationAttributes()
    {
        return [
             'title' => trans('woice::validate.title'),
             'voice' => trans('woice::validate.voice'),
        ];
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function newPostValidationErrors($request)
    {
        if (
        (
        $validator = Validator::make($request->all(), $this->newPostValidationRules())
                              ->setAttributeNames($this->newPostValidationAttributes())
        )->fails()) {
            return $validator->errors();
        }
        return false;
    }



    /**
     * @return string
     */
    protected function voiceDirectory()
    {
        return implode(DIRECTORY_SEPARATOR, [
             'uploads',
             'voices',
        ]);
    }



    /**
     * @return string
     */
    protected function generateVoiceName()
    {
        return implode('-', [time(), str_random(30)]);
    }
}
