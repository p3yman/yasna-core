<?php

namespace Modules\Woice\Http\Controllers\Api;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Woice\Http\Requests\AccountCheckRequest;
use Modules\Woice\Http\Requests\AccountRegisterRequest;
use Modules\Woice\Http\Requests\LoginRequest;
use Modules\Woice\Services\Api\Response\ApiResponse;
use Modules\Woice\Services\Api\Validation\ApiValidation;
use Nwidart\Modules\Facades\Module;
use Tymon\JWTAuth\Facades\JWTAuth;

class AccountController extends Controller
{
    /**
     * Register Fields and rules
     *
     * @var array
     */
    protected $register_fields = [
         'username'  => 'required|alpha_num',
         'email'     => 'required|email',
         'password'  => 'required|min:8',
         'firstName' => 'required|persian:70',
         'lastName'  => 'required|persian:70',
    ];

    /**
     * Users role
     *
     * @var string
     */
    protected $user_role = 'member';



    /**
     * @param AccountCheckRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(AccountCheckRequest $request)
    {
        if ($request->fieldType == 'email') {
            return $this->checkEmail($request->value);
        } elseif ($request->fieldType == 'username') {
            return $this->checkUsername($request->value);
        } else {
            // value not accepted
            return ApiResponse::create(20007)->response();
        }
    }



    /**
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $login_field = $this->loginField($request);
        if (!$login_field) {
            return ApiResponse::create(30000)->response();
        }

        $user = user()
             ->grab($request->username, $login_field);
        if ($user and $user->exists) {
            // user found
            $password_check = false;
            if (isset($request->password)) {
                $password_check = Hash::check($request->password, $user->password);
            }

            if ($password_check) {
                // user login and return token
                Auth::login($user);
                $token = JWTAuth::fromUser($user);
                return ApiResponse::create(30003)
                                  ->append('token', $token)
                                  ->response()
                     ;
            } else {
                // password not match
                return ApiResponse::create(30002)->response();
            }
        } else {
            // user not found
            return ApiResponse::create(30001)->response();
        }
    }



    /**
     * @param $request
     *
     * @return null|string
     */
    public function loginField($request)
    {
        $login_field = null;

        if ($this->isEmail($request->username)) {
            $login_field = 'email';
        }

        if ($this->isUsername($request->username)) {
            $login_field = 'username';
        }

        return $login_field;
    }



    /**
     * @param AccountRegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(AccountRegisterRequest $request)
    {
        if ($validate = $this->registerValidation($request)) {
            return ApiResponse::create(30004)
                              ->append('errors', $validate)
                              ->response()
                 ;
        }

        if ($this->checkEmailExist($request->email)) { // this email exist
            return ApiResponse::create(20001)->response();
        }

        if ($this->checkUsernameExist($request->username)) { // this username exist
            return ApiResponse::create(20004)->response();
        }

        $register = [
             'email'             => $request->email,
             'username'          => $request->username,
             'name_first'        => $request->firstName,
             'name_last'         => $request->lastName,
             'password'          => Hash::make($request->password),
             'avatar'            => $this->defaultAvatar(),
             'avatar_changed_at' => Carbon::now()->toDateTimeString(),
        ];

        $user = model('user')->batchSave($register);
        if ($user->exists) {
            // user register success and return token

            // attach role
            $user->attachRole($this->user_role);

            // login user
            Auth::login($user);

            // token generate
            $token = JWTAuth::fromUser($user);
            return ApiResponse::create(30003)
                              ->append('token', $token)
                              ->response()
                 ;
        } else {
            return ApiResponse::create(30005)->response();
        }
    }



    /**
     * @param $request
     *
     * @return array|bool
     */
    public function registerValidation($request)
    {
        $validate = ApiValidation::make()
                                 ->validate($request, $this->register_fields)
                                 ->getErrors()
        ;

        return $validate;
    }



    /**
     * @param $email
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function checkEmail($email)
    {
        if ($this->isEmail($email)) {
            // this value is email
            if ($this->checkEmailExist($email)) {
                // this email exist
                return ApiResponse::create(20001)->response();
            } else {
                // this email not exist
                return ApiResponse::create(20002)->response();
            }
        } else {
            // this is not valid email
            return ApiResponse::create(20003)->response();
        }
    }



    /**
     * @param $email
     *
     * @return bool
     */
    private function isEmail($email)
    {
        return ApiValidation::validateCustomField(
             ['email' => $email],
             ['email' => 'required|email']
        );
    }



    /**
     * @param $email
     *
     * @return bool
     */
    private function checkEmailExist($email)
    {
        if (model('user')->where('email', $email)->count()) {
            return true;
        }

        return false;
    }



    /**
     * @param $username
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUsername($username)
    {
        if ($this->isUsername($username)) {
            // this value is username
            if ($this->checkUsernameExist($username)) {
                // this username exist
                return ApiResponse::create(20004)->response();
            } else {
                // this username not exist
                return ApiResponse::create(20005)->response();
            }
        } else {
            // this is not valid username
            return ApiResponse::create(20006)->response();
        }
    }



    /**
     * @param $username
     *
     * @return bool
     */
    private function isUsername($username)
    {
        return ApiValidation::validateCustomField(
             ['username' => $username],
             ['username' => 'required|alpha_num']
        );
    }



    /**
     * @param $username
     *
     * @return bool
     */
    private function checkUsernameExist($username)
    {
        if (model('user')->where('username', $username)->count()) {
            return true;
        }

        return false;
    }



    /**
     * @return string
     */
    private function defaultAvatar()
    {
        return (
             ($avatarHashid = get_setting('default_user_avatar')) and
             ($avatarPathname = fileManager()->file($avatarHashid)->getPathName())
        )
             ? $avatarPathname
             : Module::asset('woice:images/default-avatar.png');
    }



    /**
     * @return string
     */
    protected function avatarDirectory()
    {
        return implode(DIRECTORY_SEPARATOR, [
             'uploads',
             'avatars',
        ]);
    }



    /**
     * @return string
     */
    protected function generateAvatarName()
    {
        return implode('-', [time(), str_random(30)]);
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function info()
    {
        return ApiResponse::create(30007)
                          ->append('user', user()->full_information_array)->response()
             ;
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        ($user = user())->delete();
        return ApiResponse::create(30008)
                          ->append('deletedAt', apiDate($user->deleted_at))
                          ->response()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        if ($validationErrors = $this->updateValidationErrors($request)) {
            return ApiResponse::create(30009)
                              ->append('errors', $validationErrors)
                              ->response()
                 ;
        }

        if ($this->updateEmailExistence($request->email)) { // this email exist
            return ApiResponse::create(30010)->response();
        }

        if ($this->updateUsernameExistence($request->username)) { // this username exist
            return ApiResponse::create(30011)->response();
        }

        $updatingData = $request->only(['email', 'username']);

        if ($request->firstName) {
            $updatingData['name_first'] = $request->firstName;
        }

        if ($request->lastName) {
            $updatingData['name_last'] = $request->lastName;
        }

        if ($request->password) {
            $updatingData['password'] = Hash::make($request->password);
        }


        if ($avatar = $request->file('avatar')) {
            $avatar->move(
                 $avatarDirectory = $this->avatarDirectory(),
                 $avatarName = $this->generateAvatarName() . '.' . $avatar->getClientOriginalExtension()
            );
            $updatingData['avatar']            = $avatarDirectory . DIRECTORY_SEPARATOR . $avatarName;
            $updatingData['avatar_changed_at'] = Carbon::now()->toDateTimeString();
        }

        $user = user();
        $user->batchSave($updatingData);
        if ($user->exists) {
            // user register success and return token

            // attach role
            $user->attachRole($this->user_role);

            // login user
            Auth::login($user);
            return ApiResponse::create(30013)
                              ->append('user', $user->information_array)
                              ->response()
                 ;
        } else {
            return ApiResponse::create(30012)->response();
        }
    }



    /**
     * @param $request
     *
     * @return array|bool
     */
    public function updateValidationErrors($request)
    {
        if (($validator = Validator::make($request->all(), $this->updateValidationRules()))->fails()) {
            return $validator->errors();
        }
        return false;
    }



    /**
     * @return array
     */
    protected function updateValidationRules()
    {
        return array_merge($this->updateValidationRulesFromRegister(), $this->updateSpecificValidationRules());
    }



    /**
     * @return array
     */
    protected function updateValidationRulesFromRegister()
    {
        return array_map(function ($fieldRules) {
            return implode('|', array_values(array_filter(explode_not_empty('|', $fieldRules), function ($rule) {
                return ($rule != 'required');
            })));
        }, $this->register_fields);
    }



    /**
     * @return array
     */
    protected function updateSpecificValidationRules()
    {
        return [
             'avatar' => [
                  'file',
                  'image',
             ],
        ];
    }



    /**
     * @param string|null $email
     *
     * @return bool
     */
    protected function updateEmailExistence($email)
    {
        return boolval(
             model('user')
                  ->where('email', $email)
                  ->where('id', '<>', user()->id)
                  ->limit(1)
                  ->count()
        );
    }



    /**
     * @param string|null $username
     *
     * @return bool
     */
    protected function updateUsernameExistence($username)
    {
        return boolval(
             model('user')
                  ->where('username', $username)
                  ->where('id', '<>', user()->id)
                  ->limit(1)
                  ->count()
        );
    }
}
