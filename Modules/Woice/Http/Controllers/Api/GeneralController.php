<?php

namespace Modules\Woice\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Woice\Services\Api\Response\ApiResponse;

class GeneralController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function timestamp()
    {
        return ApiResponse::create(10002)
                          ->append('timestamp', apiDate(Carbon::now()))
                          ->response()
             ;
    }
}
