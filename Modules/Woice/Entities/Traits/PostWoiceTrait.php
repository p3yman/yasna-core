<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/13/18
 * Time: 1:47 PM
 */

namespace Modules\Woice\Entities\Traits;

trait PostWoiceTrait
{

    /**
     * @return array
     */
    public function getInformationArrayAttribute()
    {
        return [
             "id"        => $this->hashid,
             "title"     => $this->title,
             "user"      => ($creator = $this->creator) ? $creator->information_array : null,
             "voice"     => url($this->spreadMeta()->file),
             "updatedAt" => $this->updated_at->toW3cString(),
             "createdAt" => $this->created_at->toW3cString(),
        ];
    }



    /**
     * @return array
     */
    public function getInfoArrayAttribute()
    {
        return $this->information_array;
    }



    /**
     * @return array
     */
    public function voiceMetaFields()
    {
        return ['file'];
    }
}
