<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/13/18
 * Time: 1:08 PM
 */

namespace Modules\Woice\Entities\Traits;

use App\Models\User;
use Carbon\Carbon;

trait UserWoiceTrait
{
    /**
     * @param User $user
     *
     * @return int
     */
    public function relationWithStatus(User $user)
    {
        $userId = $user->id;
        if ($this->isFriendWith($userId)) {
            return 3;
        } elseif ($this->isFollowedBy($userId)) {
            return 2;
        } elseif ($this->isFollowing($userId)) {
            return 1;
        } else {
            return 0;
        }
    }



    /**
     * @return array
     */
    public function getInformationArrayAttribute()
    {
        $generalInfo = [
             "id"              => $this->hashid,
             "firstName"       => $this->name_first,
             "lastName"        => $this->name_last,
             "userName"        => $this->username,
             "email"           => $this->email,
             "avatar"          => url($this->avatar),
             "avatarUpdatedAt" => Carbon::parse($this->avatar_changed_at)->toW3cString(),
             "updatedAt"       => $this->updated_at->toW3cString(),
             "createdAt"       => $this->created_at->toW3cString(),
        ];

        if ($this->id != user()->id) {
            $generalInfo['relation'] = $this->relationWithStatus(user());
        }

        return $generalInfo;
    }



    /**
     * @return array
     */
    public function getInfoArrayAttribute()
    {
        return $this->information_array;
    }



    /**
     * @return array
     */
    public function getFullInformationArrayAttribute()
    {
        return array_merge($this->information_array, [
             'followersCount'  => $this->followers()->select('id')->count(),
             'followingsCount' => $this->followings()->select('id')->count(),
             'postsCount'      => $this->posts()->select('id')->count(),
        ]);
    }



    /**
     * @return mixed
     */
    public function getFullInfoArrayAttribute()
    {
        return $this->full_information_array;
    }
}
