<?php
return [
     'email'     => 'آدرس ایمیل معتبر نمی باشد.',
     'username'  => 'نام کاربری معتبر نمی‌باشد.',
     'password'  => 'رمز عبور معتبر نمی‌باشد.',
     'firstName' => 'نام معتبر نمی‌باشد.',
     'lastName'  => 'نام خانوادگی معتبر نمی‌باشد.',

     'voice' => 'صدا',
     'title' => 'عنوان',

];
