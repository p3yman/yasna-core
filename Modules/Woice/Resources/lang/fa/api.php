<?php
return [
    // general
    10000 => [
         "headerStatus"     => 500,
         "status"           => 500,
         "errorCode"        => 10000,
         "errorMessage"     => 'امکان پذیرش درخواست وجود ندارد.',
         "developerMessage" => 'امکان اعتبار سنجی توکن رابط در حال حاضر وجود ندارد.',
    ],
    10001 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 10001,
         "errorMessage"     => 'امکان پذیرش درخواست وجود ندارد.',
         "developerMessage" => 'توکن رابط برنامه نویس معتبر نمی‌باشد.',
    ],
    10002 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'زمان درخواستی با موفقیت اضافه شد.',
    ],


    10003 => [
         "headerStatus"     => 400,
         "status"           => 401,
         "errorCode"        => 10003,
         "errorMessage"     => 'توکن کاربر معتبر نمی‌باشد.',
         "developerMessage" => 'کاربری برای این توکن یافت نشد.',
    ],


    10004 => [
         "headerStatus"     => 400,
         "status"           => 401,
         "errorCode"        => 10004,
         "errorMessage"     => 'توکن کاربر منقضی شده است.',
         "developerMessage" => 'توکن ارسال شده منقضی شده و فاقد اعتبار است.',
    ],


    10005 => [
         "headerStatus"     => 400,
         "status"           => 401,
         "errorCode"        => 10005,
         "errorMessage"     => 'توکن کاربر فاقد اعتبار است.',
         "developerMessage" => 'توکن ارسال شده فاقد اعتبار است.',
    ],


    10006 => [
         "headerStatus"     => 400,
         "status"           => 401,
         "errorCode"        => 10006,
         "errorMessage"     => 'توکن کاربر ارسال نشده است.',
         "developerMessage" => 'توکن کاربر به همراه درخواست ارسال نشده است.',
    ],


    // account check
    20001 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 20001,
         "errorMessage"     => 'این ایمیل قابل استفاده نمی‌باشد.',
         "developerMessage" => 'این ایمیل توسط کاربر دیگری انتخاب شده است.',
    ],
    20002 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "errorCode"        => 20002,
         "developerMessage" => 'ایمیل قابل پذیرش است.',
    ],
    20003 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 20003,
         "errorMessage"     => 'ایمیل وارد شده معتبر نمی‌باشد.',
         "developerMessage" => 'ایمیل وارد شده معتبر نمی‌باشد.',
    ],
    20004 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 20004,
         "errorMessage"     => 'این نام کاربری قابل استفاده نمی‌باشد.',
         "developerMessage" => 'این نام کاربری توسط شخص دیگری انتخاب شده است.',
    ],
    20005 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "errorCode"        => 20005,
         "developerMessage" => 'نام کاربری قابل استفاده است.',
    ],
    20006 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 20006,
         "errorMessage"     => 'نام کاربری معتبر نمی‌باشد.',
         "developerMessage" => 'نام کاربری فقط می‌تواند شامل حروف و اعداد باشد.',
    ],
    20007 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 20007,
         "errorMessage"     => 'مقدار ارسال شده جهت ارزیابی معتبر نمی‌باشد.',
         "developerMessage" => 'فقط ایمیل و نام‌کاربری قابل ارزیابی است.',
    ],


    /*
     * user actions
     */
    30000 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30000,
         "errorMessage"     => 'نام کاربری یا ایمیل معتبر نمی‌باشد.',
         "developerMessage" => 'نام کاربری یا ایمیل معتبر نمی‌باشد.',
    ],
    30001 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30001,
         "errorMessage"     => 'کاربر یافت نشد.',
         "developerMessage" => 'کاربری با این نام کاربری یا ایمیل یافت نشد.',
    ],
    30002 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30002,
         "errorMessage"     => 'نام کاربری یا رمز عبور صحیح نمی‌باشد.',
         "developerMessage" => 'نام کاربری یا رمز عبور صحیح نمی‌باشد.',
    ],
    30003 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "errorCode"        => 30003,
         "developerMessage" => 'ورود با موفقیت انجام شد، توکن ضمیمه گردید.',
    ],
    30004 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30004,
         "errorMessage"     => 'اعتبار سنجی ثبت نام ناموفق بود.',
         "developerMessage" => 'اعتبار سنجی فیلدهای ثبت نام ناموفق بود.',
    ],
    30005 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30005,
         "errorMessage"     => 'ثبت نام با موفقیت انجام نشد.',
         "developerMessage" => 'خطایی در زمان ثبت‌نام رخ داده است.',
    ],
    30006 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "errorCode"        => 30006,
         "developerMessage" => 'اطلاعات کاربر با موفقیت ارسال شد.',
    ],

    30007 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'اطلاعات کاربر با موفقیت ارسال شد.',
    ],

    30008 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'کاربر با موفقیت حذف شد.',
    ],

    30009 => [
         "headerStatus"     => 400,
         "status"           => 422,
         "errorCode"        => 30009,
         "errorMessage"     => 'اعتبارسنجی ویرایش ناموفق بود.',
         "developerMessage" => 'اعتبارسنجی فیلدهای ویرایش ناموفق بود.',
    ],
    30010 => [
         "headerStatus"     => 400,
         "status"           => 422,
         "errorCode"        => 30010,
         "errorMessage"     => 'این ایمیل قابل استفاده نمی‌باشد.',
         "developerMessage" => 'این ایمیل توسط کاربر دیگری انتخاب شده‌است.',
    ],
    30011 => [
         "headerStatus"     => 400,
         "status"           => 422,
         "errorCode"        => 30011,
         "errorMessage"     => 'این نام کاربری قابل استفاده نمی‌باشد.',
         "developerMessage" => 'این نام کاربری توسط شخص دیگری انتخاب شده است.',
    ],
    30012 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 30012,
         "errorMessage"     => 'ویرایش اطلاعات با موفقیت انجام نشد.',
         "developerMessage" => 'خطایی در زمان ویرایش اطلاعات رخ داده است.',
    ],
    30013 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'ویرایش اطلاعات کاربر با موفقیت انجام شد.',
    ],


    /**
     * Users Part
     */
    40000 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'لیست کاربران با موفقیت ارسال شد.',
    ],

    40001 => [
         "headerStatus"     => 400,
         "status"           => 410,
         "errorCode"        => 40001,
         "errorMessage"     => 'کاربر پیدا نشد.',
         "developerMessage" => 'چنین کاربری وجود ندارد.',
    ],
    40002 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'اطلاعات کاربر با موفقیت ارسال شد.',
    ],

    40003 => [
         "headerStatus"     => 400,
         "status"           => 404,
         "errorCode"        => 40003,
         "errorMessage"     => 'پستی برای نمایش وجود ندارد.',
         "developerMessage" => 'پستی وجود ندارد.',
    ],
    40004 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'پست‌ها موفقیت ارسال شدند.',
    ],

    40005 => [
         "headerStatus"     => 400,
         "status"           => 404,
         "errorCode"        => 40003,
         "errorMessage"     => 'این کاربر پستی ندارد.',
         "developerMessage" => 'این کاربر پستی ندارد.',
    ],
    40006 => [
         "headerStatus"     => 400,
         "status"           => 422,
         "errorCode"        => 40006,
         "errorMessage"     => 'کاربر وجود ندارد.',
         "developerMessage" => 'چنین کاربری وجود ندارد.',
    ],
    40007 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'پست‌های کاربر با موفقیت ارسال شدند.',
    ],

    40008 => [
         "headerStatus"     => 400,
         "status"           => 410,
         "errorCode"        => 40008,
         "errorMessage"     => 'پست پیدا نشد.',
         "developerMessage" => 'چنین پستی وجود ندارد.',
    ],
    40009 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'جزییات پست با موفقیت ارسال شد.',
    ],

    40010 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40010,
         "errorMessage"     => 'پست حذف نشد. دوباره تلاش کنید.',
         "developerMessage" => 'امکان حذف پست وجود ندارد.',
    ],
    40011 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'پست با موفقیت حذف شد.',
    ],

    40012 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'لیست دنبال‌کنندگان با موفقیت ارسال شد.',
    ],

    40013 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'لیست دنبال‌شوندگان با موفقیت ارسال شد.',
    ],

    40014 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40014,
         "errorMessage"     => 'اشکالی در فرایند دنبال کردن به وجود آمده است. دوباره تلاش کنید.',
         "developerMessage" => 'امکان دنبال کردن وجود ندارد.',
    ],
    40015 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'این کاربر قبلا دنبال شده‌است.',
    ],
    40016 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'کاربر دنبال شد.',
    ],

    40017 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40017,
         "errorMessage"     => 'اشکالی در فرایند رها کردن به وجود آمده است. دوباره تلاش کنید.',
         "developerMessage" => 'امکان رها کردن وجود ندارد.',
    ],
    40018 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40018,
         "errorMessage"     => 'شما این کاربر را دنبال نمی‌کنید.',
         "developerMessage" => 'این کاربر دنبال نمی‌شود.',
    ],
    40019 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'کاربر رها شد.',
    ],

    40020 => [
         "headerStatus"     => 400,
         "status"           => 422,
         "errorCode"        => 40020,
         "errorMessage"     => 'اعتبارسنجی پست ناموفق بود.',
         "developerMessage" => 'اعتبارسنجی فیلدهای پست ناموفق بود.',
    ],
    40021 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40021,
         "errorMessage"     => 'آپلود ناموفق بود.',
         "developerMessage" => 'آپلود فایل صوتی پست ناموفق بود.',
    ],
    40022 => [
         "headerStatus"     => 400,
         "status"           => 400,
         "errorCode"        => 40022,
         "errorMessage"     => 'دخیره‌ی پست ناموفق بود.',
         "developerMessage" => 'پست به درستی ذخیره نشد.',
    ],
    40023 => [
         "headerStatus"     => 200,
         "status"           => 200,
         "developerMessage" => 'پست با موفقیت ذخیره شد.',
    ],

    "manage" => [
         "message" => [
              "api-user-edit-password-hint"   => "در صورت وارد کردن جایگزین گذرواژه‌ی قبلی خواهد شد.",
              "portable-printer-configs-hint" => "به صورت json قابل ذخیره است.",
         ],

         "alert" => [
              "error"   => [
                   "password-needed-for-new-client" => "برای تعریف رابط برنامه‌نویسی جدید گذرواژه الزامی است.",
              ],
              "success" => [
                   "portable-printer-configs-save" => "تنظیمات چاپگر سیار با موفقیت ذخیر شدند.",
              ],
         ],

         "headline" => [
              "acceptable-ips"           => "آی‌پی‌های مجاز",
              "acceptable-ips-of-client" => "آی‌پی‌های مجاز :client",
              "delete-ip-of-client"      => "حذف آی‌پی «:ip» رابط برنامه‌نویسی «:client»",
              "ip"                       => "آی‌پی",
              "portable-printer-configs" => "تنظیمات چاپگر سیار",
         ],
    ],


    "password" => [
         "reset" => [
              "notification" => [
                   "sms"   => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
                   "email" => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
              ],
         ],
    ],
];
