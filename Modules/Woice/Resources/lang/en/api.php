<?php
return [
    1000 => [
        "headerStatus" => 500,
        "status" => 500,
        "errorCode" => 1000,
        "errorMessage" => 'Request not allowed',
        "developerMessage" => 'Ability to validate the token interface currently does not exist.',
    ],
    1001 => [
        "headerStatus" => 400,
        "status" => 400,
        "errorCode" => 1001,
        "errorMessage" => 'Request not allowed',
        "developerMessage" => 'Token is not a valid programmer interface.',
    ],
    "manage" => [
        "message" => [
            "api-user-edit-password-hint"   => "در صورت وارد کردن جایگزین گذرواژه‌ی قبلی خواهد شد.",
            "portable-printer-configs-hint" => "به صورت json قابل ذخیره است.",
        ],

        "alert" => [
            "error"   => [
                "password-needed-for-new-client" => "برای تعریف رابط برنامه‌نویسی جدید گذرواژه الزامی است.",
            ],
            "success" => [
                "portable-printer-configs-save" => "تنظیمات چاپگر سیار با موفقیت ذخیر شدند.",
            ],
        ],

        "headline" => [
            "acceptable-ips"           => "آی‌پی‌های مجاز",
            "acceptable-ips-of-client" => "آی‌پی‌های مجاز :client",
            "delete-ip-of-client"      => "حذف آی‌پی «:ip» رابط برنامه‌نویسی «:client»",
            "ip"                       => "آی‌پی",
            "portable-printer-configs" => "تنظیمات چاپگر سیار",
        ],
    ],


    "password" => [
        "reset" => [
            "notification" => [
                "sms"   => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
                "email" => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
            ],
        ],
    ],
];
