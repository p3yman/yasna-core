<?php

namespace Modules\Woice\Services\Api\Response;

use Illuminate\Http\Exceptions\HttpResponseException;

class ApiResponse
{
    /**
     * Status of the Response
     *
     * @var integer
     */
    protected $status;

    /**
     * Other data that will be passed with response
     *
     * @var array
     */
    protected $data = [];

    protected $trans;



    public function __construct($status)
    {
        $this->trans = trans_safe('woice::api.' . $status);
        $this->prepareData();
    }



    public function prepareData()
    {
        $this->status = $this->trans['status'];
        $this->data['data']['status'] = $this->trans['status'];
        if ($this->status != 200) {
            $this->data['data']['errorCode'] = $this->trans['errorCode'];
            $this->data['data']['errorMessage'] = $this->trans['errorMessage'];
            $this->data['data']['developerMessage'] = $this->trans['developerMessage'];
        }

        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function appendData($key, $value)
    {
        $this->data['data'][$key] = $value;

        return $this;
    }



    /**
     * @param array ...$parameters
     *
     * @return ApiResponse
     */
    public function append(...$parameters)
    {
        return $this->appendData(...$parameters);
    }



    /**
     * @param array $data
     *
     * @return $this
     */
    public function merge(array $data)
    {
        $this->data = array_merge($this->data, $data);

        return $this;
    }



    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge([
             'status' => $this->status,
        ], $this->data);
    }



    /**
     * Throw needed exception
     */
    public function throw()
    {
        throw new HttpResponseException(response()->json($this->toArray(), $this->headerStatus()));
    }



    /**
     * @return int
     */
    protected function headerStatus()
    {
        return $this->trans['headerStatus'];
    }



    /**
     * @param array ...$parameters
     *
     * @return static
     */
    public static function create(...$parameters)
    {
        return new static(...$parameters);
    }



    /**
     * @param string $exceptionSlug
     */
    public static function throwException($exceptionSlug)
    {
        static::create($exceptionSlug)->throw();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function response()
    {
        return response()->json($this->toArray(), $this->headerStatus());
    }
}
