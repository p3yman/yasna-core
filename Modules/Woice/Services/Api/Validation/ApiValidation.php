<?php

namespace Modules\Woice\Services\Api\Validation;

use Illuminate\Support\Facades\Validator;

class ApiValidation
{
    /**
     * Status of the Response
     *
     * @var integer
     */
    protected $status;

    /**
     * Validation errors
     *
     * @var array
     */
    protected $errors;

    /**
     * Other data that will be passed with response
     *
     * @var array
     */
    protected $data = [];

    protected $trans;



    public function __construct()
    {
    }



    /**
     * @param $request
     * @param $validator
     *
     * @return bool
     */
    public function validation($request, $validator)
    {
        $validator = Validator::make($request->all(), $validator);
        if ($validator->fails()) {
            // data validation failed
            return false;
        } else {
            // validation success full
            return true;
        }
    }



    /**
     * @param array $data
     * @param array $validator
     *
     * @return bool
     */
    public static function validateCustomField(array $data, array $validator)
    {
        $validator = Validator::make($data, $validator);
        if ($validator->fails()) {
            // data validation failed
            return false;
        } else {
            // validation success full
            return true;
        }
    }



    /**
     * @param $request
     * @param $validation
     *
     * @return $this
     */
    public function validate($request, $validation)
    {
        if (is_array($validation)) {
            foreach ($validation as $param => $rule) {
                $this->errorHandler($param, $this->validation($request, [$param => $rule]));
            }
        }
        return $this;
    }



    /**
     * @param $param
     * @param $validation_result
     */
    public function errorHandler($param, $validation_result)
    {
        if (!$validation_result) {
            $this->errors[$param] = trans_safe('woice::validate.' . $param);
        }
    }



    /**
     * @return static
     */
    public static function make()
    {
        return new static();
    }



    /**
     * @return array|bool
     */
    public function getErrors()
    {
        if (is_array($this->errors) and count($this->errors)) {
            return $this->errors;
        } else {
            return false;
        }
    }
}
