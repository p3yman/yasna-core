<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('username')->index()->after('email')->nullable();
            $table->string('avatar')->index()->after('edu_field')->nullable();
            $table->timestamp('avatar_changed_at')->index()->after('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('username');
            $table->dropColumn('avatar');
            $table->dropColumn('avatar_changed_at');
        });
    }
}
