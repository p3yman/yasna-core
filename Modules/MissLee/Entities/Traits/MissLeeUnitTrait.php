<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 25/11/2017
 * Time: 01:31 PM
 */

namespace Modules\MissLee\Entities\Traits;

trait MissLeeUnitTrait
{
    public function getIsDiscreteAttribute()
    {
        if ($this->concept == 'quantity') {
            return true;
        } else {
            return false;
        }
    }
}
