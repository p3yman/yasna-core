<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 12/11/2017
 * Time: 01:45 PM
 */

namespace Modules\MissLee\Entities\Traits;

use Modules\MissLee\Providers\RouteServiceProvider;

trait MissLeePosttypeTrait
{
    public function getListUrlAttribute()
    {
        return RouteServiceProvider::getPosttypeRoute($this->slug);
    }
}
