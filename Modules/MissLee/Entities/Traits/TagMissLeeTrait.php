<?php

namespace Modules\MissLee\Entities\Traits;

trait TagMissLeeTrait
{
    /**
     * return the direct url of tag.
     *
     * @return string
     */
    public function directUrl(): string
    {
        return route_locale('miss-lee.tag', [
             'tag' => urlencode($this->slug),
        ]);
    }



    /**
     * Accessor for the `directUrl()` method.
     *
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        return $this->directUrl();
    }
}
