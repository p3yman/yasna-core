<?php
/**
 * Created by PhpStorm.
 * User: Yasna-PC1
 * Date: 29/10/2017
 * Time: 03:11 PM
 */

namespace Modules\MissLee\Entities\Traits;

use App\Models\Comment;
use App\Models\Drawing;
use App\Models\Receipt;
use Carbon\Carbon;
use Modules\MissLee\Providers\PostServiceProvider;
use Modules\MissLee\Providers\RouteServiceProvider;
use Modules\MissLee\Services\Module\CurrentModuleHelper;

trait MissLeePostTrait
{
    protected $cachedReceipts;



    /**
     * @return mixed
     */
    public function receipts()
    {
        return Receipt::whereBetween('purchased_at', [$this->event_starts_at, $this->event_ends_at]);
    }



    /**
     * @param null $number
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder|\Illuminate\Support\Collection
     */
    public function similars($number = null)
    {
        $this->spreadMeta();
        //		$posts = Post::where([
        //			['id', '<>', $this->id],        // not self post
        //			'type' => $this->type,          // similar post type
        //		]);
        $selectArray = [
             'type'       => $this->type, // similar post type
             'conditions' => [
                  ['id', '<>', $this->id],        // not self post
             ],
        ];

        // similar categories
        $categories = $this->categories;
        if ($categories->count()) {
            $selectArray['category'] = $categories->pluck('slug')->toArray();
            //			$posts->whereHas('categories', function ($query) use ($categories) {
            //				$query->whereIn('categories.id', $categories->pluck('id')->toArray());
            //			});
        }

        // check availability for "products"
        if ($this->has('price')) {
            $selectArray[] = ['is_available' => true];
        }

        if ($number and is_int($number)) {
            // sort
            //			$posts->orderBy('published_at', 'DESC');
            $selectArray['limit'] = $number;

            return PostServiceProvider::collectPosts($selectArray);
        }

        return PostServiceProvider::selectPosts($selectArray);
    }



    /**
     * @return bool
     */
    public function isDrawingReady()
    {
        return Drawing::isReady($this->id);
    }



    /**
     * Prepare post for drawing.
     */
    public function prepareForDrawing()
    {
        return Drawing::prepareDatabase($this);
    }



    /**
     * @param $switch
     * @param $table
     *
     * @return mixed
     */
    public function selector_folder($switch, $table)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!isset($switch['folder']) or !$switch['folder']) {
            return $table;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $category = $switch['folder'];

        if (!is_array($category)) {
            $category = [$category];
        }

        if (is_array($category) and count($category)) {
            $table = $table->whereHas('categories', function ($query) use ($category) {
                $query->join('categories as folders', 'categories.parent_id', 'folders.id');
                $query->where('categories.is_folder', 0);
                $query->where('folders.is_folder', 0);
                $query->whereIn('folders.id', $category)
                      ->orWhereIn('folders.slug', $category)
                ;
            });
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }



    /**
     * @param $switch
     *
     * @return bool
     */
    public function isIt($switch)
    {
        $switch = strtoupper($switch);
        switch ($switch) {
            case 'NEW':
                if (
                     $this->published_at and
                     ($freshnessSetting = get_setting('products_freshness_time')) and
                     (intval($freshnessSetting) == $freshnessSetting) and
                     Carbon::now()->subMinutes($freshnessSetting)->lessThan(Carbon::parse($this->published_at))
                ) {
                    return true;
                } else {
                    return false;
                }

                break;

            case 'IN_SALE':
                if (!$this->isIt('AVAILABLE')) {
                    break;
                }
                $this->spreadMeta();
                if ($this->sale_price and ($this->sale_price != $this->price)) {
                    return true;
                }
                break;

            case 'AVAILABLE':
                // @todo: make sure to check availability properly
                return (is_null($this->is_available) or $this->is_available);
                break;
        }

        return false;
    }



    /**
     * @return bool
     */
    public function canReceiveComments()
    {
        $this->spreadMeta();
        if ((user()->exists or $this->allow_anonymous_comment) and
             (!$this->disable_receiving_comments)
        ) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return string
     */
    public function directUrl()
    {
        $specific_method = studly_case($this->type) . 'DirectUrl';

        if (method_exists($this, $specific_method)) {
            return $this->$specific_method();
        } else {
            return $this->generalDirectUrl();
        }
    }



    /**
     * @return string
     */
    public function generalDirectUrl()
    {
        $action = module('misslee')->getControllersNamespace() . "\\PostController@single";

        return action_locale($action, [
             'identifier' => RouteServiceProvider::$postDirectUrlPrefix . $this->hashid,
             'title'      => urlHyphen($this->title),
        ]);
    }



    /**
     * get the direct link of the model.
     *
     * @return string
     */
    public function getDirectLinkAttribute()
    {
        return $this->getDirectUrlAttribute();
    }



    /**
     * @return string
     */
    public function blogDirectUrl()
    {
        $this->spreadMeta();
        if ($this->redirect_to_source_link and $this->source_link) {
            return $this->source_link;
        } else {
            return $this->generalDirectUrl();
        }
    }



    /**
     * @return array
     */
    public function getWinnersArrayAttribute()
    {
        $winners = $this->getMeta('winners');
        if (!$winners or !is_array($winners)) {
            $winners = [];
        }

        return $winners;
    }



    /**
     * @return mixed
     */
    public function getReceiptsAttribute()
    {
        if (is_null($this->cachedReceipts)) {
            $this->cachedReceipts = $this->receipts()->get();
        }
        return $this->cachedReceipts;
    }



    public function getTotalReceiptsCountAttribute()
    {
        return $this->receipts->count();
    }



    /**
     * @return mixed
     */
    public function getTotalReceiptsAmountAttribute()
    {
        return $this->receipts->sum('purchased_amount');
    }



    /**
     * @return mixed
     */
    public function getCurrentPriceAttribute()
    {
        if ($this->isIt('IN_SALE')) {
            return $this->sale_price;
        } else {
            return $this->original_price;
        }
    }



    /**
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        return $this->directUrl();
    }



    /**
     * @return null|string
     */
    public function getTextColorAttribute()
    {
        if ($original = $this->getMeta('text_color')) {
            if (!starts_with($original, '#') and ((strlen($original) == 3) or (strlen($original) == 6))) {
                return '#' . $original;
            }

            return $original;
        }

        return null;
    }
}
