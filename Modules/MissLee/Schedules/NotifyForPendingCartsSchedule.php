<?php namespace Modules\MissLee\Schedules;

use App\Models\Cart;
use Carbon\Carbon;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Services\YasnaSchedule;

class NotifyForPendingCartsSchedule extends YasnaSchedule
{
    //Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.

    protected $recordType = 'pending-notification';
    protected $siteUrl;

    protected function job()
    {
        // Find target carts
        $carts = model('cart')
            ->select('carts.*')
            ->where('carts.user_id', '>', 0)
            ->whereNull('raised_at')
            ->where('carts.updated_at', '<', Carbon::now()->subDay())
            ->leftJoin('cart_records', function ($join) {
                $join->on('cart_records.cart_id', "carts.id")
                    ->where('cart_records.type', $this->recordType);
            })
            ->join('orders', 'carts.id', 'orders.cart_id')
            ->whereNull('cart_records.id')
            ->groupBy('carts.id')
            ->get();

        foreach ($carts as $cart) {
            try {
                $this->alert($cart);

                $cart->createRecord($this->recordType);
            } catch (\Exception $e) {
                //
            }
        }
    }

    protected function frequency()
    {
        return 'everyThirtyMinutes';
    }

    protected function additional()
    {
        return 'between:8:00,22:00';
    }

    protected function alert(Cart $cart)
    {
        if (($user = $cart->user) and $user->exists and $user->mobile) {
            $smsText = CurrentModuleHelper::trans('cart.message.pending-cart.sms', [], $cart->locale)
                . "\n\r"
                . $this->getSiteUrl();
            return sendSms($user->mobile, $smsText);
        }
    }

    protected function getSiteUrl()
    {
        if (!$this->siteUrl) {
            $this->siteUrl = get_setting('site_url');
        }

        return $this->siteUrl;
    }
}
