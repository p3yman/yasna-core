<?php namespace Modules\MissLee\Schedules;

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Services\YasnaSchedule;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class RunQueueWorkSchedule extends YasnaSchedule
{
    protected $defaultLimitNumber = 5;
    protected $limitNumber;
    protected $name               = 'RunQueueWorkSchedule';

    protected function job()
    {
        if (DB::table('jobs')->whereAttempts(0)->count() >= $this->limitNumber()) {
            $this->log();
            Artisan::call('queue:work');
        }
    }


    protected function frequency()
    {
        return 'everyMinute';
    }

    protected function limitNumber()
    {
        if (is_null($this->limitNumber)) {
            $this->limitNumber = get_setting('maximum_pending_jobs') ?: $this->defaultLimitNumber;
        }
        return $this->limitNumber;
    }

    protected function log()
    {
        $logger           = new Logger('Queue Work');
        $logStreamHandler = new StreamHandler(storage_path() . '/logs/queue-work.log', Logger::DEBUG);
        // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
        $logFormat = "%datetime% %channel%: %message%\n";
        $formatter = new LineFormatter($logFormat);
        $logStreamHandler->setFormatter($formatter);

        $logger->pushHandler($logStreamHandler);
        $logger->addDebug('The "queue:work" command executed.');
    }

    protected function name()
    {
        return $this->name;
    }
}
