<?php namespace Modules\MissLee\Schedules;

use App\Models\Cart;
use Carbon\Carbon;
use Modules\MissLee\Providers\RouteServiceProvider;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Services\YasnaSchedule;

class NotifyForUnpaidCartsSchedule extends YasnaSchedule
{
    //Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.

    protected $recordType = 'unpaid-notification';
    protected $siteUrl;

    protected function job()
    {
        // Find target carts
        $carts = model('cart')
            ->select('carts.*')
            ->where('user_id', '>', 0)
            ->whereNotNull('raised_at')
            ->whereNull('disbursed_at')
            ->whereNull('paid_at')
            ->where('carts.updated_at', '<', Carbon::now()->subDay())
            ->leftJoin('cart_records', function ($join) {
                $join->on('cart_records.cart_id', "carts.id")
                    ->where('cart_records.type', $this->recordType);
            })
            ->whereNull('cart_records.id')
            ->groupBy('carts.id')
            ->get();

        foreach ($carts as $cart) {
            try {
                $this->alert($cart);

                $cart->createRecord($this->recordType);
            } catch (\Exception $e) {
                //
            }
        }
    }

    protected function frequency()
    {
        return 'everyThirtyMinutes';
    }

    protected function additional()
    {
        return 'between:7:00,22:00';
    }

    protected function alert(Cart $cart)
    {
        if (($user = $cart->user) and $user->exists and $user->mobile) {
            $smsText = CurrentModuleHelper::trans('cart.message.unpaid-cart.sms', [], $cart->locale)
                . "\n\r"
                . str_replace(url('/'), $this->getSiteUrl(), RouteServiceProvider::action(
                    'PurchaseController@paymentShortLink',
                    ['cart' => $cart->hashid]
                ));

            return sendSms($user->mobile, $smsText);
        }
    }

    protected function getSiteUrl()
    {
        if (!$this->siteUrl) {
            $this->siteUrl = get_setting('site_url');
        }

        return $this->siteUrl;
    }
}
