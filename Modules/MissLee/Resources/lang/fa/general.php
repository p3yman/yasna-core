<?php
return [
     "login-or-register"               => "ورود / ثبت نام",
     "home"                            => "خانه",
     "home-page"                       => "صفحه‌ی نخست",
     "about-us"                        => "درباره‌ی ما",
     "selected-posts"                  => "مطالب برگزیده",
     "return-policies"                 => "رویه‌های بازگرداندن کالا",
     "contact-us"                      => "تماس با ما",
     "description"                     => "توضیحات",
     "categories"                      => "دسته‌بندی‌ها",
     "categories-of"                   => "دسته‌های",
     "categories-of-something"         => "دسته‌های :something",
     "categories-of-something-partial" => [
          "part1" => "",
          "part2" => "دسته‌های",
          "part3" => ":something",
     ],
     "continue-reading"                => "ادامه‌ی مطلب",
     "related-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "مرتبط",
     ],
     "similar-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "مشابه",
     ],
     "cancel"                          => "انصراف",
     "our-site"                        => "سایت ما",
     "manager"                         => "مدیریت",
     "row"                             => "ردیف",
     "date"                            => "تاریخ",
     "status"                          => "وضعیت",
     "close"                           => "بستن",
     "or"                              => "یا",
     "source"                          => "منبع",

     "message" => [
          "any-question-about-your-order" => "درباره‌ی سفارش خود سوالی دارید؟",
          "rate-product"                  => ":user عزیز از خرید شما متشکریم. به این محصول چه امتیازی می‌دهید؟",
          "success"                       => [
               "send-message" => "پیام شما با موفقیت ارسال شد.",
          ],
          "error"                         => [
               "no-result"            => "نتیجه‌ای یافت نشد.",
               "location-is-disabled" => "سرویس Location (موقعیت یاب) مرورگر شما، غیرفعال است.",
               "no-post-found"        => "مطبی پیدا نشد.",
               "send-message"         => "پیام ارسال نشد.",
          ],
     ],

     "browser" => [
          "firefox" => "فایرفاکس",
          "chrome"  => "گوگل کروم",
     ],

     "location" => [
          "message" => [
               "is-disabled-in-browser"         => "سرویس Location (موقعیت یاب) مرورگر شما، غیرفعال است.",
               "select-to-increase-precision"   => "  برای دقت بیشتر، آدرس را روی نقشه انتخاب کنید.",
               "thanks-for-specifying-location" => "از اینکه با تعیین آدرس بر روی نقشه، ما را در ارائه هرچه بهتر خدمات یاری نمودید، سپاسگزاریم.",
          ],
          "guide"   => [
               "activation-firefox" => "با فشردن Alt+t+i وارد تنظیمات شوید و تب Permission را انتخاب کنید، سپس Access your location را از حالت بلاک خارج کنید.",
               "activation-chrome"  => "روی دکمه :button یا آیکن قفل سبز، در ابتدای Address Bar کلیک کنید، سپس در تنظیمات، Location را از حالت بلاک خارج کنید.",
          ],
     ],

     "drawing" => [
          "draw"                  => [
               "singular" => "قرعه‌کشی",
               "prepare"  => "آماده‌سازی برای قرعه‌کشی",
          ],
          "receipts-count-amount" => ":count رسید خرید (در کل :amount تومان)",
          "winner"                => "برنده‌ی قرعه‌کشی",
          "no-winner-so-far"      => "فعلاً هیچ کس به عنوان برنده انتخاب نشده است.",
          "take-number-between"   => "عددی را بین :number1 تا :number2 انتخاب کنید",
          "random-number"         => "عدد شانسی",
          "redraw"                => "قرعه‌کشی دوباره",

          "code"    => [
               "singular"          => "کد",
               "registered-plural" => "کدهای ثبت شده",
               "register"          => "ثبت کد قرعه‌کشی",
               "check"             => "بررسی کد",
               "add"               => "افزودن کد",
          ],
          "message" => [
               "invalid-code"                      => "کد وارد شده معتبر نیست.",
               "register-code-success-please-wait" => "کد مورد پذیرش است، چند لحظه تامل بفرمایید.",
               "register-code-success-sms"         => "سلام :name جان،\n\rکد قرعه‌کشی شما با موفقیت ثبت شد.",
          ],
     ],

     "order" => [
          "plural" => "سفارشات",
     ],

     "footer" => [
          "copyright"  => "تمامی حقوق برای :site محفوظ است.",
          "created-by" => "طراحی و اجرا",
          "yasna-team" => "گروه یسنا",
     ],

     "edu-level" => [
          "short" => [
               "not-specified"     => "نامشخص",
               "less-than-diploma" => "زیر دیپلم",
               "diploma"           => "دیپلم",
               "associate-degree"  => "کاردانی",
               "bachelor-degree"   => "کارشناسی",
               "master-degree"     => "ارشد",
               "phd-or-more"       => "دکترا",
          ],
          "full"  => [
               "not-specified"     => "نامشخص",
               "less-than-diploma" => "پایین‌تر از دیپلم متوسطه",
               "diploma"           => "دیپلم متوسطه",
               "associate-degree"  => "کاردانی",
               "bachelor-degree"   => "کارشناسی",
               "master-degree"     => "کارشناسی ارشد",
               "phd-or-more"       => "دکترا و بالاتر",
          ],
     ],

     "gender" => [
          "male"   => "آقا",
          "female" => "خانم",
          "other"  => "سایر",
     ],

     "marital-status" => [
          "single"  => "بدون همسر",
          "married" => "متأهل",
     ],

     "unit" => [
          "currency" => [
               "IRT" => "تومان",
               "IRR" => "ریال",
          ],
     ],

     "contact" => [
          "address"   => "آدرس",
          "telephone" => "تلفن",
          "fax"       => "فکس",
          "email"     => "پست الکترونیک",
     ],

     "media" => [
          "playing" => "در حال پخش",
     ],

     "comment" => [
          "title" => [
               "singular" => "نظر",
               "plural"   => "نظرات",
          ],
          "form"  => [
               "name"      => "نام",
               "lastname"  => "نام خانوادگی",
               "fullname"  => "نام و نام خانوادگی",
               "email"     => "پست الکترونیکی",
               "url"       => "آدرس سایت",
               "statement" => "نظر خود را بنویسید",
          ],

          "answer" => "پاسخ",
          "submit" => "ثبت نظر",
          "more"   => "نظرات بیشتر",
     ],

     "address" => [
          "title"            => [
               "singular" => "نشانی",
               "plural"   => "نشانی‌ها",
          ],
          "add"              => "افزودن نشانی",
          "register"         => "ثبت نشانی",
          "select"           => "انتخاب نشانی",
          "no-address-alert" => "شما نشانی ثبت شده‌ای ندارید. لطفاً یک نشانی ثبت کنید.",
     ],

     "tag" => [
          "title"     => [
               "plural" => "برچسب‌ها",
          ],
          "tag-title" => "برچسب «:title»",
     ],

     "zoom" => [
          "loading" => "در حال بارگزاری...",
          "error"   => "امکان نمایش تصویر وجود ندارد.",
     ],

     "labels" => [
          "color" => [
               "plural"   => "رنگ‌ها",
               "singular" => "رنگ",
          ],
     ],

     "colors" => [
          "transparent" => "بی‌رنگ",
          "white"       => "سفید",
          "gray"        => "طوسی",
          "black"       => "سیاه",
          "cream"       => "کرم",
          "brown"       => "قهوه‌ای",
          "smoky"       => "دودی",
          "pewter"      => "موشی",
          "chocolate"   => "شکلاتی",
          "red"         => "قرمز",
          "dark_red"    => "قرمز تیره",
          "ruby"        => "صورتی تیره",
          "pink"        => "صورتی",
          "magenta"     => "سرخابی",
          "purple"      => "بنفش",
          "dark_blue"   => "سورمه‌ای",
          "blue"        => "آبی",
          "light_blue"  => "آبی روشن",
          "cyan"        => "فیروزه‌ای",
          "teal"        => "کله‌غازی",
          "dark_green"  => "سبز تیره",
          "green"       => "سبز",
          "light_green" => "سبز روشن",
          "yellow"      => "زرد",
          "orange"      => "نارنجی",
          "deep_orange" => "نارنجی تیره",
     ],
];
