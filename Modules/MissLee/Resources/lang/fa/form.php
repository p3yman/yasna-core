<?php
return [
    'button' => [
        'save'   => 'ذخیره',
        'submit' => 'ثبت',
        "back"   => "بازگشت",
        "send"   => "ارسال",
    ],

    'feed' => [
        'error' => 'بروز خطا',
        'wait'  => 'اندکی صبر...',
        'done'  => 'انجام شد',
    ],

    'created-at'   => 'تاریخ ثبت',
    'purchased-at' => 'تاریخ فاکتور',
    'amount'       => 'مبلغ',
];
