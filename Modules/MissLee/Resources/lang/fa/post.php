<?php
return [
    "event" => [
        "plural"           => "رویدادها",
        "singular"         => "رویداد",
        "from"             => "از",
        "to"               => "تا",
        "running-plural"   => "رویدادهای در جریان",
        "expired-plural"   => "رویدادهای پیشین",
        "collected-points" => "امتیاز جمع‌آوری شده",
        "soon"             => "به زودی",
        "no-event-fount"   => "رویدادی برای نمایش وجود ندارد.",
    ],

    "product" => [
        "singular"         => "محصول",
        "plural"           => "محصولات",
        "view-plural"      => "مشاهده‌ی محصولات",
        "is-available"     => "موجود",
        "is-not-available" => "کسر موجودی (توقف فروش)",
        "code"             => "کد محصول",
        "search"           => "جست و جو در محصولات",
        "no-result-found"  => "موردی در بین محصولات یافت نشد.",
    ],

    "news" => [
        "plural" => "اخبار",
    ],

    "faq" => [
        "plural" => "سوالات متداول",
    ],

    "teammate" => [
        "plural" => "تیم ما",
    ],
];
