<?php
return [
    "event" => [
        "plural"           => "Events",
        "singular"         => "Event",
        "from"             => "from",
        "to"               => "to",
        "running-plural"   => "Current Events",
        "expired-plural"   => "Previous Events",
        "collected-points" => "Collected Points",
        "soon"             => "Soon",
        "no-event-fount"   => "There is no event to show.",
    ],

    "product" => [
        "singular"         => "Product",
        "plural"           => "Products",
        "view-plural"      => "View Products",
        "is-available"     => "Enough",
        "is-not-available" => "Lack of Inventory (Sales Stopped)",
        "code"             => "Product Code",
        "search"           => "Search in Products",
        "no-result-found"  => "No item found in products.",
    ],

    "news" => [
        "plural" => "News",
    ],

    "faq" => [
        "plural" => "FAQ",
    ],

    "teammate" => [
        "plural" => "Teammates",
    ],
];
