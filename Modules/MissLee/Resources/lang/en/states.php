<?php
return [
    'province'       => "Province",
    'provinces'      => "Provinces",
    'provinces_of_x' => "Provinces of :x",
    'province_edit'  => "Edit Province",
    'province_new'   => "New Province",

    'city'        => "City",
    'cities'      => "Cities",
    'cities_list' => "Cities List" ,
    'cities_of_x' => "Cities of :x",
    'city_edit'   => "Edit City",
    'city_new'    => "New City",


];
