<?php
return [
     "attributes" => [
          "mobile" => "Mobile",
     ],

     "javascript-validation" => [
          "name"     => "Please insert valid name.",
          "mobile"   => "Please insert valid 11 digits mobile number.",
          "email"    => "Please insert a valid email.",
          "text"     => "Please insert text.",
          "province" => "Please select the province.",
          "city"     => "Please select the city.",
     ],

     "attributes_example" => [
          "telephone"   => "Example: 02632255213",
          "postal_code" => "Example: 1122334455",
     ],
];
