<?php
return [
     "event" => [
          "plural"           => "الفعاليات",
          "singular"         => "الفعالية",
          "from"             => "من",
          "to"               => "إلى",
          "running-plural"   => "الفعاليات الجارية",
          "expired-plural"   => "الفعاليات السابقة",
          "collected-points" => "النقاط المجمعة",
          "soon"             => "قريباً",
          "no-event-fount"   => "لا يوجد فعاليات للعرض.",
     ],

     "product" => [
          "singular"         => "المنتج",
          "plural"           => "المنتوجات",
          "view-plural"      => "مشاهدة المنتوجات",
          "is-available"     => "موجود",
          "is-not-available" => "غير متوفر (إيقاف البيع)",
          "code"             => "رمز المنتج",
          "search"           => "البحث في المنتوجات",
          "no-result-found"  => "لم يتم العثور في المنتجات.",
     ],

     "news" => [
          "plural" => "الأخبار",
     ],

     "faq" => [
          "plural" => "الأسئلة الشائعة",
     ],

     "teammate" => [
          "plural" => "فريقنا",
     ],
];