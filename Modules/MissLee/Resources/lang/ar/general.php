<?php
return [
     "login-or-register"               => "تسجيل الدخول / إنشاء حساب",
     "home"                            => "الصفحة الرئيسة",
     "home-page"                       => "الصفحة الأولي",
     "about-us"                        => "من نحن",
     "selected-posts"                  => "المنشورات المفضلة",
     "return-policies"                 => "إجراءات أرجاع السلعة",
     "contact-us"                      => "اتصل بنا",
     "description"                     => "المواصفات",
     "categories"                      => "الأقسام",
     "categories-of"                   => "القسم",
     "categories-of-something"         => "القسم :something",
     "categories-of-something-partial" => [
          "part1" => "",
          "part2" => "القسم",
          "part3" => ":something",
     ],
     "continue-reading"                => "اقرأ المزيد",
     "related-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "مرتبط",
     ],
     "similar-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "مشابه",
     ],
     "cancel"                          => "إلغاء",
     "our-site"                        => "موقعنا",
     "manager"                         => "الإدارة",
     "row"                             => "صف",
     "date"                            => "تاریخ",
     "status"                          => "الحالة",
     "close"                           => "إغلاق",
     "or"                              => "أو",
     "source"                          => "منبع",

     "message" => [
          "any-question-about-your-order" => "هل لديك سؤال حول طلبك؟",
          "rate-product"                  => ":user العزیز شکرا على شرائك. كيف تقيم هذا المنتج؟",
          "success"                       => [
               "send-message" => "تم إرسال رسالتك بنجاح.",
          ],
          "error"                         => [
               "no-result"            => "لم يتم الحصول على نتيجة.",
               "location-is-disabled" => "خدمة Location (تحديد الموقع) مغلقة في متصفحك.",
               "no-post-found"        => "لم يتم العثور على منشور.",
               "send-message"         => " لم يتم إرسال الرسالة.",
          ],
     ],

     "browser" => [
          "firefox" => "فايرفوكس",
          "chrome"  => "غوغل کروم",
     ],

     "location" => [
          "message" => [
               "location-is-disabled"           => "خدمة Location (تحديد الموقع) مغلقة في متصفحك.",
               "select-to-increase-precision"   => "  لمزيد من الدقة ، حدد العنوان على الخريطة.",
               "thanks-for-specifying-location" => "شكرًا لك على مساعدتنا في تقديم أفضل خدمة ممكنة من خلال تحديد العنوان على الخريطة.",
          ],
          "guide"   => [
               "activation-firefox" => "بالضغط Alt+t+i ادخل في الإعدادات و تب Permission اختار، ثم Access your ثم إزالة الوصول إلى location من وضع الحظر.",
               "activation-chrome"  => "الزر :button أو أيقونة الزرالأخضر، في البداية Address Bar إنقر، ثم في الإعدادات،  من وضع الحظرLocation ثم إزالة الوصول إلى.",
          ],
     ],

     "drawing" => [
          "draw"                  => [
               "singular" => "القرعة",
               "prepare"  => "التحضير للقرعة",
          ],
          "receipts-count-amount" => ":count  الفاتورة (الإجمالي:amount تومان)",
          "winner"                => "الفائز بالقرعة",
          "no-winner-so-far"      => " لم يتم اختيار أي فائز في الوقت الحالي.",
          "take-number-between"   => "عىىا بين :number1 تا :number2 اختر",
          "random-number"         => "رقم عشوائي",
          "redraw"                => "إجراء قرعة من جديد",

          "code"    => [
               "singular"          => "رمز",
               "registered-plural" => "الرموز المسجلة",
               "register"          => "تسجيل رمز القرعة",
               "check"             => " فحص الرمز",
               "add"               => "اضافة الرمز",
          ],
          "message" => [
               "invalid-code"                      => " الرمز الذي تم إدخاله غير صالح.",
               "register-code-success-please-wait" => "تم قبول الرمزلإ انتظر قليلا.",
               "register-code-success-sms"         => "مرحبا :name العزيز،\n\rتم تسجيل رمز القرعة بنجاح .",
          ],
     ],

     "order" => [
          "plural" => "طلبات",
     ],

     "footer" => [
          "copyright"  => "جميع الحقوق :site محفوظة.",
          "created-by" => "التصميم و التنفيذ",
          "yasna-team" => "مجموعة يسنا",
     ],

     "edu-level" => [
          "short" => [
               "not-specified"     => "غير معروف",
               "less-than-diploma" => " أقل من البكارلوريا",
               "diploma"           => "بكالوريا",
               "associate-degree"  => "معهد",
               "bachelor-degree"   => "بكالوريوس",
               "master-degree"     => "ماجستر",
               "phd-or-more"       => "ىكتوراه",
          ],
          "full"  => [
               "not-specified"     => "غيرمعروف",
               "less-than-diploma" => " أقل من البكارلوريا",
               "diploma"           => "بكالوريا",
               "associate-degree"  => "معهد",
               "bachelor-degree"   => "بكالوريوس",
               "master-degree"     => " ماجستر",
               "phd-or-more"       => "دكتوراه",
          ],
     ],

     "gender" => [
          "male"   => "ذكر",
          "female" => "أنثى",
          "other"  => "أخرى",
     ],

     "marital-status" => [
          "single"  => "أعذب",
          "married" => "متزوج",
     ],

     "unit" => [
          "currency" => [
               "IRT" => "تومان",
               "IRR" => "ریال",
          ],
     ],

     "contact" => [
          "address"   => "العنوان",
          "telephone" => "الهاتف",
          "fax"       => "فاكس",
          "email"     => "البريد الإلكتروني",
     ],

     "media" => [
          "playing" => "يعرض الآن",
     ],

     "comment" => [
          "title" => [
               "singular" => "تعليق",
               "plural"   => "تعليقات",
          ],
          "form"  => [
               "name"      => "الاسم",
               "lastname"  => "اسم العائلة",
               "fullname"  => "الاسم و اسم العائلة",
               "email"     => "البريد الإلكتروني",
               "url"       => "عنوان الموقع",
               "statement" => "اكتب تعليقك ",
          ],

          "answer" => "رد",
          "submit" => "إرسال التعليق",
          "more"   => "المزيد من التعليقات ",
     ],

     "address" => [
          "title"    => [
               "singular" => "عنوان",
               "plural"   => "العناوين",
          ],
          "add"      => " إضافة عنوان",
          "register" => "تسجيل العنوان",
          "select"   => "تحديد العنوان ",
     ],

     "tag" => [
          "title" => [
               "plural" => "الوسمات",
          ],
          "tag-title" => "العلامة «:title»",
     ],

     "zoom" => [
          "loading" => "جار التحميل...",
          "error"   => "لا يمكن عرض الصورة.",
     ],

     "labels" => [
          "color" => [
               "plural"   => "الألوان",
               "singular" => "اللون",
          ],
     ],

     "colors" => [
          "transparent" => "بلا لون",
          "white"       => "أبيض",
          "gray"        => "رصاصي",
          "black"       => "أسود",
          "cream"       => "بيج",
          "brown"       => "بني",
          "smoky"       => "رمادي",
          "pewter"      => "رمادي فاتح",
          "chocolate"   => "شوكولاتي",
          "red"         => "أحمر",
          "dark_red"    => "أحمر داكن",
          "ruby"        => "زهري داكن",
          "pink"        => "زهري",
          "magenta"     => "أحمر أرجواني",
          "purple"      => "بنفسجي",
          "dark_blue"   => "كحلي",
          "blue"        => "أزرق",
          "light_blue"  => "أزرق فاتح",
          "cyan"        => "فيروزي",
          "teal"        => "تراكوازي",
          "dark_green"  => "أخضر داكن",
          "green"       => "أخضر",
          "light_green" => "أخضر فاتح",
          "yellow"      => "أصفر",
          "orange"      => "برتقالي",
          "deep_orange" => "برتقالي داكن",
     ],
];
