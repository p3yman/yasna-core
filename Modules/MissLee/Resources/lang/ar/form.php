<?php
return [
     'button' => [
          'save'   => 'حفظ',
          'submit' => 'تسجيل',
          "back"   => "العودة",
          "send"   => "إرسال",
     ],

     'feed' => [
          'error' => 'حدث خطأ',
          'wait'  => ' انتظر قيلاً...',
          'done'  => 'تم',
     ],

     'created-at'   => 'تاریخ ثبت',
     'purchased-at' => 'تاريخ الفاتورة',
     'amount'       => 'المبلغ',
];