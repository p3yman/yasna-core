<?php
return [
     "title" => [
          "singular" => "سلة الشراء",
     ],

     "purchase" => [
          "singular" => "الشراء",
          "plural"   => "المشتريات",
     ],

     "numeration-label" => [
          "number" => "العدد",
          "amount" => "المقدار",
     ],

     "message" => [
          "is-empty"                  => "سلة الشراء فارغة.",
          "order-address-description" => "هذه الطلبية :receiver إلى عنوان :address  و رقم الهاتف :telephone يرجى إرسال.",
          "order-courier-description" => "تسليم إلى :courier - تكلفة الشحن: :price",
          "cart-changed"              => "سلة شرائك قد تغيرت.",
          "select-gateway-hint"       => "إختر طريقة الدفع. بإمكانك الدفع عن طريق جميع البطاقات المصرفية.",
          "thanks-for-purchasement"   => "شكراً لك على شرائك.",
          "pending-cart"              => [
               "sms" => "سلة شرائك لم تكتمل  لتكميل الشراء الرجاء زيارة الموقع.",
          ],
          "unpaid-cart"               => [
               "sms" => "طلبيتك في انتظار الدفع. قم بزيارة الموقع التالي للقيام بالدفع.",
          ],
     ],

     "alert" => [
          "success" => [
               "order-remove" => "تم حذف المنتج بنجاح.",
               "order-add"    => "تم إضافة المنتج لسلة الشراء.",
               "payment"      => " تم تسجيل طلبيتك والدفع بنجاح. شكراً لك.",
          ],
          "error"   => [
               "order-remove"                 => "حدثت مشكلة في حذف المنتج.",
               "order-add"                    => " حدثت مشكلة في إضافة المنتج إلى سلة الشراء. الرجاء المحاولة مرة أخرى.",
               "required-selection"           => "اختيار :attribute ضروري.",
               "payment"                      => "!عذرا، لم تنجح عملية الدفع",
               "unable-to-verify"             => " لا يمكن التحقق من هذه المعاملة.",
               "rejected-by-gateway"          => " لم يتم التحقق من المعاملة من جانب البنك.",
               "system-error-in-purchasement" => " بسبب حدوث خطأ في النظام، لا يمكن إكمال عملية الشراء في هذا الوقت.",
          ],
     ],

     "button" => [
          "back-to-shop"       => " العودة إلى المتجر",
          "back-to-cart"       => "العودة إلى سلة الشراء",
          "add"                => "إضافة إلى سلة الشراء",
          "flush"              => "تفريغ سلة الشراء",
          "settlement"         => "إنهاء عملية الشراء ",
          "confirm-and-review" => "التحقق من الطلبية",
          "confirm-and-pay"    => "التحقق و الدفع",
          "purchase-online"    => " الدفع عبر الانترنت",
     ],

     "price" => [
          "unit"                => "سعر الوحدة",
          "total"               => "السعر الإجمالي",
          "your-purchase-total" => "مجموع شرائك ",
          "purchasable"         => "قابل للدفع",
          "delivery"            => " تكلفة التسليم",
          "free"                => "مجاني",
     ],

     "headline" => [
          "discount"                   => "تخفيض",
          "shipping-info"              => "معلومات عن إرسال الطلبية",
          "review-order"               => "التحقق من الطلبية",
          "address-and-courier"        => "العنوان و نوع الاستلام",
          "account-number"             => "رقم الحساب",
          "card-number"                => "رقم البطاقة",
          "bank"                       => "البنك",
          "account-owner"              => "صاحب الحساب",
          "tracking-code"              => "رقم المتابعة",
          "declaration-date"           => "تاريخ الدفع",
          "delivery-info"              => "معلومات عن إرسال الطلبية",
          "receiver"                   => "المستلم",
          "order-status-summary"       => "ملخص عن حالة الطلبية",
          "receipt-number"             => "رقم الوصل",
          "order-status"               => "حالة الطلبية",
          "your-purchasements-details" => "تفاصيل عمليات الدفع",
          "gateway"                    => " بوابة الدفع",
          "payment-type"               => "نوع الدفع",
          "amount"                     => "المبلغ",
          "order-details"              => "تفاصيل الطلبية",
          "details-of-order"           => "تفاصيل الطلبية :order",
     ],

     "payment" => [
          "title"  => [
               "singular" => "الدفع",
          ],
          "status" => " حالة الدفع",

          "status-levels" => [
               "refunded"        => "مرجع",
               "canceled"        => "ملغى",
               "cancelled"       => "ملغى",
               "rejected"        => "مرفوض",
               "verified"        => "موافق عليه",
               "declared"        => " في انتظار التوثيق ",
               "sent-to-gateway" => "في انتظار الموافقة",
               "created"         => "لم يتم الدفع",
          ],

          "message" => [
               "unsuccess-online-hint"       => "في حال خصم المبلغ من حسابك، سيرجع إليك خلال ۷۲ ساعة القادمة.",
               "select-gateway-and-purchase" => "اختر أحد بوابات الدفع و سدد المبلغ لإكمال عملية الشراء.",
          ],
     ],

     "courier" => [
          "title" => [
               "singular" => "طريقة الإرسال ",
               "plural"   => " طرق الإرسال",
          ],

          "select" => "اختيار طريقة الإرسال",
     ],

     "package" => [
          "title" => [
               "singular" => "القسم",
               "plural"   => "الأقسام",
          ],
     ],

     "discount" => [
          "i-have-code"   => "لديّ رمز الخصم.",
          "register-code" => "تسجيل رمز الخصم",
          "message"       => [
               "register-and-submit" => " الرجاء إدخال رمز الخصم والنقر على زر التسجيل. ",
          ],
     ],

     "payment-method" => [
          "title" => [
               "plural" => "طرق الدفع",
          ],

          "option" => [
               "online"  => "الدفع عن طريق الانترنت",
               "deposit" => "الدفع لحساب",
               "shetab"  => "الدفع من البطاقة",
               "cash"    => "الدفع نقداً",
          ],
     ],
];
