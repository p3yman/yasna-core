<?php
return [
     'province'       => "المحافظة",
     'provinces'      => "المحافظات",
     'provinces_of_x' => "محافظات :x",
     'province_edit'  => "تحرير المحافظة",
     'province_new'   => "تسجيل محافظة جديدة",

     'city'        => "المحافظة",
     'cities'      => "المحافئات",
     'cities_list' => "قائمة المحافظات" ,
     'cities_of_x' => "محافظات :x",
     'city_edit'   => "تحرير المحافظة",
     'city_new'    => "تسجيل محافظة جديدة",


];
