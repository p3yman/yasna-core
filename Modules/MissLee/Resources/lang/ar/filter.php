<?php
return [
     "title" => [
          "plural" => "أدوات البحث",
     ],

     "option" => [
          "refresh"   => "تحديث الأدوات",
          "filtering" => "إدخال الأدوات",
     ],
     "search" => [
          "title" => "البحث",

     ],

     "sort" => [
          "title" => "التصنيف",

          "price"       => [
               "max-to-min" => "السعر مرتفع » منخفص",
               "min-to-max" => "السعر منخفض » مرتفع",
          ],
          "code"        => [
               "max-to-min" => "رمز المنتج عالي » منخفض",
               "min-to-max" => " رمز المنتج منخفض » عالي",
          ],
          "best-seller" => "الأكثر مبيعاً",
          "favorites"   => "المفضل",
          "new"         => "الأحدث",
     ],

     "range" => [
          "from" => "من",
          "to"   => "إلى",
     ],

     "availability" => [
          "positive" => "موجود",
          "negative" => "غير موجود",
     ],

     "sale-type" => [
          "special" => "عروض خاصة",
     ],
];
