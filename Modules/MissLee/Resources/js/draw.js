function drawingProgress(now_processed) {
    //Hide/Show Elements
    let $bar = $('#divProgress');
    $('.-progressHide').parent().hide();
    $bar.parent().show();

    //Progress Effect
    let current_value = parseInt($bar.attr('aria-valuenow'));
    let total_numbers = parseInt($bar.attr('aria-valuemax'));
    let new_value = current_value + now_processed;
    let percent = (new_value * 100 / total_numbers);
    if (percent > 100) percent = 100;
    $bar.attr('aria-valuenow', new_value).css('width', percent.toString() + "%");

    //Next Stage
    $("#btnPrepare").click();
}

function drawingRandom(max) {
    var $input = $("#txtDrawingGuess");

    for ($i = 1; $i < 100; $i++) {
        setTimeout(function () {
            var random_number = Math.floor(Math.random() * (max)) + 1;
            $input.val(forms_pd(random_number.toString()));
        }, 10 * $i);
    }
    setTimeout(function () {
        $("#btnSubmit").click();
    }, 11 * $i);

}

function drawingDelete(deleteUrl, post_id) {
    $.ajax({
        url: deleteUrl,
        cache: false
    })
        .done(function (html) {
            divReload('divWinnersTable');
            rowUpdate('tblPosts', post_id)
        });
}