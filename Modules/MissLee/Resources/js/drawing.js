function drawingCode() {
    var $val = ed($('#gift-input').val());
    var load = $('.load').html();
    var result = $('.result-item');

    result.html(load).show();

    if ($val) {
        $.ajax({
            type: "POST",
            url: drawingVars.routes.submit,
            cache: false,
            dataType: "json",
            data: {
                code: $val.replace(/\s/g, ''),
                _token: $('meta[name=csrf-token]').attr('content'),
            }
        }).done(function (Data) {
            if (Data.status == 'success') {
                result.text(Data.msg).show();
                setTimeout(function () {
                    window.location = drawingVars.routes.userDrawings;
                }, 3000);
            }
            else {
                result.text(Data.msg).show();
            }
        });
    }
    else {
        result.text(drawingVars.messages.invalidCode).show();
    }

}

$(document).ready(function () {
    $('form#drawing-form').submit(function (e) {
        e.preventDefault();
        drawingCode();
    })
});