@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle(CurrentModule::trans('general.contact-us'));
    Template::appendToNavBar([
        CurrentModule::trans('general.contact-us'), request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content mb40">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 col-center">
                    <div class="col-sm-6">
                        @include(CurrentModule::bladePath('contact.contact-info'))
                    </div>
                    <div class="col-sm-6">
                        @include(CurrentModule::bladePath('contact.form'))
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include(CurrentModule::bladePath('contact.map'))
@append
