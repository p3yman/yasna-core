@if ($commenting_post and $commenting_post->exists)
	@php
		$fields = CommentTools::translateFields($commenting_post->spreadMeta()->fields);
	@endphp
	<div class="part-title">
		<h3>{{ $commenting_post->title2 }}</h3></div>
	<form action="{{ CurrentModule::actionLocale('CommentController@save') }}" id="contact-form" class="js"
		  method="post">
		{{ csrf_field() }}
		<input type="hidden" name="post_id" value="{{ $commenting_post->hashid }}">
		<div class="row">
			@foreach($fields as $fieldName => $fieldValue)
				@switch($fieldName)
					@case('name')
					@case('email')
					@case('mobile')
					<div class="col-md-4">
						<div class="field">
							<label>{{ trans('validation.attributes.' . $fieldName) }}</label>
							<input type="text" name="{{ $fieldName }}">
						</div>
					</div>
					@break

					@case('message')
					<div class="col-xs-12">
						<div class="field">
							<label>{{ trans('validation.attributes.message') }}</label>
							<textarea rows="4" name="{{ $fieldName }}"></textarea>
						</div>
						<div class="contact-action tal">
							<button class="green">{{ CurrentModule::trans('form.button.send') }}</button>
							<div class="result">
								@include(CurrentModule::bladePath('layouts.widgets.form.feed'))
								{{--<div class="alert blue alt compact">--}}
								{{--<p> پیام با موفقیت ارسال شد </p>--}}
								{{--</div>--}}
								{{--<div class="alert red alt compact">--}}
								{{--<p> مشکلی در ارسال پیام ایجاد شده است </p>--}}
								{{--</div>--}}
							</div>
						</div>
					</div>
					@break
				@endswitch
			@endforeach
		</div>
	</form>

	@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@endif
