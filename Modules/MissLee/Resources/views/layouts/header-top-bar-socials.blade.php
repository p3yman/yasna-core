<div class="social-links">
	<a href="{{ CurrentModule::actionLocale('FrontController@contact')  }}" class="social-link contacts-link">
		<i class="fa fa-envelope"></i>
	</a>
	<a href="{{ get_setting('telegram_link') }}" class="social-link">
		<i class="fa fa-telegram"></i>
	</a>
	<a href="{{ get_setting('instagram_link') }}" class="social-link">
		<i class="fa fa-instagram"></i>
	</a>
	<a href="{{ get_setting('linkedin_link') }}" class="social-link">
		<i class="fa fa-linkedin"></i>
	</a>
</div>
