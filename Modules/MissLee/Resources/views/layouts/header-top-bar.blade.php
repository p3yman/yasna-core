<div id="topbar">
    <div class="container">
        @include(CurrentModule::bladePath('layouts.header-top-bar-tel-contact'))
        <div class="f-l">
            @include(CurrentModule::bladePath('layouts.header-top-bar-select-lang'))
            @include(CurrentModule::bladePath('layouts.header-top-bar-login-register-btn'))
            @include(CurrentModule::bladePath('layouts.header-top-bar-user-menu'))
            @include(CurrentModule::bladePath('layouts.header-top-bar-socials'))
        </div>
    </div>
</div>