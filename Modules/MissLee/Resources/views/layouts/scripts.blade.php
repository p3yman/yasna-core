<script>
    window.urls = {
        reloadCart: "{{ CurrentModule::actionLocale('CartController@reloadView') }}"
    };
</script>
<!-- Load Scripts -->

{!! Html::script(CurrentModule::asset('libs/jquery/jquery.min.js')) !!}
{!! Html::script(CurrentModule::asset('libs/jquery-ui/jquery-ui.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/responsiveslides.min.js')) !!}
{!! Html::script(CurrentModule::asset('libs/owl-carousel/js/owl.carousel.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/lightbox.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/slick.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/persian.min.js')) !!}
{{--{!! Html::script(CurrentModule::asset('js/bootstrap-select.min.js')) !!}--}}
{!! Html::script(CurrentModule::asset('js/nanoscroller.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/notify.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/app.min.js')) !!}
{!! Html::script(CurrentModule::asset('js/front.min.js')) !!}