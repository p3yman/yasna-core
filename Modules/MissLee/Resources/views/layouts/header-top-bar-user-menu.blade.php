@if(user()->exists)
	<div class="dropdown bottom-left user">
		<a href="#" class="dropdown-toggle">
        <span>
            <img src="{{ CurrentModule::asset("images/user-white.svg") }}">
        </span>
		<span class="text">
			{{ user()->full_name }}
		</span>
		</a>
		<div class="menu">
			<a href="{{ CurrentModule::actionLocale('UserController@orders') }}">
				{{ CurrentModule::trans('user.dashboard') }}
			</a>
			<a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
				{{ CurrentModule::trans('user.edit-profile') }}
			</a>
			{{--<a href="{{ CurrentModule::actionLocale('UserController@drawing') }}">--}}
				{{--{{ CurrentModule::trans('general.drawing.code.registered-plural') }}--}}
			{{--</a>--}}
			{{--<a href="{{ CurrentModule::actionLocale('UserController@events') }}">--}}
				{{--{{ CurrentModule::trans('post.event.plural') }}--}}
			{{--</a>--}}
			<a href="{{ route('logout') }}">
				{{ CurrentModule::trans('user.logout') }}
			</a>
		</div>
	</div>
@endif