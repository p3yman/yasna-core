<div id="logo-part">
    <div class="container">
        @if(!isset($isIndex) or !$isIndex)
        @include(CurrentModule::bladePath('layouts.header-logo'))
        @endif
        @include(CurrentModule::bladePath('layouts.header-left-buttons'))
        @include(CurrentModule::bladePath('layouts.header-main-menu'))
    </div>
</div>