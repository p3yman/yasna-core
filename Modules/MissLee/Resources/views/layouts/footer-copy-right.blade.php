<div class="copyright">
    {{ CurrentModule::trans('general.footer.copyright', ['site' => setting()->ask('site_title')->gain()]) }}
    <br/>
{{--    {{ CurrentModule::trans('general.footer.created-by') }}--}}
    <a class="text-gray" href="{{ setting()->ask('yasna-group-url')->gain() }}" target="_blank">
        {{-- {{ CurrentModule::trans('general.footer.yasna-team') }}--}}
    {{ setting()->ask('yasna-group-title')->gain() }}
    </a>
</div>