<div class="dark-navbar">
	@include(CurrentModule::bladePath('layouts.header-top-bar'))
	<div class="menu-fixed">
		@include(CurrentModule::bladePath('layouts.header-logo-part'))
		@if(isset($isIndex) and $isIndex)
		@include(CurrentModule::bladePath('layouts.dark-navbar.logo-shadow'))
		@endif
	</div>
</div>