<body>
@include(CurrentModule::bladePath('layouts.header-content'))
@yield('body')
@include(CurrentModule::bladePath('layouts.footer'))
@include(CurrentModule::bladePath('layouts.widgets.floating-btn'))
@include(CurrentModule::bladePath('layouts.scripts'))
@yield('html-footer')
</body>