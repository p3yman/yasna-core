<div id="logo-part">
    <div class="container">
        @include(CurrentModule::bladePath('layouts.header-logo'))
        @include(CurrentModule::bladePath('layouts.header-left-buttons'))
        @include(CurrentModule::bladePath('layouts.header-main-menu'))
    </div>
</div>