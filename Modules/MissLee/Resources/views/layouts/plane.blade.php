<!DOCTYPE html>
<html lang="{{ getLocale() }}">
@include(CurrentModule::bladePath('layouts.head'))
@include(CurrentModule::bladePath('layouts.body'))
</html>