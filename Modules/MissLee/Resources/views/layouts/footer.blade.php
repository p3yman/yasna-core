<footer id="main-footer">
    <div class="container">
        @include(CurrentModule::bladePath('layouts.footer-logo'))
        @include(CurrentModule::bladePath('layouts.footer-menu'))
        @include(CurrentModule::bladePath('layouts.footer-copy-right'))
    </div>
</footer>