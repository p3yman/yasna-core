{{ CurrentModule::trans('user.password.token-expired') }}
<br/>
<a href="{{ route('password.request') }}">{{ CurrentModule::trans('user.password.get-new-token') }}</a>