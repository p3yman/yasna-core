@extends(CurrentModule::bladePath('layouts.widgets.dialog.main'))

@php $dialogImage = true @endphp

@section('dialog-image-link')
    {{ CurrentModule::asset("images/loading/bar-red.gif") }}
@endsection

{{--
@section('dialog-text')
    {{ CurrentModule::trans('form.feed.wait') }}
@endsection--}}
