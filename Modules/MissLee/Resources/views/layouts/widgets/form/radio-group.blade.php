@php $prefix = $name or str_random(5) @endphp

<div class="col-xs-12 field {{ (isset($container['class']) ? $container['class'] : '') }}"
     title="{{ trans('validation.attributes_example.' . $name)}}"
        {{ isset($container['id']) ? "id=$container[id]" : '' }}>
    @if(!isset($label))
        @php $label = Lang::has("validation.attributes.$name") ? trans("validation.attributes.$name") : $name @endphp
    @endif
    @if($label)
        <label> {{ trans('validation.attributes.' . $name) }} </label>
    @endif
    @foreach($options as $opValue => $opText)
        @php $opId = $prefix . "-" . array_search($opValue, array_keys($options)) @endphp
        <div class="radio d-ib {{ $color or 'blue' }}">
            <input id="{{ $opId }}" type="radio" name="{{ $name }}" class="{{ $class or '' }}"
                value="{{ $opValue }}"
                {{ $other or '' }}
                @if($value == $opValue)
                checked="checked"
                @endif
               error-value="{{ trans('validation.javascript_validation.' . $name) }}"
            >
            <label for="{{ $opId }}"> {{ $opText }} </label>
        </div>
    @endforeach
</div>