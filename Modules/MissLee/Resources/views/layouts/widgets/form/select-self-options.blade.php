@php
    if(!isset($value)) {
        $value = null;
    }
@endphp

@if(isset($options))
    @foreach($options as $opValue => $opText)
        <option value="{{ $opValue }}" @if($value == $opValue) selected @endif>{{ $opText }}</option>
    @endforeach
@endif