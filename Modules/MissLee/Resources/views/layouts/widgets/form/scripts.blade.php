@section('html-footer')
    <!-- Load Form Scripts -->
    {!! Html::script(CurrentModule::asset('js/jquery.form.min.js')) !!}
    {!! Html::script(CurrentModule::asset('js/calendar.min.js')) !!}
    {!! Html::script(CurrentModule::asset('js/jquery.ui.datepicker-cc.min.js')) !!}
    {!! Html::script(CurrentModule::asset('js/jquery.ui.datepicker-cc-fa.min.js')) !!}
    {!! Html::script(CurrentModule::asset('js/forms.min.js')) !!}
@append