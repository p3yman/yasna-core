@php
	$cart = Template::cart();
	$cartOrders = $cart->orders;
	$ordersCount = $cartOrders->count();
@endphp
<div class="cart cart-counter mt10 @if($ordersCount) full @endif">
	<a href="{{ CurrentModule::actionLocale('CartController@index') }}" class="icon-cart"
	   data-notify="{{ ad($ordersCount) }}"></a>
</div>