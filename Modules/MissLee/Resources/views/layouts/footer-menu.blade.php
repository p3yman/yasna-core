<ul class="menu-footer">
    <li>
        <a href="{{ CurrentModule::actionLocale('FrontController@index') }}"> {{ CurrentModule::trans('general.home') }} </a>
    </li>
    <li>
        <a href="{{ CurrentModule::actionLocale('FrontController@page', ['page' => 'about']) }}">
            {{ CurrentModule::trans('general.about-us') }}
        </a>
    </li>
    <li>
        <a href="{{ RouteTools::getPosttypeRoute('products') }}">
            {{ CurrentModule::trans('post.product.plural') }}
        </a>
    </li>
    {{--<li>--}}
        {{--<a href="{{ RouteTools::getPosttypeRoute('teammates') }}">--}}
            {{--{{ CurrentModule::trans('post.teammate.plural') }}--}}
        {{--</a>--}}
    {{--</li>--}}
    <li>
        <a href="{{ CurrentModule::actionLocale('FrontController@page', ['page' => 'return-policies']) }}">
            {{ CurrentModule::trans('general.return-policies') }}
        </a>
    </li>
    <li>
        <a href="{{ CurrentModule::actionLocale('FrontController@contact') }}"> {{ CurrentModule::trans('general.contact-us') }} </a>
    </li>
</ul>