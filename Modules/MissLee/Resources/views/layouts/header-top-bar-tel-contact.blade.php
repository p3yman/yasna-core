@php $telephone = Template::contactInfo()['telephone'] @endphp

<div class="f-r">
    <div class="contact">
        @if($telephone and is_array($telephone) and count($telephone))
            <a href="{{ CurrentModule::actionLocale('FrontController@contact') }}">
                <i class="icon-phone" style="opacity: 0.7;"></i>
                <span style="direction: ltr; display: inline-block;">{{ ad($telephone[0]) }}</span>
            </a>
        @endif
            <a href="{{ CurrentModule::actionLocale('FrontController@contact')  }}" class="mr10">
                <i class="fa fa-envelope" style="opacity: 0.7;"></i> {!! get_setting('email')[0] !!}
            </a>
    </div>
</div>
