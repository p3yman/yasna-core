<article>
    @if($addresses->count())
        <div class="row addresses-list">
                @foreach($addresses as $address)
                        @include(CurrentModule::bladePath('user.addresses.item'))
                @endforeach
        </div>
        <div class="pagination-wrapper mt20">
            {!! $addresses->render() !!}
        </div>
    @else
        <div class="alert red">
            {{ CurrentModule::trans('user.message.drawing-code-not-found') }}
        </div>
    @endif
</article>