@section('html-footer')
    <script>
        function refreshAddressRow(key) {
            $.ajax({
                url: "{{ CurrentModule::actionLocale('UserController@addressesRow', ['hashid' => '__KEY__']) }}"
                    .replace('__KEY__', key),
                success: function (rs) {
                    let row = $('.addresses-list-item#' + key);
                    if (row.length) {
                        row.replaceWith(rs);
                    } else {
                        $('.addresses-list').prepend(rs);
                    }
                }
            });
        }

        function addressAddedCallback(key) {
            $('body').removeClass('modal-open');
            $('.modal').removeClass('open');
            $('.modal-wrapper').removeClass('open');

            $('.overlay').removeClass('open');
            $('.blurable').removeClass('open');
            setTimeout(function () {
                $('.overlay').remove();
            }, 300);

            refreshAddressRow(key);
        }

        $(document).ready(function () {
            let modalBtn = $('button[data-modal=add-address-modal]');

            $(document).on({
                change: function () {
                    let key = $(this).val();
                    $.ajax({
                        url: "{{ route('address.makeDefault', ['hashid' => '__KEY__']) }}"
                            .replace('__KEY__', key),
                        dataType: 'JSON',
                        beforeSend: function () {
                            $('.addresses-list').addClass('disabled-box');
                        },
                        success: function (rs) {
                            if (rs.success) {
                                $('.addresses-list').removeClass('disabled-box');
                            }
                        }
                    })
                }
            }, 'input[type=radio][name=default-address]');

            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let row = $(this).closest('.addresses-list-item');
                    let data = $.parseJSON(row.find('.json').val());
                    let mapItem = getInFormMapItem();

                    if (data.location) {
                        let dataChanges = {};
                        let centerPoint = data.location.split(',');
                        dataChanges.mapOptions = {};
                        dataChanges.mapOptions.center = centerPoint;
                        dataChanges.markers = [
                            {position: centerPoint}
                        ];
                        mapItem.reset(dataChanges);
                    } else {
                        mapItem.refresh();
                    }

                    $.each(data, function (name, value) {
                        $('form#address-from').find(':input[name=' + name + ']').val(value).change();
                    });

                    modalBtn.trigger('click');
                }
            }, '.addresses-list-item-button.btn-edit');

            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let key = $(this).closest('.addresses-list-item').attr('id');
                    $.ajax({
                        url: "{{ route('address.remove', ['hashid' => '__KEY__']) }}"
                            .replace('__KEY__', key),
                        dataType: 'JSON',
                        success: function (rs) {
                            if (rs.success) {
                                refreshAddressRow(key);
                            }
                        }
                    })
                }
            }, '.addresses-list-item-button.btn-remove');

            modalBtn.click(function (event) {
                if (event.originalEvent !== undefined) { // is clicked by human
                    refreshForm();
                }
            });
        });
    </script>
@append