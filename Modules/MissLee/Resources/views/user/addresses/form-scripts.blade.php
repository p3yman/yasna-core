@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))

@section('html-footer')
    <script>
        let mapIdentifier = '{{ $mapContainerId }}';
        let urls = {
            getCitiesSelect: '{{ CurrentModule::actionLocale('ProvinceController@getProvinceCitiesSelect', ['province' => '__PROVINCE__']) }}',
        };

        function locationAllowedCallback() {
        }

        function locationNotAllowedCallback() {
            $('.not-allowed-message').show();
        }

        function mapJobCompleted() {
            let mapItem = getInFormMapItem();
            if (!mapItem.needsClientLocation()) {
                $('.map-in-form-cover').hide();
            }
        }

        function markerPositionChanged(marker) {
            window.ttt = marker;
            $('input#location').val(marker.getPosition().lat() + ',' + marker.getPosition().lng())
        }

        function addressAddedCallback(key) {
            $('body').removeClass('modal-open');
            $('.modal').removeClass('open');
            $('.modal-wrapper').removeClass('open');

            $('.overlay').removeClass('open');
            $('.blurable').removeClass('open');
            setTimeout(function () {
                $('.overlay').remove();
            }, 300);

            refreshAddressRow(key);
        }

        function refreshForm() {
            let mapItem = getInFormMapItem();
            if (mapItem.modified) {
                mapItem.refresh();
            }

            $('form#address-from').find('input,textarea,select').not('[name^=_]').val('').change();
        }
    </script>
    {!! GoogleMapTools::getScripts() !!}
    <script>
        let inFormMapItem = null;

        function getInFormMapItem() {
            if (!inFormMapItem) {
                inFormMapItem = maps.findMapItem(mapIdentifier);
            }

            return inFormMapItem;
        }

        function modalShown() {
            getInFormMapItem().generateMap();
            $('.map-in-form-cover').show();
        }

        $(document).ready(function () {
            $('.map-in-form-cover').click(function () {
                let mapItem = getInFormMapItem();
                if (mapItem.isEnded()) {
                    $(this).hide();
                    markerPositionChanged(mapItem.markers[0]);
                }
            });

            $('#province_id').change(function (event) {
                if (event.originalEvent !== undefined) { // is clicked by human
                    let that = $(this);

                    $.ajax({
                        url: urls.getCitiesSelect.replace('__PROVINCE__', that.val()),
                        beforeSend: function () {
                            $('#city_id').attr('disabled', 'disabled');
                        },
                        success: function (rs) {
                            $('#city_id').html(rs);
                            $('#city_id').removeAttr('disabled');
                        }
                    });
                }
            });
        });
    </script>
@append