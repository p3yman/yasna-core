<!-- Add code modal -->
<div class="modal-wrapper">
	<section class="panel modal lg" id="add-address-modal" data-onshow="modalShown()">
		{!! Form::open([
			'url' => CurrentModule::actionLocale('AddressController@save'),
			'method'=> 'post',
			'class' => 'js',
			'name' => 'address-from',
			'id' => 'address-from',
			'style' => 'padding: 15px;',
			'autocomplete' => 'off'
		]) !!}
		<article>
			@php
				$mapContainerId = 'my-map';
				$map = googleMap([
						'autoGenerate' => false,
					])->element($mapContainerId)
					->markers([
						[
							'position' => '__CURRENT__',
						]
					])
					->allowedCallback('locationAllowedCallback')
					->notAllowedCallback('locationNotAllowedCallback')
					->completedCallback('mapJobCompleted')
					->allMarkersCallbacks([
						'position_changed' => 'markerPositionChanged'
					]);
			@endphp
			<h2> {{ CurrentModule::trans('general.address.register') }} </h2>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<input type="hidden" value="" name="id">

					@include(CurrentModule::bladePath('user.profile.field'), [
						'name' => 'receiver',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required',
						'min' => 2,
					])

					<div class="row">
						<div class="col-md-6 col-xs-12">
							@include(CurrentModule::bladePath('layouts.widgets.form.select'), [
								'name' => 'province_id',
								'value' => $defaultProvince ?: '',
								'options' => $provincesCombo,
								'class' => 'form-required',
								'error_value' => CurrentModule::trans('validation.javascript-validation.province'),
							])
						</div>
						<div class="col-md-6 col-xs-12">
							@include(CurrentModule::bladePath('layouts.widgets.form.select'), [
								'name' => 'city_id',
								'value' => $defaultCity ?: '',
								'options' => $citiesCombo,
								'class' => 'form-required',
								'error_value' => CurrentModule::trans('validation.javascript-validation.city'),
							])
						</div>
					</div>

					@include(CurrentModule::bladePath('user.profile.field'), [
						'name' => 'telephone',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required form-phone',
						'min' => 11,
						'max' => 11,
					])

					@include(CurrentModule::bladePath('user.profile.field'), [
						'name' => 'postal_code',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required',
						'min' => 10,
						'max' => 10,
					])

					@include(CurrentModule::bladePath('layouts.widgets.form.textarea'), [
						'name' => 'address',
						'class' => 'form-required',
					])

					@include(CurrentModule::bladePath('layouts.widgets.form.feed'))

					<button class="green">
						{{ CurrentModule::trans('general.address.register') }}
					</button>
				</div>
				<div class="col-xs-12 col-md-6">
					<div style="height: 400px;" class="map-in-form">
						<input type="hidden" value="" name="location" id="location">
						{!! $map->getView() !!}
						<div class="not-allowed-message">
							<h1 class="text-center"><i class="fa fa-exclamation-triangle"></i></h1>
							<p>{{ CurrentModule::trans('general.location.message.is-disabled-in-browser') }}</p>
							<p>
								{{ CurrentModule::trans('general.browser.firefox') }}:
								{{ CurrentModule::trans('general.location.guide.activation-firefox') }}
							</p>
							<p>
								{{ CurrentModule::trans('general.browser.chrome') }}:
								{!! CurrentModule::trans('general.location.guide.activation-chrome', [
									'button' => '<i class="fa fa-exclamation-circle"></i>'
								])  !!}
							</p>
						</div>
						<div class="map-in-form-cover">
							<div class="map-in-form-cover-alert">
								{{ CurrentModule::trans('general.location.message.select-to-increase-precision') }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="result">
				<div class="result-item" style="display: none;"></div>
			</div>

		</article>

		{!! Form::close() !!}
	</section>
</div>

@include(CurrentModule::bladePath('user.addresses.form-scripts'))