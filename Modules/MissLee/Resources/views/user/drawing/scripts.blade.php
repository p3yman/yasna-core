@section('html-footer')
    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
    <script>
        let drawingVars = {
            routes: {
                submit: "{{ CurrentModule::actionLocale('DrawingController@submitCode') }}",
                userDrawings: "{{ CurrentModule::actionLocale('UserController@drawing') }}",
                register: "{{ route('register') }}",
            },
            messages: {
                invalidCode: "{{ CurrentModule::trans('general.drawing.message.invalid-code') }}"
            }
        };
    </script>
    {!! Html::script(CurrentModule::asset('js/drawing.min.js')) !!}
@append