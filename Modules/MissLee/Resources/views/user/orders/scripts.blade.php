@section('html-footer')
    <script>
        $(document).ready(function () {
            $('.open-order-details').click(function (e) {
                e.preventDefault();
                let that = $(this);
                let hashid = that.closest('tr').data('hashid');

                $.ajax({
                    url: "{{ CurrentModule::actionLocale('UserController@orderInfo', ['order' => '__ORDER__']) }}"
                        .replace('__ORDER__', hashid),
                    beforeSend: function () {
                        $('.order-list').addClass('disabled-box');
                    },
                    success: function (rs) {
                        $('.order-list').removeClass('disabled-box');
                        $('#order-detail').html(rs);
                        $('[data-modal=order-detail]').trigger('click');
                    }
                })
            });
            
            $(document).on({
                click: function () {
                    $('.transactions-history').slideDown();
                    $(this).hide();
                }
            }, '.purchase-details')
        });
    </script>
@append