@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'orders';
    Template::appendToPageTitle(CurrentModule::trans('general.order.plural'));
    Template::appendToNavBar([
        CurrentModule::trans('general.order.plural'), CurrentModule::actionLocale('UserController@orders')
    ]);
@endphp

@section('dashboard-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-center">
                @include(CurrentModule::bladePath('user.orders.content'))
            </div>
        </div>
    </div>
@append

@include(CurrentModule::bladePath('user.orders.scripts'))