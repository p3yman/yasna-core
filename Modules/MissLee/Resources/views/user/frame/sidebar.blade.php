@php user()->spreadMeta()  @endphp

<div class="user-header">
	<div class="avatar tac">
		<img class="round-image" src="{{ user()->gravatar(CurrentModule::asset("images/user-red.svg")) }}" width="">
	</div>
	<h2 class="name"> {{ user()->full_name }} </h2>
	<hr>

	<div class="profile-tabs clearfix">
		<div class="tab @if($activeTab == 'orders') active @endif">
			<a href="{{ CurrentModule::actionLocale('UserController@orders') }}">
				<span class="dash-icon icon-cart"></span>
				<span class="dash-title">
					{{ CurrentModule::trans('general.order.plural') }}
				</span>
			</a>
		</div>
		<div class="tab @if($activeTab == 'addresses') active @endif">
			<a href="{{ CurrentModule::actionLocale('UserController@addresses') }}">
				<span class="dash-icon fa fa-address-card-o"></span>
				<span class="dash-title">
					{{ CurrentModule::trans('general.address.title.plural') }}
				</span>
			</a>
		</div>
		<div class="tab @if($activeTab == 'dashboard') active @endif">
			<a href="{{ CurrentModule::actionLocale('UserController@index') }}">
				<span class="dash-icon fa fa-comments-o"></span>
				<span class="dash-title">
				{{ CurrentModule::trans('user.comment') }}
				</span>
			</a>
		</div>
		<div class="tab @if($activeTab == 'profile') active @endif">
			<a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
				<span class="dash-icon fa fa-pencil-square-o "></span>
				<span class="dash-title">
					{{ CurrentModule::trans('user.edit-profile') }}
				</span>
			</a>
		</div>
		@if(!array_default(model('user')::getRequiredFields(), user()->toArray()))
		<div class="col-xs-12 pt20">
			<div class="row">
				<div class="alert alert-danger text-right">
					{{ CurrentModule::trans('user.not-enough-information') }}
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
						{{ CurrentModule::trans('user.edit-profile') }}
					</a>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>