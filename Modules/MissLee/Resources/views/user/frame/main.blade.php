@extends(CurrentModule::bladePath('layouts.plane'))

@section('body')
    <div class="page-content profile mt5">
        {{--@include(CurrentModule::bladePath('user.frame.header'))--}}
        <div class="dashboard-container">
            <div class="dashboard-sidebar">
                @include(CurrentModule::bladePath('user.frame.sidebar'))
            </div>
            <div class="dashboard-body">
                @yield('dashboard-body')
            </div>
        </div>
    </div>
@append

