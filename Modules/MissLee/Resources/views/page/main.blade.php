@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle($post->title);
    Template::appendToNavBar([
        $post->title, request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-center">
                    @php $post->spreadMeta() @endphp
                    {!! $post->text !!}
                </div>
            </div>
        </div>
    </div>
@append
