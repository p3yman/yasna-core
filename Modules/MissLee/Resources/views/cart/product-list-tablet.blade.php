<div class="product-list--tablet">
    @yield('products-list-tablet')
    
    <div class="cart-buttons">
        <a href="#" class="button flush-cart" onclick="flushCart()">
            {{ CurrentModule::trans('cart.button.flush') }}
        </a>
        <a href="{{ CurrentModule::actionLocale('PurchaseController@step1') }}" class="button green">
            {{ CurrentModule::trans('cart.button.settlement') }}
        </a>
    </div>
</div>