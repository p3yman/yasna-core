<div class="product-card @if(!$isAvailable) bg-light @endif" data-item="{{ $order->hashid }}">
    <div class="remove-row">
        <a href="#" class="icon-close" onclick="removeItem('{{ $order->hashid }}')"></a>
    </div>
    <div class="product-header">
        <a href="{{ $productUrl }}" target="_blank">
            <img src="{{ $postImage }}" width="80">
            <h5>{{ $productTitle }}</h5>
        </a>
    </div>
    <div class="product-details">
        @if ($isAvailable)
            <div class="clearfix">
                <div class="col-half">
                    <div class="product-price">
                    <span class="title">
                        {{ CurrentModule::trans('cart.price.unit') }}
                    </span>
                        {{ $unitPriceToShow }}
                        {{ CurrentModule::trans('general.unit.currency.IRT') }}
                    </div>
                </div>
                <div class="col-half">
                    <div class="product-count">
                    <span class="title">
                    {{ CurrentModule::trans('cart.numeration-label.amount') }}
                </span>
                        <input type="text" value="{{ $countToShow }}" style="max-width: 100px; display: inline;"
                               class="product-amount product-counter @if(!$unit->is_discrete) float @endif">
                        {{ $unit->title }}
                    </div>
                </div>
            </div>
            <div class="col-full">
                <div class="price-total">
                    @if ($isOnSale)
                        <table>
                            <tr>
                                <td>{{ CurrentModule::trans('cart.price.total') }}:</td>
                                <td>
                                    {{ ad(number_format($totalPrice)) }}
                                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ CurrentModule::trans('cart.headline.discount') }}:</td>
                                <td>
                                    {{ $discountToShow }}
                                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                                </td>
                            </tr>
                            <tr>
                                <td class="total" colspan="2">
                                    {{ $salePriceToShow }}
                                </td>
                            </tr>
                        </table>
                    @else
                        <div class="price-total--single">
                            <span class="title">
                                {{ CurrentModule::trans('cart.price.total').": " }}
                            </span>
                            {{ $totalPriceToShow }}
                            {{ CurrentModule::trans('general.unit.currency.IRT') }}
                        </div>
                    @endif
                </div>
            </div>
        @else
            <div class="col-full color-red text-center pt5 pb5">
                {{ CurrentModule::trans('post.product.is-not-available') }}
            </div>
        @endif
    </div>
</div>