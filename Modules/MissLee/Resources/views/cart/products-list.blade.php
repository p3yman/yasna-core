<table class="table bordered xs-hide">
    <thead>
    <tr>
        <th>{{ CurrentModule::trans('post.product.singular') }}</th>
        <th>{{ CurrentModule::trans('cart.price.unit') }}</th>
        <th>{{ CurrentModule::trans('cart.numeration-label.amount') }}</th>
        <th>{{ CurrentModule::trans('cart.price.total') }}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        @include(CurrentModule::bladePath('cart.products-list-item'))
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="100%" class="tal">
            <a href="#" class="button flush-cart" onclick="flushCart()">
                {{ CurrentModule::trans('cart.button.flush') }}
            </a>
            <a href="{{ CurrentModule::actionLocale('PurchaseController@step1') }}" class="button green">
                {{ CurrentModule::trans('cart.button.settlement') }}
            </a>
        </td>
    </tr>
    </tfoot>
</table>