@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@section('html-footer')
    {!! Html::script(CurrentModule::asset('js/timer.min.js')) !!}
    <script>
        function findItemRow(item) {
            return $('.cart-full').children('table').find('tr[data-item=' + item + ']');
        }

        function removeItem(item) {
            $.ajax({
                url: "{{ CurrentModule::actionLocale('CartController@removeItem') }}",
                type: "POST",
                data: {item: item, _token: $('meta[name=csrf-token]').attr('content')},
                beforeSend: function () {
                    window.cartContentEl.disableBox();
                },
                success: function (rs) {
                    refreshCart(rs);
                },
            });
        }

        function refreshCart(rs) {
            window.cartContentEl.html(rs);
            window.cartContentEl.enableBox();
        }

        function flushCart() {
            $.ajax({
                url: "{{ CurrentModule::actionLocale('CartController@flush') }}",
                success: function (rs) {
                    refreshCart(rs);
                }
            })
        }

        $(document).ready(function () {
            window.timers = {};
            window.cartContentEl = $('.page-content .container');

            $.fn.disableBox = function () {
                $(this).addClass('disabled-box');
            };

            $.fn.enableBox = function () {
                $(this).removeClass('disabled-box');
            };

            window.cartContentEl.on({
                click: function (e) {
                    e.preventDefault();
                }
            }, 'a[href=#]');

            function countModified() {
                let that = $(this);
                let item = that.closest('.product-card, tr').data('item');
                let timerKey = 't-'.item;
                let val = that.val();
                val = ed(val);
                val = val ? parseFloat(val) : 0;

                if (!window.timers[timerKey]) {
                    window.timers[timerKey] = new Timer();
                }

                window.timers[timerKey].delay(function () {

                    runningXhr = $.ajax({
                        type: 'POST',
                        url: "{{ CurrentModule::actionLocale('CartController@modifyItem', ['hashid' => '__ITEM__']) }}"
                            .replace('__ITEM__', item),
                        data: {
                            count: val,
                            _token: $('meta[name=csrf-token]').attr('content')
                        },
                        beforeSend: function () {
                            window.cartContentEl.disableBox();
                        },
                        success: function (rs) {
                            refreshCart(rs);
                        },
                    });

                }, 1);
            }

            $(document).on({
                keyup: countModified,
                change: countModified,
            }, 'input.product-amount');
        });
    </script>
@append
