@php unset($locale) @endphp

@include("manage::widgets.grid-text" , [
    'condition' => $model->has('event') and $model->total_receipts_count,
    'text' => CurrentModule::trans('general.drawing.receipts-count-amount' , [
        'count' => ad(number_format($model->total_receipts_count)),
        'amount' => ad(number_format($model->total_receipts_amount/10)),
    ])
])


@include("manage::widgets.grid-text" , [
    'condition' => $model->has('event') and Carbon::now() > $model->event_ends_at and !count($model->winners_array),
    'text' => CurrentModule::trans('general.drawing.draw.singular'),
    'icon' => "gift",
    'link' => "modal:". CurrentModule::action('DrawingController@draw', ['hashid' => $model->hashid], false),
    'class' => "btn btn-default btn-lg",
])

@include("manage::widgets.grid-tiny" , [
    'condition' => $total_winners = count($model->winners_array),
    'text' => ad($total_winners) . ' ' . CurrentModule::trans('general.drawing.winner'),
    'icon' => "smile-o",
    'color' => "primary",
    'link' => "modal:" . CurrentModule::actionLocale('DrawingController@singleAction', ['hashid' => $model->hashid, 'viewFile' => 'winners'], false),
])