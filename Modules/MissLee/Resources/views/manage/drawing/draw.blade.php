@include('manage::layouts.modal-start' , [
	'fake' => $model->prepareForDrawing(),
	'form_url' => CurrentModule::action('DrawingController@prepareForDraw'),
	'modal_title' => CurrentModule::trans('general.drawing.draw.singular'),
])
<div class='modal-body'>

    @include('manage::forms.hiddens' , ['fields' => [
        ['id' , $model->id ],
    ]])

    @include('manage::forms.input' , [
        'name' => '',
        'label' => CurrentModule::trans('post.event.singular'),
        'value' => $model->title ,
        'extra' => 'disabled' ,
    ])

    @include('manage::forms.group-start')

    @include('manage::forms.button' , [
        'id' => "btnPrepare",
        'label' => CurrentModule::trans('general.drawing.draw.prepare'),
        'shape' => 'primary',
        'class' => "-progressHide",
        'type' => 'submit' ,
    ])

    @include('manage::forms.button' , [
        'label' => CurrentModule::trans('general.cancel'),
        'shape' => 'link',
        'link' => '$(".modal").modal("hide")',
    ])

    @include('manage::forms.group-end')

    @include('manage::forms.feed' )

    <div class="progress noDisplay">
        <div id="divProgress" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
             aria-valuenow="0" aria-valuemin="0" aria-valuemax="{{ $model->receipts->count() }}" style="width:0;">
            <span class="sr-only"></span>
        </div>
    </div>

</div>
@include('manage::layouts.modal-end')