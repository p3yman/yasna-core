<tr>
    <td style="vertical-align: middle"><span class="fa fa-plus"></span></td>
    <td colspan="1">
        @include("manage::forms.hidden" , [
            'name' => "post_id",
            'value' => $model->id,
        ])
        @include("manage::forms.input-self" , [
            'id' => "txtDrawingGuess",
            'name' => "number",
            'placeholder' => CurrentModule::trans('general.drawing.take-number-between'
                    , [
                        'number1' => ad(1),
                        'number2' => ad($max_possible_number = session()->get('line_number')),
                    ]),
            'class' => "text-center ltr",
        ])
    </td>
    <td colspan="3">
        @include("manage::forms.button" , [
            'id' => "btnSubmit",
            'label' => CurrentModule::trans('form.button.submit'),
            'type' => "submit",
        ])
        @include("manage::forms.button" , [
            'label' => CurrentModule::trans('general.drawing.random-number'),
            'type' => "button",
            'link' => "drawingRandom($max_possible_number)",
            'shape' => "link",
        ])
    </td>
</tr>