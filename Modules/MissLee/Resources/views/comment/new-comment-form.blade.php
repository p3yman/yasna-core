@if (user()->id or $posttype->commenting_allow_guests)
    <div class="reply-comment pb10">
        {{--<h2>{{ trans('persiandesign::general.leave_comment') }}</h2>--}}
        <div class="row">
            
            <div class="col-md-12">
                <div class="reply-preview" style="display: none;">
                    <span class="reply-icon">
                        <i class="fa fa-mail-reply" aria-hidden="true"></i>
                    </span>
                    <span class="reply-close" onclick="escapeReplyComment()">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </span>
                    <span class="parent-text"></span>
                </div>
            </div>
            
            {!! Form::open([
                'id' => 'comment-form',
                'url' => CurrentModule::actionLocale('CommentController@save'),
                'class' => 'js'
            ]) !!}
            <input type="hidden" name="post_id" value="{{ $post->hashid }}">
            <input type="hidden" name="parent_id" value="">
            @if (auth()->guest())
                <div class="col-sm-4">
                    <!-- Fullname -->
                    <label for="fullname">{{ CurrentModule::trans('general.comment.form.fullname') }}</label>
                    <input type="text" name="name" id="fullname" class="reply-input form-required"
                           placeholder="{{ CurrentModule::trans('general.comment.form.fullname') }}"
                           error-value="{{ CurrentModule::trans('validation.javascript-validation.name') }}">
                </div>
                <div class="col-sm-4">
                    <!-- Email -->
                    <label for="email">{{ CurrentModule::trans('general.comment.form.email') }}</label>
                    <input type="email" name="email" id="email" class="reply-input form-email"
                           placeholder="{{ CurrentModule::trans('general.comment.form.email') }}"
                           error-value="{{ CurrentModule::trans('validation.javascript-validation.email') }}">
                </div>
                <div class="col-sm-4">
                    <!-- Url -->
                    <label for="url">{{ CurrentModule::trans('validation.attributes.mobile') }}</label>
                    <input type="text" name="mobile" id="url" class="reply-input form-required form-mobile"
                           placeholder="{{ CurrentModule::trans('validation.attributes.mobile') }}"
                           error-value="{{ CurrentModule::trans('validation.javascript-validation.mobile') }}">
                </div>
            @endif
        
        <!-- Comment -->
            <div class="col-md-12">
                <label for="comment">{{ CurrentModule::trans('general.comment.title.singular') }}</label>
                <textarea name="text" id="comment" cols="30" rows="10" class="reply-input form-required"
                          placeholder="{{ CurrentModule::trans('general.comment.form.statement')."..." }}"
                          error-value="{{ CurrentModule::trans('validation.javascript-validation.text') }}"></textarea>
            </div>
            
            <div class="col-xs-12">
                @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
            </div>
            
            <div class="col-sm-6">
                <button class="green" type="submit" name="_submit">
                    {{ CurrentModule::trans('general.comment.submit') }}
                </button>
            </div>
            
            {!! Form::close() !!}
        </div>
    </div>
@endif