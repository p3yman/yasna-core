@if ($posttype->commenting_receive)
    <div class="comments">
        @include(CurrentModule::bladePath('comment.new-comment-form'), ['general' => true])
        @if ($posttype->commenting_display)
            @php
                $comments = CommentTools::selectPostComments($post, $commentsLimit);
            @endphp
            @include(CurrentModule::bladePath('comment.comments-loop'))
        @endif
    </div>
@endif
