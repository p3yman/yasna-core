@php
        $transPath = CurrentModule::transPath('user.message.register-success-email');
        $messagePartsKeys = array_keys(trans($transPath));
@endphp

@foreach($messagePartsKeys as $index => $key)
    @if($index != 0) <br /> @endif
    {{ trans($transPath . '.' . $key, ['name' => $user->full_name, 'site' => setting('site_url')->gain()]) }}
@endforeach