@php
    $transPath = CurrentModule::transPath('user.password.restore-email');
    $messagePartsKeys = array_keys(trans($transPath));
@endphp

@foreach($messagePartsKeys as $index =>     $key)
    @if($index != 0) <br/> @endif
    {{ trans($transPath . '.' . $key, [ 'token' => $resetToken, 'site'  => setting()->ask('site_url')->gain(), ]) }}
@endforeach