@extends(CurrentModule::bladePath('layouts.plane'))

@php
	$page_title = CurrentModule::trans('general.tag.tag-title', ['title' => $tag->slug]);
	Template::appendToPageTitle($page_title);
	Template::appendToNavBar([
		$page_title,
		request()->url()
	]);
@endphp

@section('body')
	@include(CurrentModule::bladePath('posts.list.post.main'), ['viewFolder' => 'posts.list.post'])
@append
