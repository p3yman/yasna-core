@extends(CurrentModule::bladePath('auth.layouts.plane'))

@php
    $title = CurrentModule::trans('user.register');
    Template::appendToPageTitle($title);
    Template::setRedTitle($title);
@endphp

@section('form')
    {!! Form::open([
        'url'	=> CurrentModule::actionLocale('Auth\RegisterController@register'),
        'method'=> 'post',
        'class' => 'form-horizontal js',
        'name' => 'registerForm',
        'id' => 'registerForm',
    ]) !!}
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'name_first',
        'other' => 'autofocus',
        'type' => 'text',
        'value' => old('name_first'),
        'class' => 'form-required form-persian',
        'min' => 2,
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'name_last',
        'type' => 'text',
        'value' => old('name_last'),
        'class' => 'form-required form-persian',
        'min' => 2,
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'code_melli',
        'type' => 'text',
        'value' => old('code_melli'),
        'class' => 'form-required form-national',
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'mobile',
        'type' => 'text',
        'value' => old('mobile'),
        'class' => 'form-required form-mobile',
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'email',
        'type' => 'text',
        'value' => old('email'),
        'class' => 'form-email',
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'password',
        'type' => 'password',
        'value' => old('password'),
        'class' => 'form-required form-password',
    ])
    
    @include(CurrentModule::bladePath('auth.field'), [
        'name' => 'password2',
        'type' => 'password',
        'value' => old('password2'),
        'class' => 'form-required',

    ])
    
    <div class="tal" style="margin-bottom: 15px;">
        <button class="green block" type="submit"> {{ CurrentModule::trans('user.register') }} </button>
    </div>
    
    @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
    
    <hr class="or">
    
    <div class="tal" style="margin-bottom: 15px;">
        <a href="{{ CurrentModule::actionLocale('Auth\LoginController@showLoginForm') }}" class="button blue block">
            {{ CurrentModule::trans('user.login') }}
        </a>
    </div>
    
    
    
    {!! Form::close() !!}
    
    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@append

