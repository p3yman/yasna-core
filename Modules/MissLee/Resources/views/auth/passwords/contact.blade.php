@extends(CurrentModule::bladePath('auth.layouts.plane'))

@php
    $title = CurrentModule::trans('user.password.restore');
    Template::appendToPageTitle($title);
    Template::setRedTitle($title);
@endphp

@section('form')
    {!! Form::open([
        'url' => CurrentModule::actionLocale('Auth\ResetPasswordController@reset'),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'editForm',
        'id' => 'editForm',
        'style' => 'padding: 15px;',
    ]) !!}
    
    <div class="row">
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'code_melli',
            'label' => false,
            'placeholder' => trans('validation.attributes.code_melli'),
            'containerClass' => 'field',
        ])
        
        @include(CurrentModule::bladePath('layouts.widgets.form.radio-group'),[
            'name' => 'type',
            'value' => 'email',
            'options' => [
                'email' => trans('validation.attributes.email'),
                'mobile' => trans('validation.attributes.mobile'),
            ],
            'label' => false,
        ])
        
        
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'email',
            'label' => false,
            'placeholder' => trans('validation.attributes.email'),
            'container' => [
                'class' => 'field',
                'id' => 'email-container',
            ],
        ])
        
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'mobile',
            'label' => false,
            'placeholder' => trans('validation.attributes.mobile'),
            'container' => [
                'class' => 'field',
                'id' => 'mobile-container',
                'other' => [
                    'style' => 'display:none',
                ]
            ],
        ])
    
    </div>
    
    
    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
    {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}
    
    {{--<div class="col-md-6">--}}
    {{--<input id="email" type="email" class="form-control" name="email"--}}
    {{--value="{{ old('email') }}" required>--}}
    
    {{--@if ($errors->has('email'))--}}
    {{--<span class="help-block">--}}
    {{--<strong>{{ $errors->first('email') }}</strong>--}}
    {{--</span>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    
    {{--<div class="form-group">--}}
    {{--<div class="col-md-6 col-md-offset-4">--}}
    {{--<button type="submit" class="btn btn-primary">--}}
    {{--Send Password Reset Link--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    
    <div class="tal pb15">
        <button class="green block">
            {{ CurrentModule::trans('user.password.sent-restore-link') }}
        </button>
    </div>
    
    <div class="tal pb15">
        <a class="button green block"
           href="{{ CurrentModule::actionLocale('Auth\ResetPasswordController@getToken', ['haveCode' => 'code']) }}">
            {{ CurrentModule::trans('user.password.have-code') }}
        </a>
    </div>
    
    @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
    
    {!! Form::close() !!}
    
    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@append

@section('html-footer')
    <script>
        $(document).ready(function () {
            $('input[type=radio][name=type]').change(function () {
                if ($(this).is(':checked')) {
                    if ($(this).val() == 'email') {
                        $('#email-container').show();
                        $('#mobile-container').hide();
                    } else if ($(this).val() == 'mobile') {
                        $('#mobile-container').show();
                        $('#email-container').hide();
                    }
                }
            }).change();
        });
    </script>
@append
