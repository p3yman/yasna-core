@extends(CurrentModule::bladePath('auth.layouts.plane'))

@php
    $title = CurrentModule::trans('user.password.restore');
    Template::appendToPageTitle($title);
    Template::setRedTitle($title);
@endphp

@section('form')
    {!! Form::open([
        'url' => CurrentModule::actionLocale('Auth\ResetPasswordController@getToken'),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'editForm',
        'id' => 'editForm',
        'style' => 'padding: 15px;',
    ]) !!}

    @if(!isset($nationalId) or !$nationalId)
        <div class="row">
            @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
                'name' => 'code_melli',
                'label' => false,
                'placeholder' => trans('validation.attributes.code_melli'),
                'class' => 'form-required form-national',
                'containerClass' => 'field',
                'error_value' => trans('validation.javascript_validation.code_melli'),
            ])
        </div>
    @endif

    <div class="row">
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'password_reset_token',
            'label' => false,
            'placeholder' => trans('validation.attributes.password_reset_token'),
            'class' => 'form-required',
            'containerClass' => 'field',
            'error_value' => trans('validation.javascript_validation.password_reset_token'),
        ])
    </div>

    <div class="tal pb15">
        <button class="green block"> {{ CurrentModule::trans('user.password.check-token') }} </button>
    </div>

    @include(CurrentModule::bladePath('layouts.widgets.form.feed'))


    {!! Form::close() !!}

    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@append
