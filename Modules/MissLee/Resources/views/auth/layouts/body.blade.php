<body class="auth">
<div class="auth-wrapper">
    <div class="auth-col">
        <a href="{{ CurrentModule::actionLocale('FrontController@index') }}" class="logo">
            <img src="{{ Template::siteLogoUrl() }}">
        </a>
    </div>
    <div class="auth-col">
        @php $redTitle = Template::redTitle() @endphp
        @if($redTitle)
            <h1 class="auth-title"> {{ $redTitle }} </h1>
        @endif
        @yield('form')
    </div>
</div>
</body>

@include(CurrentModule::bladePath('layouts.scripts'))
@yield('html-footer')