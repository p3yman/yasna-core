<li class="post">
	<a href="{{ $link }}">
		<div class="post-inner">
			<div class="post-image">
				<img src="{{ $src }}" alt="{{ $alt }}">
			</div>
			<div class="post-content">
				<div class="title">
					{{ $title }}
				</div>
				<div class="abstract">
					{{ $abstract }}
				</div>
			</div>
		</div>
	</a>
</li>