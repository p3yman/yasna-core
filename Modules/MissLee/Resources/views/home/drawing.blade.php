@if ($event and $event->exists)
@section('html-header')
    @if ($eventBg = doc(get_setting('home_event_section_background'))->getUrl())
        <style>
            #gift:after {
                background-image: url("{{ $eventBg }}");
            }
        </style>
    @endif
@append
<section id="gift">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                @include(CurrentModule::bladePath('home.drawing-event-image'))
            </div>

            <div class="col-sm-4">
				@include(CurrentModule::bladePath('home.drawing-form-section'))
{{--				@include(CurrentModule::bladePath('home.drawing-event-info'))--}}
            </div>
        </div>
    </div>
    <div class="event" style="background: transparent;"></div>
</section>
@endif
