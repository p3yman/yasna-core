@php $number_of_columns = 2 @endphp


@foreach($posts as $post)
	@php $column = (($loop->iteration % $number_of_columns) ?: 2)  @endphp
	@section("blog-column-$column")
		@include(CurrentModule::bladePath('home.selected-post'),[
			"link" => $post['link'] ,
			"src" => $post['src'] ,
			"alt" => $post['alt'] ,
			"title" => $post['title'] ,
			"abstract" => $post['abstract'] ,
		])
	@append
@endforeach

<div class="selected-posts">
	<div class="container">
		<div class="simple-title">{{ CurrentModule::trans('general.selected-posts') }}</div>
		<div class="row">
			<div class="col-md-6">
				<ul class="posts">
					@yield('blog-column-1')
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="posts">
					@yield('blog-column-2')
				</ul>
			</div>
		</div>
	</div>
</div>