<h2> {{ CurrentModule::trans('general.drawing.code.register') }} </h2>
<form class="form-horizontal" id="drawing-form">
    {{ csrf_field() }}
    <div class="gift-inputs">
        <input type="text" class="gift-input" maxlength="26" id="gift-input" placeholder="-----  -----  -----  -----"
               style="width: 100%;direction: ltr">
    </div>
    <div class="action">
        <button class="block green" type="button"
                onclick="drawingCode();"> {{ CurrentModule::trans('general.drawing.code.check') }} </button>
    </div>
    <div class="result">
        <div class="load" style="display: none; width: 100%; text-align: center;">
            <img src="{{ CurrentModule::asset("images/load.gif") }}">
        </div>
        <div class="result-item" style="display: none;"></div>
    </div>
</form>

@include(CurrentModule::bladePath('user.drawing.scripts'))