@if ($customers_comments->count())
    <section id="testimonials">
        <div class="simple-title">{{ $customers_comments_post->abstract }}</div>
        <div class="testimonials-list">
            @foreach($customers_comments as $comment)
                <div class="item">
                    <div class="content">
                        <div class="text">
                            <p>{!! nl2br($comment->text) !!}</p>
                        </div>
                        <div class="person">{{ $comment->user->full_name }}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endif