@extends(CurrentModule::bladePath('layouts.plane'))

@php $hideNavbar = false; $isIndex = true; @endphp

@section('body')
    @include(CurrentModule::bladePath('home.slider'))
    {{--@include(CurrentModule::bladePath('home.mouse-wrapper'))--}}
    @include(CurrentModule::bladePath('home.about'))
    @include(CurrentModule::bladePath('home.products-categories'))
    {{--@include(CurrentModule::bladePath('home.drawing'))--}}
    {{--@include(CurrentModule::bladePath('home.comments'))--}}
	@include(CurrentModule::bladePath('home.selected-posts'), [
		'posts' => $blog_posts->map(function ($post) use ($blog_posttype) {
			$image = fileManager()
				->file($post->featured_image)
				->posttype($blog_posttype)
				->config('featured_image')
				->version('thumb')
				->resolve();

			return [
					"src" => $image->getUrl() ,
					"alt" => $image->getTitle() ,
					"title" => $post->title ,
					"abstract" => $post->abstract ,
					"link" => $post->direct_url ,
				];
		})
	])
@append