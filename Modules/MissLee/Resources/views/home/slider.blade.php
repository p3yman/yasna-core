@if($slideshow and $slideshow->count())
    <div id="home-slides" style="    display: block;width: 100%;">
        <div class="home-slides owl-carousel">
            @foreach($slideshow as $slide)
                @php $img = doc($slide->featured_image)->getUrl() @endphp
                @if($img)
                    @php $slide->spreadMeta() @endphp
                    @php $style = ($color = $slide->text_color) ? "style='color: $color'" : '' @endphp
                    <div class="home-slide" style="background-image: url({{ $img }});">
                        <a @if($slide->href) href="{{ $slide->href }}"
                           @endif style="display: block; height: 100%; width: 100%;" target="_blank">
                            <div class="container">
                                <div class="content">
                                    @if(strlen($slide->title2))
                                        <h1 {!! $style !!}> {{ $slide->title2 }} </h1>
                                    @endif
                                    @if(strlen($slide->abstract))
                                        <p {!! $style !!}> {{ $slide->abstract }} </p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endif

@section('html-footer')
    <script>
        $(document).ready(function () {
            $('.home-slides').owlCarousel({
                @if(isLangRtl())
                rtl: true,
                @endif
                loop:true,
                nav: true,
                dots: true,
                items: 1,
                autoplay: true,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                navText: [
                    '<a class="arrow icon-angle-left"></a>',
                    '<a class="arrow icon-angle-right"></a>'
                ]
            })
        })
    </script>
@endsection
