@if($event and $event->featured_image)
    @php
        $eventImage = doc($event->featured_image)
            ->posttype('events')
            ->version('620x300')
            ->getUrl();
    @endphp
    @if($eventImage)
        <div class="event-main"><img src="{{ $eventImage }}"></div>
    @endif
@endif