@php $posttype = isset($posttype) ? $posttype : $cat->posttype @endphp
<div class="col-md-3 col-sm-4">
    <a href="{{ $posttype->list_url }}/{{ $cat->slug }}" class="category-item">
        @php
            $catImage = CurrentModule::asset("images/image-not-found.png");
            $cat->spreadMeta();
            if(
                $cat->images and
                is_array($cat->images) and
                array_key_exists(getLocale(), $cat->images)
                ) {
                $catImage = doc($cat->images[getLocale()])
                    ->posttype($cat->posttype)
                    ->config('category_image_upload_configs')
                    ->version("450x650")
                    ->getUrl();
            }
        @endphp
        <img src="{{ $catImage }}">

        <div class="cat-name">
            {{--<svg width="100%" height="100%" viewBox="0 0 220 42" version="1.1"--}}
                 {{--xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">--}}
                {{--<polygon id="Rectangle" stroke="none" fill-opacity="1" fill="#ab000d"--}}
                         {{--fill-rule="evenodd" points="42 0 220 0 220 42 0 42"></polygon>--}}
            {{--</svg>--}}
            <span> {{ $cat->titleIn(getLocale()) }} </span></div>
        <div class="more">
            {{--<svg width="100%" height="100%" viewBox="0 0 220 42" version="1.1"--}}
            {{--xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">--}}
            {{--<polygon id="Rectangle" stroke="none" fill-opacity="1" fill="#E75668"--}}
            {{--fill-rule="evenodd"--}}
            {{--points="0 0 220 0 178 42 3.55271368e-15 42"></polygon>--}}
            {{--</svg>--}}
            <span> {{ CurrentModule::trans('post.product.view-plural') }} </span></div>
    </a>
</div>