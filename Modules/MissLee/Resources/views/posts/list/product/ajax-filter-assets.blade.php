@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))

@section('html-footer')
    {!! Html::script(CurrentModule::asset('js/timer.min.js')) !!}
    {!! Html::script(CurrentModule::asset('js/ajax-filter.min.js')) !!}

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>
@append