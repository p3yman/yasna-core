<header id="category-header">
    {{--        @include('front.posts.general.search-form')--}}
    <div class="field sort"><label> {{ CurrentModule::trans('filter.sort.title') }} </label>
        <div class="select rounded">
            <select class="ajax-sort">
                <option data-identifier="publishing" data-value="des">
                    {{ CurrentModule::trans('filter.sort.new') }}
                </option>
                <option data-identifier="code" data-value="desc">
                    {{ CurrentModule::trans('filter.sort.code.max-to-min') }}
                </option>
                <option data-identifier="code" data-value="asc">
                    {{ CurrentModule::trans('filter.sort.code.min-to-max') }}
                </option>
                <option data-identifier="price" data-value="desc">
                    {{ CurrentModule::trans('filter.sort.price.max-to-min') }}
                </option>
                <option data-identifier="price" data-value="asc">
                    {{ CurrentModule::trans('filter.sort.price.min-to-max') }}
                </option>
                {{--<option> {{ trans('front.best_seller') }} </option>--}}
                {{--<option> {{ trans('front.favorites') }} </option>--}}
            </select>
        </div>
    </div>
</header>