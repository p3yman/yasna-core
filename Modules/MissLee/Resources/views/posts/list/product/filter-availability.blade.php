<hr class="small">
<div class="field with-switch filter-switch filter-switch-checkbox" data-identifier="available">
    <label for="switch-a-5"> {{ CurrentModule::trans('filter.availability.positive') }}</label>
    <div class="switch d-ib blue">
        <input id="availability" type="checkbox">
        <label for="availability"></label>
    </div>
</div>