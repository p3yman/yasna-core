@isset($folder)
    @php
        $minPrice = ShopTools::postsMinPrice($allPosts);
        $maxPrice = ShopTools::postsMaxPrice($allPosts);
    @endphp
    @if($minPrice != $maxPrice)
        <hr class="small">
        <div class="field filter-slider" data-identifier="price">
            <label>
                {{--{{ trans('validation.attributes.price') }}--}}
                {{--(--}}
                {{--{{ CurrentModule::trans('filter.range.from') }}--}}
                {{--{{ ShopTools::showPrice($minPrice, false) }}--}}
                {{--{{ CurrentModule::trans('filter.range.to') }}--}}
                {{--{{ ShopTools::showPrice($maxPrice, false) }}--}}
                {{--)--}}
            </label>
            <div class="slider-container"
                 data-min="{{ ShopTools::priceToShow($minPrice, false) }}"
                 data-max="{{ ShopTools::priceToShow($maxPrice, false) }}">
                <div class="slider-self"></div>
                <div class="pt10">
                    <span class="min-label pull-left"></span>
                    <span class="max-label pull-right"></span>
                </div>
            </div>
        </div>
    @endif
@endisset