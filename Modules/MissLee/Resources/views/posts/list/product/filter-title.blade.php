@php
    $autoFocus = (Session::has('focusedElement') and (Session::get('focusedElement') == 'productName'))
        ? 'autofocus'
        : '';
@endphp
<div class="field filter-text" data-identifier="title">
    <label for="product-name"> {{ CurrentModule::trans('filter.search.title') }} </label>
    <input type="text" id="product-name" {{ $autoFocus }}>
</div>