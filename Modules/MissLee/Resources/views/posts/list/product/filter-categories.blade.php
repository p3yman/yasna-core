@isset($folder)
    @php $allCategories = $folder->children @endphp
    @if(count($allCategories) > 1)
        <hr class="small">
        <div class="field mb0 filter-checkbox" data-identifier="category">
            <label class="label-big">
                {{ CurrentModule::trans('general.categories') }}
            </label>
            @foreach($allCategories as $category)
                <div class="checkbox blue mb10">
                    <input id="category-{{ $category->slug }}" value="{{$category->slug}}" type="checkbox"
                           data-hashid="{{ $category->hashid }}" checked="checked">
                    <label for="category-{{ $category->slug }}"> {{ $category->title }} </label>
                </div>
            @endforeach
        </div>
    @endif
@endisset