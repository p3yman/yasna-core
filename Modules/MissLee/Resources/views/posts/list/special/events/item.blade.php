@php $event->spreadMeta() @endphp
{{--- If the post has a valid featured image, it will be shown, --}}
{{--- else if the posttype has a valid default featured image it will be shown --}}
{{--- and else the not found image will be shown. --}}
@php
    $postImage = CurrentModule::asset("images/image-not-found.png");
    if($event->featured_image) {
        $postSelfImage = doc($event->featured_image)->getUrl();
    }
    if(isset($postSelfImage)) {
        $postImage = $postSelfImage;
    } else {
        if($posttype->default_featured_image) {
            $posttypeImage = doc($posttype->default_featured_image)->getUrl();
        }
        if(isset($posttypeImage)) {
            $postImage = $posttypeImage;
        }
    }
@endphp
<section class="panel event-item">
    <header>
        <div class="title"> {{ $event->title }} </div>
        <div class="text-gray alt mt10 f16">
            <span class="f12">
                <i class="fa fa-clock-o"></i>
                {{ CurrentModule::trans('post.event.from') }}
                {{ ad(echoDate($event->event_starts_at, 'j F Y')) }}
                {{ CurrentModule::trans('post.event.to') }}
                {{ ad(echoDate($event->event_ends_at, 'j F Y')) }}
            </span>
            @if(user()->exists and Carbon::parse($event->event_starts_at)->lte(Carbon::now()))
                @php $color = Carbon::parse($event->event_ends_at)->lt(Carbon::now()) ? 'gray' : 'green' @endphp
                <div class="label {{ $color }} alt pull-left">
                    {{ CurrentModule::trans('post.event.collected-points') }}
                    {{ ad(user()->eventScores($event)) }}
                </div>
            @endif
        </div>
    </header>
    <article style="text-align: center;">
        {!! $event->text !!}
        <img src="{{ $postImage }}">
        <div class="text-center pt10">
            @if($event->winners_list_post)
                <a href="{{$event->winners_list_post}}" download="" target="_blank">
                    <i class="fa fa-list"></i>
                    {{trans('validation.attributes.drawing_winners')}}
                </a>
            @endif
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            @if($event->drawing_video)
                <a href="{{$event->drawing_video}}" download="" target="_blank">
                    <i class="fa fa-video-camera"></i>
                    {{trans('validation.attributes.drawing_video')}}
                </a>
            @endif
        </div>
    </article>
</section>