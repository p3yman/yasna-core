@php $posts @endphp
@if($posts and $posts->count())
	<div class="container">
		<div class="col-xs-12">
			@foreach($posts as $post)
				@include(CurrentModule::bladePath($viewFolder . '.item'))
			@endforeach
		</div>
		<div class="col-xs-12 text-center">
			{!! $posts->render() !!}
		</div>
	</div>
@endif

{{--<div class="pagination-wrapper mt20">--}}
{{--<ul class="pagination">--}}
{{--<li><a href="#">«</a></li>--}}
{{--<li class="active"><span>۱</span></li>--}}
{{--<li><a href="#">۲</a></li>--}}
{{--<li><a href="#">۳</a></li>--}}
{{--<li><a href="#">۴</a></li>--}}
{{--<li><a href="#">۵</a></li>--}}
{{--<li><a href="#">»</a></li>--}}
{{--</ul>--}}
{{--</div>--}}