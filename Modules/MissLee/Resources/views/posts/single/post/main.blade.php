<div class="row">
    <div class="col-sm-10 col-center">
        <div class="blog-single">
            <div class="blog-cover" style="text-align: center;">
                @php
                    $posttype = $post->posttype;
                    $postImage = doc($post->featured_image)
                        ->getUrl();
                @endphp
                @if($postImage)
                    <img src="{{ $postImage }}" style="width: auto; max-width: 100%;">
                @endif
            </div>
            <div class="blog-header">
                @include(CurrentModule::bladePath($viewFolder . '.categories'))
                <h1 class="title"> {{ $post->title }} </h1>
                @include(CurrentModule::bladePath($viewFolder . '.publish_info'))
            </div>
            @include(CurrentModule::bladePath($viewFolder . '.content'))
            @include(CurrentModule::bladePath($viewFolder . '.album'))
            @include(CurrentModule::bladePath($viewFolder . '.source'))
        </div>
        @include(CurrentModule::bladePath($viewFolder . '.related_posts'))
    </div>
</div>