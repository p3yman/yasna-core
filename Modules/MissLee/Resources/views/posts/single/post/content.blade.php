<div class="content text-justify">
	{!! $post->text !!}
</div>

@php $tags = $post->tags @endphp
@if ($tags->count())
	<div class="content text-justify">
		{{ CurrentModule::trans('general.tag.title.plural') }}:
		@foreach($tags as $tag)
			<a href="{{ $tag->direct_url }}" class="label tag text-gray">
				{{ $tag->slug }}
			</a>
		@endforeach
	</div>
@endif
