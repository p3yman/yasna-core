@if(isset($wares) and count($wares))
	<div class="color-selector mt40">
		@foreach($wares as $ware)

			@php
				$labels = $ware->labels()->get();
				$is_first_ware = !boolval($loop->index);
				$originalPrice = 0;
				$originalPrice = ad(number_format($ware->originalPrice()->in($previewCurrency)->get()));
				$salePrice = 0;
				if ($ware->isOnSale()){
					$salePrice = ad(number_format($ware->salePrice()->in($previewCurrency)->get()));
				}
				$isAvailable = ($ware->checkAvailability()) ? 1 : 0;

			@endphp

			@foreach($labels as $label)
				@php
					$is_first_label = ($is_first_ware and !boolval($loop->index));
					$slug = str_after($label->slug, ware()::LABEL_POSTTYPE_DIVIDER );
					$title = $label->titleIn(getLocale());
				@endphp

				@php( $color_code = config('misslee.colors.' . $slug))
				<div class="radio control--radio" title="{{ $title }}">
					<label>
						<input type="radio" value="{{ $ware->hashid }}" name="color"
							   data-currency="{{ $previewCurrencyTrans }}"
							   data-original-price = "{{ $originalPrice}}"
							   data-sale-price="{{ $salePrice }}"
							   data-availability="{{ $isAvailable }}"
							   @if($is_first_label) checked @endif>
						<div class="control__indicator" style="background-color: {{ $color_code }}"></div>
					</label>
				</div>
			@endforeach
		@endforeach
	</div>
@endif

