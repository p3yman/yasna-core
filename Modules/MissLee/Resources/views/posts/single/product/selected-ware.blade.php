@php
    $selectedWare = $selectedWare->spreadMeta()->inLocale(getLocale())->in($previewCurrency);
    $unit = $selectedWare->unit;
@endphp
@if ($wares->count() > 1)
    {{--<h3 class="product-name">{{ $selectedWare->titleIn(getLocale()) }}</h3>--}}
@endif

@foreach($wares as $ware)
    @php($ware = $ware->spreadMeta()->inLocale(getLocale())->in($previewCurrency))

    @if($ware->checkAvailability())
    <div class="ware-detail" data-ware-id="{{ $ware->hashid }}" style="display: none;">

        <div class="excerpt mt15"><p>{{ $ware->remarkIn(getLocale()) }}</p></div>

        <hr/>

        @if($ware->salePrice()->in($previewCurrency)->get() > 0)
        <div class="price">
            @if($ware->isOnSale())
                <ins>
                    {{ ad(number_format($ware->salePrice()->in($previewCurrency)->get())) }}
                    {{ $previewCurrencyTrans }}
                </ins>
                <del>
                    {{ ad(number_format($ware->originalPrice()->in($previewCurrency)->get())) }}
                    {{ $previewCurrencyTrans }}
                </del>
            @else
                <ins>
                    {{ ad(number_format($ware->originalPrice()->in($previewCurrency)->get())) }}
                    {{ $previewCurrencyTrans }}
                </ins>
            @endif
            <div class="status">
                <div class="label green"> {{ CurrentModule::trans('post.product.is-available') }}</div>
            </div>
        </div>
        @endif
        @if($ware->isPurchasable())
            <div class="field add-to-card mt20">
                {{--<label>تعداد</label>--}}
                <div class="row">
                    <div class="col-xs-6 col-sm-3 mb5">
                        <input class="ware-count product-counter @if(!$unit->is_discrete) float @endif" value="{{ ad(1) }}"
                               min="1" data-ware="{{ $ware->hashid }}">
                    </div>
                    <div class="col-xs-6 col-sm-2 mb5">
                        <div class="add-label">
                            <div class="content">
                                <h4>{{ $unit->title }}</h4>
                            </div>
                        </div>
                        {{--<label>تعداد</label>--}}
                        {{--<div class="select">--}}
                        {{--<select>--}}
                        {{--<option value="1">کیلو</option>--}}
                        {{--<option value="2">بسته</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                    </div>
                    <div class="col-md-4 col-sm-7 mb5">
                        <button class="block green add-to-cart" data-ware="{{ $ware->hashid }}">
                            {{ CurrentModule::trans('cart.button.add') }}
                        </button>
                    </div>
                </div>
            </div>
        @endif

    </div>
    @else
    <div class="ware-detail" data-ware-id="{{ $ware->hashid }}" style="display: none;">
        <div class="excerpt"><p>{{ $ware->remarkIn(getLocale()) }}</p></div>
        <hr>
        <div class="price">
            <div class="status">
                <div class="label red"> {{ CurrentModule::trans('post.product.is-not-available') }}</div>
            </div>
        </div>
    </div>
    @endif

@endforeach

