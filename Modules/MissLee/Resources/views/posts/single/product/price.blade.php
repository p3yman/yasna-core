@if($wares->count() /*and $selectedWare->checkAvailability()*/)
    @include(CurrentModule::bladePath($viewFolder . '.selected-ware'))
    {{--@include(CurrentModule::bladePath($viewFolder . '.other-wares'))--}}
@else
    <div class="price">
        <div class="status">
            <div class="label red"> {{ CurrentModule::trans('post.product.is-not-available') }}</div>
        </div>
    </div>
@endif