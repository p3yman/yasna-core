<div class="tab-content-wrapper">
    <div class="tab-content" data-id="details">
        @include(CurrentModule::bladePath('posts.single.product.content'))
    </div>
    <div class="tab-content" data-id="comments">
        @include(CurrentModule::bladePath('comment.main'))
    </div>
</div>