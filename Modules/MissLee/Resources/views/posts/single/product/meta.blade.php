<div class="product-meta">
	<div class="row">
		<div class="col-md-2 col-sm-3">
			<div class="meta-title"> {{ CurrentModule::trans('general.categories') }}:</div>
		</div>
		<div class="col-md-10 sol-sm-9">
			<div class="meta-value">
				<ul class="categories">
					@foreach($post->categories as $category)
						<li>
							<a href="{{ $posttype->list_url }}/{{ $category->slug }}" target="_blank">
								{{ $category->title }}
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	@php $tags = $post->tags @endphp
	@if ($tags->count())
		<div class="row">
			<div class="col-md-2 col-sm-3">
				<div class="meta-title"> {{ CurrentModule::trans('general.tag.title.plural') }}:</div>
			</div>
			<div class="col-md-10 sol-sm-9">
				<div class="meta-value">
					<ul class="tags">
						@foreach($tags as $tag)
							<li>
								<a href="{{ $tag->direct_url }}" target="_blank">
									{{ $tag->slug }}
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	@endif
	@if($selectedWare and $selectedWare->exists)
		<div class="row">
			<div class="col-sm-2">
				<div class="meta-title">{{ CurrentModule::trans('post.product.code') }}:</div>
			</div>
			<div class="col-sm-10">
				<div class="meta-value">
					<div class="code">{{ ad($selectedWare->custom_code) }}</div>
				</div>
			</div>
		</div>
	@endif
</div>
