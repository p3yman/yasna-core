<div class="row">
    <div class="col-sm-10 col-center">
        @php
            $post->spreadMeta();
        @endphp
        <div class="faq-item no-collapse">
            <a href="#" class="q">{{ $post->long_title }}</a>
            <div class="answer">
                <p>{!! $post->text !!}</p>
            </div>
        </div>
    </div>
</div>
