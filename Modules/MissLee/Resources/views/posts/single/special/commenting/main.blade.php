<div class="row">
    <div class="col-sm-10 col-center">
        @php $post->spreadMeta() @endphp
        <section class="panel">
            @if ($post->title2)
                <header>
                    <div class="title">
                        <span class="icon-comment"> </span>
                        {{ $post->title2 }}
                    </div>
                </header>
            @endif
            <article>
                @if ($post->text)
                    <div class="col-xs-12 pb15">
                        {!! $post->text !!}
                    </div>
                @endif
                @include(CurrentModule::bladePath('layouts.widgets.commenting-form'))
                @if($post->show_previous_comments)
                    @include(CurrentModule::bladePath($viewFolder . '.previous-comments'))
                @endif
            </article>
        </section>
    </div>
</div>