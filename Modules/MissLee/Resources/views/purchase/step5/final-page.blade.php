<div class="label green whs-n sm">
    {{ CurrentModule::trans('cart.message.thanks-for-purchasement') }}
</div>
<p class="text-xs " style="font-size: 13px; margin-top: 20px">
    {{ CurrentModule::trans('general.message.any-question-about-your-order') }}
    <a href="{{ RouteTools::getPosttypeRoute('faqs') }}">{{ CurrentModule::trans('post.faq.plural') }}</a>
</p>



@if(($telephone = get_setting('telephone')) and is_array($telephone) and count($telephone))
    <p>
        {{ CurrentModule::trans('general.contact.telephone') }}: {{ ad(implode(' - ', $telephone)) }}
    </p>
@endif
