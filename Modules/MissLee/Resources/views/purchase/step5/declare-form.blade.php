<div class="row" id="declare-payment">
    <div class="col-xs-12">
        {!! Form::open([
                'url' => CurrentModule::actionLocale('PaymentController@declare', ['transaction' => $transactionHandler->getModel()->hashid]) ,
                'method'=> 'post',
                'class' => 'js',
                'id' => 'purchase-form',
            ]) !!}
        
        <table class="table bordered method-offline" style="width: 100% !important;">
            <tbody>
            <tr>
                <td class="p30">
                    <div class="field">
                        <label>{{ CurrentModule::trans('cart.headline.tracking-code') }}</label>
                        <input type="text" name="trackingCode"/>
                    </div>
                    <div class="field">
                        <label>{{ CurrentModule::trans('cart.headline.declaration-date') }}</label>
                        <input type="text" name="declarationDate"/>
                    </div>
                    <div class="tal">
                        <button class="button green block">
                            {{ CurrentModule::trans('form.button.submit') }}
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col-xs-12" style="margin-top:12px">
                @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
            </div>
        </div>
        
        {!! Form::close() !!}
    </div>
</div>