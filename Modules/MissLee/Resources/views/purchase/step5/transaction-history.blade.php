@if ($transactions->count())
    <hr>
    <h3 class="mb15">{{ CurrentModule::trans('cart.headline.your-purchasements-details') }}</h3>
    <table class="table bordered">
        <thead>
        <tr>
            <th>{{ CurrentModule::trans('general.row') }}</th>
            <th>{{ CurrentModule::trans('cart.headline.gateway') }}</th>
            <th>{{ CurrentModule::trans('cart.headline.payment-type') }}</th>
            <th>{{ CurrentModule::trans('cart.headline.receipt-number') }}</th>
            <th>{{ CurrentModule::trans('general.date') }}</th>
            <th>{{ CurrentModule::trans('cart.headline.amount') }}</th>
            <th>{{ CurrentModule::trans('cart.payment.status') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            @php
                $account = $transaction->account
            @endphp
            <tr>
                <td>{{ ad($loop->iteration) }}</td>
                <td>{{ $account->title }}</td>
                <td>{{ CurrentModule::trans('cart.payment-method.option.' . $account->type) }}</td>
                <td>{{ ad($transaction->tracking_no) }}</td>
                <td>{{ ad(echoDate($transaction->created_at, 'j F Y - H:i')) }}</td>
                <td>{{ ShopTools::showPrice($transaction->payable_amount) }}</td>
                <td class="fw-b @if($transaction->handler->statusCode() > 0) color-green @else color-red @endif">
                    {{ CurrentModule::trans('cart.payment.status-levels.' . $transaction->status ) }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif