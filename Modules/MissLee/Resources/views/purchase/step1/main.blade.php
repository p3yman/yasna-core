@extends(CurrentModule::bladePath('purchase.layout.plane'))

@php $siteName = setting('site_title')->gain() ?: CurrentModule::trans('general.our-site') @endphp
@section('inner-body')
    <div id="page-content" class="row">
        <div class="col-sm-6 col-xs-12 text-center">
            <div style="padding-top: 50px">
                <h1><i class="fa fa-lock"></i></h1>
                <p>{{ CurrentModule::trans('user.message.have-an-account-on-site', ['site' => $siteName]) }}</p>
                <p>{{ CurrentModule::trans('user.message.login-to-complete-purchasement') }}</p>
                <a href="{{ route('login', ['redirect' => 'referrer']) }}" class="button blue">
                    {{ CurrentModule::trans('user.message.login-to-site', ['site' => $siteName]) }}
                </a>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 text-center">
            <div style="padding-top: 50px">
                <h1><i class="fa fa-user"></i></h1>
                <p>{{ CurrentModule::trans('user.message.are-you-new') }}</p>
                <p>{{ CurrentModule::trans('user.message.login-to-complete-purchasement') }}</p>
                <a href="{{ route('register', ['redirect' => 'referrer']) }}" class="button green">
                    {{ CurrentModule::trans('user.message.register-in-site', ['site' => $siteName]) }}
                </a>
                <p>
                    <small>{{ CurrentModule::trans('user.message.register-in-site-benefits', ['site' => $siteName]) }}</small>
                </p>
            </div>
        </div>
    </div>
@append