<div class="cart-total-container clearfix mt10">
    <div class="delivery">
        <h5>{{ CurrentModule::trans('cart.headline.address-and-courier') }}</h5>
        <p>
            <span class="icon-location"></span>
            {{ CurrentModule::trans('cart.message.order-address-description', [
                'receiver' => $address->receiver,
                'address' => $address->address,
                'telephone' => ad($address->telephone),
            ]) }}
        </p>
        <hr>
        <p>
            @php
                $previewCurrency = CurrencyTools::getPreviewCurrency();
                $ratio = CurrencyTools::getPreviewCurrencyRatio();
                $currency = CurrencyTools::getCurrency();
                $courierPrice = round($courier->priceIn($currency) * $ratio);
            @endphp
            <span class="icon-truck"></span>
            {{ CurrentModule::trans('cart.message.order-courier-description', [
                'courier' => $courier->titleIn(getLocale()),
                'price' => ad($courierPrice) . ' ' . CurrentModule::trans("general.unit.currency.$previewCurrency")
            ]) }}
        </p>
    </div>
</div>