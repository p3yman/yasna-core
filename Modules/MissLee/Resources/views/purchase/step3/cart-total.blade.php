<div class="cart-total-container clearfix">
    <table class="table bordered cart-total">
        <tbody>
        @if($cart->original_price != $cart->sale_price)
            {{--if cart is on sale--}}
            <tr>
                <td>{{ CurrentModule::trans('cart.price.your-purchase-total') }}:</td>
                <td>
                    {{ ad(number_format(round($cart->original_price / 10))) }}
                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                </td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('cart.headline.discount') }}:</td>
                <td>
                    {{ ad(number_format(round(($cart->original_price - $cart->sale_price) / 10))) }}
                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                </td>
            </tr>
        @endif
        <tr>
            <td>{{ CurrentModule::trans('cart.price.delivery') }}:</td>
            <td>
                {{ ad(number_format(round(($cart->delivery_price ?: 0) / 10))) }}
                {{ CurrentModule::trans('general.unit.currency.IRT') }}
            </td>
        </tr>
        <tr class="total">
            <td>{{ CurrentModule::trans('cart.price.purchasable') }}:</td>
            <td>
                {{ ad(number_format(round($cart->invoiced_amount / 10))) }}
                {{ CurrentModule::trans('general.unit.currency.IRT') }}
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>