<div class="product-list--tablet">
    @foreach($cart->orders as $order )
        @include(CurrentModule::bladePath('purchase.step3.cart-item-tablet'))
    @endforeach
</div>