@php $hashid = $courier->hashid @endphp
<div class="radio selectable-type-radio green">
    <input id="radio-{{ $hashid }}" type="radio" name="courier" value="{{ $hashid }}"
           @if($courier->id == $cart->courier_id) checked @endif>
    <label for="radio-{{ $hashid }}">
        <h5>{{ $courier->titleIn(getLocale()) }}</h5>
        <p>{{ $courier->descriptionIn(getLocale()) }}</p>
        <p>
            {{ CurrentModule::trans('cart.price.delivery') }}:
            @if ($deliveryPrice = $courier->priceIn($cart->currency))
                {{ ShopTools::showPrice($deliveryPrice) }}
            @else
                <span class="text-green">
                    <i class="fa fa-check"></i> {{ CurrentModule::trans('cart.price.free') }}
                </span>
            @endif
        </p>
    </label>
</div>