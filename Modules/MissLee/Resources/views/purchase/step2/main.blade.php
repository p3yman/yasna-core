@extends(CurrentModule::bladePath('purchase.layout.plane'))

@php
    $currentStep = 2;
@endphp

@section('inner-body')
    @if ($readyForPurchasement)
        <div id="page-content" class="row">
            @include(CurrentModule::bladePath('purchase.step2.content'))
        </div>
        @include(CurrentModule::bladePath('purchase.step2.buttons'))
    @else
        <div class="alert">{{ CurrentModule::trans('cart.alert.error.system-error-in-purchasement') }}</div>
    @endif
@append

@include(CurrentModule::bladePath('purchase.step2.scripts'))