@section('html-footer')
    <script>
        function refreshContent() {
            let contentContainer = $('#page-content');
            $.ajax({
                url: "{{ CurrentModule::actionLocale('PurchaseController@reloadStep2') }}",
                beforeSend: function () {
                    contentContainer.addClass('disabled-box');
                },
                success: function (rs) {
                    contentContainer.html(rs);
                    contentContainer.removeClass('disabled-box');
                }
            })
        }

        function addressAddedCallback(key) {
            $('body').removeClass('modal-open');
            $('.modal').removeClass('open');
            $('.modal-wrapper').removeClass('open');

            $('.overlay').removeClass('open');
            $('.blurable').removeClass('open');
            setTimeout(function () {
                $('.overlay').remove();
            }, 300);

            refreshContent();
        }

        $(document).ready(function () {
            let token = $('meta[name=csrf-token').attr('content');

            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                if (originalOptions.type === 'POST' || options.type === 'POST') {
                    options.data = $.param($.extend(originalOptions.data, {_token: token}));
                }
            });

            {{--$(document).on({--}}
                {{--change: function () {--}}
                    {{--let key = $(this).val();--}}
                    {{--$.ajax({--}}
                        {{--url: "{{ route('address.makeDefault', ['hashid' => '__KEY__']) }}"--}}
                            {{--.replace('__KEY__', key),--}}
                        {{--dataType: 'JSON',--}}
                        {{--beforeSend: function () {--}}
                            {{--$('.addresses-list').addClass('disabled-box');--}}
                        {{--},--}}
                        {{--success: function (rs) {--}}
                            {{--if (rs.success) {--}}
                                {{--$('.addresses-list').removeClass('disabled-box');--}}
                            {{--}--}}
                        {{--}--}}
                    {{--})--}}
                {{--}--}}
            {{--}, 'input[type=radio][name=default-address]');--}}

            $(document).on({
                click: function (e) {
                    e.preventDefault();

                    let row = $(this).closest('.selectable-address');
                    let data = $.parseJSON(row.find('.json').val());
                    let mapItem = getInFormMapItem();

                    if (data.location) {
                        let dataChanges = {};
                        let centerPoint = data.location.split(',');
                        dataChanges.mapOptions = {};
                        dataChanges.mapOptions.center = centerPoint;
                        dataChanges.markers = [
                            {position: centerPoint}
                        ];
                        mapItem.reset(dataChanges);
                    } else {
                        mapItem.refresh();
                    }

                    $.each(data, function (name, value) {
                        $('form#address-from').find(':input[name=' + name + ']').val(value).change();
                    });

                    $('button[data-modal=add-address-modal]').trigger('click');
                }
            }, '.addresses-list-item-button.btn-edit');

            $(document).on({
                click: function (e) {
                    e.preventDefault();

                    let key = $(this).closest('.selectable-address').attr('id');
                    $.ajax({
                        url: "{{ route('address.remove', ['hashid' => '__KEY__']) }}"
                            .replace('__KEY__', key),
                        dataType: 'JSON',
                        success: function () {
                            refreshContent();
                        }
                    })
                }
            }, '.addresses-list-item-button.btn-remove');

            $(document).on({
                click: function (event) {
                    if (event.originalEvent !== undefined) { // is clicked by human
                        refreshForm();
                    }
                }
            }, 'button[data-modal=add-address-modal]');

            $(document).on({
                change: function (event) {
                    let data = $('#page-content :input[type=radio]')
                        .filter('[name=address], [name=courier]')
                        .serializeObject();

                    $.ajax({
                        url: "{{ CurrentModule::actionLocale('CartController@modifyCart') }}",
                        type: 'POST',
                        data: data,
                        dataType: 'JSON',
                        beforeSend: function () {
                            $('#page-content').addClass('disabled-box');
                        },
                        success: function (rs) {
                            if (rs.success) {
                                refreshContent();
                            }
                        }
                    })
                }
            }, 'input[type=radio][name=courier],input[type=radio][name=address]');

            $(document).on({
                click: function (event) {
                    let contentContainer = $('#page-content');
                    let feedContainer = $('.verify-feedback');

                    $.ajax({
                        url: "{{ CurrentModule::actionLocale('PurchaseController@verifyStep2') }}",
                        dataType: 'JSON',
                        beforeSend: function () {
                            contentContainer.addClass('disabled-box');
                            feedContainer.slideUp();
                        },
                        success: function (rs) {
                            if (rs.success) {
                                window.location.replace("{{ CurrentModule::actionLocale('PurchaseController@step3') }}");
                            }
                        },
                        error: function (jqXhr) {
                            if (jqXhr.status == 422) {
                                let errors = jqXhr.responseJSON.errors;
                                let ul = $('<ul />');

                                $.each(errors, function (key, value) {
                                    let li = $('<li />').html(forms_digit_fa(value));
                                    ul.append(li);
                                });


                                feedContainer.addClass("alert-danger")
                                    .html(ul)
                                    .slideDown();

                                contentContainer.removeClass('disabled-box');
                            }
                        }
                    });
                }
            }, 'button#continue');
        });
    </script>
@append