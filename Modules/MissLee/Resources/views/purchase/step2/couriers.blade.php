@if($couriers->count())
    <div class="col-sm-5">
        <section class="panel">
            <header>
                <div class="title">
                    <span class="icon-truck"></span>
                    {{ CurrentModule::trans('cart.courier.title.singular') }}
                </div>
            </header>
            <article>
                @foreach($couriers as $courier)
                    @include(CurrentModule::bladePath('purchase.step2.courier-item'))
                @endforeach
            </article>
        </section>
    </div>
@endif