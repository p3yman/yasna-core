@if ($depositAccounts->count())
    <div class="selectable-radio">
        <div class="radio green">
            <input id="radio-2" type="radio" name="paymentType" value="deposit">
            <label for="radio-2"></label>
        </div>
        <div class="left">
            <h5>{{ CurrentModule::trans('cart.payment-method.option.deposit') }}</h5>
            <div class="hide-content">
                <table style="width: 100%" class="radio-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ CurrentModule::trans('cart.headline.account-number') }}</th>
                        <th>{{ CurrentModule::trans('cart.headline.bank') }}</th>
                        <th>{{ CurrentModule::trans('cart.headline.account-owner') }}</th>
                    </tr>
                    </thead>
                    <tbody style="text-align: center">
                    @foreach($depositAccounts as $depositAccount)
                        <tr>
                            @php $depositAccount->spreadMeta() @endphp
                            <td>
                                <input type="radio" name="depositAccount" value="{{ $depositAccount->hashid }}"
                                       @if ($loop->first) checked @endif />
                            </td>
                            <td>{{ $depositAccount->account_number }}</td>
                            <td>{{ $depositAccount->bank_name }}</td>
                            <td>{{ $depositAccount->owner }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif