<section class="panel checkout-actions mt30">
    <article>
        <a href="{{ CurrentModule::actionLocale('PurchaseController@step3') }}" class="button">
            {{ CurrentModule::trans('form.button.back') }}
        </a>
        <button type="submit" class="button green">
            {{ CurrentModule::trans('cart.button.confirm-and-pay') }}
        </button>
    </article>
</section>