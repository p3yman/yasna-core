@if ($cashAccounts->count())
    @php $cashAccount = $cashAccounts->first() @endphp
    @php $cashAccount->spreadMeta() @endphp
    <input type="hidden" name="cashAccount" value="{{ $cashAccount->hashid }}"/>
    <div class="selectable-radio">
        <div class="radio green">
            <input id="radio-4" type="radio" name="paymentType" value="cash">
            <label for="radio-4"></label>
        </div>
        <div class="left">
            <h5>{{ CurrentModule::trans('cart.payment-method.option.cash') }}</h5>
            <div class="hide-content">
                {{--<input type="text">--}}
            </div>
        </div>
    </div>
@endif
