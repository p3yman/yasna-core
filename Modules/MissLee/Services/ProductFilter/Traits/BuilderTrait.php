<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:13 PM
 */

namespace Modules\MissLee\Services\ProductFilter\Traits;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait BuilderTrait
{
    protected $builder = null;

    protected $conditions = [];

    protected static $productsPosttype               = 'products';
    protected static $selecting_columns_instructions = [
        'posts' => [
            'id',
            'title',
            'featured_image',
            'meta',
            'published_at',
            'type',
        ],
        'wares' => [
            'id'             => 'ware_id',
            'sale_price'     => 'ware_sale_price',
            'original_price' => 'ware_original_price',
            'is_available',
        ],
    ];

    public function addCondition($data)
    {
        $this->conditions[] = $data;

        return $this;
    }

    protected function resetBuilder()
    {
        $joinType      = $this->wareImportance() ? 'inner' : '';
        $locale        = getLocale();
        $this->builder = Post::select(self::getSelectingColumns())
            ->whereNotNull('published_at')
            ->where('published_at', '<=', Carbon::now())
            ->where('type', self::$productsPosttype)
            ->where('posts.locale', $locale)
            ->leftJoin('wares', function ($query) use ($locale) { // The `leftJoin` method is used to show products with no wares
                $query->whereColumn('wares.sisterhood', '=', 'posts.sisterhood')
                    ->where('wares.locales', 'like', "%$locale%");
            }, null, null, $joinType)
            ->groupBy('posts.id');

        return $this->builder;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getBuilder()
    {
        return $this->builder ?: $this->resetBuilder();
    }

    protected function setBuilder(Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    public static function getSelectingColumnsinstructions()
    {
        return self::$selecting_columns_instructions;
    }

    protected static function getSelectingColumns()
    {
        $instructions  = self::$selecting_columns_instructions;
        $resultColumns = [];
        foreach ($instructions as $table => $columns) {
            if (!is_array($columns)) {
                $columns = [$columns];
            }

            foreach ($columns as $key => $val) {
                $appending = '';
                if (is_numeric($key)) {
                    $column = $val;
                } else {
                    $column    = $key;
                    $appending = ' as ' . $val;
                }
                $resultColumns[] = $table . '.' . $column . $appending;
            }
        }

        return $resultColumns;
    }
}
