<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:12 PM
 */

namespace Modules\MissLee\Services\ProductFilter\Traits;

use Illuminate\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait OutputTrait
{
    public function run()
    {
        $this->makeQueryReady();

        return $this->getBuilder()->get();
    }

    public function paginate($limit)
    {
        $obj = $this;
        Paginator::currentPageResolver(function () use ($obj) {
            return $obj->page;
        });

        $this->makeQueryReady();

        return $this->getBuilder()->paginate($limit);
    }

    public function raw()
    {
        $this->makeQueryReady();

        $builder = $this->getBuilder();
        $sql = $builder->toSql();
        $bindings = $builder->getBindings();

        foreach ($bindings as $val) {
            $sql = str_replace_first('?', is_numeric($val) ? $val : "'$val'", $sql);
        }

        return $sql;
    }

    protected function badRequest()
    {
        throw new BadRequestHttpException();
    }
}
