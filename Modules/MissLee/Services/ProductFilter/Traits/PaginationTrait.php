<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 13/12/2017
 * Time: 06:10 PM
 */

namespace Modules\MissLee\Services\ProductFilter\Traits;

trait PaginationTrait
{
    protected $page = 0;

    protected function paginationFilter(array $filterConditions)
    {
        foreach ($filterConditions as $property => $value) {
            if (!property_exists($this, $property)) {
                $this->badRequest();
            }

            $this->$property = $value;
        }

        return $this;
    }
}
