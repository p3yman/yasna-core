<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/16/18
 * Time: 3:09 PM
 */

namespace Modules\MissLee\Services\Controller;

use Illuminate\Pagination\Paginator;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\MissLee\Services\Template\MissLeeTemplateHandler as Template;

trait MissLeeFrontControllerTrait
{
    use ModuleControllerTrait {
        __construct as protected moduleConstruct;
    }
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }



    /**
     * MissLeeFrontControllerAbstract constructor.
     */
    public function __construct()
    {
        $this->moduleConstruct();
        config(['auth.providers.users.field_name' => CurrentModuleHelper::config('username-field')]);
        Paginator::defaultSimpleView(CurrentModuleHelper::bladePath('pagination.simple-default'));
        Paginator::defaultView(CurrentModuleHelper::bladePath('pagination.default'));
    }



    /**
     * @param       $errorCode
     * @param bool  $minimal if true minimal view will be loading
     * @param mixed $data
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function abort(...$parameters)
    {
        return $this->setTemplateSiteTitle()
                    ->traitAbort(...$parameters)
             ;
    }
}
