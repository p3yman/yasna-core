<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:13 PM
 */

namespace Modules\MissLee\Services\Controller;

abstract class MissLeeControllerAbstract extends ModuleControllerAbstract
{
    protected $posttype_slugs = [
         'slider'     => 'slideshows',
         'static'     => 'pages',
         'event'      => 'events',
         'product'    => 'products',
         'commenting' => 'commenting',
         'blog'       => 'blog',
    ];
}
