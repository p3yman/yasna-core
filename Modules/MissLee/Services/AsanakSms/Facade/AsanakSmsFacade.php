<?php

namespace Modules\MissLee\Services\AsanakSms\Facades;

use Illuminate\Support\Facades\Facade;

class AsanakSmsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AsanakSms';
    }
}
