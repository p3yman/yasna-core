<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:17 PM
 */

namespace Modules\MissLee\Services\Template;

class TemplateHandler
{
    private static $method_prefixes = [
         'appending' => 'appendTo',
         'merging'   => 'mergeWith',
         'imploding' => 'implode',
         'assigning' => 'set',
    ];
    protected static $__called_method = null;



    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return bool|string
     */
    public static function __callStatic($name, $arguments)
    {
        static::$__called_method = $name;

        if (starts_with($name, self::$method_prefixes['appending'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['merging'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$method_prefixes['imploding'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['assigning'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            if (property_exists(static::class, ($property = snake_case($name)))) {
                return static::$$property;
            }
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $appending
     *
     * @return bool
     */
    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $merging
     *
     * @return bool
     */
    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $parameters
     *
     * @return bool
     */
    protected static function setProperty($property, $parameters)
    {
        if (property_exists(static::class, $property)) {
            if (count($parameters)) {
                static::$$property = $parameters[0];
            } else {
                static::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $parameters
     *
     * @return string
     */
    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(static::class, $property) and is_array($propertyVal = static::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                static::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }



    /**
     * Throw Exception
     */
    private static function undefinedMethodException()
    {
        trigger_error(
             'Call to undefined method ' . __CLASS__ . '::' . static::$__called_method . '()', E_USER_ERROR
        );
    }



    /**
     * Throw Exception
     *
     * @param int $passedArgumentsCount
     */
    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
             'Too few arguments to function '
             . __CLASS__ . '::' .
             static::$__called_method
             . ', '
             . $passedArgumentsCount
             . ' passed', E_USER_ERROR
        );
    }
}
