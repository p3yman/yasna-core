<?php

namespace Modules\MissLee\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;

class DrawingCodeSubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|digits:20',
        ];
    }


    public function all($keys = null)
    {
        $value = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
            'code' => 'ed',
        ]);
        return $purified;
    }
}
