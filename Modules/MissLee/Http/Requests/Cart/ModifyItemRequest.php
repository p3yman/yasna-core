<?php

namespace Modules\MissLee\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ModifyItemRequest extends YasnaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count' => 'numeric',
        ];
    }

    public function purifier()
    {
        return [
            'count' => 'ed',
        ];
    }
}
