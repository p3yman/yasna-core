<?php

namespace Modules\MissLee\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Modules\MissLee\Http\Controllers\Auth\ResetPasswordController;
use Modules\MissLee\Traits\Auth\ResetPasswordControllerTrait;
use Modules\Yasna\Providers\ValidationServiceProvider;

class PasswordTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_melli'           => 'required_if:direct_request,true|code_melli|exists:users,code_melli',
            'password_reset_token' => 'required|numeric',
        ];
    }

    public function all($keys = null)
    {
        $value = parent::all();

        if (session()->has(ResetPasswordController::$resetTokenSessionName)) {
            $this->merge(
                ['code_melli' => session()->get(ResetPasswordController::$resetTokenSessionName)]
            );
        } else {
            $this->merge(['direct_request' => true]);
        }

        $purified = ValidationServiceProvider::purifier($value, [
            'code_melli'           => 'ed',
            'password_reset_token' => 'ed',
        ]);

        return $purified;
    }
}
