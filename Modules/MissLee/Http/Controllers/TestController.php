<?php

namespace Modules\MissLee\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MissLee\Services\Controller\MissLeeFrontControllerTrait;

class TestController extends Controller
{
    use MissLeeFrontControllerTrait;

    public function productFilterForm()
    {
        return $this->template('test.product-filter-form.main');
    }
}
