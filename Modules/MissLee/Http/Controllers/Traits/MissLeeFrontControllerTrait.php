<?php

namespace Modules\MissLee\Http\Controllers\Traits;

use Illuminate\Pagination\Paginator;
use Modules\Manage\Traits\ManageControllerTrait;

trait MissLeeFrontControllerTrait
{
    use MissLeeControllerTrait;
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }



    /**
     * MissLeeFrontControllerTrait constructor.
     */
    public function __construct()
    {
        config(['auth.providers.users.field_name' => config(self::getModuleName() . '.username-field')]);
        Paginator::defaultSimpleView('amirhossein::pagination.simple-default');
        Paginator::defaultView('amirhossein::pagination.default');
    }



    /**
     * @param      $errorCode
     * @param bool $minimal
     * @param null $data
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function abort($errorCode, $minimal = false, $data = null)
    {
        if (is_null($data)) {
            $data = $this->template('errors.' . ($minimal ? 'm' : '') . $errorCode);
        }
        return $this->traitAbort($errorCode, $minimal, $data);
    }
}
