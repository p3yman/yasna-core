<?php

namespace Modules\MissLee\Http\Controllers\Traits;

use Modules\MissLee\Services\Module\ModuleHandler;

trait ModuleControllerTrait
{
    /**
     * @return string
     */
    public static function getModuleName()
    {
        return (ModuleHandler::create(self::class))->getAlias();
    }
}
