<?php

namespace Modules\MissLee\Http\Controllers\Traits;

trait FeedbackControllerTrait
{
    /**
     * @param       $errorCode
     * @param bool  $minimal if true minimal view will be loading
     * @param mixed $data
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     *
     */
    private function abort($errorCode, $minimal = false, $data = null)
    {
        // Return response with custom data
        if (!is_null($data)) {
            if (is_array($data)) {
                return response()->json($data, $errorCode);
            } else {
                return response($data, $errorCode);
            }
        }

        // Fetch $minimal from error code if needed
        if (str_contains($errorCode, 'm')) {
            $errorCode = str_after($errorCode, 'm');
            $minimal = true;
        }

        // Return default errors in minimal or default format
        if ($minimal) {
            return response(view(self::$moduleName . '::errors.m' . $errorCode), $errorCode);
        } else {
            return response(view(self::$moduleName . '::errors.' . $errorCode), $errorCode);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Shortcuts to Json Feeds
    |--------------------------------------------------------------------------
    |
    */
    private function jsonFeedback($message = null, $setting = [])
    {
        //Preferences...
        if (!$message) {
            $message = trans('validation.invalid');
        }
        if (is_array($message) and !sizeof($setting)) {
            $setting = $message;
        }

        $default = [
            'ok'           => 0,
            'message'      => $message,
            'redirect'     => '',
            'callback'     => '',
            'refresh'      => 0,
            'modalClose'   => 0,
            'updater'      => '',
            'redirectTime' => 1000,
            'feed_class'   => "no",
        ];

        foreach ($default as $item => $value) {
            if (!isset($setting[$item])) {
                $setting[$item] = $value;
            }
        }

        //Normalization...
        if ($setting['redirect']) {
            $setting['redirect'] = url($setting['redirect']);
        }

        //Action...
        return response()->json($setting);
    }

    private function jsonSaveFeedback($is_saved, $setting = [])
    {
        //Preferences...
        $default = [
            'success_message'      => trans('manage::forms.feed.done'),
            'success_redirect'     => '',
            'success_callback'     => '',
            'success_refresh'      => '0',
            'success_modalClose'   => '0',
            'success_updater'      => '',
            'success_form_reset'   => '',
            'success_feed_timeout' => 0,
            'redirectTime'         => 1000,

            'danger_message'    => trans('validation.invalid'),
            'danger_redirect'   => '',
            'danger_callback'   => '',
            'danger_refresh'    => '0',
            'danger_modalClose' => '0',
            'danger_updater'    => '',
        ];

        foreach ($default as $item => $value) {
            if (!isset($setting[$item])) {
                $setting[$item] = $value;
            }
        }

        //Action...
        if ($is_saved) {
            return $this->jsonFeedback(null, [
                'ok'           => '1',
                'message'      => $setting['success_message'],
                'redirect'     => $setting['success_redirect'],
                'callback'     => $setting['success_callback'],
                'refresh'      => $setting['success_refresh'],
                'modalClose'   => $setting['success_modalClose'],
                'updater'      => $setting['success_updater'],
                'form_reset'   => $setting['success_form_reset'],
                'feed_timeout' => $setting['success_feed_timeout'],
                'redirectTime' => $setting['redirectTime'],
            ]);
        } else {
            return $this->jsonFeedback([
                'ok'           => '0',
                'message'      => $setting['danger_message'],
                'redirect'     => $setting['danger_redirect'],
                'callback'     => $setting['danger_callback'],
                'refresh'      => $setting['danger_refresh'],
                'modalClose'   => $setting['danger_modalClose'],
                'updater'      => $setting['danger_updater'],
                'redirectTime' => $setting['redirectTime'],
            ]);
        }
    }

    private function jsonAjaxSaveFeedback($is_saved, $setting = [])
    {
        //Preferences...
        $default = [
            'success_message'      => trans('manage::forms.feed.done'),
            'success_redirect'     => '',
            'success_callback'     => '',
            'success_refresh'      => '0',
            'success_modalClose'   => '1',
            'success_updater'      => '',
            'success_form_reset'   => '',
            'success_feed_timeout' => 0,
            'redirectTime'         => 1000,

            'danger_message'    => trans('validation.invalid'),
            'danger_redirect'   => '',
            'danger_callback'   => '',
            'danger_refresh'    => '0',
            'danger_modalClose' => '0',
            'danger_updater'    => '',
        ];

        foreach ($default as $item => $value) {
            if (!isset($setting[$item])) {
                $setting[$item] = $value;
            }
        }

        //Action...
        return $this->jsonSaveFeedback($is_saved, $setting);
    }
}
