<?php

namespace Modules\MissLee\Http\Controllers;

use Modules\Address\Http\Controllers\AddressController as OriginalController;
use Modules\Address\Http\Requests\AddressSaveRequest;

class AddressController extends OriginalController
{
    /**
     * @inheritdoc
     */
    public function save(AddressSaveRequest $request)
    {
        return parent::save($request);
    }
}
