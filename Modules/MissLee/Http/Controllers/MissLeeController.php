<?php

namespace Modules\MissLee\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MissLee\Services\Controller\MissLeeFrontControllerTrait;

class MissLeeController extends Controller
{
    use MissLeeFrontControllerTrait;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->template('home.main');
    }
}
