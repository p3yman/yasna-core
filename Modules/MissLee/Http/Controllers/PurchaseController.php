<?php

namespace Modules\MissLee\Http\Controllers;

use App\Models\Cart;
use App\Models\Courier;
use App\Models\Posttype;
use App\Models\State;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\MissLee\Http\Requests\RepurchaseRequest;
use Modules\MissLee\Providers\CartServiceProvider;
use Modules\MissLee\Providers\CurrencyServiceProvider;
use Modules\MissLee\Providers\RouteServiceProvider;
use Modules\MissLee\Services\Controller\MissLeeFrontControllerAbstract;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Providers\ValidationServiceProvider;

class PurchaseController extends MissLeeFrontControllerAbstract
{
    protected static $acceptedPaymentTypes = ['online', 'deposit', 'shetab', 'cash'];
    protected $viewVariables        = [];



    /**
     * PurchaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('cartNonempty', ['except' => ['paymentCallback', 'paymentShortLink']]);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function step1()
    {
        if (!auth()->guest()) {
            return redirect(CurrentModuleHelper::actionLocale('PurchaseController@step2'));
        }

        return $this->template('purchase.step1.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step2()
    {
        return $this->template('purchase.step2.main', $this->getAddressAndShippingOptions());
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reloadStep2()
    {
        return $this->template('purchase.step2.content', $this->getAddressAndShippingOptions());
    }



    /**
     * @return array
     */
    protected function getAddressAndShippingOptions()
    {
        $readyForPurchasement = true;

        $suitableCourier = CartServiceProvider::getSuitableCourier($cart = CartServiceProvider::findActiveCart());
        if (!$suitableCourier) {
            $readyForPurchasement = false;
            return compact('readyForPurchasement');
        }
        $cart->addCourier($suitableCourier->id);
        $couriers = collect([$suitableCourier]);

        $provincesCombo  = State::getProvinces()->pluck('title', 'id')->toArray();
        $defaultProvince = array_search(CurrentModuleHelper::config('default-province'), $provincesCombo);

        $citiesCombo = State::getCities()->pluck('title', 'id')->toArray();
        $defaultCity = array_search(CurrentModuleHelper::config('default-city'), $citiesCombo);

        $addresses       = user()->addresses()
                                 ->orderBy('default', 'desc')
                                 ->orderBy('created_at', 'desc')
                                 ->paginate(12)
        ;
        $default_address = $addresses->where('default', 1)->first();
        if ($default_address) {
            $cart->update(['address_id' => $default_address->id]);
        }

        return compact(
             'provincesCombo',
             'defaultProvince',
             'citiesCombo',
             'defaultCity',
             'addresses',
             'couriers',
             'readyForPurchasement',
             'cart'
        );
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verifyStep2()
    {
        $cart = CartServiceProvider::findActiveCart();
        $rs   = [];

        if (!$cart->courier or !$cart->courier->exists) {
            $rs['errors']['address'] = CurrentModuleHelper::trans('cart.alert.error.required-selection', [
                 'attribute' => CurrentModuleHelper::trans('cart.courier.title.singular'),
            ]);
        }

        //        @todo: use $cart->address instead of $cart->instead_id
        if (!$cart->address_id) {
            $rs['errors']['courier'] = CurrentModuleHelper::trans('cart.alert.error.required-selection', [
                 'attribute' => CurrentModuleHelper::trans('general.address.title.singular'),
            ]);
        }

        if (array_has($rs, 'errors')) {
            $rs['success'] = false;
            return $this->abort(422, true, $rs);
        } else {
            return \response()->json(['success' => true]);
        }
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function step3()
    {
        $cart = CartServiceProvider::findActiveCart();
        $cart->refreshCalculations();
        $address = $cart->address;
        $courier = $cart->courier;
        if (!$address or !$address->exists or !$courier or !$courier->exists) {
            return redirect(CurrentModuleHelper::actionLocale('PurchaseController@step2'));
        }

        return $this->template('purchase.step3.main', compact('cart', 'address', 'courier'));
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verifyStep3()
    {
        $cart = CartServiceProvider::findActiveCart();

        if ($cart->isRaisable()) {
            return \response()->json(['success' => true]);
        } else {
            return $this->abort(422, true, [
                 'success' => false,
                 'errors'  => [
                      CurrentModuleHelper::trans('cart.message.cart-changed') .
                      '. ' .
                      view(CurrentModuleHelper::bladePath('layouts.widgets.element.a'), [
                           'html' => CurrentModuleHelper::trans('cart.button.back-to-cart'),
                           'href' => CurrentModuleHelper::actionLocale('CartController@index'),
                      ]),
                 ],
            ]);
        }
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step4()
    {
        $currency        = CurrencyServiceProvider::getCurrency();
        $onlineAccounts  = payment()->accounts()->currency($currency)->online()->get();
        $depositAccounts = payment()->accounts()->currency($currency)->deposit()->get();
        $shetabAccounts  = payment()->accounts()->currency($currency)->shetab()->get();
        $cashAccounts    = payment()->accounts()->currency($currency)->cash()->get();

        return $this->template('purchase.step4.main', compact(
             'onlineAccounts',
             'depositAccounts',
             'shetabAccounts',
             'cashAccounts'
        ));
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function final(Request $request)
    {
        if ($this->paymentValidation($data = $this->paymentPurifier($request->all()))) {
            $accountId = $this->fetchPaymentAccountId($data);
            $cart      = CartServiceProvider::findActiveCart();
            $cart->update([
                 'raised_at' => Carbon::now(),
                 'raised_by' => user()->id,
            ]);

            $transaction = payment()
                 ->transaction()
                 ->invoiceId($cart->id)
                 ->amount($cart->invoiced_amount)
                 ->accountId($accountId)
                 ->autoVerify(false)
                 ->callbackUrl(CurrentModuleHelper::actionLocale('PurchaseController@paymentCallback', ['cart' => $cart->hashid]))
            ;

            if ($declarationInfo = $this->fetchDeclarationInfo($data)) {
                $transaction->resnum($declarationInfo['declarationCode'])
                            ->declared_at($declarationInfo['declarationDate'])
                ;
            }

            $transactionResponse = $transaction->fire();

            if ($transactionResponse['slug'] == 'returnable-response') {
                return $transactionResponse['response'];
            }
        }

        return redirect(CurrentModuleHelper::actionLocale('purchase.step4'));
    }



    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function paymentPurifier(array $data)
    {
        return ValidationServiceProvider::purifier($data, [
             'onlineAccount' => 'dehash',

             'depositAccount'         => 'dehash',
             'depositTrackingCode'    => 'ed',
             'depositDeclarationDate' => 'gDate',

             'shetabAccount'         => 'dehash',
             'shetabTrackingCode'    => 'ed',
             'shetabDeclarationDate' => 'gDate',

             'cashAccount' => 'dehash',

        ]);
    }



    /**
     * @param array $data
     *
     * @return bool
     */
    protected function paymentValidation(array $data)
    {
        $validator = Validator::make($data, [
             'paymentType'            => 'required|in:' . implode(',', self::$acceptedPaymentTypes),

             // Online
             'onlineAccount'          => [
                  'required_if:paymentType,online',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'online'),
             ],

             // Deposit
             'depositAccount'         => [
                  'required_if:paymentType,deposit',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'deposit'),
             ],
             //            'depositTrackingCode'    => ['required_if:paymentType,deposit',],
             'depositDeclarationDate' => [
                 //                'required_if:paymentType,deposit',
                 'required_with:depositTrackingCode',
                 'date',
             ],

             // Shetab
             'shetabAccount'          => [
                  'required_if:paymentType,shetab',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'shetab'),
             ],
             //            'shetabTrackingCode'     => ['required_if:paymentType,shetab',],
             'shetabDeclarationDate'  => [
                 //                'required_if:paymentType,shetab',
                 'required_with:shetabTrackingCode',
                 'date',
             ],

             // Cash
             'cashAccount'            => [
                  'required_if:paymentType,cash',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'cash'),
             ],
        ]);

        return !$validator->fails();
    }



    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function fetchPaymentAccountId(array $data)
    {
        return $data[$data['paymentType'] . 'Account'];
    }



    /**
     * @param array $data
     *
     * @return array|bool
     */
    protected function fetchDeclarationInfo(array $data)
    {
        $declarationCode      = $data['paymentType'] . 'TrackingCode';
        $declarationDateIndex = $data['paymentType'] . 'DeclarationDate';
        if (
             array_key_exists($declarationCode, $data) and $data[$declarationCode] and
             array_key_exists($declarationDateIndex, $data) and $data[$declarationDateIndex]) {
            return [
                 'declarationCode' => $data[$declarationCode],
                 'declarationDate' => $data[$declarationDateIndex],
            ];
        }

        return false;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function paymentCallback(Request $request)
    {
        if ($transactions = $this->findTransactions($cart = $this->findCart($request->cart))) {
            $this->checkTransaction($cart, $transactions);
            $transactionHandler = $transactions->first()->handler;
            return $this->template('purchase.step5.main', compact(
                 'cart',
                 'transactions',
                 'transactionHandler'
            ), $this->viewVariables);
        }

        return redirect(CurrentModuleHelper::actionLocale('CartController@index'));
    }



    /**
     * @param $cartHashid
     *
     * @return mixed
     */
    protected function findCart($cartHashid)
    {
        return (($cart = model('Cart')->grabHashid($cartHashid)) and $cart->exists) ? $cart : null;
    }



    /**
     * @param $cart
     *
     * @return null
     */
    protected function findTransactions($cart)
    {
        if (
             $cart and
             ($transaction = payment()->invoice($cart->id)->orderBy('created_at', 'desc')->get()) and
             $transaction->count()
        ) {
            return $transaction;
        }

        return null;
    }



    /**
     * @param Cart $cart
     * @param      $transactions
     */
    protected function checkTransaction(Cart $cart, $transactions)
    {
        if (
             ($trackingNo = \request()->tracking_no) and
             $queryTransaction = $transactions->where('tracking_no', $trackingNo)->first()
        ) {
            $transactionHandler = $queryTransaction->handler;
            if ($transactionHandler->type() == 'online') {
                if ($oldVerifiedTransaction = $cart->verifiedTransactions()->first()) {
                    if ($oldVerifiedTransaction->id != $queryTransaction->id) {
                        $transactionHandler->makeCancelled(Carbon::now(),
                             'Cart has been verified with another transaction.');
                        $this->viewVariables['transactionAlert'] =
                             CurrentModuleHelper::trans('cart.alert.error.unable-to-verify');
                    }
                } else {
                    $verificationResult = $transactionHandler->verify();
                    if ($verificationResult['code'] > 0) {
                        $queryTransaction->refresh();
                        $cart->update([
                             'disbursed_at' => $queryTransaction->declared_at,
                             'disbursed_by' => $queryTransaction->declared_by,
                             'paid_at'      => $queryTransaction->verified_at,
                             'paid_by'      => $queryTransaction->verified_by,
                        ]);
                        $this->viewVariables['transactionAlert']   =
                             CurrentModuleHelper::trans('cart.alert.success.payment');
                        $this->viewVariables['transactionSuccess'] = true;
                    } else {
                        $this->viewVariables['transactionAlert'] =
                             CurrentModuleHelper::trans('cart.alert.error.rejected-by-gateway');
                    }
                }
            }
        }

        $this->modifyCart($cart);
    }



    /**
     * @param Cart $cart
     */
    protected function modifyCart(Cart $cart)
    {
        if (
             $cart->isNotDisbursed() and
             ($declaredTransaction = $cart->declaredTransactions()->offline()->first())
        ) {
            $cart->update([
                 'disbursed_at' => $declaredTransaction->declarad_at,
                 'disbursed_by' => $declaredTransaction->declarad_by,
            ]);
        }

        if ($cart->isNotPaid() and ($verifiedTransaction = $cart->verifiedTransactions()->first())) {
            $cart->update([
                 'paid_at' => $verifiedTransaction->verified_at,
                 'paid_by' => $verifiedTransaction->verified_by,
            ]);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paymentShortLink(Request $request)
    {
        return redirect(($cart = $this->findCart($request->cart))
             ? RouteServiceProvider::action('PurchaseController@paymentCallback', [
                  'locale' => $cart->locale,
                  'cart'   => $cart->hashid,
             ])
             : url('/'));
    }
}
