<?php namespace Modules\MissLee\Http\Controllers\Auth;

use Modules\MissLee\Providers\CartServiceProvider;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\MissLee\Services\Module\ModuleHandler;
use Modules\Yasna\Http\Controllers\Auth\LoginController as YasnaLoginController;

class LoginController extends YasnaLoginController
{
    protected $username_field = 'code_melli';
    protected $login_blade    = "auth.login";



    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        CurrentModuleHelper::assign(ModuleHandler::create(get_class($this)));
        $this->login_blade = CurrentModuleHelper::bladePath($this->login_blade);
    }



    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectAfterLogin()
    {
        if (session()->has('drawingCode')) {
            $target = CurrentModuleHelper::actionLocale('UserController@drawing');
        } elseif (user()->is_an('admin')) {
            $target = url('manage');
        } else {
            $target = url('/');
        }

        return redirect()->intended($target);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        if (request()->has('redirect')) {
            switch (strtolower(request('redirect'))) {
                case 'referrer':
                    session(['url.intended' => url()->previous()]);
                    break;
            }

            session()->save();
        }

        return parent::showLoginForm();
    }
}
