<?php namespace Modules\MissLee\Http\Controllers\Auth;

use Modules\MissLee\Services\Controller\MissLeeFrontControllerTrait;
use Modules\MissLee\Services\Controller\ModuleControllerTrait;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\MissLee\Services\Module\ModuleHandler;
use Modules\Yasna\Http\Controllers\Auth\ForgotPasswordController as YasnaForgotPasswordController;

class ForgotPasswordController extends YasnaForgotPasswordController
{
    use ModuleControllerTrait {
        __construct as protected moduleTraitConstruct;
    }
    use MissLeeFrontControllerTrait {
        __construct as protected frontTraitConstruct;
    }



    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->moduleTraitConstruct();
        $this->frontTraitConstruct();
        parent::__construct();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return $this->template('auth.passwords.contact');
    }
}
