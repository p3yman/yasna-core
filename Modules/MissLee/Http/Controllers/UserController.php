<?php

namespace Modules\MissLee\Http\Controllers;

use App\Models\Address;
use App\Models\Cart;
use App\Models\Receipt;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\MissLee\Http\Requests\ProfileSaveRequest;
use Modules\MissLee\Providers\DrawingCodeServiceProvider;
use Modules\MissLee\Providers\PostServiceProvider;
use Modules\MissLee\Services\Controller\MissLeeFrontControllerAbstract;
use Modules\MissLee\Services\Module\CurrentModuleHelper;

class UserController extends MissLeeFrontControllerAbstract
{
    public function index()
    {
        $commentingPostPreview = PostServiceProvider::showPost('customers-comments', [
            'type'      => 'commenting',
            'feature'   => null,
            'showError' => false,
        ]);

        return $this->template('user.dashboard.main', compact('commentingPostPreview'));
    }

    public function orders()
    {
        $orders = \user()->carts()->whereNotNull('raised_at')->orderby('created_at', 'desc')->get();

        return $this->template('user.orders.main', compact('orders'));
    }

    public function orderInfo(Request $request)
    {
        $cart = model('Cart')->grabHashid($request->order);

        if (!$cart or !$cart->exists) {
            return $this->abort(404);
        }

        return view(CurrentModuleHelper::bladePath('user.orders.modal-content'), compact('cart'));
    }

    public function profile()
    {
        return $this->template('user.profile.main');
    }

    public function update(ProfileSaveRequest $request)
    {
        $request = $request->all();
        if ($request['new_password']) {
            $request['password'] = Hash::make($request['new_password']);
        }
        $request['id'] = user()->id;
        if ($request['marital'] != 2) {
            unset($request['marriage_date']);
        }
        return $this->jsonAjaxSaveFeedback(model('User')->batchSaveId($request, ['new_password', 'new_password2']), [
            'success_refresh' => 1,
        ]);
    }

    public function drawing()
    {
        $receipt = session()->get('drawingCode');
        if ($receipt) {
            $receipt     = decrypt($receipt);
            $receiptInfo = DrawingCodeServiceProvider::checkCode($receipt);
            if (!$receiptInfo) {
                session()->forget('drawingCode');
            }

            $foundReceipt = model("Receipt")->grab($receipt, 'code');
            if ($foundReceipt and $foundReceipt->exists) {
                session()->forget('drawingCode');
            }

            $newReceiptData = [
                'user_id'          => user()->id,
                'code'             => $receipt,
                'purchased_at'     => Carbon::createFromTimestamp($receiptInfo['date'], 'Asia/Tehran')
                    ->setTimezone('UTC'),
                'purchased_amount' => $receiptInfo['price'],
            ];
            model('Receipt')->batchSaveId($newReceiptData);

            if (setting()->ask('send_sms_on_register_code')->gain()) {
                $smsText = CurrentModuleHelper::trans('general.drawing.message.register-code-success-please-wait', [
                        'name' => \user()->name_first,
                    ]) . "\n\r"
                    . setting()->ask('site_url')->gain();

                $sendingResult = sendSms(user()->mobile, $smsText);
                $sendingResult = json_decode($sendingResult);
            }

            session()->forget('drawingCode');
            session()->forget('drawing_try');
        }

        $receipts = \user()->receipts()->paginate(20);
        return $this->template('user.drawing.main', compact('receipts'));
    }

    public function events(Request $request)
    {
        $currentEventsHTML = PostServiceProvider::showList([
            'type'         => 'events',
            'feature'      => null,
            'conditions'   => [
                ['event_starts_at', '<=', Carbon::now()],
                ['event_ends_at', '>=', Carbon::now()],
            ],
            'max_per_page' => 5,
            'showError'    => false,
        ]);

        $waitingCount = PostServiceProvider::selectPosts([
            'type'       => 'events',
            'conditions' => [
                ['event_starts_at', '>=', Carbon::now()],
                ['event_ends_at', '>=', Carbon::now()],
            ],
        ])->count();

        $expiredCount = PostServiceProvider::selectPosts([
            'type'       => 'events',
            'conditions' => [
                ['event_starts_at', '<=', Carbon::now()],
                ['event_ends_at', '<=', Carbon::now()],
            ],
        ])->count();

        return $this->template('user.events.main', compact(
            'currentEventsHTML',
            'waitingCount',
            'expiredCount'
        ));
    }



    /**
     * Show list of user's addresses
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addresses(Request $request)
    {
        $provincesCombo  = model('state')->where('parent_id', 0)->pluck('title', 'id')->toArray();
        $defaultProvince = array_search(CurrentModuleHelper::config('default-province'), $provincesCombo);

        $citiesCombo = model('state')->where('parent_id', $defaultProvince)->pluck('title', 'id')->toArray();
        $defaultCity = array_search(CurrentModuleHelper::config('default-city'), $citiesCombo);


        $addresses = addresses()
             ->user(user())
             ->orderBy('created_at', 'desc')
             ->paginate(12)
        ;

        return $this->template('user.addresses.main', compact(
             'provincesCombo',
             'defaultProvince',
             'citiesCombo',
             'defaultCity',
             'addresses'
        ));
    }

    public function addressesRow(Request $request)
    {
        $address = model('address', $request->hashid);
        if ($address and $address->exists) {
            return view(CurrentModuleHelper::bladePath('user.addresses.item'), compact('address'));
        }

        return '';
    }
}
