<?php

namespace Modules\MissLee\Http\Controllers\Manage;

use App\Models\Drawing;
use App\Models\Post;
use App\Models\Receipt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MissLee\Http\Controllers\Traits\MissLeeControllerTrait;
use Modules\MissLee\Http\Requests\Manage\DrawRequest;
use Modules\MissLee\Providers\PostServiceProvider;
use Modules\MissLee\Services\Module\CurrentModuleHelper;
use Modules\Manage\Traits\ManageControllerTrait;

class DrawingController extends Controller
{
    use ManageControllerTrait;
    use MissLeeControllerTrait;

    protected $view_folder;
    protected $base_model;

    public function __construct()
    {
        $this->view_folder = self::getModuleName() . '::manage.drawing';
        $this->base_model = new Post();
    }


    public function draw(Request $request)
    {
        $model = PostServiceProvider::smartFindPost($request->hashid, [
            'conditions' => [
                'type' => 'features:event'
            ]
        ]);

        $posttype = $model->posttype;

        if (!$posttype->has('event')) {
            return $this->abort(403);
        }

        return $this->template('manage.drawing.draw', compact('model', 'posttype'));
    }

    public function prepareForDraw(Request $request)
    {
        /*-----------------------------------------------
        | Row Selection
        */
        $post = Post::find($request->id);
        if (!$post or $post->hasnot('event')) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }

        /*-----------------------------------------------
        | Action
        */
        $limit = CurrentModuleHelper::config('draw.query-limit');

        $receiptHolders = Receipt::select('user_id')
            ->where('operation_integer', 1)
            ->limit($limit);

        $lineNumber = session()->get('line_number');


        foreach ($receiptHolders->get() as $receipt_holder) {
            $amount = $post->receipts->where('user_id', $receipt_holder->user_id)
                ->sum('purchased_amount');
            $total_points = intval(floor($amount / $post->getMeta('rate_point')));

            if ($total_points) {
                Drawing::create([
                    'user_id'    => $receipt_holder->user_id,
                    'post_id'    => $post->id,
                    'amount'     => $amount,
                    'lower_line' => $lineNumber + 1,
                    'upper_line' => $lineNumber += $total_points,
                ]);
            }
        }

        $finished = boolval($lineNumber == session()->get('line_number'));
        $receiptHolders->update(['operation_integer' => '2',]);
        session()->put('line_number', $lineNumber);

        /*-----------------------------------------------
        | Feedback
        */

        return $this->jsonAjaxSaveFeedback($finished, [
//            'redirectTime'       => $finished ? 500 : 1,
            'redirectTime'       => 500,
            'success_message'    => CurrentModuleHelper::trans('form.feed.done'),
            'success_callback'   => "masterModal('"
                . CurrentModuleHelper::action('DrawingController@winners', ['hashid' => $post->hashid]) .
                "')",
            'success_modalClose' => "0",

            'danger_message'  => CurrentModuleHelper::trans('form.feed.wait'),
            'danger_callback' => "drawingProgress($limit);rowUpdate('tblPosts','$post->hashid')",
        ]);
    }

    public function winners(Request $request)
    {
        $model = PostServiceProvider::smartFindPost($request->hashid, [
            'conditions' => [
                'type' => 'features:event'
            ]
        ]);

        $posttype = $model->posttype;

        if (!$posttype->has('event')) {
            return $this->abort(403);
        }

        return $this->template('manage.drawing.winners', compact('model', 'posttype'));
    }

    public function select(DrawRequest $request)
    {
        $user_accepted = false;
        /*-----------------------------------------------
        | Post Selection ...
        */
        $post = Post::find($request->post_id);
        if (!$post or !$post->exists) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }

        $winners = $post->winners_array;


        /*-----------------------------------------------
        | User Selection ...
        */
        $number = $request->number;
        while ($user_accepted == false) {
            $drawingRow = Drawing::pull($number);
            if (!$drawingRow or $drawingRow->post_id != $post->id) {
                return $this->jsonFeedback([
                    'message'  => CurrentModuleHelper::trans('form.feed.error'),
                    'callback' => "masterModal("
                         . CurrentModuleHelper::action('DrawingController@draw', ['hashid' => $post->hashid])
                         . ")",
                ]);
            }
            $user = $drawingRow->user;
            //if(!$user) {
            //	return $this->jsonFeedback(trans('people.form.user_deleted'));
            //}


            if ($user and $user->exists and !in_array($user->id, $winners)) {
                $user_accepted = true;
            } else {
                $number = rand(1, session()->get('line_number'));
            }
        }

        /*-----------------------------------------------
        | Save ...
        */
        $winners[] = $user->id;
        $ok = $post->updateMeta(['winners' => $winners], true);

        /*-----------------------------------------------
        | Feedback ...
        */

        return $this->jsonAjaxSaveFeedback($ok, [
                'success_message'    => trans('validation.attributes.number')
                    . " "
                    . ad($request->number)
                    . ": "
                    . $user->full_name,
                'success_modalClose' => false,
                'success_callback'   => "divReload( 'divWinnersTable' );rowUpdate('tblPosts','$post->hashid')",
            ]
        );
    }

    public function delete(Request $request)
    {
        $key = $request->key;

        $postId = session()->get('drawing_post');
        $post = Post::find($postId);
        if ($post and  $post->exists) {
            $winners = $post->winners_array;
            unset($winners[$key]);
            $post->updateMeta(['winners' => array_values($winners)], true);
        }
    }
}
