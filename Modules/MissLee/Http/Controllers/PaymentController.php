<?php

namespace Modules\MissLee\Http\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\MissLee\Services\Controller\MissLeeFrontControllerTrait;
use Modules\MissLee\Http\Requests\DeclareRequest;
use Modules\MissLee\Http\Requests\RepurchaseRequest;
use Modules\MissLee\Services\Module\CurrentModuleHelper;

class PaymentController extends Controller
{
    use MissLeeFrontControllerTrait;

    public function repurchase(RepurchaseRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }

        $newTransaction = $transaction->clone($request->only('account_id'));

        return $this->jsonFeedback([
            'ok'           => 1,
            'redirect'     => CurrentModuleHelper::actionLocale('PaymentController@fire', [
                 'transaction' => $newTransaction->getModel()->hashid,
            ]),
            'message'      => CurrentModuleHelper::trans('form.feed.wait'),
            'redirectTime' => 3000,
        ]);
    }

    public function fire(Request $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }


        $transactionResponse = $transaction->fire();

        if ($transactionResponse['slug'] == 'returnable-response') {
            return $transactionResponse['response'];
        }

        return redirect(CurrentModuleHelper::actionLocale('PurchaseController@paymentCallback', [
             'cart' => hashid_encrypt($transaction->getModel()->invoice_id, 'ids'),
        ]));
    }

    public function declare(DeclareRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction)) or !$transaction->isDeclarable()) {
            return $this->abort(410);
        }

        $rs = $transaction->resnum($request->trackingCode)
            ->declared_at($request->declarationDate)
            ->update();

        if ($rs['code'] > 0) {
            $transaction->cart()->update([
                'disbursed_at' => Carbon::now(),
                'disbursed_by' => user()->id,
            ]);
            return $this->jsonFeedback([
                'ok'           => 1,
                'message'      => CurrentModuleHelper::trans('form.feed.wait'),
                'refresh'      => 1,
                'redirectTime' => 3000,
            ]);
        } else {
            return $this->abort(404);
        }
    }

    protected function findTransaction($hashid)
    {
        if (($transaction = model('Transaction')->grabHashid($hashid)) and $transaction->exists) {
            return $transaction->handler;
        }

        return null;
    }
}
