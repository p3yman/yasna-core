<?php

use Modules\MissLee\Providers\RouteServiceProvider;

/*
|--------------------------------------------------------------------------
| Manage
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::group(['prefix' => 'convert', 'namespace' => 'Modules\MissLee\Http\Controllers'], function () {
        Route::get('{methodTitle}', 'ConvertController@convertMain');
        Route::get('users/{offset}', 'ConvertController@users')->name('convert.users');
        Route::get('posts/{offset}', 'ConvertController@posts')->name('convert.posts');
    });

    Route::group(
         [
              'middleware' => ['auth', 'is:admin'],
              'prefix'     => 'manage/miss-lee',
              'namespace'  => 'Modules\MissLee\Http\Controllers\Manage',
         ],
         function () {
             /*
             |--------------------------------------------------------------------------
             | Drawing
             |--------------------------------------------------------------------------
             |
             */
             Route::group(['prefix' => 'draw'], function () {
                 Route::get('{hashid}', 'DrawingController@draw');
                 Route::get('{hashid}/winners', 'DrawingController@winners');
                 Route::post('prepare', 'DrawingController@prepareForDraw');
                 Route::post('select', 'DrawingController@select');
                 Route::get('act/{hashid}/{viewFile}', 'DrawingController@singleAction');
                 Route::get('delete/{key}', 'DrawingController@delete');
             });
         }
    );


    Route::group([
         'namespace' => 'Modules\MissLee\Http\Controllers\Auth',
    ],
         function () {
             Route::get('login', 'LoginController@showLoginForm')->name('login');
             Route::get('home', 'LoginController@home')->name('home');

             Route::get('register', 'RegisterController@showRegistrationForm')->name('register');

             Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
         }
    );

    Route::get('pay/{cart}', 'Modules\MissLee\Http\Controllers\PurchaseController@paymentShortLink');

    Route::group(
         ['middleware' => ['locale'], 'namespace' => 'Modules\MissLee\Http\Controllers'],
         function () {
             Route::get('/', 'FrontController@index');
             Route::group(
                  [
                       'prefix' => '{lang}',
                       'where'  => ['lang' => '[^\/_][^\/]*+'],
                  ], function ($router) {
                 Route::group([
                      'namespace' => 'Auth',
                 ],
                      function () {
                          Route::get('login', 'LoginController@showLoginForm');
                          Route::post('login', 'LoginController@login');

                          Route::get('register', 'RegisterController@showRegistrationForm');
                          Route::post('register', 'RegisterController@register');

                          Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
                          Route::post('password/reset', 'ResetPasswordController@reset');
                          Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
                          //Route::post('password/email', 'ForgotPasswordController@showLinkRequestForm')->name('password.email');

                          Route::get('password/token/{haveCode?}', 'ResetPasswordController@getToken');
                          Route::post('password/token', 'ResetPasswordController@checkToken');
                          Route::get('password/new', 'ResetPasswordController@newPassword');
                          Route::post('password/new', 'ResetPasswordController@changePassword');
                      }
                 );

                 Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
                     Route::get('dashboard', 'UserController@index');

                     Route::get('orders', 'UserController@orders');
                     Route::get('orders/{order}/info', 'UserController@orderInfo');

                     Route::get('profile', 'UserController@profile');
                     Route::post('profile', 'UserController@update');

                     Route::get('drawing', 'UserController@drawing');

                     Route::get('events', 'UserController@events');

                     Route::get('addresses', 'UserController@addresses');
                     Route::get('addresses/{hashid}', 'UserController@addressesRow');
                     //            Route::get('/dashboard/previous_comments/{post_id}', 'UserController@previousComments');
                 });

                 Route::group(['prefix' => 'address', 'middleware' => 'auth'], function () {
                     Route::post('save', 'AddressController@save');
                 });

                 Route::group(['prefix' => 'cart'], function () {
                     Route::get('/', 'CartController@index');
                     Route::get('flush', 'CartController@flush');
                     Route::post('modify', 'CartController@modifyCart');

                     Route::group(['prefix' => 'item'], function () {
                         Route::post('add', 'CartController@addItem');
                         Route::post('remove', 'CartController@removeItem');
                         Route::post('{hashid}', 'CartController@modifyItem');
                     });

                     Route::group(['prefix' => 'view'], function () {
                         Route::get('reload', 'CartController@reloadView');
                     });
                 });

                 Route::group(['prefix' => 'purchase'], function () {
                     Route::get('user', 'PurchaseController@step1');

                     Route::group(['middleware' => 'auth'], function () {
                         Route::get('shipping', 'PurchaseController@step2');
                         Route::get('shipping/content', 'PurchaseController@reloadStep2');
                         Route::get('shipping/verify', 'PurchaseController@verifyStep2');

                         Route::get('review', 'PurchaseController@step3');
                         Route::get('review/verify', 'PurchaseController@verifyStep3');

                         Route::get('pay', 'PurchaseController@step4');

                         Route::post('final', 'PurchaseController@final');

                         Route::get('payment/result/{cart}', 'PurchaseController@paymentCallback')
                              ->middleware('ownCart')
                         ;
                     });
                 });

                 Route::group(['prefix' => 'payment'], function () {
                     Route::post('repurchase/{transaction}', 'PaymentController@repurchase');
                     Route::get('fire/{transaction}', 'PaymentController@fire');
                     Route::post('declare/{transaction}', 'PaymentController@declare');
                 });

                 Route::group(['prefix' => 'purchase'], function () {
                     Route::get('reload', 'CartController@reloadView');
                 });


                 Route::group(['prefix' => 'province'], function () {
                     Route::get('cities/{province}/select', 'ProvinceController@getProvinceCitiesSelect');
                 });

                 Route::get('events/{type}', 'PostController@eventsAjax')
                      ->where('type', '^(waiting|expired){1,1}$')
                 ;

                 Route::group(['prefix' => 'drawing-code'], function () {
                     Route::post('submit', 'DrawingController@submitCode');
                 });

                 Route::get('contact', 'FrontController@contact');

                 Route::get('products/search', 'PosttypeController@productsSearch');

                 Route::group(['prefix' => 'archive'], function () {
                     Route::get('{posttype}/categories', 'PosttypeController@showCategories');
                     Route::post('{posttype}/filter', 'PosttypeController@filter');
                     Route::get('{posttype}/{folder?}', 'PosttypeController@showList');
                 });
                 foreach (RouteServiceProvider::getStaticPosttypes() as $posttype) {
                     Route::get("$posttype/categories", "PosttypeController@{$posttype}Categories");
                     Route::get("$posttype/{folder?}", "PosttypeController@$posttype");
                 }

                 Route::get('{identifier}/more-comments/{comment?}', 'PostController@moreComments')
                      ->where('identifier', '^P(\w|)+$')
                 ;

                 Route::get('{identifier}/{title?}', 'PostController@single')
                      ->where('identifier', '^P(\w|)+$')
                 ;


                 Route::get('product/{identifier}/{wareOrTitle?}/{title?}', 'PostController@productSingle')
                      ->where('identifier', '^P(\w|)+$')
                 ;

                 Route::group(['prefix' => 'comment'], function () {
                     Route::post('save', 'CommentController@save');
                 });

                 Route::get('tag/{tag}', 'TagController@index')->name('miss-lee.tag');

                 Route::get('{page}', 'FrontController@page');

                 Route::get('', 'FrontController@index');

                 Route::group(['prefix' => 'test'], function () {
                     Route::get('product-filter-form', 'TestController@productFilterForm');
                 });
             });
         });
});
