<?php

namespace Modules\MissLee\Listeners;

use Modules\MissLee\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignRolesOnUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\MissLee\Events\Registered $event
     *
     * @return void
     */
    public function handle(\Modules\MissLee\Events\Registered $event)
    {
        $user = $event->user;

        $user->attachRole('customer');
    }
}
