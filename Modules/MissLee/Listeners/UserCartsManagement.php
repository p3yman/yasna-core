<?php namespace Modules\MissLee\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\MissLee\Providers\CartServiceProvider;
use Modules\Yasna\Events\UserLoggedIn;

class UserCartsManagement implements ShouldQueue
{
    public $tries = 5;

    public function handle(UserLoggedIn $event)
    {
        dd($event, __FILE__ . ' - ' . __LINE__);
    }
}
