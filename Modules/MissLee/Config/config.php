<?php

return [
     'name' => 'MissLee',
     'slug' => 'miss-lee',

     'username-field' => 'code_melli',

     'post-url-prefix' => 'P',

     'edu-levels-values' => [
          'not-specified'     => '0',
          'less-than-diploma' => '1',
          'diploma'           => '2',
          'associate-degree'  => '3',
          'bachelor-degree'   => '4',
          'master-degree'     => '5',
          'phd-or-more'       => '6',
     ],

     'genders-values' => [
          'male'   => 1,
          'female' => 2,
          'other'  => 3,
     ],

     'marital-statuses-values' => [
          'single'  => '1',
          'married' => '2',
     ],

     "draw" => [
          "query-limit" => 300,
     ],

     "default-province" => "تهران",
     "default-city"     => "تهران",

     "colors" => [
          "transparent" => "transparent",
          "white"       => "#ffffff",
          "gray"        => "#bdbdbd",
          "black"       => "#000000",
          "cream"       => "#f1e0c7",
          "brown"       => "#562c1f",
          "smoky"       => "#424242",
          "pewter"      => "#7f7161",
          "chocolate"   => "#964a20",
          "red"         => "#e82121",
          "dark_red"    => "#c70000",
          "ruby"        => "#e0115f",
          "pink"        => "#f76e9c",
          "magenta"     => "#7b1fa2",
          "purple"      => "#7e57c2",
          "dark_blue"   => "#283593",
          "blue"        => "#03a9f4",
          "light_blue"  => "#81d4fa",
          "cyan"        => "#00acc1",
          "teal"        => "#26a69a",
          "dark_green"  => "#148c19",
          "green"       => "#4caf50",
          "light_green" => "#9ccc65",
          "yellow"      => "#ffeb3b",
          "orange"      => "#ff9800",
          "deep_orange" => "#ff5722",
     ],


     'map-box' => include(__DIR__ . DIRECTORY_SEPARATOR . 'map-box.php'),
];
