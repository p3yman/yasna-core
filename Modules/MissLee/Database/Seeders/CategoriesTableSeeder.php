<?php

namespace Modules\MissLee\Database\Seeders;

use App\Models\Category;
use App\Models\Posttype;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //$this->seedProductsCategories();
    }



    /**
     * Seed Categories
     */
    protected function seedProductsCategories()
    {
        $posttype = posttype()->grabSlug('products');
        $data     = $this->getProductCategoriesData();

        $generalData  = array_fill(0, count($data), [
             'posttype_id' => $posttype->id,
             'is_folder'   => 0,
             'parent_id'   => 0,
             'locales'     => '|fa|en|',
        ]);
        $modifiedData = array_replace_recursive($generalData, $data);

        yasna()->seed('categories', $modifiedData);
    }



    /**
     * @return array
     */
    protected function getProductCategoriesData()
    {
        return [
             [
                  'slug'        => 'spice',
                  'meta-titles' => [
                       'fa' => 'ادویه',
                       'en' => 'Spice',
                  ],
             ],
             [
                  'slug'        => 'nuts',
                  'meta-titles' => [
                       'fa' => 'آجیل',
                       'en' => 'Nuts',
                  ],
             ],
             [
                  'slug'        => 'honey',
                  'meta-titles' => [
                       'fa' => 'عسل',
                       'en' => 'Honey',
                  ],
             ],
             [
                  'slug'        => 'herbal-tea',
                  'meta-titles' => [
                       'fa' => 'دمنوش',
                       'en' => 'Herbal Tea',
                  ],
             ],
             [
                  'slug'        => 'dried-fruit',
                  'meta-titles' => [
                       'fa' => 'خشکبار',
                       'en' => 'Dried Fruits',
                  ],
             ],
             [
                  'slug'        => 'plum',
                  'meta-titles' => [
                       'fa' => 'آلو و لواشک',
                       'en' => 'Plum',
                  ],
             ],
             [
                  'slug'        => 'fruits-tab',
                  'meta-titles' => [
                       'fa' => 'برگه‌ی میوه',
                       'en' => 'Fruits Tabs',
                  ],
             ],
             [
                  'slug'        => 'dried-herbs',
                  'meta-titles' => [
                       'fa' => 'سبزیجات خشک',
                       'en' => 'Dried Herbs',
                  ],
             ],
             [
                  'slug'        => 'saffron',
                  'meta-titles' => [
                       'fa' => 'زعفران',
                       'en' => 'Saffron',
                  ],
             ],
             [
                  'slug'        => 'pastry',
                  'meta-titles' => [
                       'fa' => 'شیرینی',
                       'en' => 'Pastry',
                  ],
             ],
             [
                  'slug'        => 'chocolate',
                  'meta-titles' => [
                       'fa' => 'شکلات',
                       'en' => 'Chocolate',
                  ],
             ],
             [
                  'slug'        => 'snack',
                  'meta-titles' => [
                       'fa' => 'اسنک',
                       'en' => 'Snack',
                  ],
             ],
        ];
    }
}
