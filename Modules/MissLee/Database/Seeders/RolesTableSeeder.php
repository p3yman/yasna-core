<?php

namespace Modules\MissLee\Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('roles', [
             [
                  'slug'         => "customer",
                  'title'        => "مشتری",
                  'plural_title' => "مشتری‌ها",
                  'created_at'   => Carbon::now()->toDateTimeString(),
                  'is_admin'     => "0",
             ],
        ]);
    }
}
