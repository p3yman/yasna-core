<?php

namespace Modules\MissLee\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MissLeeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(RolesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(LabelsTableSeeder::class);
    }
}
