<?php

namespace Modules\MissLee\Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Providers\YasnaServiceProvider;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $array = [
            [
                'slug'          => 'maximum_pending_jobs',
                'title'         => 'بیشترین تعداد jobهای در انتظار',
                'category'      => 'upstream',
                'order'         => '92',
                'data_type'     => 'text',
                'default_value' => '10',
                'hint'          => 'اگر به تعداد عدد ثبت شده در این قسمت job در انتظار داشته باشیم queue:work اجرا می‌شود.',
                'css_class'     => '',
                'is_localized'  => '',
            ],
        ];

        yasna()->seed('settings', $array);
    }
}
