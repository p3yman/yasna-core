<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            /*-----------------------------------------------
            | Primary Key
            */
            $table->increments('id', 10);

            /*-----------------------------------------------
            | Purchasement Info
            */
            $table->integer('user_id')->index();
            $table->string('code', 40)->index();
            $table->dateTime('purchased_at')->nullable();
            $table->integer('purchased_amount')->default(0);
            $table->string('operation_string', 20)->nullable()
                ->comment('reserved column to be eventually used in drawing procedure');
            $table->integer('operation_integer')->default(0)
                ->comment('reserved column to be eventually used in drawing procedure');
            $table->boolean('is_verified')->default(false);

            /*-----------------------------------------------
            | Meta
            */
            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Change Logs
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
