<?php

namespace Modules\MissLee\Providers;

use App\Models\Category;
use App\Models\Posttype;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class PosttypeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Returns collection of a posttype's categories
     *
     * @param string $posttypeSlug
     * @param bool   $active If true, only categories with at least one post is in returned list
     *
     * @return mixed
     */
    public static function getPosttypeCategories(string $posttypeSlug, bool $active = false)
    {
        $posttype = posttype($posttypeSlug);

        $categories = Category::where('posttype_id', $posttype->id);

        if ($active) {
            $usedCategories = DB::table('category_post')
                ->select('category_id')
                ->groupBy('category_id')
                ->get()
                ->pluck('category_id')
                ->toArray();

            $categories->whereIn('id', $usedCategories);
        }

        return $categories->get();
    }
}
