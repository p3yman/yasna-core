<?php

namespace Modules\MissLee\Providers;

use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getCurrency()
    {
        return 'IRR';
    }

    /**
     * Returns the currency that should be used in showing prices to user
     *
     * @return string
     */
    public static function getPreviewCurrency()
    {
        return 'IRT';
    }

    public static function getPreviewCurrencyRatio()
    {
        return 0.1;
    }
}
