<?php

namespace Modules\MissLee\Providers;

use App\Models\Cart;
use App\Models\Courier;
use App\Models\Posttype;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Mockery\Exception;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected static $sessionName                    = 'cart';
    protected static $progressiveCouriersSlugsPrefix = 'progressive';

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function addToActiveCart(...$parameters)
    {
        $cart = CartServiceProvider::findActiveCart(\user(), true);
        return $cart->addOrder(... $parameters);
    }

    /**
     * @param \App\Models\User|null $user
     * @param bool                  $create
     *
     * @return \App\Models\Cart
     */
    public static function findActiveCart(User $user = null, $create = false)
    {
        if ($user and $user->exists) {
            return self::getUsersCart($user, $create);
        } elseif (!auth()->guest()) {
            return self::getUsersCart(\user(), $create);
        } else {
            return self::getSessionCart($create);
        }
    }

    public static function getUsersCart(User $user, $create = false)
    {
        $conditions = array_merge(self::newCartGeneralData(), [
            'user_id'   => $user ? $user->id : 0,
            'raised_at' => null,
        ]);

        if ($create) {
            return Cart::firstOrCreate($conditions);
        } else {
            return Cart::firstOrNew($conditions);
        }
    }

    public static function clearSession()
    {
        return session()->forget(self::$sessionName);
    }

    public static function getSessionCart($create = false)
    {
        if (session()->exists(self::$sessionName)) {
            $cartHashid = session()->get(self::$sessionName);
            if (($foundCart = model('Cart')->grabHashid($cartHashid)) and $foundCart->exists) {
                return $foundCart;
            }
        }

        $newCart = self::newCart($create);
        return $newCart;
    }

    protected static function newCart($create, User $user = null)
    {
        $attributes = array_merge(self::newCartGeneralData(), [
            'user_id' => $user ? $user->id : 0,
        ]);
        if ($create) {
            $newCart = Cart::create($attributes);
            session()->put(self::$sessionName, $newCart->hashid);
            return $newCart;
        } else {
            return new Cart($attributes);
        }
    }

    protected static function newCartGeneralData()
    {
        return [
            'currency' => CurrencyServiceProvider::getCurrency(),
            'locale'   => getLocale(),
        ];
    }

    public static function getCartIdFromSession()
    {
        if (session()->exists(self::$sessionName)) {
            return session()->get(self::$sessionName);
        }

        return null;
    }

    public static function hasVerifiedTransaction(Cart $cart)
    {
        return boolval(payment()
            ->invoice($cart->id)
            ->status('verified')
            ->count());
    }

    public static function getSuitableCourier(Cart $cart)
    {
        $cart->removeCourier();
        $cartPrice = $cart->invoiced_amount;

        if (
            ($courierSlug = self::findSuitableCourierSlug($cartPrice)) and
            ($courier = model('Courier')->grabSlug($courierSlug)) and
            $courier->exists
        ) {
            return $courier;
        } else {
            return null;
        }
    }

    protected static function findSuitableCourierSlug($price)
    {
        $productsPosttype = posttype('products');

        if (
            $levelsStr = $productsPosttype->spreadMeta()->courier_price_levels and
            count($levels = array_filter(preg_split("/\\r\\n|\\r|\\n/", $levelsStr)))
        ) {
            foreach ($levels as $level) {
                try {
                    if (count($levelParts = explode_not_empty(':', $level)) < 2) {
                        continue;
                    }
                    list($range, $slug) = $levelParts;

                    if (count($rangeParts = explode('-', $range)) < 2) {
                        continue;
                    }

                    list($floor, $ceil) = $rangeParts;

                    if ($price > $floor and $price <= $ceil) {
                        return trim($slug);
                    }
                } catch (Exception $exception) {
                    continue;
                }
            }
        }

        return null;
    }
}
