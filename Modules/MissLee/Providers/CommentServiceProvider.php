<?php

namespace Modules\MissLee\Providers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Support\ServiceProvider;

class CommentServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected static $commentsPageLength = 20;
    protected static $commentRepliesPageLength = 3;
    protected static $commentOrderParameters = ['created_at', 'desc'];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Translate standard "fields" value of a "commenting" post
     *
     * @param $fieldsString string
     *
     * @return array
     */
    public static function translateFields($fieldsString)
    {
        /**
         * translation rules
         * 1. separate fields description with comma (,)
         * 2. start each field description with its "name" (ex: "subject,first_name")
         * 3. if you want that field to has a label add "-label" to the field description (ex: "subject-label")
         * 4. if you want that field to has a column size (bootstrap columns) add ":size" (ex: "subject:6")
         * 5. put no "next-line" in fields string
         */

        if ($fieldsString and is_string($fieldsString)) {
            $fields = explode_not_empty(',', $fieldsString);
            foreach ($fields as $fieldIndex => $fieldValue) {
                unset($fields[$fieldIndex]);
                $fieldValue = trim($fieldValue);

                if (str_contains($fieldValue, '*')) {
                    $fieldValue           = str_replace('*', '', $fieldValue);
                    $tmpField['required'] = true;
                } else {
                    $tmpField['required'] = false;
                }

                if (str_contains($fieldValue, '-label')) {
                    $fieldValue        = str_replace('-label', '', $fieldValue);
                    $tmpField['label'] = true;
                } else {
                    $tmpField['label'] = false;
                }

                if (str_contains($fieldValue, ':')) {
                    $fieldValueParts  = explode_not_empty(':', $fieldValue);
                    $fieldValue       = $fieldValueParts[0];
                    $tmpField['size'] = $fieldValueParts[1];
                } else {
                    $tmpField['size'] = '';
                }
                $fields[$fieldValue] = $tmpField;
                unset($tmpField);
            }
            return $fields;
        }

        return [];
    }

    /**
     * Translate standard "rules" value of a "commenting" post
     *
     * @param string  $rulesString
     * @param boolean $detailed If "false" this function will return rules of any field in a single item (request
     *                          standard rule)
     *
     * @return array
     */
    public static function translateRules($rulesString, $detailed = true)
    {
        /**
         * translation rules
         * 1. separate fields rules with comma (,)
         * 2. start each field description with its "name" (ex: "subject,first_name")
         * 3. after name of the field, put "=>" and enter rules in the pipeline separated format
         * 4. each rule should be one of laravel standard rules
         * 5. put no "next-line" in rules string
         */

        if ($rulesString and is_string($rulesString)) {
            $rulesString = str_replace("\n", '', $rulesString);
            $rules       = explode_not_empty(',', $rulesString);

            foreach ($rules as $index => $rule) {
                $rule = trim($rule);
                unset($rules[$index]);

                $parts = explode_not_empty('=>', $rule);

                if ($parts and is_array($parts) and (count($parts) == 2)) {
                    $fieldName = trim($parts[0]);
                    if ($detailed) {
                        $fieldRules = explode_not_empty('|', $parts[1]);
                        if (count($fieldRules)) {
                            $rules[$fieldName] = $fieldRules;
                        }
                    } else {
                        $fieldRules = $parts[1];
                        if ($fieldRules) {
                            $rules[$fieldName] = $fieldRules;
                        }
                    }
                }
            }

            return $rules;
        }

        return [];
    }

    /**
     * Translate standard "conversions" value of a "commenting" post
     *
     * @param string|null $conversionsString
     * @param bool        $detailed
     *
     * @return array
     */
    public static function translateConversions($conversionsString, $detailed = true)
    {
        /**
         * translation rules
         * 1. separate fields with comma (,)
         * 2. start each field description with its "name" (ex: "subject,first_name")
         * 3. after name of the field, put "=>" and enter the name to be converted to
         */

        if ($conversionsString and is_string($conversionsString)) {
            $conversionsString = str_replace("\n", '', $conversionsString);
            $fields            = explode_not_empty(',', $conversionsString);

            foreach ($fields as $index => $description) {
                $description = trim($description);
                unset($fields[$index]);

                $parts = explode_not_empty('=>', $description);

                if ($parts and is_array($parts) and (count($parts) == 2)) {
                    $sourceFields = trim($parts[0]);
                    $targetFields = $parts[1];
                    if ($targetFields) {
                        $fields[$sourceFields] = $targetFields;
                    }
                }
            }

            return $fields;
        }

        return [];
    }

    public static function posttypeRules(Posttype $posttype)
    {
        $rules = [];
        $posttype->spreadMeta();

        if (auth()->guest()) {
            $rules['name'] = ['required'];

            if ($posttype->commenting_enter_email == 'must') {
                $rules['email'] = ['required'];
            }

            if ($posttype->commenting_enter_mobile == 'must') {
                $rules['mobile'] = ['required'];
            }
        }

        return $rules;
    }

    public static function selectPostComments(Post $post, $pageLength = null)
    {
        return $post->comments()
            ->where('parent_id', 0)
            ->whereNotNull('approved_at')
            ->orderBy(...self::$commentOrderParameters)
            ->paginate($pageLength ?: self::$commentsPageLength);
    }

    public static function selectCommentReplies(Comment $comment, $pageLength = null)
    {
        return $comment->children()
            ->whereNotNull('approved_at')
            ->orderBy(...self::$commentOrderParameters)
            ->paginate($pageLength ?: self::$commentRepliesPageLength);
    }
}
