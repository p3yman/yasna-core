<?php

namespace Modules\MissLee\Providers;

use Illuminate\Support\ServiceProvider;

class DrawingCodeServiceProvider extends ServiceProvider
{
    protected static $codeLength = 20;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function checkCode($unique)
    {
        if (strlen($unique) != self::$codeLength or ! is_numeric($unique)) {
            return false;
        }

        $unique = str_split($unique);
        $data = array();

        // change 20 and 15 uniq code character
        $character15 = $unique[14];
        $character20 = $unique[19];
        $unique[14] = $character20;
        $unique[19] = $character15;

        $controller = 9 - $unique[19];

        // controller number
        $starter = 9 - $unique[0];
        if ($starter <= 2) {
            $starter = 8;
        }

        // create controller number
        $sum = 0;
        for ($i = 18; $i > 0; $i--) {
            if ($starter < 2) {
                $starter = 8;
            }
            $sum += ($unique[$i] * $starter);
            $starter--;
        }
        $sum += $unique[0];
        $sum = str_split($sum);

        if ($sum[count($sum) - 1] != $controller) {
            return false;
        }

        // change 1 and 5 uniq code character
        $character1 = $unique[0];
        $character5 = $unique[4];
        $unique[0] = $character5;
        $unique[4] = $character1;

        $data['date'] = 1 . $unique[1] . $unique[3] . $unique[5] . $unique[7] . $unique[9] . $unique[11] . $unique[13] . $unique[15] . $unique[17];

        $price = $unique[2] . $unique[4] . $unique[6] . $unique[8] . $unique[10] . $unique[12] . $unique[14] . $unique[16] . $unique[18];
        $price = str_split($price);
        $data['price'] = '';
        for ($i = 0; $i < $unique[0]; $i++) {
            $data['price'] .= (9 - $price[$i]);
        }

        return $data;
    }
}
