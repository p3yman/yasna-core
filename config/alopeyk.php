<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API ACCESS-TOKEN
    |--------------------------------------------------------------------------
    |
    | TODO:
    | Some Description About This
    */

    'access-token' => env('ALOPEYK_API_TOKEN'),
    'debug-enabled' => env('ALOPEYK_DEBUG_MODE', false),

];
