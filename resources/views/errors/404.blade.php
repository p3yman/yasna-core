{{--
|--------------------------------------------------------------------------
| If modules does not exist
|--------------------------------------------------------------------------
|
--}}

@if(!function_exists('module'))
	@include('errors.default')
{{ die() }}
@endif

{{--
|--------------------------------------------------------------------------
| Looking into the service
|--------------------------------------------------------------------------
|
--}}
@php
    $blade = module('yasna')->service('errors')->find('404')->get('blade');
@endphp

@if($blade)
	@include($blade)
@else
	@include('errors.default')
@endif
